import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningFiDetailsPage } from './dining-fi-details.page';

const routes: Routes = [
  {
    path: '',
    component: DiningFiDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningFiDetailsPageRoutingModule {}
