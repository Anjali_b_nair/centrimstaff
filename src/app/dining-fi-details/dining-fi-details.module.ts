import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningFiDetailsPageRoutingModule } from './dining-fi-details-routing.module';

import { DiningFiDetailsPage } from './dining-fi-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningFiDetailsPageRoutingModule
  ],
  declarations: [DiningFiDetailsPage]
})
export class DiningFiDetailsPageModule {}
