import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningMenuChangePageRoutingModule } from './dining-menu-change-routing.module';

import { DiningMenuChangePage } from './dining-menu-change.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningMenuChangePageRoutingModule
  ],
  declarations: [DiningMenuChangePage]
})
export class DiningMenuChangePageModule {}
