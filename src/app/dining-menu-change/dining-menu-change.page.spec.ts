import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningMenuChangePage } from './dining-menu-change.page';

describe('DiningMenuChangePage', () => {
  let component: DiningMenuChangePage;
  let fixture: ComponentFixture<DiningMenuChangePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningMenuChangePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningMenuChangePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
