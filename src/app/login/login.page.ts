import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // keyboardStyle = { width: '100%', height: '0px' };
  device:any;
  email:string;
  password:string;
  subscription:Subscription;
  hide_pswd:boolean=true;
  passwordType:any='password';
  agree:any=1;
  isLoading:boolean=true;
  constructor(private router:Router,private config:HttpConfigService,
    private toastCntlr:ToastController,private storage:Storage,private http:HttpClient,
    private platform:Platform,private keyboard:Keyboard,private loadingCtrl:LoadingController,private iab:InAppBrowser) { }

  ngOnInit() {
    // this.keyboard.onKeyboardWillShow().subscribe( {
    //   next: x => {
    //     this.keyboardStyle.height = x.keyboardHeight + 'px';
    //   },
    //   error: e => {
    //     console.log(e);
    //   }
    // });
    // this.keyboard.onKeyboardWillHide().subscribe( {
    //   next: x => {
    //     this.keyboardStyle.height = '0px';
    //   },
    //   error: e => {
    //     console.log(e);
    //   }
    // });


  }
  async ionViewWillEnter(){
    console.log('storage is clear:',await this.storage.get('USERTOKEN'));
    
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display="none";
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      
       
      navigator['app'].exitApp(); 
    
      
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      position:'top',
      cssClass:'toastStyle',
      duration: 3000      
    });
    alert.present(); //update
  }


  login(){
    if(this.email&&this.password){
    if(this.agree==0){
      this.presentAlert('Please agree to terms & conditions.')
    }else{
    let body={
      email:this.email,
      password:this.password,
      nw_id:0
    }
    this.keyboard.hide();
    this.http.post(this.config.login_path,body).subscribe(async (res:any)=>{
      console.log("ogin:",res);
      
           console.log("res:",res.data.user_type_id);
           if((res.data.user_type_id==5) ||(res.data.user_type_id==6)||(res.data.user_type_id==4)||(res.data.user_type_id==9)){
            
            this.presentAlert('Please enter valid credentials.');
           
          }
          // this.showLoading();
          else{
            console.log("else worked:");
            let branch;
            if(res.data.user_type_id===1){
              this.storage.set("BRANCH",'0');
              console.log("sadmin:",res.data.user_type_id);
              branch=0
              
              }else{
                console.log("type3&4:",res.data.user_type_id);
                
                this.storage.set("BRANCH",res.data.user_details.branch_id.toString());
                this.storage.set("PCFBRANCH",res.data.user_details.user_branch.pcs_branch_id);
                this.storage.set("BNAME",res.data.branchname);
                this.storage.set("TIMEZONE",res.data.user_details.user_branch.timezone);
                branch=res.data.user_details.branch_id;
                this.storage.set("BRANCH_NAME", res.data.branchname);
                this.storage.set('RVSETTINGS',res.data.user_details.user_branch.retirement_living)
              }
              if ((await this.storage.get("loginedupdate")) != "deleted")
              this.storage.set("loginedupdate", 0);

      this.storage.set("TOKEN", res.token);
     
           this.storage.set("EXPIRES_IN", res.expires);
          //  this.storage.set('RVSETTINGS',res.data.user_details.user_branch.retirement_living);
           this.storage.set("SURVEY_CID", res.company_id);

           this.storage.remove("USER_ID");
           this.storage.set("USER_ID", res.data.user_id);

           this.storage.remove("loginuserid");
             this.storage.set("loginuserid", res.data.user_id);

           this.storage.set("USER_TYPE",res.data.user_type_id);
           this.storage.set("NAME", res.data.name);
           this.storage.set("USER_NAME", res.data.user_name);
           this.storage.set("EMAIL", res.data.email);
           this.storage.set("PRO_IMG",res.data.profile_pic);
           this.storage.set("PHONE",res.data.phone);
           this.storage.set("COMPANY_ID",res.data.nw_id);
           
           
        
           
          //  this.storage.set("loggedIn",1);
         
                if(res.data.first_login==1){
                 this.router.navigate(['/menu',{flag:1,branch:branch}],{replaceUrl:true});
                 this.storage.set("loggedIn",1);
                 this.storage.remove("USERTOKEN");
                    this.storage.set('USERTOKEN',res.user_token);
                    this.storage.remove("checkusertoken");
                  this.storage.set("checkusertoken", res.data.token);
                }
                else{
                  let img;
                  if(res.data.profile_pic){
                    img=res.data.profile_pic
                  }else{
                    img='https://app.centrim.life/assets/img/user/user.png'
                  }
                  // let phone;
                  // if(res.data.mobile){
                  //   phone=res.data.mobile
                  // }else{
                  //   phone=res.data.phone
                  // }
                  let info={email:res.data.email,name:res.data.name,uname:res.data.user_name,img:img,phone:res.data.phone,mobile:res.data.mobile}
                  this.router.navigate(['/onboarding-welcome',{info:JSON.stringify(info)}],{replaceUrl:true})
                  // this.router.navigate(['/resetpassword',{flag:3}]);
        
                }
         
            
          }
           // this.dismissLoader();
         },error=> {
          if (error.error.error == 'Not an active user') {
            this.presentAlert('This account is inactive.');
          } else {
            this.presentAlert('Please check your credentials');
          }
           
       });
      }
    }else{
      this.presentAlert('Please enter your credentials')
    }
  }
  showPassword(i){
    this.hide_pswd=!this.hide_pswd;
    if(i==2){
      this.passwordType='text';
    }else{
      this.passwordType='password'
    }
  }
  terms(i){
    // if(status.currentTarget.checked==true)
    if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.agree=1;
    }else{
      this.agree=0;
    }
  }
}
@HostListener('touchstart')
onTouchStart() {
  console.log('pageload:',this.isLoading)
  this.isLoading=false;
}
openTerms(){
  
    let options:InAppBrowserOptions ={
      location:'yes',
    hideurlbar:'yes',
    zoom:'no',
    hidenavigationbuttons:'yes'
    }
    // const browser = this.iab.create('https://app.centrim.life/terms-and-conditions','_blank',options);
    const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)
  
}
}
