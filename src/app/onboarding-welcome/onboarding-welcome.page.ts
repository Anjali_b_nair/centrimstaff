import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-onboarding-welcome',
  templateUrl: './onboarding-welcome.page.html',
  styleUrls: ['./onboarding-welcome.page.scss'],
})
export class OnboardingWelcomePage implements OnInit {
  info:any;
  subscription:Subscription;
    constructor(private platform:Platform,private router:Router,private route:ActivatedRoute) { }
  
    ngOnInit() {
    }
  
    ionViewWillEnter(){
      this.info=JSON.parse(this.route.snapshot.paramMap.get('info'));
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
        this.router.navigate(['/']);
      });     
    }
    ionViewDidLeave(){
       
        this.subscription.unsubscribe();
    }
    next(){
      this.router.navigate(['/onboarding-p1',{info:JSON.stringify(this.info)}])
    }

}
