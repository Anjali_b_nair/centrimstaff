import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { GetobjectService } from '../services/getobject.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';

import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import moment from 'moment';
import { Chooser } from '@ionic-native/chooser/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { OpenImageComponent } from '../components/open-image/open-image.component';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';

import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { RegisteredEmailWarningComponent } from '../components/registered-email-warning/registered-email-warning.component';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-consumer-about',
  templateUrl: './consumer-about.page.html',
  styleUrls: ['./consumer-about.page.scss'],
})
export class ConsumerAboutPage {
  consumer: any = {};
  user:any = {};
  fname: any;
  lname: any;
  dob: any;
  gender: any;
  img: any;
  pic: any;
  name: any
  wingArray: any = [];
  wing: any;
  wingId: any;
  branch: any = [];
  branch1: any;
  branch_id: any;
  room: any;
  con_type: any;
  adm_date: any;
  status: any;
  user_name: any;
  phone: any;
  email: any;
  description: any;
  likes: any;
  dislikes: any;
  interest: any;
  user_id: any;

  isImg: boolean = false;
  isVdo: boolean = false;
  isDoc: boolean = false;
  isMedia: boolean = false;
  // img:any=[];
  imageResponse: any = [];
  // vdo:any=[];
  vdoResponse: any = [];
  docs: any = [];
  docResponse: any = [];
  media: any = [];
  mediaResponse: any = [];
  images: any = [];
  videos: any = [];
  hasAttach: boolean = false;

  MAX_FILE_SIZE = 24 * 1024 * 1024;
  base64File: any;
  consent: boolean = false;
  subscription: Subscription;
  expanded1: boolean = false;
  expanded2: boolean = false;
  expanded3: boolean = false;
  expanded4: boolean = false;
  expanded5: boolean = false;
  cropOptions: CropOptions = {
    quality: 50
  }
  flag: any;
  family: any[] = [];
  share: boolean = false;
  familyArray: any[] = [];
  isLoading: boolean = false;
  utype:any;

  // name:any;
  edit:boolean=false;
  genderArray:any=[];

  listener;
selectAllCheckBox: any;
checkBoxes: HTMLCollection;

customAlertOptions: any = {
  
  // header: 'Facility',
  // subHeader: 'Select All:',
  message: '<ion-checkbox id="selectAllFamilyCheckBox"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
  constructor(private objService: GetobjectService, private http: HttpClient, private config: HttpConfigService,
    private storage: Storage,private alertCntlr: AlertController, private actionsheetCntlr: ActionSheetController, 
    private toastCntlr: ToastController, private platform: Platform, private router: Router, private chooser: Chooser,
    private iab: InAppBrowser, private loadingCtrl: LoadingController, private dialog: SpinnerDialog,
    private popCntl: PopoverController,  private route: ActivatedRoute,private modalCntl:ModalController,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) { }

 
  async ionViewWillEnter() {
   
    
    this.getGender();
    this.user = this.objService.getExtras();
   
    this.flag = this.route.snapshot.paramMap.get('flag');
    this.getDetails();
    this.getWing();
    this.getBranch();
   
    


      const data=await this.storage.get("USER_TYPE");

        this.utype=data;
     
        (await this.config.getUserPermission()).subscribe((res: any) => {
       
          let routes = res.user_routes;
          if (res.main_permissions.residents==1&&routes.includes('residents.edit')) {
    
            this.edit= true
          } else {
    
            this.edit = false;
            
          }
        });
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.back();
      // this.router.navigate(['/resident-profile', { id: this.consumer.id, flag: this.flag }], { replaceUrl: true });
    });

  }
  async getDetails(){
    this.showLoading();
    this.images = [];
    //  this.videos=[];
    this.media = [];
    this.docs = [];
    this.imageResponse = [];
    this.mediaResponse = [];
    this.docResponse = [];
    this.familyArray = [];
    this.family = [];
    let url=this.config.domain_url+'consumer/'+this.user.id;
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers)
     this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("dat:",url,res)
      this.consumer=res.data;
      this.user_id = this.consumer.id;
    this.img = this.consumer.user.profile_pic;
    this.name = this.consumer.user.name;
    this.fname = this.consumer.fname;
    this.lname = this.consumer.lname;
    this.dob = new Date(this.consumer.dob).toISOString();
    if(this.consumer.gender){
    this.gender = this.consumer.gender.gender_id.toString();
    }
    //  this.consent=this.consumer.consent_to_tag;
    // this.familyArray.push({ id: 0, name: 'Select all' });
    this.consumer.contacts.forEach(element => {
      this.familyArray.push({ id: element.user.user_id, name: element.user.name })
      if (element.share_details == 0) {
        this.family.push(element.user.user_id.toString());
      }
    });
    if (this.consumer.share_details == 1) {
      this.share = false
    } else {
      this.share = true;
    }
    if (this.consumer.consent_to_tag == 0) {
      this.consent = false
    } else {
      this.consent = true;
    }
    console.log("consumer:", this.consumer, this.share);
    this.branch1 = this.consumer.user.user_details.user_branch.name;
    this.branch_id=this.consumer.user.user_details.user_branch.id;
    this.room = this.consumer.room;
    this.con_type = this.consumer.consumer_type;
    this.adm_date = new Date(this.consumer.admission_date).toISOString();
    this.status = this.consumer.status;
    this.user_name = this.consumer.user.user_name;
    if(this.consumer.wing){
    this.wingId=this.consumer.wing.id.toString();
    this.wing=this.consumer.wing.name;
    }
    this.isLoading = true;
    
    this.email = this.consumer.user.email;
    this.description = this.consumer.description;
    this.likes = this.consumer.likes;
    this.dislikes = this.consumer.dislikes;
    this.interest = this.consumer.interests;
    if(this.consumer.user.mobile){
      let phone = this.consumer.user.mobile.toString();
    if (phone.substring(0, 2) == '61') {
      this.phone = phone.substring(2);
    } else {
      this.phone = phone
    }
    }
    if (this.consumer.documents.length > 0) {
      this.hasAttach = true;
      this.isMedia = true;
      this.consumer.documents.forEach(element => {

        // this.media.push(element.document);
        let docname;
        let name =element.document.substring(element.document.lastIndexOf('/')+1);
 
        if (name.length > 30) {
          docname=name.substring(0,10)+'...'+name.substring((name.length-10),name.length)
          // docname = element.document.substring(0, 10) + '...'+element.document.substring((element.document.length-12), element.document.length) ;
        }else{
          docname=name;
        }
        
        this.mediaResponse.push({ name: docname, doc: element.document })
      });
    }
    this.dismissLoader();
     },err=>{
      this.dismissLoader();
     })
  }
  back() {
    this.router.navigate(['/resident-profile', { id: this.consumer.id, flag: this.flag }], { replaceUrl: true })
  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getWing() {
    this.wingArray = [];
    //  this.wingArray.push({id:0,wing:'All facilities'});
    
      const bid=await this.storage.get("BRANCH")
      let headers=await this.config.getHeader();
        let branch = bid.toString();
        let body = { company_id: branch }
        let url = this.config.domain_url + 'branch_wings/' + bid;
        this.http.get(url,{headers}).subscribe((res: any) => {
        
          for (let i in res.data.details) {
          
            let obj = { id: res.data.details[i].id, wing: res.data.details[i].name };

            this.wingArray.push(obj);
            // if (res.data.details[i].id == this.consumer.wing_id) {


            //   this.wing = res.data.details[i].name;
            //   this.wingId = this.consumer.wing_id;
             
            // }

          }
          console.log("array:", this.wingArray);


        }, error => {
          console.log(error);

        })
     
  }
  // setWing(wing) {
  //   this.wingArray.forEach(element => {
  //     if (element.wing == wing) {
  //       this.wingId = element.id;
  //     }
  //   });
  //   console.log("wingId:", this.wingId);

  // }
  async getBranch() {
    
      const data=await this.storage.get("USER_TYPE")

        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")


            // this.user_type=data;
            let url = this.config.domain_url + 'branch_viatype';
            let body = {
              company_id: cid,
              role_id: data
            }
            if (data == '1') {
              let headers=await this.config.getHeader();;
              this.http.post(url, body,{headers}).subscribe((res: any) => {
                this.branch = res.data;
                for (let i in res.data) {
                  //  console.log("data:",res.data[i]);


                  if (res.data[i].name == this.consumer.user.user_details.user_branch.name) {


                    this.branch_id = res.data[i].id
                    console.log("wingg:", this.wing);
                  }

                }
                //  this.branch_id=this.branch[0].id;
                //  this.branch1=this.branch[0].name;
                // this.storage.get("BNAME").then(bname=>{
                //   if(bname){
                //   this.branch1=bname;
                //   }else{
                //     this.branch1=this.branch[0].name
                //   }
                // })

              })
            } else {
              let headers=await this.config.getHeader();;
              this.http.post(url, body, { headers }).subscribe((res: any) => {
                this.branch = res.data;
                for (let i in res.data) {



                  if (res.data[i].name == this.consumer.user.user_details.user_branch.name) {


                    this.branch_id = res.data[i].id
                    console.log("wingg:", this.wing);
                  }

                }
              })
            }
            //   if(this.user_type!=4){
            //   // const tabBar = document.getElementById('myTabBar');
            //   // tabBar.style.display="flex";
            //   }
         

  }

  setBranch(branch) {
    this.branch.forEach(element => {
      if (element.name == this.branch1) {
        this.branch_id = element.id;

      }
    });
    console.log("wingId:", this.branch_id);
  }
  changeType(t) {
    this.con_type = t;
  }
  async finish() {
    // var pattern = new RegExp('(https?:\\/\\/)?' + // protocol
    //   '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    //   '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    //   '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    //   '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    //   '(\\#[-a-z\\d_]*)?$', 'i');

    // var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i');
    
    

    if (this.phone == undefined || this.phone == '') {
      this.phone = null;
    }
    if (this.fname == undefined || this.lname == undefined || this.dob == undefined ||
      this.room == undefined || this.email == undefined || this.wingId == undefined || this.adm_date == undefined) {
      this.presentAlert('Please fill all mandatory fields.')
    } else if (this.fname == '' || this.lname == '' || this.dob == '' || this.room == '' ||
      this.email == '' || this.wingId == '' || this.adm_date == '') {
      this.presentAlert('Please fill all mandatory fields.')
    } else if (this.phone != null && (this.phone.toString().length < 10 || this.phone.toString().length > 10)) {
      this.presentAlert('Please enter a 10 digit mobile number.');
    } else if (!pattern.test(this.email)) {
      this.presentAlert('Please enter a valid email Id.')
    } else {
      this.showLoading();
      let ph;
      if (this.phone != null) {
        ph = '61' + this.phone.toString();
      } else {
        ph = null
      }

      if (this.images.length > 0) {
        this.images.forEach(element => {
          this.media.push(element)
        });
      }
      // if(this.videos.length>0){
      //   this.videos.forEach(element => {
      //     this.media.push(element);
      //   });
      // }
      if (this.docs.length > 0) {
        this.docs.forEach(element => {
          this.media.push(element)
        });
      }
      if (this.phone&&this.phone.includes('+61')) {
        this.phone = this.phone.slice(3);
      }
      let share;
      if (this.share) {
        share = 1;
      } else {
        share = 0;
        this.family = [];
      }
      let consent;
      if (this.consent) {
        consent = 1
      } else {
        consent = 0
      }
      let family;
      if (this.family.length == 0) {
        family = null
      } else {
        family = this.family
      }
      let headers=await this.config.getHeader();;
      let url = this.config.domain_url + 'consumer/' + this.user_id;
      let body = {
        fname: this.fname,
        lname: this.lname,
        dob: moment(this.dob).format('yyyy-MM-DD'),
        wing_id: this.wingId,
        room: this.room,
        status: this.status,
        description: this.description,
        likes: this.likes,
        dislikes: this.dislikes,
        interests: this.interest,
        email: this.email,
        admission_date:moment(this.adm_date).format('yyyy-MM-DD'),
        phone: ph,
        branch_id: this.branch_id,
        profile_pic: this.img,
        document: this.media,
        user_name: this.user_name,
        gender: this.gender,
        consumer_type: this.con_type,
        consent_to_tag: consent,
        share_details: share,
        family_ids: family

      }
      console.log(url, body);
      this.http.put(url, body,{headers}).subscribe((res: any) => {
        console.log("updated:", res);
       

        if (res.status == 'success') {
          this.dismissLoader();
          this.presentAlert('Updated successfully.')
          this.getDetails();
          // this.router.navigate(['/resident-profile', { id: this.consumer.id, flag: this.flag }], { replaceUrl: true })
        } else{
          this.dismissLoader();
          if(res.message=='This email address is already registered'){
              this.emailAlreadyRegistered();
          }else{
          this.presentAlert(res.message)
          }
         }

      },err=>{
        this.dismissLoader();
        this.presentAlert('Something went wrong. Please try again later.')
      })

      // })
      //   })
      // })
    }
  }
  changeGender(id) {
    this.gender = id
  }
  changeStatus(ev) {
    if (ev.currentTarget.checked == true) {
      this.status = 1;
    } else {
      this.status = 0;
    }
  }
  changeConsent(ev) {
    if (this.isLoading == true) {
      if (ev.currentTarget.checked == true) {
        this.consent = true;
      } else {
        this.consent = false;
      }
      console.log("consent:", ev.currentTarget.checked, this.consent);

    }
  }
  async pickImage(i) {
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: sourceType,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):

    //   this.crop.crop(imageData, this.cropOptions)
    //     .then(
    //       newPath => {
    //         let pic = newPath.split('?')[0]
    //         console.log("pic:", pic);
    //         let fileName = pic.substring(pic.lastIndexOf('/') + 1);
    //         let path = pic.substring(0, pic.lastIndexOf("/") + 1);
    //         //path=path.replace('Users/','');
    //         console.log("path:", path);
    //         console.log('name:', fileName);
          
    //         let that = this;
    //         this.file.resolveLocalFilesystemUrl(pic).then((entry: any) => {
    //           entry.file(function (file) {
    //             var reader = new FileReader();

    //             reader.onloadend = function (encodedFile: any) {
    //               var src = encodedFile.target.result;
    //               src = src.split("base64,");
    //               var contentAsBase64EncodedString = src[1];
    //               let base64 = 'data:image/jpeg;base64,' + contentAsBase64EncodedString;
    //               that.uploadProfilePic(base64);

    //             };
    //             reader.readAsDataURL(file);
    //           })
    //         }).catch((error) => {
    //           console.log(error);
    //         })

    //       },
    //       error => {
    //         console.log(error);
    //         // alert('Error cropping image' + error);
    //       }
    //     )

     





    // }, (err) => {
    //   // Handle error
    // });

    if(i==2){
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    // this.imageResponse.push(base64Image);
    // this.uploadImage(base64Image);
    this.uploadProfilePic(base64Image);
  }else{
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:1
      
    });
  
  
    for (var k = 0; k < image.photos.length; k++) {
            console.log('Image URI: ' + image.photos[k]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[k].path
            });
            
              // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              // this.uploadImage('data:image/jpeg;base64,' + contents.data)
              this.uploadProfilePic('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  }

  }
  async selectProfile() {
    if(this.edit){
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(1);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(2);
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  }
  async selectImage() {
   
    this.hasAttach = true;
    this.isImg = true;
    // this.isVdo = false;
    // this.isDoc = false;
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    // this.imagePicker.getPictures(options).then((results) => {
    //   for (var i = 0; i < results.length; i++) {
    //     // console.log('Image URI: ' + results[i]);

    //     if ((results[i] === 'O') || results[i] === 'K') {
    //       console.log("no img");

    //     } else {
    //       // this.img.push(results[i]);
    //       this.imageResponse.push('data:image/jpeg;base64,' + results[i])
    //       this.uploadImage('data:image/jpeg;base64,' + results[i]);
    //     }
    //   }
    // }, (err) => { });

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    
    


  
  }


  async uploadProfilePic(img) {

    this.showLoading();
    let url = this.config.domain_url + 'upload_file';
    let body = { file: img }
    console.log("body:", img);
    let headers=await this.config.getHeader();;
    this.http.post(url, body,{headers}).subscribe((res: any) => {
      console.log("pro:", res);

      // this.img.push(res.data);
      this.img = res.data;
      this.dismissLoader();
      // this.ionViewWillEnter();
      console.log("uploaded:", this.img);
    },error=>{
      this.dismissLoader();
    })
  }


 async uploadImage(img) {
    let url = this.config.domain_url + 'upload_file';
    let body = { file: img }
    console.log("body:", img);
    let headers=await this.config.getHeader();;
    this.http.post(url, body,{headers}).subscribe((res: any) => {

      // this.img.push(res.data);
      this.images.push(res.data);

      console.log("uploaded:", this.images);
    })
  }



  // selectVdo() {
  //   this.hasAttach = true;
  //   // this.isImg=false;
  //   this.isVdo = true;
  //   // this.isDoc=false;



  //   const options: CameraOptions = {
  //     quality: 100,
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     // encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.ALLMEDIA
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     // imageData is either a base64 encoded string or a file URI
  //     // If it's base64 (DATA_URL):
  //     if ((imageData).includes('.mp4') || (imageData).includes('.mkv') || (imageData).includes('.avi') || (imageData).includes('.mov')) {
  //       console.log('attach vdo');
  //       let fileUrl = 'file://' + imageData;

  //       // this.attach=fileUrl;
  //       this.file.resolveLocalFilesystemUrl(fileUrl).then(fileEntry => {

  //         fileEntry.getMetadata((metadata) => {
  //           console.log("sizee:", metadata.size);//metadata.size is the size in bytes
  //           if (metadata.size > this.MAX_FILE_SIZE) {
  //             this.alert('File is larger than 24MB');
  //             console.log('large size');

  //           }
  //           else {
  //             let ext;
  //             if ((imageData).includes('.mp4')) {
  //               ext = 'mp4'
  //             } else if ((imageData).includes('.mkv')) {
  //               ext = 'mkv'
  //             } else if ((imageData).includes('.mov')) {
  //               ext = 'mov'
  //             } else {
  //               ext = 'avi'
  //             }

  //             console.log("vdo:", imageData);

  //             // this.createThumbnail(fileUrl)
  //             this.uploadVdo(fileUrl, ext);
  //           }
  //         })

  //       })
  //       //   // 
  //     } else {
  //       console.log('not supported');

  //       this.alert('File format not supported');
  //     }
  //   }, (err) => {
  //     // Handle error
  //   });
  // }

  // uploadVdo(file, ext) {
  //   let fileName = file.substring(file.lastIndexOf('/') + 1);
  //   let name = fileName.split('.', 1)
  //   if (name.toString().length > 3) {
  //     fileName = name.toString().substring(0, 3) + '...' + ext;
  //   }
  //   this.vdoResponse.push({ name: fileName });
  //   let path = file.substring(0, file.lastIndexOf("/") + 1);
  //   //path=path.replace('Users/','');
  //   console.log("path:", path);
  //   console.log('name:', fileName);
  //   let that = this;
  //   this.file.resolveLocalFilesystemUrl(file).then((entry: any) => {
  //     entry.file(function (file) {
  //       var reader = new FileReader();

  //       reader.onloadend = function (encodedFile: any) {
  //         var src = encodedFile.target.result;
  //         src = src.split("base64,");
  //         var contentAsBase64EncodedString = src[1];
  //         that.base64File = contentAsBase64EncodedString;
  //         console.log("base:", contentAsBase64EncodedString);
  //         console.log("vdo:", that.base64File);
  //         let url = that.config.domain_url + 'upload_file';
  //         let body = { file: 'data:video/' + ext + ';base64,' + that.base64File }
  //         // console.log("body:",img);


  //         // let headers=await this.config.getHeader();
  //         that.http.post(url, body).subscribe((res: any) => {
  //           console.log(res);
  //           //  that.vdo.push(res.data);
  //           that.videos.push(res.data);
  //         }, error => {
  //           console.log(error);

  //         })

  //       };
  //       reader.readAsDataURL(file);
  //     })
  //   }).catch((error) => {
  //     console.log(error);
  //   })
  //   console.log("uploaded:", this.videos);
  // }

  selectDoc() {

    this.chooser.getFile()
      .then(file => {
        console.log(file ? file : 'canceled');
        if (file) {
          if (file.uri.includes('.pdf') || file.uri.includes('.doc')|| file.uri.includes('.docx')) {
            this.hasAttach = true;
            //   // this.isImg=false;
            //   // this.isVdo=false;
            this.isDoc = true;
            this.uploadDoc(file.dataURI, file.name);
          } else {
            this.alert('File format not supported');
          }
        }
      })
      .catch((error: any) => console.error(error));
    
  }

  // selectDoc(){
  // this.hasAttach=true;
  //   // this.isImg=false;
  //   // this.isVdo=false;
  //   this.isDoc=true;



  //   const options: CameraOptions = {
  //     quality: 100,
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     // encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.ALLMEDIA
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     // imageData is either a base64 encoded string or a file URI
  //     // If it's base64 (DATA_URL):
  //     if((imageData).includes('.pdf')||(imageData).includes('.doc')){
  //       console.log('attach vdo');
  //       let fileUrl='file://'+imageData;

  //       // this.attach=fileUrl;

  //             let ext;
  //             if((imageData).includes('.pdf')){
  //               ext='pdf'
  //             }else if((imageData).includes('.doc')){
  //               ext='doc'
  //             }

  //               console.log("vdo:",imageData);


  //             this.uploadDoc(fileUrl,ext);
  //            }else{
  //             console.log('not supported');

  //             this.alert('File format not supported');
  //           }



  //     //   // 

  //   }, (err) => {
  //     // Handle error
  //   });
  // }

  async uploadDoc(file, fname) {

    // let fileName;
    // let name = fname.split('.', 1)
   
    
    // if (name.toString().length > 30) {
    //   fileName = name.toString().substring(0, 10) + '...'+name.toString().substring((name.length-8), name.length) + fname.substring(fname.lastIndexOf('.') + 1);
    // }else{
    //   fileName=fname
    // }
    // this.docResponse.push({ name: fileName });
  
    let url = this.config.domain_url + 'upload_file';
    let body = { file: file }
    // console.log("body:",img);

    let headers=await this.config.getHeader();;
    // let headers=await this.config.getHeader();
    this.http.post(url, body,{headers}).subscribe((res: any) => {
      console.log("doc up:", res);
      this.docs.push(res.data);
      let docname;
        let name =res.data.substring(res.data.lastIndexOf('/')+1);
        console.log('docname:',name);
        if (name.length > 30) {
          docname=name.substring(0,10)+'...'+name.substring((name.length-10),name.length)
          // docname = element.document.substring(0, 10) + '...'+element.document.substring((element.document.length-12), element.document.length) ;
        }else{
          docname=name;
        }
        
        this.docResponse.push({name:docname})
    }, error => {
      console.log(error);

    })

    //      };
    //      reader.readAsDataURL(file);
    //    })
    //  }).catch((error)=>{
    //    console.log(error);
    //  })




    //   let url=this.config.domain_url+'upload_file';
    //   let body={file:doc}
    // console.log("body:",this.docs);

    //   this.http.post(url,body).subscribe((res:any)=>{
    //     console.log("uploaded:",res);
    //     this.doc.push(res.data);

    //   })
  }
  async alert(mes) {
    // let message;
    // if(type==2){
    //   message='File format not supported'
    // }
    const alert = await this.alertCntlr.create({
      mode:'ios',
      message: mes,
      backdropDismiss: true
    });

    await alert.present();
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }


  removeImg(i) {
    this.imageResponse.splice(i, 1);
    this.images.splice(i, 1);
    // this.media.splice(i,1);
  }

  removeVdo(i) {
    this.vdoResponse.splice(i, 1);
    this.videos.splice(i, 1);

  }
  removeDoc(i) {
    if(this.edit){
    this.docResponse.splice(i, 1);
    this.docs.splice(i, 1);
    }
  }

  removeMedia(i) {
    console.log("removing media:", i);

    this.media.splice(i, 1);
    this.mediaResponse.splice(i, 1);
  }

  openDoc(doc) {
    console.log("doc:", doc);
    let ext = doc.substr(doc.lastIndexOf('.') + 1);
    if (ext == 'png' || ext == 'PNG' || ext == 'jpg' || ext == 'JPG' || ext == 'jpeg' || ext == 'JPEG') {
      this.openImg(doc)
    } else {
      let options: InAppBrowserOptions = {
        location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar: 'yes',
        zoom: 'yes'
      }
      if(doc.includes('.pdf')){
        this.showpdf(doc)
      }else{
      const browser = this.iab.create(encodeURI('https://docs.google.com/viewer?url=' + doc + '&embedded=true'), '_blank', options);
      browser.on('loadstart').subscribe(() => {
        console.log('start');
        this.dialog.show();

      }, err => {
        console.log(err);

        this.dialog.hide();
      })

      browser.on('loadstop').subscribe(() => {
        console.log('stop');

        this.dialog.hide();;
      }, err => {
        this.dialog.hide();
      })

      browser.on('loaderror').subscribe(() => {
        this.dialog.hide();
      }, err => {
        this.dialog.hide();
      })

      browser.on('exit').subscribe(() => {
        this.dialog.hide();
      }, err => {
        this.dialog.hide();
      })
    }
    }
  }

  openList(i) {
    if (i == 1) {
      this.expanded1 = !this.expanded1;
      this.expanded2 = false;
      this.expanded3 = false;
      this.expanded4 = false;
      this.expanded5 = false;
    } else if (i == 2) {
      this.expanded1 = false;
      this.expanded2 = !this.expanded2;
      this.expanded3 = false;
      this.expanded4 = false;
      this.expanded5 = false;
    } else if (i == 3) {
      this.expanded1 = false;
      this.expanded2 = false;
      this.expanded3 = !this.expanded3;
      this.expanded4 = false;
      this.expanded5 = false;
    } else if (i == 4) {
      this.expanded1 = false;
      this.expanded2 = false;
      this.expanded3 = false
      this.expanded4 = !this.expanded4;
      this.expanded5 = false;
    } else if (i == 5) {
      this.expanded1 = false;
      this.expanded2 = false;
      this.expanded3 = false;
      this.expanded4 = false;
      this.expanded5 = !this.expanded5;
    }
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }

  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }


  async openImg(item) {
    console.log("openDoc");

    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
  selectFamily(family) {
    if (family.includes('0')) {
      this.family = [];

      this.familyArray.forEach(element => {
        if (this.family.includes(element.id.toString())) {

        } else {
          if ((element.id.toString()) != '0') {


            this.family.push(element.id.toString());
          }
        }

      });
    }
    console.log("selectedFam:", this.family);

  }
  shareDetails(ev) {
    if (this.isLoading == true) {
      if (ev.currentTarget.checked == true) {
        this.share = true;
      } else {
        this.share = false;
        this.family = [];
      }
    }
  }
  async showpdf(file){
    const modal = await this.modalCntl.create({
      component: ShowpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }

  async emailAlreadyRegistered(){
    const modal = await this.modalCntl.create({
      component: RegisteredEmailWarningComponent,
     
      componentProps: {
      
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }

  async getGender(){
    let url=this.config.domain_url+'get_gender';
  let headers=await this.config.getHeader();;
  console.log("dat:",url,headers)
   this.http.get(url,{headers}).subscribe((res:any)=>{

    console.log('gen:',res);
    this.genderArray=res;

    
   })
  }

  openSelector(selector) {
    selector.open().then((alert)=>{
      this.selectAllCheckBox = this.document.getElementById("selectAllFamilyCheckBox");
     this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
      this.listener = this.renderer.listen(this.selectAllCheckBox, 'click', () => {
          if (this.selectAllCheckBox.checked) {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="false") {
                (checkbox as HTMLButtonElement).click();
                this.family=[];
              };
            };
          } else {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="true") {
                (checkbox as HTMLButtonElement).click();
                this.branch.forEach(element => {
                  if(this.family.includes(element.id.toString())){
        
                  }else{
                    if((element.id.toString())!='0'){
        
                   
                  this.family.push(element.id.toString());
                    }
                  }
        
                });
              };
            };
          }
      });
      alert.onWillDismiss().then(()=>{
        this.listener();
       
      });
    })
    
  }
}
