import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerAboutPageRoutingModule } from './consumer-about-routing.module';

import { ConsumerAboutPage } from './consumer-about.page';
import { AboutInfoComponent } from '../components/about-info/about-info.component';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerAboutPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ConsumerAboutPage,AboutInfoComponent]
})
export class ConsumerAboutPageModule {}
