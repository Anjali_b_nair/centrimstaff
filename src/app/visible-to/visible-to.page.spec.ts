import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisibleToPage } from './visible-to.page';

describe('VisibleToPage', () => {
  let component: VisibleToPage;
  let fixture: ComponentFixture<VisibleToPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibleToPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisibleToPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
