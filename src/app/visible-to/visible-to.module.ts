import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisibleToPageRoutingModule } from './visible-to-routing.module';

import { VisibleToPage } from './visible-to.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisibleToPageRoutingModule
  ],
  declarations: [VisibleToPage]
})
export class VisibleToPageModule {}
