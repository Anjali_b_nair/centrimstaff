import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisibleToPage } from './visible-to.page';

const routes: Routes = [
  {
    path: '',
    component: VisibleToPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisibleToPageRoutingModule {}
