import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenancePageRoutingModule } from './maintenance-routing.module';

import { MaintenancePage } from './maintenance.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { AssignedTechniciansComponent } from '../components/assigned-technicians/assigned-technicians.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenancePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [MaintenancePage,AssignedTechniciansComponent],
  entryComponents:[
    AssignedTechniciansComponent
  ]
})
export class MaintenancePageModule {}
