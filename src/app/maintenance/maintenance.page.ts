import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { MaintenanceFilterComponent } from '../components/maintenance-filter/maintenance-filter.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { AssignedTechniciansComponent } from '../components/assigned-technicians/assigned-technicians.component';
@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.page.html',
  styleUrls: ['./maintenance.page.scss'],
})
export class MaintenancePage implements OnInit {
  request:any=[];
id:any;
filter:any=false;
sel_filter:any;
sel_id:any;
subscription:Subscription;
// tec:any=[];
branch:any=[];
bname:any;
sel_branch:any;
start:any;
end:any;

branchLoaded:boolean=false;

limit = 21;
  index: any = 0;
  terms:any;
  hideLoader:boolean=false;
  type: any[] = [];
  branchArray: any;
  utype:any;



  tec: any;
  status: any;
  risk: any;
  datefil: any;
  startDate: any;
  priority: any;
  endDate: any;
  crdatefil: any;
  crstartDate: any;
  crendDate: any;
  reqType: any;
  m_type: any;
  create:boolean=false;
  branch_name:any;
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform,private popoverCntlr:PopoverController,private route:ActivatedRoute,
    private loadingCtrl:LoadingController,private modalCntlr:ModalController) { }

  ngOnInit() {
  }


  // ------------ latest code ----------- //


  async ionViewWillEnter() {
    this.showLoading();
    // this.getBranch();
    this.request = [];
    this.limit = 21;
    this.index = 0;
    this.hideLoader=false;
    this.terms=undefined;
    this.filter = false;

    this.getRequests(false, "");
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.create')) {

        this.create = true
      } else {

        this.create = false;
        
      }
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/menu']);
    });
    
  }


  async getRequests(isFirstLoad, event) {
   this.branch_name=await this.storage.get('BNAME');
      const data=await this.storage.get('BRANCH')
        const uid=await this.storage.get('USER_ID')
          const type=await this.storage.get('USER_TYPE');
          const cid=await this.storage.get('COMPANY_ID')
            this.utype=type
            // let url=this.config.domain_url+'get_maintenance_requests/'+uid;
            let url = this.config.domain_url + 'maintenance_filter';
            let id = data.toString();
            // let headers = new HttpHeaders({ 'branch_id': id });
            console.log("head:", url);
            let body = {
              status: '0',
              offset: 0,   // always 0
              limit: this.limit,
              role_id: type,
              from_app:1,
              created_by:uid

            }
            let branch;
            // if (this.bname == undefined) {
              branch = data;
              
            // } else {
            //   branch = this.bname
            // }
            // let headers = new HttpHeaders({ 'branch_id': branch.toString()});
            let headers=await this.config.getHeader();
            this.http.post(url,body, {headers}).subscribe((res: any) => {
              console.log(res,branch);
              // this.request=res.data;
              for (let i = this.index; i < res.data.length; i++) {
                this.request.push(res.data[i]);
              }
              // this.request.sort(this.compare);
              this.dismissLoader();
              this.type = res.type;
              if (isFirstLoad)
                event.target.complete();

              this.limit = this.limit + 21;
              this.index = this.index + 21;
            }, error => {
              console.log(error);
              this.dismissLoader();
            })
         
  }


  doInfinite(event) {
    console.log('loading more open jobs')
    this.getRequests(true, event)
  }

  gotoDetails(id) {
    this.router.navigate(['/maintenance-details', { req_id: id }])
  }


  async search(){
    // this.filter=true;
    console.log("terms:",this.terms);
    
    if(this.terms==''||this.terms==null){
      this.ionViewWillEnter();
    }else{
      this.hideLoader=true;
   
      const data=await this.storage.get("USER_ID")

        const bid=await this.storage.get("BRANCH")


          const type=await this.storage.get('USER_TYPE')
           
            // let url;
            let url=this.config.domain_url+'maintenance_filter';
          
              let headers=await this.config.getHeader();
              // console.log("nead:",headers);
              let body={
              
                //  in case of technician login
                offset : 0,   // always 0
               search:this.terms,
               created_by:data
              }
              
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("requests:",res.data);
              this.type=res.type;
              // if(this.filter==3){
                this.request=res.data;
               
              //   // this.loader.dismissLoader();
              // }else{
              // for (let i = 0; i < res.data.length; i++) {
              //   this.request.push(res.data[i]);
              // }
              
              // this.request=res.data.data;
              
                // this.tec=res.technician;
                console.log(res);
                // this.loader.dismissLoader();
               
    
               
              // }
            },error=>{
              // this.loader.dismissLoader();
              console.log(error);
            })
          
       
  }
  }
  cancel(){
    // this.hide=false;
    this.terms=undefined;
    this.ionViewWillEnter();
  }
  async filterRequest(ev) {
    // open filter

    const popover = await this.modalCntlr.create({
      component: MaintenanceFilterComponent,
      // event: ev,
      backdropDismiss: true,
      cssClass: 'maintenance_filter full-width-modal',
      componentProps: {

        branch:this.bname,
        technician:this.tec,
        status: this.status,
        risk: this.risk,
        datefil: this.datefil,
        startDate: this.startDate,
        endDate: this.endDate,
        reqType: this.reqType,
        priority:this.priority,
        crdatefil: this.crdatefil,
        crstartDate : this.crstartDate,
        crendDate: this.crendDate,
        selected_type : this.m_type
        // filter:
      },

      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        // this.sel_filter = dataReturned.data.filter;
        // this.sel_id = dataReturned.data.id;
        // console.log("fil_data:", dataReturned);
        // //alert('Modal Sent Data :'+ dataReturned);
        this.tec = dataReturned.data.tec;
      this.status = dataReturned.data.status;
      this.risk = dataReturned.data.risk;
      this.datefil = dataReturned.data.date;
      this.startDate = dataReturned.data.start;
      this.endDate = dataReturned.data.end;
      this.reqType = dataReturned.data.reqType;
      this.priority = dataReturned.data.priority;
      this.crdatefil = dataReturned.data.crdatefil;
      this.crstartDate = dataReturned.data.crStart;
      this.crendDate = dataReturned.data.crEnd;
      this.m_type = dataReturned.data.type;

      this.limit = 21;
      this.index = 0;
      this.filterReq(false, "");
      this.filter = true;
      this.request=[];
        // this.filterContent();
      }
    });
    return await popover.present();
  }

  async filterReq(isFirstLoad, event) {

   
      // this.storage.get('BRANCH').then(data=>{
      const uid=await this.storage.get('USER_ID')
        const type=await this.storage.get('USER_TYPE')
          const bid=await this.storage.get("BRANCH")

            // let url;
            // if(type==4){

            this.utype = type;
            let current = new Date();
            let later;
            let to_date;
            let from_date;
            let cr_start;
            let cr_end;
            if (this.datefil == 0) {
              later = current
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
              from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
            } else if (this.datefil == 1) {
              later = new Date(Date.now() + 1 * 24 * 60 * 60 * 1000);
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
              from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
            } else if (this.datefil == 2) {
              later = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000);
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
              from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
            } else if (this.datefil == 3) {
              later = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000);
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
              from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
            } else if (this.datefil == 4) {

              from_date = moment(this.startDate).format('yyyy-MM-DD');
              to_date = moment(this.endDate).format('yyyy-MM-DD');
            } else if (this.datefil == 5) {
              from_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-01';
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-30';
            }
            else if (this.datefil == 6) {
              from_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 2)) + '-01';
              to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 2)) + '-30';
            }
            if (this.crdatefil == 0) {
              later = current
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
              cr_start = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
            } else if (this.crdatefil == 1) {
              later = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
              cr_start = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
            } else if (this.crdatefil == 2) {
              later = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
              cr_start = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
            } else if (this.crdatefil == 3) {
              later = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000);
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
              cr_start = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());
            } else if (this.crdatefil == 4) {
              cr_start = moment(this.crstartDate).format('yyyy-MM-DD');
              cr_end = moment(this.crendDate).format('yyyy-MM-DD');
            }
            else if (this.crdatefil == 5) {
              cr_start = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());;
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-30';
            }
            else if (this.crdatefil == 6) {
              cr_start = current.getFullYear() + '-' + this.fixDigit((current.getMonth())) + '-01';
              cr_end = current.getFullYear() + '-' + this.fixDigit((current.getMonth())) + '-30';
            }
            let assign;
            let branch;
            // if (this.bname == undefined || this.bname =='all') {
              branch = bid
            // } else {
            //   branch = this.bname
            // }
            let url = this.config.domain_url + 'maintenance_filter';

            let body;
            body = {
              role_id:type,
              offset: 0,   // always 0
              limit: this.limit,
              from_app:1,
              created_by:uid
            }
            if (this.status != 'all') {
              body.status = this.status
            }
            if (this.tec != 'all') {
              body.technician = this.tec;

            }
            if (this.risk != 'all') {
              body.risk_rating = this.risk
            }
            if (this.datefil != 'all') {
              body.due_sdate = from_date
            }
            if (this.datefil != 'all') {
              body.due_edate = to_date
            }
            if (this.crdatefil != 'all') {
              body.sdate = cr_start
            }
            if (this.crdatefil != 'all') {
              body.edate = cr_end
            }
            if (this.priority != 'all') {
              body.priority = this.priority
            }
            if (this.reqType != 'all') {
              body.category = this.reqType
            }
            if (this.m_type != 'all') {
              body.maintenance_type = this.m_type;
            }
            let id;
                if (this.bname == 'all' ||this.bname==undefined) {
                  id=bid
                } else {
                  body.branch = branch;
                  id=branch
                }
            
            // let headers = new HttpHeaders({ 'branch_id': id });
            let headers=await this.config.getHeader();
            console.log("head:", url, body, this.datefil);

            this.http.post(url, body, { headers }).subscribe((res: any) => {
              console.log("resFil:", res);
              this.dismissLoader();
              // for (let i = 0; i < res.data.data.length; i++) {
              // this.request=res.data;
              // }
              // this.request=res.data.data;
              let data = [];
              //   // this.loader.dismissLoader();
              // for(let i in res.data){
              //   data.push(res.data[i])
              // }
              for (let i = this.index; i < res.data.length; i++) {
                this.request.push(res.data[i]);
              }

              // this.loader.dismissLoader();
              if (isFirstLoad)
                event.target.complete();

              this.limit = this.limit + 21;
              this.index = this.index + 21;
            }, error => {
              this.dismissLoader();
              console.log(error);

            })
            //   }else{


            //       let current=new Date();
            //       let later;
            //       let to_date;
            //       let from_date;
            //       if(this.datefil==0){
            //         later=current
            //         to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
            //         from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
            //       }else if(this.datefil==1){
            //         later=new Date(Date.now()-1*24*60*60*1000);
            //         to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
            //         from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
            //       }else if(this.datefil==2){
            //         later=new Date(Date.now()-7*24*60*60*1000);
            //         to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
            //         from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
            //       }else if(this.datefil==3){
            //         later=new Date(Date.now()-30*24*60*60*1000);
            //         to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
            //         from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
            //       }else if(this.datefil==4){
            //         from_date=moment(this.startDate).format('yyyy-MM-DD');
            //         to_date=moment(this.endDate).format('yyyy-MM-DD');
            //       }


            //       url=this.config.domain_url+'maintenance_filter';

            //     // let id=data.toString();
            //     // let headers=await this.config.getHeader();;
            //     console.log("head:",url);

            //     this.http.get(url,{headers}).subscribe((res:any)=>{

            //       for (let i = 0; i < res.data.data.length; i++) {
            //         this.request.push(res.data.data[i]);
            //       }
            //       // this.request=res.data.data;

            //         this.tec=res.technician;

            //       this.loader.dismissLoader();
            //       if (isFirstLoad)
            //   event.target.complete();

            // this.page++;
            //     },error=>{
            //       this.loader.dismissLoader();
            //       console.log(error);

            //     })
            //   }
         
  }



  async filterContent() {

    
      const data=await this.storage.get('BRANCH')
        const uid=await this.storage.get('USER_ID')

          let url;
          console.log("filtering");
          if (this.sel_filter == "All") {
            this.ionViewWillEnter();
          } else if (this.sel_filter == "Status") {
            url = this.config.domain_url + 'get_maintenance_requests?status=' + this.sel_id;
          } else if (this.sel_filter == "Date") {
            let current = new Date();
            let later;
            if (this.sel_id == 0) {
              later = current
            } else if (this.sel_id == 1) {
              later = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);
            } else if (this.sel_id == 2) {
              later = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
            } else if (this.sel_id == 3) {
              later = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000);
            }
            let to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
            let from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());

            url = this.config.domain_url + 'get_maintenance_requests?sdate=' + from_date + '&edate=' + to_date;

          } else if (this.sel_filter == "Priority") {
            url = this.config.domain_url + 'filter_by_priority?type=' + this.sel_id;
          }

          let id = data.toString();
          // let headers = new HttpHeaders({ 'branch_id': id });
          let headers=await this.config.getHeader();
          console.log("head:", url);

          this.http.get(url, { headers }).subscribe((res: any) => {
            console.log(res);
            this.request = res.data;
          }, error => {
            console.log(error);

          })

      

  }

  doInfiniteFilter(event) {
    this.filterReq(true, event)
  }
  fixDigit(val) {
    return val.toString().length === 1 ? "0" + val : val;
  }
  async createReq() {
    const bid=await this.storage.get('BRANCH')
    this.router.navigate(['/create-maintenance', { type: JSON.stringify(this.type),branch:bid }])
  }


  async getBranch() {
   
      const data=await this.storage.get("USER_TYPE")

        const cid=await this.storage.get("COMPANY_ID")
        let headers=await this.config.getHeader();
          const bid=await this.storage.get('BRANCH')
           
              this.branchArray = [];

              // this.user_type=data;


              console.log("role:", data);

              if (data == '1') {
                console.log("role1");
                let url = this.config.domain_url + 'branch_viatype';
                let body = {
                  company_id: cid,
                  role_id: data
                }
                this.http.post(url, body,{headers}).subscribe((res: any) => {

                  
                  res.data.forEach(element => {
                    this.branchArray.push({ id: element.id, name: element.name })
                  });
                  this.bname=bid.toString();
                  //  this.branch_id=this.branch[0].id;
                  //  this.branch1=this.branch[0].name;
                  console.log('adminBranch:',this.branchArray);
                })
              // } else {
              //   let url = this.config.domain_url + 'technician_branches';
              //   let body = {
              //     user_id: uid,

              //   }
              //   let id = bid.toString();
              //   let headers = new HttpHeaders({ 'branch_id': id });
              //   console.log("branch_id:", bid, { headers });
              //   console.log(url, body);

              //   this.http.post(url, body).subscribe((res: any) => {
              //     // this.branch=res.data;
              //     console.log('br:', res);

              //     // let ele=res[0];
              //     for (let i in res) {
              //       this.branchArray.push({ id: res[i].branches.id, name: res[i].branches.name })
              //     }
              //       // if(this.branchArray.length>1){
              //       //   this.branch='all'
              //       // }
              //   })
              }
          
          
       
  }


  changeStatus(i){
      console.log("item:",i);
      this.bname=i.toString();
      // if(i==0){
        this.request = [];
        this.limit = 21;
        this.index = 0;
       
        this.getRequests(false, "");
        // this.sel_branch=this.sel_branch;
      // }else{
      //   this.sel_branch=i;
      // let url=this.config.domain_url+'get_maintenance_request_technician?branch='+i;
      // this.http.get(url).subscribe((res:any)=>{
      //   console.log(res);
      //   this.request=res.data;
      // },error=>{
      //   console.log(error);
        
      // })
    // }
    }

  // ------------ latest code ----------- //


// --------------------- code before changes --------------------------- //

  // ionViewWillEnter(){
  //   this.showLoading();
  //   this.filter=this.route.snapshot.paramMap.get('filter');
  //   if(this.filter!=1 || this.filter!='null'){
  //   this.id=this.route.snapshot.paramMap.get('data');
  //   }
  //   this.request=[];
  //   this.branch=[];
    
  //   this.storage.ready().then(()=>{
  //     const data=await this.storage.get("USER_ID")

  //       const bid=await this.storage.get("BRANCH")


  
  //       this.sel_branch=bid;
  //       this.bname=bid;
  //       this.getBranch();
  //       // this.branchLoaded=true;
  //       let url;
  //       if(this.filter==1){
  //       url=this.config.domain_url+'get_maintenance_request_technician?user_id='+data;
  //       }else if(this.filter==2){
  //         url=this.config.domain_url+'get_maintenance_request_technician?status='+this.id+'&branch='+bid+'&user_id='+data;
  //       }else if(this.filter==3){
  //         url=this.config.domain_url+'filter_by_priority?priority='+this.id+'&branch='+bid+'&user_id='+data;
  //       }
  //       let id=data.toString();
  //       // let headers=await this.config.getHeader();
  //       console.log("head:",url);
        
  //       this.http.get(url).subscribe((res:any)=>{
          
  //         this.request=res.data;
          
  //           this.tec=res.technician;
  //           console.log(res);
  //         this.dismissLoader();
  //       },error=>{
  //         this.dismissLoader();
  //         console.log(error);
  //       })
  //       })
  //     })
  //   })
  //   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
  //     this.router.navigate(['/maintenance-count']) ;
  //   }); 
  // }
  // gotoDetails(id){
  //   this.router.navigate(['/maintenance-details',{req_id:id,filter:this.filter}])
  // }
  
  
  // async filterRequest(ev){
  // // open filter
   
  //   const popover = await this.popoverCntlr.create({
  //     component: MaintenanceFilterComponent,
  //     event: ev,
  //     backdropDismiss:true,
  //     cssClass:'maintenance_filter',
  //     componentProps:{
  //       data:this.tec,
  //       flag:1
  //     },
      
  //     // translucent: true
  //   });
  //   popover.onDidDismiss().then((dataReturned) => {
  //     if (dataReturned !== null) {
  //       this.sel_filter = dataReturned.data.filter;
  //       this.sel_id = dataReturned.data.id;
  //       this.start = dataReturned.data.start;
  //       this.end = dataReturned.data.end;
  //       console.log("fil_data:",dataReturned);
  //       //alert('Modal Sent Data :'+ dataReturned);
  //       this.filterContent();
  //     }
  //   });
  //   return await popover.present();
  // }
  // filterContent(){
  //   this.showLoading();
  //   this.storage.ready().then(()=>{
  //     // this.storage.get('BRANCH').then(data=>{
  //       const uid=await this.storage.get("USER_ID")

  
        
  //       let url;
  //       console.log("filtering",this.sel_filter,this.sel_id);
  //        if(this.sel_filter=="Status"){
  //         url=this.config.domain_url+'get_maintenance_request_technician?status='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
  //       }else if(this.sel_filter=="Risk rating"){
  //         url=this.config.domain_url+'get_maintenance_request_technician?risk_rating='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
  //       }else if(this.sel_filter=="Date"){
  //         let current=new Date();
  //         let later;
  //         let to_date;
  //         let from_date;
  //         if(this.sel_id==0){
  //           later=current
  //           to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
  //           from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
  //         }else if(this.sel_id==1){
  //           later=new Date(Date.now()-1*24*60*60*1000);
  //           to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
  //           from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
  //         }else if(this.sel_id==2){
  //           later=new Date(Date.now()-7*24*60*60*1000);
  //           to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
  //           from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
  //         }else if(this.sel_id==3){
  //           later=new Date(Date.now()-30*24*60*60*1000);
  //           to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
  //           from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());
  //         }else if(this.sel_id==4){
  //           from_date=moment(this.start).format('yyyy-MM-DD');
  //           to_date=moment(this.end).format('yyyy-MM-DD');
  //         }
          
  
  //         url=this.config.domain_url+'get_maintenance_request_technician?sdate='+from_date+'&edate='+to_date+'&branch='+this.sel_branch+'&user_id='+uid;;
  
  //       }else if(this.sel_filter=="Technician"){
  //         url=this.config.domain_url+'get_maintenance_request_technician?technician='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
  //       }else if(this.sel_filter=="Branch"){
  //         url=this.config.domain_url+'get_maintenance_request_technician?branch='+this.sel_id;
  //       }
  
  //       // let id=data.toString();
  //       // let headers=await this.config.getHeader();
  //       console.log("head:",url);
        
  //       this.http.get(url).subscribe((res:any)=>{
          
  //         this.request=res.data;
  //         console.log(res);
  //         this.dismissLoader();
  //       },error=>{
  //         this.dismissLoader();
  //         console.log(error);
          
  //       })
  
  //     })
  //     // })
  //   })
    
  // }
  // fixDigit(val){
  //   return val.toString().length === 1 ? "0" + val : val;
  // }
  
  
  
  // getBranch(){
  //   this.branch.push({id:0,status:'All'});
  //   this.storage.ready().then(()=>{
  //     // const data=await this.storage.get("COMPANY_ID")

  //       const data=await this.storage.get("USER_ID")

  //       // let url=this.config.domain_url+'branch_viatype';
  //       let url=this.config.domain_url+'technician_branches'
  //       let body={user_id:data}
  //       // let body={company_id:data,
  //       //           role_id:4}
  //       let headers=await this.config.getHeader();
  //       this.http.post(url,body).subscribe((res:any)=>{
  //         console.log("res:",res);
  //         res.forEach(element=>{
  //           let branch={id:element.branch_id,status:element.branches.name};
  //           if(element.branch_id==this.bname){
  //             this.bname=element.branch_id;
  //           }
  //           this.branch.push(branch);
  //         })
          
  //       })
  //     })
  //   })
  // }
  // changeStatus(i){
  //   console.log("item:",i);
  //   this.bname=i.toString();
  //   if(i==0){
  //     this.ionViewWillEnter();
  //     // this.sel_branch=this.sel_branch;
  //   }else{
  //     this.sel_branch=i;
  //   let url=this.config.domain_url+'get_maintenance_request_technician?branch='+i;
  //   this.http.get(url).subscribe((res:any)=>{
  //     console.log(res);
  //     this.request=res.data;
  //   },error=>{
  //     console.log(error);
      
  //   })
  // }
  // }

  // --------------------------- code before changes --------------------- //

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  compare(i,j){
    let c=i.created_at;
    let d= j.created_at;
    let a=new Date(d).getTime() - new Date(c).getTime();
    return a;
  }
  async assigned(item){
    // let data=item.
    const popover = await this.modalCntlr.create({
      component:AssignedTechniciansComponent,
      cssClass:'cbmorepop',
    backdropDismiss:true,
      componentProps:{
        data:item,
       

      },
      
      
      
    });
    
   
   
      
  
    return await popover.present();
  }
}
