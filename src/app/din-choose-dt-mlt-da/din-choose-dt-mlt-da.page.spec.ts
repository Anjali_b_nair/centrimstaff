import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseDtMltDaPage } from './din-choose-dt-mlt-da.page';

describe('DinChooseDtMltDaPage', () => {
  let component: DinChooseDtMltDaPage;
  let fixture: ComponentFixture<DinChooseDtMltDaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseDtMltDaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseDtMltDaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
