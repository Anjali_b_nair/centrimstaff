import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseDtMltDaPageRoutingModule } from './din-choose-dt-mlt-da-routing.module';

import { DinChooseDtMltDaPage } from './din-choose-dt-mlt-da.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseDtMltDaPageRoutingModule
  ],
  declarations: [DinChooseDtMltDaPage]
})
export class DinChooseDtMltDaPageModule {}
