import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
@Component({
  selector: 'app-din-choose-dt-mlt-da',
  templateUrl: './din-choose-dt-mlt-da.page.html',
  styleUrls: ['./din-choose-dt-mlt-da.page.scss'],
})
export class DinChooseDtMltDaPage implements OnInit {

  subscription:Subscription;
  serving_area:any[]=[];
  area:any;
  serving_time:any[]=[];
  ser_time:any;
  date:any;
  minDate:any;
  servingtime:any;
  serv_area:any;
  constructor(private router:Router,private platform:Platform,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.date=new Date();
  this.minDate=new Date().toISOString();
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
 
  let headers=await this.config.getHeader();
  
  let url1=this.config.domain_url+'get_servingtime';
  
  
  this.http.get(url1,{headers}).subscribe((res:any)=>{
    this.serving_time=res.data;
    // this.servingtime=this.serving_time[0].servingtime;
    //     this.ser_time=this.serving_time[0].id;
    let closest = null;
    let remaining=[];
    this.serving_time.forEach(ele=>{
      let date=moment(this.date).format('DD MMM,YYYY')+' '+ele.end_time
      if(moment(date).isAfter(moment())){
        remaining.push(ele)
      }
    })
    remaining.reduce((acc, obj, i) => {
      let diff = Math.abs(this.timeToSecs(obj.start_time) - this.timeToSecs(moment(this.date).format('HH:mm:ss')));
      if (diff < acc) {
        acc = diff;
        closest = obj;
      }
      return acc;
    }, Number.POSITIVE_INFINITY);

    this.servingtime=closest.servingtime;
        this.ser_time=closest.id;
    console.log('res:',res)
  })
  
  let url=this.config.domain_url+'get_all_company_dining_serving_area';
  
  
  this.http.get(url,{headers}).subscribe((res:any)=>{
    
    console.log('res:',res);
    this.serving_area=res.data;
    this.area=this.serving_area[0].id;
        this.serv_area=this.serving_area[0].serving_area
  })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    
      back(){
        this.router.navigate(['/dining-dashboard'],{replaceUrl:true})
      }

      view(){
        if(!this.ser_time){
          this.presentAlert('Please choose a serving time.');
        }else if(!this.area){
          this.presentAlert('Please choose the serving area.');
        }else{
        this.router.navigate(['/din-dining-table',{area_id:this.area,area:this.serv_area,ser_time_id:this.ser_time,ser_time:this.servingtime,date:this.date}],{replaceUrl:true});
        }
      }
      selectArea(item){
        this.area=item.id;
        this.serv_area=item.serving_area
      }

      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass:'toastStyle',
          duration: 3000,
          position:'top'      
        });
        alert.present(); //update
      }
      setServetime(item){
        this.servingtime=item.servingtime;
        this.ser_time=item.id;
      }

      timeToSecs(time) {
        let [h, m, s] = time.split(':');
        return h*3.6e3 + m*60 + s*1;
      }
}
