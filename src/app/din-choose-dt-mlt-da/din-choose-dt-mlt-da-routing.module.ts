import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseDtMltDaPage } from './din-choose-dt-mlt-da.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseDtMltDaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseDtMltDaPageRoutingModule {}
