import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeclinedReasonPage } from './declined-reason.page';

describe('DeclinedReasonPage', () => {
  let component: DeclinedReasonPage;
  let fixture: ComponentFixture<DeclinedReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeclinedReasonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeclinedReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
