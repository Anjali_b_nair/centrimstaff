import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeclinedReasonPage } from './declined-reason.page';

const routes: Routes = [
  {
    path: '',
    component: DeclinedReasonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeclinedReasonPageRoutingModule {}
