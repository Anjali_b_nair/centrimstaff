import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeclinedReasonPageRoutingModule } from './declined-reason-routing.module';

import { DeclinedReasonPage } from './declined-reason.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeclinedReasonPageRoutingModule
  ],
  declarations: [DeclinedReasonPage]
})
export class DeclinedReasonPageModule {}
