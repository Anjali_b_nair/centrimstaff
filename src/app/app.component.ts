import { Component, NgZone } from '@angular/core';

import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { Network } from '@ionic-native/network/ngx';
import { NonetworkComponent } from './components/nonetwork/nonetwork.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from './services/http-config.service';
import { Market } from '@ionic-native/market/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { Location } from '@angular/common';
import { filter, map, mergeMap } from 'rxjs/operators';
// import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { PushNotifications } from '@capacitor/push-notifications';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  notification:any={};
  msgcount:any;
  currentPage:any;
  act_view: boolean = false;
  story_view: boolean = false;
  user_view: boolean = false;
  dining_view: boolean = false;
  modules:any[]=[];
  constructor(
    private platform: Platform,
   
    private statusBar: StatusBar,
    private storage:Storage,
    private router:Router,
    private network:Network,
    private modalCntrl:ModalController,
    private nav:NavController,
    private http:HttpClient,
    private config:HttpConfigService,
    private alertController:AlertController,
    private market:Market,
    private version:AppVersion,
  
    private loc:Location,
    private route:ActivatedRoute,
    private toastCntl:ToastController,
    // private deeplinks:Deeplinks,
    private zone: NgZone
  ) {
  
    
  

    this.initializeApp();
    
  }

  async ngOnInit(){
    const data=await this.storage.get("USER_ID");
    let headers=await this.config.getHeader();
    this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                map(() => this.route),
                map((route) => {
                  
                    while (route.firstChild) {
                        route = route.firstChild;
                        
                        
                    }
                    
                    // this.currentPage=route.routeConfig.component.name;
                    this.currentPage=this.loc.path();
                    
                    if(this.currentPage=='/message' ){
                      
                        
                      let m_url=this.config.domain_url+'msgcount';
                      
                      let body={
                        user_one:data
                      }
                      this.http.post(m_url,body,{headers}).subscribe((mes:any)=>{
                        
                        this.msgcount=mes.data.msgcount;
                
                     
                        
                
                      })
                    
                    }
                    return route;
                }),
                mergeMap((route) => route.data)
            )
            .subscribe(
                (routeData) => {
                   
                }
            );
  }


  async initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      SplashScreen.hide();
      this.initDeeplinks();
    
      // this.statusBar.overlaysWebView(true);
      // this.statusBar.backgroundColorByHexString('#00ffb3')
      await this.storage.create();
      
        const res=await this.storage.get("loggedIn");
        this.storage.set('USERLOG',1);
          if(res==0||res==null){
            this.router.navigateByUrl('/login');
          }
          else if(res==1){
            const id=await this.storage.get('USER_TYPE')
            this.router.navigateByUrl('/menu');

            const cid=await this.storage.get("COMPANY_ID");
            let headers=await this.config.getHeader();
            let murl = this.config.domain_url + 'get_modules/' + cid;
          this.http.get(murl,{headers}).subscribe((mod: any) => {
            
            this.modules = mod
            
          });
            (await this.config.getUserPermission()).subscribe((res: any) => {
              
              let routes = res.user_routes;
              
             
        
              // activity permissions
              if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {
        
                this.act_view = true
              } else {
        
                this.act_view = false;
              }
        
              if (res.main_permissions.life_style==1&&routes.includes('story.index')) {
        
                this.story_view = true
              } else {
        
                this.story_view = false;
              }
              
              // if (res.main_permissions.residents==1&&routes.includes('residents.index')) {
        
              //   this.user_view = true
              // } else {
        
              //   this.user_view = false;
              // }
              if (res.main_permissions.finance==1&&routes.includes('finance.index')) {
      
                this.user_view= true
              } else {
        
                this.user_view = false;
                
              }
              if (res.main_permissions.dining==1) {
        
                this.dining_view = true
              } else {
        
                this.dining_view = false;
              }
             
            })



            // if(this.platform.is('ios')||this.platform.is('ipad')||this.platform.is('iphone')){
            //   this.firebase.hasPermission().then((data)=>{
            //     if(!data)
            //     this.firebase.grantPermission().then(()=>{
                
            //     })
            //   });
              
                
            //   }

              

            // this.firebase.onMessageReceived().subscribe((data) => {
            //   // this.fcm.getInitialPushPayload().then((data)=>{
            
            //   this.notification=data;
            //   if(data){
            //   if(data.tap=='background'){
           
            //       if(data.not_type==1 ||data.not_type==2 ||data.not_type==3 || data.not_type==5 || data.not_type==14){
            //         // this.router.navigate(['/story-details',{post_id:data.link_id}])
            //         this.nav.navigateRoot(['/story-details',{post_id:data.link_id,flag:2}]);
            //       }
            //       else if(data.not_type==4 ||data.not_type==6){
            //         // this.router.navigate(['/activity-details',{act_id:data.link_id}]);
            //         this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
            //       }
            //       else if(data.not_type==7){
            //         // this.router.navigate(['/activity']);
            //         this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
            //       }
            //       else if(data.not_type==8){
            //         // this.router.navigate(['/message']);
            //         this.nav.navigateRoot(['/message']);
            //       }
            //       else if(data.not_type==9){
            //         // this.router.navigate(['/message']);
            //         this.nav.navigateRoot(['/callbooking-list']);
            //       }else if(data.not_type==21){
            //         // this.router.navigate(['/message']);
            //         this.nav.navigateRoot(['/visit-booking-list']);
            //       }else if(data.not_type==12||data.not_type==13){
            //         this.nav.navigateRoot(['/maintenance-details',{filter:0,req_id:data.link_id}]);
            //       }
            //       // else if(data.not_type==11){
            //       //   this.nav.navigateRoot(['/newsletter'])                     
            //       // }
            //       // else if(data.not_type==12){
                    
            //       //   if(id==4){
            //       //     this.nav.navigateRoot(['/maintenance-details',{filter:0,req_id:data.link_id}]);
            //       //   }else{
            //       //     this.nav.navigateRoot(['/consumer-maintenance-details',{req_id:data.link_id}])
            //       //   }
                    
            //       // }
            //     }
            //   }
              
           
            // });
  
            let permStatus = await PushNotifications.checkPermissions();
      
        if (permStatus.receive === 'prompt') {
          permStatus = await PushNotifications.requestPermissions();
        }
      
        if (permStatus.receive !== 'granted') {
          throw new Error('User denied permissions!');
        }
      
        await PushNotifications.register();
      // }

      // const addListeners = async () => {
        await PushNotifications.addListener('registration', token => {
        
          this.storage.set("uuid",token.value);
        });

        await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
         
          const data=notification.notification.data;

          if(data.not_type==1 ||data.not_type==2 ||data.not_type==3 || data.not_type==5 || data.not_type==14){
                   
                    this.nav.navigateRoot(['/story-details',{post_id:data.link_id,flag:2}]);
                  }
                  else if(data.not_type==4 ||data.not_type==6){
                    
                    this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
                  }
                  else if(data.not_type==7){
                   
                    this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
                  }
                  else if(data.not_type==8){
                    
                    this.nav.navigateRoot(['/message']);
                  }
                  else if(data.not_type==9){
                  
                    this.nav.navigateRoot(['/callbooking-list']);
                  }else if(data.not_type==21){
                   
                    this.nav.navigateRoot(['/visit-booking-list']);
                  }else if(data.not_type==12||data.type==13){
                    this.nav.navigateRoot(['/maintenance-details',{filter:0,req_id:data.link_id}]);
                  }
                });
          }
       
      this.forceUpdate();

     
        const data=await this.storage.get("USER_ID")
      let m_url=this.config.domain_url+'msgcount';
      let headers=await this.config.getHeader();
      let body={
        user_one:data
      }
      this.http.post(m_url,body,{headers}).subscribe((mes:any)=>{
        
        this.msgcount=mes.data.msgcount;

       
        mes.user.forEach(element => {
          this.storage.set("PRO_IMG",element.profile_pic)
         
        });

      })
    


      // this.firebase.getToken().then(token => {
      //  this.storage.set("uuid",token);
     
      // });
      
  
    
      

    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
     
      this.openModal();
    });
  })
  }


  async openModal() {
    const modal = await this.modalCntrl.create({
      component: NonetworkComponent,
      componentProps: {
        
        
      }
    });
    return await modal.present();
  }

async forceUpdate(){

    const cid=await this.storage.get("COMPANY_ID")
      this.version.getVersionNumber().then(value=>{
        let url=this.config.domain_url+'all_force_update';
        // let headers=await this.config.getHeader();
        let body;
        let id;
        let b;
        if(this.platform.is('android')){
          id='com.ecaret.centrimstaff';
          b=1;
          body={
            company_id:cid,
            staff_app:value,
            android:1
          }
        }else{
          id='1547210367';
          b=2;
          body={
            company_id:cid,
            staff_app:value,
            ios:1
          }
        }
        this.http.post(url,body).subscribe((res:any)=>{
              // console.log("force:",res,body,value);
              if(res.data.staff_app_fu==0){
                this.showForceUpdateAlert(id,b);
              }

        })
      })
   
}




 
  
  async showForceUpdateAlert(appId,b){
    let buttonText;
    if(b==1){
      buttonText='GO TO PLAYSTORE'
    }else{
      buttonText='GO TO APPSTORE'
    }
    const alert = await this.alertController.create({
      mode:'ios',
      header: 'Update Now!',
      cssClass: 'updateAlert',
      message: '<p>It seems you are using an old version of the application and there have been a few updates since.</p><p>It is recommended you update as soon as you can to benefit from all of the new features, improvements and bug fixes!</p>',
      buttons: [
        {
          text: buttonText,
          
          // cssClass: 'secondary',
          handler: () => {
            this.market.open(appId);
            navigator['app'].exitApp();
          }
        }
      ],
      backdropDismiss:false
    });
  
    await alert.present();
  }
  async showUpdateAlert(appId){
    const alert = await this.alertController.create({
      mode:'ios',
      header: 'New version available',
      message: 'There is a newer version available for download! Please update the app to continue. ',
      buttons: [
        {
          text: 'CANCEL',
          
          // cssClass: 'secondary',
          handler: () => {
            this.alertController.dismiss();
          }
        },
        {
          text: 'UPDATE',
          
          // cssClass: 'secondary',
          handler: () => {
            this.market.open(appId);
            navigator['app'].exitApp();
          }
        }
      ],
      backdropDismiss:true
    });
  
    await alert.present();
  }
  async initDeeplinks() {
    
  //   const res=await this.storage.get("loggedIn");
  //   this.deeplinks.route({ '/maintenance-details/': 'maintenance',
  // '/story/':'stories' }).subscribe(
  //   match => {
 
  //   const path = `/${match.$route}/${match.$args['slug']}`;
  
    
  //         if(res==0){
  //           this.router.navigateByUrl('/');
  //         }
  //         else{
  //   if(match.$link.path.includes('maintenance-details')){
  //     this.nav.navigateRoot(['/maintenance']);
  //   }
  //   if(match.$link.path.includes('story')){
  //     this.nav.navigateRoot(['/stories']);
  //   }
  //   // Run the navigation in the Angular zone
  //   // this.zone.run(() => {
  //   // this.router.navigateByUrl(path);
  //   // });
  // }
  //   },
  //   nomatch => {
  
    
  //         if(res==0){
  //           this.router.navigateByUrl('/');
  //         }
  //         else{
  //   if(nomatch.$link.path.includes('maintenance-details')){
  //     this.nav.navigateRoot(['/maintenance']);
  //   }
  //   if(nomatch.$link.path.includes('story')){
  //     this.nav.navigateRoot(['/stories']);
  //   }
  // }
    
  //   });

    
    
    }

  gotoFeeds(){
    if(this.modules.includes('Story')&&this.story_view){
    this.router.navigate(['/feeds'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoActivity(){
    if(this.modules.includes('Activity')&&this.act_view){
    this.router.navigate(['/activities'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoDining(){
   
    if(this.modules.includes('Dining Beta')&&this.dining_view){
    this.router.navigate(['/dining-dashboard'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoResidents(){
    if(this.user_view){
    this.router.navigate(['/consumers'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  async presentAlert(mes){
    const alert = await this.toastCntl.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present();
  }
}
