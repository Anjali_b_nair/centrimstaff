import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { RvActivityDetailsComponent } from '../components/rv-activity-details/rv-activity-details.component';
import { RvActivityFilterComponent } from '../components/rv-activity-filter/rv-activity-filter.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-service-activities',
  templateUrl: './rv-service-activities.page.html',
  styleUrls: ['./rv-service-activities.page.scss'],
})
export class RvServiceActivitiesPage implements OnInit {
  cid:any;
  id:any;
  sdate:any;
  edate:any;
  offset:any=0;
  terms: any;
  activity:any=[];
 
  subscription:Subscription;
  count:any;

  status:any='0';
  category:any='0';
  date_filter:any='6';
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.activity=[];
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.edate=moment().format('YYYY-MM-DD HH:mm:ss');
    this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD HH:mm:ss');

    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'load_resident_activity';
    let headers=await this.config.getHeader();
    let body;
    body={
      user_id:this.id,
      start:this.sdate,
      end:this.edate,
      take:20,
      skip:this.offset
    }
    

    console.log(body,{headers});
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
     this.count={
      total:res.data.total_count,
      attended:res.data.attended_count,
      declined:res.data.declined_count,
      upcoming:res.data.upcoming_count
    };
     
      
     },error=>{
       console.log(error);
      
     })
    // this.getActivityList(false,'');
    this.getUpcoming(false,'');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  }); 
  
  }
  back(){
    this.router.navigate(['/rv-services-home',{id:this.id,cid:this.cid}])
  }

  async getActivityList(isFirstLoad, event){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'load_resident_activity';
    let headers=await this.config.getHeader();
    let body;
    body={
      user_id:this.id,
      start:this.sdate,
      end:this.edate,
      take:20,
      skip:this.offset
    }
    

    if(this.terms){
      body.search=this.terms
     
      
    }

    if(this.status!=='0'){
      body.action_status=this.status
    }
    if(this.category!=='0'){
      body.activitycategory=this.category
    }
    console.log(body,{headers});
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
    //  this.count={
    //   total:res.data.total_count,
    //   attended:res.data.attended_count,
    //   declined:res.data.declined_count,
    //   upcoming:res.data.upcoming_count
    // };
     let dateArray=Object.keys(res.data.assigned_activities)
      for (let i = this.offset; i < dateArray.length; i++) {
        // let expand=false;
        // if(i==0){
        //   expand=true
        // }
        this.activity.push({date:dateArray[i],activity:res.data.assigned_activities[dateArray[i]]});
      }
      
      console.log('act:',this.activity);
     
      if (isFirstLoad)
      event.target.complete();
  
    this.offset=this.offset+20;
      
     },error=>{
       console.log(error);
      
     })
  }

  doInfinite(event) {
    if(this.status==0){
      this.getUpcoming(true,event)
    }else{
    this.getActivityList(true, event)
    }
    }

    changeStatus(ev){
     
      if(ev.detail.value==0){
        this.status='0';
        this.activity=[];
      this.offset=0;
       this.getUpcoming(false,'');
      }else{
      this.status=ev.detail.value;
      this.activity=[];
      this.offset=0;
      this.getActivityList(false,'');
      }
      
    }
    cancel() {
     
      this.terms = undefined;
      this.ionViewWillEnter();
    }

    async getUpcoming(isFirstLoad, event){
      let edate=moment().add(10,'years').format('YYYY-MM-DD HH:mm:ss');
      let sdate=moment().add(1,'days').format('YYYY-MM-DD HH:mm:ss');
      const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'load_resident_activity';
    let headers=await this.config.getHeader();
    let body;
    body={
      user_id:this.id,
      start:sdate,
      end:edate,
      take:20,
      skip:this.offset
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
    
     let dateArray=Object.keys(res.data.assigned_activities)
      for (let i = this.offset; i < dateArray.length; i++) {
        // let expand=false;
        // if(i==0){
        //   expand=true
        // }
        this.activity.push({date:dateArray[i],activity:res.data.assigned_activities[dateArray[i]]});
      }
      
      console.log('act:',this.activity);
     
      if (isFirstLoad)
      event.target.complete();
  
    this.offset=this.offset+20;
      
     },error=>{
       console.log(error);
      
     })
    }
  
    async search(){
      this.offset=0;
      this.activity=[];
    this.getActivityList(false,'');
    }

    async filter(){
      const modal = await this.modalCntl.create({
        component: RvActivityFilterComponent,
        componentProps: {
          
          date_filter:this.date_filter,
         category:this.category,
         status:this.status,
         sdate:this.sdate,
         edate:this.edate,
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
        if(dataReturned.data){
          this.date_filter=dataReturned.data.date_filter;
          this.status=dataReturned.data.status;
          this.sdate=dataReturned.data.sdate;
          this.edate=dataReturned.data.edate;
          this.category=dataReturned.data.category;
          this.offset=0;
          this.activity=[];
          this.getActivityList(false,'');
        }
        
      })
      return await modal.present();
    }

    async showDetails(item){
      const modal = await this.modalCntl.create({
        component: RvActivityDetailsComponent,
        componentProps: {
          
          data:item,
          cid:this.id
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
      
        if(dataReturned.data==1){
          this.ionViewWillEnter();
        }
      })
      return await modal.present();
    }
}
