import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvServiceActivitiesPage } from './rv-service-activities.page';

const routes: Routes = [
  {
    path: '',
    component: RvServiceActivitiesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvServiceActivitiesPageRoutingModule {}
