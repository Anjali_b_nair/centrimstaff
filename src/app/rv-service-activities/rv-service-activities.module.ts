import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvServiceActivitiesPageRoutingModule } from './rv-service-activities-routing.module';

import { RvServiceActivitiesPage } from './rv-service-activities.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvActivityFilterComponent } from '../components/rv-activity-filter/rv-activity-filter.component';
import { RvActivityDetailsComponent } from '../components/rv-activity-details/rv-activity-details.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvServiceActivitiesPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvServiceActivitiesPage,
    RvActivityFilterComponent,
    RvActivityDetailsComponent
  ],
  entryComponents:[
    RvActivityFilterComponent,
    RvActivityDetailsComponent
  ]
})
export class RvServiceActivitiesPageModule {}
