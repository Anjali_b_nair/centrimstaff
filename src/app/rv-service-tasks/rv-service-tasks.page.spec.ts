import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvServiceTasksPage } from './rv-service-tasks.page';

describe('RvServiceTasksPage', () => {
  let component: RvServiceTasksPage;
  let fixture: ComponentFixture<RvServiceTasksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvServiceTasksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvServiceTasksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
