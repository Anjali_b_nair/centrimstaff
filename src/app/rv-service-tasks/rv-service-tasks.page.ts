import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ModalController, ToastController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { RvAssignedResidentComponent } from '../components/rv-assigned-resident/rv-assigned-resident.component';
import { RvAssignedStaffComponent } from '../components/rv-assigned-staff/rv-assigned-staff.component';
import { RvTaskFilterComponent } from '../components/rv-task-filter/rv-task-filter.component';
import { RvTaskOptionsComponent } from '../components/rv-task-options/rv-task-options.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-service-tasks',
  templateUrl: './rv-service-tasks.page.html',
  styleUrls: ['./rv-service-tasks.page.scss'],
})
export class RvServiceTasksPage implements OnInit {

  cid:any;
  id:any;
  consumer:any;
  offset:any=0;
  tasks:any=[];
  status:any='0';
priority:any='3';
  
  sdate:any;
  edate:any;
  
  terms: any;
  subscription:Subscription;
  date_filter:any='6';
assignedTo:any=[];
staff:any;
tz:any;
    constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
      private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController,
      private toastCtlr:ToastController,private popCntl:PopoverController) { }
  
    ngOnInit() {
    }
    async ionViewWillEnter(){
      this.tz = await this.storage.get("TIMEZONE");
      this.cid=this.route.snapshot.paramMap.get('cid');
      this.id=this.route.snapshot.paramMap.get('id');
      this.consumer=this.route.snapshot.paramMap.get('consumer');
      this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD');
      this.getTaskList(false,'');
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
        this.back();
        
    }); 
    
    }
  
    back(){
      this.router.navigate(['/rv-services-home',{id:this.id,cid:this.cid}])
    }
  
    async getTaskList(isFirstLoad, event){
      const bid = await this.storage.get("BRANCH");
      const cid = await this.storage.get("COMPANY_ID");
      let url=this.config.domain_url+'load_resident_task_list';
      let headers=await this.config.getHeader();
      let body;
      body={
        user_id:this.id,
        sdate:this.sdate,
        edate:this.edate,
        take:20,
        skip:this.offset
      }
      if(this.status!=='5'){
        body.status=this.status
      }
      if(this.priority!=='3'){
        body.priority=this.priority
      }
  
      if(this.assignedTo.length){
        body.staff=this.assignedTo[0]
      }
  
      if(this.terms){
        body.search=this.terms
       
        
      }
      console.log(body,{headers},this.status);
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
        console.log('res:',res);
       
        for (let i = this.offset; i < res.data.length; i++) {
          let expand=false;
          if(i==0){
            expand=true
          }
          this.tasks.push({task:res.data[i],expanded:expand});
        }
        
        console.log('tasks:',this.tasks);
       
        if (isFirstLoad)
        event.target.complete();
    
      this.offset=this.offset+20;
        
       },error=>{
         console.log(error);
        
       })
    }
  
    doInfinite(event) {
      this.getTaskList(true, event)
      }
  
    createTask(){
      
      this.router.navigate(['/rv-create-task',{flag:3}])
    }
  
    async taskFilter(){
      const modal = await this.modalCntl.create({
        component: RvTaskFilterComponent,
        componentProps: {
          
         date_filter:this.date_filter,
         priority:this.priority,
         status:this.status,
         sdate:this.sdate,
         edate:this.edate,
         assignedTo:this.assignedTo,
         staff:this.staff
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
        if(dataReturned.data){
          this.date_filter=dataReturned.data.date_filter;
          this.priority=dataReturned.data.priority;
          this.status=dataReturned.data.status;
          this.sdate=dataReturned.data.sdate;
          this.edate=dataReturned.data.edate;
          this.assignedTo=dataReturned.data.assignedTo;
          this.staff=dataReturned.data.staff;
          this.offset=0;
          this.tasks=[];
          this.getTaskList(false,'');
        }
        
      })
      return await modal.present();
    }
    changeStatus(ev){
     
      if(ev.detail.value==5){
        this.status=undefined;
      }else{
      this.status=ev.detail.value
      }
      this.tasks=[];
      this.offset=0;
      this.getTaskList(false,'');
    }
    overdue(date){
      // if(new Date(date).getTime()<new Date(moment().add(1,'days').format()).getTime()){
      //   return true;
      // }else{
      //   return false;
      // }
      if(moment(date,'YYYY-MM-DD').isBefore(moment(),'day')){
        return true;
      }else{
        return false;
      }
    }
  
    expandItem(item){
     
            if (item.expanded) {
              item.expanded = false;
            } else {
             
              this.tasks.map(listItem => {
                if (item == listItem) {
                  listItem.expanded = !listItem.expanded;
                  
                } else {
                  listItem.expanded = false;
                }
                return listItem;
              });
            
            }
    }
  
    async changeTaskStatus(ev,item){
      const bid = await this.storage.get("BRANCH");
      const cid = await this.storage.get("COMPANY_ID");
      const uid = await this.storage.get("USER_ID");;
      let url=this.config.domain_url+'update_task_status';
      let headers=await this.config.getHeader();
      let body;
      body={
       task_id:item.task.id,
       status:ev.detail.value,
       completed_at:moment().format('YYYY-MM-DD HH:mm:ss'),
       completed_by:uid
  
      }
    
     console.log('stat:',body);
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        this.offset=0;
        this.tasks=[];
      this.getTaskList(false,'');
        this.presentAlert('Status updated successfully.')
      })
    }
    async presentAlert(mes) {
      const alert = await this.toastCtlr.create({
        message: mes,
        cssClass: 'toastStyle',
        duration: 3000,
        position:'top'
      });
      alert.present(); //update
    }
  
    cancel() {
     
      this.terms = undefined;
      this.ionViewWillEnter();
    }
  
    async search(){
      this.offset=0;
      this.tasks=[];
    this.getTaskList(false,'');
    }
  
    async options(ev,item){
      const popover = await this.popCntl.create({
        component:RvTaskOptionsComponent,
        cssClass:'full-width-modal',
        event:ev,
        backdropDismiss:true,
        componentProps:{
          data:item,
          flag:1
  
        },
        
        
        
      });
      popover.onDidDismiss().then((dataReturned) => {
        console.log('data:',dataReturned);
        if(dataReturned.data!=undefined||dataReturned.data!=null){
       
       this.offset=0;
       this.tasks=[];
       this.getTaskList(false,'');
        }
     
     
        
      });
      return await popover.present();
    }

    ShowContent(a,b,i){
      var x = document.getElementById(a+i);
      var y = document.getElementById(b+i);
            x.style.display = 'none';
            y.style.display = 'block';
    }

    async assigned(item){
      const popover = await this.modalCntl.create({
        component:RvAssignedStaffComponent,
        cssClass:'cbmorepop',
      backdropDismiss:true,
        componentProps:{
          data:item,
         
  
        },
        
        
        
      });
      
     
     
        
    
      return await popover.present();
    }

    async assignedRes(item){
      let data=item.slice(1);
      const popover = await this.popCntl.create({
        component:RvAssignedResidentComponent,
        cssClass:'cbmorepop',
      backdropDismiss:true,
        componentProps:{
          data:data,
         
  
        },
        
        
        
      });
      
     
     
        
    
      return await popover.present();
    }

    formatDate(date){
  
      return moment.utc(date).tz(this.tz).format('DD MMM, YYYY')
    }

}
