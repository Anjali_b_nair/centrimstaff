import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvServiceTasksPage } from './rv-service-tasks.page';

const routes: Routes = [
  {
    path: '',
    component: RvServiceTasksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvServiceTasksPageRoutingModule {}
