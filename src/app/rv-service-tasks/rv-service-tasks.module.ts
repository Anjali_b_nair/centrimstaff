import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvServiceTasksPageRoutingModule } from './rv-service-tasks-routing.module';

import { RvServiceTasksPage } from './rv-service-tasks.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvServiceTasksPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [RvServiceTasksPage]
})
export class RvServiceTasksPageModule {}
