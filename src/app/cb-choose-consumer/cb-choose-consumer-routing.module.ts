import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CbChooseConsumerPage } from './cb-choose-consumer.page';

const routes: Routes = [
  {
    path: '',
    component: CbChooseConsumerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CbChooseConsumerPageRoutingModule {}
