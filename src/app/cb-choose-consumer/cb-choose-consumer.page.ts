import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { CbChooseFamilyComponent } from '../components/cb-choose-family/cb-choose-family.component';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-cb-choose-consumer',
  templateUrl: './cb-choose-consumer.page.html',
  styleUrls: ['./cb-choose-consumer.page.scss'],
})
export class CbChooseConsumerPage implements OnInit {
  consumers:any[]=[];
  flag:any;
  terms:any;
  family:any[]=[];
  participants:any[]=[];
  modalOpen:boolean = false;
  subscription:Subscription;
  rv:any;
  offset:any = 1;
  constructor(private http:HttpClient,private router:Router,private config:HttpConfigService,private popCntlr:PopoverController,
    private objService:GetobjectService,private platform:Platform,private storage:Storage,private loadingCtrl:LoadingController,
    private toastCntlr:ToastController,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
  
      const bid=await this.storage.get("BRANCH")
      this.rv=await this.storage.get('RVSETTINGS');
                  this.terms='';
                  this.offset=1;
                  this.consumers=[];
                  this.showLoading();
                
                  this.getAllresidents(false,'');
                  // let url=this.config.domain_url+'residents';
                  // let headers=await this.config.getHeader();
                  // console.log(url);
  
                  // // let headers=await this.config.getHeader();
                  // this.http.get(url,{headers}).subscribe((data:any)=>{
                   
                  //   console.log("data:",data);
                  //   this.consumers=data.data
                   
                      
                  // },error=>{
                  //   console.log(error);
                  // });
          
                
      
                  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{   
                    if (this.modalOpen){
                     this.popCntlr.dismiss();
                    }else{
                    this.router.navigate(['/callbooking-list']) ;
                    }

                  }); 
                
                }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
 
 back(){
  if (this.modalOpen){
    this.popCntlr.dismiss();
   this.router.navigate(['/callbooking-list']) ;
   }else{
    this.router.navigate(['/callbooking-list']) ;
   }
 }
 cancel(){
  // this.hide=false;
  this.terms='';
  this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
}
search(){
  // this.hide=true;
  this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
}

   async openModel(item){
     this.family=[];
     let headers=await this.config.getHeader();
     let url=this.config.domain_url+'bookingConsumer/'+item.user_id
    this.http.get(url,{headers}).subscribe((data:any)=>{
      console.log("fam:",data);
      let fam=data.data.contacts;
      fam.forEach(element => {
        
          let ele={fam:element}
          this.family.push(ele);
          
    
        
      });
      this.showFamily(this.family,item);
    })
   
  }
  async showFamily(fam,item){
    if(fam.length==0){
      this.presentAlert('No family members found.')
    }else{
    if (!this.modalOpen){
      this.modalOpen = true;
    const modal = await this.modalCntl.create({
      component: CbChooseFamilyComponent,
      // backdropDismiss:false,
      componentProps:{
        data:fam
      },
      cssClass:'cbFamily full-width-modal'
      
      
    });
  
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.participants = dataReturned.data;
        this.modalOpen = false;
        console.log("selected:",this.participants);
        if(this.participants.length>0){
        this.objService.setExtras(this.participants);
        this.router.navigate(['/cb-pick-date-time',{cid:item.user_id,name:item.name,pic:item.photo}]);
        }
      }
    });
  
    return await modal.present();
  }
}
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}

async getAllresidents(isFirstLoad,event){
  

  // let url=this.config.domain_url+'get_all_residents_in_branch_with_filter';
  let url=this.config.domain_url+'get_all_residents_list_via_filters';  
  let headers=await this.config.getHeader();

  let body;
  body={
    type:1,  // 1 - active 2 -inactive 3 - no contract
    sort:'fname',  // sortby
    order:'ASC',   // ASC or DESC
  
    page:this.offset, 
    
  }

  if(this.terms){
  body.keyword=this.terms
  }
               console.log('body:',body,headers);
               
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
                 
    console.log("data:",data);

    for (let i = 0; i < data.data.residents.length; i++) {
      this.consumers.push(data.data.residents[i]);
    }

    if (isFirstLoad)
    event.target.complete();
    this.dismissLoader();
    this.offset=++this.offset;    
        
    },error=>{
      this.dismissLoader();
      console.log(error);
    });

}
doInfinite(event) {
  this.getAllresidents(true, event)
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
}
