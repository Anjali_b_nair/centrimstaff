import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CbChooseConsumerPageRoutingModule } from './cb-choose-consumer-routing.module';

import { CbChooseConsumerPage } from './cb-choose-consumer.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CbChooseConsumerPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [CbChooseConsumerPage]
})
export class CbChooseConsumerPageModule {}
