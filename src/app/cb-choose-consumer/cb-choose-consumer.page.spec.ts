import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CbChooseConsumerPage } from './cb-choose-consumer.page';

describe('CbChooseConsumerPage', () => {
  let component: CbChooseConsumerPage;
  let fixture: ComponentFixture<CbChooseConsumerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbChooseConsumerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CbChooseConsumerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
