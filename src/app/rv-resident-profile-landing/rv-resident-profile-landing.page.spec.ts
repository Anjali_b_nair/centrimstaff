import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResidentProfileLandingPage } from './rv-resident-profile-landing.page';

describe('RvResidentProfileLandingPage', () => {
  let component: RvResidentProfileLandingPage;
  let fixture: ComponentFixture<RvResidentProfileLandingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResidentProfileLandingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResidentProfileLandingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
