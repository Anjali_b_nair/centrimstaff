import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CallNumber } from "@ionic-native/call-number/ngx";
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, LoadingController, ActionSheetController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { RvMarkAwayComponent } from '../components/rv-mark-away/rv-mark-away.component';
import { RvQuickLinksComponent } from '../components/rv-quick-links/rv-quick-links.component';
import { RvMarkAwayOptionsComponent } from '../components/rv-mark-away-options/rv-mark-away-options.component';
@Component({
  selector: 'app-rv-resident-profile-landing',
  templateUrl: './rv-resident-profile-landing.page.html',
  styleUrls: ['./rv-resident-profile-landing.page.scss'],
})
export class RvResidentProfileLandingPage implements OnInit {
  id:any;
  consumer:any;
  mobile:any;
  phone:any;
  subscription:Subscription;
  edit:boolean=false;
  finance:boolean=false;
  dining:boolean=false;
  activity:boolean=false;
  maintenance:boolean=false;
  story:boolean=false;
  hide:boolean=true;
  hideOther:boolean=true;
  type:any=[];
  mark_away_list:any=[];
  task_view:boolean=false;
  expandProfile:boolean=true;
  flag:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
    private router:Router,private platform:Platform,private actionsheetCntlr:ActionSheetController,
    private storage:Storage,private loadingCtrl:LoadingController,private modalCntl:ModalController,
    private popCntl:PopoverController,private callNumber:CallNumber,private toastCtlr:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.id=this.route.snapshot.paramMap.get('id');
    this.mark_away_list=[];
    this.expandProfile=true;
    this.showLoading();
    // this.getModules();
    
     const bid=await this.storage.get("BRANCH");
     const cid=await this.storage.get("COMPANY_ID");
     this.flag=this.route.snapshot.paramMap.get('flag');
     (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.residents==1&&routes.includes('residents.edit')) {

        this.edit= true
      } else {

        this.edit = false;
        
      }
      // if (res.main_permissions.finance==1&&routes.includes('finance.index')) {

      //   this.finance= true
      // } else {

      //   this.finance = false;
        
      // }
      // if (res.main_permissions.dining==1&&routes.includes('dietry_profile.index')) {

      //   this.dining= true
      // } else {

      //   this.dining = false;
        
      // }
      // if (res.main_permissions.life_style==1&&routes.includes('story.create')) {

      //   this.story = true
      // } else {

      //   this.story = false;
      // }
      if (res.main_permissions.life_style==1&&routes.includes('activity.create')) {

        this.activity= true
      } else {
  
        this.activity = false;
        
      }
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.create')) {

        this.maintenance = true
      } else {

        this.maintenance = false
      }
      if (res.main_permissions.rv==1&&routes.includes('task.index')) {

        this.task_view = true
      } else {

        this.task_view = false;
      }
    
    })



    let url=this.config.domain_url+'consumer/'+this.id;
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers)
     this.http.get(url,{headers}).subscribe((res:any)=>{
     
      this.consumer=res.data;
     

    if(this.consumer.user.phone&&this.consumer.user.phone!=='61'){
      if(this.consumer.user.phone.substring(0,2)=='61'){
        let num=this.consumer.user.phone.slice(2);
        if(num.substring(0,1)=='0'){
          this.mobile=num;
        }else{
          this.mobile='0'+num;
        }
        
      }else{
    this.mobile=this.consumer.user.phone;
      }
    }
    if(this.consumer.user.mobile&&this.consumer.user.mobile!=='61'){
      if(this.consumer.user.mobile.substring(0,2)=='61'){
      let num=this.consumer.user.mobile.slice(2);
        if(num.substring(0,1)=='0'){
          this.phone=num;
        }else{
          this.phone='0'+num;
        }
        
      }else{
        this.phone=this.consumer.user.mobile;
      }
    
  }


      if(this.consumer.mark_away_list&&this.consumer.mark_away_list.length){
        
        for(let i in this.consumer.mark_away_list){
          this.mark_away_list.push({result:this.consumer.mark_away_list[i],expanded:false})
         
          }
      }
      console.log('con:',this.consumer);
      this.dismissLoader();
      
    },error=>{
      console.log(error)
    })
    
   
    this.getRequests();
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
     this.router.navigate(['/rv-res-list',{flag:1}]) ; 
     
 }); 
 
   }
   ionViewWillLeave() { 
     this.subscription.unsubscribe();
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  getCompanian(residents,user){
   
    let companian;
    const idx = residents.map(x => x.user_id).indexOf(user);
    
      if(idx<=0){
        
        companian=residents[idx+1].user_details.name
      }else{
        companian=residents[0].user_details.name;
      }
      return companian;
    }

    markAway(movein){
      let current=moment();
      if(movein){
      if(moment(movein)>current){
        return true;
      }
    }else{
      return true;
    }
    }

    showContent(i){
      if(i==1){
      this.hide=!this.hide;
      }else{
        this.hideOther=!this.hideOther
      }
      // if (item.expanded) {
      //   item.expanded = false;
      // } else {
      //   // if(this.area_id==0){
      //   this.mark_away_list.map(listItem => {
      //     if (item == listItem) {
      //       listItem.expanded = !listItem.expanded;
           
      //     } else {
      //       listItem.expanded = false;
      //     }
        
      //     return listItem;
      //   });
      // }
    
  }

    async quickLinks() {
      const bid=await this.storage.get('BRANCH');
      let button=[];
      if(this.maintenance){
        button.push({
            icon:'build',
            text: 'Create maintenance request',
            handler: () => {
              this.router.navigate(['/create-maintenance',{cid:this.id,flag:1,branch:bid,name:this.consumer.user.name,uid:this.consumer.user.user_id}])
            }
          })
      }
      // if(this.story){
        button.push({
            icon:'add',
            text: 'Create service request',
            handler: () => {
              
            }
          })
      // }
      // if(this.edit){
        button.push({
          icon:'star-outline',
          text: 'Add feedback',
          handler: () => {
            this.router.navigate(['/rv-add-feedback',{cid:this.id,id:this.consumer.user.user_id,consumer:this.consumer.user.name}])
          }
        })
      // }
      // if(this.message){
        button.push({
          icon:'warning',
          text: 'Add incident',
          handler: () => {
            
          }
        })
      // }
      if(this.activity){
        button.push({
          
            icon:'calendar',
            text: 'Book activity',
            handler: () => {
              this.router.navigate(['/activities',{cid:this.id}])
            }
          })
      }
      button.push({
          
        icon:'clipboard',
        text: 'Create task',
        handler: () => {
          this.router.navigate(['/rv-create-task',{cid:this.id,id:this.consumer.user.user_id,flag:1,consumer:this.consumer.user.name}])
        }
      })
      button.push({
          
        icon:'clipboard',
        text: 'Mark away',
        handler: () => {
          this.markAwayModal();
          // this.router.navigate(['/rv-create-task',{cid:this.id,id:this.consumer.user.user_id,flag:1,consumer:this.consumer.user.name}])
        }
      })
      button.push({
        icon: 'close',
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
         
        }
      })
      
        const actionSheet = await this.actionsheetCntlr.create({
          // header: "Select Image source",
          cssClass:'activity-options',
          buttons:button
          
        });
        await actionSheet.present();
      }

      gotoResidentGallery(){
        this.router.navigate(['/consumer-gallery',{cid:this.id,from:1}])
      }

      gotoNotes(){
        this.router.navigate(['/rv-resident-notes',{cid:this.id,id:this.consumer.user.user_id,details:JSON.stringify(this.consumer.property_details)}])
      }

      async markAwayModal(){
        const modal = await this.modalCntl.create({
          component:RvMarkAwayComponent,
          mode:'md',
          componentProps: {
            
           
            id:this.consumer.user.user_id,
            consumer:this.consumer
             
          },
          
          
        });
        modal.onDidDismiss().then(()=>{
          this.ionViewWillEnter();
        })
        return await modal.present();
      }


      async quickLinksModal(){
        const bid=await this.storage.get('BRANCH');
        const modal = await this.modalCntl.create({
          component: RvQuickLinksComponent,
          cssClass:'rv-quicklinks-modal',
          componentProps: {
            
           
             
          },
          
          
        });
        modal.onDidDismiss().then((dataReturned)=>{
          if(dataReturned.data){
            if(dataReturned.data==1){
              this.router.navigate(['/create-maintenance',{cid:this.id,flag:1,branch:bid,name:this.consumer.user.name,uid:this.consumer.user.user_id,type: JSON.stringify(this.type) }])
            }else if(dataReturned.data==2){

            }else if(dataReturned.data==3){
              this.router.navigate(['/rv-add-feedback',{cid:this.id,id:this.consumer.user.user_id,consumer:this.consumer.user.name}])
            }else if(dataReturned.data==4){

            }else if(dataReturned.data==5){
              this.router.navigate(['/activities',{cid:this.id}])
            }else if(dataReturned.data==6){
              let companian=null;
              if(this.consumer.property_details&&this.consumer.property_details.contract_details&&this.consumer.property_details.contract_details.all_residents){
                companian=this.consumer.property_details.contract_details.all_residents
              }
              console.log('com:',companian);
              
              this.router.navigate(['/rv-create-task',{cid:this.id,id:this.consumer.user.user_id,flag:1,consumer:this.consumer.user.name,companian:JSON.stringify(companian)}])
            }else if(dataReturned.data==7){
              this.markAwayModal();
            }
          }
          
        })
        return await modal.present();
      }

      async options(ev,item){
        let data;
        if(item){
          data=item
        }else{
          data=this.consumer.mark_away
        }
        const popover = await this.popCntl.create({
          component:RvMarkAwayOptionsComponent,
          cssClass:'rv-mark-away-options-pop',
          event:ev,
          backdropDismiss:true,
          componentProps:{
            consumer:data,
           
    
          },
          
          
          
        });
        popover.onDidDismiss().then((dataReturned) => {
          console.log('data:',dataReturned);
          if(dataReturned.data!=undefined||dataReturned.data!=null){
         this.ionViewWillEnter();
         
          }
       
       
          
        });
        return await popover.present();
      }

      async getRequests() {
   
        const data=await this.storage.get('BRANCH')
         
            const type=await this.storage.get('USER_TYPE');
            
             
              let url = this.config.domain_url + 'maintenance_filter';
             
              
              console.log("head:", url);
              let body = {
                status: '0',
                offset: 0,   // always 0
                limit: 2,
                role_id: type,
                from_app:1,
              
  
              }
              
              // let headers = new HttpHeaders({ 'branch_id': data.toString()});
              let headers=await this.config.getHeader();
              this.http.post(url,body, {headers}).subscribe((res: any) => {
          
                this.type = res.type;
               
              }, error => {
                console.log(error);
               
              })
           
    }
    getNextProfile(residents){
      let companian;
      const idx = residents.map(x => x.user_id).indexOf(this.consumer.user_id);
      
        if(idx<=0){
          
          companian=residents[idx+1].user_details.resident.id
        }else{
          companian=residents[0].user_details.resident.id;
        }
       this.router.navigate(['/rv-resident-profile-landing',{id:companian}])
    }

    gotoTasks(){
      let companian=null;
              if(this.consumer.property_details&&this.consumer.property_details.contract_details&&this.consumer.property_details.contract_details.all_residents){
                companian=this.consumer.property_details.contract_details.all_residents
              }
              console.log('com:',companian);
      this.router.navigate(['/rv-res-task-list',{cid:this.id,consumer:this.consumer.user.name,id:this.consumer.user.user_id,companian:JSON.stringify(companian)}])
    }

    makeCall(num) {
      if(num&&num.length>=10){
      this.callNumber.callNumber(num, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => {
          this.presentAlert('Something went wrong. Try again later.');
          console.log('Error launching dialer', err);
        })
      }else{
        this.presentAlert('Invalid phone number')
      }
    }
    async presentAlert(mes) {
      const alert = await this.toastCtlr.create({
        message: mes,
        cssClass: 'toastStyle',
        duration: 3000,
        position:'top'
      });
      alert.present(); //update
    }

    expand(){
      this.expandProfile=!this.expandProfile
    }
}
