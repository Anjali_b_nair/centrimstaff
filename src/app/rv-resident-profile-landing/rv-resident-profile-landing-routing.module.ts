import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResidentProfileLandingPage } from './rv-resident-profile-landing.page';

const routes: Routes = [
  {
    path: '',
    component: RvResidentProfileLandingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResidentProfileLandingPageRoutingModule {}
