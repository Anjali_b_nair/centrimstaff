import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResidentProfileLandingPageRoutingModule } from './rv-resident-profile-landing-routing.module';

import { RvResidentProfileLandingPage } from './rv-resident-profile-landing.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvMarkAwayComponent } from '../components/rv-mark-away/rv-mark-away.component';
import { RvQuickLinksComponent } from '../components/rv-quick-links/rv-quick-links.component';
import { RvMarkAwayOptionsComponent } from '../components/rv-mark-away-options/rv-mark-away-options.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResidentProfileLandingPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvResidentProfileLandingPage,
    RvMarkAwayComponent,
    RvQuickLinksComponent,
    RvMarkAwayOptionsComponent
  ],
  entryComponents:[
    RvMarkAwayComponent,
    RvQuickLinksComponent,
    RvMarkAwayOptionsComponent
  ]
})
export class RvResidentProfileLandingPageModule {}
