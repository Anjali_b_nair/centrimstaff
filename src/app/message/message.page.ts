import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {
  name:any;
  id:any;
  chat:any=[];
  message:any={};
  subscription:Subscription;
  // current:number;
  updated_at:any;
  hide:boolean=false;
  terms:any;
 
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private router:Router,
    private platform:Platform) { }

  ngOnInit() {
  }
 
  async ionViewWillEnter(){
    
    this.hide=false;
    this.chat=[];
    this.terms='';
    
      const name=await this.storage.get("NAME")
        this.name=name;
    
      const type=await this.storage.get("USER_TYPE")
      
      const data=await this.storage.get("USER_ID")

        
          this.id=data;
        
        
          
            const bid=await this.storage.get("BRANCH")

        console.log("empty:",this.chat);
        let url=this.config.domain_url+'showrooms';
        let headers=await this.config.getHeader();;
        let body={user_one:this.id}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res,body);
          let mess=[];
          for(let i in res.data){
          mess.push( res.data[i]);
          }
          console.log("me:",mess);
          mess.forEach(element => {
            if(element.user_two!=element.user_one){
              if(element.messages.length>0){
            element.messages.forEach(ele => {
                let current=new Date();
               let date=new Date(ele.updated_at);
                let time= new Date( (date.getTime() - 
                date.getTimezoneOffset()*60000)+current.getTimezoneOffset() *60000);
                
                let t=(current.getTime()-(new Date(time).getTime()));
                let hr = Math.floor(t/ 3600 / 1000);
                console.log(t);
                

                var mon = new Array();
                mon[0] = "Jan";
                mon[1] = "Feb";
                mon[2] = "Mar";
                mon[3] = "Apr";
                mon[4] = "May";
                mon[5] = "Jun";
                mon[6] = "Jul";
                mon[7] = "Aug";
                mon[8] = "Sep";
                mon[9] = "Oct";
                mon[10] = "Nov";
                mon[11] = "Dec";


                if(hr>24){
                  // this.updated_at=time.getFullYear()+'/' + this.fixDigit(time.getMonth()+1) + '/'+time.getDate();
                  this.updated_at=time.getDate()+' '+mon[time.getMonth()]
                }else{
                  var hours = time.getHours();
                  let minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  var hour = hours < 10 ? '0'+hours:hours;
                  var minute = minutes < 10 ? '0'+minutes : minutes;
                  this.updated_at= hour + ':' + minute + ' ' + ampm;
                  // this.updated_at=time.getHours()+':'+time.getMinutes();
                }
                console.log(this.updated_at);
                let ex;
                if(ele.delete_status==this.id||ele.delete_status==1){
                  this.message=null
                }else{
                if(ele.attachment&&ele.attachment!=""){
                let ext=(ele.attachment).substr((ele.attachment).lastIndexOf('.') + 1);
                
                if(ext == 'jpg'||ext=='jpeg'||ext=='png') {
                  ex = 'Photo'
                } else if (ext == 'mp4' || ext == 'mkv' || ext == 'avi'||ext=='mov'||ext=='x-flv' || ext=='x-mpegURL'|| ext=="MP2T" || ext=="3gpp"|| ext=="quicktime"|| ext=="x-msvideo"|| ext=="x-ms-wmv") {
                  ex='Video'
                }else{
                  ex='Document'
                }
              }
              this.message={'time':this.updated_at,'msg':ele.msg,'attachment':ele.attachment,'ext':ex}
            }
            });
          }else if(element.messages.length==0){
            this.message=null;
          }
              let user_two;
              console.log("one;",element.user_one,"login:",this.id);
              
              if(element.user_one==this.id){
                console.log("work:",element.user_two);
                
                user_two=element.user_two
              }else{
                user_two=element.user_one
              }
              console.log("user2:",user_two);
              let room;
              let wing;
              let fam;
              let rel;
              if(element.user.usertpe=="Family"){
                if(element.user.family.length>0){
                fam=element.user.family[0].parent_details[0].user.name;
                rel=element.user.family[0].relation;
                room=null;
                wing=null;
                }else{
                  fam=null;
                rel=null;
                room=null;
                wing=null;
                }
              }else if(element.user.usertpe=="Residents"){
                fam=null;
                rel=null;
                if(element.user.resident!=null){
                room=element.user.resident.room;
                if(element.user.resident.wing){
                wing=element.user.resident.wing.name;
                }else{
                  wing=null
                }
                }else{
                  room=null;
                  wing=null;
                }
              }else{
                fam=null;
                rel=null;
                room=null;
                wing=null;
              }
            this.chat.push({'name':element.user.name,'pic':element.user.profile_pic,'user_two':user_two,'message':this.message,'count':element.count,'tag':element.user.usertpe,room:room,wing:wing,fam:fam,rel:rel});
            }
          })
          console.log("res:",this.chat);
          });
        
          
         
         
         
          console.log("chat:",this.chat);
       
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }



  openChat(item){
    this.router.navigate(['/chatbox',{flag:1,user_two:item.user_two,pic:item.pic,name:item.name,room:item.room,wing:item.wing,fam:item.fam,rel:item.rel,tag:item.tag}]);
}
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}

cancel(){
  this.hide=false;
  this.terms='';
}
search(){
    this.hide=true;
  }
}
