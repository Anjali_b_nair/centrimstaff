import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResourcesDetailsPage } from './resources-details.page';

const routes: Routes = [
  {
    path: '',
    component: ResourcesDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourcesDetailsPageRoutingModule {}
