import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResourcesDetailsPageRoutingModule } from './resources-details-routing.module';

import { ResourcesDetailsPage } from './resources-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResourcesDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ResourcesDetailsPage]
})
export class ResourcesDetailsPageModule {}
