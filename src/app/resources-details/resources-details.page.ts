import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-resources-details',
  templateUrl: './resources-details.page.html',
  styleUrls: ['./resources-details.page.scss'],
})
export class ResourcesDetailsPage implements OnInit {

  res_id:any;
  resource:any=[];
  title:any;
  subscription:Subscription;
  searchTerm:any;
  hide:boolean=false;
  terms:any;
  folder:any=[];
  flag:any;
  count:any=0;
  res:any=[];
  type:any=0;
    constructor(private route:ActivatedRoute,private http:HttpClient,private platform:Platform,private modalCntl:ModalController,
      private iab: InAppBrowser,private config:HttpConfigService,private router:Router,private storage:Storage,private dialog:SpinnerDialog) { }
  
    ngOnInit() {
      
    }
    async ionViewWillEnter(){
      this.res=JSON.parse(this.route.snapshot.paramMap.get('res'));
      this.res_id=this.res[this.res.length-1].id;
      this.title=this.res[this.res.length-1].title;
      this.count=this.route.snapshot.paramMap.get('count');
      (await this.config.getUserPermission()).subscribe((res: any) => {
        console.log('permissions:', res);
        let routes = res.user_routes;
        if (res.main_permissions.life_style==1&&(routes.includes('resource.edit')||routes.includes('resource.create')||routes.includes('resource.destroy'))) {
  
          this.type= 2
        } else {
  
          this.type = 0;
          
        }
        
        this.getContent();
      })
      // this.res_id=this.route.snapshot.paramMap.get('res_id');
      // this.title=this.route.snapshot.paramMap.get('res_title');
      
      
      this.terms='';
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        if(this.count==0){
          this.router.navigate(['/resources-list']) ;
          
          
        }else{
          this.count--;
          this.res.pop();
          this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
        }
      });  
    }
    back(){
      if(this.count==0){
        this.router.navigate(['/resources-list']) ;
        
      }else{
        this.count--;
        this.res.pop();
        this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
      }
    }
    ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
    async getContent(){
      
       
         
          const bid=await this.storage.get("BRANCH")


            let branch=bid.toString();
      let headers=await this.config.getHeader();
      let url=this.config.domain_url+'resource/'+this.res_id;
      // let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.resource=res.resource_files;
          this.folder=res.resource_fldr;
         
          
        },error=>{
          console.log(error);
          
        })
    
    }
  
  
    openDoc(item){
      if(item.url=="null"){
        // window.open(encodeURI(item.file),"_system","location=yes");
        let options:InAppBrowserOptions ={
          location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'yes'
        }
        if(item.file.includes('.pdf')){
          this.showpdf(item.file)
        }else{
        const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
        browser.on('loadstart').subscribe(() => {
      
          this.dialog.show();   
         
        }, err => {
          this.dialog.hide();
        })
      
        browser.on('loadstop').subscribe(()=>{
          this.dialog.hide();;
        }, err =>{
          this.dialog.hide();
        })
      
        browser.on('loaderror').subscribe(()=>{
          this.dialog.hide();
        }, err =>{
          this.dialog.hide();
        })
        
        browser.on('exit').subscribe(()=>{
          this.dialog.hide();
        }, err =>{
          this.dialog.hide();
        })
      }
        }else{
          let options:InAppBrowserOptions ={
            location:'no',
          hideurlbar:'yes',
          zoom:'no'
          }
          const browser = this.iab.create(item.url,'_blank',options);
        
        }
  
  
    }
   
      async search(){
        if(this.terms){
        this.hide=true;
        const bid=await this.storage.get("BRANCH")
    
    
        let branch=bid.toString();
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'resource_search';
    // let headers=await this.config.getHeader();
    let body;
    body={
      type:this.type,
      search:this.terms,
      parent_id:this.res_id
    }

    console.log('search:',body,{headers});
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('search:',res,url);
      this.folder=res.resource_fldr;
      this.resource=res.resource_files;
      
    },error=>{
      console.log(error);
      
    })
  }else{
    this.getContent();
  }
      }
  
  
    
    cancel(){
      this.hide=false;
      this.terms='';
    }
  
    openFolder(item){
      this.count++;
      this.res.push({id:item.id,title:item.title});
      this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
      // this.router.navigate(['subfloderfiles',{title:item.title,fl_id:item.id,res_id:this.res_id,res_title:this.title}])
    }
    async showpdf(file){
      const modal = await this.modalCntl.create({
        component: ShowpdfComponent,
        cssClass:'full-width-modal',
        componentProps: {
          
          data:file,
          
           
        },
        
        
      });
      return await modal.present();
    }
}
