import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-group-members',
  templateUrl: './group-members.page.html',
  styleUrls: ['./group-members.page.scss'],
})
export class GroupMembersPage implements OnInit {
id:any;
members:any[]=[];
gname:any;
flag:any;
subscription:Subscription;
  constructor(private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,
    private toastCntlr:ToastController,private alertController:AlertController,private router:Router,
    private platform:Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.id=this.route.snapshot.paramMap.get('id');
    let url=this.config.domain_url+'group/'+this.id;
    this.flag = this.route.snapshot.paramMap.get('flag');
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("mem:",res);
      this.gname=res.data.name;
      this.members=res.data.residents;
      
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/view-group',{flag:this.flag}])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }


  edit(){
    this.router.navigate(['/edit-group',{id:this.id,flag:this.flag}]);
  }
  async delete(){
    let headers=await this.config.getHeader();
    const alert = await this.alertController.create({
      mode:'ios',
      header: 'Are you sure?',
      message: 'You will not be able to recover this group!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
          this.http.delete(this.config.domain_url+'group/'+this.id,{headers}).subscribe((res:any)=>{
            console.log('delete:',res);
            if(res.status=="success"){
              this.Toast('Group deleted successfully.')
                this.router.navigate(['/view-group']);
            }else{
              this.Toast('Something went wrong.Please try again later.');
            }
          })
            
          }
        }
      ]
    });
  
    await alert.present();
  }
  async Toast(mes){
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present();
  }
}
