import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ContactOptionsComponent } from '../components/contact-options/contact-options.component';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-contacts',
  templateUrl: './consumer-contacts.page.html',
  styleUrls: ['./consumer-contacts.page.scss'],
})
export class ConsumerContactsPage implements OnInit {
consumer:any={};
img:any;
name:any;
branch:any;
room:any;
contacts:any=[];
pending:any=[];
subscription:Subscription;
id:any;
flag:any;
wing:any;
edit:boolean=false;
  constructor(private objService:GetobjectService,private platform:Platform,private router:Router,
    private popCntlr:PopoverController,private http:HttpClient,private storage:Storage,
    private config:HttpConfigService,private route:ActivatedRoute,private toastCntlr:ToastController) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.id=this.route.snapshot.paramMap.get('id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.contacts=[];
      this.pending=[];
    // this.consumer=this.objService.getExtras();
    
    




      const bid=await this.storage.get("BRANCH")

     let url=this.config.domain_url+'consumer/'+this.id;
     let headers=await this.config.getHeader();;
     this.http.get(url,{headers}).subscribe((res:any)=>{
      
      this.consumer=res.data;
      console.log("cons:",this.consumer);
      
      this.img=this.consumer.user.profile_pic;
      this.name=this.consumer.user.name;
      this.branch=this.consumer.user.user_details.user_branch.name;
      this.room=this.consumer.room;
      this.contacts=this.consumer.all_contacts;
      this.pending=this.consumer.pending_contacts;
      this.wing=this.consumer.wing.name;
      console.log("cont:",this.contacts,"pend:",this.pending)

    });

    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.residents==1&&routes.includes('residents.edit')) {

        this.edit= true
      } else {

        this.edit = false;
        
      }
    })
  
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.router.navigate(['/resident-profile',{id:this.id,flag:this.flag}]) ; 
  }); 
  
    }
    ionViewWillLeave() { 
      this.subscription.unsubscribe();
   }
   async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }


   async showOptions(ev,item,type,i){
    const popover = await this.popCntlr.create({
      component: ContactOptionsComponent,
      cssClass:'cc-options-popover',
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:item,
        type:type,
        index:i

      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data!=undefined||dataReturned.data!=null){
      if (dataReturned.data=='resend') {
        this.presentAlert('Resend invitation successfully.')
        this.popCntlr.dismiss();
      }else{
        this.popCntlr.dismiss();
      this.ionViewWillEnter();
      }
    }
      // if (dataReturned.data!=undefined) {
        
      //   if(type==1){
      //     this.contacts.splice(i,1);
      //   }else{
      //     this.pending.splice(i,1);
      //   }
        
      // }
   
  
   
      
    });
    return await popover.present();
  }
}
