import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerContactsPage } from './consumer-contacts.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumerContactsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerContactsPageRoutingModule {}
