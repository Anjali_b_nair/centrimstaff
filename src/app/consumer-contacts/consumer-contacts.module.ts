import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerContactsPageRoutingModule } from './consumer-contacts-routing.module';

import { ConsumerContactsPage } from './consumer-contacts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerContactsPageRoutingModule
  ],
  declarations: [ConsumerContactsPage]
})
export class ConsumerContactsPageModule {}
