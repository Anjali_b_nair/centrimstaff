import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-rv-res-add-note',
  templateUrl: './rv-res-add-note.page.html',
  styleUrls: ['./rv-res-add-note.page.scss'],
})
export class RvResAddNotePage implements OnInit {
note_type:any;
note:any;
date:any;
cid:any;
id:any;
isLoading:boolean=false;
imageResponse:any=[];
subscription:Subscription;
add_for_all:any=0;
details:any;
  constructor(private router:Router,private route:ActivatedRoute,private storage:Storage,
    private platform:Platform,private http:HttpClient,private config:HttpConfigService,
    private actionsheetCntlr:ActionSheetController,
    private loadingCtrl:LoadingController,private toastCntlr:ToastController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.details=JSON.parse(this.route.snapshot.paramMap.get('details'));
    this.note_type=this.route.snapshot.paramMap.get('type');
    console.log('detsis:',this.details);
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.back();
      
  }); 
  }

  back(){
    this.router.navigate(['/rv-resident-notes',{cid:this.cid,id:this.id,details:JSON.stringify(this.details)}],{replaceUrl:true})
  }

  getCompanian(residents,user){
   
    let companian;
    const idx = residents.map(x => x.user_id).indexOf(parseInt(user));
    
      if(idx<=0){
        
        companian=residents[idx+1].user_details.name
      }else{
        companian=residents[0].user_details.name;
      }
      console.log('compan:',this.details)
      return companian;
    }
    setNoteType(t){
      this.note_type=t
    }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.uploadImg(base64Image);
  
       
    // }, (err) => {
    //   // Handle error
    // });
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
   
    this.uploadImg(base64Image);
  }
  async addImage(){
    
  //   let options;
  //   options={
  //   maximumImagesCount: 5,
  //   outputType: 1,
  //   quality:100
  // }
  // if(this.platform.is('ios')){
  //   options.disable_popover=true
  // }
  //   this.imagePicker.getPictures(options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //         console.log('Image URI: ' + results[i]);
          
          
  //         if((results[i]==='O')||results[i]==='K'){
  //           console.log("no img");
            
  //           }else{
             
  //           this.uploadImg('data:image/jpeg;base64,' + results[i])
  //           }
  //     }
  //   }, (err) => { });
  
  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
          console.log('Image URI: ' + image.photos[i]);
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
            this.uploadImg('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }

  
  
  }
  removeImg(i){
    this.imageResponse.splice(i,1);
    
  }

  async uploadImg(img){
    if(this.isLoading==false){
      this.showLoading();
      }
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      
      this.imageResponse.push(res.data);
      this.dismissLoader();
      
    })
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  change(ev){
    if(ev.currentTarget.checked==true){
      this.add_for_all=1;
    }else{
      this.add_for_all=0;
    }
  }
  
  async addNote(){
    const bid = await this.storage.get("BRANCH");
    const uid = await this.storage.get("USER_ID");
    const cid = await this.storage.get("COMPANY_ID");
    const tz = await this.storage.get("TIMEZONE");
    if(!this.note||this.note==''){
      this.presentAlert('Please enter the note.')
    }else{
      let url=this.config.domain_url+'add_consumer_note';
    let headers=await this.config.getHeader();
   let event_date;
   if(this.date){
    event_date=moment(this.date).format('YYYY-MM-DD HH:mm:ss')
   }else{
    event_date=null
   }
    let body;
    body={
      add_for_all:this.add_for_all,
      note:this.note,
      note_type:this.note_type,
      created_by:uid,
      event_date:event_date,
      note_date:moment().tz(tz).format('YYYY-MM-DD HH:mm:ss'),
      note_attachment:this.imageResponse,
      user_id:parseInt(this.id),
     

    }

    if(this.details&&this.details.contract_details){
      body. contract_id=this.details.contract_details.id
    }
    console.log('body:',body);
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     
      this.presentAlert('Note created successfully.')
      this.back();
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
}
