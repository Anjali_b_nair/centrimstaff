import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResAddNotePage } from './rv-res-add-note.page';

describe('RvResAddNotePage', () => {
  let component: RvResAddNotePage;
  let fixture: ComponentFixture<RvResAddNotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResAddNotePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResAddNotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
