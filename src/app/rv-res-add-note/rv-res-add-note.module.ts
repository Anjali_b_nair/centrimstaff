import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResAddNotePageRoutingModule } from './rv-res-add-note-routing.module';

import { RvResAddNotePage } from './rv-res-add-note.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResAddNotePageRoutingModule
  ],
  declarations: [RvResAddNotePage]
})
export class RvResAddNotePageModule {}
