import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResAddNotePage } from './rv-res-add-note.page';

const routes: Routes = [
  {
    path: '',
    component: RvResAddNotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResAddNotePageRoutingModule {}
