import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningAddItemsPage } from './dining-add-items.page';

const routes: Routes = [
  {
    path: '',
    component: DiningAddItemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningAddItemsPageRoutingModule {}
