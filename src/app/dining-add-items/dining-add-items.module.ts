import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningAddItemsPageRoutingModule } from './dining-add-items-routing.module';

import { DiningAddItemsPage } from './dining-add-items.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningAddItemsPageRoutingModule
  ],
  declarations: [DiningAddItemsPage]
})
export class DiningAddItemsPageModule {}
