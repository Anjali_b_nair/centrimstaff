import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningAddItemsPage } from './dining-add-items.page';

describe('DiningAddItemsPage', () => {
  let component: DiningAddItemsPage;
  let fixture: ComponentFixture<DiningAddItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningAddItemsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningAddItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
