import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dining-add-items',
  templateUrl: './dining-add-items.page.html',
  styleUrls: ['./dining-add-items.page.scss'],
})
export class DiningAddItemsPage implements OnInit {
  subscription:Subscription;
 flag:any;
    constructor(private router:Router,private platform:Platform,private route:ActivatedRoute) { }
  
    ngOnInit() {
    }
  ionViewWillEnter(){
   this.flag=this.route.snapshot.paramMap.get('flag');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
        back(){
          if(this.flag==1){
          this.router.navigate(['/din-inroom-dining'])
          }else{
            this.router.navigate(['/din-dining-table'])
          }
        }
}
