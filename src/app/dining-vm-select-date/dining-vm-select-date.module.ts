import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningVmSelectDatePageRoutingModule } from './dining-vm-select-date-routing.module';

import { DiningVmSelectDatePage } from './dining-vm-select-date.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningVmSelectDatePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningVmSelectDatePage]
})
export class DiningVmSelectDatePageModule {}
