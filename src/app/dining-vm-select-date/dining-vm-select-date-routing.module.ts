import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningVmSelectDatePage } from './dining-vm-select-date.page';

const routes: Routes = [
  {
    path: '',
    component: DiningVmSelectDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningVmSelectDatePageRoutingModule {}
