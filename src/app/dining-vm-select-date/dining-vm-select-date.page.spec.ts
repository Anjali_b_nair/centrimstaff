import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningVmSelectDatePage } from './dining-vm-select-date.page';

describe('DiningVmSelectDatePage', () => {
  let component: DiningVmSelectDatePage;
  let fixture: ComponentFixture<DiningVmSelectDatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningVmSelectDatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningVmSelectDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
