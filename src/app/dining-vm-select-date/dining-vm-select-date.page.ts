import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IonContent, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-dining-vm-select-date',
  templateUrl: './dining-vm-select-date.page.html',
  styleUrls: ['./dining-vm-select-date.page.scss'],
})
export class DiningVmSelectDatePage implements OnInit {
subscription:Subscription;
menu:any;
currentDate:any;
start:any;
end:any;
dates:any[]=[];
@ViewChild(IonContent, { static: false }) content: IonContent;
  constructor(private router:Router,private platform:Platform,private route:ActivatedRoute,
    @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.dates=[];
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    console.log('menu:',this.menu,menu);
    this.currentDate=moment().format('DD MMM YYYY');
    this.start=moment(this.menu.start_date);
    this.end=moment(this.menu.enddate);
    var start=this.start.clone();
    while(start.isSameOrBefore(this.end)){
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1,'days');
    }

    console.log('dates:',this.dates);
    // const cid=await this.storage.get('COMPANY_ID');
    // const bid=await this.storage.get('BRANCH');
    setTimeout(()=>{
      var titleELe = this.document.getElementById(this.currentDate);
      console.log(titleELe)
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    },500)

    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   
     back(){
       this.router.navigate(['/dining-view-menu'],{replaceUrl:true})
     }

     gotoListing(item){
       this.router.navigate(['/dining-menu-listing',{date:item,menu:JSON.stringify(this.menu)}],{replaceUrl:true})
     }
}
