import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmCallbookingPage } from './confirm-callbooking.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmCallbookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmCallbookingPageRoutingModule {}
