import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.page.html',
  styleUrls: ['./edit-group.page.scss'],
})
export class EditGroupPage implements OnInit {
id:any;
gname:any;
terms:any;
active:any[]=[];
inactive:any[]=[];
temp:any[]=[];
sel_id:any[]=[];
consumers:any[]=[];
all:boolean=false;
subscription:Subscription;
flag:any;
// members:any[]=[];
  constructor(private route:ActivatedRoute,private config:HttpConfigService,private http:HttpClient,
    private storage:Storage,private toastCntlr:ToastController,private alertCntlr:AlertController,
    private router:Router,private platform:Platform,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // this.members=[];
    this.sel_id=[];
    this.active=[];
    this.inactive=[];
    this.temp=[];
    this.consumers=[];
    this.showLoading();
    this.activeUsers();
    this.inactiveUsers();
    this.id=this.route.snapshot.paramMap.get('id');
    this.flag = this.route.snapshot.paramMap.get('flag');
    let url=this.config.domain_url+'group/'+this.id;
    let headers=await this.config.getHeader();;
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("mem:",res);
      this.gname=res.data.name;
      if(res.data.residents.length>0){
        res.data.residents.forEach(element => {
          this.sel_id.push(element.user.user_id);
          this.temp.push(element.user.user_id)
        });
      }
      
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/view-group',{flag:this.flag}])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }


  select(ev,item){
   
    //   this.temp.push(item.user.user_id);
   
    //   this.sel_id=this.temp.reduce((arr,x)=>{
    //     let exists = !!arr.find(y => y === x);
    //     console.log("exists:",exists);
        
    //     if(!exists){
    //         arr.push(x);
    //     }else{
    //       var index = arr.indexOf(parseInt(x));
    //       console.log("index:",index,x);
          
    //       arr.splice(index, 1);
    //     }
        
    //     return arr;
    // }, []);

    if(this.temp.includes(item.user.user_id)){
      var index = this.sel_id.indexOf(item.user.user_id);
      this.sel_id.splice(index, 1);
      console.log("index:",this.sel_id.indexOf(item.user.user_id))
      this.temp.splice(this.temp.indexOf(item.user.user_id),1)
    }else{
      this.temp.push(item.user.user_id);
    }
    this.sel_id=this.temp.reduce((arr,x)=>{
      let exists = !!arr.find(y => y === x);
      if(!exists){
          arr.push(x);
      }
      
      return arr;
  }, []);



    console.log("selected:",this.sel_id,this.temp);
    
    }
    
    async activeUsers(){
      
        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")

            let url=this.config.domain_url+'residents?type=1';
            let headers=await this.config.getHeader();
            console.log(url);
          
            // let headers=await this.config.getHeader();
            this.http.get(url,{headers}).subscribe((data:any)=>{
             
              console.log("actdata:",data);
              this.active=data.data
             this.active.forEach(ele=>{
               this.consumers.push(ele);
             })
                this.dismissLoader();
            },error=>{
              console.log(error);
              this.dismissLoader();
            });
         
     
    }
    async inactiveUsers(){
     
        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")

            let url=this.config.domain_url+'residents?type=2';
            let headers=await this.config.getHeader();
            console.log(url);
          
            // let headers=await this.config.getHeader();
            this.http.get(url,{headers}).subscribe((data:any)=>{
             
              console.log("inactdata:",data);
              this.inactive=data.data
              this.inactive.forEach(ele=>{
                this.consumers.push(ele);
              })
                
            },error=>{
              console.log(error);
            });
         
    }
    
    
    selectAll(ev){
      this.all=true;
      this.sel_id=[];
      
      this.consumers.forEach(element=>{
        if(this.temp.includes(element.user.user_id)){
  
        }else{
          this.temp.push(element.user.user_id);
        }
        
        this.sel_id=this.temp.reduce((arr,x)=>{
          let exists = !!arr.find(y => y=== x);
          if(!exists){
              arr.push(x);
          }
          return arr;
      }, []);
    })
    console.log("selectAll:",this.sel_id);
    
      // })
    }

    
    
    reset(){
      this.all=false;
      this.sel_id=[];
        this.temp=[];
        console.log("reset:",this.sel_id);
    }
    
    async createGroup(){
      const alert = await this.alertCntlr.create({
        cssClass: 'create-edit-group-alert',
        mode:'ios',
        header: 'Edit group',
        inputs: [
          {
            name: 'name',
            type: 'text',
            placeholder: 'Enter group name',
            value: this.gname
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
           
            handler: () => {
              this.alertCntlr.dismiss();
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Edit',
            handler: (alertData) => { //takes the data 
              console.log(alertData.name);
              if(alertData.name==undefined || alertData.name===""){
                alert.message='The name field is required!'
                // alert.setMessage('The name field is required!');
                  return false;
              }else if(alertData.name.length<3){
                alert.message='The name must be at least 3 characters.'
                return false;
              }else{
                console.log('creting');
                
                this.create(alertData.name)
                this.alertCntlr.dismiss();
              }
            }
          }
        ]
      });
    
      await alert.present();
    }
    async create(name){
     
        const bid=await this.storage.get("BRANCH")

          console.log(name);
          let url=this.config.domain_url+'update_group';
          let headers=await this.config.getHeader();;
          let body={
            name:name,
            residents:this.sel_id,
            group_id:this.id
          }
          console.log("body",body);
          
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
            if(res.status=true){
                this.presentAlert();
                this.router.navigate(['/view-group'])
            }
            
          })
       
      
      
    }
    
    async presentAlert() {
      const alert = await this.toastCntlr.create({
        message: 'Updated successfully.',
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }
    cancel(){
      this.terms='';
      
    }
    async showLoading() {
      const loading = await this.loadingCtrl.create({
        cssClass: 'custom-loading',
        
        // message: '<ion-img src="assets/loader.gif"></ion-img>',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
}
