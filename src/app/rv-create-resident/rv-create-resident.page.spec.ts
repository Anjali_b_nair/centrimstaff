import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvCreateResidentPage } from './rv-create-resident.page';

describe('RvCreateResidentPage', () => {
  let component: RvCreateResidentPage;
  let fixture: ComponentFixture<RvCreateResidentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvCreateResidentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvCreateResidentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
