import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Platform, ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-rv-create-resident',
  templateUrl: './rv-create-resident.page.html',
  styleUrls: ['./rv-create-resident.page.scss'],
})
export class RvCreateResidentPage implements OnInit {
  subscription:Subscription;
  expand1:boolean=true;
  expand2:boolean=false;
  expand3:boolean=false;
  expand4:boolean=false;
  expand5:boolean=false;
  expand6:boolean=false;
  expand7:boolean=false;
  expand8:boolean=false;

  isLoading:boolean=false;
  title:any='0';
  fname:any;
  lname:any;
  mobile:any;
  phone:any;
  email:any;
  // uname:any;
  status:any;
  position:any='0';
  description:any;
  dob:any;
  gender:any='0';
  birth_country:any;
  countries:any[]=[];
  languages:any[]=[];
  likes:any=[];
  dislikes:any=[];
  interests:any=[];
  needs:any=[];
  groups:any[]=[];
  group:any[]=[];
  group_name:any[]=[];
  religion:any='0';
  marital_status:any='0';

  have_pet:any='0';
  pet:any;
  pet_policy:any='0';
  permission_to_enter:any='0';
  property_note:any;
  share_photo:boolean=false;
  share_dob:boolean=false;
  share_phone:boolean=false;
  share_details:boolean=false;
  share_email:boolean=false;

  interpreter:any='0';
  car_registration:any;
  gate_no:any;
  key_tag:any;
  key_safe:any;
  additional_notes:any;

  medical_conditions:any[]=[];
  medical_history:any;
  medicare_number:any;
  ind_ref_no:any;
  medical_card_expiry:any;
  insurance_provider:any;
  membership_no:any;
  ambulance_cover:any;
  hcp:any='0';
  hcp_level:any;
  hcp_provider:any;
  provider_name:any;
  additional_health_note:any;

  pension:any;
  pension_type:any;
  pension_card_expiry:any;
  DVA:any;
  DVA_card:any;

  street_no:any;
  street_name:any;
  suburb:any;
  postcode:any;
  country:any='1';
  state:any;
  petImages:any=[];
  constructor(private http:HttpClient,private config:HttpConfigService,private platform:Platform,private toastCtlr:ToastController,
    private storage:Storage,private loadingCtrl:LoadingController,private router:Router,
    private actionsheetCntlr:ActionSheetController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){

    this.getCountries();
    this.getgroups();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.router.navigate(['/rv-res-list',{flag:1}]) ; 
      
  }); 
  
    }
    ionViewWillLeave() { 
      this.subscription.unsubscribe();
   }


   expandSettings(i){
    if(i==1){
      this.expand1=!this.expand1;
      this.expand2=false;
      this.expand3=false;
      this.expand4=false;
      this.expand5=false;
      this.expand6=false;
      this.expand7=false;
      this.expand8=false;
     
    }else if(i==2){
      this.expand1=false;
      this.expand2=!this.expand2;
      this.expand3=false;
      this.expand4=false;
      this.expand5=false;
      this.expand6=false;
      this.expand7=false;
      this.expand8=false;
      
    }else if(i==3){
      this.expand1=false;
      this.expand2=false;
      this.expand3=!this.expand3;
      this.expand4=false;
      this.expand5=false;
      this.expand6=false;
      this.expand7=false;
      this.expand8=false;
      
    }else if(i==4){
      this.expand1=false;
      this.expand2=false;
      this.expand3=false;
      this.expand4=!this.expand4;
      this.expand5=false;
      this.expand6=false;
      this.expand7=false;
      this.expand8=false;
      
    } else if(i==5){
      this.expand1=false;
      this.expand2=false;
      this.expand3=false;
      this.expand4=false;
      this.expand5=!this.expand5;
      this.expand6=false;
      this.expand7=false;
      this.expand8=false;
     
    }else if(i==6){
      this.expand1=false;
      this.expand2=false;
      this.expand3=false;
      this.expand4=false;
      this.expand5=false;
      this.expand6=!this.expand6;
      this.expand7=false;
      this.expand8=false;
     
    }
    else if(i==7){
      this.expand1=false;
      this.expand2=false;
      this.expand3=false;
      this.expand4=false;
      this.expand5=false;
      this.expand6=false;
      this.expand7=!this.expand7;
      this.expand8=false;
      
    }
    else if(i==8){
      this.expand1=false;
      this.expand2=false;
      this.expand3=false;
      this.expand4=false;
      this.expand5=false;
      this.expand6=false;
      this.expand7=false;
      this.expand8=!this.expand8;
     
    }
  }

  async getCountries(){
    let headers=await this.config.getHeader();;
    let url=this.config.domain_url+'countries'
    this.http.get(url,{headers}).subscribe((res:any)=>{
     
      this.countries=res.data;
    })
  }
  async getgroups(){
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'all_groups';
let headers=await this.config.getHeader();
this.http.get(url,{headers}).subscribe((res:any)=>{
  
  this.groups=res.data
  
})
  }

  async save(){
    const bid = await this.storage.get("BRANCH");
    const uid = await this.storage.get("USER_ID");
    const cid = await this.storage.get("COMPANY_ID");
   
      let url=this.config.domain_url+'rv_consumer_store';
    let headers=await this.config.getHeader();;
    console.log("dat:",url,headers);
    let share_photo,share_dob,share_phone,share_details,share_email=0
    if(this.share_photo){
      share_photo=1
    }
    if(this.share_dob){
      share_dob=1
    }
    if(this.share_phone){
      share_phone=1
    }
    if(this.share_details){
      share_details=1
    }
    if(this.share_email){
      share_email=1
    }
    let body;
    body={
      
      // username:
      email:this.email,
      // room_id:
      // preferred_name:
      likes:this.likes,
      dislikes:this.dislikes,
      interests:this.interests,
      // password:this.newPassword
      needs:this.needs,
      // communication_email:
      fname:this.fname,
      lname:this.lname,
      company_id:cid,
      phone:this.phone,
      profile_pic:null,
      created_by:uid,
      mobile:this.mobile,
      no_email:0,
      branch:bid,
      dob:moment(this.dob).format('YYYY-MM-DD'),
      description:this.description,
      consent_to_tag:share_photo,
      gender:this.gender,
      consumer_type:3,
      share_details:share_details,
      do_not_share_dob:share_dob,
      do_not_share_phone:share_phone,
      do_not_share_email:share_email,
      // title:
      // address:
      suburb:this.suburb,
      state:this.state,
      country:this.country,
      marital_status:this.marital_status,
      // mental_status:2
      religion:this.religion,
      interpreter_required:this.interpreter,
      car_registration:this.car_registration,
      // prox_number:
      key_tag:this.key_tag,
      // evacuation_place:
      medicare_number:this.medicare_number,
      // ambulance_membership_number:,
      // pension_number:,
      // private_health_fund:
      // health_fund_number:
      // caveat:5434
      
      postcode:this.postcode,
      street_number:this.street_no,
      street_name:this.street_name,
      commnunity_position:this.position,
      birth_country:this.birth_country,
      gate_number:this.gate_no,
      key_safe:this.key_safe,
      additional_notes:this.additional_notes,
      group_id:this.group,
      has_pet:this.have_pet,
      pet_type:this.pet,
      policy_provided:1,
      medical_hisitory: this.medical_history,
      individual_reference:this.ind_ref_no,
      card_expiry:this.medical_card_expiry,
      private_insurance_provider:this.insurance_provider,
      membership_number:this.membership_no,
      ambulance_cover:this.ambulance_cover,
      hcp:this.hcp,
      hcp_level:this.hcp_level,
      hcp_provider:this.hcp_provider,
      // hcp_provider_external:
      additional_health_notes:this.additional_health_note,
      pension:this.pension,
      pension_type:this.pension_type,
      dva:this.DVA,
      dva_card_type:this.DVA_card,
      pension_card_expiry:this.pension_card_expiry,
      language:this.languages,
      // consumer_type:4,
      medical_condition:this.medical_conditions,
      // ad_date:




    }
   
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     if(res.status=='error'){
      this.presentAlert(res.message)
     }else{
      this.presentAlert('Profile created successfully.')
     
      this.router.navigate(['/rv-res-list']);
     }
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
  }




  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
  addLikeItem(){
   
    if(this.likes.length==0){
      this.likes.push({'value':''});
    }
    else if(this.likes.length>0&&this.likes[this.likes.length-1].value!=''){
        this.likes.push({'value':''});
     
    
    }else{
      this.presentAlert('Please fill the empty field.')
    }
  }
    removeLikeItem(item,idx){
    
      this.likes.splice(idx,1);
    }
    addDislikeItem(){
   
      if(this.dislikes.length==0){
        this.dislikes.push({'value':''});
      }
      else if(this.dislikes.length>0&&this.dislikes[this.dislikes.length-1].value!=''){
          this.dislikes.push({'value':''});
       
      
      }else{
        this.presentAlert('Please fill the empty field.')
      }
    }
      removeDislikeItem(item,idx){
      
        this.dislikes.splice(idx,1);
      }


      addInterestsItem(){
   
        if(this.interests.length==0){
          this.interests.push({'value':''});
        }
        else if(this.interests.length>0&&this.interests[this.interests.length-1].value!=''){
            this.interests.push({'value':''});
         
        
        }else{
          this.presentAlert('Please fill the empty field.')
        }
      }
        removeInterestseItem(item,idx){
        
          this.interests.splice(idx,1);
        }

        addNeedsItem(){
   
          if(this.needs.length==0){
            this.needs.push({'value':''});
          }
          else if(this.needs.length>0&&this.needs[this.needs.length-1].value!=''){
              this.needs.push({'value':''});
           
          
          }else{
            this.presentAlert('Please fill the empty field.')
          }
        }
          removeNeedsItem(item,idx){
          
            this.needs.splice(idx,1);
          }


          async addPetImage() {
            const actionSheet = await this.actionsheetCntlr.create({
              header: "Select Image source",
              buttons: [{
                text: 'Load from Library',
                handler: () => {
                  this.addImage();
                }
              },
              {
                text: 'Use Camera',
                handler: () => {
                  this.captureImage();
                }
              },
              {
                text: 'Cancel',
                // role: 'cancel'
              }
              ]
            });
            await actionSheet.present();
          }
          
          async captureImage(){
            
            // const options: CameraOptions = {
            //   quality: 100,
            //   sourceType: this.camera.PictureSourceType.CAMERA,
            //   destinationType: this.camera.DestinationType.DATA_URL,
            //   encodingType: this.camera.EncodingType.JPEG,
            //   mediaType: this.camera.MediaType.PICTURE
            // }
            // this.camera.getPicture(options).then((imageData) => {
            //   // imageData is either a base64 encoded string or a file URI
            //   // If it's base64 (DATA_URL):
            //   // let base64Image = 'data:image/jpeg;base64,' + imageData;
            
            //           this.uploadImage('data:image/jpeg;base64,' +imageData);
              
            // }, (err) => {
            //   // Handle error
            // });

            const image = await Camera.getPhoto({
              quality: 90,
              allowEditing: false,
              resultType: CameraResultType.Base64,
              correctOrientation:true,
              source:CameraSource.Camera
            });
          
            // image.webPath will contain a path that can be set as an image src.
            // You can access the original file using image.path, which can be
            // passed to the Filesystem API to read the raw data of the image,
            // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
            var imageUrl = image.base64String;
          
            // Can be set to the src of an image now
            let base64Image = 'data:image/jpeg;base64,' + imageUrl;
            console.log('image:',imageUrl);
           
            this.uploadImage(base64Image);
          }
          
         
          
          async addImage(){
          
            // let options;
            //   options={
            //   maximumImagesCount: 5,
            //   outputType: 1,
            //   quality:100
            // }
            // if(this.platform.is('ios')){
            //   options.disable_popover=true
            // }
            //   this.imagePicker.getPictures(options).then((results) => {
            //     for (var i = 0; i < results.length; i++) {
            //         // console.log('Image URI: ' + results[i]);
                    
            //         if((results[i]==='O')||results[i]==='K'){
            //           console.log("no img");
                      
            //           }else{
                     
            //           this.uploadImage('data:image/jpeg;base64,' +results[i]);
            //           }
            //     }
            //   }, (err) => { });
            const image = await Camera.pickImages({
              quality: 90,
              correctOrientation:true,
              limit:5
              
            });
          
          
            for (var i = 0; i < image.photos.length; i++) {
                    console.log('Image URI: ' + image.photos[i]);
                    
                    
                    const contents = await Filesystem.readFile({
                      path: image.photos[i].path
                    });
                    
                      // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
                      this.uploadImage('data:image/jpeg;base64,' + contents.data)
                    
                      
                 
                     
                }
          
           
            
            }

            async uploadImage(img){
              if(this.isLoading==false){
              this.showLoading();
              }
              let url=this.config.domain_url+'upload_file';
              let headers=await this.config.getHeader();;
              let body={file:img}
            console.log("body:",img);
            
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log("uploaded:",res);
                this.petImages.push(res.data);
                this.dismissLoader();
              },error=>{
                // this.imageResponse.slice(0,1);
                console.log(error);
                this.dismissLoader();
              })
            }

            async showLoading() {
              this.isLoading=true;
              const loading = await this.loadingCtrl.create({
                cssClass: 'custom-loading',
                // message: '<ion-img src="assets/loader.gif"></ion-img>',
                spinner: null,
                // duration: 3000
              });
              return await loading.present();
            }
            
            async dismissLoader() {
              this.isLoading=false;
                return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
            }
}
