import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvCreateResidentPageRoutingModule } from './rv-create-resident-routing.module';

import { RvCreateResidentPage } from './rv-create-resident.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvCreateResidentPageRoutingModule
  ],
  declarations: [RvCreateResidentPage]
})
export class RvCreateResidentPageModule {}
