import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvCreateResidentPage } from './rv-create-resident.page';

const routes: Routes = [
  {
    path: '',
    component: RvCreateResidentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvCreateResidentPageRoutingModule {}
