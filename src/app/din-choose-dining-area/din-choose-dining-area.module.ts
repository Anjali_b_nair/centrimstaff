import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseDiningAreaPageRoutingModule } from './din-choose-dining-area-routing.module';

import { DinChooseDiningAreaPage } from './din-choose-dining-area.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseDiningAreaPageRoutingModule
  ],
  declarations: [DinChooseDiningAreaPage]
})
export class DinChooseDiningAreaPageModule {}
