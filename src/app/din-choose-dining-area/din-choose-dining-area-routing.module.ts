import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseDiningAreaPage } from './din-choose-dining-area.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseDiningAreaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseDiningAreaPageRoutingModule {}
