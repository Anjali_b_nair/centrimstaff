import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseDiningAreaPage } from './din-choose-dining-area.page';

describe('DinChooseDiningAreaPage', () => {
  let component: DinChooseDiningAreaPage;
  let fixture: ComponentFixture<DinChooseDiningAreaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseDiningAreaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseDiningAreaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
