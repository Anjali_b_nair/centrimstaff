import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingP2PageRoutingModule } from './onboarding-p2-routing.module';

import { OnboardingP2Page } from './onboarding-p2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OnboardingP2PageRoutingModule
  ],
  declarations: [OnboardingP2Page]
})
export class OnboardingP2PageModule {}
