import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import * as momenttz from 'moment-timezone';
import { ActivatedRoute, Router } from '@angular/router';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { Subscription } from 'rxjs';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-new-status',
  templateUrl: './new-status.page.html',
  styleUrls: ['./new-status.page.scss'],
})
export class NewStatusPage implements OnInit {
image:any;
media:any[]=[];
description:any;
feeling:any;
post_type:any;
date:any=new Date();
name:any;
branch:any[]=[];
id:any;
user_id:any;
subscription:Subscription;
from:any;
  constructor(private actionsheetCntlr:ActionSheetController,private config:HttpConfigService,
   private http:HttpClient,private storage:Storage,private router:Router,
    private route:ActivatedRoute,private toastCntlr:ToastController,private loadingCtrl:LoadingController,
    private modalCntrl:ModalController,private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.media=[];
    this.branch=[];
    this.id=this.route.snapshot.paramMap.get('id');
    this.user_id=this.route.snapshot.paramMap.get('user_id');
    this.name=this.route.snapshot.paramMap.get('name');
    this.branch.push(this.route.snapshot.paramMap.get('branch'));
    this.from=this.route.snapshot.paramMap.get('from');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/resident-profile',{id:this.id,flag:this.from}],{replaceUrl:true})
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  select(res,i){
    this.feeling=res;
    this.post_type=i;
  }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          
          this.captureImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async pickImage(){
    
    
    
   
   
  

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:1
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.image='data:image/jpeg;base64,' + contents.data;
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  }

  async captureImage(){
   
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
   
    var imageUrl = image.base64String;
  
    
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.image=base64Image;
    this.uploadImage(base64Image);

  }

  async uploadImage(img){
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      // this.img.push(res.data);
      this.media.push(res.data);
      // console.log("uploaded:",this.img);
    })
  }
  removeImg(){
    this.media=[];
    this.image=null;
  }

back(){
  this.router.navigate(['/resident-profile',{id:this.id,flag:this.from}],{replaceUrl:true})
}
  async publish(){
    
      
     if(this.description==undefined || this.description==''|| this.description==null){
       this.presentAlert('Please enter a note')
     }else if(!this.post_type){
       this.presentAlert('Please select a smiley.')
     }
     else{
      this.showLoading();
   
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID")

          const tz=await this.storage.get('TIMEZONE')
          let url=this.config.domain_url+'story';
          let headers=await this.config.getHeader();;
          let body={
            story:{
              post_type:this.post_type,
              title:this.name+' feeling '+this.feeling,
              description:this.description,
              company_id:cid,
              activate_date:moment.utc(momenttz.tz(tz)).format('yyyy-MM-DD HH:mm:ss'),
              status:1,
              visible_type:4,
              created_by:uid,
              web_display:0,
              enable_notification:0,
              username:this.name,
            },
            
            categories:null,
            branches:this.branch,
            document:null,
            attendees:this.user_id,
            images:this.media,
           
          }
          console.log("body:",body);
          
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);

          if(res.status=='success'){
            this.dismissLoader();
            this.presentAlert("Status added successfully.");
            this.router.navigate(['/resident-profile',{id:this.id,flag:this.from}],{replaceUrl:true});
          }else{
            this.dismissLoader();
            this.presentAlert("Something went wrong. Please try again later.");
           
          }
          
        },error=>{
          this.dismissLoader();
          this.presentAlert("Something went wrong. Please try again later.");
         
        })
     

  }

    
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }


  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
     
      componentProps: {
       data:this.media,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }
}
