import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewStatusPage } from './new-status.page';

describe('NewStatusPage', () => {
  let component: NewStatusPage;
  let fixture: ComponentFixture<NewStatusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewStatusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
