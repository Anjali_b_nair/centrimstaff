import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewStatusPageRoutingModule } from './new-status-routing.module';

import { NewStatusPage } from './new-status.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewStatusPageRoutingModule
  ],
  declarations: [NewStatusPage]
})
export class NewStatusPageModule {}
