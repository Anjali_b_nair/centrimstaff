import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewStatusPage } from './new-status.page';

const routes: Routes = [
  {
    path: '',
    component: NewStatusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewStatusPageRoutingModule {}
