import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResidentNotesPageRoutingModule } from './rv-resident-notes-routing.module';

import { RvResidentNotesPage } from './rv-resident-notes.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvNoteOptionsComponent } from '../components/rv-note-options/rv-note-options.component';
import { RvAddAdditionalNoteComponent } from '../components/rv-add-additional-note/rv-add-additional-note.component';
import { RvNoteFilterComponent } from '../components/rv-note-filter/rv-note-filter.component';
import { RvNoteTypeComponent } from '../components/rv-note-type/rv-note-type.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResidentNotesPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvResidentNotesPage,
    RvNoteOptionsComponent,
    RvAddAdditionalNoteComponent,
    RvNoteFilterComponent,
    RvNoteTypeComponent
  ],
  entryComponents:[
    RvNoteOptionsComponent,
    RvAddAdditionalNoteComponent,
    RvNoteFilterComponent,
    RvNoteTypeComponent
  ]
})
export class RvResidentNotesPageModule {}
