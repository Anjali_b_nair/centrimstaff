import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResidentNotesPage } from './rv-resident-notes.page';

const routes: Routes = [
  {
    path: '',
    component: RvResidentNotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResidentNotesPageRoutingModule {}
