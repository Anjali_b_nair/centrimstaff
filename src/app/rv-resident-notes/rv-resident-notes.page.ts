import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { element } from 'protractor';
import { Subscription } from 'rxjs';
import { MaintenanceImageComponent } from '../components/maintenance-image/maintenance-image.component';
import { RvNoteFilterComponent } from '../components/rv-note-filter/rv-note-filter.component';
import { RvNoteOptionsComponent } from '../components/rv-note-options/rv-note-options.component';
import { RvNoteTypeComponent } from '../components/rv-note-type/rv-note-type.component';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-resident-notes',
  templateUrl: './rv-resident-notes.page.html',
  styleUrls: ['./rv-resident-notes.page.scss'],
})
export class RvResidentNotesPage implements OnInit {

  cid:any;
  id:any;
  notes:any=[];
  offset:any=0;
  details:any;
  terms: any;
  subscription:Subscription;
  date_filter:any='7';
  assignedTo:any=[];
  staff:any;
  sdate:any;
  edate:any;
  type:any=['1','2','3','4','5','8'];
  order:any='1';
  filter:any=0;
  tz:any;
    constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
      private http:HttpClient,private config:HttpConfigService,private storage:Storage,
      private popCntl:PopoverController,private modalCntl:ModalController,private iab: InAppBrowser,
      private dialog:SpinnerDialog) { }
  
    ngOnInit() {
    }
    async ionViewWillEnter(){
      this.notes=[];
      this.tz = await this.storage.get("TIMEZONE");
      this.cid=this.route.snapshot.paramMap.get('cid');
      this.id=this.route.snapshot.paramMap.get('id');
      this.details=this.route.snapshot.paramMap.get('details');
      this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD');
      this.getNote(false,'');
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
        this.back();
        
    }); 
    
    }
  
    back(){
      this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
    }

    async getNote(isFirstLoad, event){
      const bid = await this.storage.get("BRANCH");
      const cid = await this.storage.get("COMPANY_ID");
      let url=this.config.domain_url+'load_resident_notes';
      let headers=await this.config.getHeader();
      let body;
      body={
        user_id:this.id,
        limit:20,
        offset:this.offset
      }
      if(this.terms){
        body.search=this.terms
      }
      if(this.date_filter!=='7'){
        body.sdate=this.sdate;
        body.edate=this.edate;
        }
      if(this.filter==1){
        body.note_type=this.type;
        body.sort_order=parseInt(this.order);
        if(this.assignedTo.length){
          body.created_by=this.assignedTo[0]
        }
        
      }
      console.log(body,url,{headers});
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
        console.log('res:',res);
        let dateArray=Object.keys(res.data)
        // for(let i in res.data){
        //   this.notes.push({date:dateArray[i],notes:res.data[i]});
        // }
        console.log('dateArr:',dateArray);
        
        for (let i = 0; i < dateArray.length; i++) {
          console.log('not:',res.data[dateArray[i]]);
          const newArr1 = res.data[dateArray[i]].map(v => ({...v, expanded: false}))
          this.notes.push({date:dateArray[i],notes:newArr1});
        }
       
        console.log('notes:',this.notes);
       
        if (isFirstLoad)
        event.target.complete();
    
      this.offset=this.offset+20;
        
       },error=>{
         console.log(error);
        
       })
    }
  
    doInfinite(event) {
      this.getNote(true, event)
      }

    async createNote(){
     
        const modal = await this.modalCntl.create({
          component: RvNoteTypeComponent,
          cssClass:'rv-note-type-modal',
          componentProps: {
            
           
             
          },
          
          
        });
        modal.onDidDismiss().then((dataReturned)=>{
          if(dataReturned.data){
            this.router.navigate(['/rv-res-add-note',{cid:this.cid,id:this.id,details:this.details,type:dataReturned.data}],{replaceUrl:true})
          }
          
        })
        return await modal.present();
      
      // this.router.navigate(['/rv-res-add-note',{cid:this.cid,id:this.id,details:this.details}])
    }

    async options(ev,item,i){
      const popover = await this.popCntl.create({
        component:RvNoteOptionsComponent,
        event:ev,
        cssClass:'rv-note-options-pop',
        backdropDismiss:true,
        componentProps:{
          data:item,
          flag:i
  
        },
        
        
        
      });
      popover.onDidDismiss().then((dataReturned) => {
        console.log('data:',dataReturned);
        if(dataReturned.data!=undefined||dataReturned.data!=null){
       
       this.offset=0;
       this.notes=[];
       this.getNote(false,'');
        }
     
     
        
      });
      return await popover.present();
    }

    async noteFilter(){
      const modal = await this.modalCntl.create({
        component: RvNoteFilterComponent,
        cssClass:'full-width-modal',
        componentProps: {
          
         date_filter:this.date_filter,
        
         sdate:this.sdate,
         edate:this.edate,
         assignedTo:this.assignedTo,
         staff:this.staff,
         type:this.type,
         order:this.order
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
        if(dataReturned.data){
          this.filter=1;
          this.date_filter=dataReturned.data.date_filter;
         
          this.sdate=dataReturned.data.sdate;
          this.edate=dataReturned.data.edate;
          this.assignedTo=dataReturned.data.assignedTo;
          this.staff=dataReturned.data.staff;
          this.type=dataReturned.data.type;
          this.order=dataReturned.data.order;
          this.offset=0;
          this.notes=[];
          this.getNote(false,'');
        }
        
      })
      return await modal.present();
    }

    cancel() {
   
      this.terms = undefined;
      this.ionViewWillEnter();
    }
  
    async search(){
      this.offset=0;
      this.notes=[];
    this.getNote(false,'');
    }

    expandItem(item){
     
      if (item.expanded) {
        item.expanded = false;
      } else {
       
        this.notes.map(listItem => {
          listItem.notes.map(note=>{ 
            if(note==item){
            note.expanded = !note.expanded;
            }
          else {
            note.expanded = false;
          }
          return note;
          })
         
        });
      
      }
}

open(ev,img){
  if(img.includes('.jpg')||img.includes('.jpeg')||img.includes('.png')){
    this.showImages(ev,img);
  }else if(img.includes('.pdf')){
    this.showpdf(img);
  }else{
    let options:InAppBrowserOptions ={
      location:'yes',
hidenavigationbuttons:'yes',
    hideurlbar:'yes',
    zoom:'yes'
    }
     const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+img),'_blank',options);
        browser.on('loadstart').subscribe(() => {
      
          this.dialog.show();   
         
        }, err => {
          this.dialog.hide();
        })
      
        browser.on('loadstop').subscribe(()=>{
          this.dialog.hide();;
        }, err =>{
          this.dialog.hide();
        })
      
        browser.on('loaderror').subscribe(()=>{
          this.dialog.hide();
        }, err =>{
          this.dialog.hide();
        })
        
        browser.on('exit').subscribe(()=>{
          this.dialog.hide();
        }, err =>{
          this.dialog.hide();
        })
  }
}

async showImages(ev: any,img) {

  const popover = await this.popCntl.create({
    component: MaintenanceImageComponent,
    event: ev,
    componentProps:{
      data:img,
      type:1
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}

async showpdf(file){
  const modal = await this.modalCntl.create({
    component: ShowpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
ShowContent(a,b,idx,i){
  console.log(a+idx+i,b+idx+i);
  console.log('in:',idx,i,idx+i);
  if(a=='p1'||a=='p2'){
  var x = document.getElementById(a+idx);
  var y = document.getElementById(b+idx);
        x.style.display = 'none';
        y.style.display = 'block';
  }else{
    var x = document.getElementById(a+idx+i);
  var y = document.getElementById(b+idx+i);
  console.log('ele:',x,y)
        x.style.display = 'none';
        y.style.display = 'block';
  }
}
removeFilter(){
  this.filter=0;
  this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD');
      this.offset=0;
       this.notes=[];
      this.getNote(false,'');
}

formatDate(date,i){
  if(i==1){
  return moment.utc(date).tz(this.tz).format('DD MMM YYYY, hh:mm A')
  }else{
    return moment(date).format('DD MMM YYYY, hh:mm A')
  }
}
getDocName(file){
  let fileUri=(file).substring((file).lastIndexOf('/') + 1);
  
  let fileName;
  if(fileUri.substring(0,(fileUri).lastIndexOf('.')).length>15){
    fileName=fileUri.substring(0,8)+'...'+fileUri.substring((fileUri).lastIndexOf('.')-5,fileUri.length)
  }else{
    fileName=fileUri
  }
  
  
  
  return fileName;
}
}
