import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResidentNotesPage } from './rv-resident-notes.page';

describe('RvResidentNotesPage', () => {
  let component: RvResidentNotesPage;
  let fixture: ComponentFixture<RvResidentNotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResidentNotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResidentNotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
