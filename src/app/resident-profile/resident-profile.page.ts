import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, LoadingController, ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
@Component({
  selector: 'app-resident-profile',
  templateUrl: './resident-profile.page.html',
  styleUrls: ['./resident-profile.page.scss'],
})
export class ResidentProfilePage implements OnInit {

  id:any;
  consumer:any={};
  img:any;
  name:any;
  branch:any;
  room:any;
  user_id:any;
  branch_id:any;
  subscription:Subscription;
  wing:any;
  flag:any;

  hours:any;
  meals:any;
  visitors:any;
  modules:any=[];

  edit:boolean=false;
  finance:boolean=false;
  dining:boolean=false;
  activity:boolean=false;
  message:boolean=false;
  story:boolean=false;
  cid:any;
    constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
      private router:Router,private objService:GetobjectService,private platform:Platform,
      private storage:Storage,private loadingCtrl:LoadingController,private actionsheetCntlr:ActionSheetController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.id=this.route.snapshot.paramMap.get('id');
    
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.showLoading();
    this.getModules();
    
     const bid=await this.storage.get("BRANCH");
     const cid=await this.storage.get("COMPANY_ID");
     (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.residents==1&&routes.includes('residents.edit')) {

        this.edit= true
      } else {

        this.edit = false;
        
      }
      if (res.main_permissions.finance==1&&routes.includes('finance.index')) {

        this.finance= true
      } else {

        this.finance = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('dietry_profile.index')) {

        this.dining= true
      } else {

        this.dining = false;
        
      }
      if (res.main_permissions.life_style==1&&routes.includes('story.create')) {

        this.story = true
      } else {

        this.story = false;
      }
      if (res.main_permissions.life_style==1&&routes.includes('activity.create')) {

        this.activity= true
      } else {
  
        this.activity = false;
        
      }
      if (res.main_permissions.communication==1&&routes.includes('message')) {

        this.message = true
      } else {

        this.message = false
      }
      // if (routes.includes('visitor.index')) {

      //   this.visit= true
      // } else {

      //   this.visit = false;
        
      // }
    })



    let url=this.config.domain_url+'consumer/'+this.id;
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers)
     this.http.get(url,{headers}).subscribe((res:any)=>{
     
      this.consumer=res.data;
      this.img=this.consumer.user.profile_pic
      this.name=this.consumer.user.name;
      this.room=this.consumer.room;
      if(this.consumer.wing){
      this.wing=this.consumer.wing.name;
      }
      this.user_id=this.consumer.user.user_id;
      this.branch=this.consumer.user.user_details.user_branch.name;
      this.branch_id=this.consumer.user.user_details.user_branch.id;
      console.log('con:',this.consumer);
      this.dismissLoader();
      this.getActivityHours();
      this.getMealsRefused();
      this.getVisitors();
    },error=>{
      console.log(error)
    })
    
   
    // this.getActivityHours();
    // this.getMealsRefused();
    // this.getVisitors();
   
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      if(this.flag==1){
     this.router.navigate(['/consumers',{flag:1}]) ; 
      }else{
       this.router.navigate(['/birthdays']) ;
      }
 }); 
 
   }
   ionViewWillLeave() { 
     this.subscription.unsubscribe();
  }
  about(){
    this.objService.setExtras(this.consumer);
    this.router.navigate(['/consumer-about',{flag:this.flag}])
  }
 
  contacts(){
   // this.objService.setExtras(this.consumer);
   this.router.navigate(['/consumer-contacts',{id:this.consumer.id,flag:this.flag}]);
 }
 overview(){
   this.router.navigate(['/consumer-overview',{id:this.user_id,cid:this.id,branch:this.branch_id,flag:this.flag}]);
 }
 gallery(){
   this.router.navigate(['/consumer-gallery',{id:this.user_id,cid:this.id,flag:this.flag}])
 }
 pettycash(){
   // this.objService.setExtras(this.consumer);
   this.router.navigate(['/petty-cash',{flag:this.flag,id:this.consumer.id}]);
 }
 
 report(){
   // this.objService.setExtras(this.consumer);
   this.router.navigate(['/consumer-report',{id:this.consumer.user.user_id,cid:this.consumer.id,flag:this.flag}])
 }
 
 
 async showLoading() {
   const loading = await this.loadingCtrl.create({
     cssClass: 'custom-loading',
     // message: '<ion-img src="assets/loader.gif"></ion-img>',
     spinner: null,
     // duration: 3000
   });
   return await loading.present();
 }
 
 async dismissLoader() {
     return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
 }
 
 back(){
   if(this.flag==1){
     this.router.navigate(['/consumers',{flag:1}]) ; 
      }else{
       this.router.navigate(['/birthdays']) ;
      }
 }
 newStatus(){
   this.router.navigate(['/new-status',{name:this.name,id:this.id,branch:this.branch_id,user_id:this.user_id,from:this.flag}],{replaceUrl:true})
 }

 async getActivityHours(){
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get("BRANCH")
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'consumerAttendHoursReport';
  
  let headers=await this.config.getHeader();
  let body={
    user_id:this.user_id,
    start:start,
    end:end,
    one_to_one:0
  }
  console.log('actbody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('hour:',data)
     if(data.total_hours=='0.00'){
      this.hours=0
     }else{
     this.hours=data.total_hours
     }
      },error=>{
          console.log(error);
   });
}
async getMealsRefused(){
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'dining_report_range';
  let headers=await this.config.getHeader();;
  // let headers=await this.config.getHeader();
  let body={
    user_id:this.user_id,
    start:start,
    end:end,
   
  }
  console.log('dinebody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('dine:',data)
     this.meals=data.refused
      },error=>{
          console.log(error);
   });
}
async getVisitors(){
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'visitor_report_range';
  let headers=await this.config.getHeader();;
  // let headers=await this.config.getHeader();
  let body={
    user_id:this.user_id,
    start:start,
    end:end,
  
  }
  console.log('visbody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('vis:',data)
     this.visitors=data.visitor_count
      },error=>{
          console.log(error);
   });
}
async getModules(){
  const cid=await this.storage.get('COMPANY_ID');
  let headers=await this.config.getHeader();;
        let murl = this.config.domain_url + 'get_modules/' + cid;
        this.http.get(murl,{headers}).subscribe((mod: any) => {
          // this.show = true;
          this.modules = mod
          console.log("modules:", this.modules);

        })
}


async quickLinks() {

  let button=[];
  
  if(this.story){
    button.push({
        icon:'globe-outline',
        text: 'New story',
        handler: () => {
          this.router.navigate(['/create-story',{flag:2,id:this.id,user_id:this.user_id,name:this.name,pic:this.img,from:this.flag}])
        }
      })
  }
  if(this.edit){
    button.push({
      icon:'leaf-outline',
      text: 'New status',
      handler: () => {
        this.router.navigate(['/new-status',{name:this.name,id:this.id,branch:this.branch_id,user_id:this.user_id,from:this.flag}],{replaceUrl:true});
      }
    })
  }
  if(this.message){
    button.push({
      icon:'mail-outline',
      text: 'New message',
      handler: () => {
        this.router.navigate(['/chatbox',{flag:2,user_two:this.user_id,pic:this.img,name:this.name,id:this.id,room:this.room,wing:this.wing,fam:null,from:this.flag}]);
      }
    })
  }
  if(this.activity){
    button.push({
      
        icon:'pulse-outline',
        text: 'Add activity',
        handler: () => {
          this.router.navigate(['/add-activity',{flag:2,id:this.user_id,cid:this.id,name:this.name,branch:this.branch_id,from:this.flag}])
        }
      })
  }
  button.push({
    icon: 'close',
    text: 'Cancel',
    role: 'cancel',
    handler: () => {
     
    }
  })
  
    const actionSheet = await this.actionsheetCntlr.create({
      // header: "Select Image source",
      cssClass:'activity-options',
      buttons:button
      
    });
    await actionSheet.present();
  }
}
