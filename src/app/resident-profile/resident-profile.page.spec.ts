import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResidentProfilePage } from './resident-profile.page';

describe('ResidentProfilePage', () => {
  let component: ResidentProfilePage;
  let fixture: ComponentFixture<ResidentProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidentProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResidentProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
