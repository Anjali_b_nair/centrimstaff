import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResidentProfilePageRoutingModule } from './resident-profile-routing.module';

import { ResidentProfilePage } from './resident-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResidentProfilePageRoutingModule
  ],
  declarations: [ResidentProfilePage]
})
export class ResidentProfilePageModule {}
