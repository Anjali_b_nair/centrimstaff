import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningMenuFiDetailsPage } from './dining-menu-fi-details.page';

const routes: Routes = [
  {
    path: '',
    component: DiningMenuFiDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningMenuFiDetailsPageRoutingModule {}
