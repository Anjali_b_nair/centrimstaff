import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningMenuFiDetailsPage } from './dining-menu-fi-details.page';

describe('DiningMenuFiDetailsPage', () => {
  let component: DiningMenuFiDetailsPage;
  let fixture: ComponentFixture<DiningMenuFiDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningMenuFiDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningMenuFiDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
