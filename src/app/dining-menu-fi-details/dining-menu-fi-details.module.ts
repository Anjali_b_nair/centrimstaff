import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningMenuFiDetailsPageRoutingModule } from './dining-menu-fi-details-routing.module';

import { DiningMenuFiDetailsPage } from './dining-menu-fi-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningMenuFiDetailsPageRoutingModule
  ],
  declarations: [DiningMenuFiDetailsPage]
})
export class DiningMenuFiDetailsPageModule {}
