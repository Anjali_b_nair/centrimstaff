import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dining-menu-fi-details',
  templateUrl: './dining-menu-fi-details.page.html',
  styleUrls: ['./dining-menu-fi-details.page.scss'],
})
export class DiningMenuFiDetailsPage implements OnInit {
  subscription:Subscription;
  menu:any;
  date:any;
  item:any;
  ser_time:any;
  flag:any;
  constructor(private route:ActivatedRoute,private router:Router,private platform:Platform,
    private config:HttpConfigService,private http:HttpClient) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    let menu=this.route.snapshot.paramMap.get('menu')
      this.menu=JSON.parse(menu);
      this.date=this.route.snapshot.paramMap.get('date');
      this.flag=this.route.snapshot.paramMap.get('flag');
      this.ser_time=this.route.snapshot.paramMap.get('ser_time');
    let item=this.route.snapshot.paramMap.get('item');
    if(this.flag==2){
      this.getItemDetails(JSON.parse(item).id);
    }else{
    this.item=JSON.parse(item)
    }
    console.log('item:',this.item)
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
async getItemDetails(id){
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_item_details/'+id;
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log('fetchedDetails:',res);
    this.item=res.data
  })
}

        back(){
          this.router.navigate(['/dining-menu-listing',{menu:JSON.stringify(this.menu),date:this.date}])
        }
}
