import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-menu-booking',
  templateUrl: './menu-booking.page.html',
  styleUrls: ['./menu-booking.page.scss'],
})
export class MenuBookingPage implements OnInit {
  modules:any[]=[];
  subscription:Subscription;
  call:boolean=false;
  visit:boolean=false;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private router:Router,private platform:Platform) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    let headers=await this.config.getHeader();
    const cid=await this.storage.get("COMPANY_ID");

      let murl=this.config.domain_url+'get_modules/'+cid;
      this.http.get(murl,{headers}).subscribe((mod:any)=>{
        this.modules=mod
        console.log("modules:",this.modules);
        
      });
      (await this.config.getUserPermission()).subscribe((res: any) => {
        console.log('permissions:', res);
        let routes = res.user_routes;
        if (res.main_permissions.visitors==1&&routes.includes('visitor_booking.index')) {
  
          this.visit= true
        } else {
  
          this.visit = false;
          
        }
        if (res.main_permissions.communication==1&&routes.includes('call_booking.index')) {
  
          this.call= true
        } else {
  
          this.call = false;
          
        }
      })
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
    this.back()
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
  back(){
    this.router.navigate(['/menu'])
  }
}
