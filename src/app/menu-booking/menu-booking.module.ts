import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuBookingPageRoutingModule } from './menu-booking-routing.module';

import { MenuBookingPage } from './menu-booking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuBookingPageRoutingModule
  ],
  declarations: [MenuBookingPage]
})
export class MenuBookingPageModule {}
