import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuBookingPage } from './menu-booking.page';

const routes: Routes = [
  {
    path: '',
    component: MenuBookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuBookingPageRoutingModule {}
