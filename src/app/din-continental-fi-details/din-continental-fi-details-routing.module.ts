import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinContinentalFiDetailsPage } from './din-continental-fi-details.page';

const routes: Routes = [
  {
    path: '',
    component: DinContinentalFiDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinContinentalFiDetailsPageRoutingModule {}
