import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinContinentalFiDetailsPageRoutingModule } from './din-continental-fi-details-routing.module';

import { DinContinentalFiDetailsPage } from './din-continental-fi-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinContinentalFiDetailsPageRoutingModule
  ],
  declarations: [DinContinentalFiDetailsPage]
})
export class DinContinentalFiDetailsPageModule {}
