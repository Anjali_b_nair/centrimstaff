import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ActivityfilterComponent } from '../components/activityfilter/activityfilter.component';
import { SelectStaffComponent } from '../components/select-staff/select-staff.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.page.html',
  styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit {
  // @ViewChildren('slideWithNav') slideWithNav: IonSlides;
  // view:boolean=false;
  date:Date;
  current:Date=new Date();
  token:string;
  activity:any[]=[];
  week=[];
  day:string;
  year:any;
  month:any;
  numAct:any;
  act:boolean;
  currentDate:any=0;
  date_1:string;
  subscription:Subscription;
  utype:any;
  // sliderOne: any;
  // slidenum:any;
  // slideOptsOne = {
  //   initialSlide: 0,
  //   slidesPerView: this.checkScreen(),
    
  // };
      type:any;
      contentStyle ={top:'70px'};
      plat:boolean;
      wingArray:any=[];
      sel_wing:any;
      wing:any;

      staffArray:any[]=[];
      staff:any;
      staff_id:any;

      rvsettings:any;
      create:boolean=false;
      options:any;
      res_activity:boolean=false;
       customPopoverOptions = {
        header: 'Calendars',
       
      };
      limit_required:any;
payment_required:any;
  constructor(public popoverController:PopoverController,public http:HttpClient,public storage:Storage,private router:Router,
    private config:HttpConfigService,private platform:Platform,private loadingCtrl:LoadingController,private modalCntl:ModalController) { }

//   checkScreen(){
//     if(window.innerWidth>=700){
//         return 6;
//     }else{
//         return 3;
//     }
// }
//   //Move to Next slide
// slideNext(object, slideView) {
//   slideView.slideNext(500).then(() => {
//     this.checkIfNavDisabled(object, slideView);
//   });
// }

// //Move to previous slide
// slidePrev(object, slideView) {
//   slideView.slidePrev(500).then(() => {
//     this.checkIfNavDisabled(object, slideView);
//   });;
// }
//   SlideDidChange(object, slideView) {
//     this.checkIfNavDisabled(object, slideView);
//     object.isActive= true;
//   }
//   checkIfNavDisabled(object, slideView) {
//     this.checkisBeginning(object, slideView);
//     this.checkisEnd(object, slideView);
//   }
 
//   checkisBeginning(object, slideView) {
//     slideView.isBeginning().then((istrue) => {
//       object.isBeginningSlide = istrue;
//     });
//   }
//   checkisEnd(object, slideView) {
//     slideView.isEnd().then((istrue) => {
//       object.isEndSlide = istrue;
//     });
//   }
 
ngOnInit() {

  if(this.platform.is('android')){
    this.contentStyle.top='75px';
    this.plat=true;
    // this.actCal.margintop='350px';
  }else if(this.platform.is('ios')){
    this.contentStyle.top='85px';
    this.plat=false;
    // this.actCal.margintop='370px';
  }
 
  this.date=this.current;
  this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
//   var mon = new Array();
//   mon[0] = "January";
//   mon[1] = "February";
//   mon[2] = "March";
//   mon[3] = "April";
//   mon[4] = "May";
//   mon[5] = "June";
//   mon[6] = "July";
//   mon[7] = "August";
//   mon[8] = "September";
//   mon[9] = "October";
//   mon[10] = "November";
//   mon[11] = "December";
//   this.year=this.date.getFullYear();
//   this.month=mon[this.date.getMonth()];

  
  

//   let m=this.current.getMonth()+1
// // get current week
//   for (let i = 0; i <= 6; i++) {

//     // let first = this.current .getTime() - (7 * 24 * 60 * 60 * 1000)

//     let mon=m;
//     let first = this.current.getDate()+ i;
//     if(m==1||m==3||m==5||m==7||m==8||m==10||m==12){
//       if (first>31){
//         first=first-31;
//         mon=m+1
        
//       }
//     }else if(m==2){
//       if(first>28){
//         first=first-28;
//         mon=m+1
//       }
//     }else{
//       if(first>30){
//         first=first-30;
//         mon=m+1
//       }
//     }
//     let x= this.current.getDay()+i;
//    if(x>6){
//      x=x-7;
//    }
//       if(x==0){
//       this.day="Sun";
//     }
//     else if(x==1){
//       this.day='Mon';
//     }
//     else if(x==2){
//       this.day='Tue';
//     }
//     else if(x==3){
//       this.day='Wed';
//     }
//     else if(x==4){
//       this.day='Thu';
//     }
//     else if(x==5){
//       this.day='Fri';
//     }
//     else if(x==6){
//       this.day='Sat';
//     }
  

    
//     var con={'day':this.day,'date':first,'mon':mon};
    
//     this.week.push(con);
//     this.sliderOne =
//     {
//       isBeginningSlide: true,
//       isEndSlide: false,
//       isActive:false,
//       week:[]=this.week
//     };
//   }

}


async ionViewWillEnter(){
  this.options = {
    canBackwardsSelected: true
  }
  // const tabBar = document.getElementById('myTabBar');
  // tabBar.style.display="none";
  const rv=await this.storage.get('RVSETTINGS');
  this.rvsettings=rv
  if(rv==1){
    this.getRVsettings();
  }
  this.sel_wing=0;
  this.wing='0';
  this.getContent();
  this.getWing();
  
  (await this.config.getUserPermission()).subscribe((res: any) => {
    console.log('permissions:', res);
    let routes = res.user_routes;
    if (res.main_permissions.life_style==1&&routes.includes('activity.create')) {

      this.create= true
    } else {

      this.create = false;
      
    }
  })
  // this.getStaff();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 


  
}
// open filter
async filter(myEvent) {
  const popover = await this.popoverController.create({
    component: ActivityfilterComponent,
    event: myEvent,
    backdropDismiss:true,
    cssClass:'filterpop',
   
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    console.log("closedData:",dataReturned);
    
    if (dataReturned !== null) {
   
      this.type = dataReturned.data;
      
      console.log("sel_wing:",this.sel_wing);
      
      //alert('Modal Sent Data :'+ dataReturned);
      if(this.sel_wing==0){
      this.filterContent();
      }else{
        this.filterTypeWing();
      }
    
    }
  });
  return await popover.present();
}
// selecting date from calendar
onChange(dat) {
  
 
  this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
  let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
  this.date=new Date(d);
  console.log(dat," ",this.date);
  console.log("sel_wing:",this.sel_wing);
  if(this.sel_wing==0){
  this.getContent();
  }else{
    this.filterByWing(this.sel_wing);
  }
}

// switch between calendar and week view
// calendarView(){
//  this.view=!this.view;
// }


// setDate(item,index){
// //  let x=item.date+' '+this.current.toUTCString().slice(0,3)+this.current.toUTCString().slice(7);
// //  let y=item.date.toString();
// //  this.date=x;

// var setMonth = new Array();
// setMonth[1] = "January";
// setMonth[2] = "February";
// setMonth[3] = "March";
// setMonth[4] = "April";
// setMonth[5] = "May";
// setMonth[6] = "June";
// setMonth[7] = "July";
// setMonth[8] = "August";
// setMonth[9] = "September";
// setMonth[10] = "October";
// setMonth[11] = "November";
// setMonth[12] = "December";


//  this.date_1=this.current.getFullYear()+'-' + this.fixDigit(item.mon)+'-'+this.fixDigit(item.date);
//  this.month=setMonth[item.mon];
//   this.currentDate=index;
//   console.log("index:",this.currentDate);
//   console.log("sel_wing:",this.sel_wing);
//   if(this.sel_wing==0){
//  this.getContent();
//   }else{
//     this.filterByWing(this.sel_wing)
//   }

// }

//  method to fix month and date in two digits
fixDigit(val){
return val.toString().length === 1 ? "0" + val : val;
}

// fetch activity list
async getContent(){

    
  const data=await this.storage.get("USER_ID")
 console.log("date:",this.date_1);
const utype=await this.storage.get("USER_TYPE")
  this.utype=utype;
  const tok=await this.storage.get("TOKEN")
    let token=tok;
    const bid=await this.storage.get("BRANCH")
      console.log("branch:",bid);
      
      let branch=bid.toString();
let headers=await this.config.getHeader();
let res_act;
if(this.res_activity){
  res_act=1
}else{
  res_act=0
}
 let body;

 body={
   date:'daily',
   date_1: this.date_1,
   user_id:data,
   role_id:utype,
   res_activity:res_act
 }
//  if(!this.res_activity){
//   body.role_id=utype
//  }
 let url=this.config.domain_url+'all_activities';
//  let headers=await this.config.getHeader();
 console.log("head:",headers,body)
 this.http.post(url,body,{headers}).subscribe((res:any)=>{
   this.activity=res.data;
   this.activity.sort(this.compare);

   console.log("res:",res);
   console.log("acttt:",this.activity);
   

   this.numAct=this.activity.length;
   if(this.numAct==0){
     this.act=false;
   }else{
     this.act=true;
   }
 },error=>{
   console.log(error);
   
 })

}

//  view single activity
gotoDetails(id){
 this.router.navigate(['/activity-details',{act_id:id}]);
}

ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

compare(i,j){
  let c=i.activity_start_date.slice(0,10)+' '+i.start_time;
  let d= j.activity_start_date.slice(0,10)+' '+j.start_time;
  let a=new Date(c).getTime() - new Date(d).getTime();
  return a;
}
async filterContent(){
console.log("date:",this.date_1);

const utype=await this.storage.get("USER_TYPE")
        const data=await this.storage.get("USER_ID")
          const tok=await this.storage.get("TOKEN")
            let token=tok;
            const bid=await this.storage.get("BRANCH")


              let branch=bid.toString();
        let headers=await this.config.getHeader();
            
          let body;
          body={
            date:'daily',
            date_1: this.date_1,
            user_id:data,
            

          }
          if(this.type==11){
            body.one_to_one=1
          }else{
            body.type=this.type
          }
          if(!this.res_activity){
            body.role_id=utype
          }
          let url=this.config.domain_url+'all_activities';
          // let headers=await this.config.getHeader();
          console.log("head:",headers)
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            this.activity=res.data;
            this.activity.sort(this.compare);
            console.log("res:",res);

            this.numAct=this.activity.length;
            if(this.numAct==0){
              this.act=false;
            }else{
              this.act=true;
            }
          },error=>{
            console.log(error);
            
          })
       
}

async getWing(){
 this.wingArray=[];
this.wingArray.push({id:0,wing:'All'});
let headers=await this.config.getHeader();;
  const bid=await this.storage.get("COMPANY_ID")
    console.log("branch:",bid);
    let branch=bid.toString();
    let body={company_id:branch}
    console.log("body:",body);
    
    let url=this.config.domain_url+'get_calendar_types';
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("wing:",res);
      for(let i in res.data){
          // console.log("data:",res.data[i]);
          
        let obj={id:res.data[i].calendar_id,wing:res.data[i].calendar_types.type};
        this.wingArray.push(obj)
      
      }
      console.log("array:",this.wingArray);
      
      
    },error=>{
      console.log(error);
      
    })
  
}
changeWing(wing){
 if(wing==0){
   this.getContent();
   this.sel_wing=0;
   this.wing='0';
 }else{
   console.log("winggg:",wing);
   this.sel_wing=wing;
   this.wing=wing;
   this.filterByWing(wing)
 }
}


async filterByWing(wing){

    
  // const data=await this.storage.get("USER_ID")

 console.log("date:",this.date_1);

  // const tok=await this.storage.get("TOKEN")

  //   let token=tok;
    const bid=await this.storage.get("BRANCH")


      let branch=bid.toString();
let headers=await this.config.getHeader();
 let body={
   date:'daily',
   date_1: this.date_1,
   calendar:wing

 }
 let url=this.config.domain_url+'activity_by_calendar_type';
//  let headers=await this.config.getHeader();
 console.log("head:",headers)
 this.http.post(url,body,{headers}).subscribe((res:any)=>{
   this.activity=res.data;
   this.activity.sort(this.compare);
   console.log("res:",res);

   this.numAct=this.activity.length;
   if(this.numAct==0){
     this.act=false;
   }else{
     this.act=true;
   }
 },error=>{
   console.log(error);
   
 })

}


async filterTypeWing(){

    
  // const data=await this.storage.get("USER_ID")

 console.log("date:",this.date_1);

  const tok=await this.storage.get("TOKEN")

    let token=tok;
    const bid=await this.storage.get("BRANCH")


      let branch=bid.toString();
let headers=await this.config.getHeader();
 let body;
 body={
   date:'daily',
   date_1: this.date_1,
   calendar:this.sel_wing,
   staff:this.staff_id

 }
 if(this.type==11){
  body.one_to_one=1
}else{
  body.type=this.type
}
 let url=this.config.domain_url+'activity_by_calendar_type';
//  let headers=await this.config.getHeader();
 console.log("head:",headers)
 this.http.post(url,body,{headers}).subscribe((res:any)=>{
   this.activity=res.data;
   this.activity.sort(this.compare);
   console.log("res:",res);

   this.numAct=this.activity.length;
   if(this.numAct==0){
     this.act=false;
   }else{
     this.act=true;
   }
 },error=>{
   console.log(error);
   
 })

}
async showLoading() {
const loading = await this.loadingCtrl.create({
  cssClass: 'custom-loading',
  // message: '<ion-img src="assets/loader.gif"></ion-img>',
  spinner: null,
  duration: 3000
});
return await loading.present();
}

async dismissLoader() {
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

async getStaff(){
  
    const bid=await this.storage.get("BRANCH")

  
    
      this.staffArray=[];
 
      this.staffArray.push({user_id:'all',name:'All'})
  let url1=this.config.domain_url+'staff_viabranch';
  // let headers=await this.config.getHeader();
  // this.storage.ready().then(()=>{
    let headers=await this.config.getHeader();
    const utype=await this.storage.get("USER_TYPE")

      const uid=await this.storage.get("USER_ID")

    const cid=await this.storage.get("COMPANY_ID")

      let body={company_id:cid}
        this.http.post(url1,body,{headers}).subscribe((res:any)=>{
          console.log("arr:",res);
          res.data.forEach(element => {
            this.staffArray.push({user_id:element.user_id,name:element.name})
          });
          // if(utype==3){
          //   this.staff_id=uid;
          //   this.staffArray.forEach(ele=>{
          //     // console.log("el:",ele.id,this.wingId);
              
          //     if(ele.user_id==uid){
          //       this.staff=ele.name;
          //     }
          //   })
          // }else{
            this.staff='All';
            this.staff_id=0
          // }
        })
        
      
}
selectStaff(ev,staff){
  if(staff==0){
    this.getContent();
  }else{
  this.staffArray.forEach(element => {
    if(element.name==this.staff){
      this.staff_id=element.user_id;
      this.filterTypeWing();
    }
  
  });
}

}
async staffFilter(myEvent) {
  const popover = await this.modalCntl.create({
    component: SelectStaffComponent,
   
    backdropDismiss:true,
    cssClass:'select-staff-modal',
   componentProps:{id:this.staff_id}
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    console.log("closedData:",dataReturned);
    
    if (dataReturned !== null) {
      if(dataReturned.data==0){
        this.staff_id=undefined;
        this.getContent();
      }else{
        this.staff_id=dataReturned.data;
        this.filterTypeWing();
      }
    
    }
  });
  return await popover.present();
}


// async signupAlert(item){
//   const modal = await this.modalCntl.create({
//     component: ConfirmSignupComponent,
    
//     componentProps: {
     
      
        
//     }
//   });
//   modal.onDidDismiss().then(()=>{
    
//   })
//   return await modal.present();
// }
// signup(item){
//   let url=this.config.domain_url+'add_attendee';
//     let body={
//       activity_id:item.id,
//       user_id:this.cid,
//       action:0
//     }
//     this.http.post(url,body).subscribe((res:any)=>{
//       console.log(res);
//       if(res.status=='success'){
//         this.presentAlert('signed up successfully.')
//         this.router.navigate(['/activity-details',{act_id:item.id}])
//       }else{
//         this.presentAlert('Something went wrong. Please try again later.')
//       }
//     })
// }
// async presentAlert(mes) {
//   const alert = await this.toastCntlr.create({
//     message: mes,
//     cssClass:'toastStyle',
//     duration: 3000      
//   });
//   alert.present(); //update
// }

includeResidentActivity(){
  this.getContent();
}

async getRVsettings(){
  const bid=await this.storage.get("BRANCH");
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_rv_settings/'+bid
  this.http.get(url,{headers}).subscribe((res:any)=>{
    this.limit_required=res.data.limit_required;
    this.payment_required=res.data.payment_required;
    console.log('required:',this.limit_required,this.payment_required)
  })
}
}
