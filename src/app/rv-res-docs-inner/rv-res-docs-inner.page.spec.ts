import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResDocsInnerPage } from './rv-res-docs-inner.page';

describe('RvResDocsInnerPage', () => {
  let component: RvResDocsInnerPage;
  let fixture: ComponentFixture<RvResDocsInnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResDocsInnerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResDocsInnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
