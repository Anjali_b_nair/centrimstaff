import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResDocsInnerPageRoutingModule } from './rv-res-docs-inner-routing.module';

import { RvResDocsInnerPage } from './rv-res-docs-inner.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResDocsInnerPageRoutingModule
  ],
  declarations: [RvResDocsInnerPage]
})
export class RvResDocsInnerPageModule {}
