import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResDocsInnerPage } from './rv-res-docs-inner.page';

const routes: Routes = [
  {
    path: '',
    component: RvResDocsInnerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResDocsInnerPageRoutingModule {}
