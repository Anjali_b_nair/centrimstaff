import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { RvDocsOptionsComponent } from '../components/rv-docs-options/rv-docs-options.component';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-res-docs-inner',
  templateUrl: './rv-res-docs-inner.page.html',
  styleUrls: ['./rv-res-docs-inner.page.scss'],
})
export class RvResDocsInnerPage implements OnInit {
  cid:any;
  id:any;
  consumer:any;
  folder:any=[];
  files:any=[];
  subscription:Subscription;
  count:any;
  parent_id:any;
  
  contents:any=[];
  title:any;
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private popCntl:PopoverController,private iab:InAppBrowser,private dialog:SpinnerDialog,
    private modalCntl:ModalController,private storage:Storage,private http:HttpClient,private config:HttpConfigService) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.consumer=this.route.snapshot.paramMap.get('consumer');
    this.contents=JSON.parse(this.route.snapshot.paramMap.get('res'));
    this.count=this.route.snapshot.paramMap.get('count');
    // this.contents=JSON.parse(this.route.snapshot.paramMap.get('contents'));
    this.parent_id=this.contents[this.contents.length-1].id;
    this.title=this.contents[this.contents.length-1].title;
    this.getDocs();
    console.log('content:',this.contents);
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.back();
      
  }); 
  
  }

  back(){
    if(this.count==0){
    this.router.navigate(['/rv-res-documents',{cid:this.cid,consumer:this.consumer,id:this.id}]);
    }else{
      this.count--;
      this.contents.pop();
      this.router.navigate(['/rv-res-docs-inner',{count:this.count,id:this.id,cid:this.cid,res:JSON.stringify(this.contents),consumer:this.consumer}])
    }
  }

  openFolder(item){
    this.count++;
    this.contents.push({id:item.id,title:item.title});
    this.router.navigate(['/rv-res-docs-inner',{count:this.count,id:this.id,cid:this.cid,res:JSON.stringify(this.contents),consumer:this.consumer}])
  
  }

  async getDocs(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'resident_documents';
    let headers=await this.config.getHeader();
    let body={
      resident_user_id:this.id,
      parent_id:this.parent_id
    
    }
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('docs:',res);
      this.folder=res.folders;
      this.files=res.contents;
     
    })
  }

  async options(ev,item){
    const popover = await this.popCntl.create({
      component:RvDocsOptionsComponent,
      cssClass:'rv-doc-options-popover',
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:item,
       

      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data!=undefined||dataReturned.data!=null){
    
    
      }
   
   
      
    });
    return await popover.present();
  }

  open(item){
    if(item.type==0){
      let options:InAppBrowserOptions ={
        location:'yes',
  hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes'
      }
      if(item.content.includes('.pdf')){
        this.showpdf(item.content)
      }else{
      const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.content),'_blank',options);
      browser.on('loadstart').subscribe(() => {
    
        this.dialog.show();   
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
      }else{
        let options:InAppBrowserOptions ={
          location:'no',
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.content,'_blank',options);
      
      }

  }
  async showpdf(file){
    const modal = await this.modalCntl.create({
      component: ShowpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
  
}
