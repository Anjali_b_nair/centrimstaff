import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-cb-pick-date-time',
  templateUrl: './cb-pick-date-time.page.html',
  styleUrls: ['./cb-pick-date-time.page.scss'],
})
export class CbPickDateTimePage implements OnInit {
date:Date;
date_1:any;
today:any;
current:Date=new Date();
participants:any[]=[];
cid:any;
name:any;
pic:any;
duration:any;
ar_avl: Array<string>;
  slot:any=[];
  start:any;
  end:any;
  hours;
  minutes;
  not_available:any=[];
  subscription:Subscription;
  sel_slot:any;
  constructor(private objService:GetobjectService,private router:Router,private route:ActivatedRoute,private storage:Storage,
    private config:HttpConfigService,private http:HttpClient,private platform:Platform,private toastCntlr:ToastController) { }

  ngOnInit() {

    this.duration="30";
    this.date=this.current;
    
    this.today=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.getAvailableTimeSlots();
  }
  
  async ionViewWillEnter(){
      this.participants=this.objService.getExtras();
      this.date=this.current;
      this.sel_slot='';
      this.cid=this.route.snapshot.paramMap.get('cid');
      this.name=this.route.snapshot.paramMap.get('name');
      this.pic=this.route.snapshot.paramMap.get('pic');
      this.date_1=this.current.getFullYear()+'-' + this.fixDigit(this.current.getMonth() + 1)+'-'+this.fixDigit(this.current.getDate());
     
      
        const data=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")


  
          
          let headers=await this.config.getHeader();
          // let url= this.config.domain_url+'company_details/'+data;
          let url= this.config.domain_url+'show_branch_call_time/'+bid;
          let body={company_id:data}
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log("res:",res);
            this.start=res.branch.call_start_time;
            this.end=res.branch.call_end_time;
            this.getAvailableTimeSlots();
            
            // this.slot=this.timeArray(this.start,this.end,30);
          },error=>{
            console.log(error);
            
          })
        
      
      
      
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/cb-choose-consumer']) ;
      }); 
    
    }
    ionViewWillLeave() { 
      this.subscription.unsubscribe();
    }


  onChange(ev){
    console.log("cdate:",this.date);
    this.date_1=this.date;
    this.getAvailableTimeSlots();
    // this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
    // let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
    // this.date=new Date(d);
   
  }


  selectDuration(){
    console.log("dur:",this.duration);
    // this.slot=this.timeArray(this.start,this.end,30);
    this.getAvailableTimeSlots();
    // this.slot=this.timeArray(this.start,this.end,30);

    console.log("slots:",this.slot);
    
  }

  async getAvailableTimeSlots(){
    this.not_available=[];
    
      const data=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")

        let headers=await this.config.getHeader();
        let url= this.config.domain_url+'timeslot_notavailable';
        let d;
        if(this.duration=='30'){
          d=1
        }else{
          d=2
        }
        let body={
          company_id:data,
          date:this.date_1,
          duration:d,
          user_id:this.cid
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("res:",res)
          res.data.forEach(element => {
            console.log(element);
            let t={slots:element}
            this.not_available.push(t);
            
            
          });
          this.ar_avl= res.json_data
          // this.not_available.push(res.data));
          console.log("fff:",this.ar_avl)
          this.slot=this.timeArray(this.start,this.end,30);
        },error=>{
          console.log(error);
          
        })
     
  console.log("not:",this.not_available);
  // this.ar_avl= this.not_available.map(p => p.slots)
  
  
  
  }

  //  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}

timeArray(start, end,duration){
  console.log("start:",start,"end:",end);
  
  var start = start.split(":");
  var end = end.split(":");
duration= parseInt(duration);
  start = parseInt(start[0]) * 60 + parseInt(start[1]);
  end = parseInt(end[0]) * 60 + parseInt(end[1]);
console.log(start,end);

  var result = [];

  for (let time = start; time <= end; time+=duration){
      var slot=this.timeString(time);
      let disable;
     
          // alligator.includes("thick scales");
      console.log("arraaaa:",this.ar_avl,"sl:",slot)
      console.log("incl:",this.ar_avl.includes(slot));
      if(this.ar_avl.includes(slot)){
        disable=true;
      }else{
        disable=false;
      }

    result.push( {'slot':slot,'disable':disable});
    
  }
    console.log("result:",result);
    
  return result;
}
timeString(time){
this.hours = Math.floor(time / 60);
let h=Math.floor(time / 60);
 this.minutes = time % 60;
 if(this.hours>12){
  this.hours=this.hours-12 ;
}
if (this.hours < 10) {
   this.hours = "0" + this.hours; //optional
}

if (this.minutes < 10){
    this.minutes = "0" + this.minutes;
}

if(h>=12){
return this.hours + ":" + this.minutes +' PM';
}else{
return this.hours + ":" + this.minutes +' AM';
}
}



bookcall(item){
  let currenttime=this.current.getTime();
  let a=item.slot.split(' ');
  let t=a[0].split(':');
  let time;
  if(a[1]=='AM'){
  time=a[0]+':00'
  }else{
     time=(parseInt(a[0])+12)+':'+t[1]+':00'
  }
  let x=this.today+'T'+time
  let sel_slot=new Date(x);
  
  console.log("x:",x,"slot:",sel_slot.getTime(),"curr:",currenttime);
  
    if(item.disable){
      this.presentAlert('This time slot is not available.Please select another.');
    }else if(this.date_1==this.today && sel_slot.getTime()<currenttime){
      console.log("current date");
      this.presentAlert('Please choose a valid time slot.');
    }else{
      
      this.sel_slot=item.slot;
    }
  }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        cssClass:'toastStyle',
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }
  

continue(){
  if(this.sel_slot==undefined||this.sel_slot==''||this.sel_slot==null){
    this.presentAlert('Please choose a time slot.');
  }else{

  this.objService.setExtras(this.participants);
      
  console.log("dura:",this.duration);
  
  this.router.navigate(['/confirm-callbooking',{consumer:this.cid,date:this.date_1,slot:this.sel_slot,duration:this.duration,name:this.name,pic:this.pic}])
  }
}
}
