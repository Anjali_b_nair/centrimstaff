import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CbPickDateTimePageRoutingModule } from './cb-pick-date-time-routing.module';

import { CbPickDateTimePage } from './cb-pick-date-time.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CbPickDateTimePageRoutingModule,
    CalendarModule,
    ApplicationPipesModule
  ],
  declarations: [CbPickDateTimePage]
})
export class CbPickDateTimePageModule {}
