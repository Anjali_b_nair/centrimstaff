import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CbPickDateTimePage } from './cb-pick-date-time.page';

const routes: Routes = [
  {
    path: '',
    component: CbPickDateTimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CbPickDateTimePageRoutingModule {}
