import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CbPickDateTimePage } from './cb-pick-date-time.page';

describe('CbPickDateTimePage', () => {
  let component: CbPickDateTimePage;
  let fixture: ComponentFixture<CbPickDateTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbPickDateTimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CbPickDateTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
