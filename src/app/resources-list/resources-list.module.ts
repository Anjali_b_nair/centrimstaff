import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResourcesListPageRoutingModule } from './resources-list-routing.module';

import { ResourcesListPage } from './resources-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResourcesListPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ResourcesListPage]
})
export class ResourcesListPageModule {}
