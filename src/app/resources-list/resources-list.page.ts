import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-resources-list',
  templateUrl: './resources-list.page.html',
  styleUrls: ['./resources-list.page.scss'],
})
export class ResourcesListPage implements OnInit {
  resource:any=[];
  files:any=[];
  subscription:Subscription;
  hide:boolean=false;
  terms:any;
  type:any=0;
  constructor(private http:HttpClient,private router:Router,private config:HttpConfigService,private platform:Platform,
    private storage:Storage,private iab: InAppBrowser,private dialog:SpinnerDialog,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    
    this.terms='';
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.life_style==1&&(routes.includes('resource.edit')||routes.includes('resource.create')||routes.includes('resource.destroy'))) {

        this.type= 2
      } else {

        this.type = 0;
        
      }
      
      this.getContent();
    })
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
  });  
  
  }
  
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }

// fetch content
async getContent(){
  
    
      const bid=await this.storage.get("BRANCH")


        let branch=bid.toString();
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'resource?type='+this.type;
  // let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res,url);
      this.resource=res.resources.data;
     
      
    },error=>{
      console.log(error);
      
    })


//   
 
}
// open resource folder
openFolder(item){
  let res=[{id:item.id,title:item.title}]
  this.router.navigate(['/resources-details',{count:0,res:JSON.stringify(res)}])
}
cancel(){
  this.hide=false;
  this.terms=undefined;

}
async search(){
    if(this.terms){
    const bid=await this.storage.get("BRANCH")


    let branch=bid.toString();
let headers=await this.config.getHeader();
let url=this.config.domain_url+'resource_search';
// let headers=await this.config.getHeader();
let body;
body={
  type:this.type,
  search:this.terms
}

this.http.post(url,body,{headers}).subscribe((res:any)=>{
  console.log('search:',res,url);
  this.resource=res.resource_fldr;
  this.files=res.resource_files;
  
},error=>{
  console.log(error);
  
})
    }else{
      this.getContent();
      this.files=[];
    }
  }


  openDoc(item){
    if(item.url=="null"){
      // window.open(encodeURI(item.file),"_system","location=yes");
      let options:InAppBrowserOptions ={
        location:'yes',
  hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes'
      }
      if(item.file.includes('.pdf')){
        this.showpdf(item.file)
      }else{
      const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
      browser.on('loadstart').subscribe(() => {
    
        this.dialog.show();   
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
      }else{
        let options:InAppBrowserOptions ={
          location:'no',
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.url,'_blank',options);
      
      }


  }

  async showpdf(file){
    const modal = await this.modalCntl.create({
      component: ShowpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}
