import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Platform, LoadingController, ToastController, ActionSheetController, PopoverController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Subscription } from 'rxjs';
import { RvContactOptionsComponent } from '../components/rv-contact-options/rv-contact-options.component';
import { HttpConfigService } from '../services/http-config.service';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { RegisteredEmailWarningComponent } from '../components/registered-email-warning/registered-email-warning.component';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { DOCUMENT } from '@angular/common';
import { RvContactMoreInfoComponent } from '../components/rv-contact-more-info/rv-contact-more-info.component';
import { RvAddPetComponent } from '../components/rv-add-pet/rv-add-pet.component';
import { RvPetLinkResidentComponent } from '../components/rv-pet-link-resident/rv-pet-link-resident.component';
import { RvPetDetailsComponent } from '../components/rv-pet-details/rv-pet-details.component';
import { RvPetOptionsComponent } from '../components/rv-pet-options/rv-pet-options.component';
import { OpenImageComponent } from '../components/open-image/open-image.component';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { CountryListComponent } from '../components/country-list/country-list.component';
import { RvAddNewAllergyComponent } from '../components/rv-add-new-allergy/rv-add-new-allergy.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-rv-res-profile',
  templateUrl: './rv-res-profile.page.html',
  styleUrls: ['./rv-res-profile.page.scss'],
})
export class RvResProfilePage implements OnInit {
  cid:any;
  consumer:any;
  subscription:Subscription;
  expand1:boolean=true;
  expand2:boolean=false;
  expand3:boolean=false;
  expand4:boolean=false;
  expand5:boolean=false;
  expand6:boolean=false;
  expand7:boolean=false;
  expand8:boolean=false;
  expand9:boolean=false;
  expand10:boolean=false;
  expand11:boolean=false;
  expand12:boolean=false;

  title:any=null;
  fname:any=null;
  lname:any=null;
  p_name:any=null;
  username:any=null;
  mobile:any=null;
  phone:any=null;
  email:any=null;
  c_email:any=null;
  no_email:boolean=false;
  status:any;
  position:any='0';
  description:any;
  dob:any=null;
  gender:any;
  genderArray:any=[];
  birth_country:any=null;
  birth_country_name:any;
  countries:any[]=[];
  languages:any[]=[];
  likes:any=[];
  dislikes:any=[];
  interests:any=[];
  needs:any=[];
  groups:any[]=[];
  group:any[]=[];
  group_name:any[]=[];
  religion:any=null;
  marital_status:any=null;

  have_pet:any='0';
  pet:any=[];
  pet_policy:any='0';
  permission_to_enter:any;
  property_note:any=null;
  share_photo:boolean=false;
  share_dob:boolean=false;
  share_phone:boolean=false;
  share_details:boolean=false;
  share_email:boolean=false;

  interpreter:any='0';
  car_registration:any=null;
  gate_no:any=null;
  key_tag:any=null;
  key_safe:any=null;
  additional_notes:any=null;

  medical_conditions:any[]=[];
  medical_history:any=null;
  medicare_number:any=null;
  ind_ref_no:any=null;
  medical_card_expiry:any;
  insurance_provider:any=null;
  membership_no:any=null;
  ambulance_cover:any=null;
  hcp:any='0';
  hcp_level:any=null;
  hcp_provider:any=null;
  provider_name:any=null;
  additional_health_note:any=null;

  pension:any=null;
  pension_type:any=null;
  pension_card_expiry:any=null;
  DVA:any=null;
  DVA_card:any=null;

  street_no:any=null;
  street_name:any=null;
  suburb:any=null;
  postcode:any=null;
  country:any='1';
  state:any=null;

  caravan_space:any='0';
  allocation_no:any=null;

  contacts:any[]=[];
community_position:any=[]=[];
cposition:any;
  newPassword:any;
  confirmPassword:any;
  editProfile:boolean=false;
  petImages:any=[];
  allergies:any=[];
  med_allergies:any=[];
  medicalConditionArray:any=[];
  allergyArray:any=[];
  medical_allergy_array:any=[];
  isLoading:boolean=false;
  disabled:boolean=false;
  family_id:any=[];
  hiddenfrom:any;
  edit_profile:boolean=false;
  expandProfile:boolean=false;

  listener;
selectAllCheckBox: any;
checkBoxes: HTMLCollection;

customAlertOptions: any = {
  
  // header: 'Facility',
  // subHeader: 'Select All:',
  message: '<ion-checkbox id="selectAllContacts"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
profile_pic:any;

new_pets:any[]=[];

evacuation_plan:any;
evacuation_assistance:any;
evacuation_go_bag:any;
go_bag_loc:any;
pet_evacuation:any;
evacuation_notes:any;

con_pass_type:any='password';
new_pass_type:any='password';
passwordForm:FormGroup;
  constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
    private router:Router,private platform:Platform,private toastCtlr:ToastController,
    private storage:Storage,private loadingCtrl:LoadingController,private modalCntl:ModalController,
    private actionsheetCntlr:ActionSheetController,private popCntl:PopoverController,private callNumber:CallNumber,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) { }

  ngOnInit() {
    this.passwordForm=new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)]),
     
      confirmPassword : new FormControl('',[Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)])
    });
  }

  async ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.languages=[];
    
  
  this.likes=[];
  this.dislikes=[];
  this.interests=[];
  this.needs=[];
 
  this.group=[];
  this.group_name=[];
  this.family_id=[];
  this.petImages=[];
  this.new_pets=[];
  this.med_allergies=[];
  this.hiddenfrom=null;
    this.editProfile=false;
    this.expandProfile=false;
    this.getCountries();
    this.getgroups();
    this.getMedicalConditions();
    this.getcommunityPositions();
    this.getGender();
    this.showLoading();
    (await this.config.getUserPermission()).subscribe((res: any) => {
   
      let routes = res.user_routes;
      if (res.main_permissions.residents==1&&routes.includes('residents.edit')) {

        this.edit_profile= true
      } else {

        this.edit_profile = false;
        
      }
    })

    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get('COMPANY_ID');
    let url=this.config.domain_url+'consumer/'+this.cid;
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers)
     this.http.get(url,{headers}).subscribe((res:any)=>{
     
      this.consumer=res.data;
      if(this.consumer.title=='Mr'){
      this.title='0';
      }else if(this.consumer.title=='Mrs'){
        this.title='1';
      }else if(this.consumer.title=='Ms'){
        this.title='2';
      }else if(this.consumer.title=='Dr'){
        this.title='3';
      }else if(this.consumer.title=='Other') {
        this.title='4';
      }
      this.fname=this.consumer.fname;
      this.lname=this.consumer.lname;
      this.p_name=this.consumer.preferred_name;
      this.profile_pic=this.consumer.user.profile_pic;
      if(this.consumer.user.phone&&this.consumer.user.phone!=='61'){
        if(this.consumer.user.phone.substring(0,2)=='61'){
          let num=this.consumer.user.phone.slice(2);
          if(num.substring(0,1)=='0'){
            this.mobile=num;
          }else{
            this.mobile='0'+num;
          }
          
        }else{
      this.mobile=this.consumer.user.phone;
        }
      }
      if(this.consumer.user.mobile&&this.consumer.user.mobile!=='61'){
        if(this.consumer.user.mobile.substring(0,2)=='61'){
        let num=this.consumer.user.mobile.slice(2);
          if(num.substring(0,1)=='0'){
            this.phone=num;
          }else{
            this.phone='0'+num;
          }
          
        }else{
          this.phone=this.consumer.user.mobile;
        }
      
    }
      
      this.c_email=this.consumer.user.communication_email;
      if(this.consumer.user.no_email){
        this.no_email=true;
        this.username=this.consumer.user.user_name;
      }else{
        this.no_email=false;
        this.email=this.consumer.user.email;
      }
      this.status=this.consumer.user.status.toString();
      if(this.consumer.position_details){
        this.position=this.consumer.position_details.id.toString();
      }
      this.description=this.consumer.description;
      if(this.consumer.religion){
      this.religion=this.consumer.religion.toString();
      }
      if(this.consumer.consent_to_tag==0){
        this.share_photo=true;
      }else{
        this.share_photo=false;
      }
      if(this.consumer.do_not_share_dob==0){
        this.share_dob=true;
      }else{
        this.share_dob=false;
      }
      if(this.consumer.do_not_share_phone==0){
        this.share_phone=true;
      }else{
        this.share_phone=false;
      }
      if(this.consumer.do_not_share_email==0){
        this.share_email=true;
      }else{
        this.share_email=false;
      }
      if(this.consumer.share_details==0){
        this.share_details=true;
      }else{
        this.share_details=false;
      }

      if(this.consumer.dob){
      this.dob=new Date(this.consumer.dob).toISOString();
      }
      if(this.consumer.gender){
      this.gender=this.consumer.gender.gender_id.toString();
      }
      if(this.consumer.languages){
        this.consumer.languages.forEach(element => {
          this.languages.push(element.language)
        });
      }
      if(this.consumer.resident_group){
        this.consumer.resident_group.forEach(element => {
          this.group.push(element.group_id.toString());
          this.group_name.push(element.group_details.name);
        });
      }
      if(this.consumer.birth_country){
      this.birth_country=this.consumer.birth_country.toString();
      const idx=this.countries.map(x=>x.id).indexOf(parseInt(this.birth_country));
      if(idx>=0){
        this.birth_country_name=this.countries[idx].name;
      }
     
      
      }
      if(this.consumer.marital_status){
      this.marital_status=this.consumer.marital_status.toString();
      }
      if(this.consumer.interpreter_required){
      this.interpreter=this.consumer.interpreter_required.toString();
      }
      this.car_registration=this.consumer.car_registration;
      this.gate_no=this.consumer.gate_number;
      this.key_tag=this.consumer.key_tag;
      this.key_safe=this.consumer.key_safe;
      this.additional_notes=this.consumer.additional_notes;
      this.contacts=this.consumer.all_contacts;
      if(this.contacts&&this.contacts.length){
        this.contacts.forEach(element => {
          if(element.share_details==0){
            this.family_id.push(element.user_details.user_id.toString());
            if(!this.hiddenfrom){
            this.hiddenfrom=element.user_details.name
            }else{
              this.hiddenfrom=this.hiddenfrom+','+element.user_details.name
            }
          }
        });
        
        
      }
      if(this.consumer.pets){
        this.have_pet=this.consumer.pets.has_pet.toString();
        if(this.consumer.pets.policy_provided){
        this.pet_policy=this.consumer.pets.policy_provided.toString();
        }
        if(this.consumer.pets.pet_type){
        this.pet=this.consumer.pets.pet_type.split(/\, +/);
        }
      }

      if(this.consumer.petss&&this.consumer.petss.length){
        this.consumer.petss.forEach(element => {
          if(element.link_residents&&element.link_residents.length){
            const idx=element.link_residents.map(x=>x.user_id).indexOf(this.consumer.user.user_id);
      if(idx>=0){
        element.link_residents.splice(idx,1);
      }
          }
          this.new_pets.push({pet:element,expand:false})
        });
      }
      if(this.consumer.petimages&&this.consumer.petimages.length){
        this.consumer.petimages.forEach(element => {
          this.petImages.push(element.images)
        });
      }
      if(this.consumer.property_details&&this.consumer.property_details.contract_details){
        this.permission_to_enter=this.consumer.property_details.contract_details.permission_to_enter.toString();
        this.property_note=this.consumer.property_details.contract_details.other_notes;
      }
      if(this.consumer.medical_conditions){
        this.consumer.medical_conditions.forEach(element => {
          this.medical_conditions.push(element.condition_id.toString())
        });
      };
      if(this.consumer.health_care){
      this.medical_history=this.consumer.health_care.medical_history;
      this.medicare_number=this.consumer.health_care.medicare_number;
      this.ind_ref_no=this.consumer.health_care.individual_reference;
      this.medical_card_expiry=this.consumer.health_care.card_expiry;
      this.insurance_provider=this.consumer.health_care.private_insurance_provider;
      this.membership_no=this.consumer.health_care.membership_number;
      this.ambulance_cover=this.consumer.health_care.ambulance_cover;
      this.hcp=this.consumer.health_care.hcp.toString();
      if(this.consumer.health_care.hcp_level)
      this.hcp_level=this.consumer.health_care.hcp_level.toString();
      if(this.consumer.health_care.hcp_provider)
      this.hcp_provider=this.consumer.health_care.hcp_provider.toString();
      this.provider_name=this.consumer.health_care.hcp_provider_external;
      this.additional_health_note=this.consumer.health_care.additional_health_notes;
      }
      if(this.consumer.resallergy&&this.consumer.resallergy.length){
        this.consumer.resallergy.forEach(element => {
          this.allergies.push(element.allergy_id.toString())
        });

      }

      if(this.consumer.resmedallergy&&this.consumer.resmedallergy.length){
        this.consumer.resmedallergy.forEach(element => {
          this.med_allergies.push(element.allergy_id.toString())
        });

      }
    
      if(this.consumer.pension){
        this.pension=this.consumer.pension.pension;
        this.pension_type=this.consumer.pension.pension_type;
        this.pension_card_expiry=this.consumer.pension.card_expiry;
        this.DVA=this.consumer.pension.dva;
        this.DVA_card=this.consumer.pension.dva_card_type;
      }

      this.street_no=this.consumer.street_number;
      this.street_name=this.consumer.street_name;
      this.suburb=this.consumer.suburb;
      this.postcode=this.consumer.postcode;
      if(this.consumer.country){
      this.country=this.consumer.country.toString();
      }
      this.state=this.consumer.state;

      if(this.consumer.caravan_space){
      this.caravan_space=this.consumer.caravan_space.toString();
      }
      this.allocation_no=this.consumer.allocation_number;

      if(this.consumer.likes){
        let arr=this.consumer.likes.split(/\, +/);
        arr.forEach(element => {
          this.likes.push({value:element})
        });
      }
      if(this.consumer.dislikes){
        let arr=this.consumer.dislikes.split(/\, +/);
        arr.forEach(element => {
          this.dislikes.push({value:element})
        });
      }

      if(this.consumer.interests){
        let arr=this.consumer.interests.split(/\, +/);
        arr.forEach(element => {
          this.interests.push({value:element})
        });
      }

      if(this.consumer.needs){
        let arr=this.consumer.needs.split(/\, +/);
        arr.forEach(element => {
          this.needs.push({value:element})
        });
      }

      if(this.consumer.position_details){
        this.cposition=this.consumer.position_details.name;

      }

      if(this.consumer.evacuation){
        if(this.consumer.evacuation.evacuation_plan_provided)
        this.evacuation_plan=this.consumer.evacuation.evacuation_plan_provided.toString();
      if(this.consumer.evacuation.evacuation_assistance)
        this.evacuation_assistance=this.consumer.evacuation.evacuation_assistance.toString();
      if(this.consumer.evacuation.evacuation_go_bag)
        this.evacuation_go_bag=this.consumer.evacuation.evacuation_go_bag.toString();
        this.go_bag_loc=this.consumer.evacuation.go_bag_location;
        if(this.consumer.evacuation.pet_evacuation_required)
        this.pet_evacuation=this.consumer.evacuation.pet_evacuation_required.toString();
        this.evacuation_notes=this.consumer.evacuation.evacuation_notes;
      }

      console.log('con:',this.consumer,this.dob);
      this.dismissLoader();
      
    },error=>{
      console.log(error)
    })
    
   
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
    //  this.router.navigate(['/rv-res-list',{flag:1}]) ; 
    this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
     
 }); 
 
   }
   ionViewWillLeave() { 
     this.subscription.unsubscribe();
  }

  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  getCompanian(residents,user){
   
    let companian;
    const idx = residents.map(x => x.user_id).indexOf(user);
    
      if(idx<=0){
        
        companian=residents[idx+1].user_details.name
      }else{
        companian=residents[0].user_details.name;
      }
      return companian;
    }
    
    expandSettings(i){
      if(i==1){
        this.expand1=!this.expand1;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }else if(i==2){
        this.expand1=false;
        this.expand2=!this.expand2;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
        
      }else if(i==3){
        this.expand1=false;
        this.expand2=false;
        this.expand3=!this.expand3;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }else if(i==4){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=!this.expand4;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      } else if(i==5){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=!this.expand5;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }else if(i==6){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=!this.expand6;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }
      else if(i==7){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=!this.expand7;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }
      else if(i==8){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=!this.expand8;
        this.expand9=false;
        this.expand10=false;
        this.expand11=false;
      }
      else if(i==9){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=!this.expand9;
        this.expand10=false;
        this.expand11=false;
      }
      else if(i==10){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=!this.expand10;
        this.expand11=false;
      }else if(i==11){
        this.expand1=false;
        this.expand2=false;
        this.expand3=false;
        this.expand4=false;
        this.expand5=false;
        this.expand6=false;
        this.expand7=false;
        this.expand8=false;
        this.expand9=false;
        this.expand10=false;
        this.expand11=!this.expand11;
      }
     
    }

    async getCountries(){
      let headers=await this.config.getHeader();
      let url=this.config.domain_url+'countries'
      this.http.get(url,{headers}).subscribe((res:any)=>{
       console.log('countries:',this.countries);
       
        this.countries=res.data;
        const idx=this.countries.map(x=>x.id).indexOf(parseInt(this.birth_country));
        if(idx>=0){
          this.birth_country_name=this.countries[idx].name;
        }
        
      })
    }
    async getgroups(){
      const bid=await this.storage.get("BRANCH");
      const cid=await this.storage.get("COMPANY_ID");
      let url=this.config.domain_url+'all_groups';
  let headers=await this.config.getHeader();
  this.http.get(url,{headers}).subscribe((res:any)=>{
    
    this.groups=res.data
    
  })
    }

    async save(){
      var pass=new RegExp('(?=[A-Za-z0-9!@#$%^&*]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).*$');
      
      if(!this.fname||!this.lname||!this.dob||(!this.no_email&&!this.email)||(this.no_email&&!this.username)||!this.gender){
        this.presentAlert('Please fill the required fields');

      // }else if(this.newPassword&&!pass.test(this.newPassword)){
      //   this.presentAlert('Password must contain atleast one uppercase, lowercase, digit and symbol.')
      }else if(this.newPassword&&!this.confirmPassword){
        this.presentAlert('Please verify your password.')
      }else if(this.newPassword&&this.newPassword!==this.confirmPassword){
        this.presentAlert("Confirmed password doesn't match with new password.");
      }else if(this.newPassword&&this.passwordForm.invalid){
        this.presentAlert("Incorrect password format.")
      }else {
        this.disabled=true;
      const bid = await this.storage.get("BRANCH");
      const uid = await this.storage.get("USER_ID");
      const cid = await this.storage.get("COMPANY_ID");
     
        let url=this.config.domain_url+'update_resident';
      let headers=await this.config.getHeader();
    
      let share_photo=0;
      let share_dob=0;
      let share_phone=0
      let share_details=0;
      let share_email=0;
      let no_email=0;
      if(!this.share_photo){
        share_photo=1
      }
      if(!this.share_dob){
        share_dob=1
      }
      if(!this.share_phone){
        share_phone=1
      }
      if(!this.share_details){
        share_details=1
      }
      if(!this.share_email){
        share_email=1
      }
      if(this.no_email){
        no_email=1
        this.email=this.consumer.user.email;
      }

      let pet_type=null;
      if(this.pet.length){
        pet_type=this.pet[0];
        this.pet.forEach((element,index) => {
          if(index!==0){
          
          pet_type=pet_type+', '+element
          }
        });
      }

      let likes=null;
      if(this.likes.length&&this.likes[0].value!=''){
        likes=this.likes[0].value;
        this.likes.forEach((element,index) => {
          if(index!==0){
          if(element.value!='')
          likes=likes+', '+element.value
          }
        });
      }

      let dislikes=null;
      if(this.dislikes.length&&this.dislikes[0].value!=''){
        dislikes=this.dislikes[0].value;
        this.dislikes.forEach((element,index) => {
          if(index!==0){
          if(element.value!='')
          dislikes=dislikes+', '+element.value
          }
        });
      }
      let needs=null;
      if(this.needs.length&&this.needs[0].value!=''){
        needs=this.needs[0].value;
        this.needs.forEach((element,index) => {
          if(index!==0){
          if(element.value!='')
          needs=needs+', '+element.value
          }
        });
      }
      let interests=null;
      if(this.interests.length&&this.interests[0].value!=''){
        interests=this.interests[0].value;
        this.interests.forEach((element,index) => {
          if(index!==0){
          if(element.value!='')
          interests=interests+', '+element.value
          }
        });
      }
      let allergy = this.allergies.filter((c, index) => {
        return this.allergies.indexOf(c) === index;
    });
    let med_allergy = this.med_allergies.filter((c, index) => {
      return this.med_allergies.indexOf(c) === index;
  });
    let condition = this.medical_conditions.filter((c, index) => {
      return this.medical_conditions.indexOf(c) === index;
  });
  let group = this.group.filter((c, index) => {
    return this.group.indexOf(c) === index;
});
      let pension_exp=null;
      let med_exp=null;
      if(this.pension_card_expiry){
        pension_exp=moment(this.pension_card_expiry).format('YYYY-MM-DD')
      }
      if(this.medical_card_expiry){
        med_exp=moment(this.medical_card_expiry).format('YYYY-MM-DD')
      }
      let title;
      if(this.title=='0'){
        title='Mr';
        }else if(this.title=='1'){
          title='Mrs';
        }else if(this.title=='2'){
          title='Ms';
        }else if(this.title=='3'){
         title='Dr';
        }else {
          title='Other';
        }
        let birth_country=null;
        let marital_status=null;
        if(this.birth_country){
          birth_country=parseInt(this.birth_country)
        }
        if(this.marital_status){
          marital_status=parseInt(this.marital_status)
        }
      let body;
      body={
        user_id:this.consumer.user.user_id,
        user_name:this.username,
        email:this.email,
        communication_email:this.c_email,
        likes:likes,
        dislikes:dislikes,
        interests:interests,
        // password:this.newPassword
        needs:needs,
        fname:this.fname,
        lname:this.lname,
        preferred_name:this.p_name,
        company_id:cid,
        phone:this.mobile,
        profile_pic:this.profile_pic,
        created_by:uid,
        mobile:this.phone,
        no_email:no_email,
        branch_id:bid,
        branch:bid,
        dob:moment(this.dob).format('YYYY-MM-DD'),
        description:this.description,
        consent_to_tag:share_photo,
        gender:parseInt(this.gender),
        consumer_type:this.consumer.consumer_type,
        share_details:share_details,
        do_not_share_dob:share_dob,
        do_not_share_phone:share_phone,
        do_not_share_email:share_email,
        title:title,
        // address:
        suburb:this.suburb,
        state:this.state,
        country:parseInt(this.country),
        marital_status:marital_status,
        // mental_status:2
        religion:parseInt(this.religion),
        interpreter_required:parseInt(this.interpreter),
        car_registration:this.car_registration,
        // prox_number:
        key_tag:this.key_tag,
        // evacuation_place:
        medicare_number:this.medicare_number,
        // ambulance_membership_number:,
        // pension_number:,
        // private_health_fund:
        // health_fund_number:
        // caveat:5434
        allocation_number:this.allocation_no,
        postcode:this.postcode,
        street_number:this.street_no,
        street_name:this.street_name,
        commnunity_position:parseInt(this.position),
        birth_country:birth_country,
        gate_number:this.gate_no,
        key_safe:this.key_safe,
        additional_notes:this.additional_notes,
        group_id:group,
        has_pet:parseInt(this.have_pet),
        pet_policy_provided:this.pet_policy,
        pet_type:pet_type,
        pet_img:this.petImages,
        policy_provided:this.pet_policy,
        medical_hisitory: this.medical_history,
        individual_reference:this.ind_ref_no,
        card_expiry:med_exp,
        private_insurance_provider:this.insurance_provider,
        membership_number:this.membership_no,
        ambulance_cover:this.ambulance_cover,
        hcp:parseInt(this.hcp),
        hcp_level:parseInt(this.hcp_level),
        hcp_provider:parseInt(this.hcp_provider),
        res_allergy:allergy,
        res_med_allergy:med_allergy,
        hcp_provider_external:this.provider_name,
        additional_health_notes:this.additional_health_note,
        pension:this.pension,
        pension_type:this.pension_type,
        dva:this.DVA,
        dva_card_type:this.DVA_card,
        pension_card_expiry:pension_exp,
        language:this.languages,
        status:parseInt(this.status),
        medical_condition:condition,
        evacuation_plan_provided:this.evacuation_plan,
        evacuation_assistance:this.evacuation_assistance,
        evacuation_go_bag:this.evacuation_go_bag,
        go_bag_location:this.go_bag_loc,
        pet_evacuation_required:this.pet_evacuation,
        evacuation_notes:this.evacuation_notes
        // ad_date:
      }
      if(this.share_details){
        body.family_ids=this.family_id
      }
      if(this.consumer.property_details&&this.consumer.property_details.contract_details){
        body.contract_id=this.consumer.property_details.contract_details.id;
        body.permission_to_enter=this.permission_to_enter.toString();
        body.other_notes=this.property_note
      }
      if(this.newPassword){
        body.password=this.newPassword
      }
      console.log('body:',body);
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
       
       console.log(res);

       if(res.status=='success'){
        this.newPassword=undefined;
        this.confirmPassword=undefined;
       this.disabled=false;
        this.presentAlert('Profile edited successfully.')
        this.expand10=false;
        this.ionViewWillEnter();
       }else{
        this.disabled=false;
        this.expand10=false;
        if(res.message=='This email address is already registered'){
            this.emailAlreadyRegistered();
        }else{
        this.presentAlert(res.message)
        }
       }
      },error=>{
        console.log(error);
        this.disabled=false;
        
        this.presentAlert('Something went wrong. Please try again later.')
      })
    }
    }

    edit(){
      this.editProfile=!this.editProfile
    }


    async presentAlert(mes) {
      const alert = await this.toastCtlr.create({
        message: mes,
        cssClass: 'toastStyle',
        duration: 3000,
        position:'top'
      });
      alert.present(); //update
    }
    addLikeItem(){
     
      if(this.likes.length==0){
        this.likes.push({'value':''});
      }
      else if(this.likes.length>0&&this.likes[this.likes.length-1].value!=''){
          this.likes.push({'value':''});
       
      
      }else{
        this.presentAlert('Please fill the empty field.')
      }
    }
      removeLikeItem(item,idx){
      
        this.likes.splice(idx,1);
      }
      addDislikeItem(){
     
        if(this.dislikes.length==0){
          this.dislikes.push({'value':''});
        }
        else if(this.dislikes.length>0&&this.dislikes[this.dislikes.length-1].value!=''){
            this.dislikes.push({'value':''});
         
        
        }else{
          this.presentAlert('Please fill the empty field.')
        }
      }
        removeDislikeItem(item,idx){
        
          this.dislikes.splice(idx,1);
        }


        addInterestsItem(){
     
          if(this.interests.length==0){
            this.interests.push({'value':''});
          }
          else if(this.interests.length>0&&this.interests[this.interests.length-1].value!=''){
              this.interests.push({'value':''});
           
          
          }else{
            this.presentAlert('Please fill the empty field.')
          }
        }
        
          removeInterestsItem(item,idx){
          
            this.interests.splice(idx,1);
          }

          addNeedsItem(){
     
            if(this.needs.length==0){
              this.needs.push({'value':''});
            }
            else if(this.needs.length>0&&this.needs[this.needs.length-1].value!=''){
                this.needs.push({'value':''});
             
            
            }else{
              this.presentAlert('Please fill the empty field.')
            }
          }
            removeNeedsItem(item,idx){
            
              this.needs.splice(idx,1);
            }

           

            async addPetImage(i) {
              const actionSheet = await this.actionsheetCntlr.create({
                header: "Select Image source",
                buttons: [{
                  text: 'Load from Library',
                  handler: () => {
                    this.addImage(i);
                  }
                },
                {
                  text: 'Use Camera',
                  handler: () => {
                    this.captureImage(i);
                  }
                },
                {
                  text: 'Cancel',
                  // role: 'cancel'
                }
                ]
              });
              await actionSheet.present();
            }
            
            async captureImage(i){
              
              // const options: CameraOptions = {
              //   quality: 100,
              //   sourceType: this.camera.PictureSourceType.CAMERA,
              //   destinationType: this.camera.DestinationType.DATA_URL,
              //   encodingType: this.camera.EncodingType.JPEG,
              //   mediaType: this.camera.MediaType.PICTURE
              // }
              // this.camera.getPicture(options).then((imageData) => {
              //   // imageData is either a base64 encoded string or a file URI
              //   // If it's base64 (DATA_URL):
              //   // let base64Image = 'data:image/jpeg;base64,' + imageData;
              
              //           this.uploadImage('data:image/jpeg;base64,' +imageData);
                
              // }, (err) => {
              //   // Handle error
              // });

              const image = await Camera.getPhoto({
                quality: 90,
                allowEditing: false,
                resultType: CameraResultType.Base64,
                correctOrientation:true,
                source:CameraSource.Camera
              });
            
              // image.webPath will contain a path that can be set as an image src.
              // You can access the original file using image.path, which can be
              // passed to the Filesystem API to read the raw data of the image,
              // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
              var imageUrl = image.base64String;
            
              // Can be set to the src of an image now
              let base64Image = 'data:image/jpeg;base64,' + imageUrl;
              console.log('image:',imageUrl);
             
              this.uploadImage(base64Image,i);

            }
            
           
            
            async addImage(k){
            
              // let options;
              //   options={
              //   maximumImagesCount: 5,
              //   outputType: 1,
              //   quality:100
              // }
              // if(this.platform.is('ios')){
              //   options.disable_popover=true
              // }
              //   this.imagePicker.getPictures(options).then((results) => {
              //     for (var i = 0; i < results.length; i++) {
              //         // console.log('Image URI: ' + results[i]);
                      
              //         if((results[i]==='O')||results[i]==='K'){
              //           console.log("no img");
                        
              //           }else{
                       
              //           this.uploadImage('data:image/jpeg;base64,' +results[i]);
              //           }
              //     }
              //   }, (err) => { });
              
              const image = await Camera.pickImages({
                quality: 90,
                correctOrientation:true,
                limit:5
                
              });
            
            
              for (var i = 0; i < image.photos.length; i++) {
                      console.log('Image URI: ' + image.photos[i]);
                      
                      
                      const contents = await Filesystem.readFile({
                        path: image.photos[i].path
                      });
                      
                        // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
                        this.uploadImage('data:image/jpeg;base64,' + contents.data,k)
                      
                        
                   
                       
                  }
            
              
              }

             async uploadImage(img,i){
                if(this.isLoading==false){
                this.showLoading();
                }
                let url=this.config.domain_url+'upload_file';
                let headers=await this.config.getHeader();;
                let body={file:img}
              console.log("body:",img);
              
                this.http.post(url,body,{headers}).subscribe((res:any)=>{
                  console.log("uploaded:",res);
                  if(i==1){
                  this.petImages.push(res.data);
                  }else{
                    this.profile_pic=res.data;
                  }
                  this.dismissLoader();
                },error=>{
                  // this.imageResponse.slice(0,1);
                  console.log(error);
                  this.dismissLoader();
                })
              }

              addContact(){
                this.router.navigate(['/rv-add-contact',{cid:this.cid,id:this.consumer.user.user_id}])
              }

              async contactOptions(ev,item){
                const popover = await this.popCntl.create({
                  component:RvContactOptionsComponent,
                  event:ev,
                  backdropDismiss:true,
                  componentProps:{
                    contact:item,
                   
            
                  },
                  
                  
                  
                });
                popover.onDidDismiss().then((dataReturned) => {
                  console.log('data:',dataReturned);
                  if(dataReturned.data!=undefined||dataReturned.data!=null){
                 this.ionViewWillEnter();
                 
                  }
               
               
                  
                });
                return await popover.present();
              }

              async getMedicalConditions(){
                const bid = await this.storage.get("BRANCH");
     
      const cid = await this.storage.get("COMPANY_ID");
      let url=this.config.domain_url+'medical_conditions/'+cid;
    let headers=await this.config.getHeader();;
  
     this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log('medAllergies:',res);
      
     
      this.medicalConditionArray=res.data;
      this.allergyArray=res.allergies
      this.medical_allergy_array=res.medical_allergies

      
     })
              }

getNextProfile(residents){
  let companian;
  const idx = residents.map(x => x.user_id).indexOf(this.consumer.user_id);
  
    if(idx<=0){
      
      companian=residents[idx+1].user_details.resident.id
    }else{
      companian=residents[0].user_details.resident.id;
    }
   this.router.navigate(['/rv-resident-profile-landing',{id:companian}])
}

removePetImg(i){
  this.petImages.splice(i,1);
}
async viewImage(array){
  let data=[];
  array.forEach(element => {
    if(element.images.includes('.jpg') ||
    element.images.includes('.png') ||
    element.images.includes('jpeg')){
      data.push(element.images)
    }
  });
  const modal = await this.modalCntl.create({
    component: AttchedImagesComponent,
   
    componentProps: {
     data:data,
     
     
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    
  });
  return await modal.present();
}
async emailAlreadyRegistered(){
  const modal = await this.modalCntl.create({
    component: RegisteredEmailWarningComponent,
   
    componentProps: {
    
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    
  });
  return await modal.present();
}
async getcommunityPositions(){
  const cid = await this.storage.get("COMPANY_ID");
  const bid = await this.storage.get("BRANCH");
  let url=this.config.domain_url+'community_positions/'+cid;
  let headers=await this.config.getHeader();;

   this.http.get(url,{headers}).subscribe((res:any)=>{

   
    this.community_position=res.data;

    
   })
  }

  async getGender(){
    let url=this.config.domain_url+'get_gender';
  let headers=await this.config.getHeader();;

   this.http.get(url,{headers}).subscribe((res:any)=>{


    this.genderArray=res;

    
   })
  }
  expand(){
    this.expandProfile=!this.expandProfile
  }
  makeCall(num) {
    if(num&&num.length>=10){
    this.callNumber.callNumber(num, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => {
        this.presentAlert('Something went wrong. Try again later.');
        console.log('Error launching dialer', err);
      })
    }else{
      this.presentAlert('Invalid phone number')
    }
  }

  openSelector(selector) {
    console.log('se.e:',selector)
    selector.open().then((alert)=>{
      this.selectAllCheckBox = this.document.getElementById("selectAllContacts");
     this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
      this.listener = this.renderer.listen(this.selectAllCheckBox, 'click', () => {
          if (this.selectAllCheckBox.checked) {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="false") {
                (checkbox as HTMLButtonElement).click();
                this.family_id=[];
              };
            };
          } else {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="true") {
                (checkbox as HTMLButtonElement).click();
                this.contacts.forEach(element => {
                  if(this.family_id.includes(element.user_details.user_id.toString())){
        
                  }else{
                    if((element.id.toString())!='0'){
        
                   
                  this.family_id.push(element.user_details.user_id.toString());
                    }
                  }
        
                });
              };
            };
          }
      });
      alert.onWillDismiss().then(()=>{
        this.listener();
       
      });
    })
    
  }
  ShowContent(a,b){
    
      var x = document.getElementById(a);
    var y = document.getElementById(b);
   
          x.style.display = 'none';
          y.style.display = 'block';
    
  }
  ShowContentEvacuationNote(a,b){
    
    var x = document.getElementById(a);
  var y = document.getElementById(b);
 
        x.style.display = 'none';
        y.style.display = 'block';
  
}

  async openContactInfo(item){
    const modal = await this.modalCntl.create({
      component: RvContactMoreInfoComponent,
      cssClass:'rv-cmi-modal',

      componentProps: {
        data:item
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }

  async addPet(item){
    const modal = await this.modalCntl.create({
      component: RvAddPetComponent,
      cssClass: 'add-pets-modal',

      componentProps: {
        item:item,
        flag:1,
        policy:this.pet_policy,
        consumer_id:this.consumer.user.user_id
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      this.ionViewWillEnter();
    });
    return await modal.present();
  }

  async petOptions(item,event){
    const modal = await this.popCntl.create({
      component: RvPetOptionsComponent,
      cssClass: 'rv-doc-options-popover',
      event:event,
      componentProps: {
        item:item,
        flag:2,
        policy:this.pet_policy,
        consumer_id:this.consumer.user.user_id
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      this.ionViewWillEnter();
    });
    return await modal.present();
  }
  async petDetails(item){
    const modal = await this.modalCntl.create({
      component: RvPetDetailsComponent,
      cssClass:'rv-pet-details-modal',

      componentProps: {
        item:item
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }
  async linkResident(){
    let res=[];
    if(this.consumer.property_details.contract_details.all_residents&&this.consumer.property_details.contract_details.all_residents.length){
      res=this.consumer.property_details.contract_details.all_residents.filter(x=>x);
    const idx=res.map(x=>x.user_id).indexOf(this.consumer.user.user_id);
    if(idx>=0){
      res.splice(idx,1);
    }
    }
    const modal = await this.modalCntl.create({
      component: RvPetLinkResidentComponent,
      cssClass: 'pets-link-resident-modal',

      componentProps: {
        pets:this.new_pets,
        residents:res,
        property:this.consumer.property_details.contract_details.property.location.name
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      this.ionViewWillEnter()
    });
    return await modal.present();
  }
  expandPetCard(i){
    this.new_pets.map(x=>{
      x.expand=false;
    })
    this.new_pets[i].expand=true;
  }

 
  async unlinkResident(pet,user){
    let url=this.config.domain_url+'delete_pet';
    let headers=await this.config.getHeader();
   
  let body={
    pet_id:pet,
    user_id:user
  }
  console.log('delete:',body);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      if (res.message == "Deleted") {
        this.presentAlert("Pet unlinked successfully");
      }
      this.ionViewWillEnter();
    })
  }

  viewPetImages(item,array){
    if (
      item.includes('.jpg') ||
      item.includes('.png') ||
      item.includes('jpeg')
    ) {
      // this.openImg(item);
      this.viewImage(array);
    } else {
      this.showPdf(item);
    }
  }
  async showPdf(item){
    const modal = await this.modalCntl.create({
      component: ShowpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:item,
        
         
      },
      
      
    });
    return await modal.present();
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
  async openCountryList(){
    console.log('opencountry',this.editProfile);
    
    if(this.editProfile){
    const modal = await this.modalCntl.create({
      component: CountryListComponent,
      cssClass:'country-list-modal',
      componentProps: {
        birth_country:this.birth_country
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
        this.birth_country=dataReturned.data.id;
        this.birth_country_name=dataReturned.data.name;
      }
      
    });
    return await modal.present();
  }
  }
  async addAllergy(i){
    const modal = await this.modalCntl.create({
      component:RvAddNewAllergyComponent,
      cssClass:'add-new-allergy-modal',
      componentProps: {
        data:i
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      this.getMedicalConditions();
      
    });
    return await modal.present();
  }

  checkStrength(password) {
    var strength = 0;
    let passed:any = {
     'special':false,
     'alpa':false,
     'num':false,
     'len':false,
    }
    var text = document.getElementById('result');
    var bar = document.getElementById('password-strength');
    var character = document.getElementById('one-char').children[0];
    var num = document.getElementById('one-num').children[0];
    var special = document.getElementById('one-special-char').children[0];
    var len = document.getElementById('eight-char').children[0];

    //If password contains both lower and uppercase characters, increase strength value.
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
     // strength += 1;
     passed.alpa =true
     character.setAttribute('name', 'checkmark');
    } else {
     passed.alpa =false
     character.setAttribute('name', 'ellipse-outline');
    }

    //If it has numbers and characters, increase strength value.
    if (password.match(/([0-9])/)) {
     // strength += 1;
     passed.num = true
     num.setAttribute('name', 'checkmark');
    } else {
     passed.num = false
     num.setAttribute('name', 'ellipse-outline');
    }

    //If it has one special character, increase strength value.
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
     // strength += 1;
     passed.special = true
     special.setAttribute('name', 'checkmark');
    } else {
     passed.special = false
     special.setAttribute('name', 'ellipse-outline');
    }

    if (password.length > 7) {
     // strength += 1;
     passed.len =true
     len.setAttribute('name', 'checkmark');
    } else {
     passed.len =false
     len.setAttribute('name', 'ellipse-outline');
    }

    strength = 0;
    for (const key in passed) {
     if (passed.hasOwnProperty(key) && passed[key] === true) {
        strength++;
     }
    }
    console.log(strength)

    if(strength<=0 ){
     text.textContent =''
     text.classList.value =''
     bar.classList.value =''
    // }else if(strength ==0 && password.length>0){
    //  text.textContent = 'Very weak';
    //  text.classList.value ='';
    //  text.classList.add('vweak-pswd');
    //  bar.classList.value ='';
    }
    else if(strength ==1){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-weak', 'p-v-weak');
     text.classList.add('vweak-pswd')
     text.textContent = 'Very weak';
    }else if(strength ==2){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-weak', 'p-weak');
     text.classList.add('weak-pswd')
     text.textContent = 'Weak';
    }
    else if(strength ==3){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-strong', 'p-strong');
     text.classList.add('strong-pswd')
     text.textContent = 'Strong';
    }
    else if(strength ==4){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-strong', 'p-v-strong');
     text.classList.add('vstrong-pswd')
     text.textContent = 'Very strong';
     return true;
    }


    return false;
}

  typeChange(i) {
    if (i == 1) {
      this.con_pass_type = 'text';
    } else if (i == 2) {
      this.new_pass_type = 'text';
    } else if (i == 3) {
      this.con_pass_type = 'password';
    } else if (i == 4) {
      this.new_pass_type = 'password';
    }
  }
}



