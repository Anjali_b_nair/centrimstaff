import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResProfilePage } from './rv-res-profile.page';

describe('RvResProfilePage', () => {
  let component: RvResProfilePage;
  let fixture: ComponentFixture<RvResProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
