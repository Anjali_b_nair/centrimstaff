import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResProfilePageRoutingModule } from './rv-res-profile-routing.module';

import { RvResProfilePage } from './rv-res-profile.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvContactOptionsComponent } from '../components/rv-contact-options/rv-contact-options.component';
import { RvContactEditComponent } from '../components/rv-contact-edit/rv-contact-edit.component';
import { RvContactPasswordComponent } from '../components/rv-contact-password/rv-contact-password.component';
import { RegisteredEmailWarningComponent } from '../components/registered-email-warning/registered-email-warning.component';
import { RvContactMoreInfoComponent } from '../components/rv-contact-more-info/rv-contact-more-info.component';
import { RvAddPetComponent } from '../components/rv-add-pet/rv-add-pet.component';
import { RvPetLinkResidentComponent } from '../components/rv-pet-link-resident/rv-pet-link-resident.component';
import { RvPetDetailsComponent } from '../components/rv-pet-details/rv-pet-details.component';
import { RvPetOptionsComponent } from '../components/rv-pet-options/rv-pet-options.component';
import { CountryListComponent } from '../components/country-list/country-list.component';
import { RvAddNewAllergyComponent } from '../components/rv-add-new-allergy/rv-add-new-allergy.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RvResProfilePageRoutingModule,
    ApplicationPipesModule,
    
  ],
  declarations: [
    RvResProfilePage,
    RvContactOptionsComponent,
    RvContactEditComponent,
    RvContactPasswordComponent,
    RegisteredEmailWarningComponent,
    RvContactMoreInfoComponent,
    RvAddPetComponent,
    RvPetLinkResidentComponent,
    RvPetDetailsComponent,
    RvPetOptionsComponent,
    CountryListComponent,
    RvAddNewAllergyComponent
  ],
  entryComponents:[
    RvContactOptionsComponent,
    RvContactEditComponent,
    RvContactPasswordComponent,
    RegisteredEmailWarningComponent,
    RvContactMoreInfoComponent,
    RvAddPetComponent,
    RvPetLinkResidentComponent,
    RvPetDetailsComponent,
    RvPetOptionsComponent,
    CountryListComponent,
    RvAddNewAllergyComponent
  ]
})
export class RvResProfilePageModule {}
