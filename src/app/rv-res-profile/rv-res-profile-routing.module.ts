import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResProfilePage } from './rv-res-profile.page';

const routes: Routes = [
  {
    path: '',
    component: RvResProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResProfilePageRoutingModule {}
