import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, PopoverController } from '@ionic/angular';
import { Chart } from 'chart.js'; 
import { CrfilterComponent } from '../components/crfilter/crfilter.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-consumer-report',
  templateUrl: './consumer-report.page.html',
  styleUrls: ['./consumer-report.page.scss'],
})
export class ConsumerReportPage implements OnInit {
  @ViewChild('barCanvas') private barCanvas: ElementRef;
  public items: any = [];
  barChart: any;
  id:any;
  cid:any;
  labels:any[]=[];
  attended:any[]=[];
  declined:any[]=[];
  max:any=0;
  data:any[]=[];
  color='rgb(40, 186, 98)';
  filter:any=1;
  activity:any[]=[];
  start:any;
  end:any;

  repostart:any=0;
  repoend:any=20;
  current:Date=new Date();
  flag:any;
  subscription:Subscription;
  constructor(private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,
    private popoverCntlr:PopoverController,private platform:Platform,private router:Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display="none";
    this.id=this.route.snapshot.paramMap.get('id');
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.labels=[];
    this.attended=[];
    this.declined=[];
    this.data=[];
    this.activity=[];
    this.color='rgb(40, 186, 98)';
    this.end=moment(this.current).format('yyyy-MM-DD');
    
    this.start=moment().subtract(1,'months').format('yyyy-MM-DD');
    console.log("start:",this.start,"end:",this.end);
    
    // this.items=[];
    this.getActivityReport();
    this.getConsumerReport('attended');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/consumer-profile',{id:this.cid,flag:this.flag}])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }

  expandItem(item): void {
    console.log(item);
    
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        console.log("list:",listItem);
        
        return listItem;
      });
    }
  }

  barChartMethod() {
    this.barChart='';
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: this.labels,
        datasets: [{
          label: moment(this.start).format('DD MMM')+'-'+moment(this.end).format('DD MMM yyyy'),
          data: this.data,
          backgroundColor: this.color,
            
          
          borderColor: this.color,
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: this.max,
              min: 0,
              stepSize: 1
            }, 
           
          }]
        },
        events: []
      }
    });
  }


  async getActivityReport(){
    let url=this.config.domain_url+'consumerAttendReport';
    let headers=await this.config.getHeader();;
    let body={
      user_id:this.id,
      start:this.start,
      end:this.end
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("actRep:",res);
      this.labels=res.categories;
      this.attended=res.attended;
      this.declined=res.declined;
      this.data=this.attended;
      this.max=Math.max.apply(null,this.attended);
      console.log("max:",this.max);
      
      this.barChartMethod();
      
    })
  }

  async getConsumerReport(filter){
    this.items=[];
    let url=this.config.domain_url+'consumer_attendance_details';
    let headers=await this.config.getHeader();;
    let body={
      user_id:this.id,
      start:this.start,
      end:this.end,
      filter:filter
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("conRep:",res);
      // this.activity=res.data;
      res.data.forEach(element => {
        let exp;
              if(element==res.data[0]){
                exp=true
              }else{
                exp=false
              }
        var centre={ expanded: exp ,head:element }
        this.items.push(centre);
      });;
      console.log("act:",this.items);
    })
    
    
  }

  setFilter(fil){
    this.filter=fil;
    console.log("setfilter",fil);
    
    if(fil==1){
      this.data=this.attended;
      this.color='rgb(40, 186, 98)';
      this.max=Math.max.apply(null,this.attended);
      this.barChartMethod();
      this.getConsumerReport('attended');
      console.log("attended");
      
    }else{
      this.data=this.declined;
      this.color=' rgb(206, 59, 79)';
      this.max=Math.max.apply(null,this.declined);
      this.barChartMethod();
      this.getConsumerReport('declined');
      console.log('declined');
      
    }
  }


  async datefilter(ev){
    const popover = await this.popoverCntlr.create({
      component: CrfilterComponent,
      event: ev,
      backdropDismiss:true,
      cssClass:'filterpop'
      
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        console.log("dataret:",dataReturned);
        
        this.start = dataReturned.data;
        this.end = dataReturned.role;
        // console.log("sel_wing:",this.sel_wing);
        
        //alert('Modal Sent Data :'+ dataReturned);
        
        this.getActivityReport();
        this.getConsumerReport('attended');
      }
    });
    return await popover.present();
  }
  viewMore(){
    this.repoend +=20;
  }
}
