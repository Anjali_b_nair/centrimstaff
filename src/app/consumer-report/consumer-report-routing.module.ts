import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerReportPage } from './consumer-report.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumerReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerReportPageRoutingModule {}
