import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerReportPageRoutingModule } from './consumer-report-routing.module';

import { ConsumerReportPage } from './consumer-report.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ConsumerActivityComponent } from '../components/consumer-activity/consumer-activity.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerReportPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ConsumerReportPage,ConsumerActivityComponent]
})
export class ConsumerReportPageModule {}
