import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
  old_pass:any;
  new_pass:any;
  confirm_pass: any;
  subscription:Subscription;
  resetform: FormGroup;
  conresetform: FormGroup
  flag:any;
  id:any;
  user_id:any;
  oldType: any = 'password';
  newType: any = 'password';
  conType: any = 'password';
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private toastCntlr:ToastController,private router:Router,private platform:Platform,private route:ActivatedRoute) { }

  ngOnInit() {
    this.resetform = new FormGroup({
      old_pass: new FormControl('', [Validators.required]),
      new_pass: new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern(
        /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/
      ),]),
      confirm_pass: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(
          /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/
        ),
      ]),
    });
    this.conresetform = new FormGroup({
      
      new_pass: new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern(
        /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/
      ),]),
      confirm_pass: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(
          /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/
        ),
      ]),
    });
  }
  ionViewWillEnter(){
    this.flag=this.route.snapshot.paramMap.get('flag');
    if(this.flag==2){
    this.id=this.route.snapshot.paramMap.get('id');
    this.user_id=this.route.snapshot.paramMap.get('user_id');
    }
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
        if(this.flag==1||this.flag==3){  
        this.router.navigate(['/userprofile'],{replaceUrl:true}) ;
        }else{
          this.router.navigate(['/resident-profile',{id:this.id,flag:1}],{replaceUrl:true});
        }
    });  
    
    }
    
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }


    async reset(){


      if(!this.old_pass){
        this.presentAlert('Please enter your current password')
      }else if(!this.new_pass){
        this.presentAlert('Please enter your new password');
      }else if(this.new_pass&&!this.confirm_pass){
        this.presentAlert('Please re enter your new password')
      }else{
        if (this.new_pass == this.confirm_pass) {
          if (this.resetform.valid) {
            const data=await this.storage.get("USER_ID")

            let url=this.config.domain_url+'update_password';
            
            
            let headers=await this.config.getHeader();;
            let body={
              oldpassword : this.old_pass,
              newpassword : this.new_pass,
              user_id : data
            }
            console.log("body",body)
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log(res);
                if(res.status=='success'){
                  this.storage.remove('USERTOKEN')
                  this.storage.set('USERTOKEN',res.token);
                  this.presentAlert('Password reset successfully.');
                  if(this.flag==1){
                    this.router.navigate(['/userprofile'],{replaceUrl:true});
                  }else{
                    this.router.navigate(['/edit-profile',{flag:2}],{replaceUrl:true});
                  }
               
                }else{
                  this.presentAlert('Old password is incorrect.');
                }
              
              },error=>{
                console.log(error);
                
              })
          }else {
            this.presentAlert('Incorrect password format.');
          }
        }else{
          this.presentAlert("Confirmed password doesn't match with new password");
        }

      }

      
    
    }


    async conreset(){
     if(!this.new_pass){
        this.presentAlert('Please enter your new password');
      }else if(this.new_pass&&!this.confirm_pass){
        this.presentAlert('Please re enter your new password')
      }else{
        if (this.new_pass == this.confirm_pass) {
          if (this.conresetform.valid) {

            let url=this.config.domain_url+'updateResPassword';
          
            
            
            let headers=await this.config.getHeader();;
            let body={
            
              newpassword : this.new_pass,
              user_id : this.user_id
            }
            console.log("body",body)
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log(res);
                if(res.status=='success'){
                this.presentAlert('Password reset successfully.');
               
                  this.router.navigate(['/resident-profile',{id:this.id,flag:1}],{replaceUrl:true});
                
                }
                
               
              },error=>{
                console.log(error);
                
              })
          }else {
            this.presentAlert('Incorrect password format.');
          }
        }else{
          this.presentAlert("Confirmed password doesn't match with new password");
        }

      }
        
    }




    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        cssClass:'toastStyle',
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }
    
    
    // skip(){
    //   // this.router.navigate(['/menu']);
    //   this.router.navigate(['/edit-profile',{flag:2}]);
    // }
    
    
    
    back(){
      if(this.flag==1||this.flag==3){
        this.router.navigate(['/userprofile'],{replaceUrl:true});
        }else{
          // this.router.navigate(['/menu']);
          this.router.navigate(['/resident-profile',{id:this.id,flag:1}],{replaceUrl:true});
        }
    }


    async quickreset(){
      if(!this.new_pass){
        this.presentAlert('Please enter your new password');
      }else if(this.new_pass&&!this.confirm_pass){
        this.presentAlert('Please re enter your new password')
      }else{
        if (this.new_pass == this.confirm_pass) {
          if (this.conresetform.valid) {

            const data=await this.storage.get("USER_ID");
            const uname=await this.storage.get("USER_NAME");
            const email=await this.storage.get("EMAIL");
          let url=this.config.domain_url+'profile_quick_update';
         
          
          
          let headers=await this.config.getHeader();;
          let body={
          
            password : this.new_pass,
            id : data,
            user_name:uname,
            email:email
          }
          console.log("body",body)
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              if(res.status=='success'){
              this.presentAlert('Password reset successfully.');
             
              this.router.navigate(['/edit-profile',{flag:2}],{replaceUrl:true});
              
              }
              
              this.new_pass="";
            },error=>{
              console.log(error);
              
            })

          }else {
            this.presentAlert('Incorrect password format.');
          }
        }else{
          this.presentAlert("Confirmed password doesn't match with new password");
        }

      }
       
  }

  // checkStrength(password) {
  //   var strength = 0;
  //   var text = document.getElementById('result');
  //   var bar = document.getElementById('password-strength');
  //   var character = document.getElementById('one-char').children[0];
  //   var num = document.getElementById('one-num').children[0];
  //   var special = document.getElementById('one-special-char').children[0];
  //   var len = document.getElementById('eight-char').children[0];

  //   //If password contains both lower and uppercase characters, increase strength value.
  //   if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
  //     strength += 1;
  //     character.setAttribute('name', 'checkmark');
  //   } else {
  //     character.setAttribute('name', 'ellipse-outline');
  //   }

  //   //If it has numbers and characters, increase strength value.
  //   if (password.match(/([0-9])/)) {
  //     strength += 1;
  //     num.setAttribute('name', 'checkmark');
  //   } else {
  //     num.setAttribute('name', 'ellipse-outline');
  //   }

  //   //If it has one special character, increase strength value.
  //   if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
  //     strength += 1;
  //     special.setAttribute('name', 'checkmark');
  //   } else {
  //     special.setAttribute('name', 'ellipse-outline');
  //   }

  //   if (password.length > 7) {
  //     strength += 1;
  //     len.setAttribute('name', 'checkmark');
  //   } else {
  //     len.setAttribute('name', 'ellipse-outline');
  //   }

  //   // If value is less than 2
  //   if (strength < 2) {
  //     text.classList.remove('weak-pswd', 'strong-pswd');

  //     bar.classList.remove('pswd-strong', 'p-strong', 'pswd-weak', 'p-weak');
  //     bar.classList.add('pswd-very-weak', 'p-v-weak');
  //     text.classList.add('vweak-pswd');
  //     text.textContent = 'Very weak';
  //   } else if (strength == 2) {
  //     //  $('#result').addClass('good');
  //     bar.classList.remove(
  //       'pswd-very-weak',
  //       'p-v-weak',
  //       'pswd-strong',
  //       'p-strong'
  //     );
  //     bar.classList.add('pswd-weak', 'p-weak');
  //     text.classList.remove('vweak-pswd', 'strong-pswd');
  //     text.classList.add('weak-pswd');
  //     text.textContent = 'Weak';
  //   } else if (strength == 4) {
  //     text.classList.remove('vweak-pswd', 'weak-pswd');
  //     text.classList.add('strong-pswd');
  //     bar.classList.remove('pswd-very-weak', 'p-v-weak', 'pswd-weak', 'p-weak');
  //     bar.classList.add('pswd-strong', 'p-strong');
  //     text.textContent = 'Strong';

  //     return true;
  //   }

  //   return false;
  // }


  checkStrength(password) {
    var strength = 0;
    let passed:any = {
     'special':false,
     'alpa':false,
     'num':false,
     'len':false,
    }
    var text = document.getElementById('result');
    var bar = document.getElementById('password-strength');
    var character = document.getElementById('one-char').children[0];
    var num = document.getElementById('one-num').children[0];
    var special = document.getElementById('one-special-char').children[0];
    var len = document.getElementById('eight-char').children[0];

    //If password contains both lower and uppercase characters, increase strength value.
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
     // strength += 1;
     passed.alpa =true
     character.setAttribute('name', 'checkmark');
    } else {
     passed.alpa =false
     character.setAttribute('name', 'ellipse-outline');
    }

    //If it has numbers and characters, increase strength value.
    if (password.match(/([0-9])/)) {
     // strength += 1;
     passed.num = true
     num.setAttribute('name', 'checkmark');
    } else {
     passed.num = false
     num.setAttribute('name', 'ellipse-outline');
    }

    //If it has one special character, increase strength value.
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
     // strength += 1;
     passed.special = true
     special.setAttribute('name', 'checkmark');
    } else {
     passed.special = false
     special.setAttribute('name', 'ellipse-outline');
    }

    if (password.length > 7) {
     // strength += 1;
     passed.len =true
     len.setAttribute('name', 'checkmark');
    } else {
     passed.len =false
     len.setAttribute('name', 'ellipse-outline');
    }

    strength = 0;
    for (const key in passed) {
     if (passed.hasOwnProperty(key) && passed[key] === true) {
        strength++;
     }
    }
    console.log(strength)

    if(strength<=0 ){
     text.textContent =''
     text.classList.value =''
     bar.classList.value =''
    // }else if(strength ==0 && password.length>0){
    //  text.textContent = 'Very weak';
    //  text.classList.value ='';
    //  text.classList.add('vweak-pswd');
    //  bar.classList.value ='';
    }
    else if(strength ==1){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-weak', 'p-v-weak');
     text.classList.add('vweak-pswd')
     text.textContent = 'Very weak';
    }else if(strength ==2){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-weak', 'p-weak');
     text.classList.add('weak-pswd')
     text.textContent = 'Weak';
    }
    else if(strength ==3){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-strong', 'p-strong');
     text.classList.add('strong-pswd')
     text.textContent = 'Strong';
    }
    else if(strength ==4){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-strong', 'p-v-strong');
     text.classList.add('vstrong-pswd')
     text.textContent = 'Very strong';
     return true;
    }


    return false;
}

  typeChange(i) {
    if (i == 1) {
      this.oldType = 'text';
    } else if (i == 2) {
      this.newType = 'text';
    } else if (i == 3) {
      this.conType = 'text';
    } else if (i == 4) {
      this.oldType = 'password';
    } else if (i == 5) {
      this.newType = 'password';
    } else if (i == 6) {
      this.conType = 'password';
    }
  }
}
