import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonContent, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.page.html',
  styleUrls: ['./feeds.page.scss'],
})
export class FeedsPage implements OnInit {
  // tabStatus:boolean=false;
  status:any;
  st:any;
  banner:boolean=true;
  
  token:string;
  user_id:any;
  branch:any;
   posts:any[]=[];
  pinned:any=null;
  url: string; 
  user_img:string;
 
  name:string;
  p_name:string;
  commentText:string;
  page_number = 1;
  no_post:boolean;
  has_more_items: boolean = true;
  subscription:Subscription;
  welcome:string;
  pin_creator:any;
  lid:any;
  index:any;
  notify:any;
  device:any;
  not_array:any=[];
  scrollPosition = 0;
  modules: any[] = [];
  dining_view: boolean = false;
  user_view: boolean = false;
  act_view: boolean = false;
  rv:any;
@ViewChild('content' , { static: false }) content: IonContent;
  constructor(public http:HttpClient,private storage:Storage,private httpConfigService: HttpConfigService,
    private router:Router,private nav:NavController,private platform:Platform,private toastCntrl:ToastController,
    private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }



async ionViewWillEnter(){
  this.not_array=[];
  this.pinned=null;
  this.page_number=1;
  this.posts=[];
  // const tabBar = document.getElementById('myTabBar');
  // tabBar.style.display="flex";
  // this.posts=[];
  // this.pinned={};
  // this.page_number=1;
  this.showLoading();

 
    const name=await this.storage.get("NAME")
      this.name=name;
      this.p_name=this.name.substr(0,1);
   
    
      this.rv=await this.storage.get('RVSETTINGS');
    const pic=await this.storage.get("PRO_IMG")
      this.user_img=pic;
   

    const data=await this.storage.get("USER_ID")

    const rv=await this.storage.get('RVSETTINGS');
        const bid=await this.storage.get("BRANCH")
        const type = await this.storage.get("USER_TYPE")

      let url=this.httpConfigService.domain_url+'notification';
      let headers=await this.httpConfigService.getHeader();
      let body={
        id:data,
        offset:0,
        limit:20,
        staff_app:1
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        
        
        
        this.notify=res.total_count;
        console.log("len:", this.notify);
      
      })
   
  
  
  let date=new Date();
  let hr=date.getHours();
  console.log("hr:",hr)
  if(hr<=6){
    this.welcome='Welcome'
  }
  else if(hr<=12){
    this.welcome='Good morning'
  }
  else if(hr<17){
    this.welcome='Good afternoon'
  }
  else if(hr>=17){
    this.welcome='Good evening'
  }
  const cid = await this.storage.get("COMPANY_ID")

  let murl = this.httpConfigService.domain_url + 'get_modules/' + cid;
    this.http.get(murl,{headers}).subscribe((mod: any) => {
      
      this.modules = mod
     

    })

  this.getPosts(false, "");
  (await this.httpConfigService.getUserPermission()).subscribe((res: any) => {
    console.log('permissions:', res);
    let routes = res.user_routes;
    
    // if (res.main_permissions.residents==1&&routes.includes('residents.index')) {

    //   this.user_view = true
    // } else {

    //   this.user_view = false;
    // }
    if (res.main_permissions.residents==1){
      if(rv==0&&routes.includes('residents.index')||rv==1&&routes.includes('rv_resident.index')) {

      this.user_view = true
    } else {

      this.user_view = false;
    }
  }
    if (res.main_permissions.dining==1) {

      this.dining_view = true
    } else {

      this.dining_view = false;
    }
    if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {

      this.act_view = true
    } else {

      this.act_view = false;
    }
  })

  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.router.navigate(['/menu']) ; 
}); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    this.content.getScrollElement().then(data => {
      console.log(data.scrollTop);
      this.scrollPosition = data.scrollTop;
    });
 }
 notification(){
   this.router.navigate(['/notification',{flag:3}]);
 }
 async like(item){
  // this.status=liked;
  
    
    const data=await this.storage.get("USER_ID")

      
        const bid=await this.storage.get("BRANCH")

      this.user_id=data;
      
      // this.status=id;
      // this.index=idx;
      if(item.liked==true  ){
         this.status=0
        //  this.lid=id
      }else{
        this.status=1
      }
  let story_id=item.id;
  let body={
    post_id: story_id,
    user_id: this.user_id,
    action_type: '1',
    status:this.status
  };
  console.log("body:",body);
  let url=this.httpConfigService.domain_url+'update_post_action';
  let headers=await this.httpConfigService.getHeader();;
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
    console.log(data);
    console.log("like:",data);
    item.liked=!item.liked
    item.liked ? item.likes_count++ : item.likes_count--;
    // feed.IsLike ? feed.LikesCount++ : feed.LikesCount--;
   
  },error=>{
    console.log(error);
    
  });


}

// close welcome banner
hideBanner(){
  this.banner=false;
}

// get newsfeed posts

async getPosts(isFirstLoad, event){
// if(this.flag==1){
//   this.user_id=this.route.snapshot.paramMap.get("user_id");
//   this.branch=this.route.snapshot.paramMap.get("branch_id");
// }else{

      
  
    const data=await this.storage.get("USER_ID")

      
      this.user_id=data;
     
      
          const res=await this.storage.get("BRANCH");
            this.branch=res;
  this.url = '?page=' + this.page_number;

            (await this.httpConfigService.getListItems(this.url, this.user_id, this.branch)).subscribe((data:any)=>{
    console.log("res:",data);
    if(data.pinned==null){
      this.pinned=null
    
    }else{
      this.pinned=data.pinned;
      this.pin_creator=this.pinned.creator.name;
      console.log("pin:",this.pinned.creator.name)
    }
    // this.posts=data.story.data;
    for (let i = 0; i < data.story.data.length; i++) {
      this.posts.push(data.story.data[i]);
    }
    this.dismissLoader();
    console.log("post:",this.posts);
    // this.content.scrollToPoint(0, this.scrollPosition);
    if (isFirstLoad)
    event.target.complete();

  this.page_number++;
  }, error => {
    console.log(error);
  })
// })


}
// view story details
gotoDetails(id){
  // this.router.navigate(['/story-details',{post_id:id}]);
  this.nav.navigateRoot(['/story-details',{post_id:id,flag:1}])

}
doRefresh(event) {
  this.page_number = 1;
  this.posts=[];
  this.ionViewWillEnter();

  setTimeout(() => {
    
    event.target.complete();
  }, 2000);
}

// infinite scrolling

doInfinite(event) {
this.getPosts(true, event)
}
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

gotoActivity(){
  if(this.modules.includes('Activity')&&this.act_view){
  this.router.navigate(['/activities'])
}else{
  this.presentAlert("Access denied." )
}
}
gotoDining(){
  console.log('mod:',this.modules.includes('Dining Beta'),this.dining_view)
  if(this.rv!==1&&this.modules.includes('Dining Beta')&&this.dining_view){
  this.router.navigate(['/dining-dashboard'])
}else{
  this.presentAlert("Access denied." )
}
}
gotoResidents(){
  // if(this.user_view){
  // this.router.navigate(['/consumers'])
  if(this.user_view){
    if(this.rv==1){
      this.router.navigate(['/rv-res-list'])
    }else{
  this.router.navigate(['/consumers',{flag:1}])
    }
}else{
  this.presentAlert("Access denied." )
}
}
async presentAlert(mes){
  const alert = await this.toastCntrl.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}
}
