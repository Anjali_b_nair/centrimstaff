import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinInroomDiningPage } from './din-inroom-dining.page';

describe('DinInroomDiningPage', () => {
  let component: DinInroomDiningPage;
  let fixture: ComponentFixture<DinInroomDiningPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinInroomDiningPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinInroomDiningPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
