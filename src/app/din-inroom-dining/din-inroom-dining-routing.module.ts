import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinInroomDiningPage } from './din-inroom-dining.page';

const routes: Routes = [
  {
    path: '',
    component: DinInroomDiningPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinInroomDiningPageRoutingModule {}
