import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment-timezone';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { DiningAddItemsComponent } from '../components/dining/dining-add-items/dining-add-items.component';
import { ServingRefusedComponent } from '../components/dining/serving-refused/serving-refused.component';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
import { ServingConsumedComponent } from '../components/dining/serving-consumed/serving-consumed.component';
import { UnserveComponent } from '../components/dining/unserve/unserve.component';
import { ItemOptionsComponent } from '../components/dining/item-options/item-options.component';
import { ServingChangeCountComponent } from '../components/dining/serving-change-count/serving-change-count.component';
import { MarkAsServedComponent } from '../components/dining/mark-as-served/mark-as-served.component';
import { ServeConfirmationComponent } from '../components/dining/serve-confirmation/serve-confirmation.component';
import { UndoRefuseComponent } from '../components/dining/undo-refuse/undo-refuse.component';
@Component({
  selector: 'app-din-inroom-dining',
  templateUrl: './din-inroom-dining.page.html',
  styleUrls: ['./din-inroom-dining.page.scss'],
})
export class DinInroomDiningPage implements OnInit {
subscription:Subscription;
ser_time:any;
  servetime:any;
  date:any;
  details:any=[];
  detailsCopy:any=[];
  wingArray:any[]=[];
  selectedList:any='1';
  wing:any;
  selectedItem: any[] = [{ Resident: 0, item: 0 , qty:1}];
  selectedAdd: any[]  =[{ Resident: 0, item: 0 ,qty:1}]
  order_id: any;
  terms:any;
  hide:boolean=true;
  count:any={};
  menu_id:any;

  status:any='1';

  selected:any=[];

  serving_order:any;
  // wing:any;
  edit:boolean=false;
  order:boolean=false;
  loading:boolean=true;
  constructor(private router:Router,private platform:Platform,private storage:Storage,private modalCntrl:ModalController,
    private route:ActivatedRoute,private config:HttpConfigService,private http:HttpClient,private popCntlr:PopoverController,
    private toastCntlr:ToastController) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
 
  this.ser_time=this.route.snapshot.paramMap.get('ser_time_id');
  this.servetime=this.route.snapshot.paramMap.get('ser_time');
  this.date=this.route.snapshot.paramMap.get('date');
  this.wing=this.route.snapshot.paramMap.get('wing');
  this.hide=true;
  // this.getDetails(this.wing);
  this.getDetailsNew();
  
  // this.getWing();
  (await this.config.getUserPermission()).subscribe((res: any) => {
    console.log('permissions:', res);
    let routes = res.user_routes;
    
    if (res.main_permissions.dining==1&&routes.includes('serving.edit')) {

      this.edit= true
    } else {

      this.edit = false;
      
    }
    if (res.main_permissions.dining==1&&routes.includes('place_order')) {

      this.order= true
    } else {

      this.order = false;
      
    }
  })
   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
     this.back();
     }); 
     
     }
     ionViewWillLeave() { 
     this.subscription.unsubscribe();
     }
     
      
 
 
       async getDetails(wing){
         this.details=[];
         this.selected=[];
         const cid=await this.storage.get('COMPANY_ID');
         const bid=await this.storage.get('BRANCH');
        
         let headers=await this.config.getHeader();
         let url=this.config.domain_url+'serving_item_listing';
         console.log(url);
         let body={
           company_id:cid,
           servingtimeid:this.ser_time,
           date:moment(this.date).format('YYYY-MM-DD'),
           serve_area_id:0,
           wing_id:[wing]
         }
         console.log('body:',body);
         this.http.post(url,body,{headers}).subscribe((res:any)=>{
           
           console.log('res:',res);
           if(res.data.menu){
           this.count=res.data.count;
           this.menu_id=res.data.menu.id;
           this.serving_order=res.data.serving_order;
           for(let i in res.data.residents){
           this.details.push({res:res.data.residents[i],expanded:false})
           }
           console.log('deta:',this.details);
           this.loading=false;
          }else{
            this.back();
              this.presentAlert('No menu available')
          }
         },error=>{
          this.loading=false
         })
       
       }
    
    
      back(){
        this.popCntlr.dismiss(null,null,'countPop');
        this.router.navigate(['/din-choose-dt-mealtime'],{replaceUrl:true})
      }
      async getWing(){
        this.wingArray=[];
      
      
         const bid=await this.storage.get("BRANCH")
      
         let headers=await this.config.getHeader();
           let branch=bid.toString();
           let body={company_id:branch}
           let url=this.config.domain_url+'branch_wings/'+bid;
           this.http.get(url,{headers}).subscribe((res:any)=>{
             console.log("wing:",res);
             let expanded;
             for(let i in res.data.details){
              console.log("data:",res.data.details[i]);
              if(i=='0'){
                expanded=true;
              }else{
                expanded=false
              }
            let obj={id:res.data.details[i].id,wing:res.data.details[i].name,expanded:expanded};
         
               this.wingArray.push(obj);
            
             
             }
      
             this.getDetails(this.wingArray[0].id);
             this.wing=this.wingArray[0].id;
             console.log("array:",this.wingArray);
             
             
           },error=>{
             console.log(error);
             
           })
        
       }
       expand(item){
        if (item.expanded) {
          item.expanded = false;
        } else {
          this.wingArray.map(listItem => {
            if (item == listItem) {
              listItem.expanded = !listItem.expanded;
              if(listItem.expanded){
                this.terms=undefined;
                this.getDetails(listItem.id);
                this.loading=true;

                this.wing=listItem.id;
                this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
              }
            } else {
              listItem.expanded = false;
            }
            return listItem;
          });
        }
       }


       async refuse(item,i) {
        // if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
    
        // } else if (this.selectedItem[0].Resident == item.res.user.user_id) {
          const modal = await this.modalCntrl.create({
            component: ServingRefusedComponent,
            cssClass: 'full-width-modal'
            
          });
          modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned.data) {
              this.refuseItem(dataReturned.data, dataReturned.role,i,item);
            }
          });
          return await modal.present();
        // }
      }
      segmentChanged(ev,status){
        this.status=status
        console.log(status)
        if(status==1){
          this.loading=true;
          // this.getDetails(this.wing)
          this.getDetailsNew();
        }else{
          // let details=this.details;
          this.details=[];
          this.detailsCopy.forEach(ele=>{
            if(status==2&&ele.res.tag_status=='default'){
              this.details.push(ele)
            }else if(status==3&&(ele.res.tag_status=='served')){
              this.details.push(ele)
            }else if(status==4&&ele.res.tag_status=='refused'){
              this.details.push(ele)
            }else if(status==5&&ele.res.is_leave==1){
              this.details.push(ele);
            }else if(status==6&&ele.res.tag_status=='consumed'){
              this.details.push(ele);
            }else if(status==7&&ele.res.tag_status=='default'){
              this.details.push(ele)
            }else if(status==8&&ele.res.tag_status=='pending'){
              this.details.push(ele)
            }
          })
        }

      }
      async dietaryPreference(item) {
        const modal = await this.modalCntrl.create({
          component: DietaryPreferencesComponent,
          cssClass: 'dining-dpreference-modal',
          componentProps: {
            details: item
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
    
        });
        return await modal.present();
      }
    
      setAllergies(item) {
        let a = [];
        a = item.split(/\, +/);
        return a;
      }

      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass:'toastStyle',
          duration: 3000,
          position:'top'      
        });
        alert.present(); //update
      }
    
      async serve(item,i) {
        if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0 &&this.selectedAdd.length==1&&this.selectedAdd[0].Resident==0) {
          this.presentAlert('Please select atleast one item')
        
        } else if (this.selectedItem[0].Resident == item.res.user.user_id || this.selectedAdd[0].Resident == item.res.user.user_id ) {
          let ordereditems=[];
          if(item.res.order&&item.res.order.order_items.length){
            item.res.order.order_items.forEach(ele=>{
             
              
              ordereditems.push(ele.id)
             
              
            })
            
          }
          if(item.res.additional_order&&item.res.additional_order.length){
            item.res.additional_order.forEach(ele=>{
             
                  ordereditems.push(ele.item.id);
                 
            })
            
          }
          
          let len1=0;let len2=0;
          if(this.selectedItem[0].Resident!==0){
            len1=this.selectedItem.length
          }
          if(this.selectedAdd[0].Resident!==0){
            len2=this.selectedAdd.length
          }
          console.log('orditem:',ordereditems,ordereditems.length,len1+len2);
          if(len1+len2==ordereditems.length){
              this.serveItem(item,i,1)
          }else{
            this.serveConfirmation(item,i)
          }
          
        }
        
      }

      async serveConfirmation(item,i){
        const modal = await this.modalCntrl.create({
          component: ServeConfirmationComponent,
          cssClass: 'full-width-modal'
          
        });
        modal.onDidDismiss().then((dataReturned) => {
          if (dataReturned.data) {
            this.serveItem(item,i,2);
          }
        });
        return await modal.present();
      }

      async serveItem(item,i,n){
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
        
        let url = this.config.domain_url + 'serve_orders';
        if(item.res.order&&item.res.order.length){
          this.order_id=item.res.order.id
        }else if(item.res.additional_order&&item.res.additional_order.length){
          this.order_id=item.res.additional_order[0].order_id
        }
        let items = [];
        let add=[];
        let quantity=[];
        if(this.selectedItem.length>0&&this.selectedItem[0].Resident!=0){
        this.selectedItem.forEach(ele => {
          items.push(ele.item)
          quantity[ele.item]=ele.qty
        })
      }
      if(this.selectedAdd.length>0&&this.selectedAdd[0].Resident!=0){
        this.selectedAdd.forEach(ele => {
          add.push(ele.item)
          quantity[ele.item]=ele.qty
        })
      }
  
        let body = {
          company_id: cid,
          served_by: uid,
          order_id: this.order_id,
          order_details_id: items,
          order_add_details_id: add,
          itemqunatity:quantity,
          serveqty:1
        }
        console.log('body:',body)
        let headers=await this.config.getHeader();
        this.http.post(url, body, { headers }).subscribe((res: any) => {
  
          console.log('res:', res);
          this.selectedItem= [{ Resident: 0, item: 0 , qty:1 }];
          this.selectedAdd= [{ Resident: 0, item: 0 , qty:1 }];
          if(this.details[i].res.tag_status!=='served'){
          // if(this.count.toserved>0){
          //   this.count.toserved--;
          //   }
          if(this.count.to_consume>0){
            this.count.to_consume--;
          }
            this.count.served++;
          }
            if(n==1){
          this.details[i].res.tag_status='served';
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='served';
          if(this.details[i].res.order.order_items&&this.details[i].res.order.order_items.length){
            this.details[i].res.order.order_items.forEach((ele,index)=>{
          this.details[i].res.order.order_items[index].serving_status=1;
            })
            this.detailsCopy[i].res.order.order_items.forEach((ele,index)=>{
              this.detailsCopy[i].res.order.order_items[index].serving_status=1;
                })
          }
          

            if(this.details[i].res.additional_order&&this.details[i].res.additional_order.length){
              this.details[i].res.additional_order.forEach((ele,index)=>{
                this.details[i].res.additional_order[index].serving_status=1;
              })
              this.detailsCopy[i].res.additional_order.forEach((ele,index)=>{
                this.detailsCopy[i].res.additional_order[index].serving_status=1;
              })
            }
          
            }
          
          
        },error=>{
          console.log(error)
        })
      }
      async leave(item,i) {
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
        let headers=await this.config.getHeader();
        let url = this.config.domain_url + 'mark_dietry_leave';
    
    
        let body = {
          company_id: cid,
          created_by: uid,
          consumer_id: item.res.user.user_id,
          start: moment(this.date).format('YYYY-MM-DD')
    
        }
        this.http.post(url, body, { headers }).subscribe((res: any) => {
    
          console.log('res:', res);
          this.details[i].res.isleave=1
          this.details[i].res.tag_status='leave';
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.is_leave=1
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='leave';
          this.selectedItem= [{ Resident: 0, item: 0 ,qty:1 }];
          this.selectedAdd= [{ Resident: 0, item: 0 , qty:1 }];
          this.count.leave++;
        },error=>{
          console.log(error)
        })
      }
      async refuseItem(reason, otherReason,i,item) {
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
        let headers=await this.config.getHeader();
        let url = this.config.domain_url + 'refuse_orders';
    
        let items = [];
        let add=[];
      
          // item.res.order[0].order_items.forEach(ele=>{
          //   items.push(ele.id)
          // });
          // item.res.additional_order.forEach(ele=>{
          //   add.push(ele.id)
          // });
          // this.order_id=item.res.order[0].id
          item.res.order.order_items.forEach(ele=>{
            items.push(ele.id)
          });
          item.res.additional_order.forEach(ele=>{
            add.push(ele.id)
          });
          this.order_id=item.res.order.id
    
        let body = {
          company_id: cid,
          served_by: uid,
          order_id: this.order_id,
          order_details_id: items,
          order_add_details_id:add,
          reason: reason,
          reason_text: otherReason
        }

        console.log('refbody:',body)
        this.http.post(url, body, { headers }).subscribe((res: any) => {
    
          console.log('res:', res);
          this.details[i].res.tag_status='refused';
          this.details[i].res.order.refuse_reason=reason;
            this.details[i].res.order.refuse_comment=otherReason;
            this.details[i].res.order.order_items.map(x=>x.serving_status=2);
          // this.details[i].res.order[0].refuse_reason=reason;
          // this.details[i].res.order[0].refuse_comment=otherReason;
          // this.details[i].res.order[0].refuse_status=1;
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='refused';
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_reason=reason;
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_comment=otherReason;
          this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items.map(x=>x.serving_status=2);
          this.selectedItem= [{ Resident: 0, item: 0 , qty: 1 }];
          this.selectedAdd= [{ Resident: 0, item: 0 , qty: 1 }];
          this.count.refused++;
          this.count.to_consume--;
          // this.count.toserved--;
        },error=>{
          console.log(error)
        })
      }
      // selectItems(item, order_id, order,i,itemQty) {
      //   if(i==1){
      //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
      //     this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
      //     this.order_id = order;
      //     console.log('first entry');
      //     this.selectedItem.splice(0, 1);
      //   } else {
      //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
      //     if (targetIdx >= 0) {
      //       const ItemIdx = this.selectedItem.map(item => item.Item).indexOf(order_id);
      //       if (ItemIdx >= 0) {
      //         this.selectedItem.splice(ItemIdx, 1);
      //         console.log('item already presented');
      //         if (this.selectedItem.length == 0) {
      //           this.selectedItem.push({ Resident: 0, item: 0 ,qty: 1 })
      //         }
      //       } else {
      //         this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
      //         this.order_id = order;
      //       }
      //     } else {
      //       console.log('another resident');
      //     }
      //   }
      // }else{

      //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
      //     if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
      //       this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
      //       this.order_id = order;
      //       console.log('first entry');
      //       this.selectedAdd.splice(0, 1);
      //     } else {
      //       const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
      //       if (targetIdx >= 0) {
      //         const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
      //         if (ItemIdx >= 0) {
      //           this.selectedAdd.splice(ItemIdx, 1);
      //           console.log('item already presented');
      //           if (this.selectedAdd.length == 0) {
      //             this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
      //           }
      //         } else {
      //           this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
      //           this.order_id = order;
      //         }
      //       } else {
      //         console.log('another resident');
      //       }
      //     }
      //   }else{
      //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
      //     if (targetIdx >= 0 && item.res.user.user_id==this.selectedItem[targetIdx].Resident) {
      //       if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
      //         this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
      //         this.order_id = order;
      //         console.log('first entry');
      //         this.selectedAdd.splice(0, 1);
      //       } else {
      //         const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
      //         if (targetIdx >= 0) {
      //           const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
      //           if (ItemIdx >= 0) {
      //             this.selectedAdd.splice(ItemIdx, 1);
      //             console.log('item already presented');
      //             if (this.selectedAdd.length == 0) {
      //               this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
      //             }
      //           } else {
      //             this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
      //             this.order_id = order;
      //           }
      //         } else {
      //           console.log('another resident');
      //         }
      //       }
      //     }
      //   }
        
      // }
      //   console.log('selectedItems:', this.selectedItem,this.selectedAdd)
      // }

     async selectItems(item, orderid, order_id,k,order,i,index) {
      if(this.edit){
       
        // if(item.res.order[0].refuse_status!=1&&item.res.isleave!==1){
          if(item.res.tag_status!=='refused'){
        let s_url=this.config.domain_url+'serve_individual_orders';
        let u_url=this.config.domain_url+'unserve_individual_orders';
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
        let headers=await this.config.getHeader();
        
        if(k==1){
          let body={
            company_id:cid,
            served_by:uid,
            order_details_id:orderid,
            itemqunatity:order.served_quantity
  
          }
          console.log('body:',body,s_url);
          
          if(order.serving_status==0){
            this.http.post(s_url,body,{headers}).subscribe(res=>{
              console.log("servedsingle:",res);
              this.details[i].res.order.order_items[index].serving_status=1;
              // this.details[i].res.order[0].order_items[index].serving_status=1;
              this.details[i].res.tag_status='served';

              if(this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order){
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index].serving_status=1;
                }
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='served';

              this.selectedItem.push({ Resident: item.res.user.user_id, item: orderid ,qty:order.served_quantity});
              if(this.selectedItem[0].Resident == 0){
                this.selectedItem.splice(0,1);
              }

             
              
              if(this.selectedItem.length==1){
                // if(this.count.toserved>0){
                //   this.count.toserved--;
                //   }
                if(this.count.to_consume>0){
                  this.count.to_consume--;
                }
                  this.count.served++;
                }
              
              // this.details[i].res.tag_status='served';
            },err=>{
              console.log(err)
            })
            
          }else{
            this.http.post(u_url,body,{headers}).subscribe(res=>{
              // console.log("unservedsingle:",res);
              this.details[i].res.order.order_items[index].serving_status=0;
              // this.details[i].res.tag_status='';

              if(this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order){
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index].serving_status=0;
                }
                // this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='';
              // this.details[i].res.order[0].order_items[index].serving_status=0;
              const idx=this.selectedItem.map(x=>x.item).indexOf(orderid);
              
              
          if(idx>=0){
            this.selectedItem.splice(idx,1);
          }
          console.log('unselect:',this.details[i].res,this.details[i].res.tag_status);
          console.log("unservedsingle:",this.details[i].res.order.order_items.map(x=>x.serving_status).includes(1),this.details[i].res.additional_order.map(x=>x.serving_status).includes(1));
          // if(!this.details[i].res.order.order_items.map(x=>x.serving_status).includes(1)){
          //   if(this.count.served>0){
          //     this.count.served--;
          //     }
             
          //     this.count.to_consume++;
          //     this.details[i].res.tag_status='default';
          //     this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
          //   }
          if(!this.selectedItem.length){
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
           
          }
          if(!this.selectedAdd.length){
           
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
          }
          if(!this.details[i].res.order.order_items.map(x=>x.serving_status).includes(1)&&!this.details[i].res.additional_order.map(x=>x.serving_status).includes(1)){
            this.count.to_consume++;
            if(this.count.served>0){
            this.count.served--;
            }
            this.details[i].res.tag_status='default';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
            console.log('everything unserved,status default:',this.details[i].res.tag_status,this.details[i].res.order,this.details[i].res.additional_order,this.selectedItem[0].Resident,this.selectedAdd[0].Resident);
            
          }
          
            },err=>{
              console.log(err)
            })
          }

        }else{
          let body={
            company_id:cid,
            served_by:uid,
            order_add_details_id:orderid,
            itemqunatity:order.served_quantity
  
          }
          console.log('body:',body,s_url);
          if(order.serving_status==0){
            this.http.post(s_url,body,{headers}).subscribe(res=>{
              console.log("servedaddsingle:",res);
              // this.details[i].res.additional_order[index].serving_status=1;
              // this.details[i].res.tag_status='served';
              this.details[i].res.additional_order[index].serving_status=1;
                this.details[i].res.tag_status='served';
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[index].serving_status=1;
                
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='served';
              this.selectedAdd.push({ Resident: item.res.user.user_id, item: orderid ,qty:order.served_quantity});
          if(this.selectedAdd[0].Resident == 0){
            this.selectedAdd.splice(0,1);
          }
          if(this.selectedItem.length>=1&&this.selectedItem[0].Resident==0&&this.selectedAdd.length==1){
            this.count.served++;
            if(this.count.to_consume>0){
            this.count.to_consume--;
            }
          }
            },err=>{
              console.log(err)
            })
          }else{
            this.http.post(u_url,body,{headers}).subscribe(res=>{
              // console.log("unservedaddsingle:",res);
              // this.details[i].res.additional_order[index].serving_status=0;
              this.details[i].res.additional_order[index].serving_status=0;
                // this.details[i].res.tag_status='';
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[index].serving_status=0;
                
                // this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='';
              const idx=this.selectedAdd.map(x=>x.item).indexOf(orderid);
          if(idx>=0){
            this.selectedAdd.splice(idx,1);
          }
          if(!this.selectedItem.length){
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
           
          }
          if(!this.selectedAdd.length){
           
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
          }
          console.log("unservedaddsingle:",this.details[i].res,this.details[i].res.tag_status);
          if(!this.details[i].res.order.order_items.map(x=>x.serving_status).includes(1)&&!this.details[i].res.additional_order.map(x=>x.serving_status).includes(1)){
            this.count.to_consume++;
            if(this.count.served>0){
            this.count.served--;
            }
            this.details[i].res.tag_status='default';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
            console.log('everything unserved,status default:',this.details[i].res.tag_status,this.details[i].res.order,this.details[i].res.additional_order,this.selectedItem[0].Resident,this.selectedAdd[0].Resident);
            
          }
          

            },err=>{
              console.log(err)
            })
          }
        }
      }
      }
      }
    
      showSelectedItems(id) {
        let s;
        const idx = this.selectedItem.map(x => x.item).indexOf(id);
        if (idx >= 0) {
          s = true;
        } else {
          s = false;
        }
        return s;
      }
    
      async addItems(item) {
        const modal = await this.modalCntrl.create({
          component: DiningAddItemsComponent,
          cssClass: 'full-width-modal',
          componentProps: {
            date: this.date,
            serve_time: this.ser_time,
            item: item,
            menu_id:this.menu_id
          }
        });
        modal.onDidDismiss().then(() => {
          this.loading=true;
            // this.getDetails(this.wing)
            this.getDetailsNew();
        });
        return await modal.present();
      }

      cancel(){
        this.hide=true;
        this.terms='';
        console.log('cancel:',this.hide);
      }
      search(){
        this.hide=false;
      }

      expandProfile(item){
        this.selected=[];
        if (item.expanded) {
          item.expanded = false;
        } else {
          this.details.map(listItem => {
            if (item == listItem) {
              listItem.expanded = !listItem.expanded;
              if(listItem.expanded){
                this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
            this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
                console.log('expandeditem:',listItem);
                if(item.res.order&&item.res.order.length){
                if(item.res.order.order_items&&item.res.order.order_items.length){
                  item.res.order.order_items.forEach(element => {
                    if(element.serving_status==1){
                      this.selectedItem = [{ Resident: item.res.user.user_id, item: element.id , qty:element.served_quantity }];
                    }
                  });
                }
              }
              if(item.res.additional_order&&item.res.additional_order){
                item.res.additional_order.forEach(element => {
                  if(element.serving_status==1){
                    this.selectedAdd = [{ Resident: item.res.user.user_id, item: element.id , qty:element.served_quantity }];
                  }
                });
              }
              }
            } else {
              listItem.expanded = false;
            }
            return listItem;
          });
        }
       }

       async showComment(item,ev){
        const popover = await this.popCntlr.create({
          component: CbCommentComponent,
          event:ev,
          backdropDismiss:true,
          componentProps:{
            data:item
          },
         
          
          
        });
        return await popover.present();
      }


      async consumed(item,i){
        if ((this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0)&&(this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0)) {
    
        } else if (this.selectedItem[0].Resident == item.res.user.user_id) {
        // const modal = await this.modalCntrl.create({
        //   component: ServingConsumedComponent,
        //   cssClass: 'dining-consumed-modal',
        //   componentProps: {
        //     res:item.res.user.name
        //   }
        // });
        // modal.onDidDismiss().then((dataReturned) => {
        //   if(dataReturned.data){
          this.serve(item,i);
          // }
        // });
        // return await modal.present();
      }
      }

      async unserve(item,ev,i){
        let id;
        if(item.res.order){
          id=item.res.order.id
        }else if(item.res.additional_order&&item.res.additional_order.length){
          id=item.res.additional_order[0].order_id
        }
        
        const popover = await this.popCntlr.create({
          component: UnserveComponent,
          event: ev,
          backdropDismiss: true,
          cssClass: 'dining-more-options-popover',
          componentProps: {
            
            order_id:id,
            flag:1
            
          },
        });
          popover.onDidDismiss().then((dataReturned) => {
            if(dataReturned.data){
              this.details[i].res.tag_status='default';
              this.details[i].res.order.order_items.map(x=>x.serving_status=0);
              this.details[i].res.additional_order.map(x=>x.serving_status=0);
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order.map(x=>x.serving_status=0);
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items.map(x=>x.serving_status=0);
                
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
              // this.details[i].res.order[0].order_items.forEach((ele,index)=>{
              //   this.details[i].res.order[0].order_items[index].serving_status=0
              // })
              // this.details[i].res.additional_order.forEach((ele,index)=>{
              //   this.details[i].res.additional_order[index].serving_status=0
              // })
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
            this.selected=[];
            if(this.count.served>0){
              this.count.served--;
            
            }
            // this.count.toserved++;
            this.count.to_consume++;
               }
              
          });
    
        
        return await popover.present();
      }

      async serveAll(item,i){
        
        this.selected=[];
        this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
            this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
        // if(item.res.order&&item.res.order.length){
        //   this.order_id=item.res.order[0].id
        // }else if(item.res.additional_order&&item.res.additional_order.length){
        //   this.order_id=item.res.additional_order[0].order_id
        // }
        if(item.res.order){
          this.order_id=item.res.order.id
        }else if(item.res.additional_order&&item.res.additional_order.length){
          this.order_id=item.res.additional_order[0].order_id
        }

          // if(item.res.order&&item.res.order.length){
          //   if(item.res.order[0].order_items&&item.res.order[0].order_items.length){
          //   item.res.order[0].order_items.forEach((ele,index)=>{
              
            
              
          //     this.selectItems(item,ele.id,ele.order_id,1,ele,i,index)
              
          //   })
           
          // }
          // }
          if(item.res.order){
            item.res.order.order_items.forEach((ele,index)=>{
             
              this.selectItems(item,ele.id,ele.order_id,1,ele,i,index)
              
            })
            
          }
          // if(item.res.additional_order&&item.res.additional_order.length){
          //   item.res.additional_order.forEach((ele,index)=>{
              
             
          //     this.selectItems(item,ele.id,ele.order_id,2,ele,i,index)
                  
          //   })
           
          // }
          if(item.res.additional_order&&item.res.additional_order.length){
            item.res.additional_order.forEach((ele,index)=>{
             
              this.selectItems(item,ele.id,ele.order_id,2,ele,i,index)
            })
            
          }
          console.log('all:',this.selectedItem,this.selectedAdd)
         
      }

      async showOptions(options,ev){
    
        const popover = await this.popCntlr.create({
          component: ItemOptionsComponent,
          event:ev,
          backdropDismiss:true,
          componentProps:{
            data:options,
            flag:1
          },
         
          
          
        });
        return await popover.present();
      }
      async changeCount(item,order,i,j,k,ev){
        if(this.edit){
        const popover = await this.popCntlr.create({
          component: ServingChangeCountComponent,
          event:ev,
          id:'countPop',
          backdropDismiss:false,
          componentProps:{
            quantity:order.served_quantity
          },
         
          
          
          
        });
        popover.onDidDismiss().then((dataReturned) => {
          console.log(dataReturned.data)
          // if(k==1){
          //   this.details[i].res.order[0].order_items[j].served_quantity=dataReturned.data;
          // }else{
          //   this.details[i].res.additional_order[j].served_quantity=dataReturned.data;
          // }
          if(k==1){
            this.details[i].res.order.order_items[j].served_quantity=dataReturned.data;
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[j].served_quantity=dataReturned.data;
          }else{
            this.details[i].res.additional_order[j].served_quantity=dataReturned.data;
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[j].served_quantity=dataReturned.data;
          }
            
        });
        return await popover.present();
      }
      }

      async markasserved(ev){
        const popover = await this.popCntlr.create({
          component: MarkAsServedComponent,
          cssClass:'mark-served-popover',
          event:ev,
          backdropDismiss:true,
          componentProps:{
           
          },
         
          
          
        });
        popover.onDidDismiss().then((dataReturned) => {
          if(dataReturned.data){
            this.markAllServed();
          }
        })
        return await popover.present();
      }

      async markAllServed(){
        let url=this.config.domain_url+'mark_all_as_served';
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
        let headers=await this.config.getHeader();
        let body={
          company_id:cid,
          date:moment(this.date).format('YYYY-MM-DD'),
          servingtimeid:this.ser_time,
          wing_id:[this.wing],
          searchvalue:null,
          serveareaid:0,
          diettype:null,
          served_by:uid

        }
        console.log('markallbo:',body)
        this.http.post(url,body,{headers}).subscribe(res=>{
          console.log('markall:',res);
          this.presentAlert('All marked as served')
          this.ionViewWillEnter();
        })
      }

      async undoRefuse(item,ev,i){
        let id;
        // if(item.res.order&&item.res.order.length){
        //   id=item.res.order[0].id
        // }else if(item.res.additional_order&&item.res.additional_order.length){
        //   id=item.res.additional_order[0].order_id
        // }
        if(item.res.order){
          id=item.res.order.id
        }else if(item.res.additional_order&&item.res.additional_order.length){
          id=item.res.additional_order[0].order_id
        }
        const popover = await this.popCntlr.create({
          component: UndoRefuseComponent,
          event: ev,
          backdropDismiss: true,
          cssClass: 'dining-more-options-popover',
          componentProps: {
            
            order_id:id
            
          },
        });
          popover.onDidDismiss().then((dataReturned) => {
            if(dataReturned.data){
              this.details[i].res.tag_status='default';
                // this.detailsCopy[i].res.tag_status='default';
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
                
                this.details[i].res.order.refuse_status=0;
                // this.detailsCopy[i].res.order.refuse_status=0;
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_status=0;
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
            this.selected=[];
            if(this.count.refused>0){
              this.count.refused--;
            
            }
            this.count.to_consume++;
            // this.count.toserved++;
               }
              
          });
    
        
        return await popover.present();
      }

      async getDetailsNew(){
        this.details=[];
        this.detailsCopy=[];
        const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
       
        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'get_serving_resident_detail_list';
        console.log(url);
        let body;
        body={
          serving_time_id:this.ser_time,
          date:moment(this.date).format('YYYY-MM-DD'),
          wing_id_arr:[this.wing],
          serving_area_id:'0',
         
        }
       
        if(this.terms){
          body.keyword=this.terms
        }
        console.log('body:',body);
        // this.http.post(url,body,{headers}).subscribe((data:any)=>{
        //   console.log('count:',data);
          
        //   this.count=data.data.count;
        // })
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          
          console.log('res:',res);
       
          if(res.data.menu_id){
            this.count=res.data.count;
            
          this.serving_order=res.data.serving_order;
          this.menu_id=res.data.menu_id;
          for(let i in res.data.residents){
          this.details.push({res:res.data.residents[i],expanded:false})
          this.detailsCopy.push({res:res.data.residents[i],expanded:false})
          }
          this.loading=false;
          console.log('deta:',this.details)
        }else{

          this.back();
            this.presentAlert('No menu available')
        }
        },error=>{
          this.loading=false
          console.log(error);
          
        })
       }
}
