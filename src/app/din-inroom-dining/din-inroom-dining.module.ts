import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinInroomDiningPageRoutingModule } from './din-inroom-dining-routing.module';

import { DinInroomDiningPage } from './din-inroom-dining.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinInroomDiningPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinInroomDiningPage]
})
export class DinInroomDiningPageModule {}
