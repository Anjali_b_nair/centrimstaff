import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitorBookingListPage } from './visitor-booking-list.page';

const routes: Routes = [
  {
    path: '',
    component: VisitorBookingListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisitorBookingListPageRoutingModule {}
