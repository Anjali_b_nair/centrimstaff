import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisitorBookingListPageRoutingModule } from './visitor-booking-list-routing.module';

import { VisitorBookingListPage } from './visitor-booking-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisitorBookingListPageRoutingModule,
    ApplicationPipesModule,
    CalendarModule
  ],
  declarations: [VisitorBookingListPage]
})
export class VisitorBookingListPageModule {}
