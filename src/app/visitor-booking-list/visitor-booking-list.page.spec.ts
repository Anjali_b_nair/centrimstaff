import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisitorBookingListPage } from './visitor-booking-list.page';

describe('VisitorBookingListPage', () => {
  let component: VisitorBookingListPage;
  let fixture: ComponentFixture<VisitorBookingListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorBookingListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisitorBookingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
