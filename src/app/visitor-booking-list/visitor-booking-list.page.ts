import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
import { CbInfoComponent } from '../components/cb-info/cb-info.component';
import { CbMoreComponent } from '../components/cb-more/cb-more.component';
import { HttpConfigService } from '../services/http-config.service';
import  moment from 'moment-timezone';

@Component({
  selector: 'app-visitor-booking-list',
  templateUrl: './visitor-booking-list.page.html',
  styleUrls: ['./visitor-booking-list.page.scss'],
})
export class VisitorBookingListPage implements OnInit {
  subscription:Subscription;
  date:Date;
  date_1:any;
  today:any;
  current:Date=new Date();
  callList:any[]=[];
  timezone:any;
  // terms:any;
  staffArray:any[]=[];
staff:any;
staff_id:any;
type:any='all';
wingArray:any[]=[];
wing:any;
wingId:any;
  constructor(private platform:Platform,private router:Router,private http:HttpClient,private config:HttpConfigService,
    private popCntlr:PopoverController,private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {
    this.date=this.current;
    this.today=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
  }
 async ionViewWillEnter(){
  // this.terms='';
  // const tabBar = document.getElementById('myTabBar');
  // tabBar.style.display="none";
  this.wingArray=[];
  this.getWing();

  const bid=await this.storage.get("BRANCH")

  const utype=await this.storage.get("USER_TYPE")

    const id=await this.storage.get("USER_ID")

    let headers=await this.config.getHeader();;
      if(utype==3){
        let  url=this.config.domain_url+'get_staff_wing/'+id;
        // let body={
        //   user_id:id
        // }
        // console.log('staff wing:',body);
        
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log('wing:',res);
          this.wingId=res[0].wing_id;
          let url1=this.config.domain_url+'branch_wings/'+bid;
          this.http.get(url1,{headers}).subscribe((res:any)=>{
            res.data.details.forEach(ele=>{
              console.log("el:",ele.id,this.wingId);
              
              if(ele.id==this.wingId){
                this.wing=ele.name;
              }
            })
            console.log("stafffff:",this.wing);
          })
      
        })
        
        
      }else{
        this.wingId='all';
        this.wing="All wings";
      }


   




  this.getContent();
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu-booking']) ;
  }); 

}

selecttype(){
    this.getContent();
}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

async getContent(){
 
    const bid=await this.storage.get("BRANCH")

      
  let url=this.config.domain_url+'visit_booking_by_staff';
  let headers=await this.config.getHeader();
  
  
 
  let body={
    wing_id:this.wingId,
    date:this.date_1,
    type:this.type
  }
  console.log("body:",body);
  
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("res:",res);
    this.callList=res.data;
    
  })

}


async getWing(){
  this.wingArray=[];
  this.wingArray.push({id:'all',wing:'All wings'});
//  this.wingArray.push({id:0,wing:'All facilities'});
 
   const bid=await this.storage.get("BRANCH")
   let headers=await this.config.getHeader();;

     const utype=await this.storage.get("USER_TYPE")

     let branch=bid.toString();
     let body={company_id:branch}
     let url=this.config.domain_url+'branch_wings/'+bid;
     this.http.get(url,{headers}).subscribe((res:any)=>{
      //  console.log("wing:",res);
       for(let i in res.data.details){
        // console.log("data:",res.data.details[i]);
        
      let obj={id:res.data.details[i].id,wing:res.data.details[i].name};
   
         this.wingArray.push(obj);
         
       
       }
       console.log("array:",this.wingArray);
       
       
     },error=>{
       console.log(error);
      })
   
 }
 setWing(wing){
  this.wingArray.forEach(element => {
    if(element.wing==wing){
      this.wingId=element.id;
    }
  });
  console.log("wingId:",this.wingId);
  this.getContent();
}

onChange(ev){
console.log("cdate:",this.date);
this.date_1=this.date;
this.getContent();
}
 //  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}
getTimeZone(timezone){

  return moment().tz(timezone).format('hh:mm');
}



async showComment(item,ev){
  const popover = await this.popCntlr.create({
    component: CbCommentComponent,
    cssClass:'vb-comments-popover',
    event:ev,
    backdropDismiss:true,
    componentProps:{
      data:item.comment
    },
   
    
    
  });
  return await popover.present();
}


async showInfo(ev,item){
  const popover = await this.popCntlr.create({
    component: CbInfoComponent,
    event:ev,
    backdropDismiss:true,
    componentProps:{
      data:item
    },
 
    
    
  });
  return await popover.present();
}











async showPeople(ev,item){
  let data=[];
  let other=[];
  if(item.family.length>0){
    data=item.family;
    // fam=1;
  }
   if(item.other_visitor_list.length>0){
    other=item.other_visitor_list;
    // fam=0
  }
 
  const popover = await this.modalCntl.create({
    component: CbMoreComponent,
    // event:ev,
    cssClass:'cbmorepop',
    backdropDismiss:true,
    componentProps:{
      data:data,
      other:other,
      call:0
    },
    
    
    
  });
  return await popover.present();
}


}
