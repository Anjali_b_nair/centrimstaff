import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningSelectChoicePage } from './dining-select-choice.page';

const routes: Routes = [
  {
    path: '',
    component: DiningSelectChoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningSelectChoicePageRoutingModule {}
