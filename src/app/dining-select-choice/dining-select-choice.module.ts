import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningSelectChoicePageRoutingModule } from './dining-select-choice-routing.module';

import { DiningSelectChoicePage } from './dining-select-choice.page';
import { ApplicationPipesModule } from '../application-pipes.module';
// import { AboutInfoComponent } from '../components/about-info/about-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningSelectChoicePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningSelectChoicePage]
})
export class DiningSelectChoicePageModule {}
