import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DietaryProfileComponent } from '../components/dining/dietary-profile/dietary-profile.component';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
@Component({
  selector: 'app-dining-select-choice',
  templateUrl: './dining-select-choice.page.html',
  styleUrls: ['./dining-select-choice.page.scss'],
})
export class DiningSelectChoicePage implements OnInit {
  subscription:Subscription;
  wingArray:any[]=[];
  menu:any;
  date:any;
  pending:any[]=[];
  servingTime:any[]=[];
  selectedList:any='1';
  offset:any=0;
  completed:any[]=[];
  completed_order:any;
  wing:any;
  pending_count:any;
  terms:any;
  hide:boolean=true;
  cutOff:any;
  loading:boolean=true;
  pendingRes:any[]=[];
  completedRes:any[]=[];
  constructor(private modalCntrl:ModalController,private router:Router,private platform:Platform,
    private http:HttpClient,private storage:Storage,private config:HttpConfigService,private route:ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
this.offset=0;
this.completed=[];
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    this.date=this.route.snapshot.paramMap.get('date');
    this.hide=true;
    this.terms='';
    this.getCutoffTime();
    this.getWing();
    this.getCompletedList(false, "");
    this.getPendingCount();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   changeView(i){
     if(i==1){
       this.selectedList='1';
       this.getPendingList(this.wingArray[0].id);
       
       this.wingArray.map(item=>{
        if(item.expanded){
          item.expanded=false
        }
       })
       this.wingArray[0].expanded=true;
       this.loading=true;
     }else{
       this.selectedList='2';
      //  this.offset=0;
      //  this.getCompletedList(false, "");
     }
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   
     back(){
       this.router.navigate(['/dining-choose-season-date',{menu:JSON.stringify(this.menu)}],{replaceUrl:true})
     }

async dietaryProfile(){
  const modal = await this.modalCntrl.create({
    component: DietaryProfileComponent,
    cssClass: 'set-dprofile-alert-modal',
   
  });
  
  modal.onDidDismiss().then((dataReturned) => {
    
  });
  
  return await modal.present();

  
}
async getWing(){
  this.wingArray=[];


   const bid=await this.storage.get("BRANCH")
   let headers=await this.config.getHeader();

     let branch=bid.toString();
     let body={company_id:branch}
     let url=this.config.domain_url+'branch_wings/'+bid;
     this.http.get(url,{headers}).subscribe((res:any)=>{
       console.log("wing:",res);
       let expanded;
       for(let i in res.data.details){
        console.log("data:",res.data.details[i]);
        if(i=='0'){
          expanded=true;
        }else{
          expanded=false
        }
      let obj={id:res.data.details[i].id,wing:res.data.details[i].name,expanded:expanded};
   
         this.wingArray.push(obj);
      
       
       }

       this.getPendingList(this.wingArray[0].id);
       console.log("array:",this.wingArray);
       
       
     },error=>{
       console.log(error);
       
     })
  
 }
 expand(item){
  if (item.expanded) {
    item.expanded = false;
  } else {
    this.wingArray.map(listItem => {
      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
        if(listItem.expanded){
          this.getPendingList(listItem.id);
          this.loading=true;
        }
      } else {
        listItem.expanded = false;
      }
      return listItem;
    });
  }
 }

 async getPendingList(wing){
  this.pending=[];
  const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'load_pending_consumers_list_via_wing_id';
    let body={
      dining_id:this.menu.id,
      date:moment(this.date).format('YYYY-MM-DD'),
      wing_id:wing
    }
    console.log('body:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.pending=res.data.consumers;
      this.servingTime=res.data.serving_time;
      this.wing=res.data.wing.name;
      this.loading=false;
    },error=>{
      this.loading=false;
    })

 }
 setServingTimeCompleted(item,id){
  let c;
    item.completed_dining_order.map(x => {
      if (id == x.serving_time_id) {
        c=true;
        }
      })
      console.log('return:',c)
      return c;
 }
 setServingTimeSkipped(item,id){
  let c;
  item.skipped_dining_order.map(x => {
    if (id == x.serving_time_id) {
      c=true;
      }
    })
    return c;
}
setServingTimePending(item,id){
  let c;
  item.pending_dining_order.map(x => {
    if (id == x.serving_time_id) {
      c=true;
      }
    })
    return c;
}
async getCompletedList(isFirstLoad, event){

  const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'load_completed_consumers_list';
    let body={
      dining_id:this.menu.id,
      date:moment(this.date).format('YYYY-MM-DD'),
      offset:this.offset,
      limit:12
    }
    console.log('bodycom:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('completed:',res);
      for (let i = 0; i < res.data.consumers.length; i++) {
        this.completed.push(res.data.consumers[i]);
      }
      this.servingTime=res.data.serving_time;
      this.completed_order=res.data.total_cons
      if (isFirstLoad)
      event.target.complete();
 
    this.offset=this.offset+12;
    },error=>{
      console.log(error)
    })
}
doInfinite(event) {
  this.getCompletedList(true, event)
  }

  gotoOrder(item){
    this.router.navigate(['/dining-ind-res-order',{menu:JSON.stringify(this.menu),date:this.date,consumer:JSON.stringify(item),wing:this.wing}])
  }

  async getPendingCount(){
    
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'dining_consumers_pending_in_a_branch_count';
        let body={
          dining_id:this.menu.id,
          date:moment(this.date).format('YYYY-MM-DD'),
          
        }
        console.log('body:',body);
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("count:",res);
          this.pending_count=res.data;
        })
  }
  cancel(){
    this.hide=true;
    this.terms='';
    console.log('cancel:',this.hide);
  }
  search(){
    this.hide=false;
  }

  async getCutoffTime(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'get_dining_ordering_details';
    let body={
      dining_id:this.menu.id,
      company_id:cid,
      consumer_user_id:uid
      
    }
    console.log('body:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      let d=this.date+' '+res.data.start_time
      console.log("cutoff:",res,d);
      this.cutOff=moment(d).format('hh:mm A')
    })
  }

  async searchResidents(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    let wing=[];
    this.wingArray.forEach(ele=>{
      wing.push(ele.id)
    })
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'load_dining_all_consumers_via_branch';
    let body={
      dining_id:this.menu.id,
      date:moment(this.date).format('YYYY-MM-DD'),
      wings:wing,
      keyword:this.terms
      
    }
    console.log('body:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("residents:",res);
      this.completedRes=res.data.consumers.completed;
      this.pendingRes=res.data.consumers.pending
      
      
      
    })
  }
}


