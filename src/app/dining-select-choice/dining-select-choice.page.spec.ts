import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningSelectChoicePage } from './dining-select-choice.page';

describe('DiningSelectChoicePage', () => {
  let component: DiningSelectChoicePage;
  let fixture: ComponentFixture<DiningSelectChoicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningSelectChoicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningSelectChoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
