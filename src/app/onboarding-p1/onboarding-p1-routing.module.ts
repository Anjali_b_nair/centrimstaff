import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingP1Page } from './onboarding-p1.page';

const routes: Routes = [
  {
    path: '',
    component: OnboardingP1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnboardingP1PageRoutingModule {}
