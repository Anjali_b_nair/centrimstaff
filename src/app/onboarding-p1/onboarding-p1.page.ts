import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-onboarding-p1',
  templateUrl: './onboarding-p1.page.html',
  styleUrls: ['./onboarding-p1.page.scss'],
})
export class OnboardingP1Page implements OnInit {
  email:any;
  name:any;
  uname:any;
  phone:any;
  img:any;
  info:any;
  mobile:any;
  subscription:Subscription;
    constructor(private actionsheetCntlr:ActionSheetController,
      private config:HttpConfigService,private router:Router,private storage:Storage,
      private platform:Platform,private http:HttpClient,private toast: ToastController, 
      private loadingCtrl: LoadingController,private route:ActivatedRoute) { }
  
    ngOnInit() {
    }
  
    ionViewWillEnter(){
      this.info=JSON.parse(this.route.snapshot.paramMap.get('info'));
      this.email=this.info.email;
      this.name=this.info.name;
      this.uname=this.info.uname;
      if(this.info.phone){
        if(this.info.phone.substring(0,2)==61){
          if(this.info.phone.slice(2).substring(0,1)==0){
            this.phone=this.info.phone.slice(2)
          }else{
            this.phone='0'+this.info.phone.slice(2)
          }
        }else{
          this.phone=this.info.phone
        }
      }
      console.log('phone:',this.info.phone.slice(2));
      
      if(this.info.mobile){
        if(this.info.mobile.substring(0,2)==61){
          if(this.info.mobile.slice(2).substring(0,1)==0){
            this.mobile=this.info.mobile.slice(2)
          }else{
            this.mobile='0'+this.info.mobile.slice(2)
          }
        }else{
          this.mobile=this.info.mobile
        }
      }
      this.img=this.info.img;
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
        this.back();
      });     
    }
    ionViewDidLeave(){
      //  this.dismissLoader();
        this.subscription.unsubscribe();
    }
   back(){
    this.router.navigate(['/onboarding-welcome',{info:JSON.stringify(this.info)}])
   }
    async selectImg() {
      const actionSheet = await this.actionsheetCntlr.create({
        header: "Profile photo",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            this.pickImage();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.captureImage();
          }
        },
        
        {
          text: 'Cancel',
          // role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
  async captureImage(){
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
   
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    
    this.uploadImage(base64Image);
  }
    async pickImage() {
      const image = await Camera.pickImages({
        quality: 90,
        correctOrientation:true,
        limit:1
        
      });
    
    
      for (var i = 0; i < image.photos.length; i++) {
              console.log('Image URI: ' + image.photos[i]);
              
              
              const contents = await Filesystem.readFile({
                path: image.photos[i].path
              });
              
               
                this.uploadImage('data:image/jpeg;base64,' + contents.data)
              
                
           
               
          }
    }
  
    async uploadImage(img) {
      // if(this.isLoading==false){
      this.showLoading();
      // }
      let url = this.config.domain_url + 'upload_file';
      let headers=await this.config.getHeader();;
      let body = { file: img }
      console.log("body:", img);
  
      this.http.post(url, body,{headers}).subscribe((res: any) => {
        console.log("uploadedpic:", res);
       
         
          this.img=res.data;
          this.dismissLoader();
        
      }, error => {
        // this.imageResponse.slice(0,1);
        console.log(error);
        this.dismissLoader();
        this.presentToast('Upload failed.Please try again.')
  
      })
    }
    async showLoading() {
  
      const loading = await this.loadingCtrl.create({
        cssClass: 'custom-loading',
        message: 'Please wait...',
        spinner: null,
        id: 'loader'
        // duration: 3000
      });
      return await loading.present();
    }
  
    async dismissLoader() {
  
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
  
    async presentToast(mes) {
      const alert = await this.toast.create({
        message: mes,
        duration: 3000,
        cssClass: 'toast-mess',
        position: 'top'
      });
      alert.present();
    }
  
    next(){
      var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!this.name||!this.uname){
        this.presentToast('Please fill all required fields.');
      }
      else if(this.email&&!pattern.test(this.email)){
        this.presentToast('Please enter a valid email Id.');
      }else if(this.phone&&(this.phone.toString().length<10|| this.phone.toString().length>10)){
        this.presentToast('Please enter a 10 digit phone number.');
      }else if(this.mobile&&(this.mobile.toString().length<10|| this.mobile.toString().length>10)){
        this.presentToast('Please enter a 10 digit mobile number.');
      }else {
        this.validateUsername();
      
      
      }
    }
  
    async validateUsername(){
  
      let url=this.config.domain_url+'validate_username';
      let headers=await this.config.getHeader();;
      const uid=await this.storage.get('USER_ID');
      let body={
        user_id:uid,
        username:this.uname
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        if(res.data){
          this.presentToast('Username already exist.');
        }else{
          let ph=null;
          // if(this.phone&&this.phone.substring(0,2)==61){
            ph=this.phone
          // }else if(this.phone){
          //   ph='61'+this.phone
          // }
          
        let data={email:this.email,name:this.name,uname:this.uname,phone:ph,img:this.img,mobile:this.mobile}
      this.router.navigate(['/onboarding-p2',{info:JSON.stringify(data)}]);
        }
      },err=>{
        console.log(err)
      })
  
    }
}

