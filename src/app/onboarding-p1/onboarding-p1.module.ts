import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingP1PageRoutingModule } from './onboarding-p1-routing.module';

import { OnboardingP1Page } from './onboarding-p1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnboardingP1PageRoutingModule
  ],
  declarations: [OnboardingP1Page]
})
export class OnboardingP1PageModule {}
