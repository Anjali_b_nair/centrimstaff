import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';

import { FilterConsumersComponent } from '../components/filter-consumers/filter-consumers.component';
import { ViewGroupConsumersComponent } from '../components/view-group-consumers/view-group-consumers.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumers',
  templateUrl: './consumers.page.html',
  styleUrls: ['./consumers.page.scss'],
})
export class ConsumersPage implements OnInit {
consumers:any[]=[];
flag:any;
terms:any;
subscription:Subscription;
type:any;
create:boolean=false;
finance:boolean=false;
offset:any = 1;
res_type:any=1;
filter_status:any=false;
  constructor(private http:HttpClient,private router:Router,private config:HttpConfigService,
    private route:ActivatedRoute,private popCntlr:PopoverController,private storage:Storage,
    private platform:Platform,private loadingCtrl:LoadingController,private modalCntl:ModalController) { }

  ngOnInit() {
  }
 async ionViewWillEnter(){

  // this.storage.ready().then(()=>{
  //   const data=await this.storage.get("USER_ID")

  //     const tok=await this.storage.get("TOKEN")

  //       let token=tok;
  //       const bid=await this.storage.get("BRANCH")


    //       let branch=bid.toString();
    // let headers=await this.config.getHeader();
    
   this.terms=null;
      this.offset=1;
      this.consumers=[];
                
                this.flag=this.route.snapshot.paramMap.get('flag');
                this.showLoading();
                
                this.getAllresidents(false,'');
             
                const bid=await this.storage.get("BRANCH")

                  const type=await this.storage.get('USER_TYPE')
                    this.type=type;
               
              
                (await this.config.getUserPermission()).subscribe((res: any) => {
                  console.log('permissions:', res);
                  let routes = res.user_routes;
                  if (res.main_permissions.residents==1&&routes.includes('residents.create')) {
            
                    this.create = true
                  } else {
            
                    this.create = false;
                    
                  }
                  if (res.main_permissions.finance==1&&routes.includes('finance.index')) {

                    this.finance= true
                  } else {
            
                    this.finance = false;
                    
                  }
                })
              

              
              
        this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
          this.router.navigate(['/menu']) ; 
      }); 
      
        }
        ionViewWillLeave() { 
          this.subscription.unsubscribe();
       }

 gotoDetails(item){
   if(this.flag==1){
  // this.router.navigate(['/consumer-profile',{id:item.id,flag:1}])
  this.router.navigate(['/resident-profile',{id:item.id,flag:1}])
   }else{
    if(this.finance)
    this.router.navigate(['/petty-cash',{flag:2,id:item.id}])
   }
 }


 cancel(){
  // this.hide=false;
  this.terms='';
  this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
}
search(){
  // this.hide=true;
  this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
}


async group(ev){
  const popover = await this.popCntlr.create({
    component: ViewGroupConsumersComponent,
    event: ev,
    backdropDismiss:true,
    cssClass:'filterpop'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned !== null) {
      let type = dataReturned.data;
      
      
      //alert('Modal Sent Data :'+ dataReturned);
      if(type==1){
        this.router.navigate(['/create-group']);
      }else if(type==2){
       this.router.navigate(['/view-group']);
      }
    }
  });
  return await popover.present();
}
async filter(ev){
  const popover = await this.modalCntl.create({
    component: FilterConsumersComponent,
 
    backdropDismiss:true,
    cssClass:'res-filter-modal'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data) {
      this.filter_status=true;
     let type = dataReturned.data;
     this.res_type=type;
     this.offset=1;
     this.consumers=[];
     this.getAllresidents(false,'');
     
    }
  });
  return await popover.present();
}


// async activeUsers(){

//     const cid=await this.storage.get("COMPANY_ID")

//       const bid=await this.storage.get("BRANCH")

//         let url=this.config.domain_url+'residents?type=1';
//         let headers=await this.config.getHeader();
//         console.log(url);
      
//         // let headers=await this.config.getHeader();
//         this.http.get(url,{headers}).subscribe((data:any)=>{
         
//           console.log("actdata:",data);
//           this.consumers=data.data
         
            
//         },error=>{
//           console.log(error);
//         });
     
 
// }
// async inactiveUsers(){
 
//     const cid=await this.storage.get("COMPANY_ID")

//       const bid=await this.storage.get("BRANCH")

//         let url=this.config.domain_url+'residents?type=2';
//         let headers=await this.config.getHeader();
//         console.log(url);
      
//         // let headers=await this.config.getHeader();
//         this.http.get(url,{headers}).subscribe((data:any)=>{
         
//           console.log("inactdata:",data);
//           this.consumers=data.data
         
            
//         },error=>{
//           console.log(error);
//         });
    
// }
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}


 // single function to list residents - new api


 async getAllresidents(isFirstLoad,event){
  

  // let url=this.config.domain_url+'get_all_residents_in_branch_with_filter';
  let url=this.config.domain_url+'get_all_residents_list_via_filters';  
  let headers=await this.config.getHeader();

  let body;
  body={
    type:this.res_type,  // 1 - active 2 -inactive 3 - no contract
    sort:'fname',  // sortby
    order:'ASC',   // ASC or DESC
  
    page:this.offset, 
    
  }

  if(this.terms){
  body.keyword=this.terms
  }
               console.log('body:',body,headers);
               
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
                 
    console.log("data:",data);

    for (let i = 0; i < data.data.residents.length; i++) {
      this.consumers.push(data.data.residents[i]);
    }

    if (isFirstLoad)
    event.target.complete();
    this.dismissLoader();
    this.offset=++this.offset;    
        
    },error=>{
      this.dismissLoader();
      console.log(error);
    });

}
doInfinite(event) {
  this.getAllresidents(true, event)
  }
  removeFilter(){
    this.filter_status=false;
   
     this.res_type=1;
     this.offset=1;
     this.consumers=[];
     this.getAllresidents(false,'');
  }
}
