import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumersPage } from './consumers.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumersPageRoutingModule {}
