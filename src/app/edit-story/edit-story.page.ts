import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, HostListener, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { File } from '@ionic-native/file/ngx';
import { CreateThumbnailOptions, VideoEditor } from '@ionic-native/video-editor/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Subscription } from 'rxjs';
import { TaggedUsersComponent } from '../components/tagged-users/tagged-users.component';
import { VisibletoComponent } from '../components/visibleto/visibleto.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';

import { HTTP } from '@ionic-native/http/ngx';
import { CaptureVideoOptions, MediaCapture, MediaFile, CaptureError } from '@ionic-native/media-capture/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
// import { element } from 'protractor';
import { ConsentTagAlertComponent } from '../components/consent-tag-alert/consent-tag-alert.component';

import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { FilePath } from '@ionic-native/file-path/ngx';
declare var $: any;

@Component({
  selector: 'app-edit-story',
  templateUrl: './edit-story.page.html',
  styleUrls: ['./edit-story.page.scss'],
})
export class EditStoryPage implements OnInit {
  type:any;
  title:any;
  description:any;
  status:any;
  minDate: string = new Date().toISOString();
  date:any;
  categories:any=[];
  category:any=[];
  categoryname:any;
  branch:any=[];
  facility:any[]=[];
  facilityname:any;
  isUrl:boolean=false;
  isImg:boolean=false;
  isVdo:boolean=false;
  isDoc:boolean=false;
  img:any=[];
  imageResponse:any=[];
  // vdo:any=[];
  vdoResponse:any=[];
  docs:any=[];
  docResponse:any=[];
  media:any=[];
  vdo:any[]=[];
  vdoThumb:any=[];
  urlThumb:any;
  hasAttach:boolean=false;
  vdoUrl:any;
  url:any[]=[];
  MAX_FILE_SIZE = 24 * 1024 * 1024;
  base64File:any;
  flag:any;
  id:any;
  attendees:any[]=[];
  tags:any[]=[];
  subscription:Subscription;
  visible_type:any;
  storyType:any=[];
  user_id:any;
  cid:any;
  descArea:any;
  isLoading:boolean=false;
  content:any;


fac_loading:boolean=false;
from:any;
disable:boolean=false;
hide_comment:any;
publish_status:boolean=false;

loading:boolean=true;
public customOptions: any = {
  header: "Categories"
};
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private file:File,private alertCntlr:AlertController,
    private editor:VideoEditor,private route:ActivatedRoute,private platform:Platform,private router:Router,
    private modalCntrl:ModalController,private toastCntlr:ToastController,private https:HTTP,
    private loadingCtrl:LoadingController,private nav:NavController,private actionsheetCntlr:ActionSheetController,
    private mediacapture:MediaCapture,private chooser:Chooser,private filePath:FilePath) {

      


     }
     @HostListener('touchstart')
     onTouchStart() {
       console.log('pageload:',this.loading)
       this.loading=false;
     }
    
  ngOnInit() {
    $('#summernote2').summernote({
      placeholder: 'Enter text here',
      tabsize: 2,
      height: 120,
      toolbar: [
        ['font', ['bold', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
      ],
      
    });
    
  }
  

async ionViewWillEnter(){
  

  this.disable=false;
  
 this.flag=this.route.snapshot.paramMap.get('flag');
 if(this.flag==3){
   this.cid=this.route.snapshot.paramMap.get('cid');
   this.from=this.route.snapshot.paramMap.get('from');
 }
 this.id=this.route.snapshot.paramMap.get('id');
 this.description=this.route.snapshot.paramMap.get('description');
this.content=document.querySelector('#content');
(await this.config.getUserPermission()).subscribe((res: any) => {
  console.log('permissions:', res);
  let routes = res.user_routes;
  if (res.main_permissions.life_style==1&&routes.includes('story.publish')) {

    this.publish_status = true
  } else {

    this.publish_status = false;
   
  }
})
//  $('#summernote2').summernote('code',this.description);
  this.getContent();
  // (await this.config.getUserPermission()).subscribe((res: any) => {
  //   console.log('permissions:', res);
  //   let routes = res.user_routes;
  //   if (routes.includes('story.publish')) {

  //     this.publish_status = true
  //   } else {

  //     this.publish_status = false;
  //     this.status=2
  //   }
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
    this.back();
 
  
    }); 
    
}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}
setType(t){
  this.type=t;
}
async showLoading() {
  this.isLoading=true;
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    message: 'Please wait...',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
  this.isLoading=false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
  async getContent(){
    this.ngOnInit();
    this.docs=[];
    this.categories=[];
    this.tags=[];
    this.attendees=[];
    this.branch=[];
        this.imageResponse=[];
        this.img=[];
        // this.vdo=[];
        this.url=[];
        // this.type=1;
       this.facility=[];
       this.category=[];
        this.vdoResponse=[];
        this.docResponse=[];
        this.media=[];
        this.vdoThumb=[];
        // this.urlThumb=[];
        this.vdo=[];
    this.showLoading();
    // this.flag=this.route.snapshot.paramMap.get('flag');
    //     this.id=this.route.snapshot.paramMap.get('id');
    
      
      
        const bid=await this.storage.get("BRANCH")

      
      const data=await this.storage.get("USER_ID")
      const type=await this.storage.get("USER_TYPE")

      const cid=await this.storage.get("COMPANY_ID")
      
        this.user_id=data;
    let url=this.config.domain_url+'storydetails';
    let body={
      post_id:this.id,
      user_id:this.user_id
  
    }
    let headers=await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.type=res.story.post_type;
        // this.storyType=[{id:1,type:'Story'},{id:2,type:'Announcement'}];
        this.title=res.story.title;
        this.description=res.story.description;
        // this.imageResponse=res.story.images;
        // this.vdoResponse=res.story.videos;
        
        this.visible_type=res.story.visible_type;
        $('#summernote2').summernote('code',this.description);
        // $('#summernote2').summernote('code').set(res.story.description)
        this.date=new Date(res.story.activate_date.replace(' ','T')+'.000Z').toISOString();;
        
        
        
        this.status=res.story.status;
        console.log("date:",this.date,res.story.status,this.status);
        this.hide_comment=res.story.hide_comment;
        if(res.story.images.length>0){
          this.hasAttach=true;
            this.isImg=true;
          res.story.images.forEach(element => {
            this.imageResponse.push(element.post_attachment);
            this.img.push(element.post_attachment);
            // this.media.push(element.post_attachment);
          });
        }
        // if(res.story.videos.length>0){
        //   res.story.videos.forEach(element => {
        //     this.vdoResponse.push(element.activity_image);
        //     this.media.push(element.activity_image);
        //   });
        // }
        
        if(res.story.tagged_users.length>0){
        res.story.tagged_users.forEach(element => {
          this.tags.push({id:element.user_id,name:element.name})
        });
        }
        console.log("tags:",this.tags,this.type);
        if(res.story.visible_users.length>0){
          res.story.visible_users.forEach(element => {
            if(element.user_type_id==5){
            this.attendees.push(element.user_id)
            }
          });
          }
        
        
        
        
       
        // this.tags=[];
        
      if(res.story.attachments.length>0){
        this.hasAttach=true;
            this.isDoc=true;
        res.story.attachments.forEach(el=>{
          let fileName =  el.post_attachment.substring(el.post_attachment.lastIndexOf('/')+1);
            let name=fileName.split('.',1);
            if(name.toString().length>3){
              fileName=name.toString().substring(0,3)+'...'+el.post_attachment.substring(el.post_attachment.lastIndexOf('.')+1);
            }
          this.docs.push(el.post_attachment);
          this.docResponse.push({name:fileName})
        })
        console.log('doc:',this.docResponse)
      }
    //  if(res.story.images.length>0){
    //     res.story.images.forEach(el=>{
    //       this.media.push(el.post_attachment);
    //     })
    //   } 
      if(res.story.videos.length>0){
        res.story.videos.forEach(el=>{
          if(el.url==null){
            this.hasAttach=true;
            this.isVdo=true;
            let fileName =  el.file.substring(el.file.lastIndexOf('/')+1);
            let name=fileName.split('.',1);
            if(name.toString().length>3){
              fileName=name.toString().substring(0,3)+'...'+el.file.substring(el.file.lastIndexOf('.')+1);;
            }
            this.vdoResponse.push({name:fileName})
            this.vdo.push(el.file);
            this.vdoThumb.push(el.thumbnail);
          }else{
            this.hasAttach=true;
            this.isUrl=true;
          this.vdoUrl=el.url;
          this.urlThumb=el.thumbnail;
          }
          
        })
      }
      console.log("img:",this.img);
      console.log("vdo:",this.vdo);
     
        let cat_url=this.config.domain_url+'activity_categories';
       
        this.http.get(cat_url,{headers}).subscribe((res:any)=>{
          console.log("cat:",res);
          // this.categories.push({id:0,category_name:'Select All'});
          res.data.forEach(element => {
            this.categories.push({id:element.id,category_name:element.category_name})
          });
          console.log("cat1:",this.categories);
          
        })
        
         

      
            // this.branch.push({id:0,name:'Select All'})
              // this.user_type=data;
              let url=this.config.domain_url+'branch_viatype';
              let body={
                company_id:cid,
                role_id:type
              }
              if(type=='1'){
                console.log("role1");
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                res.data.forEach(element => {
                  this.branch.push({id:element.id,name:element.name});
                });
              })
            }else{
             
        
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          res.data.forEach(element => {
            this.branch.push({id:element.id,name:element.name});
          });
        })
        
      }
        if(res.story.category.length>0){
          // this.category=res.story.category[0].category_name;
          this.category.push(res.story.category[0].id.toString());
          res.story.category.forEach(element => {
            if(element.category_name!=res.story.category[0].category_name){
              // this.categoryname=this.categoryname+','+element.category_name;
              this.category.push(element.id.toString());
            }
          });
        }
        if(res.story.branches.length>0){
          // this.facility=res.story.branches[0].name;
          this.facility.push(res.story.branches[0].id.toString());
          res.story.branches.forEach(element => {
            if(element.name!=res.story.branches[0].name){
              // this.facility=this.facility+','+element.name;
              this.facility.push(element.id.toString());
              
            }
            
          });
          
        }
         
        console.log("cats:",this.category);
        console.log("bran:",this.facility);
        

          this.fac_loading=true;
        
        
        // var branch=<HTMLInputElement>document.getElementById('fac');
          
        // branch.value=this.facility;
     
            // })
          
        
        
        // if(this.images.length==0){
        //   this.videos.forEach(element => {
            
        //     let item={img:element.thumbnail,vid:1};
        //     this.media.push(item);
        //   });
        // }else if(this.videos.length==0){
        //   this.images.forEach(element => {
        //     let item={img:element.post_attachment,vid:0};
        //     this.media.push(item);
        //   });
        // }else{
        //   this.images.forEach(element => {
        //     let item={img:element.post_attachment,vid:0};
        //     this.media.push(item);
        //   });
        //   this.videos.forEach(element => {
            
        //     let item={img:element.thumbnail,vid:1};
        //     this.media.push(item);
        //   });
        // }
  
  
  
  
  
  
        var mon = new Array();
        mon[0] = "Jan";
        mon[1] = "Feb";
        mon[2] = "Mar";
        mon[3] = "Apr";
        mon[4] = "May";
        mon[5] = "Jun";
        mon[6] = "Jul";
        mon[7] = "Aug";
        mon[8] = "Sep";
        mon[9] = "Oct";
        mon[10] = "Nov";
        mon[11] = "Dec";
  
        let t=new Date(res.story.activate_date);
        let d=mon[t.getMonth()]+' '+t.getDate()+' '+t.getFullYear();
  
      //   var hours = t.getHours();
      //   let minutes = t.getMinutes();
      //   var ampm = hours >= 12 ? 'pm' : 'am';
      //   hours = hours % 12;
      //   hours = hours ? hours : 12; // the hour '0' should be '12'
      //  var hour= hours < 10 ? '0'+hours:hours
      //   var minute = minutes < 10 ? '0'+minutes : minutes;
      //   this.a= d+', '+hour + ':' + minute + ' ' + ampm;
      //   console.log(this.story_time);
        
        // res.story.attachments.forEach(el=>{
        //   let attch_name=el.post_attachment.substr(el.post_attachment.lastIndexOf('_') + 1);
        //   // let ext=el.post_attachment.substr(el.post_attachment.lastIndexOf('.') + 1);
        //   this.docResponse.push({name:attch_name});
          // console.log("ext:",ext)
        // })
        // console.log("date_1:",this.doc);
        
        this.dismissLoader();
      },error=>{
        console.log(error);
        this.dismissLoader()
      
    })
    
  
  
  }
 
  select(event){
    if(!this.loading){
    if(event.currentTarget.checked==true){
      this.status=1
    }else{
      this.status=0
    }
    console.log("notify",event.currentTarget.checked);
  }
  }
  hideComment(event){
    if(!this.loading){
    if(event.currentTarget.checked==true){
      this.hide_comment=1
    }else{
      this.hide_comment=0
    }
    console.log("notify",event.currentTarget.checked);
  }
  }

  videoUrl(){
    this.hasAttach=true;
    this.isUrl=true;
    // this.isImg=false;
    //   this.isVdo=false;
    //   this.isDoc=false;
  }
  
    async publish(){
      this.disable=true;
      let vdoUrl;
      this.media=[];
      if(this.vdoUrl==undefined && this.url.length==0){
        this.vdoUrl=null;
        this.urlThumb=null;
      }else{
        
           
        
        
        let img;
      if((this.vdoUrl).includes('www.youtube.com')){
        // console.log("you",i.url)
        let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('=') + 1);
        img='http://img.youtube.com/vi/'+id+'/hqdefault.jpg';
        this.urlThumb=img
      }else if((this.vdoUrl).includes('youtu.be')){
        
        let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('/') + 1);
        img='http://img.youtube.com/vi/'+id+'/hqdefault.jpg';
        this.urlThumb=img
      } else if((this.vdoUrl).includes('vimeo.com')){
  
        let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('/') + 1);
         
         
          this.https.get('https://vimeo.com/api/oembed.json?url=https://player.vimeo.com/video/'+id,{},{'Content-Type': 'application/json','Accept':'application/vnd.vimeo.*+json;version=3.4'})
            .then(res=>{console.log('vimeo:',res);
           img=(JSON.parse(res.data)).thumbnail_url
           console.log("thumbnail:",img);
           this.urlThumb=img;
      })
    }
      
    }
      let attendee_id;
      
      
      if(this.attendees.length>0){
      let a=this.attendees[0];
    this.attendees.forEach(ele=>{
      if(ele==a){
      attendee_id=ele;
      }else{
        attendee_id=attendee_id+','+ele
      }
    })
  }else{
    attendee_id=null;
  }
    let tags=[];
    if(this.tags.length>0){
    this.tags.forEach(ele=>{
      tags.push(ele.id);
    })
  }
  if(this.img.length>0){
    this.img.forEach(element => {
      this.media.push(element);
    });
  }
  if(this.vdo.length>0){
    this.vdo.forEach(element=>{
      this.media.push(element);
    })
  }
  console.log("media:",this.media);
  
    // this.vdo.forEach(ele=>{
    //   this.media.push(ele);
    // })
    // let urlthumb;
    // if(this.urlThumb.length>0){
    // let t=this.urlThumb[0];
    // this.urlThumb.forEach(ele=>{
    //   if(ele==t){
    //   urlthumb=ele;
    //   }else{
    //     urlthumb=urlthumb+','+ele
    //   }
    // })
    // }else{
    //   urlthumb=null
    // }
    let vdothumb=[];
    this.media.forEach((element,i)=> {
      let ext=element.substr(element.lastIndexOf('.')+1);
      console.log('ext:',ext);
      if(this.vdoThumb.length>0){
        if(ext=='mp4'||ext=='avi'||ext=='mkv'||ext=='mov'){
          this.vdoThumb.forEach((ele) => {
            console.log("vdo[i]:",vdothumb[i]);
            // if(vdothumb[i]==undefined||){
            //   vdothumb[i]=ele
            // }else{
            //   console.log("index already populated");
              
            // }
            
            if(vdothumb.includes(ele)){
                console.log("include ele:",ele);
                
            }else if(vdothumb[i]!=undefined){
                console.log("index already populated");
                
            }else{
            vdothumb[i]=ele;
            }
          });
        }
      }
    });
      
      
      
      
        
          
    if(vdothumb.length==0){
      vdothumb=null
    }
    console.log("vdoThumb:",this.vdoThumb);
    
    console.log("thumb[]:",vdothumb);
    console.log("media:",this.media);
  
    let category;
    console.log("categ:",this.category);
    
    if(this.category.length>0){
     category=this.category
          
        
     
    }else{category=null}
    let facility;
    console.log("facility:",this.facility);
    
    if(this.facility.length>0){
      
      let f=this.facility[0];
        this.facility.forEach(ele => {
          if(ele==f){
            facility=f;
          }else{
            facility=facility+','+ele;
          }
        });
   
  }else{facility=null}
     
        const cid=await this.storage.get("COMPANY_ID")

          const uid=await this.storage.get("USER_ID")

            const tz=await this.storage.get('TIMEZONE')
            let url=this.config.domain_url+'story/'+this.id;
            var m = moment.utc(this.date).utcOffset('-0530').format("yyyy-MM-DD HH:mm:ss"); // parse input as UTC
            console.log(m); // local time
            
            console.log(moment.utc(this.date).tz(tz).format("yyyy-MM-DD HH:mm:ss")); // 30-03-2017 2:34:22 AM
            
            console.log(moment.utc(moment(m).tz(tz)).format('yyyy-MM-DD HH:mm:ss'));

          //  console.log("date:",date);
           
          //  let utc=new Date(Date.UTC(date.getUTCFullYear(),date.getUTCMonth(),date.getUTCDate(),date.getUTCHours(),date.getUTCMinutes(),date.getUTCSeconds()))
            let body={
              story:{
                post_type:this.type,
                title:this.title,
                description:$('#summernote2').summernote('code'),
                company_id:cid,
                activate_date:moment.utc(moment(m).tz(tz)).format('yyyy-MM-DD HH:mm:ss'),
                status:this.status,
                visible_type:this.visible_type,
                created_by:uid,
                web_display:1,
                enable_notification:1,
                updated_by:uid,
                
              },
              categories:category,
              branches:facility,
              document:this.docs,
              tags:tags,
              attendees:attendee_id,
              url:this.vdoUrl,
              images:this.media,
              // videos:this.vdo,
              video_thumbnail:vdothumb,
              url_thumbnail:this.urlThumb,
              hide_comment:this.hide_comment
            }
            console.log("body:",body);
            let headers=await this.config.getHeader();
          this.http.put(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
  
            if(res.status=='success'){
              this.presentAlert("Story edited successfully.");
              this.back();
              this.disable=false;
              // this.router.navigate(['/stories']);
            }
            
          },error=>{
            this.back();
            this.disable=false;
          })
          
      
    }
  
  
  
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }
  
    async captureImage(){
      this.hasAttach=true;
      this.isImg=true;
     

      const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: false,
        resultType: CameraResultType.Base64,
        correctOrientation:true,
        source:CameraSource.Camera,
       
      });
    
      
      var imageUrl = image.base64String;
    
      // Can be set to the src of an image now
      let base64Image = 'data:image/jpeg;base64,' + imageUrl;
      console.log('image:',imageUrl);
      this.imageResponse.push(base64Image);
      this.uploadImage(base64Image);

    }
    async pickImage(){
      this.hasAttach=true;
      this.isImg=true;
     
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    }
  
    async selectImage() {
      const actionSheet = await this.actionsheetCntlr.create({
        header: "Select Image source",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            this.pickImage();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.captureImage();
            // this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          // role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
  
  
  
    async uploadImage(img){
      if(this.isLoading==false){
        this.showLoading();
        }
      let url=this.config.domain_url+'upload_file';
      let headers=await this.config.getHeader();;
      let body={file:img}
    console.log("body:",img);
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        
        // this.img.push(res.data);
        this.img.push(res.data);
        this.dismissLoader();
        // console.log("uploaded:",this.img);
      },error=>{
        console.log(error);
        this.dismissLoader();
      })
    }
  
  
  
   async pickVdo(){
    let headers=await this.config.getHeader();;
      this.chooser.getFile()
      .then(file => {
        console.log(file ? file : 'canceled');
        if(file){
        if(file.mediaType=="video/x-flv" || file.mediaType=="video/mp4" || file.mediaType=="application/x-mpegURL"
        || file.mediaType=="video/MP2T" || file.mediaType=="video/3gpp"
        || file.mediaType=="video/quicktime"|| file.mediaType=="video/x-msvideo"|| file.mediaType=="video/x-ms-wmv"){
         
          this.vdoResponse.push({name:file.name});
          let uri;
        if(this.platform.is('android')){
          this.filePath.resolveNativePath(file.uri)
          .then(filePath => {console.log('filePath:',filePath);
          uri=filePath;
          this.createThumbnail(uri)})
          .catch(err => console.log('err:',err));
        }else{
          uri=file.uri
          this.createThumbnail(uri)
        }
          // this.createThumbnail(file.uri)
      this.hasAttach=true;
      // this.isImg=false;
      this.isVdo=true;
          let  url = this.config.domain_url+'upload_file';
          let body={file:file.dataURI}
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
           //  that.vdo.push(res.data);
            this.vdo.push(res.data);
            this.dismissLoader();
          },error=>{
            console.log(error);
            this.dismissLoader();
            
          })
         
        }else{
          this.alert('File format not supported');
        }
      }
      })
      .catch((error: any) => console.error(error));


   
    }



    async selectVdo() {
      const actionSheet = await this.actionsheetCntlr.create({
        header: "Select Image source",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            this.pickVdo();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.captureVdo();
          }
        },
        {
          text: 'Cancel',
          // role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
    
    
    
   
      
    captureVdo(){
      console.log('recording vdo')
     
      let options: CaptureVideoOptions = { limit: 1 }
      this.mediacapture.captureVideo(options)
        .then(
          (data: MediaFile[]) =>{ console.log("capturedVdo:",data)
          let fileUrl=data[0].fullPath;
          this.file.resolveLocalFilesystemUrl(fileUrl).then(fileEntry => {
           
            fileEntry.getMetadata((metadata) => {
                console.log("sizee:",metadata.size);//metadata.size is the size in bytes
               if(metadata.size>this.MAX_FILE_SIZE){
                 this.alert('File is larger than 24MB');
                 console.log('large size');
                 
               }
               else{
                let ext;
                if((fileUrl).includes('.mp4')){
                  ext='mp4'
                }else if((fileUrl).includes('.mkv')){
                  ext='mkv'
                }else if((fileUrl).includes('.mov')){
                  ext='mov'
                }else{
                  ext='avi'
                }
                
                  console.log("vdo:",fileUrl);
                  
                // this.createThumbnail(fileUrl)
                this.uploadVdo(fileUrl,ext);
                this.createThumbnail(fileUrl)
               }
            })
           
        })
      },
          (err: CaptureError) => console.error(err)
        );
  
    }
  
    async uploadVdo(file,ext){
      this.showLoading();
      let fileName =  file.substring(file.lastIndexOf('/')+1);
      let name=fileName.split('.',1)
      if(name.toString().length>3){
        fileName=name.toString().toString().substring(0,3)+'...'+ext;
      }
      this.vdoResponse.push({name:fileName});
      this.hasAttach=true;
      // this.isImg=false;
      this.isVdo=true;
      let path =  file.substring(0,  file.lastIndexOf("/") + 1);
      //path=path.replace('Users/','');
      console.log("path:",path);
       console.log('name:',fileName);
       let headers=await this.config.getHeader();

       const contents = await Filesystem.readFile({
        path: file
      });
      let  url = this.config.domain_url+'upload_file';
      let body={file:'data:video/'+ext+';base64,'+contents.data}
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
       //  that.vdo.push(res.data);
        this.vdo.push(res.data);
        this.dismissLoader();
      },error=>{
        console.log(error);
        this.dismissLoader();
        
      })
     
    }
  
  
  
  
  selectDoc(){
    this.chooser.getFile()
  .then(file => {
    console.log(file ? file : 'canceled');
    if(file){
      if(file.mediaType=="application/pdf" || file.mediaType=="application/msword" || file.mediaType=="application/doc"
      || file.mediaType=="application/ms-doc" || file.mediaType=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      || file.mediaType=="application/excel"|| file.mediaType=="application/vnd.ms-excel"|| file.mediaType=="application/x-excel"
      || file.mediaType=="application/x-msexcel"|| file.mediaType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
      this.uploadDoc(file.dataURI,file.name);
      
    }else{
      this.alert('File format not supported');
    }
  }
  })
  .catch((error: any) => console.error(error));
  }
  async uploadDoc(file,fname){
this.showLoading();
    let fileName ;
    let name=fname.split('.',1)
    if(name.toString().length>3){
      fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
    }
    this.docResponse.push({name:fileName});
    this.hasAttach=true;
 
      this.isDoc=true;
    // let path =  file.substring(0,  file.lastIndexOf("/") + 1);
    // //path=path.replace('Users/','');
    // console.log("path:",path);
    //  console.log('name:',fileName);
    //  let that=this;
    //  this.file.resolveLocalFilesystemUrl(file).then((entry: any)=>{
    //    entry.file(function (file) {
    //      var reader = new FileReader();
   
    //      reader.onloadend = function (encodedFile: any) {
    //        var src = encodedFile.target.result;
    //        src = src.split("base64,");
    //        var contentAsBase64EncodedString = src[1];
    //        that.base64File=contentAsBase64EncodedString;
    //        console.log("base:",contentAsBase64EncodedString);
    //        console.log("vdo:",that.base64File);
           let  url = this.config.domain_url+'upload_file';
           let headers=await this.config.getHeader();;
           let body={file:file}
          // console.log("body:",img);
  
    
           // let headers=await this.config.getHeader();
           this.http.post(url,body,{headers}).subscribe((res:any)=>{
             console.log("doc up:",res);
             this.docs.push(res.data);
             this.dismissLoader();
           },error=>{
             console.log(error);
             this.dismissLoader();
           })
           
    //      };
    //      reader.readAsDataURL(file);
    //    })
    //  }).catch((error)=>{
    //    console.log(error);
    //  })
  
  
  
  
  //   let url=this.config.domain_url+'upload_file';
  //   let body={file:doc}
  // console.log("body:",this.docs);
  
  //   this.http.post(url,body).subscribe((res:any)=>{
  //     console.log("uploaded:",res);
  //     this.doc.push(res.data);
      
  //   })
  }
  
  
  createThumbnail(file){
   
  
    var option:CreateThumbnailOptions = {
            fileUri:file,
            width:160, 
            height:206, 
            atTime:1, 
            outputFileName: 'sample', 
            quality:100 };
      this.editor.createThumbnail(option).then(result=>{
        console.log("thumb:",result);
        this.uploadThumb('file://'+result);
          //result-path of thumbnail
        //  localStorage.setItem('videoNum',numstr.toString());          
      }).catch(e=>{
       // alert('fail video editor');
      });
    
  //   VideoEditor.createThumbnail(
  //     this.createThumbnailSuccess,
  //     this.createThumbnailError,
  //     {
  //         fileUri: file.fullPath,
  //         outputFileName: videoFileName,
  //         atTime: 2,
  //         width: 320,
  //         height: 480,
  //         quality: 100
  //     }
  // );
  }
  
  
  async uploadThumb(file){
    let fileName =  file.substring(file.lastIndexOf('/')+1);
    // this.vdoResponse.push({name:fileName});
    let path =  file.substring(0,  file.lastIndexOf("/") + 1);
    //path=path.replace('Users/','');
    console.log("path:",path);
     console.log('name:',fileName);
     const contents = await Filesystem.readFile({
      path: file
    });
  
    let  url = this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
           let body={file:'data:image/jpg;base64,'+contents.data}
          // console.log("body:",img);
  
    
           // let headers=await this.config.getHeader();
           this.http.post(url,body,{headers}).subscribe((res:any)=>{
             console.log(res);
            //  that.vdo.push(res.data);
             this.vdoThumb.push(res.data);
           },error=>{
             console.log(error);
             
           })

    //  let that=this;
    //  this.file.resolveLocalFilesystemUrl(file).then((entry: any)=>{
    //    entry.file(function (file) {
    //      var reader = new FileReader();
   
    //      reader.onloadend = function (encodedFile: any) {
    //        var src = encodedFile.target.result;
    //        src = src.split("base64,");
    //        var contentAsBase64EncodedString = src[1];
    //        that.base64File=contentAsBase64EncodedString;
    //        console.log("base:",contentAsBase64EncodedString);
    //        console.log("vdo:",that.base64File);
    //        let  url = that.config.domain_url+'upload_file';
    //        let body={file:'data:application/jpg;base64,'+that.base64File}
    //       // console.log("body:",img);
  
    
    //        // let headers=await this.config.getHeader();
    //        that.http.post(url,body).subscribe((res:any)=>{
    //          console.log(res);
    //         //  that.vdo.push(res.data);
    //          that.vdoThumb.push(res.data);
    //        },error=>{
    //          console.log(error);
             
    //        })
           
    //      };
    //      reader.readAsDataURL(file);
    //    })
    //  }).catch((error)=>{
    //    console.log(error);
    //  })
  }
  
  
    async alert(mes){
      // let message;
      // if(type==2){
      //   message='File format not supported'
      // }
      const alert = await this.alertCntlr.create({
        mode:'ios',
        message: mes,
        backdropDismiss:true
      });
    
      await alert.present();
    }
  
    async taggedConsumer(){
      let tags=[];
    if(this.tags.length>0){
  this.tags.forEach(ele=>{
    tags.push(ele.id);
  })
}
      const modal = await this.modalCntrl.create({
        component: TaggedUsersComponent,
        cssClass:'full-width-modal',
        componentProps: {
          data:tags
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == null || dataReturned.data==undefined) {

        }else{
          console.log("dataret:",dataReturned);
          
          this.tags=dataReturned.data;
          
          
        }
      });
      return await modal.present();
    }
    async visibleto(){
      if(this.tags.length>0){
        this.visible_type=4;
        this.tags.forEach(ele=>{
          if(this.attendees.indexOf(ele.id)<0){
          this.attendees.push(ele.id);
          }
        })
      }
      const modal = await this.modalCntrl.create({
        component: VisibletoComponent,
        cssClass:'full-width-modal',
        componentProps: {
         data:this.attendees,
         type:this.visible_type,
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null) {
          console.log("dataret:",dataReturned);
          
          
          this.attendees=dataReturned.data;
          this.visible_type=dataReturned.role;
        }
      });
      return await modal.present();
    }
    back(){
      // $('#summernote2').summernote('reset');
      this.router.navigate(['/story-details',{post_id:this.id,flag:this.flag,cid:this.cid,from:this.from}],{replaceUrl: true});
      this.dismissLoader();
      
    }
    removeImg(i){
      this.imageResponse.splice(i,1);
      this.img.splice(i,1);
      // this.media.splice(i,1);
    }
  
    removeVdo(i){
      this.vdoResponse.splice(i,1);
      this.vdo.splice(i,1);
      this.vdoThumb.splice(i,1);
    }
    removeDoc(i){
      this.docResponse.splice(i,1);
      this.docs.splice(i,1);
    }

    confirm(){
      // let desc=$('#summernote2').summernote('code');
      let no_consent=[];
      if(this.tags.length>0){
        this.tags.forEach(ele=>{
          if(ele.consent==0){
            no_consent.push(ele.name);
          }
        })
      }
      if(this.title==undefined||this.title==''){
        this.presentAlert('Please enter the title.');
      }else if(this.facility.length==0){
        this.presentAlert('Please select atleast one facility.');
      }else if(this.vdoUrl&&!this.isValidUrl(this.vdoUrl)){
        this.presentAlert('Please enter a valid url');
        
      }
      // else if(desc==undefined||desc==''||desc=='<p><br></p>'){
      //   this.presentAlert('Please enter the description.');
      // }else if(no_consent.length>0){
      //   this.showAlert(no_consent);
      else{
        this.disable=true;
        this.publish();
      }
    }
  
    async showAlert(item){
      const modal = await this.modalCntrl.create({
        component: ConsentTagAlertComponent,
       
        componentProps: {
         data:item,
        
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == null || dataReturned.data==undefined) {
      
        }else{
          this.disable=true;
          this.publish();
        }
      });
      return await modal.present();
    }
   

    selectFacility(facility){

        console.log("changefac:",facility,this.facility);
        
    }


    async presentActionSheet() {
      const actionSheet = await this.actionsheetCntlr.create({
        header: 'Select',
        buttons: [{
          text: 'Image',
          // role: 'destructive',
          icon: 'image-outline',
          handler: () => {
            this.selectImage();
            console.log('Delete clicked');
          }
        }, {
          text: 'Video',
          icon: 'film-outline',
          handler: () => {
            this.selectVdo();
            console.log('Share clicked');
          }
        }, {
          text: 'Video url',
          icon: 'link-outline',
          handler: () => {
            this.videoUrl();
            console.log('Play clicked');
          }
        }, {
          text: 'Document',
          icon: 'document-outline',
          handler: () => {
            this.selectDoc();
            console.log('Favorite clicked');
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          // role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }

    async viewImage(){
      const modal = await this.modalCntrl.create({
        component: AttchedImagesComponent,
       
        componentProps: {
         data:this.imageResponse,
         
         
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        
      });
      return await modal.present();
    }

    isValidUrl(string) {
      // try {
      //   new URL(string);
      //   return true;
      // } catch (err) {
      //   return false;
      // }
      const pattern = new RegExp(
        '^([a-zA-Z]+:\\/\\/)?' + // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR IP (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
          '(\\#[-a-z\\d_]*)?$', // fragment locator
        'i'
      );
      return pattern.test(string);
    }
}
