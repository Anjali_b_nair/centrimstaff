import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackDashboardPage } from './feedback-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackDashboardPageRoutingModule {}
