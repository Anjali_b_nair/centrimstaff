import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Platform, ModalController, ActionSheetController, ToastController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';
import { HttpConfigService } from '../services/http-config.service';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-feedback-dashboard',
  templateUrl: './feedback-dashboard.page.html',
  styleUrls: ['./feedback-dashboard.page.scss'],
})
export class FeedbackDashboardPage implements OnInit {
  sid: any;
  lid: any;
  cid: any;
  id: any;
  cat_id: any;
  flag: any;
  questions: any = [];
  subscription: Subscription;
  noOfItem: number = 1;
  que_id: any;
  opt_id: any;
  selected: any;
  feedback: any;
  // suggestion:boolean=true;
  show: any = 1;
  feed_suggestion: any = [];
  name: any;
  mail: any;
  phone: any;
  nextQue: boolean = false;
  preQue: boolean = false;
  public surveyData: { QuestionId: number, OptionId: number }[];
  keyboardStyle = { width: '100%', height: '0px' };
  feedtext: any;
  feednexthide: boolean;
  img: any = [];
  imageResponse: any = [];
  opt: any;
  type: any;
  res: any;
  disabled: boolean = false;
  disableSkip: boolean = false;
  role: any;
  utype:any;
  res_name:any;
  isLoading:boolean=false;
  // con_info:boolean=false;
  constructor(private router: Router, private orient: ScreenOrientation, private route: ActivatedRoute, private storage: Storage,
    private http: HttpClient, private config: HttpConfigService, private platform: Platform, private modalCntlr: ModalController,
    private keyboard: Keyboard, private actionsheetCntlr: ActionSheetController,
    private toastCntlr: ToastController,private loadingCtrl:LoadingController) { }

  ngOnInit() {
    
    this.keyboard.onKeyboardWillShow().subscribe({
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe({
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });


  }
  inputChange() {
    if (this.feedtext == undefined || this.feedtext == '') {
      this.feednexthide = true;
    } else {
      this.feednexthide = false;
    }
  }
  async ionViewWillEnter() {
    this.disabled = false;
    this.disableSkip = false;
    this.surveyData = [{ QuestionId: 0, OptionId: 0 }];

    // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display = "none";

    this.sid = this.route.snapshot.paramMap.get("sid");
    this.lid = this.route.snapshot.paramMap.get("lid");
    // this.flag = this.route.snapshot.paramMap.get("flag");
    this.cat_id = this.route.snapshot.paramMap.get("cat_id");
    this.role = this.route.snapshot.paramMap.get("role");
    this.type = this.route.snapshot.paramMap.get("type");
    if (this.type == 2) {
      this.getSuggestion();
      // this.suggestion=false;
      this.show = 2
    } else {
      // this.suggestion=true;
      this.show = 1
      
        const dat=await this.storage.get("PCFBRANCH")
          this.id = dat;
          const res=await this.storage.get("SURVEY_CID")
            this.cid = res;
            const data=await this.storage.get("TOKEN");
            const bid=await this.storage.get("BRANCH");
            const com=await this.storage.get("COMPANY_ID");
            let url = this.config.feedback_url + 'get_review_questions';
            // let body={
            //   company_id:14,
            //   user_id:3,
            //   survey_bid:8,
            //   language_id:1
            // }
            let body = `company_id=${this.cid}&branch_id=${this.id}&review_bid=${this.sid}&language_id=${this.lid}&category_id=${this.cat_id}`;

            console.log("body:", body);

            let headers = new HttpHeaders({
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
              
              
            })

            this.http.post(url, body, { headers }).subscribe((data: any) => {
              this.questions = data.qanda
              console.log(data);

            })
          
    }

    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.back();
    });

  }
  back() {
    // if(this.flag==1){
    //   this.router.navigate(['/menu']) ;
    // } else{  
    //   this.router.navigate(['/feedback']) ;
    // }
    // this.router.navigate(['/select-category',{sid:this.sid,flag:this.flag}]);
    this.router.navigate(['/feedback',{cat_id:this.cat_id,type:this.type,role:this.role,flag:1}],{replaceUrl:true})
  }
  next() {
    if (this.noOfItem <= this.questions.length) {
      this.nextQue = false;
      this.noOfItem += 1;
      this.preQue = true;
      if (this.noOfItem > this.questions.length) {
        // this.suggestion=true;
        this.show = 2;
        this.getSuggestion();

      }
      
    } else {
      console.log('END OF DAY 1')
    }
    // console.log("no:",this.noOfItem,"len:",this.questions.length)
  }
  previous() {
    if(this.type==2||this.noOfItem==1){
      this.back();
    }else if(this.type!==2&&this.show==2){
      this.show=1;
      this.noOfItem+=-1;
      this.surveyData.unshift({ QuestionId: 0, OptionId: 0 });
    }else if(this.show==3){
      this.show=2
    }else if(this.show==4){
      this.show=3
    }else {
    if (this.noOfItem <= this.questions.length) {
      
      this.nextQue = true;

      if (this.noOfItem != 1) {
        this.noOfItem += -1;
      } else {
        this.noOfItem = 1;
      }
      // if (this.noOfItem == 1) {
      //   this.preQue = false;
      // } else {
      //   this.preQue = true;
      // }
    } else {
      console.log('END OF DAY 1')
    }
  }
    console.log("no:", this.noOfItem, "len:", this.surveyData,this.surveyData[this.noOfItem])
  }
  ionViewDidLeave() {
    // this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
    // const tabBar = document.getElementById('myTabBar');
    //   tabBar.style.display="flex";
    this.subscription.unsubscribe();
  }



  getOption(item, ques) {

    const targetIdx = this.surveyData.map(item => item.QuestionId).indexOf(ques.id);
    if (targetIdx >= 0) {

      this.surveyData[targetIdx].OptionId = item.id;

    }
    else {
      if (ques.type == 7) {
        this.surveyData.push({ QuestionId: ques.id, OptionId: this.feedtext })
        this.feedtext=undefined;
      } else {
        this.surveyData.push({ QuestionId: ques.id, OptionId: item.id });
      }
    }
    if (this.noOfItem == this.questions.length) {
      console.log("initial:", this.surveyData);

      this.surveyData.splice(0, 1);
      console.log("data:", this.surveyData);
      for (let data of this.surveyData) {
        //  
        if (this.que_id == '' || this.que_id == undefined)
          this.que_id = data.QuestionId;
        if (this.que_id == data.QuestionId) {
          console.log("firstQue");

        } else {
          this.que_id += "*" + data.QuestionId;
        }
      }

      for (let data of this.surveyData) {
        //  
        if (this.opt_id == '' || this.opt_id == undefined)
          this.opt_id = data.OptionId;
        if (this.opt == '' || this.opt == undefined)
          this.opt = data.OptionId;
        if (this.opt_id == data.OptionId) {
          console.log("firstOpt");

        } else {
          this.opt_id += "*_*" + data.OptionId;
          this.opt += "*" + data.OptionId;
        }
      }
    }

    // if(this.que_id==''){
    //   this.que_id=id;
    // }else{
    //   this.que_id=this.que_id+'*'+id;
    // }
    // if(this.opt_id==''){
    //   this.opt_id=item.id;
    // }else{
    //   this.opt_id=this.opt_id+'*'+item.id;
    // }
    if (this.noOfItem >= 1) {
      this.preQue = true;
    }
    if (this.noOfItem <= this.questions.length) {

      this.noOfItem += 1;
      if (this.noOfItem > this.questions.length) {
        // this.suggestion=false;
        this.show = 2;
        // this.surveyData.splice(0  ,1); 


        this.getSuggestion();
        // console.log("sug:",this.suggestion);
      }
    } else {
      console.log('END OF DAY 1')
    }
    this.selected = item.id;
    console.log("que:", this.surveyData, "opt:", this.noOfItem,this.surveyData[this.noOfItem].OptionId,item.id);

  }
  async getSuggestion() {
    this.feed_suggestion = [];
   

      const res=await this.storage.get("SURVEY_CID");
      const data=await this.storage.get("TOKEN");
      const bid=await this.storage.get("BRANCH");
      const com=await this.storage.get("COMPANY_ID");
        this.cid = res;

        let url = this.config.feedback_url + 'get_rsuggestion';
        // let body={
        //   company_id:14,
        //   user_id:3,
        //   survey_bid:8,
        //   language_id:1
        // }
        let body = `company_id=${this.cid}&question_id=${this.que_id}&option_id=${this.opt}`;

        console.log("body:", body);

        let headers = new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          
          
        })

        this.http.post(url, body, { headers }).subscribe((data: any) => {
          for(let i in data.feedback_text)
          this.feed_suggestion.push(data.feedback_text[i])
          console.log(data);


        })
     
  }


  setSuggestion(item,i) {
    if (this.feedback == undefined || this.feedback == '') {
      this.feedback = item;
    } else {
      this.feedback += ',' + item;
    }
    this.feed_suggestion.splice(i,1)
  }


  getDetails() {
    if (this.feedback == undefined || this.feedback == ''||this.feedback==null) {
      this.presentAlert('Please enter your feedback.')
    } else {
      if(this.role==1){
        this.submit();
      }else{
      this.show = 3
      }
    }
  }
  selectUtype(i){
    this.utype=i;
    this.show=4;
  }
  async submit() {

    // var pattern = new RegExp('(https?:\\/\\/)?' + // protocol
    //   '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    //   '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    //   '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    //   '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    //   '(\\#[-a-z\\d_]*)?$', 'i');
    var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
     

        const res=await this.storage.get("SURVEY_CID")
          this.cid = res;
          const name=await this.storage.get("NAME")
            
            const mail=await this.storage.get("EMAIL")
              
              const phone=await this.storage.get("PHONE");
              const data=await this.storage.get("TOKEN");
      const bid=await this.storage.get("BRANCH");
      const com=await this.storage.get("COMPANY_ID");
                
                let utype;

                if(this.role==1){
                  this.name=name;
                  this.mail=mail;
                  this.phone=phone;
                  utype=3;
                  this.res_name='';
                }else{
                  utype=this.utype
                  if(this.utype==0){
                    this.res_name=''
                  }
                }

    if (this.role==2&&this.utype!==1&&(this.name == undefined || this.name=='')) {
      this.presentAlert('Please enter the name.')
    // } else if (this.role==2&&((this.phone==undefined||this.phone=='')&&(this.mail==undefined||this.mail==''))) {
    //   this.presentAlert('Please enter the phone number or email Id.')
    } else if (this.role==2&&(this.phone && (this.phone.toString().length < 10 || this.phone.toString().length > 10))) {
      this.presentAlert('Please enter a 10 digit mobile number.');
    } else if (this.role==2&&(this.mail&&!(this.mail.match(pattern)))) {
      this.presentAlert('Please enter a valid email Id.')
    } else if (this.role==2&&this.utype!==1&&this.name&&/^\d+$/.test(this.name)) {
      this.presentAlert('Name field contains only digits.');
    } else if((this.utype==1||this.utype==2)&&(this.res_name!=undefined||this.res_name!='')&&(/^\d+$/.test(this.res_name))){
      this.presentAlert('Resident name field contains only digits.');
    }else if((this.utype==1||this.utype==2)&&(this.res_name==undefined||this.res_name=='')){
      this.presentAlert('Please enter the resident name.')
    }else {
      this.showLoading();
      this.disabled = true;
      
                //       this.storage.get('USER_TYPE').then(type=>{
                //         if(type==6){
                //           utype=2
                //         }else if(type==5){
                //           utype=1
                //           this.res=name;
                //         }else{
                //  let utype=0
                // }
                if(this.phone==undefined||this.phone==null){
                  this.phone=''
                }

                if(this.mail==undefined||this.mail==null){
                  this.mail=''
                }

                if(this.res_name==undefined||this.res_name==null){
                  this.res_name=''
                }
                let name;
                if(utype==1){
                  name=this.res_name
                }else{
                  name=this.name;
                }
                let url = this.config.feedback_url + 'saveReviewResponse';
                var s = "";
                for (var i = 0; i < this.imageResponse.length; i++) {
                  s += `&images[]=${this.imageResponse[i]}`;
                }
                let body = `company_id=${this.cid}&question_id=${this.que_id}&option_id=${this.opt_id}&feedback=${this.feedback}&review_branch_id=${this.sid}&name=${name}&email=${this.mail}&phone=${this.phone}` + s + `&feedback_type=${this.type}&category_id=${this.cat_id}&resident_name=${this.res_name}&usertype=${utype}`;
                //  let body=new FormData();
                //  body.append('company_id',this.cid);
                //  body.append('question_id',this.que_id);
                //  body.append('option_id',this.opt_id);
                //  body.append('feedback',this.feedback);
                //  body.append('review_branch_id',this.sid);
                //  body.append('name',this.name);
                //  body.append('email',this.mail);
                //  body.append('phone',this.phone);
                //  body.append('images',this.imageResponse);
                console.log("body:", body);

                // let data={
                //   company_id:this.cid,
                //   question_id:this.que_id,
                //   option_id:this.opt_id,
                //   feedback:this.feedback,
                //   review_branch_id:this.sid,
                //   name:this.name,
                //   email:this.mail,
                //   phone:this.phone,
                //   images:this.imageResponse
                // }
                // console.log("datttaaa:",data);
                let headers = new HttpHeaders({
                  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                  
                  
                })

               
                this.http.post(url, body, { headers }).subscribe((data: any) => {

                  console.log(data);
                  this.dismissLoader();
                  this.openModal();

                }, error => {
                  console.log(error);
                  this.dismissLoader();
                  this.presentAlert('Something went wrong. Please try again later.')
                  this.disabled = false;
                  this.disableSkip = false;
                })
              }
            
            
           
    

  }






  async openModal() {
    const modal = await this.modalCntlr.create({
      component: ThankyouComponent,
      backdropDismiss:false,
      cssClass:'thanks-modal',
      componentProps: {
        "flag": 2
      }
    });
    return await modal.present();
  }



  async addImage() {
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    // this.imagePicker.getPictures(options).then((results) => {
    //   for (var i = 0; i < results.length; i++) {
    //     // console.log('Image URI: ' + results[i]);
    //     if ((results[i] === 'O') || results[i] === 'K') {
    //       console.log("no img");

    //     } else {
    //       this.img.push(results[i]);
    //       let im = 'data:image/jpeg;base64,' + results[i]
    //       // this.imageResponse.push(im);
    //       this.uploadImage(im);
    //     }


    //   }
    // }, (err) => { });

    // console.log("imagg:", this.imageResponse);
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
  }


  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }


  async pickImage() {
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;

    //   // this.imageResponse.push(base64Image);
    //   this.uploadImage(base64Image);


    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
   
    this.uploadImage(base64Image);

  }


  removeImg(i) {
    this.imageResponse.splice(i, 1);
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async viewImage(){
      const modal = await this.modalCntlr.create({
        component: AttchedImagesComponent,
       
        componentProps: {
         data:this.imageResponse,
         
         
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        
      });
      return await modal.present();
    }
  skip(){
    this.feedback==null
    this.disableSkip = true;
    if(this.role==1){
      this.submit();
    }else{
    this.show = 3
    this.disableSkip = false;
    }
  }

  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async uploadImage(img){
    if(this.isLoading==false){
      this.showLoading();
      }
      const data=await this.storage.get("TOKEN");
      const bid=await this.storage.get("BRANCH");
      const com=await this.storage.get("COMPANY_ID");
    let url=this.config.feedback_url+'upload_feedback_image_app';
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      
       
      
    })
    // let body={file:img}
    let body= `file=${img}`
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
        console.log('img:',res)
        if(res.data){
          this.imageResponse.push(res.data);
            }else{
              this.presentAlert('Upload failed. Please try again')
            }
      this.dismissLoader();
      console.log('resp:',this.imageResponse);
    },error=>{
      console.log(error);
      this.dismissLoader();
    })
  }

}
