import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackDashboardPageRoutingModule } from './feedback-dashboard-routing.module';

import { FeedbackDashboardPage } from './feedback-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackDashboardPageRoutingModule
  ],
  declarations: [FeedbackDashboardPage]
})
export class FeedbackDashboardPageModule {}
