import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular'
;

@Injectable({
  providedIn: 'root'
})
export class HttpConfigService {
  domain_url = 'https://api.centrim.life/centrim_api/api/';
  login_path = 'https://api.centrim.life/centrim_api/public/api/auth/login';
  base_path = 'https://api.centrim.life/centrim_api/api/story_app';
  
  // domain_url = 'http://52.65.155.193/centrim_api/api/';
  // login_path = 'http://52.65.155.193/centrim_api/public/api/auth/login';
  // base_path = 'http://52.65.155.193/centrim_api/api/story_app';
  
  feedback_url = 'https://app.personcentredfeedback.com.au/smileyreviewapp_user_api/index.php/Centrim_Cntrl/';
  head:any;
  token:string;
  id:any;
  branch:any;
  headers:any;
  constructor(private http:HttpClient,private storage:Storage) {


    
      
    
   }
  
   async getHeader(){
    const data=await this.storage.get("TOKEN");
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
     let headers=new HttpHeaders({Authorization:'Bearer '+data,'company_id':cid.toString(),'branch_id':bid.toString()})
     console.log('servHeader:',headers);
     
     return headers;
   }
   async getListItems(params,uid,bid) {
   
    let headers=await this.getHeader();
    let body={
                user_id:uid,
                branch_id:bid
              }
    
        console.log('feedbody:',body,this.base_path + params);
        
     
    return this.http.post(this.base_path + params,body,{headers});
  }

  async getUserPermission(){
   
    const data= await this.storage.get("USER_ID");
      const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
     let headers=await this.getHeader();
    let url=this.domain_url+'user_module_permissions';
            let body={
              user_id:data,
             
            }
            
            
            console.log(url,body);
           
             return this.http.post(url,body,{headers});
              
         
          
  }
}
