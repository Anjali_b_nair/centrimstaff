import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvAddContactPageRoutingModule } from './rv-add-contact-routing.module';

import { RvAddContactPage } from './rv-add-contact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvAddContactPageRoutingModule
  ],
  declarations: [RvAddContactPage]
})
export class RvAddContactPageModule {}
