import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAddContactPage } from './rv-add-contact.page';

describe('RvAddContactPage', () => {
  let component: RvAddContactPage;
  let fixture: ComponentFixture<RvAddContactPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAddContactPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAddContactPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
