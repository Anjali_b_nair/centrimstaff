import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-add-contact',
  templateUrl: './rv-add-contact.page.html',
  styleUrls: ['./rv-add-contact.page.scss'],
})
export class RvAddContactPage implements OnInit {
  cid:any;
  id:any;
  email:any=null;
  relation:any;
  other_relation:any=null;
  phone:any=null;
  is_primary:boolean=false;
  family_portal:boolean=false;
  // do_not_call:0
  // do_not_text:0
  
  fname:any;
  lname:any;
  state:any=null;
  address:any=null;
  suburb:any=null;
  postcode:any=null;
  approved_for:any=[];
  contact_type:any=null;
  contact_role:any=null;
  subscription:Subscription;
  password:any=null;
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private toastCntlr:ToastController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  });
  }
  back(){
    this.router.navigate(['/rv-res-profile',{cid:this.cid}])
  }

  async save(){
    if(!this.fname||!this.lname||!this.relation||(this.family_portal&&!this.password)){
      this.presentAlert('Please fill the required fields.')
    }else{
      const bid = await this.storage.get("BRANCH");
      const uid = await this.storage.get("USER_ID");
      const cid = await this.storage.get("COMPANY_ID");
  
        let url=this.config.domain_url+'create_rv_contact';
      let headers=await this.config.getHeader();
      let primary=0;
      if(this.is_primary){
        primary=1
      }
      let portal_access=0;
      if(this.family_portal){
        portal_access=1
      }
      let eopa,eopamed,spa,fpa,epog,trustee,vcat,exe;
      if(this.approved_for.includes('EPOA')){
        eopa=1;
      }else{
        eopa=0
      }
      if(this.approved_for.includes('Supporting Power of Attorney')){
        spa=1;
      }else{
        spa=0;
      }
      if(this.approved_for.includes('Financial power of Attorney')){
        fpa=1;
      }else{
        fpa=0;
      }
      if(this.approved_for.includes('EPOAMedical')){
        eopamed=1;
      }else{
        eopamed=0;
      }
      if(this.approved_for.includes('EPOG')){
        epog=1;
      }else{
        epog=0;
      }
      if(this.approved_for.includes('Public Trustee')){
        trustee=1;
      }else{
        trustee=0;
      }
      if(this.approved_for.includes('VCAT Guardianship')){
        vcat=1;
      }else{
        vcat=0;
      }
      if(this.approved_for.includes('Executor')){
        exe=1;
      }else{
        exe=0;
      }
      let body={
        email:this.email,
        resident_user_id:parseInt(this.id),
        relation:this.relation,
        name:this.fname+' '+this.lname,
        relation2:this.other_relation,
        phone:this.phone,
        is_primary:primary,
        user_id:parseInt(this.id),
        contact_purpose:this.contact_type,
        contact_role:this.contact_role,
        do_not_call:0,
        do_not_text:0,
        enduring_powerof_attorney:eopa,
        enduring_powerof_guardianship:epog,
        supportive_powerof_attorney:spa,
        enduring_powerof_atorney_medical:eopamed,
        public_trustee:trustee,
        vcat_guardianship_order:vcat,
        financial_powerof_attorney :fpa,
        executor:exe,
        firstname:this.fname,
        lastname:this.lname,
        state:this.state,
        address:this.address,
        suburb:this.suburb,
        postcode:this.postcode,
        password:this.password,
        add_c_all:1,
        portal_access:portal_access
      }
      console.log('body:',body);
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
       
       console.log(res);
       
        this.presentAlert('Invite contact successfully.')
        this.back();
      },error=>{
        console.log(error);
        this.presentAlert('Something went wrong. Please try again later.')
      })
    }

  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
}
