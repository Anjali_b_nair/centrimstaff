import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvAddContactPage } from './rv-add-contact.page';

const routes: Routes = [
  {
    path: '',
    component: RvAddContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvAddContactPageRoutingModule {}
