import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;


import { Subscription } from 'rxjs';

import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendanceGroupComponent } from '../components/activity-attendance-group/activity-attendance-group.component';
import { ActivityAttendedReasonComponent } from '../components/activity-attended-reason/activity-attended-reason.component';
import { ActivityDeclinedReasonComponent } from '../components/activity-declined-reason/activity-declined-reason.component';
import  moment from 'moment';

@Component({
  selector: 'app-mark-attendance',
  templateUrl: './mark-attendance.page.html',
  styleUrls: ['./mark-attendance.page.scss'],
})
export class MarkAttendancePage implements OnInit {
  selected:any='1';
  consumers:any[]=[];
  con:any[]=[];
  terms:any;
  family:any=[];
  other:any[]=[];
  sel_id:any[]=[];
  temp:any[]=[];
  rem:any[]=[];
  all:boolean=false;
  sel_fam:any[]=[];
  act_id:any;
  subscription:Subscription;
  act_atnd:any[]=[];
  status:any;
  selection:any;
  sel_value:any=0;
  group:any=0;
  start:any;
  end:any;
  rv:any;
  limit_required:any=0;
  wl_id:any[]=[];
  selectAllStatus:boolean=false;
  constructor(private config:HttpConfigService,private http:HttpClient,private modalCntrl:ModalController,
    private router:Router,private platform:Platform,private toastCntlr:ToastController,private storage:Storage,
    private popCntrl:PopoverController,private route:ActivatedRoute,private loadingCtrl:LoadingController,
    private alertCntlr:AlertController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.selected='1';
    this.rv=await this.storage.get('RVSETTINGS');
    this.status=undefined;
    this.selectAllStatus=false;
    this.showLoading();
    this.act_id=this.route.snapshot.paramMap.get('act_id');
    if(this.rv==1){
      this.getRVsettings();
    }
    this.terms='';
                this.sel_id=[];
                this.temp=[];
                this.sel_fam=[];
                
                this.consumers=[];
                
                this.other=[];
              if(this.group!=0){
                this.act_atnd=[];
                this.wl_id=[];
                this.retainGroup();
              }else{
                this.con=[];
                this.act_atnd=[];
                this.wl_id=[];
                  const data=await this.storage.get("TOKEN")
                    let token=data;
                    const bid=await this.storage.get("BRANCH")


                      let branch=bid.toString();
                let headers=await this.config.getHeader();
                let url=this.config.domain_url+'activity/'+this.act_id;
                // let headers=await this.config.getHeader();
                  this.http.get(url,{headers}).subscribe((res:any)=>{
                    console.log(res);
                    
                    let start=res.activity.start_time;
                    let end=res.activity.end_time
                    this.start=moment(start,'h:mm A').format();
                    this.end=moment(end,'h:mm A').format();
                    console.log('time:',this.start,this.end)
                      if(res.activity.tagged_users.length>0){
                     res.activity.tagged_users.forEach(element => {
                        this.act_atnd.push(element.user_id);
                      });
                     if(res.activity.waiting_list.length){
                      res.activity.waiting_list.forEach(element => {
                        this.wl_id.push(element.user_id);
                      });
                     }
                     
                      }
                    })
                       
                       
                       
                 



                
// this.storage.ready().then(()=>{
  const cid=await this.storage.get("COMPANY_ID")

    const utype=await this.storage.get("USER_TYPE")

     
        // const bid=await this.storage.get("BRANCH")

      let url1=this.config.domain_url+'activity_attendee';
      // let headersa=new HttpHeaders;
      let body={
        activity_id:this.act_id,
        role_id:utype,
        company_id:cid
      }
      this.http.post(url1,body,{headers}).subscribe((res:any)=>{
          console.log("attn:",res,branch,{headers});
          let attend;
          let reason;
          let action;
          let other=0;
          res.data.forEach(element => {
            // this.act_atnd.push(element.user.user_id);
        //   });
        // })
        //   let url2=this.config.domain_url+'residents';
        //   this.http.get(url2,{headers}).subscribe((data:any)=>{
            
        //      console.log("act:",this.act_atnd);
             
        //      data.data.forEach(element => {
               if(this.act_atnd.includes(element.user.user_id)){
                 attend=1
               }else{
                 attend=0
               }
               if(this.wl_id.includes(element.user.user_id)){
                attend=2
               }
               if(element.activity_action!=null){
                 reason=element.activity_action.reason
                 action=element.activity_action.action
               }else{
                 reason=null
                 action=null
               }
               if(element.other_activity&&element.other_activity.length>0){
                element.other_activity.forEach(ele => {
                  let start,end;
                  start=moment(ele.activitydetails[0].start_time,'h:mm A').format();
                  end=moment(ele.activitydetails[0].end_time,'h:mm A').format();
                  if(this.start==start&&this.end==end){
                    other=1
                  }
                  console.log('start:',this.wl_id,"end:",attend)
                });
              }
               this.consumers.push({con:element,selected:false,attendee:attend,reason:reason,action:action,other:other});
               this.con.push({con:element,selected:false,attendee:attend,reason:reason,action:action,other:other});
               other=0
             });
             this.dismissLoader();
             console.log(other,"data:",this.consumers);
               
           },error=>{
             console.log(error);
           });
      
                
  
    
                }
      
      
    // this.selected=true;
      // let headers=await this.config.getHeader();


      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/activity-details',{act_id:this.act_id}],{replaceUrl:true});
        }); 
        
        }
        ionViewWillLeave() { 
          this.group=0;
        this.subscription.unsubscribe();
        }


  segmentChange(i){
    if(i==1){
      this.selected='1'
    }else{
      this.selected='2';
      this.getFamily();
    }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  
  
  async getFamily(){
    
    this.other=[];
   
      const bid=await this.storage.get("BRANCH")

      
      let url=this.config.domain_url+'resident_contact';
      let headers=await this.config.getHeader();;
      this.http.get(url,{headers}).subscribe((data:any)=>{ 
        console.log("fam:",data);
        data.data.forEach(element => {
          if(element.contacts.length>0){
            
            this.other.push(element);
            
          }
        });
        console.log("other:",this.other);
        
        });
    
    
    
       
  }

  async add(item){
    const modal = await this.modalCntrl.create({
      component: ActivityAttendedReasonComponent,
      componentProps: {
        
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data != null || dataReturned.data!=undefined) {
        console.log("dataret:",dataReturned);
        
        // this.attendees = dataReturned.data;
        this.markAttendance(item.con.user.user_id,dataReturned.data,1,0)
        
      }
    });
    return await modal.present();
  }
  async decline(item){
    const modal = await this.modalCntrl.create({
      component: ActivityDeclinedReasonComponent,
      componentProps: {
        
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data != null || dataReturned.data!=undefined) {
        console.log("dataret:",dataReturned);
        
        // this.attendees = dataReturned.data;
        this.markAttendance(item.con.user.user_id,dataReturned.data,2,0)
        
      }
    });
    return await modal.present();
  }
  // async dismiss(){
  //   let attendees=this.sel_id.concat(this.sel_fam)
    
  //   console.log("ond:",attendees);
    
  // }
  selectConsumer(item){
    item.selected=!item.selected;
    if(this.temp.includes(item.con.user.user_id)){
      var index = this.sel_id.indexOf(item.con.user.user_id);
      this.sel_id.splice(index, 1);
      console.log("index:",this.sel_id.indexOf(item.con.user.user_id))
      this.temp.splice(this.temp.indexOf(item.con.user.user_id),1)
    }else{
      if(this.rv==0||this.rv==1&&this.limit_required==1&&item.attendee==1||this.rv==1&&this.limit_required==0)
      this.temp.push(item.con.user.user_id);
      
    }
    this.sel_id=this.temp.reduce((arr,x)=>{
      let exists = !!arr.find(y => y === x);
      if(!exists){
          arr.push(x);
      // }else{
      //   var index = this.sel_id.indexOf(x);
      //   this.sel_id.splice(index, 1);
      //   this.temp.splice(this.temp.indexOf(x),1)
      }
      
      return arr;
  }, []);
  console.log("sel:",this.sel_id,this.temp);
  
  }
  
  selectAll(){
    console.log("se:",this.selected);
    
    if(this.selected=='1'){
      this.all=true;
      this.sel_id=[];
  this.temp=[];
      this.consumers.forEach(element=>{
        if(this.temp.includes(element.con.user.user_id)){
  
        }else{
          this.temp.push(element.con.user.user_id);
        }
        
        this.sel_id=this.temp.reduce((arr,x)=>{
          let exists = !!arr.find(y => y=== x);
          if(!exists){
              arr.push(x);
          }
          return arr;
      }, []);
    
     
        // if (element.value == value) {
           element.selected = true;
        //    break; //Stop this loop, we found it!
        // }
      
      })
      console.log("sel:",this.sel_id);
    }else{
      this.sel_fam=[];
      this.temp=[];
      this.other.forEach(element=>{
        (element.contacts).forEach(ele => {
          if(this.temp.includes(ele.user.user_id)){
  
          }else{
            this.temp.push(ele.user.user_id);
          }
        });
        
        
        this.sel_fam=this.temp.reduce((arr,x)=>{
          let exists = !!arr.find(y => y=== x);
          if(!exists){
              arr.push(x);
          }
          return arr;
      }, []);
    
     
     
        // if (element.value == value) {
          //  element.selected = true;
        //    break; //Stop this loop, we found it!
        // }
      
      })
    }
    console.log("sel_fam:",this.sel_fam);
  }
  selectedAssigned(){
    if(this.selected=='1'){
      this.sel_id=[];
      this.temp=[];
      this.consumers.forEach(ele=>{
    // this.act_atnd.forEach(element=>{
     if(this.act_atnd.includes(ele.con.user.user_id)){
      if(this.temp.includes(ele.con.user.user_id)){

      }else{
        // this.act_atnd
        if(!this.wl_id.includes(ele.con.user.user_id))
        this.temp.push(ele.con.user.user_id);
      }
      
      this.sel_id=this.temp.reduce((arr,x)=>{
        let exists = !!arr.find(y => y=== x);
        if(!exists){
            arr.push(x);
        }
        return arr;
    }, []);
   
      // if (element.value == value) {
        //  element.selected = true;
      //    break; //Stop this loop, we found it!
      // }
     }
    })
    console.log("sel:",this.sel_id);
  }else{
    this.sel_fam=[];
    this.temp=[];
    this.other.forEach(element=>{
      (element.contacts).forEach(ele => {
        if(this.act_atnd.includes(ele.contact_user_id)){
      if(this.temp.includes(ele.contact_user_id)){

      }else{
        if(!this.wl_id.includes(ele.contact_user_id))
        this.temp.push(ele.contact_user_id);
      }
    }
      this.sel_fam=this.temp.reduce((arr,x)=>{
        let exists = !!arr.find(y => y=== x);
        if(!exists){
            arr.push(x);
        }
        return arr;
    }, []);
  })
})
console.log("sel_fam:",this.sel_fam);
  }
}
  

selectUnassigned(){
  if(this.selected=='1'){
  this.sel_id=[];
  this.temp=[];
  let array=this.consumers.filter(x => !this.act_atnd.includes(x.con.user.user_id));
  array.forEach(element=>{
    if(this.temp.includes(element.con.user.user_id)){

    }else{
      this.temp.push(element.con.user.user_id);
    }
    
    this.sel_id=this.temp.reduce((arr,x)=>{
      let exists = !!arr.find(y => y=== x);
      if(!exists){
          arr.push(x);
      }
      return arr;
  }, []);
})
  }else{
    this.sel_fam=[];
    this.temp=[];
    this.other.forEach(element=>{
      (element.contacts).forEach(ele => {
        if(!this.act_atnd.includes(ele.contact_user_id)){
    if(this.temp.includes(ele.contact_user_id)){

    }else{
      this.temp.push(ele.contact_user_id);
    }
  }
    
    this.sel_fam=this.temp.reduce((arr,x)=>{
      let exists = !!arr.find(y => y=== x);
      if(!exists){
          arr.push(x);
      }
      return arr;
  }, []);
})
  })
  console.log("sel_fam:",this.sel_fam);
}
  
}

  // remove(item){
  //   item.selected=false;
   
  
  // this.sel_id.forEach(ele=>{
  //   if(ele==item.con.user.user_id){
  //     var index = this.sel_id.indexOf(ele);
  //       this.sel_id.splice(index, 1);
  //       this.temp.splice(this.temp.indexOf(ele),1)
  //   }
    
  
  // })
  // console.log("rem:",this.sel_id);
    
    
    
  // }
  
  selectFam(ev,item){
    // if(ev.currentTarget.checked==true){
    if(this.sel_fam.includes(item.contact_user_id)){
      console.log("temp includes:",item.contact_user_id,this.sel_fam);
      var index = this.sel_fam.indexOf(item.contact_user_id);
        console.log("index:",index);
        
        this.sel_fam.splice(index, 1);
      
    }else{
      // this.temp.push(item.contact_user_id);
      if(this.rv==0||this.rv==1&&this.limit_required==1&&this.act_atnd.includes(item.contact_user_id)&&!this.wl_id.includes(item.contact_user_id)||this.rv==1&&this.limit_required==0)
      this.sel_fam.push(item.contact_user_id);
      console.log("temp not includes:",item.contact_user_id,this.sel_fam)
    }
    
  // }else{
  //   this.sel_fam=this.temp.reduce((arr,x)=>{
  //     console.log("x:",x)
  //     let exists = !!arr.find(y => y==x );
  //     console.log("exist:",exists,x);
      
  //     if(!exists){
  //         arr.push(x);
  //         console.log("arr:",arr);
          
  //     }else{
  //       var index = this.sel_fam.indexOf(x);
  //       console.log("index:",index);
        
  //       this.sel_fam.splice(index, 1);
  //       this.temp.splice(this.temp.indexOf(x),1)
  //     }
      
  //     return arr;
  // }, []);
  
  console.log("sel:",item.user.user_id,this.act_atnd,!this.act_atnd.includes(item.user.user_id));
  
  }

  async byGroup(ev){
    const data=await this.storage.get("TOKEN")
              let token=data;
              const bid=await this.storage.get("BRANCH")


                let branch=bid.toString();
                const cid=await this.storage.get("COMPANY_ID")

                  const utype=await this.storage.get("USER_TYPE");
                  let headers=await this.config.getHeader();
    const popover = await this.modalCntrl.create({
      component: ActivityAttendanceGroupComponent,
      
      backdropDismiss:true,
      cssClass:'act-res-group-modal'
      
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      
      if (dataReturned.data != null || dataReturned.data!=undefined) {
        
        let id = dataReturned.data;
        this.group=id;
        this.consumers=[];
        if(id!=0){
          this.selected='1';
    this.showLoading();
          
            

          
          let apiUrl=this.config.domain_url+'activity/'+this.act_id;
                      // let headers=await this.config.getHeader();
                        this.http.get(apiUrl,{headers}).subscribe((res:any)=>{
                          console.log(res);
                          
                          
                          
                          
                            if(res.activity.tagged_users.length>0){
                           res.activity.tagged_users.forEach(element => {
                              this.act_atnd.push(element.user_id);
                            });
                            if(res.activity.waiting_list.length>0){
                              res.activity.waiting_list.forEach(element => {
                                this.wl_id.push(element.user_id);
                              });
                             }
                           
                            }
                          })
          let url=this.config.domain_url+'group/'+this.group;
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log("mem:",res);
          
            // let attend;
            // let reason;
            // let action;
      
            let url1=this.config.domain_url+'activity_attendee';
            
            let body={
              activity_id:this.act_id,
              role_id:utype,
              company_id:cid
            }
            this.http.post(url1,body,{headers}).subscribe((att:any)=>{
                console.log("attn:",att);
                let attend;
                let reason;
                let action;
                let other=0;
                att.data.forEach(ele => {
                  res.data.residents.forEach(element => {
                     if(this.act_atnd.includes(element.user.user_id)){
                       attend=1
                     }else{
                       attend=0
                     }
                     if(this.wl_id.includes(element.user.user_id)){
                      attend=2
                     }
                     if(ele.activity_action!=null){
                       reason=ele.activity_action.reason
                       action=ele.activity_action.action
                     }else{
                       reason=null
                       action=null
                     }
                     if(ele.other_activity&&ele.other_activity.length>0){
                      ele.other_activity.forEach(el => {
                        let start,end;
                        start=moment(el.activitydetails[0].start_time,'h:mm A').format();
                        end=moment(el.activitydetails[0].end_time,'h:mm A').format();
                        if(this.start==start&&this.end==end){
                          other=1
                        }
                        console.log('start:',start,"end:",end,other)
                      });
                    }
                     if(element.user.user_id==ele.user.user_id){
                     this.consumers.push({con:element,selected:false,attendee:attend,reason:reason,action:action,other:other});
                     }
                     other=0
                   });
                   this.selectAllStatus=false;
                   this.sel_id=[];
                   this.sel_fam=[];
                   this.dismissLoader();
                  
                     
                 },error=>{
                   console.log(error);
                   this.dismissLoader();
                 });
                })
              })
           
  }else{
    this.dismissLoader();
    // this.consumers=this.con;
    this.ionViewWillEnter();
  }
        
      }
      this.sel_value='0';
  // this.status='0';
  this.selection='0';
    });
    return await popover.present();
  }
 

  select(val){
    console.log("selection:",val);
    this.selection=val;
    this.sel_value=val;
    if(val==1){
      this.selectAll();
    }else if(val==2){
      this.selectedAssigned();
    }else{
      this.selectUnassigned()
    }
    
  }
  async mark(val){
    console.log("markStatus:",val);
    // this.status=val;
    if(this.selected=='1'){
    if(val==1 ){
      const modal = await this.modalCntrl.create({
        component: ActivityAttendedReasonComponent,
        componentProps: {
          
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned == null || dataReturned.data==undefined) {

        }else{
          console.log("dataret:",dataReturned);
          
          // this.attendees = dataReturned.data;
          // if(this.selected=='1'){
          this.markAttendanceAll(this.sel_id,dataReturned.data,1)
          // }else{
          //   this.markAttendanceAll(this.sel_fam,dataReturned.data,1)
          // }
          
        }
      });
      return await modal.present();
    }
    if(val==2){
      const modal = await this.modalCntrl.create({
        component: ActivityDeclinedReasonComponent,
        componentProps: {
          
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned == null || dataReturned.data==undefined) {

        }else{
          console.log("dataret:",dataReturned);
          
          // this.attendees = dataReturned.data;
          // if(this.selected=='1'){
          this.markAttendanceAll(this.sel_id,dataReturned.data,2)
          // }else{
          //   this.markAttendanceAll(this.sel_fam,dataReturned.data,2)
          // }
          
        }
      });
      return await modal.present();
    
    }
    else if(val==3){
      // if(this.selected=='1'){
        this.remove(this.sel_id)
        // }else{
        //   this.remove(this.sel_fam)
        // }
      
    }

    console.log('marked:',this.sel_id)
  }else{
    if(val==1){
      this.markAttendanceAll(this.sel_fam,null,1)
    }else if(val==2){
      this.markAttendanceAll(this.sel_fam,null,2)
    }else if(val==3){
      this.remove(this.sel_fam)
    }
  }
  
  }

 async markAttendanceAll(sel_id,reason,action){
   console.log("marksel:",sel_id)
   const cid=await this.storage.get("COMPANY_ID")

      const uid=await this.storage.get("USER_ID")
   let id;
   id=sel_id[0].toString();
   sel_id.forEach(el=>{
     console.log("el:",el,id);
     
     if(id==el.toString()){
       id=el.toString();
     }else{
       id=id+','+el.toString()
     }
   
  
   })
   console.log('markid:',id)
      let url=this.config.domain_url+'mark_attendence';
      let headers=await this.config.getHeader();;
      let body={
        activity_id:this.act_id,
        company_id:cid,
        user_id:id,
        reason:reason,
        action:action,
        marked_by:uid
      }
      console.log('body:',body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          if(this.group==0){
           
          this.ionViewWillEnter();
           
          }else{
            this.retainGroup();
          }
        },error=>{
          console.log(error);
          
        })
   
  this.sel_value='0';
  // this.status='0';
  this.selection='0';
  this.status=undefined;
}

  async markAttendance(sel_id,reason,action,type){
    console.log("famsingle:",sel_id[0]);
    
    let  id;
    if(type==0){
      id=sel_id
    }else{
    if(sel_id.length>0){
      
      id=sel_id[0].toString();
      sel_id.forEach(el=>{
        console.log("el:",el,id);
        
        if(id==el.toString()){
          id=el.toString();
        }else{
          id=id+','+el.toString()
        }
      
     
      })
  }else{
    id=sel_id[0].toString();
  }
}
   
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID")

        let url=this.config.domain_url+'mark_attendence';
        let headers=await this.config.getHeader();;
        let body={
          activity_id:this.act_id,
          company_id:cid,
          user_id:id,
          reason:reason,
          action:action,
          marked_by:uid
        }
        console.log('body:',body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          if(this.group==0){
            if(sel_id.length>1){
              this.ionViewWillEnter();
                }else{
                  this.insertReason(id,reason,action);
                  this.sel_id=[];
                }
          this.selected='1'
          }else{
            this.retainGroup();
          }

        },error=>{
          console.log(error);
          
        })
     
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
    
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }



  async attendedfam(ev,item){
    // if(item.contacts.filter(x => this.sel_fam.includes(x.user.user_id))){
    // const modal = await this.modalCntrl.create({
    //   component: ActivityAttendedReasonComponent,
    //   componentProps: {
        
        
    //   }
    // });
    // modal.onDidDismiss().then((dataReturned) => {
    //   if (dataReturned !== null || dataReturned!==undefined) {
    //     console.log("dataretfam:",dataReturned);
        
        // this.attendees = dataReturned.data;
        
        this.markAttendance(this.sel_fam,null,1,1)
        
        
    //   }
    // });
    // return await modal.present();
  // }else{
  //   this.presentAlert('Please select atleast one family member.')
  // }
  }
  async declinedfam(item){
    // if(item.contacts.filter(x => this.sel_fam.includes(x.user.user_id))){
    // const modal = await this.modalCntrl.create({
    //   component: ActivityDeclinedReasonComponent,
    //   componentProps: {
        
        
    //   }
    // });
    // modal.onDidDismiss().then((dataReturned) => {
    //   if (dataReturned !== null || dataReturned.data!==undefined) {
    //     console.log("dataretfam:",dataReturned);
        
        
          this.markAttendance(this.sel_fam,null,2,1)
          
        
    //   }
    // });
    // return await modal.present();
  // }else{
  //   this.presentAlert('Please select atleast one family member.')
  // }
  }
  cancel(){
    // this.hide=false;
    this.terms='';
  }
  // search(){
  //     this.hide=true;
  //   }
 

  async retainGroup(){
    this.consumers=[];
    this.showLoading();
      const data=await this.storage.get("TOKEN")
        let token=data;
        const bid=await this.storage.get("BRANCH")


          let branch=bid.toString();
          const cid=await this.storage.get("COMPANY_ID")

            const utype=await this.storage.get("USER_TYPE")

    let headers=await this.config.getHeader();
    let apiUrl=this.config.domain_url+'activity/'+this.act_id;
                // let headers=await this.config.getHeader();
                  this.http.get(apiUrl,{headers}).subscribe((res:any)=>{
                    console.log(res);
                    
                    
                    
                    
                      if(res.activity.tagged_users.length>0){
                     res.activity.tagged_users.forEach(element => {
                        this.act_atnd.push(element.user_id);
                      });
                     
                     
                      }
                      if(res.activity.waiting_list.length){
                        res.activity.waiting_list.forEach(element => {
                          this.wl_id.push(element.user_id);
                        });
                       }
                    })
    let url=this.config.domain_url+'group/'+this.group;
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("mem:",res);
    
      // let attend;
      // let reason;
      // let action;

      let url1=this.config.domain_url+'activity_attendee';
      
      let body={
        activity_id:this.act_id,
        role_id:utype,
        company_id:cid
      }
      this.http.post(url1,body,{headers}).subscribe((att:any)=>{
          console.log("attn:",att);
          let attend;
          let reason;
          let action;
          let other=0;
          att.data.forEach(ele => {
            res.data.residents.forEach(element => {
               if(this.act_atnd.includes(element.user.user_id)){
                 attend=1
               }else{
                 attend=0
               }
               if(this.wl_id.includes(element.user.user_id)){
                attend=2
               }
               if(ele.activity_action!=null){
                 reason=ele.activity_action.reason
                 action=ele.activity_action.action
               }else{
                 reason=null
                 action=null
               }
               
               if(ele.other_activity&&ele.other_activity.length>0){
                ele.other_activity.forEach(el => {
                  let start,end;
                  start=moment(el.activitydetails[0].start_time,'h:mm A').format();
                  end=moment(el.activitydetails[0].end_time,'h:mm A').format();
                  if(this.start==start&&this.end==end){
                    other=1
                  }
                  console.log('start:',start,"end:",end,other)
                });
              }
              if(element.user.user_id==ele.user.user_id){
                this.consumers.push({con:element,selected:false,attendee:attend,reason:reason,action:action,other:other});
                }
                other=0;
             });
             this.dismissLoader();
            
               
           },error=>{
             console.log(error);
           });





      // res.data.residents.forEach(element => {
      //   if(this.act_atnd.includes(element.user.user_id)){
      //     attend=true
      //   }else{
      //     attend=false
      //   }
      //   if(element.activity_action!=null){
      //     reason=element.activity_action.reason
      //     action=element.activity_action.action
      //   }else{
      //     reason=null
      //     action=null
      //   }
      //   this.consumers.push({con:element,selected:false,attendee:attend,reason:reason,action:action});
      // });
      // this.dismissLoader()
    })
    console.log("data:",this.consumers);
  })

}

async remove(id){
  const alert = await this.alertCntlr.create({
    mode:'ios',
    header: 'Remove attendance?',
    message: 'Are you sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Confirm',
        handler: () => {
        
          this.removeAttendance(id,1);
          
        }
      }
    ]
  });

  await alert.present();
}

async removeAttendance(id,i){
  let arr=[];
  if(i==1){
    arr=id
  }else{
    arr.push(id);
  }
  let url=this.config.domain_url+'unmark_attendance';
  let headers=await this.config.getHeader();;
  let body={
    activity_id:this.act_id,
    user_id:arr
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("unmark:",res);
    this.ionViewWillEnter();
  },error=>{
    console.log(error);
    
  })
}
insertReason(id,reason,action){
  let obj = this.consumers.find(o => o.con.user.user_id === id);
  let obj1=[{con:obj.con,selected:false,attendee:true,reason:reason,action:action,other:obj.other}]
  console.log('obj:',obj1);
  this.consumers=this.consumers.map(obj=>obj1.find(o=>o.con.user.user_id===obj.con.user.user_id)||obj);
  console.log('updatedCon:',this.consumers);
}

async getRVsettings(){
  const bid=await this.storage.get("BRANCH");
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_rv_settings/'+bid
  this.http.get(url,{headers}).subscribe((res:any)=>{
    this.limit_required=res.data.limit_required;
   
  })
}
selectToggle(event){
  if(event.currentTarget.checked==true){
    this.selectAllStatus=true;
    this.selectedAssigned();
    
  }else{
    this.sel_id=[];
    this.sel_fam=[];
    this.selectAllStatus=false;
  }
}

}
