import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarkAttendancePageRoutingModule } from './mark-attendance-routing.module';

import { MarkAttendancePage } from './mark-attendance.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { MarkAttendanceMoreComponent } from '../components/mark-attendance-more/mark-attendance-more.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarkAttendancePageRoutingModule,
    ApplicationPipesModule,
  ],
  declarations: [MarkAttendancePage,MarkAttendanceMoreComponent]
})
export class MarkAttendancePageModule {}
