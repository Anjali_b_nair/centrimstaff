import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningChooseSeasonDatePage } from './dining-choose-season-date.page';

const routes: Routes = [
  {
    path: '',
    component: DiningChooseSeasonDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningChooseSeasonDatePageRoutingModule {}
