import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningChooseSeasonDatePage } from './dining-choose-season-date.page';

describe('DiningChooseSeasonDatePage', () => {
  let component: DiningChooseSeasonDatePage;
  let fixture: ComponentFixture<DiningChooseSeasonDatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningChooseSeasonDatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningChooseSeasonDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
