import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningChooseSeasonDatePageRoutingModule } from './dining-choose-season-date-routing.module';

import { DiningChooseSeasonDatePage } from './dining-choose-season-date.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningChooseSeasonDatePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningChooseSeasonDatePage]
})
export class DiningChooseSeasonDatePageModule {}
