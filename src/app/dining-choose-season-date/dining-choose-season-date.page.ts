import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IonContent, Platform } from '@ionic/angular';
import moment from 'moment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dining-choose-season-date',
  templateUrl: './dining-choose-season-date.page.html',
  styleUrls: ['./dining-choose-season-date.page.scss'],
})
export class DiningChooseSeasonDatePage implements OnInit {
  subscription:Subscription;
  menu:any;
  currentDate:any;
  start:any;
  end:any;
  dates:any[]=[];
  hide:boolean=false;
  @ViewChild(IonContent, { static: false }) content: IonContent;
    constructor(private router:Router,private platform:Platform,private route:ActivatedRoute,
      @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.dates=[];
    this.hide=false;
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    console.log('menu:',this.menu,menu);
    this.currentDate=moment().format('DD MMM YYYY');
    this.start=moment(this.menu.start_date);
    this.end=moment(this.menu.enddate);
    var start=this.start.clone();
    while(start.isSameOrBefore(this.end)){
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1,'days');
    }

    console.log('dates:',this.dates);
    // const cid=await this.storage.get('COMPANY_ID');
    // const bid=await this.storage.get('BRANCH');
    
    setTimeout(()=>{
      var titleELe = this.document.getElementById(this.currentDate);
      console.log(titleELe,titleELe.offsetHeight,titleELe.offsetTop)
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    },500)
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   
     back(){
       this.router.navigate(['/dining-take-order'],{replaceUrl:true})
     }
     hideBanner(){
       this.hide=true;
     }
     order(item){
       this.router.navigate(['/dining-select-choice',{menu:JSON.stringify(this.menu),date:item}])
     }

     setIndicatorLine(item){
       let i=false;
       let c=moment(this.currentDate);
       let d=moment(item)
       if(d.isSameOrBefore(c)){
          i=true
       }
       return i;
     }
}
