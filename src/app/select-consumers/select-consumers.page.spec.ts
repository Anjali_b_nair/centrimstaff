import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectConsumersPage } from './select-consumers.page';

describe('SelectConsumersPage', () => {
  let component: SelectConsumersPage;
  let fixture: ComponentFixture<SelectConsumersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectConsumersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectConsumersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
