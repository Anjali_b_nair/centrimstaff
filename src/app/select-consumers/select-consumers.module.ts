import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectConsumersPageRoutingModule } from './select-consumers-routing.module';

import { SelectConsumersPage } from './select-consumers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectConsumersPageRoutingModule
  ],
  declarations: [SelectConsumersPage]
})
export class SelectConsumersPageModule {}
