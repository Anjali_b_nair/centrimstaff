import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectConsumersPage } from './select-consumers.page';

const routes: Routes = [
  {
    path: '',
    component: SelectConsumersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectConsumersPageRoutingModule {}
