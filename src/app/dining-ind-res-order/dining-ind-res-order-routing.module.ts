import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningIndResOrderPage } from './dining-ind-res-order.page';

const routes: Routes = [
  {
    path: '',
    component: DiningIndResOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningIndResOrderPageRoutingModule {}
