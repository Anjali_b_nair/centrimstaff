import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningIndResOrderPage } from './dining-ind-res-order.page';

describe('DiningIndResOrderPage', () => {
  let component: DiningIndResOrderPage;
  let fixture: ComponentFixture<DiningIndResOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningIndResOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningIndResOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
