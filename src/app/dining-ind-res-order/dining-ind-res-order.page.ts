import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ModalController, Platform, PopoverController, ToastController, LoadingController, IonContent } from '@ionic/angular';
import { AllergicInfoComponent } from '../components/dining/allergic-info/allergic-info.component';
import { ChooseDateComponent } from '../components/dining/choose-date/choose-date.component';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { MenuOptionsComponent } from '../components/dining/menu-options/menu-options.component';
import { SelectConfirmationComponent } from '../components/dining/select-confirmation/select-confirmation.component';
import { OrderConfirmationComponent } from '../components/dining/order-confirmation/order-confirmation.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import moment from  'moment-timezone';
import { MarkLeaveSingledayComponent } from '../components/dining/mark-leave-singleday/mark-leave-singleday.component';
import { MenuItemDetailsComponent } from '../components/dining/menu-item-details/menu-item-details.component';
import { ContinentalItemComponent } from '../components/dining/continental-item/continental-item.component';
import { ContinentalItemTabviewComponent } from '../components/dining/continental-item-tabview/continental-item-tabview.component';
import { DOCUMENT } from '@angular/common';
import { OrderSuccessMessageComponent } from '../components/dining/order-success-message/order-success-message.component';
import { AdditionalItemDetailsComponent } from '../components/dining/additional-item-details/additional-item-details.component';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { NoItemAlertComponent } from '../components/dining/no-item-alert/no-item-alert.component';

@Component({
  selector: 'app-dining-ind-res-order',
  templateUrl: './dining-ind-res-order.page.html',
  styleUrls: ['./dining-ind-res-order.page.scss'],
})
export class DiningIndResOrderPage implements OnInit {
  subscription:Subscription;
  consumer:any;
  menu:any;
  date:any;
  details:any;
  wing:any;
  fluid_cap_target:any;
  dateCopy:any;
  menuItems:any[]=[];
  diet_type:any[]=[];
  allergies:any[]=[];
  mealSize:any;
  tz:any;
  additionalItems:any[]=[];
  skipped:any[]=[];
  leaveMarked:boolean=false;
  selected:any[]=[];
  editOrder:boolean=false;
  preOrderedItems:any[]=[];
  dates:any[]=[];
  currentIdx:any;
  loading:boolean=false;
  serve_area:any[]=[];
  sel_additional_item:any[]=[];
  hide_footer:boolean=false;
  screen_width:any;
  screen_height:any;

  additional_by_ser_time:any=[];
  @ViewChild(IonContent, { static: false }) content: IonContent;
  constructor(private modalCntrl: ModalController, private popCntrl: PopoverController,private route:ActivatedRoute,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private router:Router,
    private platform:Platform,private toastCntlr:ToastController,private loadingCtrl:LoadingController,
    @Inject(DOCUMENT) private document: Document,private orient:ScreenOrientation) { }

  ngOnInit() {
  
  
      // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
    
      // this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
     
  }
  // async dietaryPreference() {
  //   const modal = await this.modalCntrl.create({
  //     component: DietaryPreferencesComponent,
  //     cssClass: 'dining-dpreference-modal',
  //     componentProps:{
  //       details:this.details
  //     }
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
      
  //   });
  //   return await modal.present();
  // }

  // async copy() {
  //   const modal = await this.modalCntrl.create({
  //     component: SelectConfirmationComponent,
  //     cssClass: 'dining-copy-previous-modal',
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
      
  //   });
  //   return await modal.present();
  // }
  // async chooseDate() {
  //   const modal = await this.modalCntrl.create({
  //     component: ChooseDateComponent,
  //     cssClass:'din-choosedate-modal',
  //     componentProps:{
  //       menu:this.menu
  //     }
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
  //     if(dataReturned.data){
  //       this.date=dataReturned.data;
  //       this.getList();
  //       this.getDaywiseConsumerDetails();
        
  //     }
  //   });
  //   return await modal.present();
  // }
  
  // async options(ev,id,ser_time) {
  //   let sel_area;
  //   const targetIdx=this.details.consumer_serve_areas.map(x=>x.serving_time.id).indexOf(id);
  //   if(targetIdx>=0){
  //     sel_area=this.details.consumer_serve_areas[targetIdx].serve_area
  //   }else{
  //     sel_area=null
  //   }
  //   sel_area=this.changedServeArea(id);
  //   console.log('add:',this.additional_by_ser_time,sel_area)
    
  //   let item;
  //   this.additional_by_ser_time.forEach(el=>{
  //     if(el.ser_time==id){
  //       item=el
  //     }
  //   })

  //   const popover = await this.popCntrl.create({
  //     component: MenuOptionsComponent,
  //     id:'menu_options',
  //     event: ev,
  //     backdropDismiss: true,
  //     cssClass: 'dining-more-options-popover',
  //     componentProps: {
  //       menu:item,
  //       serving_id:id,
  //       date:this.date,
  //       menu_id:this.menu.id,
  //       consumer_id:this.consumer.user.user_id,
  //       servingTime:ser_time,
  //       is_skipped:this.skippedMeal(id),
  //       serve_area:this.serve_area,
  //       sel_area:sel_area,
  //       // sel_additional_item:this.sel_additional_item
  //     },
  //   });
  //     popover.onDidDismiss().then((dataReturned) => {
  //       if(dataReturned.data==1){
          
  //         this.getList();
  //         this.getDaywiseConsumerDetails();
  //       }else if(dataReturned.data==2){
  //        const idx= this.additional_by_ser_time.map(x=>x.ser_time==id).indexOf(id);
  //        this.additional_by_ser_time[idx]=dataReturned.role;
  //        console.log('change:',this.additional_by_ser_time)
  //       //  if(this.additional_by_ser_time[idx].show){
  //       //   setTimeout(()=>{
  //       //     var titleELe = this.document.getElementById(ser_time);
  //       //     console.log(titleELe)
  //       //     this.content.scrollToPoint(0, titleELe.offsetHeight, 1000);
  //       //   },500)
  //       //  }
  //       }

  //     });

    
  //   return await popover.present();
  // }
  // async markLeave(ev) {
  //   const popover = await this.popCntrl.create({
  //     component: MarkLeaveSingledayComponent,
  //     event: ev,
  //     backdropDismiss: true,
  //     cssClass: 'dining-more-options-popover',
  //     componentProps: {
        
  //       date:this.date,
  //       menu_id:this.menu.id,
  //       consumer_id:this.consumer.user.user_id,
        
  //     },
  //   });
  //     popover.onDidDismiss().then((dataReturned) => {
  //       if(dataReturned.data==1){
  //        this.leaveMarked=true
  //       }
  //     });

    
  //   return await popover.present();
  // }
  // async allergicAlert(allergies,i,serving_id) {
  //   const modal = await this.modalCntrl.create({
  //     component: AllergicInfoComponent,
  //     cssClass:'dining-allergicinfo-modal',
  //     componentProps:{
  //       consumer:this.consumer.user.name,
  //       allergies:allergies
  //     }
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
  //     if(dataReturned.data==2){
  //       this.selectItem(i,serving_id,1);
  //     }
  //   });
  //   return await modal.present();
  // }

  // async confirmAlert(){
  //   const modal = await this.modalCntrl.create({
  //      component: OrderConfirmationComponent,
  //    cssClass:'din-order-confirmation-modal'
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
      
  //   });
  //   return await modal.present();
  //  }



  //  async ionViewWillEnter(){
  //   let menu=this.route.snapshot.paramMap.get('menu')
  //   this.menu=JSON.parse(menu);
  //   this.date=this.route.snapshot.paramMap.get('date');
  //   this.dateCopy=this.route.snapshot.paramMap.get('date');
  //   this.consumer=JSON.parse(this.route.snapshot.paramMap.get('consumer'));
  //   this.wing=this.route.snapshot.paramMap.get('wing');
  //   let url=this.config.domain_url+'ordering_consumer_profile';
  //   this.additional_by_ser_time=[];
  //   const cid=await this.storage.get('COMPANY_ID');
  //   const bid=await this.storage.get('BRANCH');
  //   this.tz=await this.storage.get('TIMEZONE');
  //   let headers=await this.config.getHeader();
  //   let body={
  //     user_id:this.consumer.user.user_id,
  //     company_id:cid
  //   }
  //   this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //     console.log(res);
  //     this.details=res.data;
  //     this.wing=this.details.resident.wing.name;

  //     if(this.details.selected_diets){
  //       this.diet_type=this.details.selected_diets.split(/\, +/);
  //     }
  //     if(this.details.allergies){
  //       this.allergies=this.details.allergies.split(/\, +/);
  //     }
  //     if(this.details.fluid_cap_target){
  //     this.fluid_cap_target=this.details.fluid_cap_target
  //     }else{
  //       this.fluid_cap_target=0
  //     }
  //     if(this.details.meal_size){
  //       this.mealSize=this.details.meal_size.id;
  //     }
  //   })
    
  //   this.getList();
  //   this.getDaywiseConsumerDetails();
  //   let currentDate=moment().format('DD MMM YYYY');
  //   let startDate=moment(this.menu.start_date);
  //   let end=moment(this.menu.enddate);
  //   var start=startDate.clone();
  //   while(start.isSameOrBefore(end)){
  //     this.dates.push(start.format('DD MMM YYYY'));
  //     start.add(1,'days');
  //   }
  //   if(moment(this.date).isSameOrAfter(moment(currentDate).clone())){
  //     this.hide_footer=false
  //   }else{
  //     this.hide_footer=true
  //   }
  //   console.log('foot:',moment(this.date),moment(currentDate).clone(),moment(this.date).isSameOrAfter(moment(currentDate).clone()))
  //   let date=moment(this.date).format('DD MMM YYYY')
  //    this.currentIdx = this.dates.map(item => item).indexOf(date);

  //    this.platform.ready().then(() => {
  //     console.log('Width: ' + this.platform.width());
  //     console.log('Height: ' + this.platform.height());
  //     this.screen_width=this.platform.width();
  //     this.screen_height=this.platform.height();
  //   });
  //   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
  //     this.back();
  //     }); 
  //  }

  //  ionViewWillLeave() { 
  //   this.subscription.unsubscribe();
  //   }
    
  //     back(){
  //       this.router.navigate(['/dining-select-choice',{menu:JSON.stringify(this.menu),date:this.dateCopy}])
  //     }
  //     async getList(){
  //       // this.showLoading();
  //       let url=this.config.domain_url+'get_menu_details_bydate/'+this.menu.id+'/'+moment(this.date).format('yyyy-MM-DD');
  //     console.log(url);
  //     const cid=await this.storage.get('COMPANY_ID');
  //     const bid=await this.storage.get('BRANCH');
  //     let headers=await this.config.getHeader();
  //     this.http.get(url,{headers}).subscribe((res:any)=>{
  //       console.log('items:',res);
  //       this.menuItems=res.data.menu_details;
  //       this.additionalItems=res.data.additionalitems;
  //       // this.dismissLoader();
  //       this.menuItems.forEach(el=>{
  //         this.setAdditionalitem(el.serving_time_id);
  //       })
  //     })
  //     }

  //     allergicIcon(i){
  //       let c;
  //       if(i.itemdetailsall.alergies){
  //       i.itemdetailsall.alergies.map(x => {
          
  //         if (this.allergies.includes(x.alergy.alergy)) {
  //           c=true;
  //           }
  //           // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
  //         })
  //       }
  //         return c;
  //     }

  //    cutoffTime(menu){

  //       let c;
  //       let d=moment.tz(new Date(),this.tz).format('YYYY-MM-DD HH:mm:ss');
  //       let cutoff=moment(menu.serving_date+' '+menu.start_time);
  //       let current=moment(d);
      
  //       if(current.diff(cutoff)>0){
  //         c=true
  //       }else{
  //         c=false
  //       }
  //       return c;
  //     }

  //     setMealSize(i,servetime){
  //       let m,Idx;
  //       // if(i.itemdetailsall.servingquantity&&i.itemdetailsall.servingquantity.length){
  //       // i.itemdetailsall.servingquantity.map(x => {

  //       //   if(x.meal_size.id==4){
  //       //     if (this.mealSize==1) {
  //       //       m='S';
  //       //     }else if(this.mealSize==2){
  //       //       m='M'
  //       //     }else if(this.mealSize==3){
  //       //       m='L'
  //       //     }else{
  //       //       m='N'
  //       //     }
  //       //   }else if(x.meal_size.id==3){
  //       //     if (this.mealSize==1) {
  //       //       m='S';
  //       //     }else if(this.mealSize==2){
  //       //       m='M'
  //       //     }else{
  //       //       m='L'
  //       //     }
  //       //   }else if(x.meal_size.id==2){
  //       //     if (this.mealSize==1) {
  //       //       m='S';
  //       //     }else {
  //       //       m='M'
  //       //     }
  //       //   }else if(x.meal_size.id==1){
           
  //       //       m='S';
            
  //       //   }
          
  //       //   })
  //       // }else{
  //       //   if (this.mealSize==1) {
  //       //     m='S';
  //       //   }else if(this.mealSize==2){
  //       //     m='M'
  //       //   }else if(this.mealSize==3){
  //       //     m='L'
  //       //   }else{
  //       //     m='N'
  //       //   }
  //       // }
        
  //       // this.preOrderedItems.map(x=>{
  //       //   if(x.pre_item_orders.length){
  //       //     for(let j in x.pre_item_orders){
  //       //     // x.pre_item_orders.map(y=>{
  //       //      if(x.pre_item_orders[j].menu_item_id==i.id){
  //       //       if (x.pre_item_orders[j].meal_size==1) {
  //       //               m='S';
  //       //             }else if(x.pre_item_orders[j].meal_size==2){
  //       //               m='M'
  //       //             }else if(x.pre_item_orders[j].meal_size==3){
  //       //               m='L'
  //       //             }else if(x.pre_item_orders[j].meal_ize==4){
  //       //               m='N'
  //       //             }else{
  //       //               m=null
  //       //             }
  //       //      }else{
  //       //        m=null
  //       //      }
  //       //     // })
  //       //   }
  //       //   }
  //       // })
  //       if(this.selected.includes(i.id)){

          
  //         // let Idx;
  //       const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(servetime);
  //       if(this.preOrderedItems[idx].pre_item_orders.length){
  //         Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
  //            if(Idx>=0){
  //             if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size==1) {
  //                             m='S';
  //                           }else if(this.preOrderedItems[idx].pre_item_orders[Idx].meal_size==2){
  //                             m='M'
  //                           }else if(this.preOrderedItems[idx].pre_item_orders[Idx].meal_size==3){
  //                             m='L'
  //                           }else if(this.preOrderedItems[idx].pre_item_orders[Idx].meal_size==4){
  //                             m='N'
  //                           }else{
  //                             m=null
  //                           }
  //                    }else{
  //                      m=null
  //                    }
  //                   }
  //                 }
  //       return m;
  //     }

  //     setQuantity(i,servetime){
  //       let q,Idx;
  //       // i.itemdetailsall.servingquantity.map(x => {
  //       //   q=x.serving_qunatity
  //       // })
  //       if(this.selected.includes(i.id)){
  //         // let Idx;
  //       const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(servetime);
  //       if(this.preOrderedItems[idx].pre_item_orders.length){
  //         Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
  //            if(Idx>=0){
  //              q=this.preOrderedItems[idx].pre_item_orders[Idx].quantity;
  //            }else{
  //              q=null
  //            }
  //       }
  //       // this.preOrderedItems.map(x=>{
  //       //   if(x.pre_item_orders.length){
  //       //     Idx = x.pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
  //       //      if(Idx>=0){
  //       //        q=x.pre_item_orders[Idx].quantity;
  //       //      }else{
  //       //        q=null
  //       //      }
            
  //       //     // })
  //       //   }
  //       // })
       
  //     }
  //       // if(i.itemdetailsall.servingquantity&&i.itemdetailsall.servingquantity.length){
  //       //   i.itemdetailsall.servingquantity.map(x => {
  //       //     if(x.serving_qunatity){
  //       //     q=x.serving_qunatity
  //       //     }else{
  //       //       q=1
  //       //     }
  //       //   })
  //       // }else{
  //       //   q=1
  //       // }
        
  //       return q;
  //     }

  //     setOptions(i,servetime){
  //       let q,Idx;
  //       // i.itemdetailsall.servingquantity.map(x => {
  //       //   q=x.serving_qunatity
  //       // })
  //       if(this.selected.includes(i.id)){
  //         // let Idx;
  //       const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(servetime);
  //       if(this.preOrderedItems[idx].pre_item_orders.length){
  //         Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
  //            if(Idx>=0&&(this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options&&this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options.length)){
  //              q=this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options;
  //            }else{
  //              q=null
  //            }
  //       }
       
       
  //     }
       
        
  //       return q;
  //     }

  //     async getDaywiseConsumerDetails(){

  //       // if(!this.loading){
  //         this.showLoading();
  //       // }
  //       let url=this.config.domain_url+'ordering_daywise_item_listing';
  //       console.log(url);
  //       const cid=await this.storage.get('COMPANY_ID');
  //       const bid=await this.storage.get('BRANCH');
  //       const uid=await this.storage.get('USER_ID');
  //       let headers=await this.config.getHeader();
  //       let body={
  //         company_id:cid,
  //         consumer_id:this.consumer.user.user_id,
  //         dining_id:this.menu.id,
  //         date:moment(this.date).format('yyyy-MM-DD'),
  //         created_by:uid
  //       }
  //       this.http.post(url,body,{headers}).subscribe((res:any)=>{
          
  //         this.skipped=res.data.skipped_meal_times;
  //         this.serve_area=res.data.serve_area;
  //         console.log('res:',res,this.serve_area);
  //         if(res.data.is_leave){
  //           this.leaveMarked=true;
  //           this.hide_footer=true;
  //         }else{
  //           this.leaveMarked=false;
  //         }
  //         if(res.data.is_edit){
  //           this.editOrder=true
  //         }else{
  //           this.editOrder=false;
  //         }
  //         this.preOrderedItems=res.data.meals_with_items;
  //         if(this.preOrderedItems.length){
  //           this.preOrderedItems.map(x=>{
  //             if(x.pre_item_orders.length){
  //               x.pre_item_orders.map(y=>{
  //                 this.selected.push(y.menu_item_id)
  //               })
  //             }
  //             if(x.additional_items.length){
  //               x.additional_items.map(y=>{
  //                 if(y.selected_data){
  //                   this.sel_additional_item.push(y.selected_data.item_id);
  //                 }
  //               })
  //             }
  //             if(x.all_coloumns.length){
  //               x.all_coloumns.map(y=>{
  //                 if(y.items[0].is_ordered){
  //                   this.selected.push(y.items[0].id)
  //                 }
  //               })
  //             }
  //           })
  //         }
  //         this.dismissLoader();
  //       })
  //     }

  //     skippedMeal(i){
  //       if(this.skipped.includes(i)){
  //         return true
  //       }else{
  //         return false
  //       }
  //     }

  //     async unmarkLeave(){
  //       const cid=await this.storage.get('COMPANY_ID');
  //       const bid=await this.storage.get('BRANCH');
  //       const uid=await this.storage.get('USER_ID');
  //       let url=this.config.domain_url+'remove_leave_via_ordering';
  //       let headers=await this.config.getHeader();
  //       let body={
  //         consumer_id:this.consumer.user.user_id,
  //         date:moment(this.date).format('YYYY-MM-DD'),
  //         updated_by:uid
  //       }
  //       this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //         // if(res.skipped=='Skipped')
  //         this.presentAlert('Leave deleted successfully.');
  //         this.leaveMarked=false;
  //         this.hide_footer=false;
  //         this.getList();
  //         this.getDaywiseConsumerDetails();
          
  //       },error=>{
    
  //       })
  //     }
  //     async presentAlert(mes) {
  //       const alert = await this.toastCntlr.create({
  //         message: mes,
  //         cssClass:'toastStyle',
  //         duration: 3000      
  //       });
  //       alert.present(); //update
  //     }


  //     preSavingItem(i,serving_id,or,item,menu){
        
  //       if(i.subitem==1&&!this.cutoffTime(menu)){
  //         this.openContinentalDetails(i,serving_id);
  //       }else{
  //         if(!this.editOrder&&!this.skippedMeal(menu.serving_time_id)){
  //       if(!this.availableItem(i)){
  //       }else{
  //         if(this.cutoffTime(menu)){
  //           console.log('Cutoff time over')
  //         }else{
  //       console.log('item:',item)
  //       if(or==2){
  //         let count=1;
  //         item.item.map(x => {
  //           if(this.selected.includes(x.id)){
  //             // var index = this.selected.indexOf(x.id);
  //             // this.selected.splice(index, 1);
  //             this.selectItem(x,serving_id,2);
              
  //             count++;
              
  //             // this.selectItem(i,serving_id,1);
  //           }else{
  //             if(x.id==i.id){
  //               count=1
  //             }
  //             if(count==1){
  //             if(this.allergicIcon(i)){
  //               this.allergicAlert(i.itemdetailsall.alergies,i,serving_id);
  //             }else{
  //             this.selectItem(i,serving_id,1)
  //             }
  //           }count++;
  //           }
  //         })

          

  //       }else{
  //         if(this.selected.includes(i.id)){
  //         //   var index = this.selected.indexOf(i.id);
  //         // this.selected.splice(index, 1);
  //         this.selectItem(i,serving_id,2)
  //         }else{
  //       if(this.allergicIcon(i)){
  //         this.allergicAlert(i.itemdetailsall.alergies,i,serving_id);
  //       }else{
  //         this.selectItem(i,serving_id,1);
  //       }
  //     }
  //     }
  //   }
  //   }
  // }
  // }
  //   }
  //   async selectItem(i,serving_id,u){
      
      
  //     const cid=await this.storage.get('COMPANY_ID');
  //     const bid=await this.storage.get('BRANCH');
  //     const uid=await this.storage.get('USER_ID');
  //     let url=this.config.domain_url+'presave_ordering_item';
  //     let headers=await this.config.getHeader();
  //     let body;
  //     body={
  //       consumer_id:this.consumer.user.user_id,
  //       company_id:cid,
  //       dining_id:this.menu.id,
  //       date:moment(this.date).format('YYYY-MM-DD'),
  //       created_by:uid,
  //       menu_item_id:i.id,
  //       serving_time_id:serving_id
  //     }
  //     if(u==2){
  //       body.uncheck=1
  //     }
  //     // console.log('body:',body,i,u)
  //     this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //      console.log('presave:',res,body);
  //      if(res.status=='success'){
  //        if(u==1){
  //         if(!this.selected.includes(i.id))
  //       this.selected.push(i.id)
  //        }else if(u==2){
  //          const idx=this.selected.indexOf(i.id);
  //          this.selected.splice(idx,1);
  //        }
  //      }
        
  //     },error=>{
  
  //     })
  //   }

  //   async confirm(){
  //     if(!this.editOrder){
  //     if(this.selected.length==0){
  //       this.presentAlert('Please select any item.');
  //     }else{
  //     const cid=await this.storage.get('COMPANY_ID');
  //     const bid=await this.storage.get('BRANCH');
  //     const uid=await this.storage.get('USER_ID');
  //     let url=this.config.domain_url+'confirm_single_day_order';
  //     let headers=await this.config.getHeader();
  //     let body={
  //       consumer_id:this.consumer.user.user_id,
  //       dining_id:this.menu.id,
  //       date:moment(this.date).format('YYYY-MM-DD'),
  //       created_by:uid,
        
  //     }
  //     this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //      console.log('confirm:',res,body);
  //      this.presentAlert('Order placed successfully.')
  //      setTimeout(()=>{
  //       this.back();
  //      },3000)
  //     },error=>{
  
  //     })
  //   }
  // }
  //   }

  //   async showItemdetails(item,st,serving_id){
  //     if(item.subitem==1){
  //       this.openContinentalDetails(item,serving_id)
  //     }else{
  //       if(!this.availableItem(item)||this.cutoffTime(this.menu)||this.skippedMeal(serving_id)){
  //       }else{
  //         console.log('getCom:',this.getComment(item,serving_id),this.setMealSize(item,serving_id))
  //     const modal = await this.modalCntrl.create({
  //       component: MenuItemDetailsComponent,
  //       cssClass:'full-width-modal',
  //       componentProps:{
  //         menu:item,
  //         date:this.date,
  //         serving_time:st,
  //         menu_id:this.menu.id,
  //         consumer:this.consumer.user.user_id,
  //         serving_time_id:serving_id,
  //         ml_sz:this.setMealSize(item,serving_id),
  //         sel_quantity:this.setQuantity(item,serving_id),
  //         comment:this.getComment(item,serving_id),
  //         options:this.setOptions(item,serving_id)
  //       }
  //     });
  //     modal.onDidDismiss().then((dataReturned) => {
  //       if(dataReturned.data){
          
  //         // this.selected.push(dataReturned.data);
  //         this.getDaywiseConsumerDetails();
  //       }
  //     });
     
  //     return await modal.present();
  //   }
  // }
  //   }


  //   previous(){
  //     if(this.currentIdx==0){
  //         console.log('reached first item')
  //     }else{
  //       console.log('curr:',this.currentIdx,this.dates[this.currentIdx]);
  //       this.currentIdx--;
  //       console.log('changedcur:',this.currentIdx,this.dates[this.currentIdx])
  //       this.date=this.dates[this.currentIdx];
  //       this.getList();
  //       this.getDaywiseConsumerDetails();
        
  //       let currentDate=moment().format('DD MMM YYYY');
  //       if(moment(this.date).isSameOrAfter(moment(currentDate))){
  //         this.hide_footer=false
  //       }else{
  //         this.hide_footer=true
  //       }
  //     }
  //   }
  //   next(){
  //     if(this.currentIdx==(this.dates.length-1)){
  //       console.log('reached end')
  //     }else{
  //       this.currentIdx++;
  //       this.date=this.dates[this.currentIdx];
  //       this.getList();
  //       this.getDaywiseConsumerDetails();
        
  //       let currentDate=moment().format('DD MMM YYYY');
  //       if(moment(this.date).isSameOrAfter(moment(currentDate))){
  //         this.hide_footer=false
  //       }else{
  //         this.hide_footer=true
  //       }
  //     }
  //   }

  //   async showLoading() {
  //     this.loading=true;
  //     const loading = await this.loadingCtrl.create({
  //       cssClass: 'dining-loading',
  //       message: 'Please wait...',
  //       // message: '<ion-img src="assets/loader.gif"></ion-img>',
  //       spinner: null,
  //       // duration: 3000
  //     });
  //     return await loading.present();
  //   }
    
  //   async dismissLoader() {
  //     this.loading=false;
  //       return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  //   }

  //    getComment(i,servetime){
  //     let c;
  //     if(this.selected.includes(i.id)){
  //       let Idx;
  //     const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(servetime);
  //     if(this.preOrderedItems[idx].pre_item_orders.length){
  //       Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
  //          if(Idx>=0){
  //            c=this.preOrderedItems[idx].pre_item_orders[Idx].comment;
  //          }else{
  //            c=null
  //          }
  //     }
  //   }
    
  //   return c;
  //   }


  //   availableItem(i){
      
  //     let avl;
      
  //     if(i.itemdetailsall.foodtexture&&i.itemdetailsall.foodtexture.length){
  //    const idx= i.itemdetailsall.foodtexture.map(x=>x.fdtexture.id).indexOf(this.details.food_texture.id);
  //    const Idx= i.itemdetailsall.foodtexture.map(x=>x.fdtexture.id).indexOf(this.details.fluid_texture.id);
  //    if(idx>=0||Idx>=0){
  //      avl=true
  //    }else{
  //      avl=false
  //    }
     
  //    }else{
  //      avl=true
  //    }
    
  //     return avl;
  //   }
  //   incompatable(item,serving_time_id){
  //     let inc=false;
  //     const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(serving_time_id);
  //     const Idx=this.preOrderedItems[idx].all_coloumns.map(x=>x.id).indexOf(item.id);
  //     if(Idx<0){
  //       inc=true
  //     }
  //     return inc;
     

  //   }

  //  async openContinentalDetails(item,serving_id){
  //    console.log('screen:',this.screen_width,this.screen_height);
  //    let component;
  //   //  if(this.screen_width>=768){
  //   //    component=ContinentalItemTabviewComponent
  //   //  }else{
  //      component=ContinentalItemComponent;
  //   //  }

  //    console.log(component)
  //   const modal = await this.modalCntrl.create({

  //     component: component,
  //     cssClass:'full-width-modal',
  //     componentProps:{
  //       menu:item,
  //         date:this.date,
  //         menu_id:this.menu.id,
  //         consumer:this.consumer.user.user_id,
  //         con_name:this.consumer.user.name,
  //         serving_time_id:serving_id,
  //         fdtexture:this.details.food_texture.id,
  //         fltexture:this.details.fluid_texture.id,
  //         preOrderedItems:this.preOrderedItems,
  //         allergies:this.allergies,
  //         edit:this.editOrder,
  //         skipped:this.skippedMeal(serving_id)
  //     }
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
  //     if(dataReturned.data){
        
  //       this.selected.push(dataReturned.data);
  //     }
  //   });
   
  //   return await modal.present();
  //   }

  //   async edit(){
  //     this.editOrder=false;
  //     const cid=await this.storage.get('COMPANY_ID');
  //     const bid=await this.storage.get('BRANCH');
  //     const uid=await this.storage.get('USER_ID');
  //     let headers=await this.config.getHeader();


     
    
  //     let url=this.config.domain_url+'enable_dining_order_via_date';
      
  //     let body;
  //     body={
  //       consumer_id:this.consumer.user.user_id,
  //       dining_id:this.menu.id,
  //       date:moment(this.date).format('YYYY-MM-DD'),
  //       created_by:uid,
        
  //     }
      
    
  //     console.log('body:',body)
  //     this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //      console.log('edit:',res,body);
       
        
  //     },error=>{
  //       console.log('err:',error)
  //     })

  //   }

  //   setAdditionalitem(id){
  //     console.log('setadditionalitem:',id)
  //     let item=[];
  //     this.additionalItems.forEach(element => {
  //       element.servingtime.map(x => {
  //         if(id==x.servingtime[0].id){
  //           item.push(element.item)
            
  //           // this.servingTime=x.servingtime[0].servingtime
  //         }
  //       })
        
  //     });
  //     this.additional_by_ser_time.push({item:item,ser_time:id,show:false});
  //   }


  //   async selectAdditionalItem(item,id,menu){
  //     const cid=await this.storage.get('COMPANY_ID');
  //       const bid=await this.storage.get('BRANCH');
  //       const uid=await this.storage.get('USER_ID');
  //       let headers=await this.config.getHeader();
  
  
       
      
  //       let url=this.config.domain_url+'presave_additional_ordering_item';
        
  //       let body;
  //       body={
  //         consumer_id:this.consumer.user.user_id,
  //         dining_id:this.menu.id,
  //         date:moment(this.date).format('YYYY-MM-DD'),
  //         created_by:uid,
  //         item_id:item.id,
  //         serving_time_id:id
  //       }
        
  //     if(this.sel_additional_item.includes(item.id)){
  //       body.uncheck=1
  //     }
  //       console.log('body:',body)
  //       this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //        console.log('presave:',res,body);
  //        if(res.status=='success'){
  //          if(res.message=='created'){
  //         this.sel_additional_item.push(item.id)
  //          }else{
  //            var idx=this.sel_additional_item.indexOf(item.id);
  //            this.sel_additional_item.splice(idx,1)
  //          }
  //        }
          
  //       },error=>{
  //         console.log('err:',error)
  //       })
      
  //   }


  //   changedServeArea(serv_id){
  //     let a;
      
  //     const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(serv_id);
  //       console.log('fu:',serv_id,idx,this.preOrderedItems[idx])
  //       // if(x.serving_time_id==serv_id){
  //       a=this.preOrderedItems[idx].changed_serving_area_id
  //       // }
  //     // })
  //     return a;
  //   }


    async dietaryPreference() {
      const modal = await this.modalCntrl.create({
        component: DietaryPreferencesComponent,
        cssClass: 'dining-dpreference-modal',
        componentProps: {
          details: this.details
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
  
      });
      return await modal.present();
    }
  
    async copy() {
      const modal = await this.modalCntrl.create({
        component: SelectConfirmationComponent,
        cssClass: 'dining-copy-previous-modal',
      });
      modal.onDidDismiss().then((dataReturned) => {
  
      });
      return await modal.present();
    }
    async chooseDate() {
      const modal = await this.modalCntrl.create({
        component: ChooseDateComponent,
        cssClass: 'din-choosedate-modal',
        componentProps: {
          menu: this.menu
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data) {
          this.date = dataReturned.data;
          // this.getList();
          this.selected=[];
          this.sel_additional_item=[];
          this.showLoading();
          
          // this.getDaywiseConsumerDetails();
          this.getDetails();
  
        }
      });
      return await modal.present();
    }
  
    async options(ev, item) {
      // let sel_area;
      // const targetIdx=this.details.consumer_serve_areas.map(x=>x.serving_time.id).indexOf(id);
      // if(targetIdx>=0){
      //   sel_area=this.details.consumer_serve_areas[targetIdx].serve_area
      // }else{
      //   sel_area=null
      // }
  
      console.log('add:', this.additional_by_ser_time)
  
      let add;
      this.additional_by_ser_time.forEach(el=>{
        if(el.ser_time==item.serving_time_id){
          add=el
        }
      })
  
      const popover = await this.popCntrl.create({
        component: MenuOptionsComponent,
        id: 'menu_options',
        event: ev,
        backdropDismiss: true,
        cssClass: 'dining-more-options-popover',
        componentProps: {
          menu: item,
          additional:add,
          // serving_id:id,
          date: this.date,
          // menu_id:this.menu.id,
          consumer_id: this.consumer.user.user_id,
          // servingTime:ser_time,
          // is_skipped:this.skippedMeal(id),
          serve_area: this.details.serving_area_id,
          // sel_area:sel_area,
          // sel_additional_item:this.sel_additional_item
        },
      });
      popover.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == 1) {
  
          // this.getList();
          this.showLoading();
          // this.getDaywiseConsumerDetails();
          this.getDetails();
        } else if (dataReturned.data == 2) {
          //  const idx= this.additional_by_ser_time.map(x=>x.ser_time==id).indexOf(id);
          //  this.additional_by_ser_time[idx]=dataReturned.role;
          //  console.log('change:',this.additional_by_ser_time)
          //  if(this.additional_by_ser_time[idx].show){
          //   setTimeout(()=>{
          //     var titleELe = this.document.getElementById(ser_time);
          //     console.log(titleELe)
          //     this.content.scrollToPoint(0, titleELe.offsetHeight, 1000);
          //   },500)
          //  }
        }
  
      });
  
  
      return await popover.present();
    }
    async markLeave(ev) {
      const popover = await this.popCntrl.create({
        component: MarkLeaveSingledayComponent,
        event: ev,
        backdropDismiss: true,
        cssClass: 'dining-more-options-popover',
        componentProps: {
  
          date: this.date,
          menu_id: this.menu.id,
          consumer_id: this.consumer.user.user_id,
  
        },
      });
      popover.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == 1) {
          this.leaveMarked = true;
          this.hide_footer = true;
        }
      });
  
  
      return await popover.present();
    }
    async allergicAlert(allergies, i, serving_id,or,item) {
      const modal = await this.modalCntrl.create({
        component: AllergicInfoComponent,
        cssClass: 'dining-allergicinfo-modal',
        componentProps: {
          consumer: this.consumer.user.name,
          allergies: allergies
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == 2) {
          this.selectItem(i, serving_id, 1,or,item);
         
        }
      });
      return await modal.present();
    }
  
    async confirmAlert(stat) {
      const modal = await this.modalCntrl.create({
        component: OrderConfirmationComponent,
        cssClass: 'din-order-confirmation-modal',
        componentProps:{
          status:stat
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
          if(dataReturned.data){
            this.hide_footer=true;
            this.editOrder=true;
            this.confirmsingledayorder();
          }
      });
      return await modal.present();
    }
  
  async noItemAlert(){
    const modal = await this.modalCntrl.create({
      component: NoItemAlertComponent,
      cssClass: 'din-order-confirmation-modal',
      componentProps:{
        date: this.date
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data){
          this.hide_footer=true;
          this.editOrder=true;
          this.confirmsingledayorder();
        }
    });
    return await modal.present();
  }
  
    async ionViewWillEnter() {
      let menu = this.route.snapshot.paramMap.get('menu')
      this.menu = JSON.parse(menu);
      this.date = this.route.snapshot.paramMap.get('date');
      this.dateCopy = this.route.snapshot.paramMap.get('date');
      this.consumer = JSON.parse(this.route.snapshot.paramMap.get('consumer'));
      this.wing = this.route.snapshot.paramMap.get('wing');
      let url = this.config.domain_url + 'ordering_consumer_profile';
      this.additional_by_ser_time = [];
      this.selected=[];
      this.sel_additional_item=[];
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      this.tz = await this.storage.get('TIMEZONE');
      let headers = await this.config.getHeader();
      let body = {
        user_id: this.consumer.user.user_id,
        company_id: cid
      }
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        console.log(res);
        this.details = res.data;
        this.wing = this.details.resident.wing.name;
  
        if (this.details.selected_diets) {
          this.diet_type = this.details.selected_diets.split(/\, +/);
        }
        if (this.details.allergies) {
          this.allergies = this.details.allergies.split(/\, +/);
        }
        if (this.details.fluid_cap_target) {
          this.fluid_cap_target = this.details.fluid_cap_target
        } else {
          this.fluid_cap_target = 0
        }
        if (this.details.meal_size) {
          this.mealSize = this.details.meal_size.id;
        }
      })
  
      // this.getList();
      this.showLoading();
      // this.getDaywiseConsumerDetails();
      this.getDetails();
      
      let currentDate = moment().format('DD MMM YYYY');
      let startDate = moment(this.menu.start_date);
      let end = moment(this.menu.enddate);
      var start = startDate.clone();
      while (start.isSameOrBefore(end)) {
        this.dates.push(start.format('DD MMM YYYY'));
        start.add(1, 'days');
      }
      if (moment(this.date).isSameOrAfter(moment(currentDate).clone())) {
        this.hide_footer = false
      } else {
        this.hide_footer = true
      }
      console.log('foot:', moment(this.date), moment(currentDate).clone(), moment(this.date).isSameOrAfter(moment(currentDate).clone()))
      let date = moment(this.date).format('DD MMM YYYY')
      this.currentIdx = this.dates.map(item => item).indexOf(date);
  
      this.platform.ready().then(() => {
        console.log('Width: ' + this.platform.width());
        console.log('Height: ' + this.platform.height());
        this.screen_width = this.platform.width();
        this.screen_height = this.platform.height();
      });
      this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
        this.back();
      });
    }
  
    ionViewWillLeave() {
      this.subscription.unsubscribe();
    }
  
    back() {
      this.router.navigate(['/dining-select-choice', { menu: JSON.stringify(this.menu), date: this.dateCopy }])
    }
    async getList() {
      // this.showLoading();
      let url = this.config.domain_url + 'get_menu_details_bydate/' + this.menu.id + '/' + moment(this.date).format('yyyy-MM-DD');
      console.log(url);
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      let headers = await this.config.getHeader();
      this.http.get(url, { headers }).subscribe((res: any) => {
        console.log('items:', res, url);
        this.menuItems = res.data.menu_details;
        this.additionalItems = res.data.additionalitems;
        // this.dismissLoader();
        // this.menuItems.forEach(el=>{
        //   this.setAdditionalitem(el.serving_time_id);
        // })
      })
    }
  
    allergicIcon(i) {
      let c;
      if (i.itemdetails.alergies) {
        i.itemdetails.alergies.map(x => {
  
          if (this.allergies.includes(x.alergy.alergy)) {
            c = true;
          }
          // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
        })
      }
      return c;
    }
  
    cutoffTime(menu) {
  
      let c;
      let d = moment.tz(new Date(), this.tz).format('YYYY-MM-DD HH:mm:ss');
      let cutoff = moment(menu.serving_date + ' ' + menu.end_time);
      let current = moment(d);
  
      if (current.diff(cutoff) > 0) {
        c = true
      } else {
        c = false
      }
      return c;
    }
  
    setMealSize(i, servetime) {
      let m, Idx;
      // if(i.itemdetailsall.servingquantity&&i.itemdetailsall.servingquantity.length){
      // i.itemdetailsall.servingquantity.map(x => {
  
      //   if(x.meal_size.id==4){
      //     if (this.mealSize==1) {
      //       m='S';
      //     }else if(this.mealSize==2){
      //       m='M'
      //     }else if(this.mealSize==3){
      //       m='L'
      //     }else{
      //       m='N'
      //     }
      //   }else if(x.meal_size.id==3){
      //     if (this.mealSize==1) {
      //       m='S';
      //     }else if(this.mealSize==2){
      //       m='M'
      //     }else{
      //       m='L'
      //     }
      //   }else if(x.meal_size.id==2){
      //     if (this.mealSize==1) {
      //       m='S';
      //     }else {
      //       m='M'
      //     }
      //   }else if(x.meal_size.id==1){
  
      //       m='S';
  
      //   }
  
      //   })
      // }else{
      //   if (this.mealSize==1) {
      //     m='S';
      //   }else if(this.mealSize==2){
      //     m='M'
      //   }else if(this.mealSize==3){
      //     m='L'
      //   }else{
      //     m='N'
      //   }
      // }
  
      // this.preOrderedItems.map(x=>{
      //   if(x.pre_item_orders.length){
      //     for(let j in x.pre_item_orders){
      //     // x.pre_item_orders.map(y=>{
      //      if(x.pre_item_orders[j].menu_item_id==i.id){
      //       if (x.pre_item_orders[j].meal_size==1) {
      //               m='S';
      //             }else if(x.pre_item_orders[j].meal_size==2){
      //               m='M'
      //             }else if(x.pre_item_orders[j].meal_size==3){
      //               m='L'
      //             }else if(x.pre_item_orders[j].meal_ize==4){
      //               m='N'
      //             }else{
      //               m=null
      //             }
      //      }else{
      //        m=null
      //      }
      //     // })
      //   }
      //   }
      // })
      if (this.selected.includes(i.id)) {
  
  
        // let Idx;
        const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
        if (this.preOrderedItems[idx].pre_item_orders.length) {
          Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
          if (Idx >= 0) {
            if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 1) {
              m = 'S';
            } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 2) {
              m = 'M'
            } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 3) {
              m = 'L'
            } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 4) {
              m = 'N'
            } else {
              m = null
            }
          } else {
            m = null
          }
        }
      }
      return m;
    }
  
    setQuantity(i, servetime) {
      let q, Idx;
      // i.itemdetailsall.servingquantity.map(x => {
      //   q=x.serving_qunatity
      // })
      if (this.selected.includes(i.id)) {
        // let Idx;
        const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
        if (this.preOrderedItems[idx].pre_item_orders.length) {
          Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
          if (Idx >= 0) {
            q = this.preOrderedItems[idx].pre_item_orders[Idx].quantity;
          } else {
            q = null
          }
        }
        // this.preOrderedItems.map(x=>{
        //   if(x.pre_item_orders.length){
        //     Idx = x.pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
        //      if(Idx>=0){
        //        q=x.pre_item_orders[Idx].quantity;
        //      }else{
        //        q=null
        //      }
  
        //     // })
        //   }
        // })
  
      }
      // if(i.itemdetailsall.servingquantity&&i.itemdetailsall.servingquantity.length){
      //   i.itemdetailsall.servingquantity.map(x => {
      //     if(x.serving_qunatity){
      //     q=x.serving_qunatity
      //     }else{
      //       q=1
      //     }
      //   })
      // }else{
      //   q=1
      // }
  
      return q;
    }
  
    setOptions(i, servetime) {
      let q, Idx;
      // i.itemdetailsall.servingquantity.map(x => {
      //   q=x.serving_qunatity
      // })
      if (this.selected.includes(i.id)) {
        // let Idx;
        const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
        if (this.preOrderedItems[idx].pre_item_orders.length) {
          Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
          if (Idx >= 0 && (this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options && this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options.length)) {
            q = this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options;
          } else {
            q = null
          }
        }
  
  
      }
  
  
      return q;
    }
  
    async getDaywiseConsumerDetails() {
  
      // if(!this.loading){
      // this.showLoading();
      this.selected=[];
      this.additional_by_ser_time=[];
      this.sel_additional_item=[];
      // }
      let url = this.config.domain_url + 'ordering_daywise_item_listing';
      console.log(url);
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let headers = await this.config.getHeader();
      let body = {
        company_id: cid,
        consumer_id: this.consumer.user.user_id,
        dining_id: this.menu.id,
        date: moment(this.date).format('yyyy-MM-DD'),
        created_by: uid
      }
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        this.menuItems = res.data.meals_with_items;
        
        // this.additionalItems=res.data.additionalitems;
        // this.dismissLoader();
        this.menuItems.forEach(el => {
          // this.setAdditionalitem(el.serving_time_id);
          this.additional_by_ser_time.push({ item: el.additional_items, ser_time: el.serving_time_id, show: false });
        })
        this.skipped = res.data.skipped_meal_times;
        this.serve_area = res.data.serve_area;
        console.log('res:', res, url);
        if (res.data.is_leave) {
          this.leaveMarked = true;
          this.hide_footer = true;
        } else {
          this.leaveMarked = false;
          this.hide_footer = false;
        }
        if (res.data.is_edit) {
          this.editOrder = true
        } else {
          this.editOrder = false;
        }
        this.preOrderedItems = res.data.meals_with_items;
        this.selected=[];
        if (this.preOrderedItems.length) {
          this.preOrderedItems.map(x => {
            if (x.pre_item_orders.length) {
              x.pre_item_orders.map(y => {
                this.selected.push(y.menu_item_id)
              })
            }
            if (x.additional_items.length) {
              x.additional_items.map(y => {
                if (y.selected_data) {
                  this.sel_additional_item.push(y.selected_data.item_id);
                }
              })
            }
            if (x.all_coloumns.length) {
              x.all_coloumns.map(y => {
                if(y.items.length==1){
                if (y.items[0].is_ordered==true) {
                  this.selected.push(y.items[0].id)
                }
              }else{
                y.items.map(z=>{
                  if(z.is_ordered==true){
                    this.selected.push(z.id)
                  }
                })
              }
              })
            }
          })
        }
        console.log('sele:',this.selected)
        this.dismissLoader();
      })
    }
  
    skippedMeal(i) {
      if (this.skipped.includes(i)) {
        return true
      } else {
        return false
      }
    }
  
    async unmarkLeave() {
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let url = this.config.domain_url + 'remove_leave_via_ordering';
      let headers = await this.config.getHeader();
      let body = {
        consumer_id: this.consumer.user.user_id,
        date: moment(this.date).format('YYYY-MM-DD'),
        updated_by: uid
      }
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        // if(res.skipped=='Skipped')
        if(res.status=='error'){
          this.presentAlert(res.message);
        }else{
        this.presentAlert('Leave deleted successfully.');
        this.leaveMarked = false;
        this.hide_footer = false;
        // this.getList();
        this.selected=[];
          this.sel_additional_item=[];
        this.showLoading();
        // this.getDaywiseConsumerDetails();
        this.getDetails();
        }
  
      }, error => {
  
      })
    }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        cssClass: 'toastStyle',
        duration: 3000,
        position:'top'
      });
      alert.present(); //update
    }
  
  
    preSavingItem(i, serving_id, or, item, menu) {
  console.log('sk:',!menu.is_skipped)
      if (i.subitem == 1 && !this.cutoffTime(menu)) {
        this.openContinentalDetails(i, serving_id,menu,or,item);
      } else {
        if (!this.editOrder && !menu.is_skipped) {
          if (!this.availableItem(i)||i.visibility==0) {
          } else {
            if (this.cutoffTime(menu)) {
              console.log('Cutoff time over')
            } else {
              console.log('item:', item)
              // if (or == 2) {
              //   let count = 1;
              //   item.items.map(x => {
              //     if (this.selected.includes(x.id)) {
              //       // var index = this.selected.indexOf(x.id);
              //       // this.selected.splice(index, 1);
                    
              //       if (i.subitem == 1 ) {
              //         this.openContinentalDetails(i, serving_id,menu);
              //       }
              //       else{
              //         this.selectItem(x, serving_id, 2);
              //       }
              //         count++;
                    
  
              //       console.log('item already in selected')
  
              //       // this.selectItem(i,serving_id,1);
              //     } else {
              //       if (x.id == i.id) {
              //         count = 1
              //       }
              //       if (count == 1) {
                      
              //         // if (i.subitem == 1 ) {
              //         //   this.openContinentalDetails(i, serving_id,menu);
              //         // }else{
              //         if (this.allergicIcon(i)) {
              //           this.allergicAlert(i.itemdetails.alergies, i, serving_id);
              //         } else {
              //           this.selectItem(i, serving_id, 1)
              //         }
              //       } count++;
              //     }
              //     // }
              //   })
  
  
  
              // } else {
                if (this.selected.includes(i.id)) {
                  //   var index = this.selected.indexOf(i.id);
                  // this.selected.splice(index, 1);
                  this.selectItem(i, serving_id, 2,or,item)
                } else {
                  if (this.allergicIcon(i)) {
                    this.allergicAlert(i.itemdetails.alergies, i, serving_id,or,item);
                  } else {
                    this.selectItem(i, serving_id, 1,or,item);
                    // if (or == 2) {
                      
                    //     item.items.map(x => {
                    //       if (this.selected.includes(x.id)) {
                    //         var index = this.selected.indexOf(x.id);
                    //         this.selected.splice(index, 1);
                    //       }
                    //     })
                    //   }
                  }
                }
              // }
            }
          }
        }
      }
    }
    async selectItem(i, serving_id, u,or,item) {
  
  
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let url = this.config.domain_url + 'presave_ordering_item';
      let headers = await this.config.getHeader();
      let body;
      body = {
        consumer_id: this.consumer.user.user_id,
        company_id: cid,
        dining_id: this.menu.id,
        date: moment(this.date).format('YYYY-MM-DD'),
        created_by: uid,
        menu_item_id: i.id,
        serving_time_id: serving_id
      }
      if (u == 2) {
        body.uncheck = 1
      }
      // console.log('body:',body,i,u)
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        console.log('presave:', res, body,u,i.id);
        if (res.status == 'success') {
          if (or == 2) {
            //   let count = 1;
              item.items.map(x => {
                if (this.selected.includes(x.id)) {
                  var index = this.selected.indexOf(x.id);
                  this.selected.splice(index, 1);
                }
              })
            }
          if (u == 1) {
            if (!this.selected.includes(i.id))
              this.selected.push(i.id)
          } else if (u == 2) {
            const idx = this.selected.indexOf(i.id);
            this.selected.splice(idx, 1);
          }

          
          // this.showSpinnerLoading();
          // this.getDaywiseConsumerDetails();
          this.getDetails();
        }
  
      }, error => {
  
      })
    }
  
    async confirm() {
      if (!this.editOrder) {
        // if (this.selected.length == 0 && this.sel_additional_item.length == 0) {
        //   this.presentAlert('Please select an item.');
        // } else {
          this.checkStatus();
          
        // }
      }
    }

    async checkStatus(){
      const cid = await this.storage.get('COMPANY_ID');
          const bid = await this.storage.get('BRANCH');
          const uid = await this.storage.get('USER_ID');
          let url = this.config.domain_url + 'dining_order_confirmation_info';
          let headers = await this.config.getHeader()
          let body = {
            consumer_id: this.consumer.user.user_id,
            dining_id: this.menu.id,
            date: moment(this.date).format('YYYY-MM-DD'),
            logged_userid:uid
          }
          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log('confirminfo:', res, body,this.menuItems.map(x=>x.all_coloumns.length==0));
            if(res.data.no_selection){
              this.presentAlert('Please select atleast one item.')
            }else if(res.data.show_alert){
              this.confirmAlert(res.data.stat)
            }else if(res.data.no_items){
              this.noItemAlert();
            }else{
              this.confirmsingledayorder();
            }
            // this.presentAlert('Order placed successfully.')
            
            // setTimeout(() => {
            //   this.back();
            // }, 3000)
          }, error => {
            console.log(error)
          })
    }

    async confirmsingledayorder(){
      const cid = await this.storage.get('COMPANY_ID');
          const bid = await this.storage.get('BRANCH');
          const uid = await this.storage.get('USER_ID');
          let url = this.config.domain_url + 'confirm_single_day_order';
          let headers = await this.config.getHeader();
          let body = {
            consumer_id: this.consumer.user.user_id,
            dining_id: this.menu.id,
            date: moment(this.date).format('YYYY-MM-DD'),
            created_by: uid,
  
          }
          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log('confirm:', res, body);
            // this.presentAlert('Order placed successfully.')
            this.showSuccessMessage()
            // setTimeout(() => {
            //   this.back();
            // }, 3000)
          }, error => {
  
          })
    }
  
    async showItemdetails(item, st, serving_id,menu) {
      if (item.subitem == 1) {
        this.openContinentalDetails(item, serving_id,menu,1,item)
      } else {
        if (!this.availableItem(item) || this.cutoffTime(menu) || menu.is_skipped||item.visibility==0) {
        } else {
          // console.log('getCom:', this.getComment(item, serving_id), this.setMealSize(item, serving_id))
          const modal = await this.modalCntrl.create({
            component: MenuItemDetailsComponent,
            cssClass: 'full-width-modal',
            componentProps: {
              item: item,
              date: this.date,
              serving_time: st,
              menu_id: this.menu.id,
              consumer: this.consumer.user.user_id,
              serving_time_id: serving_id,
              flag:2
              // ml_sz: this.setMealSize(item, serving_id),
              // sel_quantity: this.setQuantity(item, serving_id),
              // comment: this.getComment(item, serving_id),
              // options: this.setOptions(item, serving_id)
            }
          });
          modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned.data) {
              // this.getDaywiseConsumerDetails();
              // this.selected.push(dataReturned.data);
              // this.showSpinnerLoading();
              // this.getDaywiseConsumerDetails();
              this.getDetails();
            }
          });
  
          return await modal.present();
        }
      }
    }



    async showAdditionalItemdetails(item, st, serving_id,menu) {
      if (item.subitem == 1) {
        this.openContinentalDetails(item, serving_id,menu,1,item)
      } else {
        if ( this.cutoffTime(menu) || menu.is_skipped) {
        } else {
          // console.log('getCom:', this.getComment(item, serving_id), this.setMealSize(item, serving_id))
          const modal = await this.modalCntrl.create({
            component: AdditionalItemDetailsComponent,
            cssClass: 'full-width-modal',
            componentProps: {
              item: item,
              date: this.date,
              serving_time: st,
              menu_id: this.menu.id,
              consumer: this.consumer.user.user_id,
              serving_time_id: serving_id,
              
              // ml_sz: this.setMealSize(item, serving_id),
              // sel_quantity: this.setQuantity(item, serving_id),
              // comment: this.getComment(item, serving_id),
              // options: this.setOptions(item, serving_id)
            }
          });
          modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned.data) {
              // this.getDaywiseConsumerDetails();
              const id = this.sel_additional_item.indexOf(dataReturned.data);
              if(id>=0){
                     
                    }else{
              this.sel_additional_item.push(dataReturned.data);
                    }
              // this.showSpinnerLoading();
              // this.getDaywiseConsumerDetails();
              // this.getDetails();
            }
          });
  
          return await modal.present();
        }
      }
    }
  
  
    previous() {
      if (this.currentIdx == 0) {
        console.log('reached first item')
      } else {
        console.log('curr:', this.currentIdx, this.dates[this.currentIdx]);
        this.currentIdx--;
        console.log('changedcur:', this.currentIdx, this.dates[this.currentIdx])
        this.date = this.dates[this.currentIdx];
        // this.getList();
        this.selected=[];
          this.sel_additional_item=[];
        this.showLoading();
        // this.getDaywiseConsumerDetails();
        this.getDetails();
  
        let currentDate = moment().format('DD MMM YYYY');
        if (moment(this.date).isSameOrAfter(moment(currentDate))) {
          this.hide_footer = false
        } else {
          this.hide_footer = true
        }
      }
    }
    next() {
      if (this.currentIdx == (this.dates.length - 1)) {
        console.log('reached end')
      } else {
        this.currentIdx++;
        this.date = this.dates[this.currentIdx];
        // this.getList();
        this.selected=[];
          this.sel_additional_item=[];
        this.showLoading();
        // this.getDaywiseConsumerDetails();
        this.getDetails();
  
        let currentDate = moment().format('DD MMM YYYY');
        if (moment(this.date).isSameOrAfter(moment(currentDate))) {
          this.hide_footer = false
        } else {
          this.hide_footer = true
        }
      }
    }
  
    async showLoading() {
      this.loading = true;
      const loading = await this.loadingCtrl.create({
        cssClass: 'dining-loading',
        // message: 'Please wait...',
        // message: '<ion-img src="assets/loader.gif"></ion-img>',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
  
    async dismissLoader() {
      this.loading = false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }

    // async showSpinnerLoading() {
    //   this.loading = true;
    //   const loading = await this.loadingCtrl.create({
    //     cssClass: 'dining-spinner-loading',
    //     // message: 'Please wait...',
    //     // message: '<ion-img src="assets/loader.gif"></ion-img>',
    //     spinner: 'lines',
    //     // duration: 3000
    //   });
    //   return await loading.present();
    // }
  
    getComment(i, servetime) {
      let c;
      if (this.selected.includes(i.id)) {
        let Idx;
        const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
        if (this.preOrderedItems[idx].pre_item_orders.length) {
          Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
          if (Idx >= 0) {
            c = this.preOrderedItems[idx].pre_item_orders[Idx].comment;
          } else {
            c = null
          }
        }
      }
  
      return c;
    }
  
  
    availableItem(i) {
  
      let avl;
  
      //   if(i.itemdetails.foodtexture&&i.itemdetails.foodtexture.length){
      //  const idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.details.food_texture.id);
      //  const Idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.details.fluid_texture.id);
      
      if (i.itemdetails.texture_array && i.itemdetails.texture_array.length) {
        let idx,Idx=-1;
        if(this.details.food_texture&&this.details.food_texture.id){
        idx = i.itemdetails.texture_array.map(x => x.food_texture_id).indexOf(this.details.food_texture.id);
        }
        if(this.details.fluid_texture&&this.details.fluid_texture.id){
        Idx = i.itemdetails.texture_array.map(x => x.food_texture_id).indexOf(this.details.fluid_texture.id);
        }
        if (idx >= 0 || Idx >= 0) {
          avl = true
        } else {
          avl = false
        }
  
      } else {
        avl = true
      }
  
      return avl;
    }
    incompatable(item, serving_time_id) {
      let inc = false;
      const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(serving_time_id);
      const Idx = this.preOrderedItems[idx].all_coloumns.map(x => x.id).indexOf(item.id);
      if (Idx < 0) {
        inc = true
      }
      return inc;
  
  
    }
  
    async openContinentalDetails(item, serving_id,menu,or,i) {
      console.log('screen:', this.screen_width, this.screen_height);
      let component;
      //  if(this.screen_width>=768){
      //    component=ContinentalItemTabviewComponent
      //  }else{
      component = ContinentalItemComponent;
      //  }
      let fdtexture,fltexture=null;
      if(this.details.food_texture){
        fdtexture=this.details.food_texture.id
      }
      if(this.details.fluid_texture){
        fltexture=this.details.fluid_texture.id
      }
  
      console.log(component)
      const modal = await this.modalCntrl.create({
  
        component: component,
        cssClass: 'full-width-modal',
        componentProps: {
          menu: item,
          date: this.date,
          menu_id: this.menu.id,
          consumer: this.consumer.user.user_id,
          con_name: this.consumer.user.name,
          serving_time_id: serving_id,
          fdtexture: fdtexture,
          fltexture: fltexture,
          preOrderedItems: this.preOrderedItems,
          allergies: this.allergies,
          edit: this.editOrder,
          skipped:menu.is_skipped
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        console.log(dataReturned)
        if (dataReturned.data) {
          // this.showSpinnerLoading();
          // this.getDaywiseConsumerDetails();
          if (or == 2) {
            //   let count = 1;
              i.items.map(x => {
                if (this.selected.includes(x.id)) {
                  var index = this.selected.indexOf(x.id);
                  this.selected.splice(index, 1);
                }
              })
            }
  if(dataReturned.role=='1'){
          this.selected.push(dataReturned.data);
  }else{
    const idx = this.selected.indexOf(dataReturned.data);
    if(idx>=0)
            this.selected.splice(idx, 1);
    
  }
  
        }
        this.getDetails();
      });
  
      return await modal.present();
    }
  
    async edit() {
      this.editOrder = false;
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let headers = await this.config.getHeader();
  
  
  
  
      let url = this.config.domain_url + 'enable_dining_order_via_date';
  
      let body;
      body = {
        consumer_id: this.consumer.user.user_id,
        dining_id: this.menu.id,
        date: moment(this.date).format('YYYY-MM-DD'),
        created_by: uid,
  
      }
  
  
      console.log('body:', body)
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        console.log('edit:', res, body);
  
  
      }, error => {
        console.log('err:', error)
      })
  
    }
  
    // setAdditionalitem(id){
    //   console.log('setadditionalitem:',id)
    //   let item=[];
    //   this.additionalItems.forEach(element => {
    //     element.servingtime.map(x => {
    //       if(id==x.servingtime[0].id){
    //         item.push(element.item)
  
    //         // this.servingTime=x.servingtime[0].servingtime
    //       }
    //     })
  
    //   });
    //   this.additional_by_ser_time.push({item:item,ser_time:id,show:false});
    // }
  
  
    async selectAdditionalItem(i, item, menu) {
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let headers = await this.config.getHeader();
  
  
  
  
      let url = this.config.domain_url + 'presave_additional_ordering_item';
  
      let body;
      body = {
        consumer_id: this.consumer.user.user_id,
        dining_id: this.menu.id,
        date: moment(this.date).format('YYYY-MM-DD'),
        created_by: uid,
        item_id: i.item.id,
        serving_time_id: item.ser_time
      }
  
      if (this.sel_additional_item.includes(i.item.id)) {
        body.uncheck = 1
      }
      console.log('body:', body,this.sel_additional_item,i,item)
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        console.log('presave:', res);
        if (res.status == 'success') {
          if (res.message == 'created') {
            const idx=this.additional_by_ser_time.findIndex(x=>x.ser_time==item.ser_time);
            const Idx=this.additional_by_ser_time[idx].item.findIndex(x=>x.id==i.id);
            console.log('slcadd:',this.additional_by_ser_time[idx],this.additional_by_ser_time[idx].item[Idx],i,item)
            this.additional_by_ser_time[idx].item[Idx].is_ordered=true;
            this.sel_additional_item.push(i.item.id)
          } else {
            const idx=this.additional_by_ser_time.findIndex(x=>x.ser_time==item.ser_time);
            const Idx=this.additional_by_ser_time[idx].item.findIndex(x=>x.id==i.id);
            console.log('slcadd:',this.additional_by_ser_time[idx],this.additional_by_ser_time[idx].item[Idx],i,item)
            this.additional_by_ser_time[idx].item[Idx].is_ordered=false;
            const id = this.sel_additional_item.indexOf(i.item.id);
    if(id>=0)
            this.sel_additional_item.splice(id, 1);
          }
    // this.showSpinnerLoading();
    // this.getDaywiseConsumerDetails();
    // this.getDetails();
    
        }
  
      }, error => {
        console.log('err:', error)
      })
  
    }


async showSuccessMessage(){
  const modal = await this.modalCntrl.create({
    component: OrderSuccessMessageComponent,
    cssClass: 'full-width-modal',
    componentProps: {
      
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    this.back();
  });
  return await modal.present();
}


async getDetails(){
  // this.selected=[];
      this.additional_by_ser_time=[];
      // this.sel_additional_item=[];
      // }
      let url = this.config.domain_url + 'day_ordering_menu_listing';
      console.log(url);
      const cid = await this.storage.get('COMPANY_ID');
      const bid = await this.storage.get('BRANCH');
      const uid = await this.storage.get('USER_ID');
      let headers = await this.config.getHeader();
      let body = {
        company_id: cid,
        consumer_id: this.consumer.user.user_id,
        dining_id: this.menu.id,
        date: moment(this.date).format('yyyy-MM-DD'),
        created_by: uid
      }
      this.http.post(url, body, { headers }).subscribe((res: any) => {

        console.log('newData:',res)
        this.menuItems = res.data.meals_with_items;
        
        // this.additionalItems=res.data.additionalitems;
        // this.dismissLoader();
        this.menuItems.forEach(el => {
          // this.setAdditionalitem(el.serving_time_id);
          this.additional_by_ser_time.push({ item: el.additional_items, ser_time: el.serving_time_id, show: false });
        })
        this.skipped = res.data.skipped_meal_times;
        this.serve_area = res.data.serve_area;
        console.log('res:', res, url);
        if (res.data.is_leave) {
          this.leaveMarked = true;
          this.hide_footer = true;
        } else {
          this.leaveMarked = false;
        }
        if (res.data.is_edit) {
          this.editOrder = true
        } else {
          this.editOrder = false;
        }
        this.preOrderedItems = res.data.meals_with_items;
        // this.selected=[];
        if (this.preOrderedItems.length) {
          this.preOrderedItems.map(x => {
            if (x.pre_item_orders.length) {
              x.pre_item_orders.map(y => {
                if(!this.selected.includes(y.menu_item_id)){
                this.selected.push(y.menu_item_id)
                }
              })
            }
            if (x.additional_items.length) {
              x.additional_items.map(y => {
                if (y.selected_data) {
                  if(!this.sel_additional_item.includes(y.selected_data.item_id)){
                  this.sel_additional_item.push(y.selected_data.item_id);
                  }
                }
              })
            }
            if (x.all_coloumns.length) {
              x.all_coloumns.map(y => {
                if(y.items.length==1){
                if (y.items[0].is_ordered==true) {
                  if(!this.selected.includes(y.items[0].id)){
                  this.selected.push(y.items[0].id)
                  }
                }
              }else{
                y.items.map(z=>{
                  if(z.is_ordered==true){
                    if(!this.selected.includes(z.id)){
                    this.selected.push(z.id)
                    }
                  }
                })
              }
              })
            }
          })
        }
        console.log('sele:',this.selected)
        this.dismissLoader();
      })
}

checkNoItems(menu){
  let no_items=true;
  if(menu.all_coloumns.length==0&&menu.additional_items.length==0){
    no_items=false
  }else if(menu.all_coloumns.length&&menu.additional_items.length==0){
      menu.all_coloumns.map(x=>{
        if(!x.items||!x.items.length){
          no_items=false;
        }
      })
  }else if(menu.additional_items.length){
    no_items=true;
  }else{
    no_items=true
  }
  return no_items;
}
}
