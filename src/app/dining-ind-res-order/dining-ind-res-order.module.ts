import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningIndResOrderPageRoutingModule } from './dining-ind-res-order-routing.module';

import { DiningIndResOrderPage } from './dining-ind-res-order.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningIndResOrderPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningIndResOrderPage]
})
export class DiningIndResOrderPageModule {}
