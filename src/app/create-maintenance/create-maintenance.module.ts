import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateMaintenancePageRoutingModule } from './create-maintenance-routing.module';

import { CreateMaintenancePage } from './create-maintenance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateMaintenancePageRoutingModule
  ],
  declarations: [CreateMaintenancePage]
})
export class CreateMaintenancePageModule {}
