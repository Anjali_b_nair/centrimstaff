import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { LocationsComponent } from '../components/locations/locations.component';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';
import { ResidentComponent } from '../components/resident/resident.component';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-create-maintenance',
  templateUrl: './create-maintenance.page.html',
  styleUrls: ['./create-maintenance.page.scss'],
})
export class CreateMaintenancePage {
  title: any;
  location: any;
  priority: any = "1";
  type: any;
  description: any;
  currentDate: Date;
  due_date: Date;
  image: any = [];
  subscription: Subscription;
  other_type: any;
  risk: any = "0";
  hazard: any = "0";
  out_of_order: any = "0";
  img: any = [];
  imageResponse: any = [];
  keyboardStyle = { width: '100%', height: '0px' };
  typeArray: any = [];
  loc: any;
  parent: any;
  branch: any;
  loading: boolean = false;

  preferred_status:boolean=false;
  entry_permission:boolean=false;
  hazard_status:boolean=false;
  out_of_order_status:boolean=false;
  priority_status:boolean=false;
  photo_status:boolean=false;
  risk_status:boolean=false;
  preferred_date:any;
  permission:any='0';
  resident:any;
  resident_name:any;
  rv_branch:any;
  flag:any;
  cid:any;
  constructor(private http: HttpClient, private storage: Storage, private config: HttpConfigService, private router: Router,
    private platform: Platform, private loadingCtrl: LoadingController, private toast: ToastController,
     private actionsheetCntlr: ActionSheetController,
    private modalCntrl: ModalController, private route: ActivatedRoute) { }

 
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }


  async pickImage() {
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;

    //   this.imageResponse.push(base64Image);
    //   this.uploadImg(base64Image);
    //   // this.img.push(imageData)

    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.uploadImg(base64Image);
  }

  async addImage() {
    // console.log("imagR:", this.imageResponse);
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1,
    //   quality: 100
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    // this.imagePicker.getPictures(options).then((results) => {
    //   for (var i = 0; i < results.length; i++) {
    //     console.log('Image URI: ' + results[i]);



    //     if ((results[i] === 'O') || results[i] === 'K') {
    //       console.log("no img");

    //     } else {
    //       // this.img.push(results[i]);
    //       this.uploadImg('data:image/jpeg;base64,' + results[i])
    //       this.imageResponse.push('data:image/jpeg;base64,' + results[i])
    //     }



    //   }
    // }, (err) => { });

    // console.log("imagg:", this.img);

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImg('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  

  }
  removeImg(i) {
    this.imageResponse.splice(i, 1);
    this.img.splice(i, 1);
  }
  async submit() {

   
      const uid=await this.storage.get('USER_ID')
        const bid=await this.storage.get('BRANCH')
          let branch;
          if (this.branch == undefined || this.branch == 'undefined') {
            branch = bid
          } else {
            branch = this.branch
          }
          this.currentDate = new Date();
          if (this.priority == 2) {
            this.due_date = this.currentDate
          } else if (this.priority == 1) {
            this.due_date = new Date(Date.now() + 2 * 24 * 60 * 60 * 1000);
          } else if (this.priority == 0) {
            this.due_date = new Date(Date.now() + 3 * 24 * 60 * 60 * 1000);
          }
          if (this.description == undefined) {
            this.description = null
          }
          if (this.loc == undefined) {
            this.loc = null
          }
          // let files = this.fileField.getFiles();
          // console.log(files);
          let current = this.currentDate.getFullYear() + '-' + this.fixDigit((this.currentDate.getMonth() + 1)) + '-' + this.fixDigit(this.currentDate.getDate());
          let due = this.due_date.getFullYear() + '-' + this.fixDigit((this.due_date.getMonth() + 1)) + '-' + this.fixDigit(this.due_date.getDate());

          // let formData = new FormData();
          // formData.append('request_title', this.title); // Add any other data you want to send
          // formData.append('location', this.location);
          // formData.append('risk_rating', '1');
          // formData.append('hazard', '1'); // Add any other data you want to send
          // formData.append('priority', this.priority);
          // formData.append('out_of_order', '1');
          // formData.append('status', '0'); // Add any other data you want to send
          // formData.append('due_date', due);
          // formData.append('request_date', current);
          // formData.append('type', this.type); // Add any other data you want to send
          // formData.append('priority', this.priority);
          // formData.append('other_type', null);
          // formData.append('description', this.description);
          // formData.append('created_by', uid); // Add any other data you want to send
          // formData.append('user_id', uid);
          // formData.append('branch_id', bid);
          // files.forEach((file) => {
          //   formData.append('images[]', file.rawFile);
          // });

          if (this.type != 0) {
            this.other_type = null
          }
          if (/^\d+$/.test(this.title) || this.title == undefined || this.title == '') {
            this.presentAlert('Please enter a proper request title.');
            // }else if(/^\d+$/.test(this.location)|| this.location==undefined || this.location==''){
            //   this.presentAlert('Please enter proper location.');
          } else if (this.type == undefined) {
            this.presentAlert('Please select the type.');
          } else {
            this.showLoading();

            // let body={
            //   request_title:this.title,
            //   location:this.location,
            //   risk_rating:this.risk,
            //   hazard:this.hazard,
            //   priority:this.priority,
            //   out_of_order:this.out_of_order,
            //   status:0,
            //   due_date:due,
            //   request_date:current,
            //   type:this.type,
            //   other_type:this.other_type,
            //   description:this.description,
            //   created_by:uid,
            //   user_id:uid,
            //   branch_id:bid,
            //   images:this.img
            // }

            let body;
            body = {
              request_title: this.title,
              location: this.loc,
              location_id: null,
              risk_rating: this.risk,
              hazard: this.hazard,
              priority: this.priority,
              out_of_order: this.out_of_order,
              status: 0,
              due_date: due,
              request_date: current,
              type: this.type,
              other_type: null,
              description: this.description,
              created_by: uid,
              user_id: uid,
              branch_id: branch,
              images: this.img,
              category: 0,
              assets: null,
              recurring: 0,
              repeat: null,
              endon: null,
              enddate: null,
              endafteroccu: null,
              daily: null,
              weekly: null,
              weekcount: null,
              dayof: null,
              monthly: null,
              monthofyear: null,
              technician: null
            }
            if(this.preferred_status){
              body.preferred_datetime=momenttz(this.preferred_date).format('YYYY-MM-DD HH:mm:ss')
            }
            if(this.entry_permission){
              body.permission_to_enter=this.permission
            }

            if(this.rv_branch==1&&this.resident){
              body.created_for=this.resident
            }
            console.log("body:", body)
            let url = this.config.domain_url + 'store_maintenance_request';
            let headers=await this.config.getHeader();;
            this.http.post(url, body,{headers}).subscribe((res: any) => {
              console.log(res);
              this.presentAlert('Your maintenance request has been placed.');
              this.dismissLoader();
              this.back();

            }, error => {
              this.dismissLoader();
              this.presentAlert('Something went wrong.Please try again.');
              console.log(error);

            })
          }
       

  }

  fixDigit(val) {
    return val.toString().length === 1 ? "0" + val : val;
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }


  back() {
    // this.title='';
    // this.location='';
    // this.priority="1";

    // this.description='';

    // this.image=[];

    // this.other_type='';
    // this.img=[];
    // this.imageResponse=[];
    if(this.flag==1){
      this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
    }else{
    this.router.navigate(['/maintenance'], { replaceUrl: true })
    }
  }

  async viewImage() {
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
      cssClass: 'maintenance-image-modal',
      componentProps: {
        data: this.imageResponse,


      }
    });
    modal.onDidDismiss().then((dataReturned) => {

    });
    return await modal.present();
  }

  async ionViewWillEnter() {

    this.typeArray = JSON.parse(this.route.snapshot.paramMap.get('type'));
    this.typeArray.sort((a,b) => a.type > b.type ? 1 : -1)
    this.branch = this.route.snapshot.paramMap.get('branch');
    this.flag = this.route.snapshot.paramMap.get('flag');
   
    if(this.flag==1){
      this.cid=this.route.snapshot.paramMap.get('cid');
      this.resident=this.route.snapshot.paramMap.get('uid');
      this.resident_name=this.route.snapshot.paramMap.get('name');
      this.getResidentPropertyLocation();
    }
    // this.typeArray.push({ id: 0, type: 'Other' });
    console.log("type:", this.typeArray);
    const rv=await this.storage.get('RVSETTINGS');
  this.rv_branch=rv
    this.getPublicSettings();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {

      this.back();


    });

  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async openLocation() {

    const popover = await this.modalCntrl.create({
      component: LocationsComponent,

      componentProps: {
        data: this.loc,
        branch: this.branch
      },
      cssClass: 'assign-location-modal'
    });

    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data != undefined) {
        this.loc = dataReturned.data.id;
        this.location = dataReturned.data.loc_name;
        this.parent = dataReturned.data.parent
      }
      console.log("data:", dataReturned);

    });
    return await popover.present();
  }

  removeLoc() {

    this.loc = undefined;
    this.location = undefined;
    this.parent = '';

  }
  async uploadImg(file) {
    let url = this.config.domain_url + 'upload_file';
    let headers=await this.config.getHeader();;
    let body = { file: file }
    console.log("body:", body);
    if (!this.loading) {
      this.showLoading();
      this.loading = true
    }
    this.http.post(url, body,{headers}).subscribe((res: any) => {

      // this.img.push(res.data);
      this.img.push(res.data);
      this.dismissLoader();
      this.loading = false
      console.log("uploaded:", res);
    }, error => {
      console.log(error);
      this.dismissLoader();
      this.loading = false

    })
  }


  async getPublicSettings(){
   
      const type=await this.storage.get('USER_TYPE');
       
        const bid=await this.storage.get('BRANCH');
        
            const tz=await this.storage.get('TIMEZONE');
            // let branch;
            // if (type == 1 || type == 4 || type == 9) {
            //   branch = this.branch
            // } else {
            //   branch = bid
            // }
            let headers=await this.config.getHeader();
          let url=this.config.domain_url+'maintenance_public_form_settings/'+bid
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log('pub:',res,url,tz,momenttz.tz(tz).format());
            if(res.data&&res.data.date_time==1){
              this.preferred_status=true;
              this.preferred_date=momenttz.tz(tz).format();
            }else{
              this.preferred_status=false
            }
            if(res.data&&res.data.entry_permission==1){
              this.entry_permission=true
            }else{
              this.entry_permission=false
            }
            if(res.data&&res.data.hazard==1){
              this.hazard_status=true
            }else{
              this.hazard_status=false
            }
            if(res.data&&res.data.out_of_order==1){
              this.out_of_order_status=true
            }else{
              this.out_of_order_status=false
            }
            if(res.data&&res.data.priority==1){
              this.priority_status=true
            }else{
              this.priority_status=false
            }
            if(res.data&&res.data.photos==1){
              this.photo_status=true
            }else{
              this.photo_status=false
            }
            if(res.data&&res.data.risk_rating==1){
              this.risk_status=true
            }else{
              this.risk_status=false
            }
          })
          
       
  }

  removeResident(){
    this.resident=null;
    this.resident_name=null;
  }

  async openResident() {
    console.log('br:',this.branch)
    if(this.flag!=='1'){

    const popover = await this.modalCntrl.create({
      component: ResidentComponent,

      componentProps: {
        data: this.resident,
        rv:this.rv_branch,
        bid:this.branch,
        name:this.resident_name,
        flag:1
      
      },
      cssClass:'full-width-modal'
    });

    popover.onDidDismiss().then((dataReturned) => {
      console.log('res:',dataReturned);
      if (dataReturned.data != undefined) {
        
        this.resident = dataReturned.data.data;
        this.resident_name = dataReturned.data.name;
        this.getResidentPropertyLocation();
      }else{
        if(dataReturned.role!='backdrop'){
        this.resident = undefined;
        this.resident_name = undefined;
        }
      }
      console.log("data:", dataReturned);

    });
    return await popover.present();
  }
  }

  async getResidentPropertyLocation(){
    let headers=await this.config.getHeader();
    
      let url = this.config.domain_url + 'get_resident_location/'+this.resident;
      
      
      this.http.get(url,{headers}).subscribe((res: any) => {
        console.log('resloc:',res);
        if(res.data){
        this.location=res.data.name;
        this.loc=res.data.id
        }
      }, error => {
        console.log(error);
  
      })
    
  }
}
