import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateMaintenancePage } from './create-maintenance.page';

const routes: Routes = [
  {
    path: '',
    component: CreateMaintenancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateMaintenancePageRoutingModule {}
