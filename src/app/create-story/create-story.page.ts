import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { File } from '@ionic-native/file/ngx';
import { CreateThumbnailOptions, VideoEditor } from '@ionic-native/video-editor/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { TaggedUsersComponent } from '../components/tagged-users/tagged-users.component';
import { VisibletoComponent } from '../components/visibleto/visibleto.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { HTTP } from '@ionic-native/http/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { ConsentTagAlertComponent } from '../components/consent-tag-alert/consent-tag-alert.component';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { DOCUMENT } from '@angular/common';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { FilePath } from '@ionic-native/file-path/ngx';
import { SendMailWarningComponent } from '../components/send-mail-warning/send-mail-warning.component';
declare var $: any;
@Component({
  selector: 'app-create-story',
  templateUrl: './create-story.page.html',
  styleUrls: ['./create-story.page.scss'],
})
export class CreateStoryPage implements OnInit {
type:any=1;
title:any;
description:any;
status:any=1;
minDate: string = new Date().toISOString();
date:any;
categories:any=[];
category:any;
branch:any=[];
facility:any=[];
isUrl:boolean=false;
isImg:boolean=false;
isVdo:boolean=false;
isDoc:boolean=false;
// img:any=[];
imageResponse:any=[];
// vdo:any=[];
vdoResponse:any=[];
docs:any=[];
docResponse:any=[];
media:any=[];
vdo:any[]=[];
vdoThumb:any=[];
urlThumb:any=[];
hasAttach:boolean=false;
vdoUrl:any;
MAX_FILE_SIZE = 24 * 1024 * 1024;
base64File:any;
flag:any;
id:any;
user_id:any;
name:any;
pic:any;
attendees:any[]=[];
tags:any[]=[];
subscription:Subscription;
visible_type:any=1;
stat:any=0;
storyType=[{id:1,type:'Story'},{id:2,type:'Announcement'}];
isLoading:boolean=false;
from:any;
disable:boolean=false;

listener;
selectAllCheckBox: any;
checkBoxes: HTMLCollection;

customAlertOptions: any = {
  
  // header: 'Facility',
  // subHeader: 'Select All:',
  message: '<ion-checkbox id="selectAllCheckBox"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
utype:any;
act_id:any;
hide_comments:any=0;
publish_status:boolean=false;
send_email:any=0;
public customOptions: any = {
  header: "Categories"
};
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private file:File,private alertCntlr:AlertController,
    private editor:VideoEditor,private route:ActivatedRoute,private platform:Platform,private router:Router,
    private modalCntrl:ModalController,private toastCntlr:ToastController,private https:HTTP,
    private actionsheetCntlr:ActionSheetController,private mediaCapture: MediaCapture,
    private chooser:Chooser,private loadingCtrl:LoadingController,private filePath: FilePath,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) { }

  ngOnInit() {
    $('#summernote1').summernote({
      placeholder: 'Enter text here',
      tabsize: 2,
      height: 120,
      toolbar: [
        ['font', ['bold', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
      ],
      
    });
 
  }

async ionViewWillEnter(){
  this.ngOnInit();
  this.disable=false;
  this.date=new Date(new Date().getTime() - new Date().getTimezoneOffset()*60000).toISOString();
  // const tabBar = document.getElementById('myTabBar');
  //   tabBar.style.display="none";
  this.categories=[];
  this.branch=[];
  this.imageResponse=[];
  // this.img=[];
  // this.vdo=[];
  this.type=1;
  this.docs=[];
  this.vdoResponse=[];
  this.docResponse=[];
  this.media=[];
  this.vdoThumb=[];
  this.urlThumb=[];
  this.vdo=[];
  this.attendees=[];
  this.tags=[];

  this.flag=this.route.snapshot.paramMap.get('flag');
  this.id=this.route.snapshot.paramMap.get('user_id');
  this.user_id=this.route.snapshot.paramMap.get('id');
  this.name=this.route.snapshot.paramMap.get('name');
  this.pic=this.route.snapshot.paramMap.get('pic');
  if(this.flag==2){
    this.visible_type=4;
    this.attendees.push(parseInt(this.id));
    this.tags.push({id:parseInt(this.id),profile_pic:this.pic,name:this.name});
    this.from=this.route.snapshot.paramMap.get('from');
  }else if(this.flag==5){
    this.act_id=this.route.snapshot.paramMap.get('act_id');
    this.visible_type=4;
    this.title=this.route.snapshot.paramMap.get('title');
    this.description=this.route.snapshot.paramMap.get('desc');
    if(this.description==null || this.description=='<p><br></p>'||this.description=='null'){
      console.log("descif:",this.description);
      
    }else{
    $('#summernote1').summernote('code',this.description);
    }
    let img=JSON.parse(this.route.snapshot.paramMap.get('img'));
    let doc=JSON.parse(this.route.snapshot.paramMap.get('doc'));
    this.attendees=JSON.parse(this.route.snapshot.paramMap.get('tagged'));
    
    if(img&&img.length){
      this.isImg=true;
      this.hasAttach=true;
      img.forEach(ele => {
        this.imageResponse.push(ele.activity_image)
      });
    }
    if(doc&&doc.length){
      this.isDoc=true;
      this.hasAttach=true;
      doc.forEach(ele => {
        this.docs.push(ele.post_attachment);
        this.docResponse.push({name:ele.title})
      });
    }
    if(this.attendees&&this.attendees.length){
      this.attendees.forEach(el=>{
        this.tags.push({id:parseInt(el)});
      })
    }

  }
  
    const bid=await this.storage.get("BRANCH")

      const data=await this.storage.get("USER_TYPE")

        this.utype=data;
  let cat_url=this.config.domain_url+'activity_categories';
  let headers=await this.config.getHeader();;
  this.http.get(cat_url,{headers}).subscribe((res:any)=>{
    console.log("cat:",res);
    
    res.data.forEach(element => {
      this.categories.push({id:element.id,category_name:element.category_name})
    });
    console.log("cat1:",this.categories);
    
  },error=>{
    console.log(error);
    
  })
  // this.storage.ready().then(()=>{
    // const data=await this.storage.get("USER_TYPE")

      const cid=await this.storage.get("COMPANY_ID")


      // this.branch.push({id:0,name:'Select All'})
        // this.user_type=data;
        let url=this.config.domain_url+'branch_viatype';
        let body={
          company_id:cid,
          role_id:data
        }
        if(data=='1'){
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          if(data==1){
            // this.branch.push({id:0,name:'Select All'});
            }
          res.data.forEach(element => {
            this.branch.push({id:element.id,name:element.name});
          });
          if(this.facility.length==0){
            // this.facility=this.branch[0].id.toString();
            this.facility=bid.toString();
            }
        },err=>{
          console.log(err);
          
        })
      }else{
        let headers=await this.config.getHeader();;
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
        res.data.forEach(element => {
          this.branch.push({id:element.id,name:element.name});
        });
        // if(data!='1'){
          if(this.facility.length==0){
          // this.facility=this.branch[0].id.toString();
          this.facility=bid.toString();
          }
          
        // }

        console.log("ffff:",this.facility,this.branch)
      },err=>{
        console.log(err);
        
      })
      }
      if(this.flag==5){
        this.facility=JSON.parse(this.route.snapshot.paramMap.get('fac'));
      }

      (await this.config.getUserPermission()).subscribe((res: any) => {
        console.log('permissions:', res);
        let routes = res.user_routes;
        if (res.main_permissions.life_style==1&&routes.includes('story.publish')) {
  
          this.publish_status = true
        } else {
  
          this.publish_status = false;
          this.status=2
        }
      },err=>{
        console.log(err);
        
      })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
  //   if(this.flag==1){
  //   this.router.navigate(['/stories'],{replaceUrl: true}) ;
  // }else if(this.flag==2){
  //   this.router.navigate(['/resident-profile',{id:this.user_id,flag:this.from}],{replaceUrl: true})
  // }else if(this.flag==5){
  //   this.router.navigate(['/activity-details',{act_id:this.act_id}],{replaceUrl:true});
  // }else{
  //   this.router.navigate(['/menu'],{replaceUrl: true});
  // }
  // this.clearData();
  // this.dismissLoader();
  this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // $('#summernote1').summernote('destroy');
    }


back(){
  console.log('click is working:',this.flag)
 
  // if(this.flag==1){
   
  //   this.router.navigateByUrl('stories',{replaceUrl:true})
  // }else if(this.flag==2){
  //   this.router.navigateByUrl('resident-profile;id='+this.user_id+';flag='+this.from,{replaceUrl: true})
  // }else if(this.flag==5){
  //   this.router.navigateByUrl('activity-details;act_id='+this.act_id,{replaceUrl:true});
  // }else{
  //   this.router.navigateByUrl('menu',{replaceUrl: true});
  // }

  if(this.flag==1){
    this.router.navigate(['/stories'],{replaceUrl: true}) ;
    }else if(this.flag==2){
      this.router.navigate(['/resident-profile',{id:this.user_id,flag:this.from}],{replaceUrl: true})
    }else if(this.flag==5){
      this.router.navigate(['/activity-details',{act_id:this.act_id}],{replaceUrl:true});
    }else{
      this.router.navigate(['/menu'],{replaceUrl: true});
    }
  
  
 if(this.isLoading){
  this.dismissLoader();
 }
}

  setType(t){
    this.type=t;
    console.log("type:",this.type);
  }
  select(event){
    if(event.currentTarget.checked==true){
      this.status=1
    }else{
      this.status=0
    }
    console.log("notify",event.currentTarget.checked);
  }

  hideComment(event){
    if(event.currentTarget.checked==true){
      this.hide_comments=1
    }else{
      this.hide_comments=0
    }
   
  }


videoUrl(){
  this.hasAttach=true;
  this.isUrl=true;
  // this.isImg=false;
  //   this.isVdo=false;
  //   this.isDoc=false;
}

  async publish(){
    this.disable=true;
    
    
    if(this.vdoUrl==undefined || this.vdoUrl==""){
      this.vdoUrl=null
    }else{

    
      let img;
    if((this.vdoUrl).includes('www.youtube.com')){
      // console.log("you",i.url)
      let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('=') + 1);
      img='http://img.youtube.com/vi/'+id+'/hqdefault.jpg';
      this.urlThumb.push(img)
    }else if((this.vdoUrl).includes('youtu.be')){
      
      let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('/') + 1);
      img='http://img.youtube.com/vi/'+id+'/hqdefault.jpg';
      this.urlThumb.push(img)
    } else if((this.vdoUrl).includes('vimeo.com')){

      let id=(this.vdoUrl).substr(this.vdoUrl.lastIndexOf('/') + 1);
       
        
        this.https.get('https://vimeo.com/api/oembed.json?url=https://player.vimeo.com/video/'+id,{},{'Content-Type': 'application/json','Accept':'application/vnd.vimeo.*+json;version=3.4'})
          .then(res=>{console.log('vimeo:',res);
         img=(JSON.parse(res.data)).thumbnail_url
         console.log("thumbnail:",img);
         this.urlThumb.push(img);
    })
  }
}
    
    let attendee_id;
    if(this.attendees.length>0){
    let a=this.attendees[0];
    this.attendees.forEach(ele=>{
      if(ele==a){
      attendee_id=ele;
      }else{
        attendee_id=attendee_id+','+ele
      }
    })
    }else{
      attendee_id=null
    }
  let tags=[];
  this.tags.forEach(ele=>{
    tags.push(ele.id);
  })
  this.vdo.forEach(ele=>{
    if(this.media.includes(ele)){

    }else{
    this.media.push(ele)
    }
  })
  let urlthumb;
    if(this.urlThumb.length>0){
    let t=this.urlThumb[0];
    this.urlThumb.forEach(ele=>{
      if(ele==t){
      urlthumb=ele;
      }else{
        urlthumb=urlthumb+','+ele
      }
    })
    }else{
      urlthumb=null
    }
    let vdothumb=[];
    // if(this.vdoThumb.length>0){
    //   vdothumb=this.vdoThumb;
 
    // }else{
    //   vdothumb=null
    // }


    this.media.forEach((element,i)=> {
      let ext=element.substr(element.lastIndexOf('.')+1);
      console.log('ext:',ext);
      if(this.vdoThumb.length>0){
        if(ext=='mp4'||ext=='avi'||ext=='mkv'||ext=='mov'){
          this.vdoThumb.forEach((ele) => {
            // console.log("vdo[i]:",vdothumb[i]);
            
            
            if(vdothumb.includes(ele)){
                console.log("include ele:",ele);
                
            }else if(vdothumb[i]!=undefined){
                console.log("index already populated");
                
            }else{
            vdothumb[i]=ele;
            }
          });
        }
      }
    });
      
      
      
      
        
          
    if(vdothumb.length==0){
      vdothumb=null
    }


    let fac=[];
    if(Array.isArray(this.facility)){
      fac=this.facility;

    }else{
      fac.push(this.facility)
    }
    
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID")

          const tz=await this.storage.get('TIMEZONE')
          let url=this.config.domain_url+'story';
          let headers=await this.config.getHeader();;
         
          let body={
            story:{
              post_type:this.type,
              title:this.title,
              description:$('#summernote1').summernote('code'),
              company_id:cid,
              activate_date:moment.utc(moment.tz(tz)).format('yyyy-MM-DD HH:mm:ss'),
              status:this.status,
              visible_type:this.visible_type,
              created_by:uid,
              web_display:1,
              enable_notification:1,
              
            },
            categories:this.category,
            branches:fac,
            document:this.docs,
            tags:tags,
            attendees:attendee_id,
            url:this.vdoUrl,
            images:this.media,
            // videos:this.vdo,
            video_thumbnail:vdothumb,
            url_thumbnail:urlthumb,
            hide_comment:this.hide_comments,
            send_email:this.send_email
          }
          console.log("body:",body);
          
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);

          if(res.status=='success'){
            this.presentAlert("Story created successfully.");
            this.router.navigate(['/stories'],{replaceUrl: true});
            this.clearData();
            this.disable=false;
          }else{
            this.presentAlert("Something went wrong. Please try again later.");
            this.disable=false;
          }
          
        },error=>{
          console.log(error)
          this.presentAlert("Something went wrong. Please try again later.");
          this.disable=false;
        })
      


    
  }



  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }


  async pickImage(){
    this.hasAttach=true;
    this.isImg=true;
   


    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
  }


  async captureImage(){
    this.hasAttach=true;
    this.isImg=true;
   


    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
   
    var imageUrl = image.base64String;
  
    
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.uploadImage(base64Image);

  }



  async uploadImage(img){
    if(this.isLoading==false){
      this.showLoading();
      }
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
     
      this.media.push(res.data);
      this.dismissLoader();
      
    })
  }

  selectBranch(ev,facility){
      console.log("branchselect:",facility);
      if(facility.includes('0')){
        this.facility=[];
        
        this.branch.forEach(element => {
          if(this.facility.includes(element.id.toString())){

          }else{
            if((element.id.toString())!='0'){

           
          this.facility.push(element.id.toString());
            }
          }

        });
        
        console.log("fac:",this.facility);
        
        
      }
      
  }

  async pickVdo(){
    let headers=await this.config.getHeader();;
    this.chooser.getFile()
    .then(async file => {
      console.log(file ? file : 'canceled');
      if(file){
      if(file.mediaType=="video/x-flv" || file.mediaType=="video/mp4" || file.mediaType=="application/x-mpegURL"
      || file.mediaType=="video/MP2T" || file.mediaType=="video/3gpp"
      || file.mediaType=="video/quicktime"|| file.mediaType=="video/x-msvideo"|| file.mediaType=="video/x-ms-wmv"){
        
      
        let uri;
        if(this.platform.is('android')){
          this.filePath.resolveNativePath(file.uri)
          .then(filePath => {console.log('filePath:',filePath);
          uri=filePath;
          this.createThumbnail(uri)})
          .catch(err => console.log('err:',err));
        }else{
          uri=file.uri
          this.createThumbnail(uri)
        }
          
          
        
       
        this.showLoading();
        let fileName ;
        let name=file.name.split('.',1)
        if(name.toString().length>3){
          fileName=name.toString().substring(0,3)+'...'+file.name.substring(file.name.lastIndexOf('.')+1);
        }
        this.vdoResponse.push({name:fileName});
        
    this.hasAttach=true;
    // this.isImg=false;
    this.isVdo=true;
        let  url = this.config.domain_url+'upload_file';
        
        let body={file:file.dataURI}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
         //  that.vdo.push(res.data);
          this.vdo.push(res.data);
          this.dismissLoader();
        },error=>{
          console.log(error);
          this.dismissLoader();
          
        })
       console.log('vdo:',this.vdoResponse,this.vdo);
       
      }else{
        this.alert('File format not supported');
      }
    }
    })
    .catch((error: any) => console.error(error));
  

   
  }
 
  captureVdo(){
    console.log('recording vdo')
 
    let options: CaptureVideoOptions = { limit: 1 }
    this.mediaCapture.captureVideo(options)
      .then(
        (data: MediaFile[]) =>{ console.log("capturedVdo:",data)
        let fileUrl=data[0].fullPath;
        this.file.resolveLocalFilesystemUrl(fileUrl).then(fileEntry => {
         
          fileEntry.getMetadata((metadata) => {
              console.log("sizee:",metadata.size);//metadata.size is the size in bytes
             if(metadata.size>this.MAX_FILE_SIZE){
               this.alert('File is larger than 24MB');
               console.log('large size');
               
             }
             else{
              let ext;
              if((fileUrl).includes('.mp4')){
                ext='mp4'
              }else if((fileUrl).includes('.mkv')){
                ext='mkv'
              }else if((fileUrl).includes('.mov')){
                ext='mov'
              }else{
                ext='avi'
              }
              
                console.log("vdo:",fileUrl);
                
              // this.createThumbnail(fileUrl)
              this.uploadVdo(fileUrl,ext);
              this.createThumbnail(fileUrl)
             }
          })
         
      })
    },
        (err: CaptureError) => console.error(err)
      );

  }


  async uploadVdo(file,ext){
    let headers=await this.config.getHeader();;
    this.showLoading();
    let fileName =  file.substring(file.lastIndexOf('/')+1);
    let name=fileName.split('.',1)
    if(name.toString().length>3){
      fileName=name.toString().substr(0,3)+'...'+ext;
    }
    console.log("name:",name.toString(),"fileNma:",fileName);
    
    this.vdoResponse.push({name:fileName});
    console.log('respons:',this.vdoResponse)
    this.hasAttach=true;
    // this.isImg=false;
    this.isVdo=true;
    let path =  file.substring(0,  file.lastIndexOf("/") + 1);
    //path=path.replace('Users/','');
    console.log("path:",path);
     console.log('name:',fileName);
     const contents = await Filesystem.readFile({
      path: file
    });
    let  url = this.config.domain_url+'upload_file';
    let body={file:'data:video/'+ext+';base64,'+contents.data}
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
     //  that.vdo.push(res.data);
      this.vdo.push(res.data);
      this.dismissLoader();
    },error=>{
      console.log(error);
      this.dismissLoader();
      
    })
   
  }





selectDoc(){
  this.chooser.getFile()
.then(file => {
  console.log(file ? file : 'canceled');
  if(file){
    console.log('type:',file.mediaType);
    
    if(file.mediaType=="application/pdf" || file.mediaType=="application/msword" || file.mediaType=="application/doc"
    || file.mediaType=="application/ms-doc" || file.mediaType=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    || file.mediaType=="application/excel"|| file.mediaType=="application/vnd.ms-excel"|| file.mediaType=="application/x-excel"
    || file.mediaType=="application/x-msexcel"|| file.mediaType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
    this.uploadDoc(file.dataURI,file.name);
    
  }else{
    this.alert('File format not supported');
  }
}
})
.catch((error: any) => console.error(error));
}


async uploadDoc(file,fname){
  let headers=await this.config.getHeader();;
this.showLoading();
  let fileName ;
  let name=fname.split('.',1)
  if(name.toString().length>3){
    fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
  }
  this.docResponse.push({name:fileName});
  this.hasAttach=true;
 
      this.isDoc=true;
  
         let  url = this.config.domain_url+'upload_file';
         let body={file:file}
       
         this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log("doc up:",res);
           this.docs.push(res.data);
           this.dismissLoader();
         },error=>{
           console.log(error);
           
         })
         
 





}


createThumbnail(file){
 
console.log('createthumbnail:',file);

  var option:CreateThumbnailOptions = {
          fileUri:file, 
          atTime:1, 
          outputFileName: 'sample', 
          quality:100 };
    this.editor.createThumbnail(option).then(result=>{
      console.log("thumb:",result);
      this.uploadThumb('file://'+result);
        //result-path of thumbnail
      //  localStorage.setItem('videoNum',numstr.toString());          
    }).catch(e=>{
      console.log('thumberror;',e);
      
     // alert('fail video editor');
    });
  

}


async uploadThumb(file){
  let headers=await this.config.getHeader();;
  let fileName =  file.substring(file.lastIndexOf('/')+1);
  // this.vdoResponse.push({name:fileName});
  let path =  file.substring(0,  file.lastIndexOf("/") + 1);
  //path=path.replace('Users/','');
  console.log("path:",path);
   console.log('name:',fileName);

   const contents = await Filesystem.readFile({
    path: file
  });

  let  url = this.config.domain_url+'upload_file';
         let body={file:'data:image/jpg;base64,'+contents.data}
        // console.log("body:",img);

  
         // let headers=await this.config.getHeader();
         this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log(res);
          //  that.vdo.push(res.data);
           this.vdoThumb.push(res.data);
         },error=>{
           console.log(error);
           
         })

  
}

async selectImage() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.pickImage();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.captureImage();
        // this.pickImage(this.camera.PictureSourceType.CAMERA);
      }
    },
    {
      text: 'Cancel',
      // role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
}


async selectVdo() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.pickVdo();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.captureVdo();
      }
    },
    {
      text: 'Cancel',
      // role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
}


  async alert(mes){
    // let message;
    // if(type==2){
    //   message='File format not supported'
    // }
    const alert = await this.alertCntlr.create({
      mode:'ios',
      message: mes,
      backdropDismiss:true
    });
  
    await alert.present();
  }

  async taggedConsumer(){
    let tags=[];
    if(this.tags.length>0){
  this.tags.forEach(ele=>{
    tags.push(ele.id);
  })
}
    const modal = await this.modalCntrl.create({
      component: TaggedUsersComponent,
     cssClass:'full-width-modal',
      componentProps: {
        data:tags
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data == null || dataReturned.data==undefined) {
        
      }else{
        console.log("dataret:",dataReturned);
        
        this.tags=dataReturned.data;
        
        if(this.tags.length>0){
          this.visible_type=4
          this.tags.forEach(ele=>{
            if(this.attendees.indexOf(ele.id)<0){
            this.attendees.push(ele.id);
            }
          })
        }
      }
    });
    return await modal.present();
  }
  async visibleto(){
   
    const modal = await this.modalCntrl.create({
      component: VisibletoComponent,
      cssClass:'full-width-modal',
      componentProps: {
       data:this.attendees,
       type:this.visible_type,
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data == null || dataReturned.data==undefined) {

      }else{
        console.log("dataret:",dataReturned);
        
        
        this.attendees=dataReturned.data;
        this.visible_type=dataReturned.role;
      }
    });
    return await modal.present();
  }

  removeImg(i){
    this.imageResponse.splice(i,1);
    this.media.splice(i,1);
  }

  removeVdo(i){
    this.vdoResponse.splice(i,1);
    this.vdo.splice(i,1);
    this.vdoThumb.splice(i,1);
  }
  removeDoc(i){
    this.docResponse.splice(i,1);
    this.docs.splice(i,1);
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  clearData(){
    this.imageResponse=[];
            this.type=1;
            this.docs=[];
            this.vdoResponse=[];
            this.docResponse=[];
            this.media=[];
            this.vdoThumb=[];
            this.urlThumb=[];
            this.vdo=[];
            this.attendees=[];
            this.tags=[];
            this.isUrl=false;
            this.isImg=false;
            this.isVdo=false;
            this.isDoc=false;
            this.type=1;
            this.category=''
            this.visible_type=1;
            this.vdoUrl='';
            this.title='';
            $('#summernote1').summernote('code','');
            this.status=1;
  }


  confirm(i){
    this.disable=true;
    console.log("fac:",this.facility,this.category);
    let desc=$('#summernote1').summernote('code');
    let no_consent=[];
    if(this.tags.length>0){
      this.tags.forEach(ele=>{
        if(ele.consent==0){
          no_consent.push(ele.name);
        }
      })
    }
    if(this.title==undefined||this.title==''){
      this.presentAlert('Please enter the title.');
      this.disable=false;
    }else if(Array.isArray(this.facility)&&this.facility.length==0){
      this.presentAlert('Please select atleast one facility.');
      this.disable=false;
    }else if(this.vdoUrl&&!this.isValidUrl(this.vdoUrl)){
      this.presentAlert('Please enter a valid url');
      this.disable=false
    }
    // else if(desc==undefined||desc==''||desc=='<p><br></p>'){
    //   this.presentAlert('Please enter the description.');
    // }else if(no_consent.length>0){
    //   this.showAlert(no_consent);
    else{
      if(i==1){
      this.sendMailWarning()
      }else{
        this.send_email=0;
        this.publish()
      }
      // this.publish();
    }
  }

  async showAlert(item){
    const modal = await this.modalCntrl.create({
      component: ConsentTagAlertComponent,
     
      componentProps: {
       data:item,
      
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data == null || dataReturned.data==undefined) {
    
      }else{
        this.disable=true;
        this.sendMailWarning();
        // this.publish();
      }
    });
    return await modal.present();
  }



  async presentActionSheet() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: 'Select',
      buttons: [{
        text: 'Image',
        // role: 'destructive',
        icon: 'image-outline',
        handler: () => {
          this.selectImage();
          console.log('Delete clicked');
        }
      }, {
        text: 'Video',
        icon: 'film-outline',
        handler: () => {
          this.selectVdo();
          console.log('Share clicked');
        }
      }, {
        text: 'Video url',
        icon: 'link-outline',
        handler: () => {
          this.videoUrl();
          console.log('Play clicked');
        }
      }, {
        text: 'Document',
        icon: 'document-outline',
        handler: () => {
          this.selectDoc();
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        // role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
     
      componentProps: {
       data:this.imageResponse,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }



  openSelector(selector) {
    selector.open().then((alert)=>{
      this.selectAllCheckBox = this.document.getElementById("selectAllCheckBox");
     this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
      this.listener = this.renderer.listen(this.selectAllCheckBox, 'click', () => {
          if (this.selectAllCheckBox.checked) {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="false") {
                (checkbox as HTMLButtonElement).click();
                this.facility=[];
              };
            };
          } else {
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="true") {
                (checkbox as HTMLButtonElement).click();
                this.branch.forEach(element => {
                  if(this.facility.includes(element.id.toString())){
        
                  }else{
                    if((element.id.toString())!='0'){
        
                   
                  this.facility.push(element.id.toString());
                    }
                  }
        
                });
              };
            };
          }
      });
      alert.onWillDismiss().then(()=>{
        this.listener();
        console.log('fac:',this.facility)
      });
    })
    
  }

  async sendMailWarning(){
  const modal = await this.modalCntrl.create({
    component: SendMailWarningComponent,
   cssClass:'full-width-modal',
    componentProps: {
      
      
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data == 1) {
      this.send_email=1
    }else{
      this.send_email=0
    }
    this.publish();
  });
  return await modal.present();
}

isValidUrl(string) {
  
  const pattern = new RegExp(
    '^([a-zA-Z]+:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR IP (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', // fragment locator
    'i'
  );
  return pattern.test(string);
}
}
