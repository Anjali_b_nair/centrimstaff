import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateStoryPageRoutingModule } from './create-story-routing.module';

import { CreateStoryPage } from './create-story.page';

import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateStoryPageRoutingModule,
    ApplicationPipesModule
  ],
  
  declarations: [CreateStoryPage]
})
export class CreateStoryPageModule {}
