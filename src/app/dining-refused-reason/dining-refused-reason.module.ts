import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningRefusedReasonPageRoutingModule } from './dining-refused-reason-routing.module';

import { DiningRefusedReasonPage } from './dining-refused-reason.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningRefusedReasonPageRoutingModule
  ],
  declarations: [DiningRefusedReasonPage]
})
export class DiningRefusedReasonPageModule {}
