import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningRefusedReasonPage } from './dining-refused-reason.page';

describe('DiningRefusedReasonPage', () => {
  let component: DiningRefusedReasonPage;
  let fixture: ComponentFixture<DiningRefusedReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningRefusedReasonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningRefusedReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
