import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningRefusedReasonPage } from './dining-refused-reason.page';

const routes: Routes = [
  {
    path: '',
    component: DiningRefusedReasonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningRefusedReasonPageRoutingModule {}
