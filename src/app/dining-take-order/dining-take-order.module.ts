import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningTakeOrderPageRoutingModule } from './dining-take-order-routing.module';

import { DiningTakeOrderPage } from './dining-take-order.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningTakeOrderPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningTakeOrderPage]
})
export class DiningTakeOrderPageModule {}
