import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment-timezone';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-dining-take-order',
  templateUrl: './dining-take-order.page.html',
  styleUrls: ['./dining-take-order.page.scss'],
})
export class DiningTakeOrderPage implements OnInit {
  subscription:Subscription;
  currentMenu:any[]=[];
  upcoming:any[]=[];
    constructor(private router:Router,private platform:Platform,private http:HttpClient,private storage:Storage,
      private config:HttpConfigService,private loadingCtrl:LoadingController,private orient:ScreenOrientation) { }

  ngOnInit() {
   
    
  }
  async ionViewWillEnter(){
    this.currentMenu=[];
    this.upcoming=[];
this.showLoading();
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const tz=await this.storage.get('TIMEZONE');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'get_menu_list';
    let body={
      date:moment().tz(tz).format('YYYY-MM-DD')
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      let cm=res.data.current_menu;
      if(cm&&cm.length){
        cm.forEach(ele=>{
          if(ele.status==2){
            this.currentMenu.push(ele)
          }
        })
      }
      
      let um=res.data.other_menu;
      if(um&&um.length){
        um.forEach(ele=>{
          if(ele.status==2){
            this.upcoming.push(ele)
          }
        })
      }
      this.dismissLoader();
    },error=>{
      this.dismissLoader();
    });
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
   }
   
     back(){
       this.router.navigate(['/dining-dashboard'])
     }

     takeOrder(item){
       this.router.navigate(['/dining-choose-season-date',{menu:JSON.stringify(item)}],{replaceUrl:true})
     }
     async showLoading() {
      
      const loading = await this.loadingCtrl.create({
        cssClass: 'dining-loading',
        
        // message: 'Please wait...',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
      
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
}
