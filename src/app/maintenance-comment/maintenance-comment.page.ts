import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Subscription } from 'rxjs';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-maintenance-comment',
  templateUrl: './maintenance-comment.page.html',
  styleUrls: ['./maintenance-comment.page.scss'],
})
export class MaintenanceCommentPage  {
  req_id:any;
  comment:any;
  subscription:Subscription;
  // files:FileLikeObject[];
  
  imageResponse:any=[];
  visibility:boolean=false;
  isLoading:boolean=false;
  constructor(private route:ActivatedRoute,private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private router:Router,private platform:Platform,private toast:ToastController,private loadingCtrl:LoadingController,
    private actionsheetCntlr:ActionSheetController,private modalCntrl:ModalController) { }

 
  ionViewWillEnter(){
   
    this.imageResponse=[];
    
    this.req_id=this.route.snapshot.paramMap.get('req_id');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      
        this.router.navigate(['/maintenance-details',{req_id:this.req_id}],{replaceUrl:true}) ;
     
      }); 
  }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.imageResponse.push(base64Image);
  
    //    this.img.push(imageData);
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    // console.log('image:',base64Image);
   
    // this.imageResponse.push(base64Image);
    this.uploadImg(base64Image)

  }
  async addImage(){
    
  //   let options;
  //   options={
  //   maximumImagesCount: 5,
  //   outputType: 1,
  //   quality:100
  // }
  // if(this.platform.is('ios')){
  //   options.disable_popover=true
  // }
  //   this.imagePicker.getPictures(options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //         console.log('Image URI: ' + results[i]);
          
          
  //         if((results[i]==='O')||results[i]==='K'){
  //           console.log("no img");
            
  //           }else{
  //             this.img.push(results[i]);
  //           this.imageResponse.push('data:image/jpeg;base64,' + results[i])
  //           }
  //     }
  //   }, (err) => { });
  
  // console.log("imagg:",this.img);

  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
          console.log('Image URI: ' + image.photos[i]);
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
          // console.log('img:','data:image/jpeg;base64,' + contents.data);
          
            // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
            this.uploadImg('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }

  
  }
  removeImg(i){
    this.imageResponse.splice(i,1);
    
  }
  async submit(){
  
  
    
    
      
  console.log('com:',this.comment)
      
    
      const data=await this.storage.get("USER_ID")

        
        // let url=this.config.domain_url+'store_comment_app';
        let url=this.config.domain_url+'store_comments';
        let headers=await this.config.getHeader();;
        if(this.comment==undefined || this.comment=='' || this.comment==' '){
          // this.dismissLoader();
          this.presentAlert('Please enter your comment');
          // this.dismissLoader();
        }else{
          this.showLoading();
      //     this.files = this.fileField.getFiles();
      //     console.log(this.files);
      //   let formData = new FormData();
      // formData.append('from_id', data); // Add any other data you want to send
      // formData.append('request_id', this.req_id);
      // formData.append('comment', this.comment);
      // this.files.forEach((file) => {
      //   formData.append('images[]', file.rawFile);
      // });
      let visibility;
          if(this.visibility){
            visibility=1
          }else{
            visibility=0
          }
        let body={
          from_id:data,
          request_id:this.req_id,
          comment:this.comment,
          images:this.imageResponse,
          visibility:visibility
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.comment="";
          this.presentAlert('Comment added successfully.');
            this.dismissLoader();
          this.back();
          
        },error=>{
          this.dismissLoader();
            this.presentAlert('Something went wrong.Please try again.');
          console.log(error);
          
        })
        }
     
  }
  back(){
    
      this.router.navigate(['/maintenance-details',{req_id:this.req_id,flag:1}],{replaceUrl:true})
    
    this.comment='';
    this.dismissLoader();
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    this.isLoading=false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
      cssClass: 'maintenance-image-modal',
      componentProps: {
       data:this.imageResponse,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }

  async uploadImg(img){
    if(this.isLoading==false){
      this.showLoading();
      }
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
 
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      
      this.imageResponse.push(res.data);
      this.dismissLoader();
      
    })
  }
  
}
