import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceCommentPageRoutingModule } from './maintenance-comment-routing.module';

import { MaintenanceCommentPage } from './maintenance-comment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceCommentPageRoutingModule
  ],
  declarations: [MaintenanceCommentPage]
})
export class MaintenanceCommentPageModule {}
