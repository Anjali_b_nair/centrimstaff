import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinDiningAreaPageRoutingModule } from './din-dining-area-routing.module';

import { DinDiningAreaPage } from './din-dining-area.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinDiningAreaPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinDiningAreaPage]
})
export class DinDiningAreaPageModule {}
