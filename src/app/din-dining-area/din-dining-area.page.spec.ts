import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinDiningAreaPage } from './din-dining-area.page';

describe('DinDiningAreaPage', () => {
  let component: DinDiningAreaPage;
  let fixture: ComponentFixture<DinDiningAreaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinDiningAreaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinDiningAreaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
