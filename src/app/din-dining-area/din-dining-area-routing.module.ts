import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinDiningAreaPage } from './din-dining-area.page';

const routes: Routes = [
  {
    path: '',
    component: DinDiningAreaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinDiningAreaPageRoutingModule {}
