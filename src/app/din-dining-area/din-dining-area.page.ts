import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ModalController, LoadingController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { AssignSeatComponent } from '../components/dining/assign-seat/assign-seat.component';
import { RemoveSeatComponent } from '../components/dining/remove-seat/remove-seat.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-din-dining-area',
  templateUrl: './din-dining-area.page.html',
  styleUrls: ['./din-dining-area.page.scss'],
})
export class DinDiningAreaPage implements OnInit {

  subscription:Subscription;
  area:any;
  area_id:any;
  ser_time:any;
  servetime:any;
  date:any;
  details:any;
  terms:any;
  hide:boolean=true;
  constructor(private router:Router,private platform:Platform,private modalCntrl:ModalController,
    private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private loadingCtrl:LoadingController,private popCntrl:PopoverController) { }

  ngOnInit() {
  }
ionViewWillEnter(){
 this.area=this.route.snapshot.paramMap.get('area');
 this.ser_time=this.route.snapshot.paramMap.get('ser_time_id');
 this.area_id=this.route.snapshot.paramMap.get('area_id');
 this.servetime=this.route.snapshot.paramMap.get('ser_time');
 this.date=this.route.snapshot.paramMap.get('date');
 this.getDetails();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    
      back(){
        this.router.navigate(['/din-choose-dt-mlt-da'],{replaceUrl:true})
      }


      async getDetails(){
        this.showLoading();
        const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
       
        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'serving_item_listing_table';
        
        let body={
          company_id:cid,
          servingtimeid:this.ser_time,
          date:moment(this.date).format('YYYY-MM-DD'),
          serveareaid:this.area_id,
          wing_id:[0]
        }
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          
          console.log('res:',res);
          this.details=res.data;
          this.dismissLoader();
        },error=>{
          console.log(error)
          this.dismissLoader();
        })
      }


      tableDetails(item){
        this.router.navigate(['/din-dining-table',{table:JSON.stringify(item),ser_time:this.servetime,date:this.date,area_id:this.area_id,area:this.area,ser_time_id:this.ser_time,menu_id:this.details.menu.id,serving_order:this.details.serving_order}],{replaceUrl:true})
      }
      async assignSeat(seat){
        const modal = await this.modalCntrl.create({
          component: AssignSeatComponent,
          cssClass: 'full-width-modal',
          componentProps:{
            seat:seat,
            area:this.area_id
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
          if(dataReturned.data){
         this.getDetails();
          }
        });
        return await modal.present();
      }


      async showLoading() {
      
        const loading = await this.loadingCtrl.create({
          cssClass: 'dining-loading',
          // message: 'Please wait...',
          // message: '<ion-img src="assets/loader.gif"></ion-img>',
          spinner: null,
          // duration: 3000
        });
        return await loading.present();
      }
      
      async dismissLoader() {
        
          return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
      }

      cancel(){
        this.hide=true;
        this.terms='';
        console.log('cancel:',this.hide);
      }
      search(){
        this.hide=false;
      }

      async removeSeat(item,ev){
        const popover = await this.popCntrl.create({
          component: RemoveSeatComponent,
          event: ev,
          backdropDismiss: true,
          cssClass: 'dining-more-options-popover',
          componentProps: {
            
            consumer:item.user_id
            
          },
        });
          popover.onDidDismiss().then((dataReturned) => {
            if(dataReturned.data){
              this.getDetails();
               }
          });
    
        
        return await popover.present();
      }
}
