import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { NotificationComponent } from '../components/notification/notification.component';
import { HttpConfigService } from '../services/http-config.service';
import { PushNotifications } from '@capacitor/push-notifications';
// import { PushNotifications } from '@capacitor/push-notifications';
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.page.html',
  styleUrls: ['./userprofile.page.scss'],
})
export class UserprofilePage implements OnInit {
  name:string;
  p_name:string;
  img:any;
  email:any;
  details:any={};
  parent:any=[];
  notifyAll:boolean;
  notify:any=[];
  // myStory:any;
  // allStory:any;
  // activity:any;
  // subStory:any;
  message:any;
  missed:any;
  user_type:any;
  subscription:Subscription;
  expanded:boolean;
  status:any;
  my_comment:any;
  call:any;
  isLoading:boolean=true;
  constructor(private router:Router,public storage:Storage,private platform:Platform,private iab:InAppBrowser,
    public alertController: AlertController,private http:HttpClient,private config:HttpConfigService,
    private modalCntlr:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.isLoading=true;

    const name=await this.storage.get("NAME")
      this.name=name;
      this.p_name=this.name.substr(0,1);
    
    const pic=await this.storage.get("PRO_IMG")
      this.img=pic;
  
    const mail=await this.storage.get("EMAIL")
      this.email=mail;
   
   
    const data=await this.storage.get("USER_TYPE")

      this.user_type=data;
    
  
  this.getContent();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
});  
 }


 ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  select(event){
    if(this.isLoading==false){
    if(event.currentTarget.checked==true){
      this.expanded=true;
      // this.myStory=1;
      // this.activity=1;
      this.my_comment=1;
      this.message=1;
      this.call=1;
      this.status=1;
      // this.storage.set("NOTIFY",true);
  
    }else{
      // this.myStory=0;
      this.expanded=false;
      // this.activity=0;
      this.my_comment=0;
      this.message=0;
      this.call=0;
      this.status=0;
      // this.storage.set("NOTIFY",false);
    }
    console.log("notify",event.currentTarget.checked);
    
      this.updateNotificationStatus();
      }
    }
    @HostListener('touchstart')
    onTouchStart() {
      console.log('pageload:',this.isLoading)
      this.isLoading=false;
    }
  logOut(){
    this.presentAlertConfirm();
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      mode:'ios',
      header: 'Log out',
      message: 'You will be returned to the login screen.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Logout',
          handler:async ()  => {
            // this.auth.logout();
            await this.storage.clear();
            this.storage.set("loggedIn",'0');
            this.storage.set("launched",true);
            // this.firebase.unregister().then(()=>{});
            // this.userData.next(false);
           this.deleteuuid();
            this.router.navigate(['/login'],{replaceUrl:true});
            await PushNotifications.unregister();
          }
        }
      ]
    });
  
    await alert.present();
  }
  
  async getContent(){
    this.parent=[];
    
      const data=await this.storage.get("USER_ID")

        let url=this.config.domain_url+'profile/'+data;
        let headers=await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log(res);
            this.details=res.data;
            // this.myStory=this.details.user_detail_notification.my_story;
                // this.activity=this.details.user_detail_notification.consumer_activity;
                this.my_comment=this.details.user_detail_notification.my_story_comment;
                // this.subStory=this.details.user_detail_notification.post_subscribed;
                this.message=this.details.user_detail_notification.message;
                // this.missed=this.details.user_detail_notification.missed_call;
                this.call=this.details.user_detail_notification.video_call;
                this.status=this.details.user_detail_notification.status;
                if(this.details.user_detail_notification.status==1){
                  this.notifyAll=true;
                  this.expanded=true
                }else{
                  this.notifyAll=false;
                  this.expanded=false;
                }
            
                
                // this.notify=[
                //   {'title':'New comment for my story','status':this.details.user_detail_notification.my_story_comment},
                //   {'title':'New comment for all stories','status':this.details.user_detail_notification.all_story_comment},
                //   {'title':'New comment for a story that I subscribed','status':this.details.user_detail_notification.post_subscribed},
                //   {'title':'New message','status':this.details.user_detail_notification.message},
                //   {'title':'Missed call','status':this.details.user_detail_notification.missed_call},
                // ]
                
                
          },error=>{
            console.log(error);
            
          });
      
  }
  
  
  // myStoryStatus(i){
  //   // if(status.currentTarget.checked==true)
  
  //   if(i.currentTarget.checked==true){
  //     this.myStory=1;
  //   }else{
  //     this.myStory=0;
  //   }
  //   this.updateNotificationStatus();
  // }
  
  // activityStatus(i){
  //   if(i.currentTarget.checked==true){
  //     this.activity=1;
  //   }else{
  //     this.activity=0;
  //   }
  //   this.updateNotificationStatus();
  // }
  commentStatus(i){
    if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.my_comment=1;
    }else{
      this.my_comment=0;
    }
    
      this.updateNotificationStatus();
      }
  }
  messageStatus(i){
    if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.message=1;
    }else{
      this.message=0;
    }
    
      this.updateNotificationStatus();
      }
  }
  callStatus(i){
    if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.call=1;
    }else{
      this.call=0;
    }
    
      this.updateNotificationStatus();
      }
  }
  async updateNotificationStatus(){
   
      
        
      const data=await this.storage.get("USER_ID")

        
        let url=this.config.domain_url+'profile/'+data;
        let body={
          my_story:0,
          video_call:this.call,
          my_story_comment:this.my_comment,
          consumer_activity:0,
          message:this.message,
          status:this.status,
        }
        let headers=await this.config.getHeader();
        console.log("body:",body);
          this.http.put(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
          },error=>{
            console.log(error);
            
          });
     
     
  }
 
  
  reset(){
    this.router.navigate(['/resetpassword',{flag:1}]);
  }
  

  openTerms(){
  
    let options:InAppBrowserOptions ={
      location:'yes',
    hideurlbar:'yes',
    zoom:'no',
    hidenavigationbuttons:'yes'
    }
    // const browser = this.iab.create('https://app.centrim.life/terms-and-conditions','_blank',options);
    const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)
  
  }


  async shownotificationsettings(){
    const not={
      com:this.my_comment,
      mes:this.message,
      call:this.call,
      status:this.status}
    const modal = await this.modalCntlr.create({
      component: NotificationComponent,
      cssClass:'notify-modal',
      componentProps: {
        data:not
        
        
         
      },
      
      
      
    });
    modal.onDidDismiss().then((dataReturned)=>{
      console.log(dataReturned)
      if(dataReturned.data){
    
      this.my_comment=dataReturned.data.com;
      this.message=dataReturned.data.mes;
      this.call=dataReturned.data.call;
      this.status=1;
      this.updateNotificationStatus();
      }
      })
    return await modal.present();
  }
  async deleteuuid(){
    const uid=await this.storage.get('USER_ID');
    const uuid = await this.storage.get('uuid');
    let url=this.config.domain_url+'delete_devicetoken';
    let headers=await this.config.getHeader();
    let body={
      user_id:uid,
      device_token:uuid
    }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log('deletetok:',res);
        
      })
  }
}
