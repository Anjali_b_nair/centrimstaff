import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResidentOverviewPageRoutingModule } from './resident-overview-routing.module';

import { ResidentOverviewPage } from './resident-overview.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResidentOverviewPageRoutingModule,
    ApplicationPipesModule,
    CalendarModule
  ],
  declarations: [ResidentOverviewPage]
})
export class ResidentOverviewPageModule {}
