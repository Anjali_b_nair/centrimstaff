import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CbDisabletimeslotPageRoutingModule } from './cb-disabletimeslot-routing.module';

import { CbDisabletimeslotPage } from './cb-disabletimeslot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CbDisabletimeslotPageRoutingModule
  ],
  declarations: [CbDisabletimeslotPage]
})
export class CbDisabletimeslotPageModule {}
