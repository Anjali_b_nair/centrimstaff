import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CbDisabletimeslotPage } from './cb-disabletimeslot.page';

describe('CbDisabletimeslotPage', () => {
  let component: CbDisabletimeslotPage;
  let fixture: ComponentFixture<CbDisabletimeslotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbDisabletimeslotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CbDisabletimeslotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
