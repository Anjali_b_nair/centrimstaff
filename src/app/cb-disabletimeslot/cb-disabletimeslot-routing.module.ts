import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CbDisabletimeslotPage } from './cb-disabletimeslot.page';

const routes: Routes = [
  {
    path: '',
    component: CbDisabletimeslotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CbDisabletimeslotPageRoutingModule {}
