import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-story-viewed-list',
  templateUrl: './story-viewed-list.component.html',
  styleUrls: ['./story-viewed-list.component.scss'],
})
export class StoryViewedListComponent implements OnInit {
@Input() data:any;
views:any[]=[];
  constructor(private config:HttpConfigService,private http:HttpClient,private modalCntrl:ModalController) { }

  ngOnInit() {}


async ionViewWillEnter(){
  this.views=[];
  let url=this.config.domain_url+'post_action_view_list';
  let headers=await this.config.getHeader();
  let body={post_id:this.data}
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("res:",res);
    this.views=res.views;
  },error=>{
    console.log(error);
    
  })
}
dismiss(){
  this.modalCntrl.dismiss();
}
}
