import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-activity-attendees-options',
  templateUrl: './activity-attendees-options.component.html',
  styleUrls: ['./activity-attendees-options.component.scss'],
})
export class ActivityAttendeesOptionsComponent implements OnInit {
@Input() flag;
@Input() item;
@Input() act_id;
@Input() wl;
  constructor(private popCntrl:PopoverController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private toastCntlr:ToastController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('itemdetails:',this.item);
    
  }
  async remove(){
    let headers=await this.config.getHeader();
    let action;
    if(this.flag==1){
      action=1
    }else{
      action=0
    }
      let url=this.config.domain_url+'remove_signup';
       
       let body={
        user_id:this.item,
        activity_id:this.act_id,
        action : action
       }
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res,body);
            if(res.status=='success'){
       
              this.presentAlert('removed successfully.');
              this.popCntrl.dismiss(1);
             }else{
              this.presentAlert('Something went wrong. Please try again later.')
              this.popCntrl.dismiss();
             }
             },error=>{
              console.log(error);
              
              this.presentAlert('Something went wrong. Please try again later.');
              this.popCntrl.dismiss();
             })
   
    
   
  }

  async moveToSignup(i){
    const uid=await this.storage.get('USER_ID');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'move_signup';
    let body={
      activity_id:this.act_id,
      user_id:this.item,
      action:i,
      created_by:uid
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('signedup:',res,body);
       if(res.status=='success'){
       if(i==0){
        this.presentAlert('Signed up successfully.');
       }else{
        this.presentAlert('Moved to waiting list successfully.');
       }
        this.popCntrl.dismiss(1);
       }else{
        this.presentAlert('Something went wrong. Please try again later.')
        this.popCntrl.dismiss();
       }
       },error=>{
        console.log(error,body);
        
        this.presentAlert('Something went wrong. Please try again later.');
        this.popCntrl.dismiss();
       })
      
    
  }

  async moveToWl(){
    const uid=await this.storage.get('USER_ID');
    let headers=await this.config.getHeader();
   
      
      let url=this.config.domain_url+'add_to_waiting_list';
       
       let body={
        user_id:[this.item],
        activity_id:parseInt(this.act_id),
        action : 0,
        created_by:uid
       }
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res,body);
            if(res.status=='success'){
       
              this.presentAlert('Moved to waiting list successfully.');
              this.popCntrl.dismiss(1);
             }else{
              this.presentAlert('Something went wrong. Please try again later.')
              this.popCntrl.dismiss();
             }
             },error=>{
              console.log(error,body);
              
              this.presentAlert('Something went wrong. Please try again later.');
              this.popCntrl.dismiss();
             })
    
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'    
    });
    alert.present(); //update
  }
}
