import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActivityAttendeesOptionsComponent } from './activity-attendees-options.component';

describe('ActivityAttendeesOptionsComponent', () => {
  let component: ActivityAttendeesOptionsComponent;
  let fixture: ComponentFixture<ActivityAttendeesOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityAttendeesOptionsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActivityAttendeesOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
