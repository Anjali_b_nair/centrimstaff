import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-consent-tag-alert',
  templateUrl: './consent-tag-alert.component.html',
  styleUrls: ['./consent-tag-alert.component.scss'],
})
export class ConsentTagAlertComponent implements OnInit {
@Input() data;
name:any;
  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}

  ionViewWillEnter(){
      console.log("no-consent:",this.data);
      this.name=this.data.user.name;
      
      
  }

  async dismiss(){
    const onClosedData:any='proceed';
    this.modalCntrl.dismiss(onClosedData);
  }
  async cancel(){
    this.modalCntrl.dismiss();
  }
}
