import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, PopoverController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendedReasonComponent } from '../activity-attended-reason/activity-attended-reason.component';
import { ActivityDeclinedReasonComponent } from '../activity-declined-reason/activity-declined-reason.component';

@Component({
  selector: 'app-rv-activity-details',
  templateUrl: './rv-activity-details.component.html',
  styleUrls: ['./rv-activity-details.component.scss'],
})
export class RvActivityDetailsComponent implements OnInit {
  @Input() data:any;
  @Input() cid:any;
day:any;
month:any;
year:any;
activity:any;
image:any=[];
photos:any=[];
doc:any=[];

signupcount:any;
waitinglist:boolean=false;
rvsettings:any;
pastactivity:boolean=true;
  constructor(private modalCntrl:ModalController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private popCntlr:PopoverController,
   ) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    console.log('item:',this.data)
    const rv=await this.storage.get('RVSETTINGS');
    this.rvsettings=rv;
    this.activity=this.data
   
  }
  dismiss(){
    this.modalCntrl.dismiss();
  }
  async add(){
    const modal = await this.modalCntrl.create({
      component: ActivityAttendedReasonComponent,
      componentProps: {
        
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data != null || dataReturned.data!=undefined) {
        console.log("dataret:",dataReturned);
        
        // this.attendees = dataReturned.data;
        this.markAttendance(dataReturned.data,1)
        
      }
    });
    return await modal.present();
  }
  async decline(){
    const modal = await this.modalCntrl.create({
      component: ActivityDeclinedReasonComponent,
      componentProps: {
        
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data != null || dataReturned.data!=undefined) {
        console.log("dataret:",dataReturned);
        
        // this.attendees = dataReturned.data;
        this.markAttendance(dataReturned.data,2)
        
      }
    });
    return await modal.present();
  }


  async markAttendance(reason,action){
   
    

   
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID");
        let headers=await this.config.getHeader();;

        let url=this.config.domain_url+'mark_attendence';
        let body={
          activity_id:this.data.id,
          company_id:cid,
          user_id:this.cid,
          reason:reason,
          action:action,
          marked_by:uid
        }
        console.log('body:',body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
        this.modalCntrl.dismiss(1);

        },error=>{
          console.log(error);
          
        })
     
  }
}
