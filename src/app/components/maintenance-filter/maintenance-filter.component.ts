import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-maintenance-filter',
  templateUrl: './maintenance-filter.component.html',
  styleUrls: ['./maintenance-filter.component.scss'],
})
export class MaintenanceFilterComponent implements OnInit {
//   @Input() data;
//   @Input() flag;
//    sel_index:any;
//    sub_id:any;
//    type:any;
//    sub:any=[];
//  filters:any;
//  branch:any=[];
//  startDate:any;
//  endDate:any;
//  custom:boolean=false;
//  minDate:string = new Date().toISOString();



expanded1: boolean = false;
  expanded2: boolean = false;
  expanded3: boolean = false;
  expanded4: boolean = false;
  expanded5: boolean = false;
  expanded6: boolean = false;
  expanded7: boolean = false;
  expanded8: boolean = false;

  type: any = [];
  @Input()selected_type;
  tec: any = [];
  @Input() filter: any;
  // subscription: Subscription;
  @Input()technician: any;
  @Input()status;
  @Input()priority;
  @Input()risk;
  @Input()reqType;


  @Input()startDate;
  @Input()endDate;
  custom: boolean = false;
  minDate: string = new Date().toISOString();
  @Input()datefil;

  @Input()crstartDate: any;
  @Input()crendDate: any;
  @Input()crdatefil;
  utype: any;
  role: any;
  @Input() branch: any;
   constructor(private popCntl:PopoverController,private http:HttpClient,private storage:Storage,
    private config:HttpConfigService,private toastCntlr:ToastController,private modalCntl:ModalController) { }
 
   ngOnInit() {}
  //  ionViewWillEnter(){
  //    this.sub=[];
  //    this.branch=[];
     
  //    if(this.flag==1){
 
  //      this.storage.ready().then(()=>{
  //        const data=await this.storage.get("COMPANY_ID")

  //          let url=this.config.domain_url+'branch_viatype';
  //          let body={company_id:data,
  //                    role_id:4}
  //          let headers=await this.config.getHeader();
  //          this.http.post(url,body).subscribe((res:any)=>{
  //            console.log("res:",res);
  //            res.data.forEach(element=>{
  //              let branch={id:element.id,status:element.name}
  //              this.branch.push(branch);
  //            })
             
  //          })
  //        })
  //      })
       
  //      this.filters=[
  //        // {
  //        //   filter:"All",
  //        //   sub:null
  //        // },
  //        // {
  //        //   filter:"Branch",
  //        //   sub:this.branch
  //        // },
  //        {
  //          filter:"Status",
  //          sub:[{id:0,status:"Pending"},
  //               {id:1,status:"Inprogress"},
  //               {id:2,status:"With third party"},
  //               {id:3,status:"Closed"},
  //               {id:4,status:"Reopened"},
  //               {id:5,status:"Waiting for feedback"}
  //              ]
  //        },
         
         
  //        {
  //          filter:"Risk rating",
  //          sub:[{id:0,status:"Low"},
  //               {id:1,status:"Medium"},
  //               {id:2,status:"High"}
  //              ]
  //        },
  //        {
  //         filter:"Date",
  //         sub:[{id:0,status:"Today"},
  //              {id:1,status:"Yesterday"},
  //              {id:2,status:"Last 7 days"},
  //              {id:3,status:"Last 30 days"},
  //              {id:4,status:"Custom range"}
  //             ]
  //       },
  //      ]
       
  //    this.data.forEach(element => {
  //      let tec={id:element.user_id,status:element.name}
  //       this.sub.push(tec);
       
  //    });
  //    this.filters.splice(2,0,{filter:"Technician",sub:this.sub});
  //  }else{
  //    this.filters=[
  //      {
  //        filter:"All",
  //        sub:null
  //      },
  //      {
  //        filter:"Status",
  //        sub:[{id:0,status:"Pending"},
  //             {id:1,status:"Inprogress"},
  //             {id:2,status:"With third party"},
  //             {id:3,status:"Closed"},
  //             {id:4,status:"Reopened"},
  //             {id:5,status:"Waiting for feedback"}
  //            ]
  //      },
       
      
  //      {
  //        filter:"Priority",
  //        sub:[{id:0,status:"Low"},
  //             {id:1,status:"Medium"},
  //             {id:2,status:"Urgent"}
  //            ]
  //      }, {
  //       filter:"Date",
  //       sub:[{id:0,status:"Today"},
  //            {id:1,status:"Yesterday"},
  //            {id:2,status:"Last 7 days"},
  //            {id:3,status:"Last 30 days"},
  //            {id:4,status:"Custom range"}
  //           ]
  //     }
  //    ]
  //  }
  //  }
  //  expand(item,i){
 
  //      this.sel_index=i;
     
  //  }
  //  selected(sub,id){
  //    console.log("sub:",sub,sub.filter)
  //    this.type=sub.filter;
  //    console.log("type:",this.type,sub.filter)
  //    this.sub_id=id;
  //    if(this.type=='Date'&&this.sub_id==4){
  //       this.custom=true;
  //    }else{
  //    this.closeModal();
  //    }
  //    console.log("filt:",this.type,this.sub_id,this.custom)
  //  }
 
  //  async closeModal() {
  //    const onClosedData: any = {filter:this.type,id:this.sub_id};
  //    await this.popCntl.dismiss(onClosedData);
  //  }

  //  apply(){
  //   const onClosedData: any = {filter:this.type,id:this.sub_id,start:this.startDate,end:this.endDate};
  //    this.popCntl.dismiss(onClosedData);
  //  }





  ionViewWillEnter() {
    // this.filter = this.route.snapshot.paramMap.get('filter');
    // this.branch = this.route.snapshot.paramMap.get('branch');
    this.tec = [];
    this.type = [];
    if(!this.selected_type){
      this.selected_type='all'
    }
    if(!this.technician){
      this.technician='all';
    }
    if(!this.status){ this.status="unresolved";
    }
    if(!this.priority){ this.priority="all";
  }
    if(!this.risk){ this.risk='all';
}
    if(!this.reqType){this.reqType= "all";
}

  if(!this.datefil){this.datefil = 'all';
}

  if(!this.crdatefil){this.crdatefil = 'all';
}
    this.getData();
    this.getTechnician();

   
  }

  // back() {
  //   this.router.navigate(['/job-listing'], { replaceUrl: true });
  // }
  openList(i) {
    console.log("open:", i);

    if (i == 1) {
      this.expanded1 = !this.expanded1;
      // this.expanded2 =false;
      // this.expanded3 =false;
      // this.expanded4 =false;
      // this.expanded5 =false;
      // this.expanded6 =false;
      // this.expanded7 =false;
    } else if (i == 2) {
      // this.expanded1 =false;
      this.expanded2 = !this.expanded2;
      // this.expanded3 =false;
      // this.expanded4 =false;
      // this.expanded5 =false;
      // this.expanded6 =false;
      // this.expanded7 =false;
    } else if (i == 3) {
      // this.expanded1 =false;
      // this.expanded2 =false;
      this.expanded3 = !this.expanded3;
      // this.expanded4 =false;
      // this.expanded5 =false;
      // this.expanded6 =false;
      // this.expanded7 =false;
    } else if (i == 4) {
      // this.expanded1 =false;
      // this.expanded2 =false;
      // this.expanded3 =false 
      this.expanded4 = !this.expanded4;
      // this.expanded5 =false;
      // this.expanded6 =false;
      // this.expanded7 =false;
    } else if (i == 5) {
      // this.expanded1 =false;
      // this.expanded2 =false;
      // this.expanded3 =false;
      // this.expanded4 =false;
      this.expanded5 = !this.expanded5;
      // this.expanded6 =false;
      // this.expanded7 =false;
    } else if (i == 6) {
      // this.expanded1 =false;
      // this.expanded2 =false;
      // this.expanded3 =false;
      // this.expanded4 =false;
      // this.expanded5 =false;
      this.expanded6 = !this.expanded6;
      // this.expanded7 =false;
    } else if (i == 7) {
      // this.expanded1 =false;
      // this.expanded2 =false;
      // this.expanded3 =false;
      // this.expanded4 =false;
      // this.expanded5 =false;
      // this.expanded6 =false;
      this.expanded7 = !this.expanded7;
    } else if (i == 8) {
      this.expanded8 = !this.expanded8;
    }
  }

  async getData() {
   
      const bid=await this.storage.get('BRANCH')
        const type=await this.storage.get('USER_TYPE')
          this.utype = type;
          let url = this.config.domain_url + 'maintenance_filter';

          let id = bid.toString();
          // let headers = new HttpHeaders({ 'branch_id': id });
          let headers=await this.config.getHeader();
          console.log("head:", url);
          let body = {
            status: '0',
            offset: 0,   // always 0
            limit: 21,
            role_id: type
          }
          this.http.post(url, body, { headers }).subscribe((res: any) => {


            this.type.push({ id: 'all', type: 'All type' });
            res.type.forEach(element => {
              this.type.push({ id: element.id, type: element.type })
            });
            this.type.push({id:0,type:'Other'})
            console.log("main:", res, this.tec);
          }, error => {
            console.log(error);

          })
      
   
  }


  apply() {

    if (this.datefil == 4) {
      if (this.startDate == undefined || this.endDate == undefined) {
        this.presentAlert('Please enter the start&end dates.')
      }
    }
    if (this.crdatefil == 4) {
      if (this.crstartDate == undefined || this.crendDate == undefined) {
        this.presentAlert('Please enter the start&end dates.')
      }
    }
    const onClosedData: any = { tec: this.technician, type: this.selected_type, status: this.status, risk: this.risk, date: this.datefil, start: this.startDate, end: this.endDate, flag: 1, reqType: this.reqType, crdatefil: this.crdatefil, crStart: this.crstartDate, crEnd: this.crendDate, priority: this.priority };
       this.modalCntl.dismiss(onClosedData);
    // this.router.navigate(['/job-listing', { tec: this.technician, type: this.selected_type, status: this.status, risk: this.risk, date: this.datefil, start: this.startDate, end: this.endDate, flag: 1, reqType: this.reqType, crdatefil: this.crdatefil, crStart: this.crstartDate, crEnd: this.crendDate, priority: this.priority }], { replaceUrl: true })
  }
async dismiss(){
  await this.modalCntl.dismiss();
}
  reset() {
    this.technician = 'all';
    this.status = "unresolved";
    this.priority = "all";
    this.risk = 'all';
    this.reqType = "all";
    this.datefil = 'all';
    this.crdatefil = 'all';
    this.selected_type = 'all';
  }
  async getTechnician() {
    let url = this.config.domain_url + 'technician_list';
    
      const cid=await this.storage.get('COMPANY_ID')
        const bid=await this.storage.get('BRANCH')
        // let headers = new HttpHeaders({ 'company_id': cid.toString() });
        let headers=await this.config.getHeader();
        let branch=[];
        if(this.branch){
        branch.push(this.branch)
        }else{
          branch.push(bid)
        }
        let body={
          branch_id:branch
        }
        this.http.post(url,body, { headers }).subscribe((res: any) => {
          console.log("tech:", res,body);
          this.tec.push({ user_id: 'all', name: 'All' });

          res.data.forEach(element => {
            this.tec.push({ user_id: element.user_id, name: element.name })
          });
        })
      
}
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
