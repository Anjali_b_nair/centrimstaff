import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cb-info',
  templateUrl: './cb-info.component.html',
  styleUrls: ['./cb-info.component.scss'],
})
export class CbInfoComponent implements OnInit {
@Input() data;

  constructor() { }

  ngOnInit() {}

}
