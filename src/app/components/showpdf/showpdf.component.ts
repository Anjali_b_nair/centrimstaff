import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-showpdf',
  templateUrl: './showpdf.component.html',
  styleUrls: ['./showpdf.component.scss'],
})
export class ShowpdfComponent implements OnInit {

  @Input() data:any;
  pdfSource:any;
  zoom_to:any=1;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {
    // this.data='https://docs.google.com/gview?embedded=true&url='+this.data
  }
  dismissModal(){
    this.modalCntl.dismiss();
  }
  ionViewWillEnter(){
    // this.data=this.data+'?origin='+ window.location.host
    // this.pdfSource='https://app.centrimlife.uk/public/media/resource/1594786364_faith-in-time-%20of-pandemic_v2.pdf'
    this.pdfSource=this.data
    // this.data=this.domSanitizer.bypassSecurityTrustResourceUrl(this.navParams.data.data);
    
    console.log('data:',this.data)
  }
  zoomIn() {
    this.zoom_to = this.zoom_to + 0.25;
  }

  zoomOut() {
    if (this.zoom_to > 1) {
       this.zoom_to = this.zoom_to - 0.25;
    }
  }
}
