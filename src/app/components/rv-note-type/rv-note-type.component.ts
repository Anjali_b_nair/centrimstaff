import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-rv-note-type',
  templateUrl: './rv-note-type.component.html',
  styleUrls: ['./rv-note-type.component.scss'],
})
export class RvNoteTypeComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}


  setNoteType(t){
    this.modalCntl.dismiss(t)
  }
}
