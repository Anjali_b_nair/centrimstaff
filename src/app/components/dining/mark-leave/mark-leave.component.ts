import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import moment from 'moment';
@Component({
  selector: 'app-mark-leave',
  templateUrl: './mark-leave.component.html',
  styleUrls: ['./mark-leave.component.scss'],
})
export class MarkLeaveComponent implements OnInit {
minDate:any=new Date().toISOString();
date:any;
@Input() consumer;
  constructor(private modalCntrl:ModalController,private http:HttpClient,private config:HttpConfigService,private toast:ToastController,
    private storage:Storage) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }

  async continue(){

    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'mark_dietry_leave';
    let headers=await this.config.getHeader();
    let body={
      company_id:cid,
      created_by:uid,
      consumer_id:this.consumer,
      start:moment(this.date).format('YYYY-MM-DD')

    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
      this.presentAlert('Leave marked successfully.');
      this.modalCntrl.dismiss(1);
    },error=>{

    })

  }
  async presentAlert(mes) {
    const alert = await this.toast.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
