import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment-timezone';
import { DiningIrFiDetailsComponent } from '../dining-ir-fi-details/dining-ir-fi-details.component';
import { MenuItemDetailsComponent } from '../menu-item-details/menu-item-details.component';
import { ContinentalItemComponent } from '../continental-item/continental-item.component';
import { AllergicInfoComponent } from '../allergic-info/allergic-info.component';
import { AdditionalItemDetailsComponent } from '../additional-item-details/additional-item-details.component';
@Component({
  selector: 'app-dining-add-items',
  templateUrl: './dining-add-items.component.html',
  styleUrls: ['./dining-add-items.component.scss'],
})
export class DiningAddItemsComponent implements OnInit {
@Input() date:any;
@Input() serve_time:any;
@Input() item:any;
@Input() menu_id:any;

menu_item:any=[];
servingtime:any;
additional:any[]=[];
allergies:any[]=[];

selected:any[]=[];
sel_additional_item:any[]=[];
texture:any;
saved:boolean=false;
preOrderedItems:any[]=[];
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private toastCntlr:ToastController,private loadingCtrl:LoadingController) { }

  ngOnInit() {}

  back(){
    this.modalCntl.dismiss();
  }
  async ionViewWillEnter(){
    this.selected=[];
    this.saved=false;
    this.sel_additional_item=[];
    // if(this.item.additional_order&&this.item.additional_order.length){
    //   this.item.additional_order.forEach(element => {
    //     this.sel_additional_item.push(element.item.id)
    //   });
    // }
    // if(this.item.order&&this.item.order.length){
    //   if(this.item.order[0].order_items&&this.item.order[0].order_items.length){
    //     this.item.order[0].order_items.forEach(ele=>{
    //       this.selected.push(ele.menu_item_id)
    //     })
    //   }
    // }
    this.showLoading();
    this.getContent();
    if(this.item.profile.allergies){
      
        this.allergies=this.item.profile.allergies.split(/\, +/);
      
    }
  }

  async getContent(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    this.preOrderedItems=[];
   console.log('it:',this.item)
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'ordering_servingtime_wise_item_listing'

    
    
    let body={
      company_id:cid,
      dining_id:this.menu_id,
      servingtime:this.serve_time,
      date:moment(this.date).format('YYYY-MM-DD'),
      serveareaid:'all',
      wing_id:[this.item.wing_id],
      consumer_id:this.item.user.user_id
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      console.log('items:',res,'body:',this.sel_additional_item);

      this.menu_item=res.data.meals_with_items;
      this.servingtime=res.data.meals_time[0].servingtime;
      this.texture=res.data.user_texture;
      if (this.menu_item.length) {
        this.menu_item.map(x => {
          if (x.pre_item_orders.length) {
            
            x.pre_item_orders.map(y => {
              this.preOrderedItems.push(y);
              if(!this.selected.includes(y.menu_item_id)){
              this.selected.push(y.menu_item_id)
              }
            })
          }
          if (x.additional_items.length) {
            x.additional_items.map(y => {
              if (y.selected_data) {
                if(!this.sel_additional_item.includes(y.selected_data.item_id)){
                this.sel_additional_item.push(y.selected_data.item_id);
                }
              }
            })
          }
          if (x.all_coloumns.length) {
            x.all_coloumns.map(y => {
              if(y.items.length==1){
              if (y.items[0].is_ordered==true) {
                if(!this.selected.includes(y.items[0].id)){
                this.selected.push(y.items[0].id)
                }
              }
            }else{
              y.items.map(z=>{
                if(z.is_ordered==true){
                  if(!this.selected.includes(z.id)){
                  this.selected.push(z.id)
                  }
                }
              })
            }
            })
          }
        })
      }
      // this.additional=re
      this.dismissLoader();
    },error=>{
      this.dismissLoader();
    })
  }
  async showDetails(i){
    console.log('detailitem:',i)
    if(i.subitem==1){
      this.openContinentalDetails(i,1,i)
    }else{
      if(!this.availableItem(i)){
      }else{
    const modal = await this.modalCntl.create({
      component: MenuItemDetailsComponent,
      cssClass:'fullWidth-modal',
      componentProps:{
        item:i,
        date:moment(this.date).format('YYYY-MM-DD'),
        serving_time:this.servingtime,
        menu_id:this.menu_id,
        consumer:this.item.user.user_id,
        serving_time_id:this.serve_time,
        // ml_sz:item,
        sel_quantity:1,
        flag:1
        
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      console.log('datareturned:',dataReturned);
      
      if(dataReturned.data){
        
        // this.selected.push(dataReturned.data);
        this.getContent()
      }
    });
   
    return await modal.present();
  }
}
}

async openContinentalDetails(i,or,item){
  
  let component;
 //  if(this.screen_width>=768){
 //    component=ContinentalItemTabviewComponent
 //  }else{
    component=ContinentalItemComponent;
 //  }

  console.log(component)
 const modal = await this.modalCntl.create({

   component: component,
   cssClass:'full-width-modal',
   componentProps:{
     menu:i,
       date:moment(this.date).format('YYYY-MM-DD'),
       serving_time_id:this.serve_time,
       menu_id:this.menu_id,
       con_name:this.item.user.name,
       consumer:this.item.user.user_id,
       fdtexture:this.item.profile.food_texture?this.item.profile.food_texture.id:null,
          fltexture:this.item.profile.fluid_texture?this.item.profile.fluid_texture.id:null,
          allergies:this.allergies,
       flag:1
   }
 });
 modal.onDidDismiss().then((dataReturned) => {
   if(dataReturned.data){
    if (or == 2) {
      //   let count = 1;
        item.items.map(x => {
          if (this.selected.includes(x.id)) {
            var index = this.selected.indexOf(x.id);
            this.selected.splice(index, 1);
          }
        })
      }
    //  this.selected.push(dataReturned.data);
    if(dataReturned.role=='1'){
      this.selected.push(dataReturned.data);
}else{
const idx = this.selected.indexOf(dataReturned.data);
if(idx>=0)
        this.selected.splice(idx, 1);

}

   }
   this.getContent();
 });

 return await modal.present();
 }

 preSavingItem(i,or,item){
 
  if(i.subitem==1){
    this.openContinentalDetails(i,or,item);
  }else{
  if(!this.availableItem(i)){
  }else{
  console.log('item:',i)
  // if(or==2){
  //   item.items.map(x => {
  //     if(this.selected.includes(x.id)){
      
  //       this.selectItem(x,2);
        
        
  //     }else{
        // if(this.allergicIcon(i)){
        //   this.allergicAlert(i.itemdetailsall.alergies,i);
        // }else{
        // this.selectItem(i,1)
        // }
          
    //   }
    // })
  // }else{
    if(this.selected.includes(i.id)){
   
    this.selectItem(i,2,or,item)
    }else{
  if(this.allergicIcon(i)){
    this.allergicAlert(i.itemdetails.alergies,i,or,item);
  }else{
    this.selectItem(i,1,or,item);
  }
}
}
// }
}

}

async selectItem(i,u,or,item){
      
      
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  const uid=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'presave_ordering_item';
  let headers=await this.config.getHeader();
  let body;
  body={
    consumer_id:this.item.user.user_id,
    company_id:cid,
    dining_id:this.menu_id,
    date:moment(this.date).format('YYYY-MM-DD'),
    created_by:uid,
    menu_item_id:i.id,
    serving_time_id:this.serve_time
  }
  if(u==2){
    body.uncheck=1
  }
  // console.log('body:',body,i,u)
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
   console.log('presave:',res,body);
   if(res.status=='success'){
    if (or == 2) {
      //   let count = 1;
        item.items.map(x => {
          if (this.selected.includes(x.id)) {
            var index = this.selected.indexOf(x.id);
            this.selected.splice(index, 1);
          }
        })
      }
     if(u==1){
      if(!this.selected.includes(i.id))
    this.selected.push(i.id)
     }else if(u==2){
       const idx=this.selected.indexOf(i.id);
       this.selected.splice(idx,1);
     }
     
     console.log('se:',this.selected);
     this.getContent();
   }
    
  },error=>{

  })
}

allergicIcon(i){
  let c;
  if(i.itemdetails.alergies){
  i.itemdetails.alergies.map(x => {
    
    if (this.allergies.includes(x.alergy.alergy)) {
      c=true;
      }
      // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
    })
  }
    return c;
}

async allergicAlert(allergies,i,or,item) {
  const modal = await this.modalCntl.create({
    component: AllergicInfoComponent,
    cssClass:'dining-allergicinfo-modal',
    componentProps:{
      consumer:this.item.user.name,
      allergies:allergies
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if(dataReturned.data==2){
      this.selectItem(i,1,or,item);
    }
  });
  return await modal.present();
}

availableItem(i){
  let avl;
  if(i.itemdetails.texture_array&&i.itemdetails.texture_array.length){
 const idx= i.itemdetails.texture_array.map(x=>x.food_texture_id).indexOf(this.texture.food);
 const Idx= i.itemdetails.texture_array.map(x=>x.food_texture_id).indexOf(this.texture.fluid);
 if(idx>=0||Idx>=0){
   avl=true
 }else{
   avl=false
 }
 
 }else{
   avl=true
 }
    
  return avl;
}

async save(){
  if(this.selected.length==0&&this.sel_additional_item.length==0){
    this.presentAlert('Please select an item.');
  }else{
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  const uid=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'confirm_single_serve_time_order';
  let headers=await this.config.getHeader();
  let body={
    consumer_id:this.item.user.user_id,
    dining_id:this.menu_id,
    date:moment(this.date).format('YYYY-MM-DD'),
    created_by:uid,
    serving_time_id:this.serve_time
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
   console.log('confirm:',res,body,url);
   if(res.confirmed==1){
   this.presentAlert('Items added successfully.');
   this.saved=true
   }else{
    this.presentAlert('Order can not be placed.')
   }
    // this.back();
  },error=>{

  })
}
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}

async showLoading() {
      
  const loading = await this.loadingCtrl.create({
    cssClass: 'dining-loading',
    // message: 'Please wait...',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
  
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

async presaveAdditionalItem(item) {
  const cid = await this.storage.get('COMPANY_ID');
  const bid = await this.storage.get('BRANCH');
  const uid = await this.storage.get('USER_ID');
  // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
  let headers=await this.config.getHeader();




  let url = this.config.domain_url + 'presave_additional_ordering_item';

  let body;
  body = {
    consumer_id: this.item.user.user_id,
    dining_id: this.menu_id,
    date: moment(this.date).format('YYYY-MM-DD'),
    created_by: uid,
    item_id: item.item.id,
    serving_time_id: this.serve_time
  }

  if (this.sel_additional_item.includes(item.item.id)) {
    body.uncheck = 1
  }
  console.log('body:', body)
  this.http.post(url, body, { headers }).subscribe((res: any) => {
    console.log('presave:', res, body);
    if (res.status == 'success') {
      if (res.message == 'created') {
        if(!this.sel_additional_item.includes(item.item.id)){
    this.sel_additional_item.push(item.item.id)
     }
      } else {
        if(this.sel_additional_item.includes(item.item.id)){
        const idx=this.sel_additional_item.indexOf(item.item.id);
       this.sel_additional_item.splice(idx,1);
        }
      }
      this.getContent();
    }
   
  }, error => {
    console.log('err:', error)
  })

}

async showAdditionalItemdetails(item) {
  if (item.subitem == 1) {
    this.openContinentalDetails(item,1,item)
  } else {
   
      // console.log('getCom:', this.getComment(item, serving_id), this.setMealSize(item, serving_id))
      const modal = await this.modalCntl.create({
        component: AdditionalItemDetailsComponent,
        cssClass: 'full-width-modal',
        componentProps: {
          item: item,
          date:moment(this.date).format('YYYY-MM-DD'),
        serving_time:this.servingtime,
          menu_id:this.menu_id,
          consumer:this.item.user.user_id,
          serving_time_id:this.serve_time,
          
          // ml_sz: this.setMealSize(item, serving_id),
          // sel_quantity: this.setQuantity(item, serving_id),
          // comment: this.getComment(item, serving_id),
          // options: this.setOptions(item, serving_id)
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data) {
          // this.getDaywiseConsumerDetails();
          // this.sel_additional_item.push(item.item.id);
          this.getContent()
        }
      });

      return await modal.present();
    
  }
}


setMealSize(i) {
  let m, Idx;
 
  if (this.selected.includes(i.id)) {


   
    // const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
    // if (this.preOrderedItems[idx].pre_item_orders.length) {
      Idx = this.preOrderedItems.map(item => item.menu_item_id).indexOf(i.id);
      if (Idx >= 0) {
        if (this.preOrderedItems[Idx].meal_size == 1) {
          m = 'S';
        } else if (this.preOrderedItems[Idx].meal_size == 2) {
          m = 'M'
        } else if (this.preOrderedItems[Idx].meal_size == 3) {
          m = 'L'
        } else if (this.preOrderedItems[Idx].meal_size == 4) {
          m = 'N'
        } else {
          m = null
        }
      } else {
        m = null
      }
    // }
  }
  return m;
}

setQuantity(i) {
  let q, Idx;
  console.log('selectq:',this.selected,'id:',i.id)
  if (this.selected.includes(i.id)) {
  
    // const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
    // if (this.preOrderedItems[idx].pre_item_orders.length) {
      Idx = this.preOrderedItems.map(item => item.menu_item_id).indexOf(i.id);
      if (Idx >= 0) {
        q = this.preOrderedItems[Idx].quantity;
      } else {
        q = null
      }
    // }
    console.log('q:',Idx,this.preOrderedItems,q)

  }
 

  return q;
}
}
