import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnserveComponent } from './unserve.component';

describe('UnserveComponent', () => {
  let component: UnserveComponent;
  let fixture: ComponentFixture<UnserveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnserveComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnserveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
