import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-select-confirmation',
  templateUrl: './select-confirmation.component.html',
  styleUrls: ['./select-confirmation.component.scss'],
})
export class SelectConfirmationComponent implements OnInit {

  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }
}
