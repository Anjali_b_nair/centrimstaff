import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ConsumptionNoteAlertComponent } from '../consumption-note-alert/consumption-note-alert.component';

@Component({
  selector: 'app-serving-consumed',
  templateUrl: './serving-consumed.component.html',
  styleUrls: ['./serving-consumed.component.scss'],
})
export class ServingConsumedComponent implements OnInit {
@Input() res;
@Input() ml_time;
 area:any;
qty:any;
note:any;
items:any;
hide:boolean=false;
  constructor(private mdalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,private storage :Storage,
    private toastCntlr:ToastController,private popCntlr:PopoverController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    console.log('res:',this.res);
    if(this.res.profile){
      if(this.res.profile.serving_area_id==0){
        this.area='room'
      }else{
        if(this.res.profile.area_detail){
          this.area=this.res.profile.area_detail.serving_area
        }
      }
    }
    if(this.res.order.order_items&&this.res.order.order_items.length){
    this.items=this.res.order.order_items[0].quantity+' '+this.res.order.order_items[0].menu_meta.itemdetails.item_name+'('+this.res.order.order_items[0].size.meal_size.substring(0,1)+')'
    this.res.order.order_items.forEach((element,index )=> {
      if(index==0){

      }else{
      this.items=this.items+', '+element.quantity+' '+element.menu_meta.itemdetails.item_name+'('+element.size.meal_size.substring(0,1)+')'
      }
    })
    }
    let add
    if(this.res.additional_order&&this.res.additional_order.length){
      add=this.res.additional_order[0].quantity+' '+this.res.additional_order[0].item.item_name+'('+this.res.additional_order[0].size.meal_size.substring(0,1)+')'
      this.res.additional_order.forEach((element,index )=> {
        if(index==0){
  
        }else{
        add=add+', '+element.quantity+' '+element.item.item_name+'('+element.size.meal_size.substring(0,1)+')'
        }
        })
      }
      if(add){
    if(this.items){
      this.items=this.items+', '+add;
    }else{
      this.items=add
    }
  }
  
  }
async dismiss(){
this.mdalCntl.dismiss();
}
consumed(i){
  // this.mdalCntl.dismiss(i);
  this.qty=i
}
async confirm(){
  if(!this.qty){
    this.presentAlert('Please select the consumed quantity.')
    }else{
        const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
          const uid=await this.storage.get('USER_ID');
          let headers=await this.config.getHeader();
          let s_url = this.config.domain_url + 'serve_orders';

          let url=this.config.domain_url+'consume_orders';

          let items = [];
          let add=[];
          let quantity=[];
          let id=this.res.order.id;
          if(this.res.order.order_items&&this.res.order.order_items.length){
            this.res.order.order_items.forEach(ele => {
            items.push(ele.id)
            quantity[ele.id]=ele.quantity
          })
        }
        if(this.res.additional_order&&this.res.additional_order.length){
          this.res.additional_order.forEach(ele => {
            add.push(ele.id)
            quantity[ele.id]=ele.quantity
          })
        }

        let s_body = {
          company_id: cid,
          served_by: uid,
          order_id: id,
          order_details_id: items,
          order_add_details_id: add,
          itemqunatity:quantity,
          serveqty:this.qty
        }
          let body={
            company_id:cid,
            served_by:uid,
            order_id:id,
            order_details_id:items,
            order_add_details_id:add,
            itemqunatity:quantity,
            serveqty:this.qty,
            carenote:this.note
          }

          // this.http.post(s_url,s_body,{headers}).subscribe((res:any)=>{
          //   console.log('res:',res);
            
          //  },error=>{
          //    console.log(error)
             
          //  })
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log('cres:',res);
           this.mdalCntl.dismiss(1);
          },error=>{
            console.log(error)
            
          })
        }
}



async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
async showAlert(ev){
  const popover = await this.popCntlr.create({
    component: ConsumptionNoteAlertComponent,
    event:ev,
    backdropDismiss:true,
    
   
    
    
  });
  return await popover.present();
}
showTextArea(){
  this.hide=!this.hide;
}
}
