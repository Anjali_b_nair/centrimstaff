import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import { OpenImageComponent } from '../../open-image/open-image.component';
@Component({
  selector: 'app-additional-options',
  templateUrl: './additional-options.component.html',
  styleUrls: ['./additional-options.component.scss'],
})
export class AdditionalOptionsComponent implements OnInit {
@Input() menu:any;
@Input() id:any;
@Input() menu_id:any;
@Input() consumer_id:any;
@Input() date:any;
items:any[]=[];
servingTime:any;
@Input() selected:any;
  constructor(private modalCntrl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private popCntl:PopoverController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('menu:',this.menu,this.id);
    this.items=[];
    this.menu.forEach(element => {
      element.servingtime.map(x => {
        if(this.id==x.servingtime[0].id){
          this.items.push(element.item);
          this.servingTime=x.servingtime[0].servingtime
        }
      })
    });
    console.log('addit:',this.items)
  }

  async dismiss(){
    if(this.selected&&this.selected.length){
      this.modalCntrl.dismiss(1);
    }else{
    this.modalCntrl.dismiss();
    }
  }

  async selectItem(item){
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let headers=await this.config.getHeader();


     
    
      let url=this.config.domain_url+'presave_additional_ordering_item';
      
      let body;
      body={
        consumer_id:this.consumer_id,
        dining_id:this.menu_id,
        date:moment(this.date).format('YYYY-MM-DD'),
        created_by:uid,
        item_id:item.id,
        serving_time_id:this.id
      }
      
    if(this.selected.includes(item.id)){
      body.uncheck=1
    }
      console.log('body:',body)
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
       console.log('presave:',res,body);
       if(res.status=='success'){
         if(res.message=='created'){
        this.selected.push(item.id)
         }else{
           var idx=this.selected.indexOf(item.id);
           this.selected.splice(idx,1)
         }
       }
        
      },error=>{
        console.log('err:',error)
      })
    
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
}
