import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-dietary-profile',
  templateUrl: './dietary-profile.component.html',
  styleUrls: ['./dietary-profile.component.scss'],
})
export class DietaryProfileComponent implements OnInit {

  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }
}
