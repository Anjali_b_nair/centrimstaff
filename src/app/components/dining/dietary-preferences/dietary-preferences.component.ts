import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import moment from 'moment-timezone';
import { Storage } from '@ionic/storage-angular';
@Component({
  selector: 'app-dietary-preferences',
  templateUrl: './dietary-preferences.component.html',
  styleUrls: ['./dietary-preferences.component.scss'],
})
export class DietaryPreferencesComponent implements OnInit {
@Input()details:any;
last_updated_at:any;
  constructor(private modalCntrl:ModalController,private storage:Storage) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    const tz=await this.storage.get('TIMEZONE');
    let pre_date=moment().subtract(7,'days').format('DD/MM/YYYY');
    let date=moment.utc(this.details.last_updated_at).tz(tz).format('DD/MM/YYYY');
    console.log('pref:',pre_date,date,moment(pre_date).isSameOrBefore(moment(date)));
    
    if(moment(pre_date).isSameOrBefore(moment(date))){
    this.last_updated_at=moment.utc(this.details.last_updated_at).tz(tz).format('DD/MM/YYYY, hh:mm A')
    }else{
      this.last_updated_at=undefined
    }
  }
  async dismiss(){
    this.modalCntrl.dismiss()
  }

}
