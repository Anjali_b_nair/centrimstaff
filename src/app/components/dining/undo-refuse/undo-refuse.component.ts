import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-undo-refuse',
  templateUrl: './undo-refuse.component.html',
  styleUrls: ['./undo-refuse.component.scss'],
})
export class UndoRefuseComponent implements OnInit {
  @Input() order_id:any;
  constructor(private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  async undo(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'unrefused';
    let headers=await this.config.getHeader();
    let body={
      
      
      served_by:uid,
      
     
      order_id:this.order_id
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      // this.presentAlert('Leave marked successfully.');
      this.popCntl.dismiss(1);
    },error=>{

    })
  }
}
