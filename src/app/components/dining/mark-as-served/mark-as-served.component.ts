import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { MarkAsServedConfirmComponent } from '../mark-as-served-confirm/mark-as-served-confirm.component';

@Component({
  selector: 'app-mark-as-served',
  templateUrl: './mark-as-served.component.html',
  styleUrls: ['./mark-as-served.component.scss'],
})
export class MarkAsServedComponent implements OnInit {

  constructor(private popCntrl:PopoverController,private modalCntl:ModalController) { }

  ngOnInit() {}

  async mark(){
    
    const modal = await this.modalCntl.create({
      component: MarkAsServedConfirmComponent,
      // cssClass: 'full-width-modal'
      
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {
        this.popCntrl.dismiss(1);
      }else{
        this.popCntrl.dismiss();
      }
      
    });
    return await modal.present();
  }

}
