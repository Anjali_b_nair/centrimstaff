import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { AdditionalOptionsComponent } from '../additional-options/additional-options.component';
import moment from 'moment';
import { DiningAreaComponent } from '../dining-area/dining-area.component';
@Component({
  selector: 'app-menu-options',
  templateUrl: './menu-options.component.html',
  styleUrls: ['./menu-options.component.scss'],
})
export class MenuOptionsComponent implements OnInit {
@Input() menu:any;
// @Input() serving_id:any;
@Input() date:any;
// @Input() menu_id:any;
@Input() consumer_id:any;
// @Input() servingTime:any;
@Input() is_skipped:any;
@Input() serve_area:any;
// @Input() sel_area:any;
@Input() additional:any;

  constructor(private modalCntrl:ModalController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}


  ionViewWillEnter(){
    console.log('m:',this.menu)
  }
//   async additional(){
//     const modal = await this.modalCntrl.create({
//       component: AdditionalOptionsComponent,
//       cssClass:'dining-additional-fd-opt-modal',
//       componentProps:{
//         menu:this.menu,
//         id:this.serving_id,
//         consumer_id:this.consumer_id,
//         date:this.date,
//         menu_id:this.menu_id,
//         selected:this.sel_additional_item
//       }
//     });
//     modal.onDidDismiss().then((dataReturned) => {
//       if(dataReturned.data==1){
//         this.popCntl.dismiss(1,null,'menu_options');
//       }
//     });
//     return await modal.present();
//   }

//   async skip(){
//     const cid=await this.storage.get('COMPANY_ID');
//     const bid=await this.storage.get('BRANCH');
//     const uid=await this.storage.get('USER_ID');
//     let url=this.config.domain_url+'skipping_day_wise_single_meal';
//     let headers=await this.config.getHeader();
//     let body={
//       consumer_id:this.consumer_id,
//       company_id:cid,
//       dining_id:this.menu_id,
//       meal_time_id:this.serving_id,
//       date:moment(this.date).format('YYYY-MM-DD'),
//       created_by:uid
//     }
//     this.http.post(url,body,{headers}).subscribe((res:any)=>{
//       // if(res.skipped=='Skipped')
//       this.presentAlert(res.skipped+' '+this.servingTime);
//       this.popCntl.dismiss(1);
//     },error=>{

//     })
//   }
//   async presentAlert(mes) {
//     const alert = await this.toastCntlr.create({
//       message: mes,
//       cssClass:'toastStyle',
//       duration: 3000      
//     });
//     alert.present(); //update
//   }

//  async chooseServingArea(ev){
//     const popover = await this.popCntl.create({
//       component: DiningAreaComponent,
//       event: ev,
//       backdropDismiss: true,
//       cssClass: 'dining-more-options-popover',
//       componentProps: {
//         serving_id:this.serving_id,
//         consumer_id:this.consumer_id,
//         date:this.date,
//         menu_id:this.menu_id,
//         serve_area:this.serve_area,
//         sel_area:this.sel_area
//       },
//     });
//       popover.onDidDismiss().then((dataReturned) => {
//         if(dataReturned.data==1){
//           this.popCntl.dismiss(1,null,'menu_options');
//         }
//       });

    
//     return await popover.present();
//   }
//   show(i){
//     if(i==1){
//       this.menu.show=true;
//     }else{
//       this.menu.show=false;
//     }
    
//     this.popCntl.dismiss(2,this.menu)
//   }



  async skip(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'skipping_day_wise_single_meal';
    let headers=await this.config.getHeader();
    let body={
      consumer_id:this.consumer_id,
      company_id:cid,
      dining_id:this.menu.menu_id,
      meal_time_id:this.menu.serving_time_id,
      date:moment(this.date).format('YYYY-MM-DD'),
      created_by:uid
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      // if(res.skipped=='Skipped')
      this.presentAlert(res.skipped+' '+this.menu.servingtime);
      this.popCntl.dismiss(1);
    },error=>{

    })
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

 async chooseServingArea(ev){
    const popover = await this.popCntl.create({
      component: DiningAreaComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        serving_id:this.menu.serving_time_id,
        consumer_id:this.consumer_id,
        date:this.date,
        menu_id:this.menu.menu_id,
        // serve_area:this.serve_area,
        sel_area:this.menu.changed_serving_area_id
      },
    });
      popover.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data==1){
          this.popCntl.dismiss(1,null,'menu_options');
        }
      });

    
    return await popover.present();
  }
  show(i){
    if(i==1){
      this.additional.show=true;
    }else{
      this.additional.show=false;
    }
    
    this.popCntl.dismiss(2,this.additional)
  }



}
