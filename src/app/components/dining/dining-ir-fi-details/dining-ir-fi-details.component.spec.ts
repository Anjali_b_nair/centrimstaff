import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningIrFiDetailsComponent } from './dining-ir-fi-details.component';

describe('DiningIrFiDetailsComponent', () => {
  let component: DiningIrFiDetailsComponent;
  let fixture: ComponentFixture<DiningIrFiDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningIrFiDetailsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningIrFiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
