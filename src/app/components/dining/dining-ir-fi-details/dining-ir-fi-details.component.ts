import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-dining-ir-fi-details',
  templateUrl: './dining-ir-fi-details.component.html',
  styleUrls: ['./dining-ir-fi-details.component.scss'],
})
export class DiningIrFiDetailsComponent implements OnInit {
@Input() item:any;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

}
