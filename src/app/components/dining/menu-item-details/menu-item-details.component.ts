import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import { OpenImageComponent } from '../../open-image/open-image.component';
@Component({
  selector: 'app-menu-item-details',
  templateUrl: './menu-item-details.component.html',
  styleUrls: ['./menu-item-details.component.scss'],
})
export class MenuItemDetailsComponent implements OnInit {

  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private popCntl:PopoverController) { }
@Input()item:any;
@Input()date:any;
@Input()serving_time:any;
@Input()menu_id:any;
@Input()consumer:any;
@Input()serving_time_id:any;
quantity:any;
comments:any;
@Input()ml_sz:any;
@Input()sel_quantity:any;
@Input()comment:any;
ml_sz_id:any;
optionData: { OptionId: number, Value: number }[];
selectedVal:any[]=[];
@Input()flag:any;
@Input()options:any;
menu:any;
  ngOnInit() {}


  async ionViewWillEnter(){
    console.log('item:',this.item,this.comments,this.flag);
    this.optionData = [{ OptionId: 0, Value: 0 }];
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'ordering_item_details';
      let headers=await this.config.getHeader();
      let body;
      body={
        consumer_id:this.consumer,
        company_id:cid,
        item_id:this.item.menu_item_id,
        date:moment(this.date).format('YYYY-MM-DD'),
        
        menu_item_id:this.item.id,
        
        meal_time_id:this.serving_time_id
      }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      this.menu=res.data.item
      
    if(this.menu.servingquantity&&this.menu.servingquantity.length>0){
this.ml_sz_id=this.menu.servingquantity[0].meal_size.id.toString();
    }
    console.log('res:',this.menu,'size:',this.ml_sz_id);
    
  // if(this.ml_sz=='S'){
  //   this.ml_sz_id='1'
  // }else if(this.ml_sz=='M'){
  //   this.ml_sz_id='2'
  // }else if(this.ml_sz=='L'){
  //   this.ml_sz_id='3'
  // }else if(this.ml_sz=='N'){
  //   this.ml_sz_id='4'
  // }
  if(this.flag!=1){
    console.log('flag2')
    if(this.menu&&this.menu.selected_data){
      console.log('secteddata:',this.menu.selected_data.comment,this.menu.selected_data.quantity)
      if(this.menu.selected_data.items_selected_options&&this.menu.selected_data.items_selected_options.length){
        // this.selectedVal.splice(0,1);
      this.menu.selected_data.items_selected_options.map(x=>{
        const targetIdx = this.selectedVal.map(item => item.option).indexOf(x.menu_item_option.id);
        if(targetIdx<0)
        this.selectedVal.push({option:x.menu_item_option.id,val:x.menu_item_value.id})
      })
    }else{
        if(this.menu&&this.menu.options&&this.menu.options){
          // this.selectedVal.splice(0,1);
          this.menu.options.map(x=>{
            x.valuess.map(y=>{
              if(y.default_value==1){
              this.selectedVal.push({option:x.id,val:y.id})
              }
            })
            
          })
        }
}
if(this.menu.selected_data.comment){
  this.comments=this.menu.selected_data.comment
}
if(this.menu.selected_data.quantity){
  this.quantity=parseInt(this.menu.selected_data.quantity)
}else{
this.quantity=1;
}
if(this.menu.selected_data.meal_size){
  this.ml_sz_id=this.menu.selected_data.meal_size.toString();
}else{

}
}else{
  this.quantity=1;
  if(this.menu&&this.menu.options&&this.menu.options){
    // this.selectedVal.splice(0,1);
    this.menu.options.map(x=>{
      x.valuess.map(y=>{
        if(y.default_value==1){
        this.selectedVal.push({option:x.id,val:y.id})
        }
      })
      
    })
  }
}
  }

if(this.flag==1){
  // this.menu=this.item
// }

// }else{
  // if(this.item.itemdetails.options&&this.item.itemdetails.options){
  //   // this.selectedVal.splice(0,1);
  //   this.item.itemdetails.options.map(x=>{
  //     x.valuess.map(y=>{
  //       if(y.default_value==1){
  //       this.selectedVal.push({option:x.option.id,val:y.valuename.id})
  //       }
  //     })
      
  //   })
  // }

  if(this.menu&&this.menu.selected_data){
   
    if(this.menu.selected_data.items_selected_options&&this.menu.selected_data.items_selected_options.length){
      // this.selectedVal.splice(0,1);
    this.menu.selected_data.items_selected_options.map(x=>{
      const targetIdx = this.selectedVal.map(item => item.option).indexOf(x.menu_item_option.id);
        if(targetIdx<0)
      this.selectedVal.push({option:x.menu_item_option.id,val:x.menu_item_value.id})
    })
  }else{
      if(this.menu&&this.menu.options&&this.menu.options){
        // this.selectedVal.splice(0,1);
        this.menu.options.map(x=>{
          x.valuess.map(y=>{
            if(y.default_value==1){
            this.selectedVal.push({option:x.id,val:y.id})
            }
          })
          
        })
      }
}
if(this.menu.selected_data.comment){
this.comments=this.menu.selected_data.comment
}
if(this.menu.selected_data.quantity){
this.quantity=parseInt(this.menu.selected_data.quantity)
}else{
this.quantity=1;
}
if(this.menu.selected_data.meal_size){
this.ml_sz_id=this.menu.selected_data.meal_size.toString();
}else{

}
}else{
this.quantity=1;
if(this.menu&&this.menu.options&&this.menu.options){
  // this.selectedVal.splice(0,1);
  this.menu.options.map(x=>{
    x.valuess.map(y=>{
      if(y.default_value==1){
      this.selectedVal.push({option:x.id,val:y.id})
      }
    })
    
  })
}
}
}
    })
    console.log('selOpt:',this.selectedVal)
  }
  dismiss(i){
    this.modalCntl.dismiss()
  }
  increment(){
    if(this.quantity<5){
    this.quantity++;
    }
  }
  decrement(){
    if(this.quantity>1){
    this.quantity--;
    }
  }
  async apply(){
    let comment,options,values;
    if(this.comments==undefined||this.comments==''||this.comments=='undefined'||this.comments=='null'){
      comment=null
    }else{
      comment=this.comments
    }

    
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'update_ordering_items_option';
      let headers=await this.config.getHeader();
      let body;
      body={
        consumer_id:this.consumer,
        company_id:cid,
        dining_id:this.menu_id,
        date:moment(this.date).format('YYYY-MM-DD'),
        created_by:uid,
        menu_item_id:this.item.id,
        quantity:this.quantity,
        size:this.ml_sz_id,
        comment:comment,
        serving_time_id:this.serving_time_id
      }
      // this.optionData.splice(0, 1);
      // console.log('optionData:',this.optionData)
      // if(this.optionData&&this.optionData.length){
      //   for (let data of this.optionData) {
      //     //  
      //     console.log('loop:',data)
      //     if (options == '' || options == undefined){
      //       options = data.OptionId;
      //     }
      //     if (options == data.OptionId) {
      //       console.log("firstOpt");
  
      //     } else {
            
      //       options += "*" + data.OptionId;
      //     }
      //     if (values == '' || values == undefined){
      //       values = data.Value;
      //     }
      //     if (values == data.Value) {
      //       console.log("firstOpt");
  
      //     } else {
            
      //       values += "*" + data.Value;
      //     }
      //   }
      //   body.options=options;
      //   body.option_values=values;
      // }


      // this.optionData.splice(0, 1);
      console.log('optionData:',this.selectedVal)
      if(this.selectedVal&&this.selectedVal.length){
        for (let data of this.selectedVal) {
          //  
          console.log('loop:',data)
          if (options == '' || options == undefined){
            options = data.option;
          }
          if (options == data.option) {
            console.log("firstOpt");
  
          } else {
            
            options += "*" + data.option;
          }
          if (values == '' || values == undefined){
            values = data.val;
          }
          if (values == data.val) {
            console.log("firstOpt");
  
          } else {
            
            values += "*" + data.val;
          }
        }
        body.options=options;
        body.option_values=values;
      }

      console.log("body:",body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
       console.log('presave:',res,body);
       if(res.status=='success'){
        this.modalCntl.dismiss(this.item.id);
       }
        
      },error=>{
        console.log('err:',error)
      })
  }

  setMealSize(id){
    this.ml_sz_id=id
  }
  selectOptions(opt,val){
    if(this.optionData&&this.optionData.length){
    const targetIdx = this.optionData.map(item => item.OptionId).indexOf(opt);
    if (targetIdx >= 0) {

      this.optionData[targetIdx].Value = val;
      
    }else{
      this.optionData.push({ OptionId: opt, Value: val });
    }
  }else{
    this.optionData.push({ OptionId: opt, Value: val });
    
  }
  console.log('sel:',this.selectedVal,opt,val)
  // if(this.selectedVal&&this.selectedVal.length&&this.selectedVal[0].option==0){
  //   this.selectedVal.splice(0,1);
  //   this.selectedVal.push({option:opt,val:val})
  // }
  
  // if(this.selectedVal&&this.selectedVal.length&&this.selectedVal[0].option!=0){
    
  const targetIdx = this.selectedVal.map(item => item.option).indexOf(opt);

  // if(targetIdx>=0){
    
    this.selectedVal[targetIdx].val = val;
  // }else{
  //   this.selectedVal.push({option:opt,val:val})
  // }
    
  // }
  }

  showSelectedOption(val){
    if(this.selectedVal&&this.selectedVal.length){
    this.selectedVal.map(x=>{
      if(x.val==val){
        return true;
      }
    })
    }
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
}
