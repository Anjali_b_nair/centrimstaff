import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-serve-confirmation',
  templateUrl: './serve-confirmation.component.html',
  styleUrls: ['./serve-confirmation.component.scss'],
})
export class ServeConfirmationComponent implements OnInit {

  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }
  confirm(){
    this.modalCntrl.dismiss(1);
  }
}
