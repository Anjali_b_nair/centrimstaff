import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import moment from 'moment';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { AllergicInfoComponent } from '../allergic-info/allergic-info.component';
import { ContinentalItemDetailsComponent } from '../continental-item-details/continental-item-details.component';
import { OpenImageComponent } from '../../open-image/open-image.component';
@Component({
  selector: 'app-continental-item',
  templateUrl: './continental-item.component.html',
  styleUrls: ['./continental-item.component.scss'],
})
export class ContinentalItemComponent implements OnInit {
  @Input()menu:any;
  @Input()date:any;
  // @Input()serving_time:any;
  @Input()menu_id:any;
  @Input()consumer:any;
  @Input()con_name:any;
  @Input()serving_time_id:any;
  @Input()fdtexture:any;
  @Input()fltexture:any;
  details:any=[];
  serving_time:any;
  @Input()preOrderedItems:any;
  @Input()allergies:any;
  @Input()flag;
  @Input()edit:any;
  @Input()skipped:any;
  selected:any=[];
  items:any=[];
  // menu_id:any;
  item_id:any;
  parent_id:any;
  selected_items:any=[];
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,private modalCntl:ModalController,
    private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}

//   async ionViewWillEnter(){
//     this.selected=[];
//     this.items=[];
//     console.log('con:',this.menu,this.edit,this.fltexture);
//     const cid=await this.storage.get('COMPANY_ID');
//       const bid=await this.storage.get('BRANCH');
     
//       let url=this.config.domain_url+'continental_breakfast_via_menu_detail_id';
//       let headers=await this.config.getHeader();
//       let body={
//         consumer_id:this.consumer,
//         menu_item_id:this.menu.id,
//         date:moment(this.date).format('YYYY-MM-DD'),
//         meal_time_id:this.serving_time_id
//         // consumer_id:45,
//         // menu_item_id:8812,
//         // date:'2022-03-10',
//         // meal_time_id:3
//       }
//       this.http.post(url,body,{headers}).subscribe((res:any)=>{
//        console.log('confirm:',res,body);
//        this.serving_time=res.data.meal_time;
//        this.details=res.data.menu_item.sub_items;
//        this.item_id=res.data.menu_item.id;
//        this.parent_id=res.data.menu_item.menu_item_id;

//        this.details.forEach(element => {
//          if(element.is_ordered){
//           //  if(this.selected&&this.selected.length){
//           //   const idx=this.selected.indexOf(element.itemdetails.id);
//           //   const Idx=this.items.map(x=>x.menu_item_id).indexOf(element.itemdetails.id);
//           //   if(idx>=0){
//           //     this.selected.splice(idx,1);
//           //     this.items.splice(Idx,1);
//           //   }else{
//               this.selected.push(element.itemdetails.id);
//               let options,values=[];
//               if(element.selected_data.items_selected_options&&element.selected_data.items_selected_options.length){
//                 element.selected_data.items_selected_options.map(x=>{
//                   x.valuess.map(y=>{
                   
//                     options.push(x.option.id)
//                     values.push(y.valuename.id)
                    
//                   })
                  
//                 })
//               if(options.length==0){
//                 options=null;
//                 values=null;
//               } 
              
//             }
//             this.items.push({menu_item_id:element.id,quantity:element.selected_data.quantity,size:element.selected_data.meal_size,comment:element.selected_data.comment,options:options,option_values:values})
//           //  }else{
//           //   this.selected.push(element.itemdetails.id)
//           //  }
//            console.log('sel:',this.items,this.selected)
//          }
//        });
//       },error=>{
        
//       })

//   }
//   availableItem(i){
//     let avl;
//     if(i.itemdetails.foodtexture&&i.itemdetails.foodtexture.length){
//    const idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fdtexture);
//    const Idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fltexture);
//    if(idx>=0||Idx>=0){
//      avl=true
//    }else{
//      avl=false
//    }
   
//    }else{
//      avl=true
//    }
      
//     return avl;
//   }

//   async dismiss(i){
//     if(i==1&&!this.edit){
//       this.modalCntl.dismiss(this.item_id)
//     }else{
//     await this.modalCntl.dismiss()
//     }
//   }
//   // incompatable(item){
//   //   let inc=false;
//   //   const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(this.serving_time_id);
//   //   const Idx=this.preOrderedItems[idx].all_coloumns.map(x=>x.id).indexOf(item.id);
//   //   if(Idx<0){
//   //     inc=true
//   //   }
//   //   return inc;
   

//   // }

//   allergicIcon(i){
//     let c;
//     if(i.itemdetails.alergies){
//     i.itemdetails.alergies.map(x => {
      
//       if (this.allergies.includes(x.alergy.alergy)) {
//         c=true;
//         }
//         // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
//       })
//     }
//       return c;
//   }

//   selectoptions(item){
//     if(!this.availableItem(item)){
//       console.log('not available')
//     }else{
      
//         this.ItemDetails(item)
//       }
//   }
//   selectitem(item){
//     console.log('click worked')
//     if(!this.edit&&!this.skipped){
//     const idx=this.selected.indexOf(item.itemdetails.id);
//     const Idx=this.items.map(x=>x.menu_item_id).indexOf(item.itemdetails.id);
//     if(idx>=0){
      
//       this.selected.splice(idx,1);
      
//       this.items.splice(Idx,1);
//       this.save(1,item);
//       console.log('selected:',this.selected,idx,Idx)
//     }else{
//     if(!this.availableItem(item)){
//       console.log('not available')
//     }else{
//       if(this.allergicIcon(item)){
//         console.log('allergic item')
//         this.allergicAlert(item.itemdetails.alergies,item);
//       }else{
//         console.log('open details')
//         // this.ItemDetails(item)
//         this.items.push({menu_item_id:item.id,quantity:1,size:item.itemdetails.servingquantity[0].meal_size.id,comment:null,options:null,option_values:null});
//         this.save(2,item)
//       }
//     }
//   }
// }
//   }
//   async allergicAlert(allergies,item) {
//     const modal = await this.modalCntl.create({
//       component: AllergicInfoComponent,
//       cssClass:'dining-allergicinfo-modal',
//       componentProps:{
//         consumer:this.con_name,
//         allergies:allergies
//       }
//     });
//     modal.onDidDismiss().then((dataReturned) => {
//       if(dataReturned.data==2){
//         // this.ItemDetails(item);
//         this.items.push({menu_item_id:item.id,quantity:1,size:item.itemdetails.servingquantity[0].meal_size.id,comment:null,options:null,option_values:null});
//         this.save(2,item)
//       }
//     });
//     return await modal.present();
//   }

//   async ItemDetails(item) {
//     const modal = await this.modalCntl.create({
//       component: ContinentalItemDetailsComponent,
//       cssClass:'full-width-modal',
//       componentProps:{
//         item:item,
//         date:this.date,
//         servingtime:this.serving_time
//       }
//     });
//     modal.onDidDismiss().then((dataReturned) => {
//       if(dataReturned.data){
//         console.log('data:',dataReturned.data)
//         this.items.push(dataReturned.data)
//         // this.items.push({menu_item_id:item.itemdetails.id,quantity:daquantity,size:size,comment:comment,options:options,option_values:values});
//         this.selected.push(item.itemdetails.id);
//         this.save(2,item);
//       }
//     });
//     return await modal.present();
//   }

//   async save(i,item){
//     // if(this.selected&&this.selected.length){
//     const cid=await this.storage.get('COMPANY_ID');
//     const bid=await this.storage.get('BRANCH');
//     const uid=await this.storage.get('USER_ID');
//     let url=this.config.domain_url+'order_continental_breakfast';
//     let headers=await this.config.getHeader();
//     let body={
//       created_by:uid,
//       consumer_id:this.consumer,
//       dining_id:this.menu_id,
//       date:moment(this.date).format('YYYY-MM-DD'),
//       serving_time_id:this.serving_time_id,
//       parent_menu_item_id:this.item_id,
//       items:this.items
//     }
//     console.log('body:',body);
    
//     this.http.post(url,body,{headers}).subscribe((res:any)=>{
//      console.log('confirm:',res);
//      if(i==1){
//       // const idx=this.selected.indexOf(item.itemdetails.id);
//       // const Idx=this.items.map(x=>x.menu_item_id).indexOf(item.itemdetails.id);
//       // if(idx>=0){
        
//       //   this.selected.splice(idx,1);
        
//       //   this.items.splice(Idx,1);
//       // }
//      }else{
//       this.selected.push(item.itemdetails.id);
//      }
//     //  this.modalCntl.dismiss(this.item_id)
//     },error=>{
      
//     })
//   // }else{
//   //   this.presentAlert('Please select any item.')
//   // }
//   }

//   async presentAlert(mes) {
//     const alert = await this.toastCntlr.create({
//       message: mes,
//       cssClass:'toastStyle',
//       duration: 3000      
//     });
//     alert.present(); //update
//   }

//   setMealSize(i){
//     let m,Idx;
//     if(i.selected_data){
   

      
    
//           if (i.selected_data.size.id==1) {
//                           m='S';
//                         }else if(i.selected_data.size.id==2){
//                           m='M'
//                         }else if(i.selected_data.size.id==3){
//                           m='L'
//                         }else if(i.selected_data.size.id==4){
//                           m='N'
//                         }else{
//                           m=null
//                         }
//                  }else{
//                    m=null
//                  }
               
//     return m;
//   }

//   setQuantity(i){
//     let q,Idx;
    
//     if(i.selected_data){
     
//            q=i.selected_data.quantity;
//          }else{
//            q=null
//          }
//     return q;
//   }






  async ionViewWillEnter(){
    this.selected=[];
    this.items=[];
    this.selected_items=[];
    console.log('con:',this.menu,this.edit,this.fltexture);
    
    this.getDetails();
  }

  async getDetails(){
  this.items=[];
  this.selected_items=[];
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
   
    let url=this.config.domain_url+'continental_breakfast_via_menu_detail_id';
    let headers=await this.config.getHeader();
    let body={
      consumer_id:this.consumer,
      menu_item_id:this.menu.id,
      date:moment(this.date).format('YYYY-MM-DD'),
      meal_time_id:this.serving_time_id
      // consumer_id:45,
      // menu_item_id:8812,
      // date:'2022-03-10',
      // meal_time_id:3
    }
    console.log('ccbody:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     console.log('confirm:',res);
     this.serving_time=res.data.meal_time;
     this.details=res.data.menu_item.sub_items;
     this.item_id=res.data.menu_item.id;
     this.parent_id=res.data.menu_item.menu_item_id;

     this.details.forEach(element => {
       if(element.is_ordered){
        //  if(this.selected&&this.selected.length){
        //   const idx=this.selected.indexOf(element.itemdetails.id);
        //   const Idx=this.items.map(x=>x.menu_item_id).indexOf(element.itemdetails.id);
        //   if(idx>=0){
        //     this.selected.splice(idx,1);
        //     this.items.splice(Idx,1);
        //   }else{
          if(!this.selected.includes(element.itemdetails.id)){
            this.selected.push(element.itemdetails.id);
          }
            let options=[];
            let values=[];
            let opt,val;
            if(element.selected_data.items_selected_options&&element.selected_data.items_selected_options.length){
              element.selected_data.items_selected_options.map(x=>{
                // x.valuess.map(y=>{
                 
                //   options.push(x.option.id)
                //   values.push(y.valuename.id)
                  
                // })
                options.push(x.option_id)
                values.push(x.option_value_id)
                
              })
            if(options.length==0){
              options=null;
              values=null;
            }  else{
              opt=options[0];
              for(let data of options){
                if(opt==options[0]){

                }else{
                opt += "*" + data;
                }
              }
                val=values[0];
                for(let data of values){
                  if(val==values[0]){
  
                  }else{
                  val += "*" + data;
                  }
                }

              
            }
            
          }
          if(this.selected.includes(element.itemdetails.id)){
            this.selected_items.push(element.itemdetails.id);
          this.items.push({menu_item_id:element.id,quantity:element.selected_data.quantity,size:element.selected_data.meal_size,comment:element.selected_data.comment,options:options,option_values:values})
          }
          //  }else{
        //   this.selected.push(element.itemdetails.id)
        //  }
         console.log('sel:',this.items,this.selected)
       }
     });
    },error=>{
      
    })
  }
  availableItem(i){
    let avl;
    if(i.itemdetails.foodtexture&&i.itemdetails.foodtexture.length){
   const idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fdtexture);
   const Idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fltexture);
   if(idx>=0||Idx>=0){
     avl=true
   }else{
     avl=false
   }
   
   }else{
     avl=true
   }
      
    return avl;
  }

  async dismiss(i){
    if(i==1&&!this.edit){
      if(this.selected&&this.selected.length>=1){
      this.modalCntl.dismiss(this.item_id,'1')
      }else{
        this.modalCntl.dismiss(this.item_id,'2')
      }
    }else{
    await this.modalCntl.dismiss()
    }
  }
  // incompatable(item){
  //   let inc=false;
  //   const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(this.serving_time_id);
  //   const Idx=this.preOrderedItems[idx].all_coloumns.map(x=>x.id).indexOf(item.id);
  //   if(Idx<0){
  //     inc=true
  //   }
  //   return inc;
   

  // }

  allergicIcon(i){
    let c;
    if(i.itemdetails.alergies){
    i.itemdetails.alergies.map(x => {
      
      if (this.allergies.includes(x.alergy.alergy)) {
        c=true;
        }
        // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
      })
    }
      return c;
  }

  selectoptions(item){
    if(item.is_non_texture||item.is_non_pckg){
      console.log('not available')
    }else{
      
        this.ItemDetails(item)
      }
  }
  selectitem(item){
    console.log('click worked:',item)
    if(!this.edit&&!this.skipped){
    const idx=this.selected.indexOf(item.itemdetails.id);
    const id=this.selected_items.indexOf(item.itemdetails.id);
    const Idx=this.items.map(x=>x.menu_item_id).indexOf(item.id);
    if(idx>=0){
      
      this.selected.splice(idx,1);
      this.selected_items.splice(id,1);
      this.items.splice(Idx,1);
      this.save(1,item);
      console.log('selected:',this.selected,idx,Idx)
    }else{
    if(item.is_non_texture||item.is_non_pckg||item.visibility==0){
      console.log('not available')
    }else{
      if(item.is_allergic||item.is_dietic){
        console.log('allergic item')
        this.allergicAlert(item.itemdetails.alergies,item);
      }else{
        console.log('open details')
        // this.ItemDetails(item);
        let sz;
        if(item.itemdetails.servingquantity&&item.itemdetails.servingquantity.length){
          sz=item.itemdetails.servingquantity[0].meal_size.id
        }else{
          sz=null
        }
        let opt=null;
        this.selected_items.push(item.itemdetails.id)
        this.items.push({menu_item_id:item.id,quantity:1,size:sz,comment:null,options:opt,option_values:null});
        
        this.save(2,item)
      }
    }
  }
}
  }
  async allergicAlert(allergies,item) {
    const modal = await this.modalCntl.create({
      component: AllergicInfoComponent,
      cssClass:'dining-allergicinfo-modal',
      componentProps:{
        consumer:this.con_name,
        allergies:allergies
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data==2){
        // this.ItemDetails(item);
        if(this.selected.includes(item.itemdetails.id)){
          let sz;
        if(item.itemdetails.servingquantity&&item.itemdetails.servingquantity.length){
          sz=item.itemdetails.servingquantity[0].meal_size.id
        }else{
          sz=null
        }
        this.selected_items.push(item.itemdetails.id)
        this.items.push({menu_item_id:item.id,quantity:1,size:sz,comment:null,options:null,option_values:null});
      }
        this.save(2,item)
      }
    });
    return await modal.present();
  }

  async ItemDetails(item) {
    const modal = await this.modalCntl.create({
      component: ContinentalItemDetailsComponent,
      cssClass:'full-width-modal',
      componentProps:{
        item:item,
        date:this.date,
        servingtime:this.serving_time
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
        console.log('data:',dataReturned.data)
        // if(!this.selected.includes(item.itemdetails.id)){
        // this.items.push(dataReturned.data)
        // }
        const idx=this.items.map(x=>x.menu_item_id).indexOf(dataReturned.data.menu_item_id);
        if(idx>=0){
          this.items[idx]=dataReturned.data
        }else{
          this.items.push(dataReturned.data);
          this.selected_items.push(item.itemdetails.id)
        }
          
        // this.items.push({menu_item_id:item.itemdetails.id,quantity:daquantity,size:size,comment:comment,options:options,option_values:values});
        // if(!this.selected.includes(item.itemdetails.id)){
        // this.selected.push(item.itemdetails.id);
        // }
        this.save(2,item);
        
      }
    });
    return await modal.present();
  }

  async save(i,item){
    // if(this.selected&&this.selected.length){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'order_continental_breakfast';
    let headers=await this.config.getHeader();
    let body;
    body={
      created_by:uid,
      consumer_id:this.consumer,
      dining_id:this.menu_id,
      date:moment(this.date).format('YYYY-MM-DD'),
      serving_time_id:this.serving_time_id,
      parent_menu_item_id:this.item_id,
      items:this.items,
      selected_items:this.selected_items
    }
    
    console.log('body:',body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     console.log('confirm:',res);
     if(i==1){
      const idx=this.selected.indexOf(item.itemdetails.id);
      const Idx=this.items.map(x=>x.menu_item_id).indexOf(item.id);
      if(idx>=0){
        
        this.selected.splice(idx,1);
        
        this.items.splice(Idx,1);
      }
     }else{
       if(!this.selected.includes(item.itemdetails.id))
      this.selected.push(item.itemdetails.id);
     }
    //  setTimeout(()=>{
     this.getDetails();
    // },500)
    //  this.modalCntl.dismiss(this.item_id)
    },error=>{
      
    })
  // }else{
  //   this.presentAlert('Please select any item.')
  // }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

  setMealSize(i){
    let m,Idx;
    if(i.selected_data){
   

      
    
          if (i.selected_data.size.id==1) {
                          m='S';
                        }else if(i.selected_data.size.id==2){
                          m='M'
                        }else if(i.selected_data.size.id==3){
                          m='L'
                        }else if(i.selected_data.size.id==4){
                          m='N'
                        }else{
                          m=null
                        }
                 }else{
                   m=null
                 }
               
    return m;
  }

  setQuantity(i){
    let q,Idx;
    
    if(i.selected_data){
     
           q=i.selected_data.quantity;
         }else{
           q=null
         }
    return q;
  }



  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }

}
