import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { OpenImageComponent } from '../../open-image/open-image.component';

@Component({
  selector: 'app-additional-item-details',
  templateUrl: './additional-item-details.component.html',
  styleUrls: ['./additional-item-details.component.scss'],
})
export class AdditionalItemDetailsComponent implements OnInit {

  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private popCntl:PopoverController) { }
@Input()item:any;
@Input()date:any;
@Input()serving_time:any;
@Input()menu_id:any;
@Input()consumer:any;
@Input()serving_time_id:any;
quantity:any;
comments:any;
@Input()ml_sz:any;
@Input()sel_quantity:any;
@Input()comment:any;
ml_sz_id:any;
optionData: { OptionId: number, Value: number }[];
selectedVal:any[]=[];
// @Input()flag:any;
@Input()options:any;
menu:any;
  ngOnInit() {}


  async ionViewWillEnter(){
    console.log('item:',this.item,this.comments);
    this.optionData = [{ OptionId: 0, Value: 0 }];
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'ordering_item_details';
      let headers=await this.config.getHeader();
      let body;
      body={
        consumer_id:this.consumer,
        company_id:cid,
        item_id:this.item.item_id,
        date:moment(this.date).format('YYYY-MM-DD'),
        
        // menu_item_id:this.item.id,
        
        meal_time_id:this.serving_time_id
      }
      console.log('body:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      this.menu=res.data.item
      console.log('res:',this.menu);
      if(this.menu.servingquantity&&this.menu.servingquantity.length>0){
        this.ml_sz_id=this.menu.servingquantity[0].meal_size.id.toString();
            }

   
    
  // if(this.ml_sz=='S'){
  //   this.ml_sz_id='1'
  // }else if(this.ml_sz=='M'){
  //   this.ml_sz_id='2'
  // }else if(this.ml_sz=='L'){
  //   this.ml_sz_id='3'
  // }else if(this.ml_sz=='N'){
  //   this.ml_sz_id='4'
  // }
  // if(this.flag!=1){
    console.log('flag2')
    if(this.item&&this.item.selected_data){
      console.log('secteddata:',this.item.selected_data.comment,this.item.selected_data.quantity)
      if(this.item.selected_data.items_selected_options&&this.item.selected_data.items_selected_options.length){
      this.item.selected_data.items_selected_options.map(x=>{
        this.selectedVal.push({option:x.menu_item_option.id,val:x.menu_item_value.id})
      })
    }else{
        if(this.menu&&this.menu.options&&this.menu.options){
          this.menu.options.map(x=>{
            x.valuess.map(y=>{
              if(y.default_value==1){
              this.selectedVal.push({option:x.id,val:y.id})
              }
            })
            
          })
        }
}
if(this.item.selected_data.comment){
  this.comments=this.item.selected_data.comment
}
if(this.item.selected_data.quantity){
  this.quantity=parseInt(this.item.selected_data.quantity)
}else{
this.quantity=1
}
if(this.item.selected_data.meal_size){
  this.ml_sz_id=this.item.selected_data.meal_size.toString();
}else{

}
}else{
  this.quantity=1;
  if(this.menu&&this.menu.options&&this.menu.options){
    this.menu.options.map(x=>{
      x.valuess.map(y=>{
        if(y.default_value==1){
        this.selectedVal.push({option:x.id,val:y.id})
        }
      })
      
    })
  }
}
  // }
})
// if(this.flag==1){
//   this.menu=this.item
// // }

// // }else{
//   if(this.item.itemdetails.options&&this.item.itemdetails.options){
//     this.item.itemdetails.options.map(x=>{
//       x.valuess.map(y=>{
//         if(y.default_value==1){
//         this.selectedVal.push({option:x.option.id,val:y.valuename.id})
//         }
//       })
      
//     })
//   }
// }
    console.log('selOpt:',this.selectedVal)
  }
  dismiss(i){
    this.modalCntl.dismiss()
  }
  increment(){
    if(this.quantity<5){
    this.quantity++;
    }
  }
  decrement(){
    if(this.quantity>1){
    this.quantity--;
    }
  }
  async apply(){
    let comment,options,values;
    if(this.comments==undefined||this.comments==''||this.comments=='undefined'||this.comments=='null'){
      comment=null
    }else{
      comment=this.comments
    }

    
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'update_ordering_additional_items_option';
      let headers=await this.config.getHeader();
      let body;
      body={
        consumer_id:this.consumer,
        company_id:cid,
        dining_id:this.menu_id,
        date:moment(this.date).format('YYYY-MM-DD'),
        created_by:uid,
        menu_item_id:this.item.id,
        quantity:this.quantity,
        item_size:this.ml_sz_id,
        comment:comment,
        serving_time_id:this.serving_time_id
      }
      // this.optionData.splice(0, 1);
      if(this.selectedVal&&this.selectedVal.length){
        for (let data of this.selectedVal) {
          //  
          if (options == '' || options == undefined){
            options = data.option;
          }
          if (options == data.option) {
            console.log("firstOpt");
  
          } else {
            
            options += "*" + data.option;
          }
          if (values == '' || values == undefined){
            values = data.val;
          }
          if (values == data.val) {
            console.log("firstOpt");
  
          } else {
            
            values += "*" + data.val;
          }
        }
        body.options=options;
        body.option_values=values;
      }

      console.log("body:",body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
       console.log('presave:',res,body);
       if(res.status=='success'){
        this.modalCntl.dismiss(this.item.item.id);
       }
        
      },error=>{
        console.log('err:',error)
      })
  }

  setMealSize(id){
    this.ml_sz_id=id
  }
  selectOptions(opt,val){
    if(this.optionData&&this.optionData.length){
    const targetIdx = this.optionData.map(item => item.OptionId).indexOf(opt);
    if (targetIdx >= 0) {

      this.optionData[targetIdx].Value = val;
      
    }else{
      this.optionData.push({ OptionId: opt, Value: val });
    }
  }else{
    this.optionData.push({ OptionId: opt, Value: val });
  }
  const targetIdx = this.selectedVal.map(item => item.option).indexOf(opt);
    
    this.selectedVal[targetIdx].val = val;
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
}
