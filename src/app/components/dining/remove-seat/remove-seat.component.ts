import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-remove-seat',
  templateUrl: './remove-seat.component.html',
  styleUrls: ['./remove-seat.component.scss'],
})
export class RemoveSeatComponent implements OnInit {
@Input() consumer:any;
  constructor(private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  async remove(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'save_consumer_table';
    let headers=await this.config.getHeader();
    let body={
      seat_id:0,
      area_id:0,
      table_id:0,
      user_id:this.consumer
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
      this.presentAlert('Removed successfully.');
      this.popCntl.dismiss(1);
    },error=>{

    })
  }
}
