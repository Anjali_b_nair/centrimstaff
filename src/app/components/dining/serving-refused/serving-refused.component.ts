import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-serving-refused',
  templateUrl: './serving-refused.component.html',
  styleUrls: ['./serving-refused.component.scss'],
})
export class ServingRefusedComponent implements OnInit {
reason:any;
otherReason:any;
  constructor(private modalCntl:ModalController,private toastCntlr:ToastController) { }

  ngOnInit() {}
  setReason(reason){
    this.reason=reason
  }
  async dismiss(i){
    if(i==1){
      if(this.reason==undefined){
        this.presentAlert('Please select a reason.')
      }else{
     if(this.otherReason==undefined||this.otherReason==null){
       this.otherReason=null
     }
      
      this.modalCntl.dismiss(this.reason,this.otherReason)
    }
    }else{
      this.modalCntl.dismiss();
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
