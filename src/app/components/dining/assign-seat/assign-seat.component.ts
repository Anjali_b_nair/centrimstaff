import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-assign-seat',
  templateUrl: './assign-seat.component.html',
  styleUrls: ['./assign-seat.component.scss'],
})
export class AssignSeatComponent implements OnInit {
  subscription:Subscription;
  consumers:any[]=[];
  terms:any;
    hide:boolean=true;
    @Input() seat:any;
    @Input() area:any;
    @Input() table_id:any;
    rv:any;
    skip:any=0;
    constructor(private modalCntl:ModalController,private storage:Storage,private config:HttpConfigService,
      private http:HttpClient,private loadingCtrl:LoadingController,private platform:Platform,private toastCntlr:ToastController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    this.showLoading();
             console.log('seat:',this.seat)
    
    this.rv=await this.storage.get('RVSETTINGS');

     
    this.skip=0;

this.getList(false,'')

this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
this.back();
}); 

}

async getList(isFirstLoad,event){
  let url=this.config.domain_url+'load_residentsfiltertable';
 
  let headers=await this.config.getHeader();
  let body;
  body={
    take:20,
    skip:this.skip
  }
  if(this.terms){
    body.name=this.terms
  }

  console.log('body:',body);
  
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
   
    console.log("data:",data);
    // this.consumers=data.data

  for (let i = 0; i < data.data.length; i++) {
      this.consumers.push(data.data[i]);
    }

    if (isFirstLoad)
    event.target.complete();
    this.dismissLoader();
    this.skip=this.skip+20;   
      
  },error=>{
    this.dismissLoader();
    console.log(error);
  });
}

doInfinite(event) {
  this.getList(true, event)
  }

 
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

  back(){
    this.modalCntl.dismiss();
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'dining-loading',
      // message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  

  //  cancel(){
  //   this.hide=true;
  //   this.terms='';
  //   console.log('cancel:',this.hide);
  // }
  searchHide(){
    this.hide=false;
  }
  cancel(){
    this.hide=true;
    this.terms='';
    this.skip=0;
    this.consumers=[];
    this.getList(false,'')
  }
  search(){
    // this.hide=true;
    this.skip=0;
    this.consumers=[];
    this.getList(false,'')
  }
  async assignSeat(item){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let headers=await this.config.getHeader();


   
  
    let url=this.config.domain_url+'save_consumer_table';
    
    let body;
    body={
      company_id:cid,
      user_id:item.user.user_id,
      created_by:uid,
      seat_id:this.seat.seat_id,
      area_id:parseInt(this.area),
      table_id:this.table_id
    }
    
  
    console.log('body:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     console.log('assigned:',res,url);
    this.presentAlert('Seat assigned successfully.')
     this.modalCntl.dismiss(1)
      
    },error=>{
      console.log('err:',error)
    })
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
