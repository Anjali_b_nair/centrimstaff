import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ServingChangeCountComponent } from './serving-change-count.component';

describe('ServingChangeCountComponent', () => {
  let component: ServingChangeCountComponent;
  let fixture: ComponentFixture<ServingChangeCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServingChangeCountComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ServingChangeCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
