import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
@Component({
  selector: 'app-serving-change-count',
  templateUrl: './serving-change-count.component.html',
  styleUrls: ['./serving-change-count.component.scss'],
})
export class ServingChangeCountComponent implements OnInit {
@Input() quantity;
qty:BehaviorSubject<1>;

  constructor(private popCntl:PopoverController) { }

  ngOnInit() {
    this.qty=this.quantity;
  }

  
  increment(){
    if(this.quantity<5){
    this.quantity++;
    this.qty=this.quantity;
    }
  }
  decrement(){
    if(this.quantity>1){
    this.quantity--;
    this.qty=this.quantity;
    }
  }

  dismiss(){
    console.log('dismissed:',this.quantity)
    this.popCntl.dismiss(this.quantity)
  }
}
