import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastController, PopoverController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
@Component({
  selector: 'app-mark-leave-singleday',
  templateUrl: './mark-leave-singleday.component.html',
  styleUrls: ['./mark-leave-singleday.component.scss'],
})
export class MarkLeaveSingledayComponent implements OnInit {
  @Input() date:any;
  @Input() menu_id:any;
  @Input() consumer_id:any;
  constructor(private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}



  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  async markLeave(){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'mark_as_leave_via_ordering';
    let headers=await this.config.getHeader();
    let body={
      consumer_id:this.consumer_id,
      
      dining_id:this.menu_id,
      
      date:moment(this.date).format('YYYY-MM-DD'),
      created_by:uid
    }
    console.log(url,body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res)
      // if(res.skipped=='Skipped')
      if(res.status=='error'){
        this.presentAlert(res.message);
        this.popCntl.dismiss(0);
      }else{
      this.presentAlert('Leave marked successfully.');
      this.popCntl.dismiss(1);
      }
      
    },error=>{
      console.log(error);
      
    })
  }
}
