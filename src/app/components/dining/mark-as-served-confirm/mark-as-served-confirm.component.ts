import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-mark-as-served-confirm',
  templateUrl: './mark-as-served-confirm.component.html',
  styleUrls: ['./mark-as-served-confirm.component.scss'],
})
export class MarkAsServedConfirmComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}
  dismiss(){
    this.modalCntl.dismiss();
  }
  confirm(){
    this.modalCntl.dismiss(1);
  }
}
