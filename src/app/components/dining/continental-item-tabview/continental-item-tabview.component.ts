import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { AllergicInfoComponent } from '../allergic-info/allergic-info.component';
import { ContinentalItemDetailsComponent } from '../continental-item-details/continental-item-details.component';
import { Storage } from '@ionic/storage-angular'
import { OpenImageComponent } from '../../open-image/open-image.component';
@Component({
  selector: 'app-continental-item-tabview',
  templateUrl: './continental-item-tabview.component.html',
  styleUrls: ['./continental-item-tabview.component.scss'],
})
export class ContinentalItemTabviewComponent implements OnInit {
  @Input()menu:any;
  @Input()date:any;
  // @Input()serving_time:any;
  @Input()menu_id:any;
  @Input()consumer:any;
  @Input()serving_time_id:any;
  @Input()fdtexture:any;
  @Input()fltexture:any;
  details:any=[];
  serving_time:any;
  @Input()preOrderedItems:any;
  @Input()allergies:any;

  selected:any=[];
  items:any=[];
  // menu_id:any;
  item_id:any;
  selectedItem:any;
  quantity:any=1;
size:any;
comments:any;
optionData: { OptionId: number, Value: number }[];
selectedVal:any[]=[];
ml_sz_id:any;
parent_id:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,private modalCntl:ModalController,
    private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    this.selected=[];
    this.items=[];
    this.optionData = [{ OptionId: 0, Value: 0 }];
    console.log('con:',this.menu,this.fdtexture,this.fltexture);
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
     
      let url=this.config.domain_url+'continental_breakfast_via_menu_detail_id';
      let headers=await this.config.getHeader();
      let body={
        consumer_id:this.consumer,
        menu_item_id:this.menu.id,
        date:moment(this.date).format('YYYY-MM-DD'),
        meal_time_id:this.serving_time_id
        // consumer_id:45,
        // menu_item_id:8812,
        // date:'2022-03-10',
        // meal_time_id:3
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
       console.log('confirm:',res,body);
       this.serving_time=res.data.meal_time;
       this.details=res.data.menu_item.sub_items;
       this.item_id=res.data.menu_item.id;
       this.parent_id=res.data.menu_item.menu_item_id;

      

       this.details.forEach(element => {
        if(element.is_ordered){
          if(this.selected&&this.selected.length){
           const idx=this.selected.indexOf(element.itemdetails.id);
           const Idx=this.items.map(x=>x.menu_item_id).indexOf(element.itemdetails.id);
           if(idx>=0){
             this.selected.splice(idx,1);
             this.items.splice(Idx,1);
           }else{
             this.selected.push(element.itemdetails.id);
             this.items.push({menu_item_id:element.itemdetails.id,quantity:element.selected_data.quantity,size:element.selected_data.meal_size,comment:element.comment,options:null,option_values:null})
           }
          }else{
           this.selected.push(element.itemdetails.id)
          }
          
        }
      });
      if(this.selected.length==0){
        this.chooseitem(this.details[0]);
      }
      },error=>{
        
      })
      

  }
  availableItem(i){
    let avl;
    if(i.itemdetails.foodtexture&&i.itemdetails.foodtexture.length){
   const idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fdtexture);
   const Idx= i.itemdetails.foodtexture.map(x=>x.fdtexture.id).indexOf(this.fltexture);
   if(idx>=0||Idx>=0){
     avl=true
   }else{
     avl=false
   }
   
   }else{
     avl=true
   }
      
    return avl;
  }

  async dismiss(){
    await this.modalCntl.dismiss()
  }
  // incompatable(item){
  //   let inc=false;
  //   const idx=this.preOrderedItems.map(x=>x.serving_time_id).indexOf(this.serving_time_id);
  //   const Idx=this.preOrderedItems[idx].all_coloumns.map(x=>x.id).indexOf(item.id);
  //   if(Idx<0){
  //     inc=true
  //   }
  //   return inc;
   

  // }

  allergicIcon(i){
    let c;
    if(i.itemdetails.alergies){
    i.itemdetails.alergies.map(x => {
      
      if (this.allergies.includes(x.alergy.alergy)) {
        c=true;
        }
        // console.log("allergy:",x.alergy.alergy,this.allergies.includes(x.alergy))
      })
    }
      return c;
  }

  selectitem(item){
    console.log('click worked')

    let options,values;
    this.optionData.splice(0, 1);
      if(this.optionData&&this.optionData.length){
        for (let data of this.optionData) {
          //  
          if (options == '' || options == undefined){
            options = data.OptionId;
          }
          if (options == data.OptionId) {
            console.log("firstOpt");
  
          } else {
            
            options += "*" + data.OptionId;
          }
          if (values == '' || values == undefined){
            values = data.Value;
          }
          if (values == data.Value) {
            console.log("firstOpt");
  
          } else {
            
            values += "*" + data.Value;
          }
        }
        
      }else{
        options=null;
        values=null
      }
      if(this.comments==undefined||this.comments==''){
        this.comments=null
      }


      this.items.push({menu_item_id:item.id,quantity:this.quantity,size:this.ml_sz_id,comment:this.comments,options:options,option_values:values});
            this.selected.push(item.itemdetails.id)







   
  }
  async allergicAlert(allergies,item) {
    const modal = await this.modalCntl.create({
      component: AllergicInfoComponent,
      cssClass:'dining-allergicinfo-modal',
      componentProps:{
        consumer:this.consumer.user.name,
        allergies:allergies
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data==2){
        this.selectedItem=item;
        this.quantity=1;
        this.comments=undefined;
        this.optionData = [{ OptionId: 0, Value: 0 }];
        this.selectedVal=[];
        if(this.selectedItem.selected_data){
          this.quantity=this.selectedItem.selected_data.quantity
          this.comments=this.selectedItem.selected_data.comment;
        if(this.selectedItem.selected_data.size){
          this.ml_sz_id=this.selectedItem.selected_data.size.id.toString()
        }else{
          if(this.selectedItem.itemdetails.servingqunatity&&this.selectedItem.itemdetails.servingqunatity.length){
            this.ml_sz_id=this.selectedItem.itemdetails.servingqunatity[0].size_id.toString()
          }
        }
        if(this.selectedItem.selected_data.items_selected_options&&this.selectedItem.selected_data.items_selected_options.length){
          this.selectedItem.selected_data.items_selected_options.map(x=>{
            x.valuess.map(y=>{
             
              this.selectedVal.push({option:x.option.id,val:y.valuename.id})
              
            })
            
          })
        }
      }else{
        
        if(this.selectedItem.itemdetails.options&&this.selectedItem.itemdetails.options){
          this.selectedItem.itemdetails.options.map(x=>{
            x.valuess.map(y=>{
              if(y.default_value==1){
              this.selectedVal.push({option:x.option.id,val:y.valuename.id})
              }
            })
            
          })
        }
        if(this.selectedItem.itemdetails.servingqunatity&&this.selectedItem.itemdetails.servingqunatity.length){
          this.ml_sz_id=this.selectedItem.itemdetails.servingqunatity[0].size_id.toString()
        }
      }
      }
    });
    return await modal.present();
  }

  // async ItemDetails(item) {
  //   const modal = await this.modalCntl.create({
  //     component: ContinentalItemDetailsComponent,
  //     // cssClass:'dining-allergicinfo-modal',
  //     componentProps:{
  //       item:item,
  //       date:this.date,
  //       servingtime:this.serving_time
  //     }
  //   });
  //   modal.onDidDismiss().then((dataReturned) => {
  //     if(dataReturned.data){
  //       console.log('data:',dataReturned.data)
  //       this.items.push(dataReturned.data)
  //       // this.items.push({menu_item_id:item.itemdetails.id,quantity:daquantity,size:size,comment:comment,options:options,option_values:values});
  //       this.selected.push(item.itemdetails.id)
  //     }
  //   });
  //   return await modal.present();
  // }

  async save(){
    if(this.selected&&this.selected.length){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'order_continental_breakfast';
    let headers=await this.config.getHeader();
    let body={
      created_by:uid,
      consumer_id:this.consumer,
      dining_id:this.menu_id,
      date:moment(this.date).format('YYYY-MM-DD'),
      serving_time_id:this.serving_time_id,
      parent_menu_item_id:this.parent_id,
      items:this.items
    }
    console.log('body:',body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
     console.log('confirm:',res);
     this.modalCntl.dismiss(this.item_id)
    },error=>{
      
    })
  }else{
    this.presentAlert('Please select any item.')
  }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

  chooseitem(item){

    const idx=this.selected.indexOf(item.itemdetails.id);
    const Idx=this.items.map(x=>x.menu_item_id).indexOf(item.itemdetails.id);
    if(idx>=0){
      
      this.selected.splice(idx,1);
      
      this.items.splice(Idx,1);
      console.log('selected:',this.selected,idx,Idx)
    }else{
    if(!this.availableItem(item)){
      console.log('not available')
    }else{
      if(this.allergicIcon(item)){
        console.log('allergic item')
        this.allergicAlert(item.itemdetails.alergies,item);
      }else{
        console.log('open details')
        this.selectedItem=item;
    this.quantity=1;
    this.comments=undefined;
    this.optionData = [{ OptionId: 0, Value: 0 }];
    this.selectedVal=[];
    if(this.selectedItem.selected_data){
      this.quantity=this.selectedItem.selected_data.quantity
      this.comments=this.selectedItem.selected_data.comment;
    if(this.selectedItem.selected_data.size){
      this.ml_sz_id=this.selectedItem.selected_data.size.id.toString()
    }else{
      if(this.selectedItem.itemdetails.servingqunatity&&this.selectedItem.itemdetails.servingqunatity.length){
        this.ml_sz_id=this.selectedItem.itemdetails.servingqunatity[0].size_id.toString()
      }
    }
    if(this.selectedItem.selected_data.items_selected_options&&this.selectedItem.selected_data.items_selected_options.length){
      this.selectedItem.selected_data.items_selected_options.map(x=>{
        x.valuess.map(y=>{
         
          this.selectedVal.push({option:x.option.id,val:y.valuename.id})
          
        })
        
      })
    }
  }else{
    
    if(this.selectedItem.itemdetails.options&&this.selectedItem.itemdetails.options){
      this.selectedItem.itemdetails.options.map(x=>{
        x.valuess.map(y=>{
          if(y.default_value==1){
          this.selectedVal.push({option:x.option.id,val:y.valuename.id})
          }
        })
        
      })
    }
    if(this.selectedItem.itemdetails.servingqunatity&&this.selectedItem.itemdetails.servingqunatity.length){
      this.ml_sz_id=this.selectedItem.itemdetails.servingqunatity[0].size_id.toString()
    }
  }
      }
    }
  }








    
  }

  increment(){
    this.quantity++;
  }
  decrement(){
    if(this.quantity>1){
    this.quantity--;
    }
  }
  setMealSize(id){
    this.ml_sz_id=id
  }
  selectOptions(opt,val){
    if(this.optionData&&this.optionData.length){
    const targetIdx = this.optionData.map(item => item.OptionId).indexOf(opt);
    if (targetIdx >= 0) {

      this.optionData[targetIdx].Value = val;
      
    }else{
      this.optionData.push({ OptionId: opt, Value: val });
    }
  }else{
    this.optionData.push({ OptionId: opt, Value: val });
  }
  const targetIdx = this.selectedVal.map(item => item.option).indexOf(opt);
    
    this.selectedVal[targetIdx].val = val;
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
}
