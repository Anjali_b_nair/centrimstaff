import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContinentalItemTabviewComponent } from './continental-item-tabview.component';

describe('ContinentalItemTabviewComponent', () => {
  let component: ContinentalItemTabviewComponent;
  let fixture: ComponentFixture<ContinentalItemTabviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContinentalItemTabviewComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContinentalItemTabviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
