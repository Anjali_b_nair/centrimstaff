import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastController, PopoverController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
@Component({
  selector: 'app-dining-area',
  templateUrl: './dining-area.component.html',
  styleUrls: ['./dining-area.component.scss'],
})
export class DiningAreaComponent implements OnInit {
@Input() date:any;
@Input() menu_id:any;
// @Input() serve_area:any;
@Input() serving_id:any;
@Input() consumer_id:any;
@Input() sel_area:any;
  constructor(private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toastCntlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('area:',this.sel_area)
    // const targetIdx = this.serve_area.map(item => item.id).indexOf(0);
    // if (targetIdx >= 0) {
    //   }else{
    //     this.serve_area.unshift({id:0,serving_area:'In room'})
    //   }
    
    
  }

  async choose(item){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'change_single_meal_serve_area';
    let headers=await this.config.getHeader();
    let body={
      company_id:cid,
      consumer_id:this.consumer_id,
      dining_id:this.menu_id,
      meal_time_id:this.serving_id,
      date:moment(this.date).format('YYYY-MM-DD'),
      created_by:uid,
      serving_area_id:item.id
    }
    console.log('body:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      // if(res.skipped=='Skipped')
      this.presentAlert('Dining area changed successfully.');
      this.popCntl.dismiss(1);
    },error=>{
      console.log(error)
    })
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

  async select(i){
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'change_single_meal_serve_area';
    let headers=await this.config.getHeader();
    let body={
      company_id:cid,
      consumer_id:this.consumer_id,
      dining_id:this.menu_id,
      meal_time_id:this.serving_id,
      date:moment(this.date).format('YYYY-MM-DD'),
      created_by:uid,
      in_serving_area:i
    }
    console.log('body:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      // if(res.skipped=='Skipped')
      this.presentAlert('Dining area changed successfully.');
      this.popCntl.dismiss(1);
    },error=>{
      console.log(error)
    })
  }
}
