import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-contact-password',
  templateUrl: './rv-contact-password.component.html',
  styleUrls: ['./rv-contact-password.component.scss'],
})
export class RvContactPasswordComponent implements OnInit {
newPassword:any;
confirmPassword:any;
@Input()contact;
  constructor(private modalCntl:ModalController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,) { }

  ngOnInit() {}

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  dismiss(){
    this.modalCntl.dismiss();
  }

  async update(){
    if(!this.newPassword||!this.confirmPassword){
      this.presentAlert('Please fill the required fields');

    }else if(this.newPassword!==this.confirmPassword){
      this.presentAlert('Password mismatch')
    }else{
      const bid = await this.storage.get("BRANCH");
      const cid = await this.storage.get("COMPANY_ID");
      let url=this.config.domain_url+'updateResPassword';
      let headers=await this.config.getHeader();
     let body={
      user_id:this.contact.contact_user_id,
      newpassword:this.newPassword
     }
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        this.presentAlert('Password updated successfully.')
        this.modalCntl.dismiss(1);
      })
    }
  }

}
