import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-filter-consumers',
  templateUrl: './filter-consumers.component.html',
  styleUrls: ['./filter-consumers.component.scss'],
})
export class FilterConsumersComponent implements OnInit {
status:any;
rv:any;
type:any;
  constructor(private modalCntl:ModalController,private storage:Storage) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    this.rv=await this.storage.get('RVSETTINGS');
  }
  // active(){
  //   this.status=1;
  //   this.closeModal();
  // }

  // inactive(){
  //   this.status=2;
  //   this.closeModal();
  // }

  // noContract(){
  //   this.status=3;
  //   this.closeModal();
  // }

  // movingIn(){
  //   this.status=4;
  //   this.closeModal();
  // }


  async closeModal() {
    const onClosedData: any = {status:this.status,type:this.type};
    await this.modalCntl.dismiss(onClosedData);
  }

  

}
