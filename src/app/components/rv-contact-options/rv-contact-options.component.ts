import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvContactEditComponent } from '../rv-contact-edit/rv-contact-edit.component';
import { RvContactPasswordComponent } from '../rv-contact-password/rv-contact-password.component';

@Component({
  selector: 'app-rv-contact-options',
  templateUrl: './rv-contact-options.component.html',
  styleUrls: ['./rv-contact-options.component.scss'],
})
export class RvContactOptionsComponent implements OnInit {
@Input()contact;
  constructor(private modalCntl:ModalController,private popCntl:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,
    private alertCnlr:AlertController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('data:',this.contact)
  }

  async edit(){
    const modal = await this.modalCntl.create({
      component:RvContactEditComponent,
      cssClass:'full-width-modal',
      componentProps: {
        
       contact:this.contact
        
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }

  // async delete(){
  //   const bid = await this.storage.get("BRANCH");
  //   const cid = await this.storage.get("COMPANY_ID");
  //   let url=this.config.domain_url+'user/'+this.contact.contact_user_id;
  //   let headers=await this.config.getHeader();
 
  //   this.http.delete(url,{headers}).subscribe((res:any)=>{
  //     this.presentAlert('Deleted successfully.')
  //     this.popCntl.dismiss(1);
  //   })
  // }

  async reset(){
    const modal = await this.modalCntl.create({
      component:RvContactPasswordComponent,
      cssClass:'rv-edit-folder-modal',
      componentProps: {
        
       contact:this.contact
        
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }


  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  // async remove() {
  //   const alert = await this.alertCnlr.create({
  //     mode:'ios',
  //     header: 'Are you sure?',
  //     message: 'You will not be able to recover this contact!',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         // cssClass: 'secondary',
  //         handler: (blah) => {
  //           this.popCntl.dismiss()
  //           console.log('Confirm Cancel: blah');
  //         }
  //       }, {
  //         text: 'Delete',
  //         handler: () => {
           
  //           this.delete();
  //         }
  //       }
  //     ]
  //   });
  
  //   await alert.present();
  // }

  async inactivateAlert() {
    const alert = await this.alertCnlr.create({
      mode:'ios',
      header: 'Are you sure?',
      message: 'You can activate this contact later.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            this.popCntl.dismiss()
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            this.inactivate();
          }
        }
      ]
    });
  
    await alert.present();
  }

  async inactivate(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'user/'+this.contact.contact_user_id;
    let headers=await this.config.getHeader();
  //  let body={

  //  }
  
    this.http.delete(url,{headers}).subscribe((res:any)=>{
      this.presentAlert('Inactivated successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async  activate(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'activate_user';
    let headers=await this.config.getHeader();
    let body={user_id:this.contact.contact_user_id}
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.presentAlert('Activated.');
     
      this.popCntl.dismiss(1);
    })
  }
}
