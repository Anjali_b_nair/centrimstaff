import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvMarkAwayOptionsComponent } from './rv-mark-away-options.component';

describe('RvMarkAwayOptionsComponent', () => {
  let component: RvMarkAwayOptionsComponent;
  let fixture: ComponentFixture<RvMarkAwayOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvMarkAwayOptionsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvMarkAwayOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
