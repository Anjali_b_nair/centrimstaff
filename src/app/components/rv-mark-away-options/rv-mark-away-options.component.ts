import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvDocEditComponent } from '../rv-doc-edit/rv-doc-edit.component';
import { RvMarkAwayComponent } from '../rv-mark-away/rv-mark-away.component';

@Component({
  selector: 'app-rv-mark-away-options',
  templateUrl: './rv-mark-away-options.component.html',
  styleUrls: ['./rv-mark-away-options.component.scss'],
})
export class RvMarkAwayOptionsComponent implements OnInit {
@Input() consumer;
  constructor(private modalCntl:ModalController,private popCntl:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,private alertCntlr:AlertController) { }

  ngOnInit() {}

  async edit(){
    const modal = await this.modalCntl.create({
      component:RvMarkAwayComponent,
      componentProps: {
        
       
        
        consumer:this.consumer,
        flag:1
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }
  async deleteAlert(){

    const alert = await this.alertCntlr.create({
      header: 'Are you sure?',
      backdropDismiss:true,
      message: 'You will not be able to recover this!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            
            this.delete();
           
  
          }
        }
      ]
    });
    
    await alert.present();
  
   
  }
  async delete(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_mark_away/'+this.consumer.id;
    let headers=await this.config.getHeader();
   
  
    this.http.delete(url,{headers}).subscribe((res:any)=>{
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

}
