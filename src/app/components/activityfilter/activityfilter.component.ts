import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';


@Component({
  selector: 'app-activityfilter',
  templateUrl: './activityfilter.component.html',
  styleUrls: ['./activityfilter.component.scss'],
})
export class ActivityfilterComponent implements OnInit {
  type:any;
  wingArray:any=[];
  wing:any;
 
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {
   
  }
  // selectAll(){
  //   this.type=0;
  //   this.closeModal();
    
  // }

  // activity(){
  //   this.type=1;
  //   this.closeModal();
    
  // }

//   event(){
//     this.type=8;
//     this.closeModal();
    
//   }

//   meeting(){
//     this.type=9;
//     this.closeModal();
    
//   }
// inactive(){
//   this.type=10;
//   this.closeModal();
 
// }
  async closeModal(t) {
  this.type=t
    const onClosedData: any = this.type;
    await this.popCntl.dismiss(onClosedData);
  }
  


}
