import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-about-info',
  templateUrl: './about-info.component.html',
  styleUrls: ['./about-info.component.scss'],
})
export class AboutInfoComponent implements AfterViewInit {
  @ViewChild("expandWrapper", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded1") expanded1: boolean = false;
  @Input("expanded2") expanded2: boolean =false;
  @Input("expanded3") expanded3: boolean = false;
  @Input("expanded4") expanded4: boolean =false;
  @Input("expanded5") expanded5: boolean = false;
  // @Input("expanded") expanded: boolean = false;
  @Input("expandHeight") expandHeight: string = "500px";
  constructor(public renderer: Renderer2) { }

  
  ngAfterViewInit() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }

}
