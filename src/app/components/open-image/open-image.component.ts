import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-open-image',
  templateUrl: './open-image.component.html',
  styleUrls: ['./open-image.component.scss'],
})
export class OpenImageComponent implements OnInit {
  @Input() data;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    console.log("openImg:",this.data);
    
  }
  back(){
    this.popCntl.dismiss();
  }
}
