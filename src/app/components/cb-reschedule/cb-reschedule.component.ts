import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { IonicModule, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { NgModule } from '@angular/core';

// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//   ]
// })

@Component({
  selector: 'app-cb-reschedule',
  templateUrl: './cb-reschedule.component.html',
  styleUrls: ['./cb-reschedule.component.scss'],
})

export class CbRescheduleComponent implements OnInit {
  minDate: string = new Date().toISOString();
  date:any;
  today:any;
  current:Date=new Date();
  @Input() data:any;
  @Input() cdate:any;
  dateString:any;
  duration:any;
  ar_avl: Array<string>;
  slot:any=[];
  start:any;
  end:any;
  hours;
  minutes;
  sel_time:any;
  not_available:any=[];
  disableButton:boolean=true;
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,private popCntl:ModalController,
    private toastCntlr:ToastController) { }

  ngOnInit() {
    // this.date=this.cdate;
  }


async ionViewWillEnter(){
  
    this.duration="30";
    this.today=this.current.getFullYear()+'-' + this.fixDigit(this.current.getMonth() + 1)+'-'+this.fixDigit(this.current.getDate());
  console.log("data:",this.data,"date:",this.cdate);
  this.date=new Date(this.cdate).toISOString();
  this.dateString=moment(this.date).format('dddd,DD MMMM,yyyy ')
 
    const data=await this.storage.get("COMPANY_ID")

      const bid=await this.storage.get("BRANCH")



      
      let headers=await this.config.getHeader();
      // let url= this.config.domain_url+'company_details/'+data;
      let url= this.config.domain_url+'show_branch_call_time/'+bid;
      let body={company_id:data}
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("res:",res);
        this.start=res.branch.call_start_time;
        this.end=res.branch.call_end_time;
        this.getAvailableTimeSlots();
        
        // this.slot=this.timeArray(this.start,this.end,30);
      },error=>{
        console.log(error);
        
      })
  
  
}

onChange(ev){
  console.log("cdate:",this.date);
  this.dateString=moment(this.date).format('dddd,DD MMMM,yyyy ')
  // this.date_1=this.date;
  this.getAvailableTimeSlots();
  
  // this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
  // let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
  // this.date=new Date(d);
 
}
selectDuration(){
  console.log("dur:",this.duration);
  // this.slot=this.timeArray(this.start,this.end,30);
  this.getAvailableTimeSlots();
  // this.slot=this.timeArray(this.start,this.end,30);

  console.log("slots:",this.slot);
  
}

async getAvailableTimeSlots(){
  this.not_available=[];
  
    const data=await this.storage.get("COMPANY_ID")

      const bid=await this.storage.get("BRANCH")

      let headers=await this.config.getHeader();
      let url= this.config.domain_url+'timeslot_notavailable';
      let d;
      let date=moment(this.date).format('yyyy-MM-DD');
      if(this.duration=='30'){
        d=1
      }else{
        d=2
      }
      let body={
        company_id:data,
        date:date,
        duration:d,
        user_id:this.data.consumer.user_id
      }
      console.log("body:",body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("res:",res)
        res.data.forEach(element => {
          console.log(element);
          let t={slots:element}
          this.not_available.push(t);
          
          
        });
        this.ar_avl= res.json_data
        // this.not_available.push(res.data));
        console.log("fff:",this.ar_avl)
        this.slot=this.timeArray(this.start,this.end,30);
      },error=>{
        console.log(error);
        
      })
    
console.log("not:",this.not_available);
// this.ar_avl= this.not_available.map(p => p.slots)
  



}

//  method to fix month and date in two digits
fixDigit(val){
return val.toString().length === 1 ? "0" + val : val;
}

timeArray(start, end,duration){
console.log("start:",start,"end:",end);

var start = start.split(":");
var end = end.split(":");
duration= parseInt(duration);
start = parseInt(start[0]) * 60 + parseInt(start[1]);
end = parseInt(end[0]) * 60 + parseInt(end[1]);
console.log(start,end);

var result = [];

for (let time = start; time <= end; time+=duration){
    var slot=this.timeString(time);
    let disable;
   
        // alligator.includes("thick scales");
    console.log("arraaaa:",this.ar_avl,"sl:",slot)
    console.log("incl:",this.ar_avl.includes(slot));
    if(this.ar_avl.includes(slot)){
      disable=true;
    }else{
      disable=false;
    }

  result.push( {'slot':slot,'disable':disable});
  
}
  console.log("result:",result);
  
return result;
}
timeString(time){
this.hours = Math.floor(time / 60);
let h=Math.floor(time / 60);
this.minutes = time % 60;
if(this.hours>12){
this.hours=this.hours-12 ;
}
if (this.hours < 10) {
 this.hours = "0" + this.hours; //optional
}

if (this.minutes < 10){
  this.minutes = "0" + this.minutes;
}

if(h>=12){
return this.hours + ":" + this.minutes +' PM';
}else{
return this.hours + ":" + this.minutes +' AM';
}
}


bookcall(item){
  this.disableButton=false;
  let currenttime=this.current.getTime();
  let a=item.slot.split(' ');
  let t=a[0].split(':');
  let time;
  if(a[1]=='AM'){
  time=a[0]+':00'
  }else{
     time=(parseInt(a[0])+12)+':'+t[1]+':00'
  }
  let x=this.today+'T'+time
  let sel_slot=new Date(x);
  
  console.log("x:",x,"slot:",sel_slot.getTime(),"curr:",currenttime);
  
    if(item.disable){
      this.presentAlert('This time slot is not available.Please select another.');
    }else if(this.date==this.today && sel_slot.getTime()<currenttime){
      console.log("current date");
      this.presentAlert('Please choose a valid time slot.');
    }else{
      
      this.sel_time=item.slot;
      
      
    }
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
async confirm(){
 
    const bid=await this.storage.get("BRANCH")

  if(this.sel_time==undefined){
    this.presentAlert('Please select a time slot.');
  }else{
    let date=moment(this.date).format('yyyy-MM-DD');;
  let url=this.config.domain_url+'reschedule_call';
  let headers=await this.config.getHeader();
  let d;
            if(this.duration=='30'){
              d=1
            }else{
              d=2
            }
  let body={
        call_id:this.data.id,
        duration:d,
        start_time:this.sel_time,
        date:date
  }
  console.log("body:",body);
this.http.post(url,body,{headers}).subscribe((res:any)=>{
  console.log(res);
  if(res.status=='success'){
    this.presentAlert('Rescheduled successfully');
    this.popCntl.dismiss()
  }else{
    this.presentAlert('Something went wrong.');
  }
  
})
  
}
   
}

cancel(){
  this.popCntl.dismiss();
}
}
