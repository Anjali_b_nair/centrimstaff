import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';

import moment from 'moment';
import { RvSelectStaffComponent } from '../rv-select-staff/rv-select-staff.component';
import { Storage } from '@ionic/storage-angular';
import { ResidentComponent } from '../resident/resident.component';


@Component({
  selector: 'app-rv-task-filter',
  templateUrl: './rv-task-filter.component.html',
  styleUrls: ['./rv-task-filter.component.scss'],
})
export class RvTaskFilterComponent implements OnInit {
@Input()date_filter;
@Input()priority;
@Input()status;
@Input()sdate;
@Input()edate;
@Input()assignedTo;
@Input()staff;
@Input() flag;
@Input()resident;
@Input()res_name;
date:any=new Date().toISOString();
  constructor(private modalCntl:ModalController,private popCntl:PopoverController,private storage:Storage) { }

  ngOnInit() {}

ionViewWillEnter(){
  console.log('fil:',this.date_filter,this.assignedTo,this.staff);
  
  this.edate=new Date(this.edate).toISOString();
    this.sdate=new Date(this.sdate).toISOString();
}
  dismiss(){
    this.modalCntl.dismiss();
  }

  save(){
    
    if(this.date_filter==0){
      this.sdate=moment().format('YYYY-MM-DD');
      this.edate=moment().format('YYYY-MM-DD');
    }else if(this.date_filter==1){
      this.edate=moment().subtract(1,'days').format('YYYY-MM-DD');
      this.sdate=moment().subtract(1,'days').format('YYYY-MM-DD');
    }else if(this.date_filter==2){
      this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().subtract(7,'days').format('YYYY-MM-DD');
    }else if(this.date_filter==3){
      this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD');
    }else if(this.date_filter==4){
      this.edate=moment().endOf('month').format('YYYY-MM-DD');
      this.sdate= moment().startOf('month').format('YYYY-MM-DD');
    }else if(this.date_filter==5){
      this.edate=moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');
      this.sdate= moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
    }else if(this.date_filter==6){
      this.sdate=moment(this.sdate).format('YYYY-MM-DD');
      this.edate=moment(this.edate).format('YYYY-MM-DD');
    }
    const onClosedData={
      date_filter:this.date_filter,
      priority:this.priority,
      status:this.status,
      sdate:this.sdate,
      edate:this.edate,
      assignedTo:this.assignedTo,
      staff:this.staff,
      resident:this.resident,
      res_name:this.res_name
    };
    this.modalCntl.dismiss(onClosedData);
  }

async openAssignedTo(){
  console.log('assigned modal');
 



  const popover = await this.popCntl.create({
    component:RvSelectStaffComponent,
    cssClass:'rv-select-staf-pop',
    backdropDismiss:true,
    componentProps:{
      data:this.assignedTo,
      flag:0,
      status:1

    },
    
    
    
  });
  popover.onDidDismiss().then((dataReturned) => {
    console.log('data:',dataReturned);
    if(dataReturned.data){
      if(dataReturned.data=='no_data'){
        this.assignedTo=[];
        this.staff=undefined;
      }else{
          this.assignedTo=dataReturned.data.id;
          
            this.staff=dataReturned.data.name
      }
          
        }
 
 
    
  });
  return await popover.present();

}

async openResident(){
  const bid = await this.storage.get("BRANCH");
    const popover = await this.modalCntl.create({
      component: ResidentComponent,

      componentProps: {
        data: this.resident,
        rv:1,
        bid:bid,
        name:this.res_name,
        flag:1
      },
      cssClass:'full-width-modal'
    });

    popover.onDidDismiss().then((dataReturned) => {
      console.log('res:',dataReturned);
      if (dataReturned.data != undefined) {
        if(dataReturned.data=='no_data'){
          this.resident=undefined;
          this.res_name=undefined;
        }else{
        this.resident=dataReturned.data.data;
        
       
        this.res_name = dataReturned.data.name
        }
      
      }else{
        if(dataReturned.role!='backdrop'){
        this.resident = undefined;
        this.res_name = undefined;
       
        }
      }
      console.log("data:", dataReturned);

    });
    return await popover.present();
}

}
