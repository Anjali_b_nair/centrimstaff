import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActivityAttendanceGroupComponent } from './activity-attendance-group.component';

describe('ActivityAttendanceGroupComponent', () => {
  let component: ActivityAttendanceGroupComponent;
  let fixture: ComponentFixture<ActivityAttendanceGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityAttendanceGroupComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActivityAttendanceGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
