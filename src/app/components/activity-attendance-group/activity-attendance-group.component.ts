import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-activity-attendance-group',
  templateUrl: './activity-attendance-group.component.html',
  styleUrls: ['./activity-attendance-group.component.scss'],
})
export class ActivityAttendanceGroupComponent implements OnInit {
group:any[]=[];
  constructor(private http:HttpClient,private config:HttpConfigService,
    private modalCntl:ModalController,private storage:Storage) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    
      const bid=await this.storage.get("BRANCH");
      const cid=await this.storage.get("COMPANY_ID");
    this.group=[];
    this.group.push({id:0,name:'All residents'})
    let url=this.config.domain_url+'all_groups';
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
     res.data.forEach(element => {
       this.group.push({id:element.id,name:element.name})
     });
      
    })
  
  }

  async select(item){
    const onClosedData:any = item.id;
    await this.modalCntl.dismiss(onClosedData);
  }
  dismiss(){
    this.modalCntl.dismiss();
  }
}
