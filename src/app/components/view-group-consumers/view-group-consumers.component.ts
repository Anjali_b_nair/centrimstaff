import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-view-group-consumers',
  templateUrl: './view-group-consumers.component.html',
  styleUrls: ['./view-group-consumers.component.scss'],
})
export class ViewGroupConsumersComponent implements OnInit {

  status:any;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  create(){
    this.status=1;
    this.closeModal();
  }

  view(){
    this.status=2;
    this.closeModal();
  }


  async closeModal() {
    const onClosedData: any = this.status;
    await this.popCntl.dismiss(onClosedData);
  }

}
