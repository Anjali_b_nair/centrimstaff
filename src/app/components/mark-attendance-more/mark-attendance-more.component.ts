import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-mark-attendance-more',
  templateUrl: './mark-attendance-more.component.html',
  styleUrls: ['./mark-attendance-more.component.scss'],
})
export class MarkAttendanceMoreComponent implements AfterViewInit {

  @ViewChild("expandWrapper", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded") expanded1: boolean = false;
  
  @Input("expandHeight") expandHeight: string = "500px";
  constructor(public renderer: Renderer2) { }

  
  ngAfterViewInit() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }

}
