import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss'],
})
export class ThankyouComponent implements OnInit {
  // flag:any;
  // @Input() data:any;
  constructor(private router:Router,private modalCntlr:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    // this.flag=this.navParams.data.flag;
   setTimeout(() => { 
     this.closeModal();
    //  if(this.flag==2){
     this.router.navigate(['/menu'],{replaceUrl:true})
    //  }else{
    //    this.router.navigate(['/survey'])
    //  }
   }, 3000)
  }
 
 
  closeModal(){
    this.modalCntlr.dismiss();
  }
}
