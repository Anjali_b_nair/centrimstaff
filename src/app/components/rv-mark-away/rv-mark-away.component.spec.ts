import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvMarkAwayComponent } from './rv-mark-away.component';

describe('RvMarkAwayComponent', () => {
  let component: RvMarkAwayComponent;
  let fixture: ComponentFixture<RvMarkAwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvMarkAwayComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvMarkAwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
