import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-mark-away',
  templateUrl: './rv-mark-away.component.html',
  styleUrls: ['./rv-mark-away.component.scss'],
})
export class RvMarkAwayComponent implements OnInit {
@Input() id;
@Input() consumer;
@Input() flag;
sdate:any;
edate:any;
comment:any;
residents:any=[];
selected_res:any=[];
no_return:any=false;
date:any=new Date().toISOString();

  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private modalCntl:ModalController,private toastCtlr:ToastController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('con:',this.consumer);
    if(this.flag==1){
      this.id=this.consumer.user_id;
      this.sdate=new Date(this.consumer.move_out).toISOString();
      if(this.consumer.type==1&&this.consumer.move_in){
      this.edate=new Date(this.consumer.move_in).toISOString();
      }
      this.comment=this.consumer.comment;
      // if(this.consumer.mark_away.residents&&this.consumer.mark_away.residents.length){
      //   this.consumer.mark_away.residents.forEach(element => {
      //     this.selected_res.push(element.user_id)
      //   });
      
      // }
      if(this.consumer.type==1){
      this.no_return=false;
      }else{
        this.no_return=true;
      }
    }
    else{
      if(this.consumer.property_details&&this.consumer.property_details.contract_details){
    this.residents=this.consumer.property_details.contract_details.all_residents;
      }else{
        this.residents.push({user_details:{user_id:this.consumer.user.user_id,name:this.consumer.user.name}})
      }
    }
  }


  async save(){
    if(!this.selected_res.length&&this.flag!==1){
      this.presentAlert('Please select a resident.')
    }
    else if(!this.sdate){
      this.presentAlert('Please select start date.')
    }
    else if(!this.no_return&&!this.edate){
      this.presentAlert('Please select a return date.')
    }else if(!this.no_return&&moment(this.edate).isBefore(moment(this.sdate))){
      this.presentAlert('The return date must be a date after the start date.')
    }
    else{
  
    const uid = await this.storage.get("USER_ID");
    
    
    let url,body;
    let type,edate,edate1;
    if(!this.no_return){
      type=1;
      edate=moment(this.edate).format('YYYY-MM-DD HH:mm:ss');
      edate1=moment(this.edate).format('YYYY-MM-DD');
    }else{
      type=0
      edate=null;
      edate1=null
    }
    if(this.flag==1){
      url=this.config.domain_url+'update_mark_away';
      body={
        move_inEdit:edate1,
        move_outEdit:moment(this.sdate).format('YYYY-MM-DD'),
        type:type,
        comment:this.comment,
        id:this.consumer.id,
        user_id:this.id,
        
       }
    }else{
    url=this.config.domain_url+'mark_away';
    body={
      move_in:edate,
      move_out:moment(this.sdate).format('YYYY-MM-DD HH:mm:ss'),
      type:type,
      comment:this.comment,
      residents:this.selected_res,
      user_id:this.id,
      created_by:uid
     }
    }
    let headers=await this.config.getHeader();
    
   

   console.log('body:',body,url)
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('markaway:',res);
      if(res.status=='error'){
        this.presentAlert('Already marked as away in this date range')
      }else{
      if(this.flag==1){
        this.presentAlert('Updated successfully')
      }else{
        this.presentAlert("Added successfully")
      }
    
      this.modalCntl.dismiss();
  }
    })
  }
  }

  dismiss(){
    this.modalCntl.dismiss();
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }


}
