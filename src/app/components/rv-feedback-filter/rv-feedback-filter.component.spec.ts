import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvFeedbackFilterComponent } from './rv-feedback-filter.component';

describe('RvFeedbackFilterComponent', () => {
  let component: RvFeedbackFilterComponent;
  let fixture: ComponentFixture<RvFeedbackFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvFeedbackFilterComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvFeedbackFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
