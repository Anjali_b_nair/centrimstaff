import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import moment from 'moment';
import { RvSelectStaffComponent } from '../rv-select-staff/rv-select-staff.component';

@Component({
  selector: 'app-rv-maintenace-filter',
  templateUrl: './rv-maintenace-filter.component.html',
  styleUrls: ['./rv-maintenace-filter.component.scss'],
})
export class RvMaintenaceFilterComponent implements OnInit {

  @Input()date_filter;
  @Input()risk;
  @Input()status;
  @Input()sdate;
  @Input()edate;
  @Input()assignedTo;
  @Input()staff;
  @Input()category;
  date:any=new Date().toISOString();
    constructor(private modalCntl:ModalController,private popCntl:PopoverController) { }
  
    ngOnInit() {}
  
  ionViewWillEnter(){
    console.log('fil:',this.date_filter,this.assignedTo);
    
    this.edate=new Date(this.edate).toISOString();
      this.sdate=new Date(this.sdate).toISOString();
  }
    dismiss(){
      this.modalCntl.dismiss();
    }
  
    save(){
      
      if(this.date_filter==0){
        this.sdate=moment().format('YYYY-MM-DD');
        this.edate=moment().format('YYYY-MM-DD');
      }else if(this.date_filter==1){
        this.edate=moment().subtract(1,'days').format('YYYY-MM-DD');
        this.sdate=moment().subtract(1,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==2){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(7,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==3){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==4){
        this.edate=moment().endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==5){
        this.edate=moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==6){
        this.sdate=moment(this.sdate).format('YYYY-MM-DD');
        this.edate=moment(this.edate).format('YYYY-MM-DD');
      }
      const onClosedData={
        date_filter:this.date_filter,
        risk:this.risk,
        status:this.status,
        sdate:this.sdate,
        edate:this.edate,
        assignedTo:this.assignedTo,
        staff:this.staff,
        category:this.category
      };
      this.modalCntl.dismiss(onClosedData);
    }
  
  async openAssignedTo(){
    console.log('assigned modal');
    
    const popover = await this.popCntl.create({
      component:RvSelectStaffComponent,
      cssClass:'rv-select-staf-pop',
      backdropDismiss:true,
      componentProps:{
        data:this.assignedTo,
        flag:0,
        status:2
  
      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data){
        if(dataReturned.data=='no_data'){
          this.assignedTo=[];
          this.staff=undefined
        }else{
        this.assignedTo=dataReturned.data.id;
          
        this.staff=dataReturned.data.name
        }
          }
   
   
      
    });
    return await popover.present();
  
  }

  removeUser(){
    this.staff=null;
    this.assignedTo=[];
    
  }

}
