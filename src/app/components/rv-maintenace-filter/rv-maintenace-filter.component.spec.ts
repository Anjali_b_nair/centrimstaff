import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvMaintenaceFilterComponent } from './rv-maintenace-filter.component';

describe('RvMaintenaceFilterComponent', () => {
  let component: RvMaintenaceFilterComponent;
  let fixture: ComponentFixture<RvMaintenaceFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvMaintenaceFilterComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvMaintenaceFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
