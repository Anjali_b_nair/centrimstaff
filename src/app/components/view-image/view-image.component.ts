import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { IonSlides, ModalController, PopoverController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { UtbVdoComponent } from '../utb-vdo/utb-vdo.component';
@Component({
  selector: 'app-view-image',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.scss'],
})
export class ViewImageComponent implements OnInit {
  @Input() data;
  @Input() video;
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
 
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    
  };
  sliderOne: any;
  content:any=[];
  viewEntered = false;
  constructor(private streamingMedia:StreamingMedia,private iab:InAppBrowser,private modalCntl:ModalController,
    private orient:ScreenOrientation,private popOver:PopoverController) { }

  ngOnInit() {
    console.log(this.data);
    this.content=[];
  if(this.data.length==0){
    this.video.forEach(element => {
      let url;
      let yurl;
      if(element.file==null){
        if(element.url.includes('www.youtube.com')){
          yurl=(element.url).replace('watch?v=','embed/');
         }else if(element.url.includes('youtu.be')){
          let id=(element.url).substr(element.url.lastIndexOf('/') + 1);
          yurl='https://www.youtube.com/embed/'+id
         }else{
           yurl=element.url
         }
         url=yurl
      }
      else{
        url=element.file;
      }
      let item={img:element.thumbnail,vid:1,url:url};
      this.content.push(item);
    });
  }else if(this.video.length==0){
    this.data.forEach(element => {
      let item={img:element.post_attachment,vid:0,url:null};
      this.content.push(item);
    });
  }else{
    this.data.forEach(element => {
      let item={img:element.post_attachment,vid:0,url:null};
      this.content.push(item);
    });
    this.video.forEach(element => {
      let url;
      let yurl;
      if(element.file==null){
        if(element.url.includes('www.youtube.com')){
          yurl=(element.url).replace('watch?v=','embed/');
         }else if(element.url.includes('youtu.be')){
          let id=(element.url).substr(element.url.lastIndexOf('/') + 1);
          yurl='https://www.youtube.com/embed/'+id
         }else{
           
           yurl=element.url
         }
         url=yurl;
        // url=element.url;
      }
      else{
        url=element.file;
      }
      let item={img:element.thumbnail,vid:1,url:url};
      this.content.push(item);
    });
  }
  this.sliderOne =
  {
    isBeginningSlide: true,
    isEndSlide: false,
    isActive:false,
    content:[]=this.content
  };
  console.log('img:',this.data);
  console.log('vdo:',this.video);
  console.log("con:",this.content);
  }
ionViewWillEnter(){
  this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);

  
  
}
ionViewDidEnter() {
  this.viewEntered = true;
}
  slideNext(object,slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object,slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
    SlideDidChange(object,slideView) {
      this.checkIfNavDisabled(object, slideView);
      object.isActive= true;
    }
    checkIfNavDisabled(object, slideView) {
      this.checkisBeginning(object, slideView);
      this.checkisEnd(object, slideView);
    }
   
    checkisBeginning(object, slideView) {
      slideView.isBeginning().then((istrue) => {
        object.isBeginningSlide = istrue;
      });
    }
    checkisEnd(object, slideView) {
      slideView.isEnd().then((istrue) => {
        object.isEndSlide = istrue;
      });
    }



    playVdo(attachment){
      console.log("videoplaying")
      if(attachment!=null){

        if((attachment.includes('vimeo.com'))){
        let options:InAppBrowserOptions ={
          location:'no',
          hideurlbar:'yes',
          zoom:'no'
        }
        screen.orientation.lock('landscape');
          
        const browser = this.iab.create(attachment,'_blank',options);
        browser.on('exit').subscribe(()=>{
          screen.orientation.unlock();
          screen.orientation.lock('portrait');
        })
      }else if((attachment.includes('www.youtube.com'))){
        this.playUtb(attachment)
      }else{
        let options: StreamingVideoOptions = {
          successCallback: () => { console.log('Video played')

          
         },
          errorCallback: (e) => { console.log('Error streaming')
          
         },
          
          orientation: 'landscape',
          shouldAutoClose: true,
          controls: true
        };
      
        this.streamingMedia.playVideo(attachment, options);
        }
      }
    }





    async playUtb(link){
      const modal = await this.modalCntl.create({
        component: UtbVdoComponent,
        cssClass:"vdoModal",
        componentProps: {
         data:link,
          
            
        }
      });
      modal.onDidDismiss().then(()=>{
        this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
      })
      return await modal.present();
    }
    async dismiss(){
      await this.popOver.dismiss();
    }
}
