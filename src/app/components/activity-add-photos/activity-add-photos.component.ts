import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';

import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-activity-add-photos',
  templateUrl: './activity-add-photos.component.html',
  styleUrls: ['./activity-add-photos.component.scss'],
})
export class ActivityAddPhotosComponent {
imageResponse:any[]=[];
img:any[]=[]
isLoading:boolean=false;
disable:boolean=true;
@Input() id;
  constructor(private modalCntl:ModalController,private http:HttpClient,private storage:Storage,private config:HttpConfigService,
    private actionsheetCntlr:ActionSheetController,
    private loadingCtrl:LoadingController,private toastCntlr:ToastController,private platform:Platform) { }

 

  async dismiss(){
    this.modalCntl.dismiss();
  }


  async add() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.captureImage();
          // this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  async captureImage(){
  
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   // let base64Image = 'data:image/jpeg;base64,' + imageData;
    //   this.imageResponse.push('data:image/jpeg;base64,' + imageData)
    //           this.uploadImage('data:image/jpeg;base64,' +imageData);
      
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.uploadImage(base64Image);

  }
  
 
  
  async addImage(){
  
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1,
    //   quality:100
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    //   this.imagePicker.getPictures(options).then((results) => {
    //     for (var i = 0; i < results.length; i++) {
    //         // console.log('Image URI: ' + results[i]);
            
    //         if((results[i]==='O')||results[i]==='K'){
    //           console.log("no img");
              
    //           }else{
    //             // this.img.push(results[i]);
    //           this.imageResponse.push('data:image/jpeg;base64,' + results[i])
    //           this.uploadImage('data:image/jpeg;base64,' +results[i]);
    //           }
    //     }
    //   }, (err) => { });
    
    // // console.log("imagg:",this.img);
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    
    }

    async uploadImage(img){
      let headers=await this.config.getHeader();;
      if(this.isLoading==false){
      this.showLoading();
      }
      let url=this.config.domain_url+'upload_file';
      let body={file:img}
    console.log("body:",img);
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("uploaded:",res);
        this.img.push(res.data);
        this.dismissLoader();
        this.disable=false;
      },error=>{
        // this.imageResponse.slice(0,1);
        console.log(error);
        this.dismissLoader();
      })
    }
    async showLoading() {
      this.isLoading=true;
      const loading = await this.loadingCtrl.create({
        cssClass: 'custom-loading',
        message: 'Please wait...',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
      this.isLoading=false;
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }

    async submit(){
      let headers=await this.config.getHeader();;
      this.disable=true
      let body={
        activity_id:this.id,
        images:this.img
      }

      console.log('body:',body);
      
      let url=this.config.domain_url+'upload_activity_photos';
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        this.presentAlert('Photos added successfully.');
        this.modalCntl.dismiss();
      })
    }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }

    removeImg(i){
      
        this.imageResponse.splice(i,1);
        this.img.splice(i,1);
      
    }
}
