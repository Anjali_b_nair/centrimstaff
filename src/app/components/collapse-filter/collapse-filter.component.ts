import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild  } from '@angular/core';

@Component({
  selector: 'app-collapse-filter',
  templateUrl: './collapse-filter.component.html',
  styleUrls: ['./collapse-filter.component.scss'],
})
export class CollapseFilterComponent implements AfterViewInit {
  @ViewChild("expandWrapper", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded1") expanded1: boolean = false;
  @Input("expanded2") expanded2: boolean = false;
  @Input("expanded3") expanded3: boolean = false;
  @Input("expanded4") expanded4: boolean = false;
  @Input("expanded5") expanded5: boolean = false;
  @Input("expanded6") expanded6: boolean = false;
  @Input("expanded7") expanded7: boolean = false;
  @Input("expanded8") expanded8: boolean = false;

  @Input("expandHeight") expandHeight: string = "";
  constructor(public renderer: Renderer2) { }

  
  ngAfterViewInit() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }
}
