import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';
import { CbReassignComponent } from '../cb-reassign/cb-reassign.component';
import { CbRescheduleComponent } from '../cb-reschedule/cb-reschedule.component';

@Component({
  selector: 'app-cb-options',
  templateUrl: './cb-options.component.html',
  styleUrls: ['./cb-options.component.scss'],
})
export class CbOptionsComponent implements OnInit {
@Input() data:any;
@Input() cdate:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private popCntrl:PopoverController,
    private alertCnlr:AlertController,private toastCntlr:ToastController,private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {}




  cancel(){
    this.presentAlertConfirm();
    }
    
    
    async presentAlertConfirm() {
      const alert = await this.alertCnlr.create({
        mode:'ios',
        header: 'Cancel booking',
        message: 'Are you sure you want to cancel this booking ?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntrl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
             
              this.cancelBooking();
            }
          }
        ]
      });
    
      await alert.present();
    }
    
     async cancelBooking(){
        
          const bid=await this.storage.get("BRANCH")

        let url=this.config.domain_url+'booking/'+this.data.id;
        console.log(url);
        
        let headers=await this.config.getHeader();
    
        this.http.delete(url,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.presentAlert('Booking cancelled successfully.')
          this.popCntrl.dismiss();
        })
     
    
      }

      async reschedule(){
        // this.popCntrl.dismiss();
        const popover = await this.modalCntl.create({
          component: CbRescheduleComponent,
          // event:ev,
          backdropDismiss:true,
          componentProps:{
            data:this.data,
            cdate:this.cdate
          },
          cssClass:'cb-reschedule-modal'
          
          
        });
        popover.onDidDismiss().then(()=>{
          this.popCntrl.dismiss();
        })
        return await popover.present();
      }

      async resend(){

        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'resend_callbooking/'+this.data.id;
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log(res);
           this.presentAlert('Invitation has been sent successfully. ')
          this.popCntrl.dismiss();
        })

      }

      async reassign(){
        // this.popCntrl.dismiss();
        const popover = await this.popCntrl.create({
          component: CbReassignComponent,
          // event:ev,
          backdropDismiss:true,
          componentProps:{
            data:this.data
          },
          cssClass:'morepop'
          
          
        });
        popover.onDidDismiss().then(()=>{
          this.popCntrl.dismiss();
        })
        return await popover.present()
      }
      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass:'toastStyle',
          duration: 3000,
          position:'top'      
        });
        alert.present(); //update
      }
}
