import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActivityDeclinedReasonComponent } from './activity-declined-reason.component';

describe('ActivityDeclinedReasonComponent', () => {
  let component: ActivityDeclinedReasonComponent;
  let fixture: ComponentFixture<ActivityDeclinedReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDeclinedReasonComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActivityDeclinedReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
