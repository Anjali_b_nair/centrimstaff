import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-quick-links',
  templateUrl: './rv-quick-links.component.html',
  styleUrls: ['./rv-quick-links.component.scss'],
})
export class RvQuickLinksComponent implements OnInit {
  maintenance_view: boolean = false;
  act_view: boolean = false;
  feedback:boolean=false;
  task_view: boolean = false;
  constructor(private modalCntl:ModalController,private config:HttpConfigService) { }

  ngOnInit() {}

  dismiss(i){
    this.modalCntl.dismiss(i);
  }

  async ionViewWillEnter(){
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.create')) {

        this.maintenance_view = true
      } else {

        this.maintenance_view = false;
      }

   

      // activity permissions
      if (res.main_permissions.life_style==1&&routes.includes('activity.create')) {

        this.act_view = true
      } else {

        this.act_view = false;
      }

      if (res.main_permissions.feedback==1&&routes.includes('PCF')) {

        this.feedback = true
      } else {

        this.feedback = false;
      }
      if (res.main_permissions.rv==1&&routes.includes('task.create')) {

        this.task_view = true
      } else {

        this.task_view = false;
      }
    })
  }

}
