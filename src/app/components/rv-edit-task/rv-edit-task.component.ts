import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ResidentComponent } from '../resident/resident.component';
import { RvSelectStaffComponent } from '../rv-select-staff/rv-select-staff.component';

@Component({
  selector: 'app-rv-edit-task',
  templateUrl: './rv-edit-task.component.html',
  styleUrls: ['./rv-edit-task.component.scss'],
})
export class RvEditTaskComponent implements OnInit {
@Input() data;
@Input() companian;
@Input() flag;
type:any;
date:any;
priority:any;
consumer:any;
assignedTo:any=[];
staff:any;
title:any;
description:any;
cid:any;
residents:any=[];
resident_name:any;
minDate:any=new Date().toISOString();
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private toastCntlr:ToastController,private popCntl:PopoverController,private modalCntrl:ModalController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
   
    const tz = await this.storage.get("TIMEZONE");
    this.residents=[];
    this.type=this.data.type.toString();
    // this.date=this.data.due_date;
    let d=moment.utc(this.data.date_time).format();
    let date=moment(d).tz(tz).format();
    // this.date =new Date(moment(date).format('DD MMM YYYY, hh:mm A')).toISOString();
    
    this.date=new Date(moment.utc(this.data.date_time).tz(tz).format('DD MMM YYYY ,hh:mm A')).toISOString();
    console.log('data:',this.data,this.date);
    this.priority=this.data.priority.toString();
    
    if(this.data.assignee&&this.data.assignee.length){
      this.data.assignee.forEach(element => {
        this.assignedTo.push(element.user_id);
        
      });
     
        this.staff=this.data.assignee[0].name
       if(this.data.assignee.length>1){
        this.staff=this.staff+', '+this.data.assignee[1].name
      }
    }

    if(this.flag==3){
      
      
      this.residents=await this.data.assigned_resident.map(x=>x.user.user_id.toString());
      this.resident_name=this.data.assigned_resident[0].user.name;
      if(this.data.assigned_resident.length>1){
        this.data.assigned_resident.forEach(element => {
          if(this.resident_name==element.user.name){

          }else{
            this.resident_name=this.resident_name+','+element.user.name
          }
        });
      }
    }else{
      if(this.data.assigned_resident&&this.data.assigned_resident.length&&(!this.companian||!this.companian.length)){
        console.log('reached');
        
        this.consumer=this.data.assigned_resident[0].user.name;
        this.cid=this.data.assigned_resident[0].user.user_id
      }else{
        console.log('reach here');
        
        this.residents=await this.data.assigned_resident.map(x=>x.user.user_id.toString());
  
        this.cid=this.data.assigned_resident[0].user.user_id
      }
    }
 
    this.title=this.data.title;
    this.description=this.data.description;
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async save(){

    const bid = await this.storage.get("BRANCH");
    const uid = await this.storage.get("USER_ID");
    const cid = await this.storage.get("COMPANY_ID");
    const tz= await this.storage.get('TIMEZONE');
    if(!this.title||this.title==''){
      this.presentAlert('Please enter the title.')
    }else{
      let url=this.config.domain_url+'update_resident_task';
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers);
    let res=[];
    if(this.flag==3){
      res=this.residents.map(x=>parseInt(x))
    }else{
    if(this.companian&&this.companian.length){
     if(this.residents.length){
      res=this.residents.map(x=>parseInt(x))
     }else{
      res.push(parseInt(this.cid))
     }
    }else{
      res.push(parseInt(this.cid))
    }
  }
    let date,due,due_date;
    let currentDate = new Date();
          if (this.priority == 2) {
            due = currentDate
          } else if (this.priority == 1) {
            due = new Date(Date.now() + 2 * 24 * 60 * 60 * 1000);
          } else if (this.priority == 0) {
            due = new Date(Date.now() + 3 * 24 * 60 * 60 * 1000);
          }
          date = due.getFullYear() + '-' + this.fixDigit((due.getMonth() + 1)) + '-' + this.fixDigit(due.getDate());
          // due_date=moment(date).tz(tz).format('YYYY-MM-DD');
          due_date=moment(this.date).format('YYYY-MM-DD');
          // let date_time=moment(this.date).tz(tz);
          var s = moment.tz(this.date,      // input date time string
          'YYYY-MM-DD hh:mm:ss',      // format of input
          tz);
          let body={
      date_time:moment.utc(s).format('YYYY-MM-DD HH:mm:ss'),
      due_date:due_date,
      title:this.title,
      priority:this.priority,
      description:this.description,
      type:this.type,
      created_by:uid,
      created_time:moment().format('YYYY-MM-DD HH:mm:ss'),
      resident_user_id:res,
      assignee:this.assignedTo,
      user_id:uid,
      id:this.data.id
     

    }

    console.log('body:',body);
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     
      this.presentAlert('Task updated successfully.')
      this.modalCntl.dismiss();
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
    }
  }


  fixDigit(val) {
    return val.toString().length === 1 ? "0" + val : val;
  }

  async openAssignedTo(){
  
    const popover = await this.popCntl.create({
      component:RvSelectStaffComponent,
      cssClass:'rv-select-staf-pop',
      backdropDismiss:true,
      componentProps:{
        data:this.assignedTo,
        flag:1,
        status:1
  
      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data){
        this.assignedTo=dataReturned.data.id;
        if(dataReturned.data.name.length>1){
          this.staff=dataReturned.data.name[0]+', '+dataReturned.data.name[1];
        }else{
          this.staff=dataReturned.data.name[0]
        }
      }
   
   
      
    });
    return await popover.present();
  

  }

  dismiss(){
    this.modalCntl.dismiss();
  }


  async openResident() {
   
   
    const bid = await this.storage.get("BRANCH");
    const popover = await this.modalCntrl.create({
      component: ResidentComponent,

      componentProps: {
        data: this.residents,
        rv:1,
        bid:bid,
        name:this.resident_name,
        flag:2
      },
      cssClass:'fullWidthModal'
    });

    popover.onDidDismiss().then((dataReturned) => {
      console.log('res:',dataReturned);
      if (dataReturned.data != undefined) {
        
        this.residents=dataReturned.data.data;
       
        if(dataReturned.data.name.length>1){
          this.resident_name = dataReturned.data.name[0]+','+dataReturned.data.name[1];
        }else{
          this.resident_name=dataReturned.data.name[0]
        }
       
      }else{
        if(dataReturned.role!='backdrop'){
        this.residents = [];
        this.resident_name = undefined;
       
        }
      }
      console.log("data:", dataReturned);

    });
    return await popover.present();
  
  }
}
