import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CbDisabletimeslotComponent } from './cb-disabletimeslot.component';

describe('CbDisabletimeslotComponent', () => {
  let component: CbDisabletimeslotComponent;
  let fixture: ComponentFixture<CbDisabletimeslotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbDisabletimeslotComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CbDisabletimeslotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
