import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';
import moment from 'moment';
@Component({
  selector: 'app-cb-disabletimeslot',
  templateUrl: './cb-disabletimeslot.component.html',
  styleUrls: ['./cb-disabletimeslot.component.scss'],
})
export class CbDisabletimeslotComponent implements OnInit {
  minDate: string = new Date().toISOString();
  date:any;
  ar_avl: Array<string>;
  slot:any=[];
  start:any;
  end:any;
  hours;
  minutes;
 
  not_available:any=[];
  disableButton:boolean=true;
  staffArray:any[]=[];
staff:any;
staff_id:any;
sel_slot:any[]=[];
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,private popCntl:ModalController) { }

  ngOnInit() {}


  async ionViewWillEnter(){
    this.sel_slot=[];
    console.log("disable time slot component");
    
    this.date=new Date().toISOString();
    this.staffArray=[];
    this.staff='All';
  this.staff_id='all';
  this.staffArray.push({user_id:this.staff_id,name:this.staff})
   
      const data=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")


  
        
        let headers=await this.config.getHeader();
        // let url= this.config.domain_url+'company_details/'+data;
        let url= this.config.domain_url+'show_branch_call_time/'+bid;
        let body={company_id:data}
        console.log('body:',body,'head:',bid);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("res:",res);
          this.start=res.branch.call_start_time;
          this.end=res.branch.call_end_time;
          this.getAvailableTimeSlots();
          
          // this.slot=this.timeArray(this.start,this.end,30);
        },error=>{
          console.log(error);
          
        })


        let url1=this.config.domain_url+'staff_viabranch';
        let body1={company_id:data}
        this.http.post(url1,body1,{headers}).subscribe((res:any)=>{
          console.log("arr:",res);
          res.data.forEach(element => {
            this.staffArray.push({user_id:element.user_id,name:element.name})
          });
          
        })
     
    
  }

  setstaff($event){
    this.staffArray.forEach(element => {
      if(element.name==this.staff){
        this.staff_id=element.user_id;
  
      }
    });
  }

  onChange(ev){
    console.log("cdate:",this.date);
   this.getAvailableTimeSlots();
    // this.date_1=this.date;
    // this.getAvailableTimeSlots();
    
    // this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
    // let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
    // this.date=new Date(d);
   
  }

  // getAvailableTimeSlots(){
   
  //         this.slot=this.timeArray(this.start,this.end,30);
        
  // console.log("not:",this.not_available);

  // }
  
  //  method to fix month and date in two digits
  fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
  }
  
  timeArray(start, end,duration){
  console.log("start:",start,"end:",end);
  
  var start = start.split(":");
  var end = end.split(":");
  duration= parseInt(duration);
  start = parseInt(start[0]) * 60 + parseInt(start[1]);
  end = parseInt(end[0]) * 60 + parseInt(end[1]);
  console.log(start,end);
  
  var result = [];

  for (let time = start; time <= end; time+=duration){
      var slot=this.timeString(time);
      let disable;
     
          // alligator.includes("thick scales");
      console.log("arraaaa:",this.ar_avl,"sl:",slot)
      console.log("incl:",this.ar_avl.includes(slot));
      if(this.ar_avl.includes(slot)){
        disable=true;
      }else{
        disable=false;
      }

    result.push( {'slot':slot,'disable':disable});
    
  }
    console.log("result:",result);
    
  return result;
    
  }
    
  timeString(time){
  this.hours = Math.floor(time / 60);
  let h=Math.floor(time / 60);
  this.minutes = time % 60;
  if(this.hours>12){
  this.hours=this.hours-12 ;
  }
  if (this.hours < 10) {
   this.hours = "0" + this.hours; //optional
  }
  
  if (this.minutes < 10){
    this.minutes = "0" + this.minutes;
  }
  
  if(h>=12){
  return this.hours + ":" + this.minutes +' PM';
  }else{
  return this.hours + ":" + this.minutes +' AM';
  }
  }
  
bookCall(item){
  console.log("slot:",item.slot);
  
  this.disableButton=false;
  this.sel_slot.push(item.slot)
}

  cancel(){
    this.popCntl.dismiss();
  }


  async confirm(){
   
      const bid=await this.storage.get("BRANCH")

    let url=this.config.domain_url+'make_disable_timeslots';
    let headers=await this.config.getHeader();
    let date=moment(this.date).format('yyyy-MM-DD');
    let time;
    let slot=this.sel_slot[0];
    this.sel_slot.forEach(ele=>{
      // let a=ele.split(' ');
      // let t=a[0].split(':');
      
      // if(a[1]=='AM'){
      // time=a[0]
      // }else{
      //    time=(parseInt(a[0])+12)+':'+t[1]
      // }
      if(ele==slot){

      }else{
        slot=slot+','+ele
      }
    })
    console.log("slotssss:",slot);
    
    // let slot=this.sel_slot
   
      const data=await this.storage.get("USER_ID")

        let body={
            date:date,
            slots:slot,
            staff_id:this.staff_id,
            creator:data
        }
        console.log("body:",body);
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          if(res.status=='success'){
            this.popCntl.dismiss();
          }
          
        })
     
  }
  async getAvailableTimeSlots(){
    this.not_available=[];
    
      const data=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")

          const id=await this.storage.get("USER_ID")

        let headers=await this.config.getHeader();
        let url= this.config.domain_url+'timeslot_notavailable_staff';
        
        let body={
          company_id:data,
          date:moment(this.date).format('yyyy-MM-DD'),
          duration:1,
          staff:'all'
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("res:",res)
          res.data.forEach(element => {
            console.log(element);
            let t={slots:element}
            this.not_available.push(t);
            
            
          });
          this.ar_avl= res.json_data
          // this.not_available.push(res.data));
          console.log("fff:",this.ar_avl)
          this.slot=this.timeArray(this.start,this.end,30);
        },error=>{
          console.log(error);
        })
        
  console.log("not:",this.not_available);
  // this.ar_avl= this.not_available.map(p => p.slots)
  
  
  
  }

  
}
