import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, NgModule, OnInit } from '@angular/core';
import { LoadingController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;

import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendanceGroupComponent } from '../activity-attendance-group/activity-attendance-group.component';



@Component({
  selector: 'app-activity-attendees',
  templateUrl: './activity-attendees.component.html',
  styleUrls: ['./activity-attendees.component.scss'],
  
})
export class ActivityAttendeesComponent implements OnInit {
selected:boolean=true;
consumers:any[]=[];
con:any[]=[];
terms:any;
family:any=[];
other:any[]=[];
sel_id:any[]=[];
temp:any[]=[];
rem:any[]=[];
all:boolean=false;
sel_fam:any[]=[];
@Input() data:any;
@Input()slot;
@Input()waiting;
@Input()limit_required;
count:any;
rv:any;
  constructor(private config:HttpConfigService,private http:HttpClient,private toastCntlr:ToastController,
    private modalCntl:ModalController,private storage:Storage,private popCntrl:PopoverController,
    private loadingCtrl:LoadingController) { }

  ngOnInit() {}
async ionViewWillEnter(){
  
    const bid=await this.storage.get("BRANCH");
    this.rv=await this.storage.get('RVSETTINGS');
if(this.slot){
  this.count=this.slot
}else{
  this.count=0
}

console.log('det:',this.limit_required,this.waiting,this.count,this.data);

  this.terms='';
  this.showLoading();
              this.sel_id=[];
              this.temp=[];
              this.sel_fam=[];
              if(this.data&&this.data.length){
                this.sel_id=this.data;
                this.temp=this.data;
                if(this.sel_id.length<this.slot){
                this.count=this.slot-this.sel_id.length
                }else{
                  this.count=0;
                }
              }else{
                this.sel_id=[];
              }
    let url=this.config.domain_url+'residents';
    let headers=await this.config.getHeader();
    console.log(url,this.sel_id);
  // this.selected=true;
    // let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((data:any)=>{
     
      
      data.data.forEach(element => {
        this.consumers.push({con:element,selected:false});
        this.con.push({con:element,selected:false});
      });
      
      console.log("data:",this.consumers);
        this.dismissLoader();
    },error=>{
      console.log(error);
      this.dismissLoader();
    });
 
  }

segmentChange(i){
  if(i==1){
    this.selected=true
  }else{
    this.selected=false;
    this.getFamily();
  }
}


async getFamily(){
  
    const bid=await this.storage.get("BRANCH")

  this.other=[];
  
    
    let url=this.config.domain_url+'resident_contact';
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((data:any)=>{ 
      console.log("fam:",data);
      data.data.forEach(element => {
      if(element.contacts.length>0){
      this.other.push(element);
      element.contacts.forEach(ele=>{
        var index = this.sel_fam.indexOf(ele.user.user_id);
        if(!index&&this.data&&this.data.includes(ele.user.user_id)){
        this.sel_fam.push(ele.user.user_id);
        }
    
      })
      }
    })
      });
  
  console.log("other:",this.other);
   
}

async save(){
  let attendees=this.sel_id.concat(this.sel_fam);
  if(!attendees.length){
    this.presentAlert('Please select attendees.')
  }else{
  await this.modalCntl.dismiss(attendees);
  }
  console.log("ond:",attendees);
  
}
async dismiss(){
  this.modalCntl.dismiss();
}

selectConsumer(ev,item){
  if(this.temp.includes(item.con.user.user_id)){
    var index = this.sel_id.indexOf(item.con.user.user_id);
    this.sel_id.splice(index, 1);
    console.log("index:",this.sel_id.indexOf(item.con.user.user_id))
    this.temp.splice(this.temp.indexOf(item.con.user.user_id),1);
    if(this.count<this.slot)
                  this.count=++this.count
  }else{
    if(this.limit_required==1&&!this.waiting&&this.slot<=this.sel_id.length+this.sel_fam.length){
      this.presentAlert('No more slots available.')
    }else{
      if(this.count>0)
                  this.count=--this.count;
                  if(this.limit_required==1&&this.waiting&&this.count<=0){
                    this.presentAlert('No slots available, adding to waiting list.')
                  }
    this.temp.push(item.con.user.user_id);
    }
    console.log('condition:',this.limit_required,this.waiting,this.slot,this.slot<=this.sel_id.length+this.sel_fam.length,this.sel_id.length,this.sel_fam.length);
    
  }
  this.sel_id=this.temp.reduce((arr,x)=>{
    let exists = !!arr.find(y => y === x);
    if(!exists){
        arr.push(x);
    }
    
    return arr;
}, []);
console.log("sel:",this.sel_id,this.temp);
}
// selectConsumer(ev,item){
//   item.selected=true;
//   if(this.temp.includes(item.con.user.user_id)){

//   }else{
//     this.temp.push(item.con.user.user_id);
//   }
//   this.sel_id=this.temp.reduce((arr,x)=>{
//     let exists = !!arr.find(y => y === x);
//     if(!exists){
//         arr.push(x);
//     }
    
//     return arr;
// }, []);
// console.log("sel:",this.sel_id,this.temp);

// }
unselectAll(ev){
  this.all=false;
  if(this.slot){
    this.count=this.slot
  }else{
    this.count=0
  }
  this.temp=[];
  this.sel_id=[];
}
selectAll(ev){
    this.all=true;
    
    this.consumers.forEach(element=>{
      if(this.temp.includes(element.con.user.user_id)){

      }else{
        if(this.limit_required==1&&!this.waiting&&this.slot<=this.sel_id.length+this.sel_fam.length){
          this.presentAlert('No more slots available.')
        }else{
        
        if(this.count>0)
                      this.count=--this.count;
        }
        this.temp.push(element.con.user.user_id);
      }
      
      this.sel_id=this.temp.reduce((arr,x)=>{
        let exists = !!arr.find(y => y=== x);
        if(!exists){
            arr.push(x);
        }
        return arr;
    }, []);
   
      // if (element.value == value) {
         element.selected = true;
      //    break; //Stop this loop, we found it!
      // }
    
    })
    console.log("sel:",this.sel_id);
    
}

remove(item){
  item.selected=false;
 

this.sel_id.forEach(ele=>{
  if(ele==item.con.user.user_id){
    var index = this.sel_id.indexOf(ele);
      this.sel_id.splice(index, 1);
      this.temp.splice(this.temp.indexOf(ele),1);
      if(this.count<this.slot)
      this.count=++this.count;
  }
  

})
console.log("rem:",this.sel_id);
  // this.rem.push({id:item.con.user.user_id});
  
  // this.sel_id=this.rem.reduce((arr,x)=>{
  // let exists = !!this.sel_id.find(y => y.id === x.id);
  // console.log("exist:",exists,arr);
  
  //     if(exists){
  //       var index = arr.indexOf(x.id);
  //     arr.splice(index, 1);
  //     }else{
      
  //   }

  //   return arr;
  //   }, []);
  //   var idx = this.sel_id.indexOf('90');
  //   if (idx != -1) {
  //     this.sel_id.splice(this.sel_id.indexOf(item.con.user.user_id), 1);
  // }
  
  
}

selectFam(ev,item){
  if(ev.currentTarget.checked==true){
  if(this.sel_fam.includes(item.user.user_id)){

  }else{
    if(this.limit_required==1&&!this.waiting&&this.slot<=this.sel_id.length+this.sel_fam.length){
      this.presentAlert('No more slots available.')
    }else{
      if(this.count>0)
                      this.count=--this.count;
      if(this.limit_required==1&&this.waiting&&this.count<=0){
        this.presentAlert('No slots available, adding to waiting list.')
      }
    this.sel_fam.push(item.user.user_id);
    
    }
  }
}else{
  var index = this.sel_fam.indexOf(item.user.user_id);
  var ind = this.sel_id.indexOf(item.user.user_id);
  this.sel_fam.splice(index, 1);
  this.sel_id.splice(ind,1);
  if(this.count<this.slot)
  this.count=++this.count
  console.log('unselect fam:',index,this.count);
  
}
console.log("selfam:",this.sel_fam);

}
cancel(){
  this.terms='';
}



async byGroup(ev){
  let headers=await this.config.getHeader();
  const popover = await this.modalCntl.create({
    component: ActivityAttendanceGroupComponent,
   
    backdropDismiss:true,
    cssClass:'act-res-group-modal'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned == null || dataReturned.data ==undefined) {
    }else{
      let id = dataReturned.data;
      this.consumers=[];
      if(id!=0){
      let url=this.config.domain_url+'group/'+id;
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log("mem:",res);
  
    
    res.data.residents.forEach(element => {
  
      this.consumers.push({con:element,selected:false});
    });
    
  })
}else{
  this.consumers=this.con;
}
    }
  
  });
  return await popover.present();
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
}

