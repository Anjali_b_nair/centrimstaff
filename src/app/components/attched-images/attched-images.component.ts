import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-attched-images',
  templateUrl: './attched-images.component.html',
  styleUrls: ['./attched-images.component.scss'],
})
export class AttchedImagesComponent implements OnInit {
@Input() data;
@ViewChildren('slideWithNav') slideWithNav: IonSlides;
 
slideOptsOne = {
  initialSlide: 0,
  slidesPerView: 1,
  
};
sliderOne: any;
content:any=[];
viewEntered = false;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {
    console.log("data:",this.data)
    this.sliderOne =
  {
    isBeginningSlide: true,
    isEndSlide: false,
    isActive:false,
    content:[]=this.data
  };
  console.log("content:",this.sliderOne);
  }
  ionViewDidEnter() {
    this.viewEntered = true;
  }
    slideNext(object,slideView) {
      slideView.slideNext(500).then(() => {
        this.checkIfNavDisabled(object, slideView);
      });
    }
  
    //Move to previous slide
    slidePrev(object,slideView) {
      slideView.slidePrev(500).then(() => {
        this.checkIfNavDisabled(object, slideView);
      });;
    }
      SlideDidChange(object,slideView) {
        this.checkIfNavDisabled(object, slideView);
        object.isActive= true;
      }
      checkIfNavDisabled(object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
      }
     
      checkisBeginning(object, slideView) {
        slideView.isBeginning().then((istrue) => {
          object.isBeginningSlide = istrue;
        });
      }
      checkisEnd(object, slideView) {
        slideView.isEnd().then((istrue) => {
          object.isEndSlide = istrue;
        });
      }


      async dismiss(){
        this.modalCntl.dismiss();
      }
}
