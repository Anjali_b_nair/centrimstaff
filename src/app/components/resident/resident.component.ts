import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-resident',
  templateUrl: './resident.component.html',
  styleUrls: ['./resident.component.scss'],
})
export class ResidentComponent implements OnInit {
  consumers:any[]=[];
  @Input() data;
  @Input()name;
  terms:any;
  @Input() rv;
  @Input()bid;
  check:any;
  offset:any=1;
  @Input() flag;
  // residents_id:any=[]=[];
  res_name:any=[]=[];
  constructor(private modalCntrl:ModalController,private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private loadingCtrl:LoadingController,private toastCntl:ToastController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
   
          const tok=await this.storage.get("TOKEN")
          this.showLoading();
          this.getAllresidents(false,'');
          if(this.data){
                  this.check=this.data;
          }
if(this.flag==2&&this.name&&this.name.length){
          this.res_name=this.name.split(',');
}
          console.log('datacom:',this.data,this.res_name);
          
  //           let url=this.config.domain_url+'residents';
  // let headers=await this.config.getHeader();
  
  //   this.http.get(url,{headers}).subscribe((res:any)=>{
  //     console.log('res:',res,url)
  //     this.consumers=res.data
  //     if(this.data){
  //       this.check=this.data;
  //       this.consumers.forEach(ele=>{
  //         if(this.data==ele.user.user_id){
  //           this.name=ele.user.name
  //         }
  //       })
  //     }
  //     this.dismissLoader()
  //   },error=>{
  //     console.log(error);
  //     this.dismissLoader();
  //   })
 
  }
  async dismiss(){
    if(this.flag==1&&this.rv==1&&!this.data){
          this.modalCntrl.dismiss('no_data')
    }else{
    if(this.check){
      let name;
      if(this.flag==2){
        name=this.res_name;
      }else{
        name=this.name;
      }
    const onClosedData:any={data:this.data,name:name}
    this.modalCntrl.dismiss(onClosedData)
    }else{
    this.modalCntrl.dismiss();
    }
  }
  }

  
  select(id,name){
   
    //  if(this.data==id){
    //   this.data=undefined;
    //   this.name=undefined;
    //  }else{
      if(this.flag==1){
        if(this.flag==1&&this.rv==1&&this.data==id){
          
            this.data=undefined;
            this.name=undefined;
          
        }else{
       this.data=id;
       this.name=name
       const onClosedData:any={data:this.data,name:this.name}
       this.modalCntrl.dismiss(onClosedData)
        }
      }else{
        if(this.data.includes(id.toString())){
          const idx=this.data.indexOf(id);
          this.data.splice(idx,1);

          const ind=this.res_name.indexOf(this.res_name);
          this.res_name.splice(ind,1);
          
        }else{
          this.data.push(id.toString());
          this.res_name.push(name);
        }
      }
      console.log('selected:',this.data,this.res_name);
      
    //  }
   }


   submit(){
    if(!this.data||!this.data.length){
      this.presentAlert('Please select a resident')
    }else{
   
    const onClosedData:any={data:this.data,name:this.res_name}
  this.modalCntrl.dismiss(onClosedData)
    }
  }

  cancel(){
    this.terms=null;
  }
  search(){
  
    this.offset=1;
    this.consumers=[];
    this.getAllresidents(false,'')
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async getAllresidents(isFirstLoad,event){
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
    const type=await this.storage.get('USER_TYPE');

    let url=this.config.domain_url+'get_all_residents_list_via_filters';
                 
    let headers=await this.config.getHeader();

    let body;
    body={
      type:1,  // 1 - active 2 -inactive 3 - no contract
      sort:'fname',  // sortby
      order:'ASC',   // ASC or DESC
    
      page:this.offset, // 0   -- increment by 20
      
    }

    if(this.terms){
    body.keyword=this.terms
    }
                 console.log('body:',body,cid,bid);
                 
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
                   
      console.log("data:",data);

      // for (let i = 0; i < data.data.residents.length; i++) {
      //   this.consumers.push(data.data.residents[i]);
      // }
      for(let i in data.data.residents){
        this.consumers.push(data.data.residents[i]);
      }

      if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      this.offset=++this.offset;    
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });

  }
  doInfinite(event) {
    this.getAllresidents(true, event)
    }
    selectedResidents(id){
      let selected=false;
      if(Array.isArray(this.data)){
      if(this.data.includes(id.toString())){
        selected=true
      }
      }else{
        if(this.data==id){
          selected=true
        }
      }
      return selected;
    }
    async presentAlert(mes) {
      const alert = await this.toastCntl.create({
        message: mes,
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }
  
}
