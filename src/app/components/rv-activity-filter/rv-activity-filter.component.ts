import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-activity-filter',
  templateUrl: './rv-activity-filter.component.html',
  styleUrls: ['./rv-activity-filter.component.scss'],
})
export class RvActivityFilterComponent implements OnInit {
  @Input()date_filter;
  @Input()status;
  @Input()sdate;
  @Input()edate;
  @Input()category;
  date:any=new Date().toISOString();
  categories:any=[];
    constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
      private storage:Storage) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('fil:',this.date_filter);
    
    this.edate=new Date(this.edate).toISOString();
      this.sdate=new Date(this.sdate).toISOString();
      this.getCategories();
  }
    dismiss(){
      this.modalCntl.dismiss();
    }
  
    save(){
      
      if(this.date_filter==0){
        this.sdate=moment().format('YYYY-MM-DD');
        this.edate=moment().format('YYYY-MM-DD');
      }else if(this.date_filter==1){
        this.edate=moment().subtract(1,'days').format('YYYY-MM-DD');
        this.sdate=moment().subtract(1,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==2){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(7,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==3){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==4){
        this.edate=moment().endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==5){
        this.edate=moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==6){
        this.sdate=moment(this.sdate).format('YYYY-MM-DD');
        this.edate=moment(this.edate).format('YYYY-MM-DD');
      }
      const onClosedData={
        date_filter:this.date_filter,
        status:this.status,
        sdate:this.sdate,
        edate:this.edate,
        category:this.category
      };
      this.modalCntl.dismiss(onClosedData);
    }

    async getCategories(){
      const bid = await this.storage.get("BRANCH");
      let url=this.config.domain_url+'activity_categories';
      let headers=await this.config.getHeader();;
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("cat:",res);
        this.categories.push({id:'0',name:'All category'});
        res.data.forEach(element => {
          this.categories.push({id:element.id,name:element.category_name})
        });
        console.log("cat1:",this.categories);
        
      })
    }
}
