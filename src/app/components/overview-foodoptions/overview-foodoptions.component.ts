import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview-foodoptions',
  templateUrl: './overview-foodoptions.component.html',
  styleUrls: ['./overview-foodoptions.component.scss'],
})
export class OverviewFoodoptionsComponent implements OnInit {
  @Input() data:any;
  constructor() { }

  ngOnInit() {}

}
