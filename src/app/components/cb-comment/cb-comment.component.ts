import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cb-comment',
  templateUrl: './cb-comment.component.html',
  styleUrls: ['./cb-comment.component.scss'],
})
export class CbCommentComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {}

}
