import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-registered-email-warning',
  templateUrl: './registered-email-warning.component.html',
  styleUrls: ['./registered-email-warning.component.scss'],
})
export class RegisteredEmailWarningComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  close(){
    this.modalCntl.dismiss();
  }

}
