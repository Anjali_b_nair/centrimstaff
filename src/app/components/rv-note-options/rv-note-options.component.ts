import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvAddAdditionalNoteComponent } from '../rv-add-additional-note/rv-add-additional-note.component';

@Component({
  selector: 'app-rv-note-options',
  templateUrl: './rv-note-options.component.html',
  styleUrls: ['./rv-note-options.component.scss'],
})
export class RvNoteOptionsComponent implements OnInit {
@Input() data;
@Input() flag;
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private popCntl:PopoverController,private toastCtlr:ToastController,private alertCntlr:AlertController) { }

  ngOnInit() {}

  async add(){
    const modal = await this.modalCntl.create({
      component:RvAddAdditionalNoteComponent,
      componentProps: {
        data:this.data
       
        
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }

  async delete(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_note';
    let headers=await this.config.getHeader();
   
  let body={
    note_id:this.data.id
  }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async deleteFile(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_ad_note_file/'+this.data.id;
    let headers=await this.config.getHeader();
   
  
    this.http.delete(url,{headers}).subscribe((res:any)=>{
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }
  async deleteAlert(){

    const alert = await this.alertCntlr.create({
      header: 'Are you sure?',
      backdropDismiss:true,
      message: 'You will not be able to recover this!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            if(this.flag==1){
            this.delete();
            }else{
              this.deleteFile();
            }
           
  
          }
        }
      ]
    });
    
    await alert.present();
  
   
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

}
