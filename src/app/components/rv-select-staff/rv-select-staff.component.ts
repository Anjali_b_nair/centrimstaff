import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-select-staff',
  templateUrl: './rv-select-staff.component.html',
  styleUrls: ['./rv-select-staff.component.scss'],
})
export class RvSelectStaffComponent implements OnInit {
staff:any[]=[];
@Input() data;
@Input()flag;
@Input() status;
dataCopy:any=[];
temp:any=[];
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private modalCntl:ModalController,private popCntl:PopoverController,private toastCtlr:ToastController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    console.log('flag:',this.flag);
    
      this.staff=[];
      if(this.data&&this.data.length){
        this.dataCopy=this.data.filter(x=>x);
        this.temp=this.data.filter(x=>x);
      }
     
     if(this.status==1){
      this.getCompanyStaff();
     }else if(this.status==2){
      this.getMaintenanceStaff();
     }else{
      this.getStaff();
     }
  
         
    }

    async getStaff(){
      const cid=await this.storage.get("COMPANY_ID")
  
      const bid=await this.storage.get("BRANCH")


      let url=this.config.domain_url+'staff_viabranch';
      let headers=await this.config.getHeader();
      let body={company_id:cid}
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        
        res.data.forEach(element => {
          if(element.user_type_id!=4){
            this.staff.push(element);
          }
        });
       
        console.log("staff:",res,this.staff);
      })
    }

async getCompanyStaff(){
  const cid=await this.storage.get("COMPANY_ID")
  
  const bid=await this.storage.get("BRANCH")


  let url=this.config.domain_url+'users';
  let headers=await this.config.getHeader();
  let body={company_id:cid,status:1}
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("staff:",res,this.staff);
    res.data.super_admins.forEach(element => {
     
      this.staff.push(element);
   
  });
    res.data.managers.forEach(element => {
     
        this.staff.push(element);
     
    });
    res.data.staffs.forEach(element => {
     
      this.staff.push(element);
   
  });
  
   
    console.log("staff:",res,this.staff);
  })
}
async getMaintenanceStaff(){
  const cid=await this.storage.get("COMPANY_ID")
  
  const bid=await this.storage.get("BRANCH")


  let url=this.config.domain_url+'technician_list';
  let headers=await this.config.getHeader();
  let body={branch_id:[parseInt(bid)]}
  console.log('body:',body);
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("staff:",res,this.staff);
    res.data.forEach(element => {
     
        this.staff.push(element);
      
    });
   
    console.log("staff:",res,this.staff);
  })
}
    select(item){
      if(this.flag==1){
      if(this.temp.includes(item.user_id)){
        const idx=this.temp.indexOf(item.user_id);
        this.temp.splice(idx,1);
        
      }else{
        this.temp.push(item.user_id);
      }
      console.log('dataselected:',this.temp)
    }else{
      if(this.temp.includes(item.user_id)){
        this.temp=[];
        // this.popCntl.dismiss(1)
      }else{
      this.temp=[];
      this.temp.push(item.user_id);
      let onClosedData:any={id:this.temp,name:item.name};
      this.popCntl.dismiss(onClosedData);
      }
   
    
    }
    }
  save(){
    if(!this.temp||!this.temp.length){
      this.presentAlert('Please select an assignee.')
    }else{
    let name=[];
    if(this.temp&&this.temp.length){
      this.staff.forEach(ele=>{
        if(this.temp.includes(ele.user_id)){
         name.push(ele.name)
        }
      })
    }
    let onClosedData:any={id:this.temp,name:name};
    console.log('cosed:',onClosedData);
    
    // this.modalCntl.dismiss(onClosedData);
    this.popCntl.dismiss(onClosedData);
  }
  }

  dismiss(){
    console.log('closed:',this.data,this.dataCopy);
    
    this.data=this.dataCopy.filter(x=>x);
    if(this.flag==0&&!this.data.length){
    this.popCntl.dismiss('no_data');
    }else{
      this.popCntl.dismiss();
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
}
