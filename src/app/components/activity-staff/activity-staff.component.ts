import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-activity-staff',
  templateUrl: './activity-staff.component.html',
  styleUrls: ['./activity-staff.component.scss'],
})
export class ActivityStaffComponent implements OnInit {
staffArray:any=[];
@Input() data;
@Input()flag;
selected:any=[];
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.selected=[];
    this.getStaff();
  }

  async getStaff(){
  
    const bid=await this.storage.get("BRANCH")

  

  const cid=await this.storage.get("COMPANY_ID")
  this.staffArray=[];
 
 
 
        

  let url=this.config.domain_url+'activity_staffs';
 
    let headers=await this.config.getHeader();
   

      let body={company_id:cid,status:1}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("arrStaff:",res,this.flag,this.data);
          let staff=res.data.super_admins.concat(res.data.managers).concat(res.data.staffs)
          console.log('staff:',staff)
          this.staffArray=staff;
          // staff.forEach(element => {
          //   this.staffArray.push({user_id:element.user_id,name:element.name})
          // });

          if(this.flag==1){
            this.staffArray.forEach(element => {
              if(this.data.includes(element.user_id.toString())){
                this.selected.push(element)
              }
            });
              
            console.log('selected:',this.selected)
          }

          
         
        })
    
}

dismiss(){
  this.modalCntl.dismiss();
}

selectAll(ev){
  if(ev.currentTarget.checked==true){
    this.data=this.staffArray.map(x=>x.user_id.toString());
  }else{
    this.data=[];
  }
}

select(item){
  console.log('selected:',this.data,item,);
  
  if(this.data.includes(item.user_id.toString())){
    console.log('if worked');
    
    const idx=this.data.indexOf(item.user_id.toString());
    this.data.splice(idx,1);
    
  }else{
    console.log('else worked');
    
    this.data.push(item.user_id.toString());
  }
}

save(){
  const onClosedData=this.data
  this.modalCntl.dismiss(onClosedData)
}
}
