import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-activity-attended-reason',
  templateUrl: './activity-attended-reason.component.html',
  styleUrls: ['./activity-attended-reason.component.scss'],
})
export class ActivityAttendedReasonComponent implements OnInit {
show_reason:boolean=false;
other:any;
reason:any;
  constructor(private modalCntl:ModalController,private toastCntlr:ToastController) { }

  ngOnInit() {}

  select(val){
    if(val=='Other'){
      this.show_reason=true;
    }else{
      this.show_reason=false;
      this.reason=val;
      
    }
  }
async dismiss(){
  console.log(this.reason,this.show_reason);
  if(this.show_reason&&(this.other==undefined||this.other=='')){
    this.presentAlert('Please enter the reason')
  }else{
  if(this.show_reason==true){
    this.reason=this.other
  }
  if(this.reason){
   let onClosedData:any=this.reason;
   console.log(onClosedData);
   
   await this.modalCntl.dismiss(onClosedData);
  }else{
    this.presentAlert('Please select an option');
  }
}
}
async cancel(){
  let onClosedData:any=null;
  await this.modalCntl.dismiss(onClosedData);
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
}
