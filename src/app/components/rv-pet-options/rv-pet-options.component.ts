import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController, AlertController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvDocEditComponent } from '../rv-doc-edit/rv-doc-edit.component';
import { RvAddPetComponent } from '../rv-add-pet/rv-add-pet.component';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-rv-pet-options',
  templateUrl: './rv-pet-options.component.html',
  styleUrls: ['./rv-pet-options.component.scss'],
})
export class RvPetOptionsComponent implements OnInit {
  @Input()flag;
  @Input()item;
  @Input()policy;
  @Input()consumer_id;
  constructor(private modalCntl:ModalController,private popCntl:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,
    private alertCnlr:AlertController) { }

  ngOnInit() {}
  async edit(){
    const modal = await this.modalCntl.create({
      component:RvAddPetComponent,
      cssClass: 'add-pets-modal',
      componentProps: {
        
        item:this.item,
        flag:2,
        policy:this.policy,
        consumer_id:this.consumer_id
        
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }

  async deletePet(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_pet';
    let headers=await this.config.getHeader();
   
  let body={
    pet_id:this.item.id,
    user_id:this.consumer_id
  }
  console.log('delete:',body);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('res:',res);
      
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async delete() {
    let mes;
   
      const alert = await this.alertCnlr.create({
        mode:'ios',
        header: 'Are you sure?',
        message: 'You will not be able to recover this data',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Delete',
            handler: () => {
             
              this.deletePet();
            }
          }
        ]
      });
    
      await alert.present();
    }
}
