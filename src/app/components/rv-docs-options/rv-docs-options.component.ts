import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvDocEditComponent } from '../rv-doc-edit/rv-doc-edit.component';

@Component({
  selector: 'app-rv-docs-options',
  templateUrl: './rv-docs-options.component.html',
  styleUrls: ['./rv-docs-options.component.scss'],
})
export class RvDocsOptionsComponent implements OnInit {
@Input() data;
  constructor(private modalCntl:ModalController,private popCntl:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,
    private alertCnlr:AlertController) { }

  ngOnInit() {}

  async edit(){
    const modal = await this.modalCntl.create({
      component:RvDocEditComponent,
      cssClass:'rv-edit-folder-modal',
      componentProps: {
        
       data:this.data
        
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }

  async deleteDoc(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_res_folder/'+this.data.id;
    let headers=await this.config.getHeader();
   
  
    this.http.delete(url,{headers}).subscribe((res:any)=>{
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async delete() {
    let mes;
    if(!this.data.contents||!this.data.contents.length){
      mes='You will not be able to recover this folder!'
    }else{
      mes='This folder is not empty. Are you sure to continue?'
    }
      const alert = await this.alertCnlr.create({
        mode:'ios',
        header: 'Are you sure?',
        message: mes,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Delete',
            handler: () => {
             
              this.deleteDoc();
            }
          }
        ]
      });
    
      await alert.present();
    }
}
