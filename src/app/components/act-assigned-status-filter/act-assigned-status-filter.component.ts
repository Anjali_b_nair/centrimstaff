import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-act-assigned-status-filter',
  templateUrl: './act-assigned-status-filter.component.html',
  styleUrls: ['./act-assigned-status-filter.component.scss'],
})
export class ActAssignedStatusFilterComponent implements OnInit {

  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}

  async closeModal(t) {
   
      const onClosedData: any = t;
      await this.popCntl.dismiss(onClosedData);
    }

}
