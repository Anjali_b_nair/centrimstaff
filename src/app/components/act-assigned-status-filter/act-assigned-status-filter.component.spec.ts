import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActAssignedStatusFilterComponent } from './act-assigned-status-filter.component';

describe('ActAssignedStatusFilterComponent', () => {
  let component: ActAssignedStatusFilterComponent;
  let fixture: ComponentFixture<ActAssignedStatusFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActAssignedStatusFilterComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActAssignedStatusFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
