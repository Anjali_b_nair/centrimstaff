import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-approve-invitation',
  templateUrl: './approve-invitation.component.html',
  styleUrls: ['./approve-invitation.component.scss'],
})
export class ApproveInvitationComponent implements OnInit {
@Input() data:any;
email:any;
password:any;
  constructor(private popCntl:PopoverController,private config:HttpConfigService,private http:HttpClient,
    private toastCntlr:ToastController,private modalCntrl:ModalController) { }

  ngOnInit() {}

ionViewWillEnter(){
  console.log(this.data);
this.email=this.data.email
}

  cancel(){
    this.modalCntrl.dismiss();
  }
  async confirm(){
    let headers=await this.config.getHeader();;
    if(this.password==undefined ||this.password=='' ||this.password.toString().length<6){
      this.presentAlert('Please enter atleast 6 characters.');
      
    }else{
    let url=this.config.domain_url+'approve_contact_invitation';
    let body={
      id:this.data.id,
      password:this.password
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      if(res.status=='success'){
        this.presentAlert('Approved.')
        let onClosedData:any=this.data
        this.modalCntrl.dismiss(onClosedData);
      }else{
        this.presentAlert(res.message);
      }
    })
  }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
