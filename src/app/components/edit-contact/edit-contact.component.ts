import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss'],
})
export class EditContactComponent implements OnInit {
name:any;
relation:any;
email:any;
phone:any;
home:any;
work:any;
user_name:any;
is_primary:any;
@Input() data:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private toastCntlr:ToastController,
    private modalCntl:ModalController) { }

  ngOnInit() {}


  ionViewWillEnter(){
    console.log(this.data);
    
    this.name=this.data.user_details.name;
    this.relation=this.data.relation;
    this.email=this.data.user_details.email;
    this.user_name=this.data.user_details.user_name;
    
    this.home=this.data.homephone;
    this.work=this.data.workphone;
    this.is_primary=this.data.is_primary;
    if(this.data.user_details.phone){
    let phone=this.data.user_details.phone.toString();
    if(phone.substring(0, 2)=='61'){
      this.phone=phone.substring(2);
      if(phone.substring(0,1)=='0'){
        this.phone=phone;
      }else{
        this.phone='0'+phone;
      }
    }else{
      this.phone=phone
    }
  }
  }
  cancel(){
    this.modalCntl.dismiss();
  }



  primaryCheck(ev){
    if(ev.currentTarget.checked){
      this.is_primary=1
    }else{
      this.is_primary=0;
    }
   }



  async confirm(){

    // var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
    //             '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    //             '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    //             '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    //             '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    //             '(\\#[-a-z\\d_]*)?$','i');
    var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    console.log(this.name,this.email,this.relation,this.phone)
    if(this.phone==undefined || this.phone=='' ||this.phone==null){
      this.phone=null;
    }
    if(this.home==undefined || this.home==''){
      this.home=null;
    }
    if(this.work==undefined || this.work==''){
      this.work=null;
    }
    if(this.name==undefined||this.email==undefined||this.relation==undefined){
      this.presentAlert('All * fields required!');
      
    }else if(this.name==''||this.email==''||this.relation==''){
      this.presentAlert('All * fields required!');
      
    }else if(this.phone!=null &&(this.phone.toString().length<10 || this.phone.toString().length>10)){
      this.presentAlert('Please enter a 10 digit mobile number.');
    }else if(this.is_primary==1&&this.phone==null){
      this.presentAlert('Please enter your mobile number.');
    }else if(this.home!=null && (this.home.toString().length<10 || this.home.toString().length>10)){
     
        this.presentAlert('Please enter a 10 digit number as home phone number.');
     
    }else if(this.work!=null && (this.work.toString().length<10 || this.work.toString().length>10)){
      
        this.presentAlert('Please enter a 10 digit number as work phone number.');
      
    }else if(/^\d+$/.test(this.name)){
      this.presentAlert('Name field contains only digits.');
    }else if(/^\d+$/.test(this.relation)){
      this.presentAlert('Please enter a proper relation.');
    }else{
    console.log("else worked.");
    
      if(pattern.test(this.email)){
        console.log("email match");
        let phone;
        if(this.phone!=null){
          phone=this.phone
        }else{
          phone=null
        }
    let url=this.config.domain_url+'update_contact';
    let headers=await this.config.getHeader();;
    let body={
      user_id:this.data.user_details.user_id,
      resident_user_id:this.data.resident_user_id,
      name:this.name,
      relation:this.relation,
      email:this.email,
      phone:phone,
      primary:this.is_primary,
      homephone:this.home,
      workphone:this.work,
      username:this.user_name
    }
    console.log("body:",body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      if(res.status=='success'){
        this.presentAlert('Updated successfully.')
        let onClosedData:any='updated'
        this.modalCntl.dismiss(onClosedData);
      }
    })
  }else{
    this.presentAlert('Please enter a valid email Id.')
  }
    }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
