import { HttpClient } from '@angular/common/http';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-community-position-filter',
  templateUrl: './community-position-filter.component.html',
  styleUrls: ['./community-position-filter.component.scss'],
})
export class CommunityPositionFilterComponent implements OnInit {
  community_position:any=[];
  @Input() position;
  loading:boolean=true;
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,private popCntl:PopoverController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.getcommunityPositions();
  }

  async getcommunityPositions(){
    const cid = await this.storage.get("COMPANY_ID");
    const bid = await this.storage.get("BRANCH");
    let url=this.config.domain_url+'community_positions/'+cid;
    let headers=await this.config.getHeader();;
    console.log("dat:",url,headers)
     this.http.get(url,{headers}).subscribe((res:any)=>{
  
      console.log('pos:',res);
      this.community_position=res.data;
      this.community_position.splice(0,0,{id:0,name:'All community positions'});
      
     })
     
     if(!this.position){
      this.position='0';
     }
    }
    @HostListener('touchstart')
    onTouchStart() {
      console.log('pageload:',this.loading)
      this.loading=false;
    }
    setPosition(event){
     if(!this.loading)
      this.popCntl.dismiss({id:this.position})
    }
}
