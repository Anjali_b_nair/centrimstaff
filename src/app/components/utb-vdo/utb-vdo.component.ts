import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-utb-vdo',
  templateUrl: './utb-vdo.component.html',
  styleUrls: ['./utb-vdo.component.scss'],
})
export class UtbVdoComponent implements OnInit {

  data:SafeResourceUrl;
  constructor(private domSanitizer: DomSanitizer,private navParams: NavParams,private orientation:ScreenOrientation,
    private modalCntl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
   
    this.orientation.lock(this.orientation.ORIENTATIONS.LANDSCAPE);
    this.data=this.domSanitizer.bypassSecurityTrustResourceUrl(this.navParams.data.data);
    console.log(this.data);
    
          
    }
    dismiss(){
      this.modalCntl.dismiss();
    }
}
