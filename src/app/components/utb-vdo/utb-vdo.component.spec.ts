import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UtbVdoComponent } from './utb-vdo.component';

describe('UtbVdoComponent', () => {
  let component: UtbVdoComponent;
  let fixture: ComponentFixture<UtbVdoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtbVdoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UtbVdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
