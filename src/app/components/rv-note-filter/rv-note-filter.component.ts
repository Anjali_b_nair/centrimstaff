import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import moment from 'moment';
import { RvSelectStaffComponent } from '../rv-select-staff/rv-select-staff.component';

@Component({
  selector: 'app-rv-note-filter',
  templateUrl: './rv-note-filter.component.html',
  styleUrls: ['./rv-note-filter.component.scss'],
})
export class RvNoteFilterComponent implements OnInit {
  @Input()date_filter;
  @Input()sdate;
@Input()edate;
@Input()assignedTo;
@Input()staff;
@Input()type;
date:any=new Date().toISOString();
user:any='0';
time_frame:any='0';
@Input()order;
constructor(private modalCntl:ModalController,private popCntl:PopoverController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    console.log('fil:',this.date_filter);
    
    this.edate=new Date(this.edate).toISOString();
      this.sdate=new Date(this.sdate).toISOString();
      if(this.assignedTo.length){
        this.user='1';
      }
  }
    dismiss(){
      this.modalCntl.dismiss();
    }

    save(){
    
      if(this.date_filter==0){
        this.sdate=moment().format('YYYY-MM-DD');
        this.edate=moment().format('YYYY-MM-DD');
      }else if(this.date_filter==1){
        this.edate=moment().subtract(1,'days').format('YYYY-MM-DD');
        this.sdate=moment().subtract(1,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==2){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(7,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==3){
        this.edate=moment().format('YYYY-MM-DD');
        this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD');
      }else if(this.date_filter==4){
        this.edate=moment().endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==5){
        this.edate=moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');
        this.sdate= moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
      }else if(this.date_filter==6){
        this.sdate=moment(this.sdate).format('YYYY-MM-DD');
        this.edate=moment(this.edate).format('YYYY-MM-DD');
      }
      if(this.time_frame==0){
        this.date_filter='7'
      }
      const onClosedData={
        date_filter:this.date_filter,
        sdate:this.sdate,
        edate:this.edate,
        assignedTo:this.assignedTo,
        staff:this.staff,
        type:this.type,
        order:this.order
      };
      this.modalCntl.dismiss(onClosedData);
    }

    async openAssignedTo(){
      console.log('assigned modal');
      
      const popover = await this.popCntl.create({
        component:RvSelectStaffComponent,
        cssClass:'rv-select-staf-pop',
        backdropDismiss:true,
        componentProps:{
          data:this.assignedTo,
          flag:0,
          status:1
    
        },
        
        
        
      });
      popover.onDidDismiss().then((dataReturned) => {
        console.log('data:',dataReturned);
        if(dataReturned.data){
          if(dataReturned.data=='no_data'){
            this.assignedTo=[];
            this.staff=undefined
          }else{
          this.assignedTo=dataReturned.data.id;
          
          this.staff=dataReturned.data.name
            }
          }
     
        
      });
      return await popover.present();
    

    }
    selectType(ev){
      console.log('val:',ev.target.value);
      if(this.type.includes(ev.target.value)){
        const idx=this.type.indexOf(ev.target.value)
        this.type.splice(idx,1)
      }else{
        this.type.push(ev.target.value)
      }
      
    }

    removeUser(){
      this.staff=null;
      this.assignedTo=[];
      
    }
}
