import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-consumer-activity',
  templateUrl: './consumer-activity.component.html',
  styleUrls: ['./consumer-activity.component.scss'],
})
export class ConsumerActivityComponent implements AfterViewInit {
  @ViewChild("expandWrapper", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded") expanded: boolean = false;
  @Input("expandHeight") expandHeight: string = "500px";
  constructor(public renderer: Renderer2) { }

  ngAfterViewInit() {

    console.log("exp:",this.expanded);
    
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }

}
