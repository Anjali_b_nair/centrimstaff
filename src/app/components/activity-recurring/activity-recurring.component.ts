import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import moment from 'moment';
@Component({
  selector: 'app-activity-recurring',
  templateUrl: './activity-recurring.component.html',
  styleUrls: ['./activity-recurring.component.scss'],
})
export class ActivityRecurringComponent implements OnInit {
  // Daily='Daily';
daily:any=1;
repeat:any='1';
week:any=[];
monthly:any;
occurance:any=1;
date:any;
c_day:any;
// ocuurance:any;
minDate: string ;
@Input() data;
@Input() rep;
start:any;
dates:any=[];

interval:any;
end:any='on';
rec_details:any;
  constructor(private modalCntl:ModalController,private toastCntlr:ToastController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.dates=[];
    this.week=[];
    console.log("rep:",this.rep);
    // this.date=this.data;
    this.minDate=new Date(this.data).toISOString();
    if(Object.keys(this.rep).length>0){
     
    if(this.rep.repeat==undefined||this.rep.repeat=='undefined'){

    }else{
      this.repeat=this.rep.repeat;
      this.daily=this.rep.daily;
      this.week=this.rep.week;
      this.monthly=this.rep.monthly;
      this.end=this.rep.end;
      this.occurance=this.rep.occurance;
      this.date=this.rep.date;
    }
      
    
    }
    // this.start=this.data;
    // this.dates.push(this.data);
    let d=new Date();
    this.c_day=d.getDate();
    this.monthly='Monthly on day '+this.c_day
  }
  increment () {
    if(this.daily<100)
    this.daily++;
  }
  
   decrement () {
     if(this.daily>1)
    this.daily--;
  }
  setWeek(i){
    if(this.week.includes(i)){
      this.week.splice(this.week.indexOf(i),1);
    }else{
    this.week.push(i);
    }
    console.log("wee:",this.week);
    
  }
  incrementOccurance () {
    if(this.end=='After'){
    this.occurance++;
    
    }
    
  }
  
   decrementOccurance () {
    if(this.end=='After'){
      if(this.occurance>1){
    this.occurance--;
      }
    }
  }


recurring(){
  if(this.repeat==1){
    this.dailyActivity();
  }else if(this.repeat==2){
    this.weeklyActivity();
  }else{
    this.monthlyActivity();
  }
}

dailyActivity(){
   let end

}

weeklyActivity(){

}
monthlyActivity(){

}
setDate(d){
  console.log("endDate:",this.date);
  if(this.repeat==1){
    let days=moment(this.date).diff(moment(this.data),'days');
    console.log("days:",days,"daily:",this.daily);
    
    this.interval=Number(parseInt((days/this.daily).toString()));
    
    // let start=moment(this.data);
    // let end=moment(this.date);
    
    // var current = start.clone();
    // while (current.day(1 + this.daily).isBefore(end)) {
    //   this.dates.push(current.clone());
    // }

      
  }else if(this.repeat==2){
    let days=moment(this.date).diff(moment(this.data),'days');
    this.interval=Number(parseInt((days/7).toString()));
    console.log("interval:",days,this.interval);
    
    // this.occurance=days/this.daily;
    // let start=moment(this.data);
    // let end=moment(this.date);
    
    // var current = start.clone();
    // while (current.day(7 + this.week).isBefore(end)) {
    //   this.dates.push(current.clone());
    // }
  }else if(this.repeat==3){
    let days=moment(this.date).diff(moment(this.data),'days');
    this.interval=Number(parseInt((days/30).toString()));
    // let start=moment(this.data);
    // let end=moment(this.date);
    
    // var current = start.clone();
    // while (current.day(1).isBefore(end)) {
    //   this.dates.push(current.clone());
    // }
  }console.log(this.interval);
}
setEnd(val){
  this.end=val;
  console.log("end:",this.end);
  
  if(val=='on'){
    
    console.log("endDate:",this.date);
    if(this.repeat==1){
      let days=moment(this.date).diff(moment(this.data),'days');
      console.log("days:",days,"daily:",this.daily);
      
      this.interval=Number(parseInt((days/this.daily).toString()));
      
      // let start=moment(this.data);
      // let end=moment(this.date);
      
      // var current = start.clone();
      // while (current.day(1 + this.daily).isBefore(end)) {
      //   this.dates.push(current.clone());
      // }

        
    }else if(this.repeat==2){
      let days=moment(this.date).diff(moment(this.data),'days');
      this.interval=Number(parseInt((days/7).toString()));
      console.log("interval:",this.interval);
      
      // this.occurance=days/this.daily;
      // let start=moment(this.data);
      // let end=moment(this.date);
      
      // var current = start.clone();
      // while (current.day(7 + this.week).isBefore(end)) {
      //   this.dates.push(current.clone());
      // }
    }else if(this.repeat==3){
      let days=moment(this.date).diff(moment(this.data),'days');
      this.interval=Number(parseInt((days/30).toString()));
      // let start=moment(this.data);
      // let end=moment(this.date);
      
      // var current = start.clone();
      // while (current.day(1).isBefore(end)) {
      //   this.dates.push(current.clone());
      // }
    }console.log("ye:",this.interval);
    
  }else{
   console.log("af:",this.occurance);
   
   this.interval=this.occurance
  }
}

async dismiss(i){
this.dates=[];
console.log("occur:",this.interval);
this.dates.push(moment(this.data).format('yyyy-MM-DD'));

if(this.end=='on'){

  if(this.repeat==1){
    let start=this.data;
    console.log('st:',start)
    for(let i=0;i<this.interval;i++){
      let date=moment(start).add(this.daily,'days').format('yyyy-MM-DD'); 
      var d1=Date.parse(this.date);
      var d2=Date.parse(date);
      console.log("ddd:",d1,d2)
      if(d2<=d1){
      this.dates.push(date);
      }
      console.log("dt:",date);
      
      start=date;
    }
  }else if(this.repeat==2){
    
    var start = moment(this.data), // Sept. 1st
    end   = moment(this.date), // Nov. 2nd
    week =this.getDay(moment(this.data).format('ddd')),
    day;
    console.log("date[]:",this.date,this.data);
    
    var result = [];
    if(this.week.indexOf(week)<0){
      this.dates.splice(0,1);
    }
    console.log('weekly act:',this.dates,week,this.week,this.week.indexOf(week));
    this.week.forEach(element => {
      day=element;
      var current = start.clone();
      if(day>week){
        this.dates.push(moment((current.day(-1+ day)).clone()).format('yyyy-MM-DD'));
      }
      while (current.day(6 + day).isBefore(end)) {
        this.dates.push(moment(current.clone()).format('yyyy-MM-DD'));
      }
    });
     
    
     

      console.log('result:',result);
      
    // let start=this.data;
    // for(let i=0;i<=this.interval;i++){
    //   console.log("i:",i);
    //   let date;
    //   this.week.forEach(element => {
    //     console.log("weekele:",element);
    //     if(i==0){
    //     date=moment(start).add(element,'days').format('yyyy-MM-DD');
    //     }else{
    //       date=moment(start).add(element+6+i,'days').format('yyyy-MM-DD');
    //     }
    //     console.log("date:",date);
    //     var d1=Date.parse(this.date);
    //     var d2=Date.parse(date);
    //     if(d2<=d1){
    //   this.dates.push(date);
    //     }
     
    //   });
    //   // start=date;
    //   console.log("start:",start);
      
    // }
  }else if(this.repeat==3){
    let start=this.data;
    for(let i=0;i<this.interval;i++){
      let date=moment(start).add(1,'months').format('yyyy-MM-DD');
      var d1=Date.parse(this.date);
      var d2=Date.parse(date);
      if(d2<=d1){
      this.dates.push(date);
      }
      start=date;
    }
  }
}else{
  this.interval=this.occurance
  if(this.repeat==1){
    let start=this.data;
    console.log('st:',start)
    for(let i=1;i<this.interval;i++){
      let date=moment(start).add(this.daily,'days').format('yyyy-MM-DD'); 
      var d1=Date.parse(this.date);
      var d2=Date.parse(date);
      console.log("ddd:",d1,d2)
      // if(d2<=d1){
      this.dates.push(date);
      // }
      console.log("dt:",date);
      
      start=date;
    }
  }else if(this.repeat==2){
    
   var start = moment(this.data), // Sept. 1st
    // end   = moment(this.date), // Nov. 2nd
    week =this.getDay(moment(this.data).format('ddd')),
    day;
    console.log("date[]:",this.date,this.data);
    
    var result = [];
    if(this.week.indexOf(week)<0){
      this.dates.splice(0,1);
    }
    
    this.week.forEach(element =>{
      
      day=element;

      if(day>week){
        let d=moment(this.data);
          start=moment(moment((d.day(-1+ day)).clone()).format('yyyy-MM-DD'));
          }else if(day<week){
            start=moment(moment(this.data).add(5+day,'days').format('yyyy-MM-DD'));
          }else if(day==week){
            start=moment(moment(this.data).format('yyyy-MM-DD'));
          }
      var current = start;
      console.log("stae:",start.format('yyyy-MM-DD'));
      this.dates.push(start.format('yyyy-MM-DD'));
      console.log('weekly act:',this.dates,week,this.week,this.week.indexOf(week),day);
      // if(day>week){
      //   this.dates.push(moment((current.day(-1+ day)).clone()).format('yyyy-MM-DD'));
      // }else{
        
      // while (current.day(6 + day).isBefore(end)) {
        for(let i=1;i<this.interval;i++){
        this.dates.push(moment(current).add(i,'weeks').format('yyyy-MM-DD'));
      }
     
      
    });
  
    let set = new Set(this.dates);

    this.dates  = Array.from(set);
     

      console.log('result:',this.dates);
      
   
  }else if(this.repeat==3){
    let start=this.data;
    for(let i=1;i<this.interval;i++){
      let date=moment(start).add(1,'months').format('yyyy-MM-DD');
      var d1=Date.parse(this.date);
      var d2=Date.parse(date);
      // if(d2<=d1){
      this.dates.push(date);
      // }
      start=date;
    }
  }
}


if(this.repeat==1){
  this.rec_details='Every '+this.daily+ ' days'
}

if(this.repeat==2){
  let week='';
  if(this.week.includes(2)){
    
    week=week+' Mon,'
  }
  if(this.week.includes(3)){
    
    week=week+' Tue,'
  }
  if(this.week.includes(4)){
    
    week=week+' Wed,'
  }
  if(this.week.includes(5)){
   
    week=week+' Thu,'
  }
  if(this.week.includes(6)){
    
    week=week+' Fri,'
  }
  if(this.week.includes(7)){
    
    week=week+' Sat,'
  }
  if(this.week.includes(1)){
    
    week=week+' Sun,'
  }
  this.rec_details='Every '+week
}
// let d;
// if (this.c_day == 1 || this.c_day == 21 || this.c_day == 31) {
//   d = 'st'
// } else if (this.c_day == 2 || this.c_day == 22) {
//   d = 'nd'
// } else if (this.c_day == 3 || this.c_day == 23) {
//   d = 'rd'
// } else {
//   d = 'th'
// }
  if(this.repeat==3){
    let d;
if (this.c_day == 1 || this.c_day == 21 || this.c_day == 31) {
  d = 'st'
} else if (this.c_day == 2 || this.c_day == 22) {
  d = 'nd'
} else if (this.c_day == 3 || this.c_day == 23) {
  d = 'rd'
} else {
  d = 'th'
}
    this.rec_details=this.c_day+d+' day of every month'
  }
  
  
  if(this.end=='on'){
    this.rec_details=this.rec_details+' ends on '+moment(this.date).format('DD MMM,yyyy')
  }
  if(this.end=='After'){
    this.rec_details=this.rec_details+' ends after '+this.occurance+' occurrence'
  }
  console.log('dates:',this.dates);
  let onClosedData:any=this.dates;
  let data:any={'repeat':this.repeat,'daily':this.daily,"week":this.week,'monthly':this.monthly,"end":this.end,"date":this.date,"occurance":this.occurance,rec_details:this.rec_details,status:i};
  if(i==2){
  if(this.repeat==1&&this.daily==null){
    this.presentAlert('Please fill all data')
  }else if(this.repeat==2&&this.week.length==0){
    this.presentAlert('Please select week days')
  }else if(this.end=='on'&&!this.date){
    this.presentAlert('Please choose an end date')
  }else if(this.end=='After'&&this.occurance==null){
    this.presentAlert('Please specify the occurrence')
  }else{
  await this.modalCntl.dismiss(onClosedData,data);
  }
}else{
    await this.modalCntl.dismiss(onClosedData,data);
  }
  
}

getDay(d){
  let day;
  if(d=="Sun"){
    day=1
  }else if(d=="Mon"){
    day=2
  }else if(d=="Tue"){
    day=3
  }else if(d=="Wed"){
    day=4
  }else if(d=="Thu"){
    day=5
  }else if(d=="Fri"){
    day=6
  }else if(d=="Sat"){
    day=7
  }
  return day;
}
cancel(){
  this.modalCntl.dismiss();
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
limitDailyInput(){
  if(this.daily<1||this.daily==0){
    this.daily=1;
  }else if(this.daily>100){
    this.daily=100
  }
}
}
