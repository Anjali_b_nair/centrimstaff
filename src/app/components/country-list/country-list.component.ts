import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss'],
})
export class CountryListComponent implements OnInit {
countries:any=[];
@Input()birth_country;
terms:any;
  constructor(private config:HttpConfigService,private http:HttpClient,private modalCntl:ModalController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    this.getCountries();
  }

  async getCountries(){
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'countries'
    this.http.get(url,{headers}).subscribe((res:any)=>{
     console.log('countries:',this.countries);
     
      this.countries=res.data;
      
      
    })
  }
  dismiss(){
    if(this.birth_country){
      const idx=this.countries.map(x=>x.id).indexOf(parseInt(this.birth_country));
      let name;
      if(idx>=0){
        name=this.countries[idx].name;
      }
    this.modalCntl.dismiss({id:this.birth_country,name:name})
    }else{
      this.modalCntl.dismiss();
    }
  }
  select(item){
    
    this.modalCntl.dismiss({id:item.id,name:item.name})

  }

}
