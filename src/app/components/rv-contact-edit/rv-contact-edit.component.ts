import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-contact-edit',
  templateUrl: './rv-contact-edit.component.html',
  styleUrls: ['./rv-contact-edit.component.scss'],
})
export class RvContactEditComponent implements OnInit {
  @Input()contact;
  email:any=null;
  relation:any;
  other_relation:any=null;
  phone:any=null;
  is_primary:boolean=false;
  family_portal:boolean=false;
  fname:any;
  lname:any;
  state:any=null;
  address:any=null;
  suburb:any=null;
  postcode:any=null;
  approved_for:any=[];
  contact_type:any=null;
  contact_role:any=null;
  subscription:Subscription;
  password:any=null;

  constructor(private modalCntl:ModalController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController,) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.email=this.contact.user_details.email;
    if(this.contact.relation=='Spouse'||this.contact.relation=='Son'||this.contact.relation=='Daughter'||this.contact.relation=='Family'||
        this.contact.relation=='Friend'||this.contact.relation=='GP'||this.contact.relation=='Specialist'||this.contact.relation=='Solicitor'||this.contact.relation=='Funeral director'){
    this.relation=this.contact.relation;
        }else{
          this.relation='Other';
          this.other_relation=this.contact.relation;
        }
   
    this.phone=this.contact.user_details.phone;
    if(this.contact.is_primary==1){
    this.is_primary=true;
    }else{
      this.is_primary=false
    }
    if(this.contact.user_details.portal_access==1){
    this.family_portal=true;
    }else{
      this.family_portal=false;
    }
    this.fname=this.contact.firstname;
    this.lname=this.contact.lastname;
    this.state=this.contact.state;
    this.address=this.contact.address;
    this.suburb=this.contact.suburb;
    this.postcode=this.contact.postcode;
    
    this.contact_type=this.contact.contact_purpose;
    this.contact_role=this.contact.contact_role;

    
      if(this.contact.enduring_powerof_attorney==1){
        this.approved_for.push('EPOA')
      }
      if(this.contact.supportive_powerof_attorney==1){
        this.approved_for.push('Supporting Power of Attorney')
      }
      if(this.contact.financial_powerof_attorney==1){
        this.approved_for.push('Financial power of Attorney')
      }
      if(this.contact.enduring_powerof_atorney_medical==1){
        this.approved_for.push('EPOAMedical')
      }
      if(this.contact.enduring_powerof_guardianship==1){
        this.approved_for.push('EPOG')
      }
      if(this.contact.public_trustee==1){
        this.approved_for.push('Public Trustee')
      }
      if(this.contact.vcat_guardianship_order==1){
        this.approved_for.push('VCAT Guardianship')
      }
      if(this.contact.executor==1){
        this.approved_for.push('Executor')
      }

      console.log('app:',this.approved_for);
      
  }

  async save(){
    if(!this.fname||!this.lname||!this.relation){
      this.presentAlert('Please fill the required fields.')
    }else{
      const bid = await this.storage.get("BRANCH");
      const uid = await this.storage.get("USER_ID");
      const cid = await this.storage.get("COMPANY_ID");
  
        let url=this.config.domain_url+'update_rv_contact';
      let headers=await this.config.getHeader();
      let primary=0;
      let portal_access=0;
      if(this.family_portal){
        portal_access=1
      }
      if(this.is_primary){
        primary=1
      }
      let eopa,eopamed,spa,fpa,epog,trustee,vcat,exe;
      if(this.approved_for.includes('EPOA')){
        eopa=1;
      }else{
        eopa=0
      }
      if(this.approved_for.includes('Supporting Power of Attorney')){
        spa=1;
      }else{
        spa=0;
      }
      if(this.approved_for.includes('Financial power of Attorney')){
        fpa=1;
      }else{
        fpa=0;
      }
      if(this.approved_for.includes('EPOAMedical')){
        eopamed=1;
      }else{
        eopamed=0;
      }
      if(this.approved_for.includes('EPOG')){
        epog=1;
      }else{
        epog=0;
      }
      if(this.approved_for.includes('Public Trustee')){
        trustee=1;
      }else{
        trustee=0;
      }
      if(this.approved_for.includes('VCAT Guardianship')){
        vcat=1;
      }else{
        vcat=0;
      }
      if(this.approved_for.includes('Executor')){
        exe=1;
      }else{
        exe=0;
      }

      if(this.relation!=='Other'){
        this.other_relation=null
      }
      let body={
        email:this.email,
        user_id:parseInt(this.contact.resident_user_id),
        relation:this.relation,
        name:this.fname+' '+this.lname,
        relation2:this.other_relation,
        phone:this.phone,
        is_primary:primary,
        contact_purpose:this.contact_type,
        contact_role:this.contact_role,
        do_not_call:0,
        do_not_text:0,
        enduring_powerof_attorney:eopa,
        enduring_powerof_guardianship:epog,
        supportive_powerof_attorney:spa,
        enduring_powerof_atorney_medical:eopamed,
        public_trustee:trustee,
        vcat_guardianship_order:vcat,
        financial_powerof_attorney :fpa,
        executor:exe,
        firstname:this.fname,
        lastname:this.lname,
        state:this.state,
        address:this.address,
        suburb:this.suburb,
        postcode:this.postcode,
        portal_access:portal_access
      }
      console.log('body:',body);
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
       
       console.log(res);
       
        this.presentAlert('Edit contact successfully.')
        this.modalCntl.dismiss();
      },error=>{
        console.log(error);
        this.presentAlert('Something went wrong. Please try again later.')
      })
    }

  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }


  dismiss(){
    this.modalCntl.dismiss();
  }
}
