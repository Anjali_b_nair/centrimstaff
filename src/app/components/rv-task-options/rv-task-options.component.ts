import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { RvEditTaskComponent } from '../rv-edit-task/rv-edit-task.component';

@Component({
  selector: 'app-rv-task-options',
  templateUrl: './rv-task-options.component.html',
  styleUrls: ['./rv-task-options.component.scss'],
})
export class RvTaskOptionsComponent implements OnInit {
  @Input() data;
  @Input() companian;
  @Input() flag;
  task_delete:boolean=false;
  task_edit:boolean=false;
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private popCntl:PopoverController,private toastCtlr:ToastController,private alertCntlr:AlertController) { }

  ngOnInit() {}

  async edit(){
    const modal = await this.modalCntl.create({
      component:RvEditTaskComponent,
      cssClass:'full-width-modal',
      componentProps: {
        data:this.data,
        companian:this.companian,
        flag:this.flag
         
      },
      
      
    });
    modal.onDidDismiss().then(()=>{
      this.popCntl.dismiss(1);
    })
    return await modal.present();
  }
  async deleteAlert(){

    const alert = await this.alertCntlr.create({
      header: 'Are you sure?',
      backdropDismiss:true,
      message: 'You will not be able to recover this!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            
            this.delete();
           
  
          }
        }
      ]
    });
    
    await alert.present();
  
   
  }
  async delete(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'delete_resident_task/'+this.data.id;
    let headers=await this.config.getHeader();
   
  
    this.http.delete(url,{headers}).subscribe((res:any)=>{
      this.presentAlert('Deleted successfully.')
      this.popCntl.dismiss(1);
    })
  }

  async mark(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    const uid = await this.storage.get("USER_ID");;
    let url=this.config.domain_url+'update_task_status';
    let headers=await this.config.getHeader();
    let body;
    body={
     task_id:this.data.id,
     status:2,
     completed_at:moment().format('YYYY-MM-DD HH:mm:ss'),
     completed_by:uid

    }
  
   console.log('stat:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      this.presentAlert('Marked as completed.')
     this.popCntl.dismiss(1);
    })
  }
  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async ionViewWillEnter(){
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
     
      if (res.main_permissions.rv==1&&routes.includes('task.edit')) {

        this.task_edit = true
      } else {

        this.task_edit = false;
      }

      if (res.main_permissions.rv==1&&routes.includes('task.destroy')) {

        this.task_delete = true
      } else {

        this.task_delete = false;
      }
     
    })
  }
}
