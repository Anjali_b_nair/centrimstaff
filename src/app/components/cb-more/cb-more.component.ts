import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;

@Component({
  selector: 'app-cb-more',
  templateUrl: './cb-more.component.html',
  styleUrls: ['./cb-more.component.scss'],
})
export class CbMoreComponent implements OnInit {
  @Input() data;
  @Input() other;
  @Input() call;
  user_id:any;
   mem:any=[];
   other_mem:any=[];
  constructor(private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.mem=[];
    this.mem=this.data.slice(1);
    if(this.data.length==0){
      this.other_mem=this.other.slice(1);
    }else{
      this.other_mem=this.other
    }
    console.log(this.data);
  }

  cancel(){
    this.modalCntl.dismiss();
  }
}
