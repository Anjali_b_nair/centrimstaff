import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent implements AfterViewInit {

  @ViewChild("expandWrapper1", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded") expanded: boolean = false;
  // @Input("expanded2") expanded2: boolean =false;
  // @Input("expanded3") expanded3: boolean = false;
  // @Input("expanded") expanded: boolean = false;
  @Input("expandHeight") expandHeight: string = "";
  constructor(public renderer: Renderer2) { }

  
  ngAfterViewInit() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }

}
