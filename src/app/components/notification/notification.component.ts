import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {

  @Input() data:any;
  
    status:any;
    my_comment:any;
    message:any;
    call:any;
    constructor(private modalCntl:ModalController) { }
  
    ngOnInit() {}
  
    ionViewWillEnter(){
      console.log('data:',this.data)
      this.my_comment=this.data.com;
      this.message=this.data.mes;
      this.call=this.data.call;
      this.status=this.data.status;
    }
  
    dismiss(){
      this.modalCntl.dismiss();
    }
    continue(){
      const not={
        com:this.my_comment,
        mes:this.message,
        call:this.call,
        status:this.status}
        this.modalCntl.dismiss(not)
    }
    
    
    
   
    commentStatus(i){
      // if(this.isLoading==false){
      if(i.currentTarget.checked==true){
        this.my_comment=1;
      }else{
        this.my_comment=0;
      }
      
        // this.updateNotificationStatus();
        // }
    }
    messageStatus(i){
      // if(this.isLoading==false){
      if(i.currentTarget.checked==true){
        this.message=1;
      }else{
        this.message=0;
      }
    
        // this.updateNotificationStatus();
        // }
    }
    callStatus(i){
      // if(this.isLoading==false){
      if(i.currentTarget.checked==true){
        this.call=1;
      }else{
        this.call=0;
      }
      
        // this.updateNotificationStatus();
        // }
    }
}
