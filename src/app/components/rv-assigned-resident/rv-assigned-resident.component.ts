import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-rv-assigned-resident',
  templateUrl: './rv-assigned-resident.component.html',
  styleUrls: ['./rv-assigned-resident.component.scss'],
})
export class RvAssignedResidentComponent implements OnInit {
  @Input()data;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  cancel(){
    this.popCntl.dismiss();
  }
}
