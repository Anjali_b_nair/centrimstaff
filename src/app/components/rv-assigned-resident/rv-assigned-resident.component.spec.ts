import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAssignedResidentComponent } from './rv-assigned-resident.component';

describe('RvAssignedResidentComponent', () => {
  let component: RvAssignedResidentComponent;
  let fixture: ComponentFixture<RvAssignedResidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAssignedResidentComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAssignedResidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
