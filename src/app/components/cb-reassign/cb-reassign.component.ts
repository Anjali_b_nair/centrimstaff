import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-cb-reassign',
  templateUrl: './cb-reassign.component.html',
  styleUrls: ['./cb-reassign.component.scss'],
})
export class CbReassignComponent implements OnInit {
@Input() data:any;
staffArray:any[]=[];
staff:any;
staff_id:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private popCntl:PopoverController,private toastCntlr:ToastController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
   
      const bid=await this.storage.get("BRANCH")

    this.staffArray=[];
    this.staff=this.data.assignee.name;
    this.staff_id=this.data.assignee.user_id;

    let url=this.config.domain_url+'staff_viabranch';
    let headers=await this.config.getHeader();
    
    
    // this.storage.ready().then(()=>{
      const cid=await this.storage.get("COMPANY_ID")

        let body={company_id:cid};
        console.log('body:',body,'head:',bid);
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log("arr:",res);
            this.staffArray=res.data
            
          })
    


  }
  setstaff($event){
    this.staffArray.forEach(element => {
      if(element.name==this.staff){
        this.staff_id=element.user_id;

      }
    });
    console.log("staff:",this.staff,this.staff_id);
    
  }

cancel(){
  this.popCntl.dismiss();
}
async confirm(){
  let headers=await this.config.getHeader();;
  let url=this.config.domain_url+'assign_call';
  let body={
    call_id:this.data.id,
    assigned_to:this.staff_id
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    if(res.status=='success'){
      this.presentAlert('Reassigned successfully.')
      this.popCntl.dismiss();
    }
  })
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
}
