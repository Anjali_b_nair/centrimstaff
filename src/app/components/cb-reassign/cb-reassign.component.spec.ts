import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CbReassignComponent } from './cb-reassign.component';

describe('CbReassignComponent', () => {
  let component: CbReassignComponent;
  let fixture: ComponentFixture<CbReassignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbReassignComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CbReassignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
