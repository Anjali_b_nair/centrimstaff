import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisibletoComponent } from './visibleto.component';

describe('VisibletoComponent', () => {
  let component: VisibletoComponent;
  let fixture: ComponentFixture<VisibletoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibletoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisibletoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
