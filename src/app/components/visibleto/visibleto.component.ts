import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendanceGroupComponent } from '../activity-attendance-group/activity-attendance-group.component';

@Component({
  selector: 'app-visibleto',
  templateUrl: './visibleto.component.html',
  styleUrls: ['./visibleto.component.scss'],
})
export class VisibletoComponent implements OnInit {
value:any;
consumers:any[]=[];
  flag:any;
  terms:any;
  // tags:any[]=[];
  
temp:any[]=[];
result:any=[];
view:boolean;
selected:any[]=[];
con:any[]=[];
@Input() data:any;
@Input() type:any;
@Input() tags:any;
// @Input()filter:any;
dataCopy:any=[];
choose_all:any;
rv:any;
valueCopy:any;
  constructor(private modalCntl:ModalController,private http:HttpClient,private loadingCtrl:LoadingController,
    private config:HttpConfigService,private storage:Storage,private popCntrl:PopoverController,private toast:ToastController) { }

  ngOnInit() {}

async ionViewWillEnter(){
  this.rv=await this.storage.get('RVSETTINGS');
  this.result=[];
  this.terms='';
  // this.getConsumers();
  if(this.data&&this.data.length){
    this.dataCopy=this.data.filter(x=>x);
  }
  if(this.data&&this.data.length){
  this.result=this.data;
  this.temp=this.data;
  }
  this.value=this.type;
  this.valueCopy=this.type;
  // if(this.tags.length>0){
  //   this.tags.forEach(element => {
  //     if(this.result.indexOf(element)<0){
  //     this.result.push(element)
  //     }
  //   });
  // }
  if(this.result&&this.result.length){
    this.view=true;
    this.getConsumers();
  }else{
    this.view=false;
  }
  // if(this.filter==2){
  //   this.view=true;
  //   this.viewSelected(this.view)
  // }else{
  //   this.view=false;
  // }
  console.log('data:',this.tags,this.result,'type:',this.type)
 
}
  select(val){
    if(val==1||val==2){
      this.result=[];
      this.temp=[];
      this.view=false;
      // this.getConsumers();
    }else{
      this.view=true;
      this.result=[];
      this.temp=[];
      this.choose_all=0
      this.getConsumers();
    }
    this.value=val;
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
     
    });
    return await loading.present();
    }
    
    async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
async getConsumers(){
  this.showLoading();
    const bid=await this.storage.get("BRANCH")

 console.log("branch:",bid);
 
                  let url=this.config.domain_url+'residents';
                  let headers=await this.config.getHeader();
                  console.log(url);
  
                  // let headers=await this.config.getHeader();
                  this.http.get(url,{headers}).subscribe((data:any)=>{
                   
                    console.log("data:",data);
                    this.consumers=data.data
                    this.con=data.data;
                      if(this.view){
                        this.viewSelected(this.view)
                      }
                      this.dismissLoader();
                  },error=>{
                    console.log(error);
                    this.dismissLoader();
                  });
              
}
  async dismiss(i){
    let  id;
    let type;
    if(i==2){
      
      if((this.value==3||this.value==4)&&!this.result.length){
        this.presentAlert('Please select atleast one resident')
      }else{
        id=this.result.filter(x=>x);
      type=this.value;
      }
    }else{
      id=this.dataCopy.filter(x=>x);
      type=this.valueCopy
    }
  
  
  await this.modalCntl.dismiss(id,type);
  console.log("ond:",this.result,this.dataCopy)
  
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  selectConsumer(ev,item){
    // item.selected=true;
    if(this.temp.includes(item.user.user_id)){
      var index = this.result.indexOf(item.user.user_id);
      this.result.splice(index, 1);
      console.log("index:",this.result.indexOf(item.user.user_id))
      this.temp.splice(this.temp.indexOf(item.user.user_id),1)
    }else{
      this.temp.push(item.user.user_id);
    }
    this.result=this.temp.reduce((arr,x)=>{
      let exists = !!arr.find(y => y === x);
      if(!exists){
          arr.push(x);
      }
      
      return arr;
  }, []);
  if(this.result.length>0){
    this.view=true;
  }else{
    this.view=false;
  }
  console.log("sel:",this.result,this.view,this.temp);
  
  }


  async byGroup(ev){
    let headers=await this.config.getHeader();
    const popover = await this.modalCntl.create({
      component: ActivityAttendanceGroupComponent,
      
      backdropDismiss:true,
      cssClass:'act-res-group-modal'
      
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned == null || dataReturned.data==undefined) {
      }else{
        
        let id = dataReturned.data;
        this.consumers=[];
        if(id!=0){
          this.showLoading();
        let url=this.config.domain_url+'group/'+id;
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("mem:",res);
    
      // let attend;
      res.data.residents.forEach(element => {
      //   if(this.act_atnd.includes(element.user.user_id)){
      //     attend=true
      //   }else{
      //     attend=false
      //   }
        this.consumers.push(element);
      });
      this.dismissLoader();
    },err=>{
      this.dismissLoader();
    })
        }else{
          this.consumers=this.con;
        }
        
      }
    });
    return await popover.present();
  }


  viewSelected(view){
  
    if(this.view==true){
      if(this.result.length>0){
      this.consumers.forEach(ele=>{
        if(this.result.includes(ele.user.user_id)){
            this.selected.push(ele);
        }
      })
      this.consumers=this.selected
    }
    }else{
      this.selected=[];
      this.consumers=this.con
    }
    this.view=!this.view;
    
  }
  cancel(){
    this.terms='';
  }


  selectAll(ev){
    if(ev.currentTarget.checked==true){
      this.choose_all=1;
    this.consumers.forEach(ele=>{
      if(this.temp.includes(ele.user.user_id)){
        var index = this.result.indexOf(ele.user.user_id);
        // this.result.splice(index, 1);
        // console.log("index:",this.result.indexOf(ele.user.user_id))
        // this.temp.splice(this.temp.indexOf(ele.user.user_id),1)
      }else{
        this.temp.push(ele.user.user_id);
      }
      this.result=this.temp.reduce((arr,x)=>{
        let exists = !!arr.find(y => y === x);
        if(!exists){
            arr.push(x);
        }
        if(this.result.length>0){
          this.view=true;
        }else{
          this.view=false;
        }
        return arr;
    }, []);
    })
  }else{
    this.choose_all=0;
    this.temp=[];
    this.result=[];
  }
  }
}
