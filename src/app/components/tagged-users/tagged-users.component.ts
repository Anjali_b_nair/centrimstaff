import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendanceGroupComponent } from '../activity-attendance-group/activity-attendance-group.component';
import { ConsentTagAlertComponent } from '../consent-tag-alert/consent-tag-alert.component';

@Component({
  selector: 'app-tagged-users',
  templateUrl: './tagged-users.component.html',
  styleUrls: ['./tagged-users.component.scss'],
})
export class TaggedUsersComponent implements OnInit {
  consumers:any[]=[];
  
  flag:any;
  terms:any;
  tags:any[]=[];
  
temp:any[]=[];
result:any=[];
view:boolean;
selected:any[]=[];
con:any[]=[];
@Input() data:any;
@Input() filter:any;
rv:any;
dataCopy:any=[];
    constructor(private http:HttpClient,private config:HttpConfigService,
      private modalCntl:ModalController,private storage:Storage,private popCntrl:PopoverController,
      private toastCntlr:ToastController,private loadingCtrl:LoadingController) { }
  
    ngOnInit() {
    }
   async ionViewWillEnter(){
    
      const bid=await this.storage.get("BRANCH");
      this.rv=await this.storage.get('RVSETTINGS');
       
      this.terms='';
      if(this.data&&this.data.length){
        this.dataCopy=this.data.filter(x=>x)
      }
      if(this.dataCopy&&this.dataCopy.length){
        this.tags=this.data;
        this.temp=this.data;
        this.view=true;
        
      }else{
        this.tags=[];
        this.temp=[];
        this.view=false;
      }
      // if(this.filter==2){
      //   this.view=true;
      //   this.viewSelected(this.view)
      // }else{
      //   this.view=false;
      // }
                console.log("tags:",this.tags,this.temp);
                this.showLoading();
                this.result=[];
                  let url=this.config.domain_url+'residents';
                  let headers=await this.config.getHeader();;
                  console.log(url);
  
                  // let headers=await this.config.getHeader();
                  this.http.get(url,{headers}).subscribe((data:any)=>{
                   
                    console.log("data:",data);
                    this.consumers=data.data;
                    this.con=data.data;
                      if(this.view){
                        this.viewSelected(this.view)
                      }
                      this.dismissLoader();
                  },error=>{
                    this.dismissLoader();
                    console.log(error);
                  });
                
    }
         
  
 
  
  
   cancel(){
    // this.hide=false;
    this.terms='';
  }

async dismiss(i){
  if(i==2){
  this.consumers.forEach(ele=>{
    if(this.tags.includes(ele.user.user_id)){
        this.result.push({id:ele.user.user_id,pic:ele.user.profile_pic,name:ele.user.name,consent:ele.consent_to_tag})
    }
  })
}else{
  this.consumers.forEach(ele=>{
    if(this.dataCopy.includes(ele.user.user_id)){
        this.result.push({id:ele.user.user_id,pic:ele.user.profile_pic,name:ele.user.name,consent:ele.consent_to_tag})
    }
  })
}
  let onClosedData:any=this.result;
  await this.modalCntl.dismiss(onClosedData)
}


selectConsumer(item){
  // item.selected=true;
 
  if(this.temp.includes(item.user.user_id)){
    var index = this.tags.indexOf(item.user.user_id);
    this.tags.splice(index, 1);
    
    this.temp.splice(this.temp.indexOf(item.user.user_id),1);
    console.log("index:",item.user.user_id,index,this.tags,this.temp);
  }else{
    this.temp.push(item.user.user_id);
  }
  this.tags=this.temp.reduce((arr,x)=>{
    let exists = !!arr.find(y => y === x);
    if(!exists){
        arr.push(x);
    }
    
    return arr;
}, []);
  
if(this.tags.length>0){
  this.view=true;
}else{
  this.view=false;
}
console.log("sel:",this.tags,this.temp);

}
async byGroup(ev){
  let headers=await this.config.getHeader();
  const popover = await this.modalCntl.create({
    component: ActivityAttendanceGroupComponent,
   
    backdropDismiss:true,
    cssClass:'act-res-group-modal'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned == null || dataReturned.data ==undefined) {
    }else{
      let id = dataReturned.data;
      this.consumers=[];
      if(id!=0){
      let url=this.config.domain_url+'group/'+id;
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log("mem:",res);
  
    // let attend;
    res.data.residents.forEach(element => {
    //   if(this.act_atnd.includes(element.user.user_id)){
    //     attend=true
    //   }else{
    //     attend=false
    //   }
      this.consumers.push(element);
    });
    
  })
}else{
  this.consumers=this.con
}
    }
  
  });
  return await popover.present();
}


viewSelected(view){
  
  if(this.view==true){
    if(this.tags.length>0){
    this.consumers.forEach(ele=>{
      if(this.tags.includes(ele.user.user_id)){
          this.selected.push(ele);
      }
    })
    this.consumers=this.selected
  }
  }else{
    this.selected=[];
    this.consumers=this.con
  }
  this.view=!this.view;
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}

async select(item){
  if(item.consent_to_tag==0){
    if(this.tags.indexOf(item.user.user_id)>=0){
      console.log("consented to tag");
      this.selectConsumer(item);
      
    }
    else{
      this.showAlert(item);
    }
    
  }else{
    this.selectConsumer(item);
  }

}

async showAlert(item){
  const modal = await this.modalCntl.create({
    component: ConsentTagAlertComponent,
    cssClass:'consent-tag-alert-modal',
    componentProps: {
     data:item,
    
      
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data == null || dataReturned.data==undefined) {
  
    }else{
      this.selectConsumer(item);
    }
  });
  return await modal.present();
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}





//  // single function to list residents - new api


//  async getAllresidents(isFirstLoad,event){
  

//   // let url=this.config.domain_url+'get_all_residents_in_branch_with_filter';
//   let url=this.config.domain_url+'get_all_residents_list_via_filters';  
//   let headers=await this.config.getHeader();

//   let body;
//   body={
//     type:1,  // 1 - active 2 -inactive 3 - no contract
//     sort:'fname',  // sortby
//     order:'ASC',   // ASC or DESC
  
//     page:this.offset, 
    
//   }

//   if(this.terms){
//   body.keyword=this.terms
//   }
//                console.log('body:',body,headers);
               
//   this.http.post(url,body,{headers}).subscribe((data:any)=>{
                 
//     console.log("data:",data);

//     for (let i = 0; i < data.data.residents.length; i++) {
//       this.consumers.push(data.data.residents[i]);
//       this.con.push(data.data.residents[i])
//     }

//                       if(this.view){
//                         this.viewSelected(this.view)
//                       }
//     if (isFirstLoad)
//     event.target.complete();
//     this.dismissLoader();
//     this.offset=++this.offset;    
        
//     },error=>{
//       this.dismissLoader();
//       console.log(error);
//     });

// }
// doInfinite(event) {
//   this.getAllresidents(true, event)
//   }

//   cancel(){
//     // this.hide=false;
//     this.terms='';
//     this.offset=1;
//     this.consumers=[];
//     this.getAllresidents(false,'')
//   }
//   search(){
//     // this.hide=true;
//     this.offset=1;
//     this.consumers=[];
//     this.getAllresidents(false,'')
//   }
}
