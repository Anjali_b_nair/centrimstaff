import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-more-visitors',
  templateUrl: './more-visitors.component.html',
  styleUrls: ['./more-visitors.component.scss'],
})
export class MoreVisitorsComponent implements OnInit {
  @Input() data;
 
  constructor() { }

  ngOnInit() {}

}
