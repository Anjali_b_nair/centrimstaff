import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-assigned-technicians',
  templateUrl: './assigned-technicians.component.html',
  styleUrls: ['./assigned-technicians.component.scss'],
})
export class AssignedTechniciansComponent implements OnInit {
  @Input()data;

    constructor(private modalCntl:ModalController) { }
  
    ngOnInit() {}
  
    cancel(){
      this.modalCntl.dismiss();
    }
  
  }


