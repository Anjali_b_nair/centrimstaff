import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { OpenImageComponent } from '../open-image/open-image.component';
import { ShowpdfComponent } from '../showpdf/showpdf.component';

@Component({
  selector: 'app-rv-pet-details',
  templateUrl: './rv-pet-details.component.html',
  styleUrls: ['./rv-pet-details.component.scss'],
})
export class RvPetDetailsComponent implements OnInit {
@Input() item;
pet:any;
emergency_section:boolean=true;
vet_section:boolean=true;
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,private callNumber:CallNumber,
    private toastCntl:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    
    let url=this.config.domain_url+'pet_details/'+this.item.id;
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log('pet:',res);
      this.pet=res.data;
    })
  }
  dismiss(){
    this.modalCntl.dismiss();
  }
  hideSection(i){
    if(i==1){
      this.emergency_section=!this.emergency_section
    }else{
      this.vet_section=!this.vet_section
    }
  }

  makeCall(num) {
    if(num&&num.length>=10){
    this.callNumber.callNumber(num, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => {
        this.presentAlert('Something went wrong. Try again later.');
        console.log('Error launching dialer', err);
      })
    }else{
      this.presentAlert('Invalid phone number')
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntl.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
  viewPetImages(item){
    if (
      item.includes('.jpg') ||
      item.includes('.png') ||
      item.includes('jpeg')
    ) {
      this.openImg(item);
    } else {
      this.showPdf(item);
    }
  }
  async showPdf(item){
    const modal = await this.modalCntl.create({
      component: ShowpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:item,
        
         
      },
      
      
    });
    return await modal.present();
  }
  async openImg(item) {
   
    const popover = await this.popCntl.create({
      component: OpenImageComponent,
      backdropDismiss: true,
      componentProps: {
        data: item
      },
      cssClass: 'msg_attach'
    });
    return await popover.present();
  }
}
