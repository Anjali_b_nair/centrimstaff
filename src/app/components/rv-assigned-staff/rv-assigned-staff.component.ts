import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-rv-assigned-staff',
  templateUrl: './rv-assigned-staff.component.html',
  styleUrls: ['./rv-assigned-staff.component.scss'],
})
export class RvAssignedStaffComponent implements OnInit {
@Input()data;
@Input() flag;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  cancel(){
    this.modalCntl.dismiss();
  }

}
