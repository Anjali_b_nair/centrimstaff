import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAssignedStaffComponent } from './rv-assigned-staff.component';

describe('RvAssignedStaffComponent', () => {
  let component: RvAssignedStaffComponent;
  let fixture: ComponentFixture<RvAssignedStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAssignedStaffComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAssignedStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
