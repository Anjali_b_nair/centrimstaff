import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-bdyfilter',
  templateUrl: './bdyfilter.component.html',
  styleUrls: ['./bdyfilter.component.scss'],
})
export class BdyfilterComponent implements OnInit {
type:any;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  select(i){
    this.type=i;
    this.closeModal();
  }
  async closeModal() {
    const onClosedData: any = this.type;
    await this.popCntl.dismiss(onClosedData);
  }
}
