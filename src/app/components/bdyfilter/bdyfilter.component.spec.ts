import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BdyfilterComponent } from './bdyfilter.component';

describe('BdyfilterComponent', () => {
  let component: BdyfilterComponent;
  let fixture: ComponentFixture<BdyfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BdyfilterComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BdyfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
