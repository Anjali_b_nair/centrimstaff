import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-activity-warning',
  templateUrl: './activity-warning.component.html',
  styleUrls: ['./activity-warning.component.scss'],
})
export class ActivityWarningComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  cancel(){
    this.modalCntl.dismiss();
  }

  confirm(){
    this.modalCntl.dismiss(1)
  }

}
