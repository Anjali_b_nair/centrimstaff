import { Component, Input, OnInit } from '@angular/core';
import { PopoverController, ToastController, ModalController } from '@ionic/angular';


@Component({
  selector: 'app-cb-choose-family',
  templateUrl: './cb-choose-family.component.html',
  styleUrls: ['./cb-choose-family.component.scss'],
})
export class CbChooseFamilyComponent implements OnInit {
  @Input() data;
  temp:any[]=[];
participants:any[]=[];
sel_id:any[]=[];
  constructor(private popCntrl:PopoverController,private toastCntlr:ToastController,
    private modalCntrl:ModalController) { }

  ngOnInit() {}
ionViewWillEnter(){
  this.participants=[];
  this.sel_id=[];
  this.temp=[];
  console.log("data:",this.data);
  
}

select(item){
  // if(ev.currentTarget.checked==true){
    // this.participants.push({id:item.})
  if(item.fam.user.status==0){
    this.presentAlert('Inactive family member.')
  }else{
    this.temp.push(item.fam.user.user_id);
  this.sel_id=this.temp.reduce((arr,x)=>{
    let exists = !!arr.find(y => y === x);
    if(!exists){
        arr.push(x);
    }else{
      var index = arr.indexOf(x);
      arr.splice(index, 1);
     
    }
    
    return arr;
}, []);
  
  
  console.log('part:',this.participants,'temp:',this.temp);
  }
}


async dismiss(){
  if(this.sel_id.length==0){
    this.presentAlert('Please choose atleast one family member.')
  }else{
    this.data.forEach(element => {
      if(this.sel_id.indexOf(element.fam.user.user_id)>=0){
          this.participants.push({cid:element.fam.user.user_id,name:element.fam.user.name,pic:element.fam.user.profile_pic})
      }
    });
    console.log(this.participants);
    
  const onClosedData: any = this.participants;
  await this.modalCntrl.dismiss(onClosedData);
  }
}


async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
async cancel(){
  await this.modalCntrl.dismiss();
}
}
