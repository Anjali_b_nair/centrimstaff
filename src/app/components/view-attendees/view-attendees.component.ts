import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-view-attendees',
  templateUrl: './view-attendees.component.html',
  styleUrls: ['./view-attendees.component.scss'],
})
export class ViewAttendeesComponent implements OnInit {
@Input() data:any[];
attendees:any[]=[];
terms:any;
family:any[]=[];
contacts:any[]=[];
rv:any;
  constructor(private http:HttpClient,private config:HttpConfigService,
    private modalCntl:ModalController,private storage:Storage,private loadingCtrl:LoadingController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    
      const bid=await this.storage.get("BRANCH")
      this.rv=await this.storage.get('RVSETTINGS');
      this.showLoading();
    this.attendees=[];
    this.family=[];
    this.getFam();
    let url=this.config.domain_url+'residents';
    let headers=await this.config.getHeader();;
    console.log(url);
  // this.selected=true;
    // let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((data:any)=>{
     
      
      data.data.forEach(element => {
        if(this.data.includes(element.user.user_id)){
        this.attendees.push(element);
        }
      });
      this.dismissLoader();
      console.log("data:",this.attendees);
        
    },error=>{
      console.log(error);
      this.dismissLoader();
    });
  
  }

  async getFam(){
   
      const bid=await this.storage.get("BRANCH")

    this.family=[];
    this.contacts=[];
      
      let url=this.config.domain_url+'resident_contact';
      let headers=await this.config.getHeader();;
      this.http.get(url,{headers}).subscribe((data:any)=>{ 
        console.log("fam:",data);
        data.data.forEach(element => {
        if(element.contacts.length>0){
          
            for(let ele of element.contacts){
        this.contacts.push({res:element.user.name,ele:ele})
            }
        }
      })
      console.log("cont:",this.contacts)
        this.contacts.forEach(el=>{
          if(this.data.includes(el.ele.contact_user_id)){
           
              this.family.push({res:el.res,user:{name:el.ele.user.name},el:el.ele})
           
          }
        })
        this.family=this.family.reduce((arr,x)=>{
          let exists = !!arr.find(y => y.el.contact_user_id === x.el.contact_user_id);
          if(!exists){
              arr.push(x);
          }
         
          return arr;
      }, []);
        console.log("other:",this.family);
     
        });
    
    
     
  }

async dismiss(){
  await this.modalCntl.dismiss();
}
cancel(){
  this.terms='';
}
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
}
