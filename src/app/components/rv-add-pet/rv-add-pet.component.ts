import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Filesystem } from '@capacitor/filesystem';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Chooser } from '@ionic-native/chooser/ngx';
@Component({
  selector: 'app-rv-add-pet',
  templateUrl: './rv-add-pet.component.html',
  styleUrls: ['./rv-add-pet.component.scss'],
})
export class RvAddPetComponent implements OnInit {
@Input()flag;
@Input()item;
@Input()policy;
@Input()consumer_id;
pet:any;
type:any[]=[];
isLoading:boolean=false;
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private actionsheetCntlr:ActionSheetController,private loadingCtrl:LoadingController,
    private alertCntlr:AlertController,private chooser:Chooser,private toastCntl:ToastController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    this.getTypes();
    console.log('petto:',this.item);
    
    if(this.flag==1){
      this.pet={
        additional_info: null,
        breed: null,
        clinic: null,
        colour: null,
        description: null,
        doctor: null,
        emergency_email: null,
        emergency_info: null,
        emergency_name: null,
        emergency_phone: null,
        link_residents: [],
        name: null,
        other_type: null,
        pet_sit: '1',
        pet_type: null,
        petimages: [],
        phone: null
       

      }
    }else{
     
    
        let url=this.config.domain_url+'pet_details/'+this.item.id;
        let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log('pet:',res);
          this.pet=res.data;
          this.pet.pet_type=this.pet.pet_type.toString();
          this.pet.pet_sit=this.pet.pet_sit.toString();
        })
      
    }
  }

  dismiss(){
    this.modalCntl.dismiss();
  }
  async getTypes(){
    const cid=await this.storage.get("COMPANY_ID")
    let url=this.config.domain_url+'get_pet_type/'+cid;
        let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log('type:',res);
          this.type=res.data;
        })
  }

  async addPet(){
    var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(!this.pet.pet_type){
      this.presentAlert('Please select a pet type.')
    }else if(!this.pet.name){
      this.presentAlert('Please enter the pet name.')
    }else if(this.pet.emergency_phone&&this.pet.emergency_phone.length!==10){
      this.presentAlert('Please enter a 10 digit phone number.')
    }else if(this.pet.phone&&this.pet.phone.length!==10){
      this.presentAlert('Please enter a 10 digit phone number.')
    }else if(this.pet.emergency_email&&!pattern.test(this.pet.emergency_email)){
      this.presentAlert('Please enter a valid email Id');
    }else{
    
   const cid=await this.storage.get('COMPANY_ID');
   const role=await this.storage.get('USER_TYPE');
    let petimages=[];
    if(this.pet.petimages&&this.pet.petimages.length){
      this.pet.petimages.forEach(element => {
        petimages.push(element.images)
      });
    }
    let url=this.config.domain_url+'save_resident_pet';
        let headers=await this.config.getHeader();
        let body={
          user_id:this.consumer_id,
          ptype:this.pet.pet_type,
          othertype:this.pet.other_type,
          name:this.pet.name,
          breed:this.pet.breed,
          colour:this.pet.colour,
          pet_sit:this.pet.pet_sit,
          petimages:petimages,
          description:this.pet.description,
          edescription:this.pet.emergency_info,
          ename:this.pet.emergency_name,
          ephone:this.pet.emergency_phone,
          eemail:this.pet.emergency_email,
          clinic:this.pet.clinic,
          doctor:this.pet.doctor,
          phone:this.pet.phone,
          adescription:this.pet.additional_info,
          policy_provided:this.policy,
          company_id:cid,
          resident_ids:[this.consumer_id],
          role_id:role,
          upload_doc:petimages
        }
        console.log('addpet:',body);
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log('addedpet:',res);
          this.modalCntl.dismiss();
        })
      }
  }

  async addFile(){
    const actionSheet = await this.actionsheetCntlr.create({
      header: 'Select',
      buttons: [{
        text: 'Image',
        // role: 'destructive',
        icon: 'image-outline',
        handler: () => {
          this.selectImage();
          console.log('Delete clicked');
        }
      }, {
        text: 'Document',
        icon: 'document-outline',
        handler: () => {
          this.selectDoc();
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        // role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  selectDoc(){
    this.chooser.getFile()
  .then(file => {
   
    if(file){
      console.log('type:',file.mediaType);
    
      if(file.mediaType=="application/pdf"){
      this.uploadImage(file.dataURI);
    }else{
      this.alert('File format not supported');
    }
  }
  })
  .catch((error: any) => console.error(error));
  }



  async alert(mes){
    
    const alert = await this.alertCntlr.create({
      mode:'ios',
      message: mes,
      backdropDismiss:true
    });
  
    await alert.present();
  }

  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.captureImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  async captureImage(){
 

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
   
   
    this.uploadImage(base64Image);

  }
  
  
  
  async addImage(){
  

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
             
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    
    }
    async uploadImage(img){
      if(this.isLoading==false){
        this.showLoading();
        }
      let url=this.config.domain_url+'upload_file';
      let body={file:img};
      let headers=await this.config.getHeader();;
    
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("uploaded:",res);
        this.pet.petimages.push({images:res.data});
        this.dismissLoader();
        
      })
    }
    async showLoading() {
      this.isLoading=true;
      const loading = await this.loadingCtrl.create({
        cssClass: 'custom-loading',
        // message: '<ion-img src="assets/loader.gif"></ion-img>',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
      this.isLoading=false;
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
    removeImg(i){
      this.pet.petimages.splice(i,1);
    }

    async edit(){
      const cid=await this.storage.get('COMPANY_ID');
      const role=await this.storage.get('USER_TYPE');
      var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if(!this.pet.pet_type||!this.pet.name){
        this.presentAlert('Please fill the required fields')
      }
        else if(this.pet.emergency_phone&&this.pet.emergency_phone.length!==10){
          this.presentAlert('Please enter a 10 digit phone number.')
        }else if(this.pet.phone&&this.pet.phone.length!==10){
          this.presentAlert('Please enter a 10 digit phone number.')
        }else if(this.pet.emergency_email&&!pattern.test(this.pet.emergency_email)){
          this.presentAlert('Please enter a valid email Id');
      }else{
      
      let petimages=[];
      if(this.pet.petimages&&this.pet.petimages.length){
        this.pet.petimages.forEach(element => {
          petimages.push(element.images)
        });
      }
      let url=this.config.domain_url+'update_resident_pet';
          let headers=await this.config.getHeader();
          let body={
            user_id:this.consumer_id,
            ptype:this.pet.pet_type,
            othertype:this.pet.other_type,
            name:this.pet.name,
            breed:this.pet.breed,
            colour:this.pet.colour,
            id:this.pet.id,
            pet_sit:this.pet.pet_sit,
            description:this.pet.description,
            edescription:this.pet.emergency_info,
            ename:this.pet.emergency_name,
            ephone:this.pet.emergency_phone,
            eemail:this.pet.emergency_email,
            clinic:this.pet.clinic,
            doctor:this.pet.doctor,
            phone:this.pet.phone,
            adescription:this.pet.additional_info,
            policy_provided:this.policy,
            role_id:role,
            company_id:cid,
            upload_doc:petimages
          }
          console.log('edit:',body);
          
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log('editpet:',res);
            this.modalCntl.dismiss();
          })
        }
    }

    async presentAlert(mes) {
      const alert = await this.toastCntl.create({
        message: mes,
        cssClass: 'toastStyle',
        duration: 3000,
        position:'top'
      });
      alert.present(); //update
    }
}
