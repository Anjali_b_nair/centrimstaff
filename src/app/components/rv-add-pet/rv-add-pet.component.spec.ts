import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAddPetComponent } from './rv-add-pet.component';

describe('RvAddPetComponent', () => {
  let component: RvAddPetComponent;
  let fixture: ComponentFixture<RvAddPetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAddPetComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAddPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
