import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-send-mail-warning',
  templateUrl: './send-mail-warning.component.html',
  styleUrls: ['./send-mail-warning.component.scss'],
})
export class SendMailWarningComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  cancel(){
    this.modalCntl.dismiss();
  }

  confirm(){
    this.modalCntl.dismiss(1)
  }

}
