import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-add-new-allergy',
  templateUrl: './rv-add-new-allergy.component.html',
  styleUrls: ['./rv-add-new-allergy.component.scss'],
})
export class RvAddNewAllergyComponent implements OnInit {
@Input() data;
allergy:any;
  constructor(private modalCntl:ModalController,private storage:Storage,private toastCtlr:ToastController,
    private config:HttpConfigService,private http:HttpClient) { }

  ngOnInit() {}

  dismiss(){
    this.modalCntl.dismiss();
  }

  async create(){
    const cid = await this.storage.get("COMPANY_ID");
    if(!this.allergy){
      this.presentAlert('Please enter the allergy')
    }else{
      let url;
      if(this.data==1){
      url=this.config.domain_url+'quick_create_med_allergy';
      }else if(this.data==2){
        url=this.config.domain_url+'quick_create_allergy';
      }else{
        url=this.config.domain_url+'quick_create_medical_condition';
      }
      let headers=await this.config.getHeader();
      let body={
        name:this.allergy,
        company_id:cid
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log('added:',res);
        if(res.status=='success'){
        this.presentAlert('Added successfully');
        this.modalCntl.dismiss();
        }else{
          this.presentAlert(res.message)
        }
      })
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

}
