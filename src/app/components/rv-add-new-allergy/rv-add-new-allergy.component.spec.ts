import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAddNewAllergyComponent } from './rv-add-new-allergy.component';

describe('RvAddNewAllergyComponent', () => {
  let component: RvAddNewAllergyComponent;
  let fixture: ComponentFixture<RvAddNewAllergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAddNewAllergyComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAddNewAllergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
