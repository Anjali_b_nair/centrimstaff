import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ApproveInvitationComponent } from '../approve-invitation/approve-invitation.component';
import { EditContactComponent } from '../edit-contact/edit-contact.component';

@Component({
  selector: 'app-contact-options',
  templateUrl: './contact-options.component.html',
  styleUrls: ['./contact-options.component.scss'],
})
export class ContactOptionsComponent implements OnInit {
@Input() data:any;
@Input() type:any;
@Input() index:any;

action:any;
  constructor(private alertCnlr:AlertController,private config:HttpConfigService,private popCntrl:PopoverController,
    private http:HttpClient,private storage:Storage,private modalCntrl:ModalController,
    private toastCntlr:ToastController) { }

  ngOnInit() {
    if(this.action==1){
      let onClosedData:any='approve';
      this.popCntrl.dismiss(onClosedData);
    }
  }



  async removeInvitation() {
    const alert = await this.alertCnlr.create({
      mode:'ios',
      header: 'Are you sure?',
      message: 'You will not be able to recover this contact!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            this.popCntrl.dismiss()
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            this.deleteContact();
          }
        }
      ]
    });
  
    await alert.present();
  }
  
    async deleteContact(){
     
        const bid=await this.storage.get("BRANCH")

      let url=this.config.domain_url+'deleteInviteContact/'+this.data.id;
      let headers=await this.config.getHeader();;
  
      this.http.delete(url,{headers}).subscribe((res:any)=>{
        console.log(res);
  
        let onClosedData=this.index;
          this.popCntrl.dismiss(onClosedData);
      })
  
  
    }



    async remove() {
      const alert = await this.alertCnlr.create({
        mode:'ios',
        header: 'Are you sure?',
        message: 'You can activate this contact later.',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntrl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Delete',
            handler: () => {
             
              this.delete();
            }
          }
        ]
      });
    
      await alert.present();
    }
    
     async delete(){
        
          const bid=await this.storage.get("BRANCH")

        let url=this.config.domain_url+'user/'+this.data.user_details.user_id;
        let headers=await this.config.getHeader();;
    
        this.http.delete(url,{headers}).subscribe((res:any)=>{
          console.log(res);
          let onClosedData=this.index;
          this.presentAlert('Inactivated.');
          this.popCntrl.dismiss(onClosedData);
        })
     
    
      }
  





    async approve(){
      // this.popCntrl.dismiss();
      const popover = await this.modalCntrl.create({
        component: ApproveInvitationComponent,
        // event:ev,
        backdropDismiss:true,
        componentProps:{
          data:this.data
        },
        cssClass:'approve-invitation-modal',

        
        
      });
      let that=this;
      // this.popCntrl.dismiss();
      popover.onDidDismiss().then((dataReturned) => {
        console.log('data:',dataReturned);
        let onClosedData:any=dataReturned.data
        that.popCntrl.dismiss(onClosedData);
      })
      return await popover.present()


      // let that=this;
      // popover.onDidDismiss().then((dataReturned) => {
      //   console.log('data:',dataReturned);
      //   this.action=1;
      //   let onClosedData:any=dataReturned.data
      //     that.popCntrl.dismiss(onClosedData);
       
        
      // })
      // return await popover.present();
    }



    async resend() {
      const alert = await this.alertCnlr.create({
        mode:'ios',
        header: 'Resend invitation',
        message: 'Do you want to resend the invitation?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntrl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Resend',
            handler: () => {
             
              this.resendInvitation();
            }
          }
        ]
      });
    
      await alert.present();
    }
    
      async resendInvitation(){
        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'resnt_invite_contact/'+this.data.id;
        // let headers=await this.config.getHeader();
    
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log(res);
          let onClosedData:any='resend'
          this.popCntrl.dismiss(onClosedData);
        })
    
      }


      async edit(){
        
        const popover = await this.modalCntrl.create({
          component: EditContactComponent,
          // event:ev,
          backdropDismiss:true,
          componentProps:{
            data:this.data
          },
          cssClass:'edit-contact-modal'

          
          
        });
        let that=this;
        // this.popCntrl.dismiss();
        popover.onDidDismiss().then((dataReturned) => {
          console.log('data:',dataReturned);
          let onClosedData:any=dataReturned.data
          that.popCntrl.dismiss(onClosedData);
        })
        return await popover.present()
      }



      async activate(){
        let headers=await this.config.getHeader();;
        let url=this.config.domain_url+'activate_user';
        // let headers=await this.config.getHeader();
    let body={user_id:this.data.user_details.user_id}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.presentAlert('Activated.');
          let onClosedData:any='activate'
          this.popCntrl.dismiss(onClosedData);
        })
      }
      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass:'toastStyle',
          duration: 3000,
          position:'top'      
        });
        alert.present(); //update
      }
}
