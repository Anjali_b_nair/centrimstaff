import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-pet-link-resident',
  templateUrl: './rv-pet-link-resident.component.html',
  styleUrls: ['./rv-pet-link-resident.component.scss'],
})
export class RvPetLinkResidentComponent implements OnInit {
@Input()pets;
@Input()residents;
@Input()property;
selected:any[]=[];
selected_pet:any;
  constructor(private modalCntl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private toastCtlr:ToastController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log('data:',this.pets,this.residents);
    
  }

  dismiss(){
    this.modalCntl.dismiss();
  }

  selectResident(id){
    if(this.selected.includes(id)){
      const idx=this.selected.indexOf(id);
      this.selected.splice(idx,1)
    }else{
      this.selected.push(id)
    }
  }

  async continue(){

    if(!this.selected_pet||!this.selected_pet.length){
      this.presentAlert("Please select a pet");
    }else if(!this.selected||!this.selected.length){
      this.presentAlert("Please select a resident");
    }else{
    let url=this.config.domain_url+'link_resident_pet';
    let headers=await this.config.getHeader();
    let body={
      pet_id:this.selected_pet,
      resident_ids:this.selected
    }
    console.log('linking:',body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('linked:',res);
      if (res.status == "success") {
        // toasting the success message
        this.presentAlert("Pet linked successfully");
      }
      this.modalCntl.dismiss();
    })
  }
  }


  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
}
