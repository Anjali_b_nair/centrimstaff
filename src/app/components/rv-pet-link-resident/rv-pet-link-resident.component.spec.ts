import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvPetLinkResidentComponent } from './rv-pet-link-resident.component';

describe('RvPetLinkResidentComponent', () => {
  let component: RvPetLinkResidentComponent;
  let fixture: ComponentFixture<RvPetLinkResidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvPetLinkResidentComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvPetLinkResidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
