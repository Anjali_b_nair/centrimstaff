import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-messageoptions',
  templateUrl: './messageoptions.component.html',
  styleUrls: ['./messageoptions.component.scss'],
})
export class MessageoptionsComponent implements OnInit {

  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  delete(){
    const onClosedData:any=1;
    this.popCntl.dismiss(onClosedData);
    }
}
