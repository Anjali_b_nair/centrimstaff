import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-storyfilter',
  templateUrl: './storyfilter.component.html',
  styleUrls: ['./storyfilter.component.scss'],
})
export class StoryfilterComponent implements OnInit {
type:any;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}

  select(i){
    this.type=i;
    this.closeModal();
  }
  async closeModal() {
    const onClosedData: any = this.type;
    await this.popCntl.dismiss(onClosedData);
  }
}
