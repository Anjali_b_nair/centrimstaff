import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-rv-doc-edit',
  templateUrl: './rv-doc-edit.component.html',
  styleUrls: ['./rv-doc-edit.component.scss'],
})
export class RvDocEditComponent implements OnInit {
  @Input() data;
  name:any;
  constructor(private modalCntl:ModalController,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toastCtlr:ToastController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.name=this.data.title;
  }
  async save(){
    if(!this.name){
      this.presentAlert('Please enter the name.')
    }else{
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'update_res_folder';
    let headers=await this.config.getHeader();
   
  
    let body={
      id:this.data.id,
      title:this.name,
      
    }

    console.log('body:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      this.presentAlert('Updated successfully.')
      this.modalCntl.dismiss(1);
    })
  }
  }

  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  dismiss(){
    this.modalCntl.dismiss();
  }
}
