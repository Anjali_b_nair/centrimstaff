import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvDocEditComponent } from './rv-doc-edit.component';

describe('RvDocEditComponent', () => {
  let component: RvDocEditComponent;
  let fixture: ComponentFixture<RvDocEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvDocEditComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvDocEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
