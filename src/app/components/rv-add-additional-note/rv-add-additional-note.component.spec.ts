import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvAddAdditionalNoteComponent } from './rv-add-additional-note.component';

describe('RvAddAdditionalNoteComponent', () => {
  let component: RvAddAdditionalNoteComponent;
  let fixture: ComponentFixture<RvAddAdditionalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvAddAdditionalNoteComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvAddAdditionalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
