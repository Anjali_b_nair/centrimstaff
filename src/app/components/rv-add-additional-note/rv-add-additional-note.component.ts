import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-rv-add-additional-note',
  templateUrl: './rv-add-additional-note.component.html',
  styleUrls: ['./rv-add-additional-note.component.scss'],
})
export class RvAddAdditionalNoteComponent implements OnInit {
@Input() data;
note:any;
isLoading:boolean=false;
imageResponse:any=[];
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController,
    private toastCntl:ToastController,private actionsheetCntlr:ActionSheetController,private loadingCtrl:LoadingController
    ) { }

  ngOnInit() {}

  async addNote(){
    const bid = await this.storage.get("BRANCH");
    
    const cid = await this.storage.get("COMPANY_ID");
    if(!this.note||this.note==''){
      this.presentAlert('Please enter the note.')
    }else{
      let url=this.config.domain_url+'add_additional_note';
    let headers=await this.config.getHeader();
  
    let body={
      note:this.note,
      note_id:this.data.id,
      attachments:this.imageResponse
    }
    console.log('body:',body);
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     
      this.presentAlert('Note created successfully.');
      this.modalCntl.dismiss();
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
    }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntl.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }
  dismiss(){
    this.modalCntl.dismiss();
  }

  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  async pickImage(){
   
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
   
    this.uploadImg(base64Image);
  }
  async addImage(){
 
  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
          console.log('Image URI: ' + image.photos[i]);
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
            this.uploadImg('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }

  
  
  }
  removeImg(i){
    this.imageResponse.splice(i,1);
    
  }

  async uploadImg(img){
    if(this.isLoading==false){
      this.showLoading();
      }
      let headers=await this.config.getHeader();
    let url=this.config.domain_url+'upload_file';
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      
      this.imageResponse.push(res.data);
      this.dismissLoader();
      
    })
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
}
