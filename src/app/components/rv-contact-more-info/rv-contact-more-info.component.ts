import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-rv-contact-more-info',
  templateUrl: './rv-contact-more-info.component.html',
  styleUrls: ['./rv-contact-more-info.component.scss'],
})
export class RvContactMoreInfoComponent implements OnInit {
@Input()data;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    console.log('contact:',this.data);
    
  }
  dismiss(){
    this.modalCntl.dismiss();
  }
  getContactTypes(purpose){
    return purpose.split(', ')
  }
}
