import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvContactMoreInfoComponent } from './rv-contact-more-info.component';

describe('RvContactMoreInfoComponent', () => {
  let component: RvContactMoreInfoComponent;
  let fixture: ComponentFixture<RvContactMoreInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvContactMoreInfoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvContactMoreInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
