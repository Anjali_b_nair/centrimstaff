import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-select-staff',
  templateUrl: './select-staff.component.html',
  styleUrls: ['./select-staff.component.scss'],
})
export class SelectStaffComponent implements OnInit {
 staff:any[]=[];
  sel_id:any;
  sel_staff:any;
  isLoading:boolean=true;
  @Input() id;
  // branch1:any;
  constructor(private popCntl:ModalController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService) { }

  ngOnInit() {}


  async ionViewWillEnter(){
    console.log("id:",this.id);
    
    
      const bid=await this.storage.get("BRANCH")

    
      
        this.staff=[];
   
        this.staff.push({user_id:0,name:'All'})
    let url1=this.config.domain_url+'staff_viabranch';
    // let headers=await this.config.getHeader();
    // this.storage.ready().then(()=>{
      let headers=await this.config.getHeader();
      const utype=await this.storage.get("USER_TYPE")

        const uid=await this.storage.get("USER_ID")

      const cid=await this.storage.get("COMPANY_ID")

        let body={company_id:cid}
          this.http.post(url1,body,{headers}).subscribe((res:any)=>{
            console.log("arr:",res);
            res.data.forEach(element => {
              this.staff.push({user_id:element.user_id,name:element.name})
            });
            if(this.id!=undefined){
              this.sel_id=this.id;
              this.staff.forEach(ele=>{
                // console.log("el:",ele.id,this.wingId);
                
                if(ele.user_id==this.id){
                  this.sel_staff=ele.name;
                }
              })
            // }
            // else if(utype==3){
            //   this.sel_id=uid;
            //   this.staff.forEach(ele=>{
            //     // console.log("el:",ele.id,this.wingId);
                
            //     if(ele.user_id==uid){
            //       this.sel_staff=ele.name;
            //     }
            //   })
            }else{
              this.sel_staff='All';
              this.sel_id=0
            }
          })
          
      
  }
  setStaff(item){
    
   
    this.sel_id=item.user_id;
    this.sel_staff=item.name;
    this.dismiss();
    // if(staff=='All'){
    //  this.sel_id=0
    // }else{
    // this.staff.forEach(element => {
    //   if(element.name==this.sel_staff){
    //     this.sel_id=element.user_id;
        
    //   }
    
    // });
  // }
    
  }
  
  
 
  async dismiss(){
    const onclosed:any=this.sel_id;
   await this.popCntl.dismiss(onclosed);
  }

}