import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-resident-info',
  templateUrl: './resident-info.component.html',
  styleUrls: ['./resident-info.component.scss'],
})
export class ResidentInfoComponent implements OnInit {
@Input() phone;
@Input() partner;

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  cancel(){
    this.modalCntl.dismiss();
  }
}
