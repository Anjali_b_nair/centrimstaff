import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActFeedbackOptionsComponent } from './act-feedback-options.component';

describe('ActFeedbackOptionsComponent', () => {
  let component: ActFeedbackOptionsComponent;
  let fixture: ComponentFixture<ActFeedbackOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActFeedbackOptionsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActFeedbackOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
