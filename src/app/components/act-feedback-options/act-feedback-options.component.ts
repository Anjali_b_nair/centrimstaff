import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-act-feedback-options',
  templateUrl: './act-feedback-options.component.html',
  styleUrls: ['./act-feedback-options.component.scss'],
})
export class ActFeedbackOptionsComponent implements OnInit {

  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  dismiss(i){
    const onClosedData:any=i;
    this.popCntl.dismiss(onClosedData);
    }
}
