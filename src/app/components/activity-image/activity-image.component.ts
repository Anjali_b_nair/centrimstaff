import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { IonSlides, PopoverController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-activity-image',
  templateUrl: './activity-image.component.html',
  styleUrls: ['./activity-image.component.scss'],
})
export class ActivityImageComponent implements OnInit {
  @Input() data;
  @Input() flag;
 
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
 
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    
  };
  sliderOne: any;
  viewEntered = false;
  // content:any=[];
  constructor(private popOver:PopoverController,private http:HttpClient,private config:HttpConfigService) { }

  ngOnInit() {
    this.sliderOne =
  {
    isBeginningSlide: true,
    isEndSlide: false,
    isActive:false,
    content:[]=this.data
  };
  }

ionViewDidEnter() {
  this.viewEntered = true;
}
  slideNext(object,slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object,slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
    SlideDidChange(object,slideView) {
      this.checkIfNavDisabled(object, slideView);
      object.isActive= true;
    }
    checkIfNavDisabled(object, slideView) {
      this.checkisBeginning(object, slideView);
      this.checkisEnd(object, slideView);
    }
   
    checkisBeginning(object, slideView) {
      slideView.isBeginning().then((istrue) => {
        object.isBeginningSlide = istrue;
      });
    }
    checkisEnd(object, slideView) {
      slideView.isEnd().then((istrue) => {
        object.isEndSlide = istrue;
      });
    }

    dismiss(){
      this.popOver.dismiss();
    }

    async remove(i,id){
      let url=this.config.domain_url+'activity_photo/'+id;
      let headers=await this.config.getHeader();
      console.log("remove img:",i,id);
      this.http.delete(url,{headers}).subscribe((res:any)=>{
        // this.presentAlert('Photos added successfully.');
        // this.modalCntl.dismiss();
        this.data.splice(i,1);
      console.log(this.data);
      if(this.data.length==0){
        this.dismiss();
      }
      })
      
    }
}
