import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import moment from 'moment';
@Component({
  selector: 'app-crfilter',
  templateUrl: './crfilter.component.html',
  styleUrls: ['./crfilter.component.scss'],
})
export class CrfilterComponent implements OnInit {
start:any;
end:any;
startdate:any;
enddate:any;
minDate:string = new Date().toISOString();
current:Date=new Date();
custom:boolean=false;
  constructor(private popCntlr:PopoverController) { }

  ngOnInit() {}





  select(i){
    if(i==1){
      this.custom=false;
      this.start=moment(this.current).format('yyyy-MM-DD');
      this.end=moment(this.current).format('yyyy-MM-DD');
      this.closeModal();
    }else if(i==2){
      this.custom=false;
      this.start=moment().subtract(1,'days').format('yyyy-MM-DD');
      this.end=moment(this.current).format('yyyy-MM-DD');
      this.closeModal();
    }else if(i==3){
      this.custom=false;
      this.start=moment().subtract(1,'weeks').format('yyyy-MM-DD');
      this.end=moment(this.current).format('yyyy-MM-DD');
      this.closeModal();
    }else if(i==4){
      this.custom=false;
      this.start=moment().subtract(1,'months').format('yyyy-MM-DD');
      this.end=moment(this.current).format('yyyy-MM-DD');
      this.closeModal();
    }else if(i==5){
        this.custom=true;
    }
  }


apply(){
  console.log(this.startdate,this.enddate);
  this.start=moment(this.startdate).format('yyyy-MM-DD');
  this.end=moment(this.enddate).format('yyyy-MM-DD');
  this.closeModal();
  
}

  async closeModal() {
    const start: any = this.start;
    const end: any = this.end;
    await this.popCntlr.dismiss(start,end);
  }
}
