import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-set-branch',
  templateUrl: './set-branch.component.html',
  styleUrls: ['./set-branch.component.scss'],
})
export class SetBranchComponent implements OnInit {
  branch:any=[];
  branch_id:any;
  branch1:any;
  loaded:boolean=false;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController) { }

  ngOnInit() {}


  async ionViewWillEnter(){
    this.loaded=false;
    
    const data=await this.storage.get("USER_TYPE")
    const id=await this.storage.get("USER_ID")
      const cid=await this.storage.get("COMPANY_ID")

const bid=await this.storage.get("BRANCH")

const bname=await this.storage.get("BNAME")
      // this.user_type=data;
      let headers=await this.config.getHeader();
      let url=this.config.domain_url+'user_branches';
      let body={
        company_id:cid,
        role_id:data,
        user_id:id
      }
      
      console.log("role:",data);
      
      if(data=='1'){
        console.log("role1");
        
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
         this.branch=res.data;
         console.log("data:",this.branch);
         
        //  this.branch_id=this.branch[0].id;
        //  this.branch1=this.branch[0].name;
        // const bname=await this.storage.get("BNAME")
          if(bname){
          this.branch1=bname;
          }else{
            this.branch1=this.branch[0].name;
            this.storage.set("BRANCH",this.branch[0].id.toString());
            this.storage.set("BNAME",this.branch1);
            this.storage.set("TIMEZONE",this.branch[0].timezone);
            this.storage.set('RVSETTINGS',this.branch[0].retirement_living);
            this.storage.set("PCFBRANCH",this.branch[0].pcs_branch_id);
          }
      })
    
      
    }else{
      
      let headers=await this.config.getHeader();
      console.log("branch_id:",bid);
      console.log(url,body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        this.branch=res.data;
       //  this.branch_id=this.branch[0].id;
       //  this.branch1=this.branch[0].name;
      //  const bname=await this.storage.get("BNAME")
         if(bname){
         this.branch1=bname;
         }else{
           this.branch1=this.branch[0].name;
           
         }
     
   
     })
    }
    //   if(this.user_type!=4){
    //   // const tabBar = document.getElementById('myTabBar');
    //   // tabBar.style.display="flex";
    //   }
     
  }
  @HostListener('touchstart')
    onTouchStart() {
      console.log('pageload:',this.loaded)
      this.loaded=true;
    }
  setBranch(ev){
    if(this.loaded){
    console.log('setBranch clicked1:',this.loaded);
    // if(this.loaded){
      console.log('setBranch clicked');
      
      let tz,rv,pcf;
    this.branch.forEach(element => {
      if(element.name==this.branch1){
        this.branch_id=element.id;
        tz=element.timezone;
        rv=element.retirement_living;
        pcf=element.pcs_branch_id;
      }
    });
    
    console.log("branch:",this.branch_id,this.branch1,tz,rv);
    this.storage.set("BRANCH",this.branch_id.toString());
    this.storage.set("BNAME",this.branch1);

    this.storage.set("TIMEZONE",tz);
    this.storage.set('RVSETTINGS',rv);
    this.storage.set("PCFBRANCH",pcf);
    this.modalCntl.dismiss();
  }
  // }
  }


  dismiss(){
    this.modalCntl.dismiss();
  }
}
