import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-add-petty-cash',
  templateUrl: './add-petty-cash.page.html',
  styleUrls: ['./add-petty-cash.page.scss'],
})
export class AddPettyCashPage implements OnInit {
img:any;
name:any;
id:any;
room:any;
minDate: string = new Date().toISOString();
date:any;
amount:any;
type:any='Debit';
note:any;
consumer:any={};
flag:any;
subscription:Subscription;
wing:any;
disable:boolean=false;
  constructor(private router:Router,private storage:Storage,private http:HttpClient,private route:ActivatedRoute,
    private config:HttpConfigService,private objService:GetobjectService,private toastCntlr:ToastController,
    private platform:Platform) { }

  ngOnInit() {
  }
  
  ionViewWillEnter(){
    this.disable=false;
      this.consumer=this.objService.getExtras();
      this.id=this.consumer.id;
      this.img=this.consumer.user.profile_pic;
      this.name=this.consumer.user.name;
      this.room=this.consumer.room;
      this.wing=this.consumer.wing.name;
      this.flag=this.route.snapshot.paramMap.get('flag');

      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
        this.router.navigate(['/petty-cash',{flag:this.flag,id:this.id}])
     
      
        }); 
        
}
 ionViewWillLeave() { 
  this.subscription.unsubscribe();
 }

  selectType(type){
    this.type=type;
    console.log("type:",type);
    
  }
  async save(){
    this.disable=true;
    if(this.type==undefined || this.date==undefined || this.note==undefined || this.note=='' || this.amount==undefined || this.amount==''){
        this.presentAlert("All fields required.");
        this.disable=false;
    }else if(!(/^\d*\.?\d*$/.test(this.amount))){
      this.presentAlert("The amount format is invalid.");
      this.disable=false;
    }else if(/^\d+$/.test(this.note)){
      this.presentAlert("Please specify the purpose correctly.");
      this.disable=false;
    }else{
  
      const cid=await this.storage.get("COMPANY_ID")
      let headers=await this.config.getHeader();;
        const uid=await this.storage.get("USER_ID")

          this.date=this.date.split('.',1);
          this.date=this.date.toString().replace('T',' ');
          let amount;
          if(this.type=='Debit'){
           amount='-'+this.amount;
          }else{
            amount=this.amount
          }
          let url=this.config.domain_url+'pettycash';
          let body={
            user_id:this.id,
            company_id:cid,
            type:this.type,
            date:this.date,
            note:this.note,
            created_by:uid,
            amount:amount
          }
          console.log("body:",body);
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
            this.date='';
            this.note='';
            this.amount='';
            this.presentAlert("Record added successfully.");
            this.router.navigate(['/petty-cash',{flag:this.flag,id:this.id}]);
          })
          // this.objService.setExtras(this.consumer);
          
          
       
    }
  }


  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

}
