import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddPettyCashPage } from './add-petty-cash.page';

describe('AddPettyCashPage', () => {
  let component: AddPettyCashPage;
  let fixture: ComponentFixture<AddPettyCashPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPettyCashPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddPettyCashPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
