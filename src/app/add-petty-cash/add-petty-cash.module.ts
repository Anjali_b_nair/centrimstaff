import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPettyCashPageRoutingModule } from './add-petty-cash-routing.module';

import { AddPettyCashPage } from './add-petty-cash.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPettyCashPageRoutingModule
  ],
  declarations: [AddPettyCashPage]
})
export class AddPettyCashPageModule {}
