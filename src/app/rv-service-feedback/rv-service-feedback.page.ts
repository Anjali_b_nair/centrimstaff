import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { RvFeedbackFilterComponent } from '../components/rv-feedback-filter/rv-feedback-filter.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-service-feedback',
  templateUrl: './rv-service-feedback.page.html',
  styleUrls: ['./rv-service-feedback.page.scss'],
})
export class RvServiceFeedbackPage implements OnInit {
  cid:any;
  id:any;
  feedbacks:any=[];
  sdate:any;
  edate:any;
  offset:any=0;
  terms: any;

  type:any;
  subscription:Subscription;
  count:any;
  code:any;
  status:any='0';
  category:any='0';
  date_filter:any='6';
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.edate=moment().format('YYYY-MM-DD HH:mm:ss');
    this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD HH:mm:ss');
    this.getFeedbackList(false,'');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  }); 
  
  }
  back(){
    this.router.navigate(['/rv-services-home',{id:this.id,cid:this.cid}],{replaceUrl:true})
  }

  async getFeedbackList(isFirstLoad, event){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'ViewResponseList';
    let headers=await this.config.getHeader();
    let body;
    body={
      user_id:this.id,
      company_id:cid,
      start_date:this.sdate,
      end_date:this.edate,
      take:20,
      skip:this.offset
    }
    if(this.type){
      body.feedback_type=this.type
    }

    if(this.terms){
      body.search=this.terms
     
      
    }

    if(this.status!=='0'){
      body.status=this.status
    }
    if(this.category!=='0'){
      body.category_id=this.category
    }
    console.log(body,{headers});
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
     this.count=res.total;
     this.code=res.feedback.code;
     let dateArray=Object.keys(res.data)
      for (let i = this.offset; i < dateArray.length; i++) {
       
        this.feedbacks.push({date:dateArray[i],feedback:res.data[dateArray[i]]});
      }
      
      console.log('feed:',this.feedbacks);
     
      if (isFirstLoad)
      event.target.complete();
  
    this.offset=this.offset+20;
      
     },error=>{
       console.log(error);
      
     })
  }

  doInfinite(event) {
    this.getFeedbackList(true, event)
    }

    changeStatus(ev){
     
      if(ev.detail.value==0){
        this.type=undefined;
      }else{
      this.type=ev.detail.value
      }
      this.feedbacks=[];
      this.offset=0;
      this.getFeedbackList(false,'');
    }
    cancel() {
     
      this.terms = undefined;
      this.ionViewWillEnter();
    }
  
    async search(){
      this.offset=0;
      this.feedbacks=[];
    this.getFeedbackList(false,'');
    }

    async filter(){
      const modal = await this.modalCntl.create({
        component: RvFeedbackFilterComponent,
        componentProps: {
          
          date_filter:this.date_filter,
         category:this.category,
         status:this.status,
         sdate:this.sdate,
         edate:this.edate,
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
        if(dataReturned.data){
          this.date_filter=dataReturned.data.date_filter;
          this.status=dataReturned.data.status;
          this.sdate=dataReturned.data.sdate;
          this.edate=dataReturned.data.edate;
          this.category=dataReturned.data.category;
          this.offset=0;
          this.feedbacks=[];
          this.getFeedbackList(false,'');
        }
        
      })
      return await modal.present();
    }

    ShowContent(a,b,i){
      var x = document.getElementById(a+i);
      var y = document.getElementById(b+i);
            x.style.display = 'none';
            y.style.display = 'block';
    }
}
