import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvServiceFeedbackPage } from './rv-service-feedback.page';

const routes: Routes = [
  {
    path: '',
    component: RvServiceFeedbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvServiceFeedbackPageRoutingModule {}
