import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvServiceFeedbackPageRoutingModule } from './rv-service-feedback-routing.module';

import { RvServiceFeedbackPage } from './rv-service-feedback.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvFeedbackFilterComponent } from '../components/rv-feedback-filter/rv-feedback-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvServiceFeedbackPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvServiceFeedbackPage,
    RvFeedbackFilterComponent
  ],
  entryComponents:[
    RvFeedbackFilterComponent
  ]
})
export class RvServiceFeedbackPageModule {}
