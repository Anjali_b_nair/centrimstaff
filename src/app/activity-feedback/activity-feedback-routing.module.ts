import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityFeedbackPage } from './activity-feedback.page';

const routes: Routes = [
  {
    path: '',
    component: ActivityFeedbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivityFeedbackPageRoutingModule {}
