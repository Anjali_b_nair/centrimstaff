import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ActFeedbackOptionsComponent } from '../components/act-feedback-options/act-feedback-options.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-activity-feedback',
  templateUrl: './activity-feedback.page.html',
  styleUrls: ['./activity-feedback.page.scss'],
})
export class ActivityFeedbackPage implements OnInit {

  act_id:any;
  feedback:any;
  editStatus:boolean=false;
  creation:boolean=true;
  subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private toast:ToastController,
    private http:HttpClient,private config:HttpConfigService,private popCntlr:PopoverController,
    private alertCntlr:AlertController,private platform:Platform) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.act_id=this.route.snapshot.paramMap.get('act_id');
    this.feedback=this.route.snapshot.paramMap.get('feedback');
    if(this.feedback==undefined||this.feedback=='null'){
      this.feedback=undefined;
      this.editStatus=true;
    }else{
      this.creation=false;
    }
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.back();
      })
  }
  back(){
    this.router.navigate(['/activity-details',{act_id:this.act_id}]);
  }

 async submit(){
  let headers=await this.config.getHeader();;
    if(this.feedback==undefined||this.feedback==null){
      this.presentAlert('Please enter the feedback')
    }else{
      let url=this.config.domain_url+'save_activity_feedback';
      let body={
        activity_id:this.act_id,
        feedback:this.feedback
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        if(this.creation){
          this.presentAlert('Feedback added successfully');
        }else{
        this.presentAlert('Updated successfully');
        }
        this.back();
      },error=>{
        console.log(error);
        
        this.presentAlert('Something went wrong. Please try again.')
      })
    }
  }

  async presentAlert(mes) {
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

  async options(ev){

    const popover = await this.popCntlr.create({
      component: ActFeedbackOptionsComponent,
      event:ev,
      backdropDismiss:true,
      cssClass:'message-options'
      
      
    });
    popover.onDidDismiss().then((data)=>{
     if(data.data==1){
        this.editStatus=true;

      
     }else if(data.data==2){
      this.deleteChat();
     }
    })
    return await popover.present();
  }

  async deleteChat() {
    let headers=await this.config.getHeader();;
    const alert = await this.alertCntlr.create({
      mode:'ios',
      header: 'Are you sure?',
      message: 'You will not be able to recover this data!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
            let url=this.config.domain_url+'save_activity_feedback';
            let body={
              activity_id:this.act_id,
              feedback:null
            }
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              
              this.back();
            },error=>{
              console.log(error);
              
              this.presentAlert('Something went wrong. Please try again.')
            })
            
            
          }
        }
      ]
    });
  
    await alert.present();
  }
}
