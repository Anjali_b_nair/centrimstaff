import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityFeedbackPageRoutingModule } from './activity-feedback-routing.module';

import { ActivityFeedbackPage } from './activity-feedback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityFeedbackPageRoutingModule
  ],
  declarations: [ActivityFeedbackPage]
})
export class ActivityFeedbackPageModule {}
