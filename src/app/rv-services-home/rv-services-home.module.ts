import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvServicesHomePageRoutingModule } from './rv-services-home-routing.module';

import { RvServicesHomePage } from './rv-services-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvServicesHomePageRoutingModule
  ],
  declarations: [RvServicesHomePage]
})
export class RvServicesHomePageModule {}
