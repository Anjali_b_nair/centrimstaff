import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvServicesHomePage } from './rv-services-home.page';

const routes: Routes = [
  {
    path: '',
    component: RvServicesHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvServicesHomePageRoutingModule {}
