import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-services-home',
  templateUrl: './rv-services-home.page.html',
  styleUrls: ['./rv-services-home.page.scss'],
})
export class RvServicesHomePage implements OnInit {
  cid:any;
  id:any;
  subscription:Subscription;
  modules: any[] = [];
  maintenance_view: boolean = false;
  act_view: boolean = false;
  feedback:boolean=false;
  task_view: boolean = false;
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private http: HttpClient, private config: HttpConfigService,private storage:Storage) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    let headers=await this.config.getHeader();;
    const cid = await this.storage.get("COMPANY_ID")

    let murl = this.config.domain_url + 'get_modules/' + cid;
    this.http.get(murl,{headers}).subscribe((mod: any) => {
    
      this.modules = mod
      console.log("modules:", this.modules);

    });

    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.index')) {

        this.maintenance_view = true
      } else {

        this.maintenance_view = false;
      }

   

      // activity permissions
      if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {

        this.act_view = true
      } else {

        this.act_view = false;
      }

      if (res.main_permissions.feedback==1&&routes.includes('PCF')) {

        this.feedback = true
      } else {

        this.feedback = false;
      }
      if (res.main_permissions.rv==1&&routes.includes('task.index')) {

        this.task_view = true
      } else {

        this.task_view = false;
      }
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  }); 
  
  }

  back(){
    this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
  }
}
