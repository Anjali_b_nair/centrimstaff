import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonContent, LoadingController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { StoryfilterComponent } from '../components/storyfilter/storyfilter.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.page.html',
  styleUrls: ['./stories.page.scss'],
})
export class StoriesPage implements OnInit {
 
  // user_id:any;
  // branch:any;
   posts:any[]=[];
  
  
  user_img:string;
  url: string;
 
  offset = 0;
  no_post:boolean;
  has_more_items: boolean = true;
  subscription:Subscription;
  
  notify:any;
  not_array:any=[];
  type:any=3;
  scrollPosition = 0;
  create : boolean = false;
  modules: any[] = [];
  dining_view: boolean = false;
  user_view: boolean = false;
  act_view: boolean = false;
@ViewChild('content' , { static: false }) content: IonContent;
rv:any;
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private router:Router,private platform:Platform,private toastCntrl:ToastController,
    private loadingCtrl:LoadingController,private popCntlr:PopoverController) { }

  ngOnInit() {
  }
 async ionViewWillEnter(){
   this.not_array=[];
  // const tabBar = document.getElementById('myTabBar');
  // tabBar.style.display="flex";
  this.posts=[];
  this.offset=0;
    
      const data=await this.storage.get("USER_ID")
      const rv=await this.storage.get('RVSETTINGS');
       this.rv=rv;
          const bid=await this.storage.get("BRANCH")

        let url=this.config.domain_url+'notification';
        let body={
          id:data,
          offset:0,
          limit:20,
          staff_app:1
        }
        let headers=await this.config.getHeader();;
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          
         
          this.notify=res.total_count;
          console.log("len:", this.notify);
        
        })
        const cid = await this.storage.get("COMPANY_ID")
        let murl = this.config.domain_url + 'get_modules/' + cid;
        this.http.get(murl,{headers}).subscribe((mod: any) => {
         
          this.modules = mod
         
    
        })
    this.getPosts(false, "");
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.life_style==1&&routes.includes('story.create')) {

        this.create = true
      } else {

        this.create = false;
      }
      // if (res.main_permissions.residents==1&&routes.includes('residents.index')) {

      //   this.user_view = true
      // } else {

      //   this.user_view = false;
      // }
      if (res.main_permissions.residents==1){
        if(rv==0&&routes.includes('residents.index')||rv==1&&routes.includes('rv_resident.index')) {

        this.user_view = true
      } else {

        this.user_view = false;
      }
    }
      if (res.main_permissions.dining==1) {

        this.dining_view = true
      } else {

        this.dining_view = false;
      }
      if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {

        this.act_view = true
      } else {

        this.act_view = false;
      }
      
    })
  
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    this.content.getScrollElement().then(data => {
      console.log(data.scrollTop);
      this.scrollPosition = data.scrollTop;
    });
    }
  async getPosts(isFirstLoad, event){
this.showLoading();

  const bid=await this.storage.get("BRANCH")


  
    let url=this.config.domain_url+'story?type='+this.type+'&no_page=1&offset='+this.offset+'&limit=21';
   
    let headers=await this.config.getHeader();
    console.log("head:",bid);
    
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(url,res);
      for (let i = 0; i < res.story.length; i++) {
        this.posts.push(res.story[i]);
      }
      // this.dismissLoader();
      console.log("post:",this.posts);
      // this.content.scrollToPoint(0, this.scrollPosition);
      if (isFirstLoad)
      event.target.complete();
  this.dismissLoader();
    this.offset=this.offset+21;
    console.log("off:",this.offset);
    
    }, error => {
      console.log(error);
    })
 
    
  }
  // infinite scrolling

doInfinite(event) {
  this.getPosts(true, event)
  }



  // view story details
gotoDetails(id){
  this.router.navigate(['/story-details',{post_id:id,flag:2}]);
  // this.nav.navigateRoot(['/story-details',{post_id:id,flag:2}])

}

notification(){
  this.router.navigate(['/notification',{flag:1}]);
}
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}


// open filter
async filter(myEvent) {
  const popover = await this.popCntlr.create({
    component: StoryfilterComponent,
    event: myEvent,
    backdropDismiss:true,
    cssClass:'storyfilpop'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data != null ||dataReturned.data!=undefined) {
      this.type = dataReturned.data;
      // console.log("sel_wing:",this.sel_wing);
      
      //alert('Modal Sent Data :'+ dataReturned);
      // if(this.type==0){
      // this.getPosts(false, "");
      // }else{
        this.posts=[];
        this.offset = 0;
        this.getPosts(false, "");
      // }
    }
  });
  return await popover.present();
}

gotoFeeds(){
  if(this.modules.includes('Story')){
  this.router.navigate(['/feeds'])
}else{
  this.presentAlert("Access denied." )
}
}
gotoActivity(){
  if(this.modules.includes('Activity')&&this.act_view){
  this.router.navigate(['/activities'])
}else{
  this.presentAlert("Access denied." )
}
}
gotoDining(){
  console.log('mod:',this.modules.includes('Dining Beta'),this.dining_view)
  if(this.modules.includes('Dining Beta')&&this.dining_view){
  this.router.navigate(['/dining-dashboard'])
}else{
  this.presentAlert("Access denied." )
}
}
gotoResidents(){
  // if(this.user_view){
  // this.router.navigate(['/consumers'])
  if(this.user_view){
    if(this.rv==1){
      this.router.navigate(['/rv-res-list'])
    }else{
  this.router.navigate(['/consumers',{flag:1}])
    }
}else{
  this.presentAlert("Access denied." )
}
}
async presentAlert(mes){
  const alert = await this.toastCntrl.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}
}
