import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartchatPageRoutingModule } from './startchat-routing.module';

import { StartchatPage } from './startchat.page';
// import { ContactsComponent } from '../components/expandable/contacts/contacts.component';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationPipesModule,
    StartchatPageRoutingModule
  ],
  declarations: [StartchatPage]
})
export class StartchatPageModule {}
