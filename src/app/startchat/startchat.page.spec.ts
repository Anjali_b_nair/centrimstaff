import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StartchatPage } from './startchat.page';

describe('StartchatPage', () => {
  let component: StartchatPage;
  let fixture: ComponentFixture<StartchatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartchatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StartchatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
