import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartchatPage } from './startchat.page';

const routes: Routes = [
  {
    path: '',
    component: StartchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartchatPageRoutingModule {}
