import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-startchat',
  templateUrl: './startchat.page.html',
  styleUrls: ['./startchat.page.scss'],
})
export class StartchatPage implements OnInit {
  contacts:any=[];
  staff:any=[];
  family:any=[];
  user_type:any;
  subscription:Subscription;
  branch:any;
  expanded1:boolean=false;
  expanded2:boolean=false;
 expanded3:boolean=false;
user_id:any;
flag:any;
hide:boolean=false;
terms:any;
rv:any;
offset:any = 0;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private router:Router,
    private platform:Platform,private route:ActivatedRoute,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.contacts=[];
    this.family=[];
    this.staff=[];
    this.rv=await this.storage.get('RVSETTINGS');
    this.hide=false;
    this.terms='';
    this.flag=this.route.snapshot.paramMap.get('flag');
    
      const name=await this.storage.get("BNAME")
        this.branch=name
      
      
       const utype=await this.storage.get("USER_TYPE")

         this.user_type=utype
      
       const id=await this.storage.get("USER_ID")

        this.user_id=id
     
    
          
      //  if(this.flag==1 || this.flag==2){
      //     this.getContacts();
      //  }else{
      //     this.getStaffContacts();
      //  }

      if(this.flag==1){
        this.showLoading();
        this.getAllresidents(false,'');
      }else if(this.flag==2){
        this.showLoading();
        this.getContacts(false,'');
      }else{
        this.showLoading();
        this.getStaffContacts(false,'');
      }

    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/message']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  
  
  
  
// get resident contacts
  async getAllresidents(isFirstLoad,event){
   
  // if(!isFirstLoad){
  //   this.showLoading();
  // }
    let url=this.config.domain_url+'get_chat_resident_list';
                 
    let headers=await this.config.getHeader();
  
    let body;
    body={
      skip:this.offset,
      take:20,
      user_id:this.user_id
    }
  
    if(this.terms){
    body.keyword=this.terms
    }
                 console.log('body:',body,headers);
                 
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
                   
      console.log("data:",data);
  
      for (let i = 0; i < data.data.length; i++) {
        this.contacts.push(data.data[i]);
      }
  
      if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      this.offset=this.offset+20;    
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });
  
  }
  doInfinite(event) {
   
    if(this.flag==1){
      this.getAllresidents(true,event);
    }else if(this.flag==2){
      this.getContacts(true,event);
    }else{
      this.getStaffContacts(true,event);
    }
    }











// get resident & family contacts
  async getContacts(isFirstLoad,event){
   
    if(!isFirstLoad){
      // this.showLoading();
      // this.contacts=[];
      // this.family=[];
    }
   
    
    
      const uid=await this.storage.get("COMPANY_ID")
          const bid=await this.storage.get("BRANCH")


            let url=this.config.domain_url+'get_chat_family_list';
            let headers=await this.config.getHeader();;
            // let headers=await this.config.getHeader();
            let body;
            body={
              skip:this.offset,
              take:20,
              user_id:this.user_id
            }
            if(this.terms){
              body.keyword=this.terms
              }
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("con:",res,body);
              for (let i = 0; i < res.data.length; i++) {
                this.family.push(res.data[i]);
              }
              if (isFirstLoad)
              event.target.complete();
              this.dismissLoader();
              this.offset=this.offset+20;    
                  
              },error=>{
                this.dismissLoader();
                console.log(error);
              });
        
  }
  
  // get staff contacts
  async getStaffContacts(isFirstLoad,event){
   
    if(!isFirstLoad){
      // this.showLoading();
      this.staff=[];
    }
    
    
    
      const uid=await this.storage.get("USER_ID")

        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")


          let url=this.config.domain_url+'showstaff';
          let headers=await this.config.getHeader();
          let body;
          body={
            user_one:uid,
            nw_id:cid,
            branch_id:bid,
            skip:this.offset,
            take:20
          }
          if(this.terms){
            body.keyword=this.terms
          }
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            // res.data.forEach(element => {
            //   if(element.user_type_id!=4){
            //     this.staff.push(element);
            //   }
            // });
            console.log('staff:',res);
            
            for (let i = 0; i < res.data.length; i++) {
              if(res.data[i].user_type_id!=4&&res.data[i].user_type_id!=9&&res.data[i].user_type_id!=13&&res.data[i].user_type_id!=14){
                this.staff.push(res.data[i]);
              }
            }
            if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      this.offset=this.offset+20;    
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });
           
            
            
       
  }

 


  openResidentChat(item){
    let wing;
    if(item.residents.wing){
      wing=item.residents.wing.name
    }else{
      wing=null
    }
    this.router.navigate(['/chatbox',{user_two:item.user_id,pic:item.profile_pic,name:item.name,flag:1,room:item.residents.room,wing:wing,fam:null,rel:null}]);
}
openFamilyChat(item){
  this.router.navigate(['/chatbox',{user_two:item.user_id,pic:item.profile_pic,name:item.name,flag:1,room:null,wing:null,rel:item.family[0].relation,fam:item.family[0].parent_details[0].user.name}]);
}
openStaffChat(item){
  this.router.navigate(['/chatbox',{user_two:item.user_id,pic:item.profile_pic,name:item.name,flag:1,room:null,wing:null,fam:null,rel:null}]);
}

async showLoading() {
  
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
  
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}



  cancel(){
    
    this.terms='';
    this.offset=0;
    if(this.flag==1){
      this.contacts=[];
      this.getAllresidents(false,'')
    }else if(this.flag==2){
    
      this.family=[];
      this.getContacts(false,'');
    }else{
    this.staff=[];
      this.getStaffContacts(false,'');
    }
  }
  search(){
   
    this.offset=0;
    if(this.flag==1){
    this.contacts=[];
    this.getAllresidents(false,'')
    }else if(this.flag==2){
     
      this.family=[];
      this.getContacts(false,'');
    }else{
     this.staff=[];
      this.getStaffContacts(false,'');
    }
  }
}
