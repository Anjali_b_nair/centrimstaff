import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ActivityAddPhotosComponent } from '../components/activity-add-photos/activity-add-photos.component';
import { ActivityImageComponent } from '../components/activity-image/activity-image.component';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { ViewImageComponent } from '../components/view-image/view-image.component';
import { HttpConfigService } from '../services/http-config.service';
import { ActivityAttendeesOptionsComponent } from '../components/activity-attendees-options/activity-attendees-options.component';

// declare var jitsiplugin;
@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.page.html',
  styleUrls: ['./activity-details.page.scss'],
})
export class ActivityDetailsPage implements OnInit {
  act_id:any;
  token:any;
  activity:any=[];
  doc:any=[];
  type:any={};
  date:string;
  day:any;
  month:any;
  year:any;
  subscription:Subscription;
  image:any=[];
  attendees:any=[];
  sel_id:any[]=[];
  photos:any=[];
  markToday:boolean=false;
  canEdit:boolean=true;
  facility:any[]=[];
  assigned_residents:any[]=[];
  rvsettings:any;
  segment:any='1';
  signupcount:any=0;
  waiting_list:any[]=[];
  edit_status:boolean=false;
  delete_status:boolean=false;
  story_status:boolean=false;
  mark:boolean=false;
  limit_required:any;
payment_required:any;
  constructor(private route:ActivatedRoute,private http:HttpClient,private storage:Storage,private platform:Platform,
    private config:HttpConfigService,private router:Router,private iab:InAppBrowser,
    private nav:NavController,private popCntlr:PopoverController,private dialog:SpinnerDialog,
    private alertCntl:AlertController,private toastCntl:ToastController,private actionsheetCntlr:ActionSheetController,
    private modalCntl:ModalController,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  
}
async ionViewWillEnter(){
  //get activity id through navigation 
  this.doc=[];
  this.segment='1';
  this.attendees=[];
  this.sel_id=[];
  this.assigned_residents=[];
  this.facility=[];
this.act_id=this.route.snapshot.paramMap.get('act_id');
const rv=await this.storage.get('RVSETTINGS');
  this.rvsettings=rv
  if(rv==1){
    this.getRVsettings();
  }
this.getContent();
if(rv==1){
  
  this.signupList();
  }
(await this.config.getUserPermission()).subscribe((res: any) => {
  console.log('permissions:', res);
  let routes = res.user_routes;
  if (res.main_permissions.life_style==1&&routes.includes('activity.edit')) {

    this.edit_status= true
  } else {

    this.edit_status = false;
    
  }
  if (res.main_permissions.life_style==1&&routes.includes('activity.destroy')) {

    this.delete_status= true
  } else {

    this.delete_status = false;
    
  }
  if (res.main_permissions.life_style==1&&routes.includes('story.create')) {

    this.story_status= true
  } else {

    this.story_status = false;
    
  }
  if (res.main_permissions.life_style==1&&routes.includes('activity.mark_attendance')) {

    this.mark= true
  } else {

    this.mark = false;
    
  }
})
this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
this.router.navigate(['/activities']) ;
}); 

}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

async getContent(){

  const uid=await this.storage.get("USER_ID")

    const type=await this.storage.get("USER_TYPE")
  const data=await this.storage.get("TOKEN")
    let token=data;

    const bid=await this.storage.get("BRANCH")


      let branch=bid.toString();
let headers=await this.config.getHeader();
let url=this.config.domain_url+'activity/'+this.act_id;
// let headers=await this.config.getHeader();
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.activity=res.activity;
    if(type==1||type==2){
    }else{
      if(uid!==this.activity.creator.user_id){
      this.canEdit=false
      }
    }
    this.image=this.activity.images;
    this.photos=this.activity.activity_photos;
    this.date=this.activity.activity_start_date;
    
    if(new Date().toDateString()===new Date(this.date.replace(' ','T')).toDateString()){
      this.markToday=true;
    }
    this.type=this.activity.type;
    console.log("type:",this.type);
    
    this.activity.attachments.forEach(el=>{
      console.log("att:",el.activity_attachment.substr(el.activity_attachment.lastIndexOf('/') + 1));
      let attch_name;
      attch_name=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('/') + 1);
      let ext=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('.') + 1);
      this.doc.push({'title':attch_name,'post_attachment':el.activity_attachment,'ext':ext});
     
    })
    if(this.activity.branch_details.length>0){
      this.activity.branch_details.forEach(element => {
        this.facility.push(element.id.toString())
      });
    }
    if(this.rvsettings!=1){
    let url=this.config.domain_url+'residents';
    this.http.get(url,{headers}).subscribe((data:any)=>{
      let attend=[];
      if(this.activity.tagged_users.length>0){
      // this.activity.tagged_users.forEach(element => {
      //   attend.push(element.user_id);
      // });
      for(let i in this.activity.tagged_users){
      data.data.forEach(element => {
        // if(attend.includes(element.user_id)){
          if(this.activity.tagged_users[i].user_id==element.user_id){
          this.attendees.push({data:element,type:0,action:this.activity.tagged_users[i].action,reason:this.activity.tagged_users[i].reason});
          this.sel_id.push(element.user_id);
          this.assigned_residents.push(element.user_id);
        }
      });
    }
    console.log("act:",this.attendees);
      }
    })
       
    let url1=this.config.domain_url+'resident_contact';
    // let headers=await this.config.getHeader();;
    this.http.get(url1,{headers}).subscribe((data:any)=>{ 
      console.log("fam:",data);
      let attend=[];
      if(this.activity.tagged_users.length>0){
      // this.activity.tagged_users.forEach(element => {
      //   attend.push(element.user_id);
      // });
      // data.data.forEach(element => {
      //   if(element.contacts.length>0){
      //     element.contacts.forEach(el=>{
      //       if(attend.includes(el.contact_user_id)){
      //         this.attendees.push({data:el,user:element.user.name,type:1});
      //         this.sel_id.push(el.contact_user_id);
             
             
      //       }
      //     })
        
      // }
      // });

      for(let i in this.activity.tagged_users){
        data.data.forEach(element => {
          if(element.contacts.length>0){
                element.contacts.forEach(el=>{
                  // if(attend.includes(el.contact_user_id)){
                  //   this.attendees.push({data:el,user:element.user.name,type:1});
                  //   this.sel_id.push(el.contact_user_id);
                    if(this.activity.tagged_users[i].user_id==el.contact_user_id){
                      this.attendees.push({data:el,type:1,user:element.user.name,action:this.activity.tagged_users[i].action,reason:this.activity.tagged_users[i].reason});
                      this.sel_id.push(el.contact_user_id);
                    }
                   
                  
                })
              
            }
            
        });
      }
      console.log("act:",this.attendees);
      }
      
      
      
      });
    }
    console.log("doc:",this.doc);


    // this.activity.attachments.forEach(el=>{
    //   let attch_name=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('_') + 1);
    //   let ext=el.post_attachment.substr(el.activity_attachment.lastIndexOf('.') + 1);
    //   this.doc.push({'title':attch_name,'post_attachment':el.activity_attachment,'ext':ext});
    // })
    this.year=this.date.slice(0,4);
    let x= this.date.slice(5,7);
    if(x=='01'){
      this.month="Jan";
    }
    
    else if(x=='02'){
      this.month="Feb";
    }
    else if(x=='03'){
      this.month="Mar";
    }
    else if(x=='04'){
      this.month="Apr";
    }
    else if(x=='05'){
      this.month="May";
    }
    else if(x=='06'){
      this.month="Jun";
    }
    else if(x=='07'){
      this.month="Jul";
    }
    else if(x=='08'){
      this.month="Aug";
    }
    else if(x=='09'){
      this.month="Sep";
    }
    else if(x=='10'){
      this.month="October";
    }
    else if(x=='11'){
      this.month="Nov";
    }
    else if(x=='12'){
      this.month="Dec";
    }
    this.day=this.date.slice(8,10);
   
    
  },error=>{
    console.log(error);
    
  })

}

// open document
openDoc(item){
// window.open(encodeURI(item.activity_attachment),"_system","location=yes");


let options:InAppBrowserOptions ={
  location:'yes',
  hidenavigationbuttons:'yes',
hideurlbar:'yes',
zoom:'yes'
}
if(item.post_attachment.includes('.pdf')){
  this.showpdf(item.post_attachment)
}else{
const browser = this.iab.create('https://docs.google.com/viewer?url='+item.post_attachment+'&embedded=true','_blank',options);
browser.on('loadstart').subscribe(() => {
  console.log('start');
  this.dialog.show();   
 
}, err => {
  console.log(err);
  
  this.dialog.hide();
})

browser.on('loadstop').subscribe(()=>{
  console.log('stop');
  
  this.dialog.hide();;
}, err =>{
  this.dialog.hide();
})

browser.on('loaderror').subscribe(()=>{
  this.dialog.hide();
}, err =>{
  this.dialog.hide();
})

browser.on('exit').subscribe(()=>{
  this.dialog.hide();
}, err =>{
  this.dialog.hide();
})
}

// let path = null;
//     if(this.platform.is('ios')){
//       path=this.file.documentsDirectory;
//     }
//     else if(this.platform.is('android')){
//       path=this.file.externalDataDirectory;
//     }
//       const fileTransfer: FileTransferObject=this.transfer.create();
//       fileTransfer.download(item.activity_attachment,path+'Activity report').then(entry=>{
//         console.log("download");
      
//         let url=entry.toURL();
//         console.log("url",url);
      
//         var fileExtension = item.activity_attachment.substr(item.activity_attachment.lastIndexOf('.') + 1);
//         let fileMIMEType=this.getMIMEtype(fileExtension);
//       // if(fileExtension=='pdf'){
//       this.fileOpener.open(url,fileMIMEType);
//       // }
//       // else if(fileExtension=='doc'){
//       //   this.fileOpener.open(url,'application/msword');
//       // }
//       });
}



getMIMEtype(extn){
let ext=extn.toLowerCase();
let MIMETypes={
'txt' :'text/plain',
'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
'doc' : 'application/msword',
'pdf' : 'application/pdf',
'jpg' : 'image/jpeg',
'bmp' : 'image/bmp',
'png' : 'image/png',
'xls' : 'application/vnd.ms-excel',
'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
'rtf' : 'application/rtf',
'ppt' : 'application/vnd.ms-powerpoint',
'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
}
return MIMETypes[ext];
}

edit(){
  this.nav.navigateRoot(['/edit-activity',{act_id:this.act_id,description:this.activity.activity_info}])
}

async showImages(ev: any,i) {

  let img;
  let flag;
if(i==1){
img=this.image;
flag=2
}else{
  img=this.photos;
  flag=3
}
  const popover = await this.popCntlr.create({
    component: ActivityImageComponent,
    event: ev,
    componentProps:{
      data:img,
      flag:flag,
    },
    cssClass:'act-image_pop'
  });
  return await popover.present();
}

addAttendee(){
  
  this.router.navigate(['/add-attendees',{act_id:this.act_id,attendee:JSON.stringify(this.sel_id),rv_details:JSON.stringify(this.activity.rv_details),limit:this.limit_required,signupCount:this.signupcount}],{replaceUrl:true})
}

start(item){
  console.log("url:",item);
 
//  if(this.platform.is('android')){
//   const roomId =item.substr(item.lastIndexOf('/') + 1);

//   // const roomId = 'VqyaB3flP';
//   jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, false, function (data) {
//     if (data === "CONFERENCE_WILL_LEAVE") {
//         jitsiplugin.destroy(function (data) {
//             // call finished
//         }, function (err) {
//             console.log(err);
//         });
//     }
// }, function (err) {
//     console.log(err);
// });
//  }else {
  let options:InAppBrowserOptions ={
    location:'no',
    hideurlbar:'yes',
    zoom:'no'
  }
  
  const browser = this.iab.create(item,'_system',options);
  
//  }

}
videocall(html,ev){
  console.log("href:",ev.target.href);
  if(this.platform.is('android')){
    ev.preventDefault();
  if(this.type.id==9){
    var regex = /href\s*=\s*(['"])(https?:\/\/.+?)\1/ig;
    var link;
  
    while((link = regex.exec(html)) !== null) {
console.log('link:',link[2]);
if(link[2].includes('calllink')){
  ev.preventDefault();
this.start(link[2]);
      // html = html.replace(link[2], "https://ctrlq.org?redirect_to" + encodeURIComponent(link[2]));
    }
    }
    // return html;
  }else{
    if(ev.target.href==undefined){

    }else{
      let options:InAppBrowserOptions ={
        location:'no',
        hideurlbar:'yes',
        zoom:'no'
      }
      
      const browser = this.iab.create(ev.target.href,'_system',options);
    }
  }
}else{
  if(ev.target.href==undefined){

  }else{
  let options:InAppBrowserOptions ={
    location:'no',
    hideurlbar:'yes',
    zoom:'no'
  }
  
  const browser = this.iab.create(ev.target.href,'_system',options);
}
}
}

async delete(){
  let headers=await this.config.getHeader();
  const alert = await this.alertCntl.create({
    mode:'ios',
    header: 'Delete activity?',
    message: 'Are you sure you want to delete this activity?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Delete',
        handler: () => {
        this.http.delete(this.config.domain_url+'activity/'+this.act_id,{headers}).subscribe((res:any)=>{
          console.log('delete:',res);
          if(res.status=="success"){
            this.Toast('Activity deleted successfully.')
            this.router.navigate(['/activities']) ;
          }else{
            this.Toast('Something went wrong.Please try again later.');
          }
        })
          
        }
      }
    ]
  });

  await alert.present();
}

async Toast(mes){
  const alert = await this.toastCntl.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}


async actOptions() {

let button=[];
if(this.edit_status){
  button.push({
    
      icon:'pulse-outline',
      text: 'Activity feedback',
      handler: () => {
        this.router.navigate(['/activity-feedback',{act_id:this.act_id,feedback:this.activity.feedback}])
      }
    },{
      icon:'image-outline',
      text: 'Activity photos',
      handler: () => {
       this.openActivityPhotos();
      }
    })
}
if(this.story_status){
  button.splice(1,0,{
      icon:'globe-outline',
      text: 'Create story',
      handler: () => {
        this.router.navigate(['/create-story',{flag:5,act_id:this.act_id,title:this.activity.title,desc:this.activity.activity_info,img:JSON.stringify(this.image),doc:JSON.stringify(this.doc),tagged:JSON.stringify(this.assigned_residents),fac:JSON.stringify(this.facility)}])
      }
    })
}
if(this.delete_status){
  button.push({
    icon:'trash-outline',
    text: 'Delete activity',
    handler: () => {
      this.delete();
    }
  })
}

button.push({
  icon: 'close',
  text: 'Cancel',
  role: 'cancel',
  handler: () => {
   
  }
})

  const actionSheet = await this.actionsheetCntlr.create({
    // header: "Select Image source",
    cssClass:'activity-options',
    buttons:button
    // buttons: [{
   
    //   icon:'pulse-outline',
    //   text: 'Activity feedback',
    //   handler: () => {
    //     this.router.navigate(['/activity-feedback',{act_id:this.act_id,feedback:this.activity.feedback}])
    //   }
    // },
    // {
    //   icon:'globe-outline',
    //   text: 'Create story',
    //   handler: () => {
    //     this.router.navigate(['/create-story',{flag:5,act_id:this.act_id,title:this.activity.title,desc:this.activity.activity_info,img:JSON.stringify(this.image),doc:JSON.stringify(this.doc),tagged:JSON.stringify(this.assigned_residents),fac:JSON.stringify(this.facility)}])
    //   }
    // },
    // {
    //   icon:'image-outline',
    //   text: 'Activity photos',
    //   handler: () => {
    //    this.openActivityPhotos();
    //   }
    // },
   
    // {
    //   icon:'trash-outline',
    //   text: 'Delete activity',
    //   handler: () => {
    //     this.delete();
    //   }
    // },
    // {
    //   icon: 'close',
    //   text: 'Cancel',
    //   role: 'cancel',
    //   handler: () => {
       
    //   }
    // }
    // ]
  });
  await actionSheet.present();
}

async openActivityPhotos(){
  const modal = await this.modalCntl.create({
    component: ActivityAddPhotosComponent,
    cssClass:"vdoModal",
    componentProps: {
     id:this.act_id,
      
        
    }
  });
  modal.onDidDismiss().then(()=>{
   this.ionViewWillEnter();
  })
  return await modal.present();
}

segmentChanged(event){

}

async signupList(){
  this.showLoading();
  this.waiting_list=[];
  this.attendees=[];
  this.sel_id=[];
  const bid=await this.storage.get("BRANCH");
  const cid=await this.storage.get("COMPANY_ID")
  const uid=await this.storage.get("USER_ID")
  const utype=await this.storage.get("USER_TYPE")
  let headers=await this.config.getHeader();


  let uri=this.config.domain_url+'activity_all_attendees';
  let body={
    activity_id:this.act_id,
    role_id:utype,
    company_id:cid,
    branch_id:bid,
    user_id:uid
  }

  this.http.post(uri,body,{headers}).subscribe((res:any)=>{
    console.log('allatte:',res);

    res.data.activity_attendee.forEach(element => {
      if(element.signups&&element.signups.action==1){
      this.attendees.push({data:element,type:0,action:element.activity_action.action,reason:element.activity_action.reason})
      this.sel_id.push(element.user_id);
            this.assigned_residents.push(element.user_id);
      }else if(element.signups&&element.signups.action==0){
        this.waiting_list.push({data:element,type:0,action:element.activity_action.action});
      }
    });
    res.data.contact_attendee.forEach(el => {
      if(el.signups&&el.signups.action==1){
    this.attendees.push({data:{user:el},type:1,user:el.name,action:el.activity_action.action,reason:el.activity_action.reason})
    this.sel_id.push(el.user_id)
      }else if(el.signups&&el.signups.action==0){
        this.waiting_list.push({data:{user:el},type:1,user:el.name,action:el.activity_action.action});
      }
    })

    console.log('wl:',this.waiting_list)
    if(this.attendees&&this.attendees.length){
      this.signupcount=this.attendees.length
    }else{
      this.signupcount=0;
      this.segment='2'
    }
    this.dismissLoader();
  },error=>{
    this.dismissLoader();
  })


  // let url=this.config.domain_url+'activity_signup_list/'+this.act_id;
  //   // let headers=await this.config.getHeader();
  //   console.log(url)
  //     this.http.get(url).subscribe((res:any)=>{
  //       console.log('signup:',res);
  //       if(res.data.signup&&res.data.signup.length){
  //         this.signupcount=res.data.signup.length
  //       }else{
  //         this.signupcount=0
  //       }

        
       
  //       let url=this.config.domain_url+'residents';
  //   this.http.get(url,{headers}).subscribe((data:any)=>{
      
     
  //     if(res.data.waiting_list&&res.data.waiting_list.length){
  //     for(let i in res.data.waiting_list){
  //     data.data.forEach(element => {
  //       // if(attend.includes(element.user_id)){
          

          
  //         if(res.data.waiting_list[i].user_id==element.user_id){
  //         this.waiting_list.push({data:element,type:0,action:res.data.waiting_list[i].action});
          
  //       }
  //     });
  //   }
  // }

  // // if(res.data.signup&&res.data.signup.length){
  // //   for(let i in res.data.signup){
  // //   data.data.forEach(element => {
  // //     if(res.data.signup[i].user_id==element.user_id){
  // //     const idx=this.activity.tagged_users.map(x=>x.user_id).indexOf(element.user_id);
      
  // //     if(idx>=0){
  // //       this.attendees.push({data:element,type:0,action:this.activity.tagged_users[idx].action,reason:this.activity.tagged_users[idx].reason})
  // //       this.sel_id.push(element.user_id);
  // //             this.assigned_residents.push(element.user_id);
  // //     }
  // //   }
  // //   })
  // // }
  // // }
       
  //     })

  //     let url1=this.config.domain_url+'resident_contact';
    
  //   this.http.get(url1,{headers}).subscribe((data:any)=>{
      
  //     if(res.data.waiting_list&&res.data.waiting_list.length){
  //       for(let i in res.data.waiting_list){
  //       data.data.forEach(element => {
  //         if(element.contacts.length>0){
  //           element.contacts.forEach(el=>{
             
  //               if(res.data.waiting_list.user_id==el.contact_user_id){
  //                 this.waiting_list.push({data:el,type:1,user:element.user.name,action:res.data.waiting_list[i].action});
                  
  //               }
               
              
  //           })
  //         }
  //       })
          
  //       }
  //   }

  // //   if(res.data.signup&&res.data.signup.length){
  // //     for(let i in res.data.signup){
  // //     data.data.forEach(element => {
  // //       if(element.contacts.length>0){
  // //         element.contacts.forEach(el=>{
  // //       if(res.data.signup[i].user_id==el.user.user_id){
  // //       const idx=this.activity.tagged_users.map(x=>x.user_id).indexOf(el.user.user_id);
        
  // //       let index=-1;
  // //       if(this.attendees.length>0){
  // //         index=this.attendees.map(x=>x.data.user.user_id).indexOf(el.user.user_id)
  // //       }
       
  // //       if(idx>=0&&index<0){
  // //         this.attendees.push({data:el,type:1,user:element.user.name,action:this.activity.tagged_users[idx].action,reason:this.activity.tagged_users[idx].reason})
  // //         this.sel_id.push(el.contact_user_id)
  // //       }
  // //     }
  // //     })
  // //   }
  // //   })
  // //   }
  // // }
  
  //   // this.dismissLoader();
  //   })
      // })
}
async showpdf(file){
  const modal = await this.modalCntl.create({
    component: ShowpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}

async getRVsettings(){
  const bid=await this.storage.get("BRANCH");
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_rv_settings/'+bid
  this.http.get(url,{headers}).subscribe((res:any)=>{
    this.limit_required=res.data.limit_required;
    this.payment_required=res.data.payment_required;
  })
}

async assigneeOptions(ev,i,item){
  console.log(item);
  let wl=0;
  if(this.activity.rv_details&&this.activity.rv_details.waiting_list==1){
    wl=1
  }
  let uid;
  if(item.type==0){
    uid=item.data.user_id
  }else{
    uid=item.data.user.user_id
  }
  const popover = await this.popCntlr.create({
    component: ActivityAttendeesOptionsComponent,
    cssClass:'aa-options-pop',
    event: ev,
    componentProps:{
      act_id:this.act_id,
      flag:i,
      item:uid,
      wl:wl
    },
    
  });
  popover.onDidDismiss().then((dataReturned)=>{
    if(dataReturned.data)
    this.signupList();
   })
  return await popover.present();
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
}
