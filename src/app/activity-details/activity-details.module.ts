import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityDetailsPageRoutingModule } from './activity-details-routing.module';

import { ActivityDetailsPage } from './activity-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ActivityAttendeesOptionsComponent } from '../components/activity-attendees-options/activity-attendees-options.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ActivityDetailsPage,ActivityAttendeesOptionsComponent],
  entryComponents:[ActivityAttendeesOptionsComponent]
})
export class ActivityDetailsPageModule {}
