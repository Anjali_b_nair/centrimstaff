import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-overview',
  templateUrl: './consumer-overview.page.html',
  styleUrls: ['./consumer-overview.page.scss'],
})
export class ConsumerOverviewPage implements OnInit {
user_id:any;
cid:any;
branch_id:any;
user_post:any[]=[];
status:any;
tabStatus:boolean=false;
activity:any[]=[];
user_img:string;
 
  name:string;
  p_name:string;
  commentText:string;
  start:any=0;
  end:any=20;
  flag:any;
  subscription:Subscription;
  consumerName:any;
  constructor(private config:HttpConfigService,private http:HttpClient,private route:ActivatedRoute,
    private router:Router,private storage:Storage,private loadingCtrl:LoadingController,
    private platform:Platform) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.user_post=[];
  this,this.activity=[];
  this.showLoading();

  
    const data=await this.storage.get("NAME")
      this.name=data;
      this.p_name=this.name.substr(0,1);
   
    
    
    const pic=await this.storage.get("PRO_IMG")
      this.user_img=pic;
    
  

  this.user_id=this.route.snapshot.paramMap.get('id');
  this.cid=this.route.snapshot.paramMap.get('cid');
  this.branch_id=this.route.snapshot.paramMap.get('branch');
  this.flag=this.route.snapshot.paramMap.get('flag');
  let body={
    user_id : this.user_id
  }
  let url=this.config.domain_url+'user_post';
  let headers=await this.config.getHeader();;
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
        
        this.user_post=data.data.tag_post;
        console.log("len:",data);
        this.dismissLoader();
        
      },error=>{
        console.log(error);
        
      })
      
        const bid=await this.storage.get("BRANCH")

       let url1=this.config.domain_url+'consumer/'+this.cid;
       
       this.http.get(url1,{headers}).subscribe((res:any)=>{
         console.log(url,'res:',res);
         
         this.activity=res.attended_activity;
         this.consumerName=res.data.user.name
       })
     
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/resident-profile',{id:this.cid,flag:this.flag}])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
getContent(){
  this.tabStatus=!this.tabStatus;
}
async like(item){
  // this.status=liked;
  
    
    const data=await this.storage.get("USER_ID")

      
        const bid=await this.storage.get("BRANCH")

      this.user_id=data;
      
      // this.status=id;
      // this.index=idx;
      if(item.liked==true  ){
         this.status=0
        //  this.lid=id
      }else{
        this.status=1
      }
  let story_id=item.id;
  let body={
    post_id: story_id,
    user_id: this.user_id,
    action_type: '1',
    status:this.status
  };
  console.log("body:",body);
  let url=this.config.domain_url+'update_post_action';
  let headers=await this.config.getHeader();;
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
    console.log(data);
    console.log("like:",data);
    item.liked=!item.liked
    item.liked ? item.likes_count++ : item.likes_count--;
    // feed.IsLike ? feed.LikesCount++ : feed.LikesCount--;
   
  },error=>{
    console.log(error);
    
  });


}
gotoDetails(id){
  // this.router.navigate(['/story-details',{post_id:id}]);
  this.router.navigate(['/story-details',{post_id:id,flag:3,from:this.flag,cid:this.cid}])

}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

viewMore(){
  this.end +=20;
}
}
