import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerOverviewPageRoutingModule } from './consumer-overview-routing.module';

import { ConsumerOverviewPage } from './consumer-overview.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerOverviewPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ConsumerOverviewPage]
})
export class ConsumerOverviewPageModule {}
