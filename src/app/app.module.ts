import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage-angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { VideoEditor } from '@ionic-native/video-editor/ngx';
import { HTTP } from '@ionic-native/http/ngx';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { CallNumber } from "@ionic-native/call-number/ngx";
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
// import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { ViewImageComponent } from './components/view-image/view-image.component';
import { NonetworkComponent } from './components/nonetwork/nonetwork.component';
import { OpenImageComponent } from './components/open-image/open-image.component';
import { UtbVdoComponent } from './components/utb-vdo/utb-vdo.component';
import { ActivityfilterComponent } from './components/activityfilter/activityfilter.component';
import { CbChooseFamilyComponent } from './components/cb-choose-family/cb-choose-family.component';
import { CbCommentComponent } from './components/cb-comment/cb-comment.component';
import { CbInfoComponent } from './components/cb-info/cb-info.component';
import { CbOptionsComponent } from './components/cb-options/cb-options.component';
import { CbRescheduleComponent } from './components/cb-reschedule/cb-reschedule.component';
import { CbReassignComponent } from './components/cb-reassign/cb-reassign.component';
import { CbMoreComponent } from './components/cb-more/cb-more.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CbDisabletimeslotComponent } from './components/cb-disabletimeslot/cb-disabletimeslot.component';
import { ActivityRecurringComponent } from './components/activity-recurring/activity-recurring.component';
import { ActivityAttendeesComponent } from './components/activity-attendees/activity-attendees.component';
import { ApplicationPipesModule } from './application-pipes.module';
import { ViewGroupConsumersComponent } from './components/view-group-consumers/view-group-consumers.component';
import { FilterConsumersComponent } from './components/filter-consumers/filter-consumers.component';
import { CrfilterComponent } from './components/crfilter/crfilter.component';
import { ActivityAttendedReasonComponent } from './components/activity-attended-reason/activity-attended-reason.component';
import { ActivityDeclinedReasonComponent } from './components/activity-declined-reason/activity-declined-reason.component';
import { ActivityAttendanceGroupComponent } from './components/activity-attendance-group/activity-attendance-group.component';
import { ViewAttendeesComponent } from './components/view-attendees/view-attendees.component';
import { TaggedUsersComponent } from './components/tagged-users/tagged-users.component';
import { VisibletoComponent } from './components/visibleto/visibleto.component';
import { ContactOptionsComponent } from './components/contact-options/contact-options.component';
import { ApproveInvitationComponent } from './components/approve-invitation/approve-invitation.component';
import { EditContactComponent } from './components/edit-contact/edit-contact.component';
import { SetBranchComponent } from './components/set-branch/set-branch.component';
import { ActivityImageComponent } from './components/activity-image/activity-image.component';
import { MarkAttendanceMoreComponent } from './components/mark-attendance-more/mark-attendance-more.component';
import { StoryViewedListComponent } from './components/story-viewed-list/story-viewed-list.component';
import { StoryfilterComponent } from './components/storyfilter/storyfilter.component';
import { MoreVisitorsComponent } from './components/more-visitors/more-visitors.component';
import { BdyfilterComponent } from './components/bdyfilter/bdyfilter.component';
import { ConsentTagAlertComponent } from './components/consent-tag-alert/consent-tag-alert.component';
import { MessageAttachmentComponent } from './components/message-attachment/message-attachment.component';
import { MessageoptionsComponent } from './components/messageoptions/messageoptions.component';
import { MaintenanceFilterComponent } from './components/maintenance-filter/maintenance-filter.component';
import { MaintenanceImageComponent } from './components/maintenance-image/maintenance-image.component';
import { TaggedConsumerInfoComponent } from './components/tagged-consumer-info/tagged-consumer-info.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { AttchedImagesComponent } from './components/attched-images/attched-images.component';
import { SelectStaffComponent } from './components/select-staff/select-staff.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ActFeedbackOptionsComponent } from './components/act-feedback-options/act-feedback-options.component';
import { CollapseFilterComponent } from './components/collapse-filter/collapse-filter.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { AdditionalOptionsComponent } from './components/dining/additional-options/additional-options.component';
import { AllergicInfoComponent } from './components/dining/allergic-info/allergic-info.component';
import { ChooseDateComponent } from './components/dining/choose-date/choose-date.component';
import { DietaryPreferencesComponent } from './components/dining/dietary-preferences/dietary-preferences.component';
import { DietaryProfileComponent } from './components/dining/dietary-profile/dietary-profile.component';
import { MarkLeaveComponent } from './components/dining/mark-leave/mark-leave.component';
import { MenuOptionsComponent } from './components/dining/menu-options/menu-options.component';
import { OrderConfirmationComponent } from './components/dining/order-confirmation/order-confirmation.component';
import { SelectConfirmationComponent } from './components/dining/select-confirmation/select-confirmation.component';
import { ServeConfirmationComponent } from './components/dining/serve-confirmation/serve-confirmation.component';
import { ActivityAddPhotosComponent } from './components/activity-add-photos/activity-add-photos.component';
import { MarkLeaveSingledayComponent } from './components/dining/mark-leave-singleday/mark-leave-singleday.component';
import { MenuItemDetailsComponent } from './components/dining/menu-item-details/menu-item-details.component';
import { DiningAreaComponent } from './components/dining/dining-area/dining-area.component';
import { ServingRefusedComponent } from './components/dining/serving-refused/serving-refused.component';
import { DiningAddItemsComponent } from './components/dining/dining-add-items/dining-add-items.component';
import { DiningIrFiDetailsComponent } from './components/dining/dining-ir-fi-details/dining-ir-fi-details.component';
import { ContinentalItemComponent } from './components/dining/continental-item/continental-item.component';
import { ContinentalItemDetailsComponent } from './components/dining/continental-item-details/continental-item-details.component';
import { ContinentalItemTabviewComponent } from './components/dining/continental-item-tabview/continental-item-tabview.component';
import { ItemOptionsComponent } from './components/dining/item-options/item-options.component';
import { AssignSeatComponent } from './components/dining/assign-seat/assign-seat.component';
import { RemoveSeatComponent } from './components/dining/remove-seat/remove-seat.component';
import { UnserveComponent } from './components/dining/unserve/unserve.component';
import { ViewMenuAdditionalComponent } from './components/dining/view-menu-additional/view-menu-additional.component';
import { OrderSuccessMessageComponent } from './components/dining/order-success-message/order-success-message.component';
import { AdditionalItemDetailsComponent } from './components/dining/additional-item-details/additional-item-details.component';
import { ServingConsumedComponent } from './components/dining/serving-consumed/serving-consumed.component';
import { ServingChangeCountComponent } from './components/dining/serving-change-count/serving-change-count.component';
import { MarkAsServedComponent } from './components/dining/mark-as-served/mark-as-served.component';
import { MarkAsServedConfirmComponent } from './components/dining/mark-as-served-confirm/mark-as-served-confirm.component';
import { ConsumptionNoteAlertComponent } from './components/dining/consumption-note-alert/consumption-note-alert.component';
import { OverviewActivityComponent } from './components/overview-activity/overview-activity.component';
import { OverviewDiningComponent } from './components/overview-dining/overview-dining.component';
import { OverviewFoodoptionsComponent } from './components/overview-foodoptions/overview-foodoptions.component';
import { ShowVisitorsComponent } from './components/show-visitors/show-visitors.component';
import { ShowpdfComponent } from './components/showpdf/showpdf.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ResidentComponent } from './components/resident/resident.component';
import { UndoRefuseComponent } from './components/dining/undo-refuse/undo-refuse.component';
import { RvSelectStaffComponent } from './components/rv-select-staff/rv-select-staff.component';
import { RvEditTaskComponent } from './components/rv-edit-task/rv-edit-task.component';
import { RvTaskFilterComponent } from './components/rv-task-filter/rv-task-filter.component';
import { RvTaskOptionsComponent } from './components/rv-task-options/rv-task-options.component';
import { RvDocsOptionsComponent } from './components/rv-docs-options/rv-docs-options.component';
import { RvAssignedStaffComponent } from './components/rv-assigned-staff/rv-assigned-staff.component';
import { RvAssignedResidentComponent } from './components/rv-assigned-resident/rv-assigned-resident.component';
import { NoItemAlertComponent } from './components/dining/no-item-alert/no-item-alert.component';
import { FilePath } from '@ionic-native/file-path/ngx';
import { SendMailWarningComponent } from './components/send-mail-warning/send-mail-warning.component';
import { ActivityWarningComponent } from './components/activity-warning/activity-warning.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewImageComponent,
    NonetworkComponent,
    OpenImageComponent,
    UtbVdoComponent,
    ActivityfilterComponent,
    CbChooseFamilyComponent,
    CbCommentComponent,
    CbInfoComponent,
    CbOptionsComponent,
    CbRescheduleComponent,
    CbReassignComponent,
    CbMoreComponent,
    CbDisabletimeslotComponent,
    ActivityRecurringComponent,
    ActivityAttendeesComponent,
    ViewGroupConsumersComponent,
    FilterConsumersComponent,
    CrfilterComponent,
    ActivityAttendedReasonComponent,
    ActivityDeclinedReasonComponent,
    ActivityAttendanceGroupComponent,
    ViewAttendeesComponent,
    TaggedUsersComponent,
    VisibletoComponent,
    ContactOptionsComponent,
    ApproveInvitationComponent,
    EditContactComponent,
    SetBranchComponent,
    ActivityImageComponent,
    StoryViewedListComponent,
    StoryfilterComponent,
    MoreVisitorsComponent,
    BdyfilterComponent,
    ConsentTagAlertComponent,
    MessageAttachmentComponent,
    MessageoptionsComponent,
    MaintenanceFilterComponent,
    MaintenanceImageComponent,
    TaggedConsumerInfoComponent,
    ThankyouComponent,
    AttchedImagesComponent,
    SelectStaffComponent,
    LocationsComponent,
    ActFeedbackOptionsComponent,
    CollapseFilterComponent,
    ContactsComponent,
    AdditionalOptionsComponent,
    AllergicInfoComponent,
    ChooseDateComponent,
    DietaryPreferencesComponent,
    DietaryProfileComponent,
    MarkLeaveComponent,
    MenuOptionsComponent,
    OrderConfirmationComponent,
    SelectConfirmationComponent,
    ServeConfirmationComponent,
    ActivityAddPhotosComponent,
    MarkLeaveSingledayComponent,
    MenuItemDetailsComponent,
    DiningAreaComponent,
    ServingRefusedComponent,
    DiningAddItemsComponent,
    DiningIrFiDetailsComponent,
    ContinentalItemComponent,
    ContinentalItemDetailsComponent,
    ContinentalItemTabviewComponent,
    ItemOptionsComponent,
    AssignSeatComponent,
    RemoveSeatComponent,
    UnserveComponent,
    ViewMenuAdditionalComponent,
    OrderSuccessMessageComponent,
    AdditionalItemDetailsComponent,
    ServingConsumedComponent,
    ServingChangeCountComponent,
    MarkAsServedComponent,
    NoItemAlertComponent,
    MarkAsServedConfirmComponent,
    ConsumptionNoteAlertComponent,
    OverviewActivityComponent,
    OverviewDiningComponent,
    OverviewFoodoptionsComponent,
    ShowVisitorsComponent,
    ShowpdfComponent,
    NotificationComponent,
    ResidentComponent,
    UndoRefuseComponent,
    RvSelectStaffComponent,
    RvTaskOptionsComponent,
    RvTaskFilterComponent,
    RvEditTaskComponent,
    RvDocsOptionsComponent,
    RvAssignedStaffComponent,
    RvAssignedResidentComponent,
    SendMailWarningComponent,
    ActivityWarningComponent
  ],
  entryComponents: [
    ViewImageComponent,
    NonetworkComponent,
    OpenImageComponent,
    UtbVdoComponent,
    ActivityfilterComponent,
    CbChooseFamilyComponent,
    CbCommentComponent,
    CbInfoComponent,
    CbOptionsComponent,
    CbRescheduleComponent,
    CbReassignComponent,
    CbMoreComponent,
    CbDisabletimeslotComponent,
    ActivityRecurringComponent,
    ActivityAttendeesComponent,
    ViewGroupConsumersComponent,
    FilterConsumersComponent,
    CrfilterComponent,
    ActivityAttendedReasonComponent,
    ActivityDeclinedReasonComponent,
    ActivityAttendanceGroupComponent,
    ViewAttendeesComponent,
    TaggedUsersComponent,
    VisibletoComponent,
    ContactOptionsComponent,
    ApproveInvitationComponent,
    EditContactComponent,
    SetBranchComponent,
    ActivityImageComponent,
    StoryViewedListComponent,
    StoryfilterComponent,
    MoreVisitorsComponent,
    BdyfilterComponent,
    ConsentTagAlertComponent,
    MessageAttachmentComponent,
    MessageoptionsComponent,
    MaintenanceFilterComponent,
    MaintenanceImageComponent,
    TaggedConsumerInfoComponent,
    ThankyouComponent,
    AttchedImagesComponent,
    SelectStaffComponent,
    LocationsComponent,
    ActFeedbackOptionsComponent,
    CollapseFilterComponent,
    ContactsComponent,
    AdditionalOptionsComponent,
    AllergicInfoComponent,
    ChooseDateComponent,
    DietaryPreferencesComponent,
    DietaryProfileComponent,
    MarkLeaveComponent,
    MenuOptionsComponent,
    OrderConfirmationComponent,
    NoItemAlertComponent,
    SelectConfirmationComponent,
    ServeConfirmationComponent,
    ActivityAddPhotosComponent,
    MarkLeaveSingledayComponent,
    MenuItemDetailsComponent,
    DiningAreaComponent,
    ServingRefusedComponent,
    DiningAddItemsComponent,
    DiningIrFiDetailsComponent,
    ContinentalItemComponent,
    ContinentalItemDetailsComponent,
    ContinentalItemTabviewComponent,
    ItemOptionsComponent,
    AssignSeatComponent,
    RemoveSeatComponent,
    UnserveComponent,
    ViewMenuAdditionalComponent,
    OrderSuccessMessageComponent,
    AdditionalItemDetailsComponent,
    ServingConsumedComponent,
    ServingChangeCountComponent,
    MarkAsServedComponent,
    MarkAsServedConfirmComponent,
    ConsumptionNoteAlertComponent,
    OverviewActivityComponent,
    OverviewDiningComponent,
    OverviewFoodoptionsComponent,
    ShowVisitorsComponent,
    ShowpdfComponent,
    NotificationComponent,
    ResidentComponent,
    UndoRefuseComponent,
    RvSelectStaffComponent,
    RvTaskOptionsComponent,
    RvTaskFilterComponent,
    RvEditTaskComponent,
    RvDocsOptionsComponent,
    RvAssignedStaffComponent,
    RvAssignedResidentComponent,
    SendMailWarningComponent,
    ActivityWarningComponent
  ],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ApplicationPipesModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    InAppBrowser,
    ScreenOrientation,
    StreamingMedia,
    CallNumber,
    File,
    Camera,
    Base64,
    Network,
    AppVersion,
    Market,
    // ImagePicker,
    WebView,
    VideoEditor,
    HTTP,
    // FCM,
    MediaCapture,
    Chooser,
    Clipboard,
    SpinnerDialog,
    Crop,
    FilePath,
    // FirebaseX,
    // Deeplinks,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
