import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'locationsearch'
})
export class LocationsearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    //   console.log("pipe:",items,terms);
    if(!items) return [];
    if(!terms) return items;
    
    return items.filter(v => this.filterFn(v, terms));
    
  }
  filterFn(value: any, searchValue) {
    let flag = this.testSearchCondition(value.data.name, searchValue);
    
    if (!flag && value.data.sub_locations) {
      
      flag = value.data.sub_locations.filter(v => this.testSearchCondition(v.name, searchValue)).length > 0;
      
    }
    
    return flag;
  }
  testSearchCondition(name: string, searchValue: string): boolean {
    
    return name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
  }

}
