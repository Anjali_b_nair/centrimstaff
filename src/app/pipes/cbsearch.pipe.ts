import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cbsearch'
})
export class CbsearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      return it.consumer.resident.user.name.toLowerCase().includes(terms); // only filter  name
    });
  }

}
