import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'diningTableResidents'
})
export class DiningTableResidentsPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      let result;
      if(it.user.name.toLowerCase().includes(terms)){
        result=it.user.name.toLowerCase().includes(terms)
      // }else if(it.res.room.toLowerCase().includes(terms)){
      //   result=it.res.room.toLowerCase().includes(terms)
      // }else if(it.res.wing&&it.res.wing.name.toLowerCase().includes(terms)){
      //   result=it.res.wing.name.toLowerCase().includes(terms)
      }
      
      // return it.user.name.toLowerCase().includes(terms)  ; // only filter  
      return result;
    });
  }

}
