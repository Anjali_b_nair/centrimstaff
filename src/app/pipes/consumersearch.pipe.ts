import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'consumersearch'
})
export class ConsumersearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    
    return items.filter( it => {
      let result;
     
      if(it.user.name.toLowerCase().includes(terms)){
        result=it.user.name.toLowerCase().includes(terms)
      }else if(it.room&&it.room.toLowerCase().includes(terms)){
        result=it.room.toLowerCase().includes(terms)
      }else if(it.wing&&it.wing.name.toLowerCase().includes(terms)){
        result=it.wing.name.toLowerCase().includes(terms)
      }
      
      // return it.user.name.toLowerCase().includes(terms)  ; // only filter  
      return result;
    });
  }

}
// it.con.user.name.toLowerCase().includes(terms)