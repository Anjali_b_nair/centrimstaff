import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'consumersearch2'
})
export class Consumersearch2Pipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      let result;
      console.log("term:",terms," it:",it.con);
      if(it.con.user.name.toLowerCase().includes(terms)){
      result=it.con.user.name.toLowerCase().includes(terms)  ; // only filter  name
      }else if(it.con.property_details&&it.con.property_details.contract_details&&
        it.con.property_details.contract_details.property&&
        it.con.property_details.contract_details.property.location&&
        it.con.property_details.contract_details.property.location.name.toLowerCase().includes(terms)
        ){
          result=it.con.property_details.contract_details.property.location.name.toLowerCase().includes(terms)
        }
        return result;
    });
  }
}
