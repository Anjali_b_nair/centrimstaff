import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'visitorsearch'
})
export class VisitorsearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      return it.resident_details.name.toLowerCase().includes(terms)  ; // only filter  name
    });
  }

}
