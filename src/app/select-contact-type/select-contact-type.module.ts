import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectContactTypePageRoutingModule } from './select-contact-type-routing.module';

import { SelectContactTypePage } from './select-contact-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectContactTypePageRoutingModule
  ],
  declarations: [SelectContactTypePage]
})
export class SelectContactTypePageModule {}
