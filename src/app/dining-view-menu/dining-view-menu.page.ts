import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment-timezone';
@Component({
  selector: 'app-dining-view-menu',
  templateUrl: './dining-view-menu.page.html',
  styleUrls: ['./dining-view-menu.page.scss'],
})
export class DiningViewMenuPage implements OnInit {
subscription:Subscription;
currentMenu:any[]=[];
upcoming:any[]=[];
  constructor(private router:Router,private platform:Platform,private http:HttpClient,private storage:Storage,
    private config:HttpConfigService,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.currentMenu=[];
    this.upcoming=[];
this.showLoading();
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const tz=await this.storage.get('TIMEZONE');
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'get_menu_list';
    let body={
      date:moment().tz(tz).format('YYYY-MM-DD')
    }
    console.log('tz:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      // this.currentMenu=res.data.current_menu;
      // this.upcoming=res.data.other_menu;
      let cm=res.data.current_menu;
      if(cm&&cm.length){
        cm.forEach(ele=>{
          if(ele.status==2){
            this.currentMenu.push(ele)
          }
        })
      }
      
      let um=res.data.other_menu;
      if(um&&um.length){
        um.forEach(ele=>{
          if(ele.status==2){
            this.upcoming.push(ele)
          }
        })
      }
      this.dismissLoader();
    },error=>{
      this.dismissLoader();
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   
     back(){
       this.router.navigate(['/dining-dashboard'])
     }

     viewMenu(item){
       console.log('item:',item);
      this.router.navigate(['/dining-vm-select-date',{menu:JSON.stringify(item)}],{replaceUrl:true})
     }
  
     async showLoading() {
     
      const loading = await this.loadingCtrl.create({
        cssClass: 'dining-loading',
        // message: 'Please wait...',
        // message: '<ion-img src="assets/loader.gif"></ion-img>',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
     
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
}
