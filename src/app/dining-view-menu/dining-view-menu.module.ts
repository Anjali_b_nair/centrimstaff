import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningViewMenuPageRoutingModule } from './dining-view-menu-routing.module';

import { DiningViewMenuPage } from './dining-view-menu.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningViewMenuPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningViewMenuPage]
})
export class DiningViewMenuPageModule {}
