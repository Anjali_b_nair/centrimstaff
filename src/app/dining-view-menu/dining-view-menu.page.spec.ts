import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningViewMenuPage } from './dining-view-menu.page';

describe('DiningViewMenuPage', () => {
  let component: DiningViewMenuPage;
  let fixture: ComponentFixture<DiningViewMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningViewMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningViewMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
