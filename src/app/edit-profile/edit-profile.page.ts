import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Crop, CropOptions } from '@ionic-native/crop/ngx';

import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActionSheetController, AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Filesystem } from '@capacitor/filesystem';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  name:string;
  p_name:string;
  img:any;
  email:any;
  croppedImagepath = "";
  isLoading = false;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };
  cropOptions: CropOptions = {
    quality: 50
  }
  subscription:Subscription;
  flag:any;
  image:any;
  constructor(private actionsheetCntlr:ActionSheetController,private sanitizer:DomSanitizer,
    public storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform,private route:ActivatedRoute,private webView:WebView,private crop:Crop,
    private alert:AlertController,private http:HttpClient,private toast: ToastController, 
    private loadingCtrl: LoadingController){}

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.flag=this.route.snapshot.paramMap.get('flag');
    
      const data=await this.storage.get("NAME")
        this.name=data;
        this.p_name=this.name.substr(0,1);
      
      const pic=await this.storage.get("PRO_IMG")
        this.img=pic;
        console.log(this.img)
      
      const mail=await this.storage.get("EMAIL")
        this.email=mail;
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      // this.router.navigate(['/relative-profile']) ;
      this.back();
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
  async captureImage() {
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: sourceType,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
     
    //   this.img=imageData;
      

    //   // this.crop.crop(imageData, this.cropOptions)
    //   // .then(
    //   //   newPath => {
    //   //     let pic=newPath.split('?')[0]
    //   //     let img=this.webView.convertFileSrc(pic);
    //   //     if(this.platform.is('ios')){
    //   //       this.img=this.sanitizer.bypassSecurityTrustUrl(img)
    //   //     }else{
    //   //       this.img=img
    //   //     }
    //   //     this.image=pic
    //   //     this.storage.set("PRO_IMG",this.img);
        
    //   //     console.log("PRO_u",pic)
    //   //   console.log("PRO_IMG",this.img)
    //   this.uploadImage('data:image/jpeg;base64,' + imageData);
    // //     this.ionViewWillEnter();
    // //   },
    // //   error => {
    // //     // alert('Error cropping image' + error);
    // //   }
    // // );
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
    this.img=imageUrl;
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    
    this.uploadImage(base64Image);
  }
  async pickImage(){
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:1
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
             this.img=contents.data
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  }
// select gallery or camera
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.captureImage();
        }
      },
      {
        text: 'Remove photo',
        handler: () => {
          this.removePhoto();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }


  // image upload method
  async upload(){
    
      
        const data=await this.storage.get("USER_ID");
        let headers=await this.config.getHeader();
          
             
              let url=this.config.domain_url+'upload_profilepic';
              let body={
                user_id:data,
                profile_pic:this.image
              }
              // fileTransfer.upload(this.image,url,options).then((data)=>{
              //   console.log("success");
              //   console.log(data);
                
                
              // },error=>{
              //   console.log(error);
                
              // })
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                this.dismissLoader();
                this.storage.set("PRO_IMG",this.img);
              },error=>{
                this.dismissLoader();
              })
          
      
  }

  back(){
    if(this.flag==1){
      this.router.navigateByUrl('/userprofile');
      }else{
        this.router.navigate(['/menu']);
      }
  }
  // skip(){
  //   this.router.navigate(['/menu']);
  // }


  async removePhoto(){
    const alert = await this.alert.create({
      header:'Confirm?',
      message: 'Remove profile photo?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Remove',
          handler: () => {
           this.image='https://app.centrim.life/assets/img/user/user.png';
           this.img='https://app.centrim.life/assets/img/user/user.png';
           this.storage.set("PRO_IMG",'https://app.centrim.life/assets/img/user/user.png');
           this.upload();
          }
        }
      ]
    });
  
    await alert.present();
    }
    async uploadImage(img) {
      // if(this.isLoading==false){
      this.showLoading();
      // }
      let url = this.config.domain_url + 'upload_file';
      let headers=await this.config.getHeader();;
      let body = { file: img }
      console.log("body:", img);
  
      this.http.post(url, body,{headers}).subscribe((res: any) => {
        console.log("uploaded:", res);
       
          this.image = res.data;
          this.img=res.data;
         this.upload();
        
      }, error => {
        // this.imageResponse.slice(0,1);
        console.log(error);
        this.dismissLoader();
        this.presentToast('Upload failed.Please try again.')
  
      })
    }
    async showLoading() {
  
      const loading = await this.loadingCtrl.create({
        cssClass: 'custom-loading',
        message: 'Please wait...',
        spinner: null,
        id: 'loader'
        // duration: 3000
      });
      return await loading.present();
    }
  
    async dismissLoader() {
  
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
    async presentToast(mes) {
      const alert = await this.toast.create({
        message: mes,
        duration: 3000,
        cssClass: 'toast-mess',
        position: 'top'
      });
      alert.present();
    }
}
