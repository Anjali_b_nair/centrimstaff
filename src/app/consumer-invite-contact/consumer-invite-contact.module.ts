import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerInviteContactPageRoutingModule } from './consumer-invite-contact-routing.module';

import { ConsumerInviteContactPage } from './consumer-invite-contact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerInviteContactPageRoutingModule
  ],
  declarations: [ConsumerInviteContactPage]
})
export class ConsumerInviteContactPageModule {}
