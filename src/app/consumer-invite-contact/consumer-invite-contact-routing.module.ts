import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerInviteContactPage } from './consumer-invite-contact.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumerInviteContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerInviteContactPageRoutingModule {}
