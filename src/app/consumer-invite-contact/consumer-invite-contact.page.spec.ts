import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsumerInviteContactPage } from './consumer-invite-contact.page';

describe('ConsumerInviteContactPage', () => {
  let component: ConsumerInviteContactPage;
  let fixture: ComponentFixture<ConsumerInviteContactPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumerInviteContactPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsumerInviteContactPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
