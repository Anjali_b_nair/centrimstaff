import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-invite-contact',
  templateUrl: './consumer-invite-contact.page.html',
  styleUrls: ['./consumer-invite-contact.page.scss'],
})
export class ConsumerInviteContactPage implements OnInit {
pic:any;
name:any;
room:any;
id:any;

con_name:any;
relation:any;
email:any;
phone:any;
is_primary:any=0;
subscription:Subscription;
user_id:any;
home:any;
work:any;
flag:any;
wing:any;
disable:boolean=false;
  constructor(private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,
    private toastCntlr:ToastController,private router:Router,private platform:Platform,private storage:Storage) { }

  ngOnInit() {
  }
ionViewWillEnter(){
  this.disable=false;
  this.id=this.route.snapshot.paramMap.get('id');
  this.user_id=this.route.snapshot.paramMap.get('user_id');
  this.pic=this.route.snapshot.paramMap.get('pic');
  this.name=this.route.snapshot.paramMap.get('name');
  this.room=this.route.snapshot.paramMap.get('room');
  this.wing=this.route.snapshot.paramMap.get('wing');
  this.flag=this.route.snapshot.paramMap.get('flag');
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.router.navigate(['/resident-profile',{id:this.id,flag:this.flag}]) ; 
}); 

  }
  ionViewWillLeave() { 
    this.clearData();
    this.subscription.unsubscribe();
 }
primaryCheck(ev){
 if(ev.currentTarget.checked){
   this.is_primary=1
 }else{
   this.is_primary=0;
 }
}

async send(){
 
 
    const bid=await this.storage.get("BRANCH")


    
// var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
//                 '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
//                 '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
//                 '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
//                 '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
//                 '(\\#[-a-z\\d_]*)?$','i');

var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    console.log(this.name,this.email,this.relation,this.phone)
    if(this.phone==undefined || this.phone==''){
      this.phone=null;
    }
    if(this.home==undefined || this.home==''){
      this.home=null;
    }
    if(this.work==undefined || this.work==''){
      this.work=null;
    }
    if(this.con_name==undefined||this.email==undefined||this.relation==undefined){
      this.presentAlert('Please fill all mandatory fields.');
      
    }else if(this.con_name==''||this.email==''||this.relation==''){
      this.presentAlert('Please fill all mandatory fields.');
      
    }else if(this.phone!=null &&(this.phone.toString().length<10|| this.phone.toString().length>10)){
      this.presentAlert('Please enter a 10 digit mobile number.');
      
    // }else if(this.is_primary==1&&this.phone==null){
    //   this.presentAlert('Please enter your mobile number.');
    }else if(/^\d+$/.test(this.con_name)){
      this.presentAlert('Name field contains only digits.');
    }else if(/^\d+$/.test(this.relation)){
      this.presentAlert('Please enter a proper relation.');
    }else if(this.home!=null && (this.home.toString().length<10 || this.home.toString().length>10)){
            this.presentAlert('Please enter a 10 digit home phone number.');
         
    }else if( this.work!=null && (this.work.toString().length<10 || this.work.toString().length>10)){
            this.presentAlert('Please enter a 10 digit work phone number.');
     

    }else{

      if(pattern.test(this.email)){
        this.disable=true;
       
          const data=await this.storage.get("USER_ID");
          let headers=await this.config.getHeader();;
            console.log("id:",this.id);
            
            let url=this.config.domain_url+'validate_invite_contact';
           
            let body={
              resident_user_id:this.user_id,
              
              email:this.email,
              
            }
            console.log("bodyval:",body)
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log("res:",res);
                if(res.status=='success'){
               this.invite();
                }else{
                  this.presentAlert('Already registered email Id.')
                  this.disable=false
                }
            },error=>{
              
              
            
              this.presentAlert('Already registered email Id.');
              this.disable=false
              console.log("err:",error);
            })
            
            
          
          
          
          
       
    }else{
      this.presentAlert('Please enter a valid email Id.');
      this.disable=false
    }
      }
   
    }


    handleError(error: HttpErrorResponse) {
      console.log("handle:",error);
      
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    };


    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }

  async inviteAlert() {
    const alert = await this.toastCntlr.create({
      message: 'Invitation send successfully',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
clearData(){
  this.con_name='';
  this.relation='';
  this.email='';
  this.phone='';
  this.home='';
  this.work='';
  this.is_primary=0;
}

async invite(){
  let url=this.config.domain_url+'invite_contact';
  let headers=await this.config.getHeader();;
          let body={
            resident_user_id:this.user_id,
            name:this.con_name,
            relation:this.relation,
            email:this.email,
            phone:this.phone,
            is_primary:this.is_primary,
            homephone:this.home,
            workphone:this.work
          }
          console.log('body:',body)
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.inviteAlert();
              this.clearData();
              this.router.navigate(['/resident-profile',{id:this.id,flag:this.flag}],{replaceUrl:true})
              this.disable=false;
            // this.popoverController.dismiss();
          }),error=>{
            this.disable=false;
            console.log(error);
          }
}
}
