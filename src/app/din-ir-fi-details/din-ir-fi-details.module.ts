import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinIrFiDetailsPageRoutingModule } from './din-ir-fi-details-routing.module';

import { DinIrFiDetailsPage } from './din-ir-fi-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinIrFiDetailsPageRoutingModule
  ],
  declarations: [DinIrFiDetailsPage]
})
export class DinIrFiDetailsPageModule {}
