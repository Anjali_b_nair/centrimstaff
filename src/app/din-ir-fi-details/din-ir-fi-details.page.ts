import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-din-ir-fi-details',
  templateUrl: './din-ir-fi-details.page.html',
  styleUrls: ['./din-ir-fi-details.page.scss'],
})
export class DinIrFiDetailsPage implements OnInit {

  subscription:Subscription;
  flag:any;
    constructor(private router:Router,private platform:Platform,private route:ActivatedRoute) { }
  
    ngOnInit() {
    }
  ionViewWillEnter(){
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
        back(){
          this.router.navigate(['/dining-add-items',{flag:this.flag}])
        }

}
