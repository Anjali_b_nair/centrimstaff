import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinIrFiDetailsPage } from './din-ir-fi-details.page';

const routes: Routes = [
  {
    path: '',
    component: DinIrFiDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinIrFiDetailsPageRoutingModule {}
