import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseDtMltConsumedPage } from './din-choose-dt-mlt-consumed.page';

describe('DinChooseDtMltConsumedPage', () => {
  let component: DinChooseDtMltConsumedPage;
  let fixture: ComponentFixture<DinChooseDtMltConsumedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseDtMltConsumedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseDtMltConsumedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
