import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseDtMltConsumedPageRoutingModule } from './din-choose-dt-mlt-consumed-routing.module';

import { DinChooseDtMltConsumedPage } from './din-choose-dt-mlt-consumed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseDtMltConsumedPageRoutingModule
  ],
  declarations: [DinChooseDtMltConsumedPage]
})
export class DinChooseDtMltConsumedPageModule {}
