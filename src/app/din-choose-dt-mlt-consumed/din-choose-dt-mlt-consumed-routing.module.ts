import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseDtMltConsumedPage } from './din-choose-dt-mlt-consumed.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseDtMltConsumedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseDtMltConsumedPageRoutingModule {}
