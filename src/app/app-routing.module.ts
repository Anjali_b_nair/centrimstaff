import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  // {
  //   path: '',
  //   redirectTo: 'login',
  //   pathMatch: 'full'
  // },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'stories',
    loadChildren: () => import('./stories/stories.module').then( m => m.StoriesPageModule)
  },
  {
    path: 'feeds',
    loadChildren: () => import('./feeds/feeds.module').then( m => m.FeedsPageModule)
  },
  {
    path: 'story-details',
    loadChildren: () => import('./story-details/story-details.module').then( m => m.StoryDetailsPageModule)
  },
  {
    path: 'create-story',
    loadChildren: () => import('./create-story/create-story.module').then( m => m.CreateStoryPageModule)
  },
  
  {
    path: 'select-consumers',
    loadChildren: () => import('./select-consumers/select-consumers.module').then( m => m.SelectConsumersPageModule)
  },
  {
    path: 'activities',
    loadChildren: () => import('./activities/activities.module').then( m => m.ActivitiesPageModule)
  },
  {
    path: 'activity-details',
    loadChildren: () => import('./activity-details/activity-details.module').then( m => m.ActivityDetailsPageModule)
  },
  {
    path: 'mark-attendance',
    loadChildren: () => import('./mark-attendance/mark-attendance.module').then( m => m.MarkAttendancePageModule)
  },
  {
    path: 'add-attendees',
    loadChildren: () => import('./add-attendees/add-attendees.module').then( m => m.AddAttendeesPageModule)
  },
  {
    path: 'callbooking-list',
    loadChildren: () => import('./callbooking-list/callbooking-list.module').then( m => m.CallbookingListPageModule)
  },
  {
    path: 'cb-choose-consumer',
    loadChildren: () => import('./cb-choose-consumer/cb-choose-consumer.module').then( m => m.CbChooseConsumerPageModule)
  },
  {
    path: 'cb-pick-date-time',
    loadChildren: () => import('./cb-pick-date-time/cb-pick-date-time.module').then( m => m.CbPickDateTimePageModule)
  },
  {
    path: 'confirm-callbooking',
    loadChildren: () => import('./confirm-callbooking/confirm-callbooking.module').then( m => m.ConfirmCallbookingPageModule)
  },
  {
    path: 'callbooking-success',
    loadChildren: () => import('./callbooking-success/callbooking-success.module').then( m => m.CallbookingSuccessPageModule)
  },
  {
    path: 'create-activity',
    loadChildren: () => import('./create-activity/create-activity.module').then( m => m.CreateActivityPageModule)
  },
  {
    path: 'consumers',
    loadChildren: () => import('./consumers/consumers.module').then( m => m.ConsumersPageModule)
  },
  {
    path: 'consumer-profile',
    loadChildren: () => import('./consumer-profile/consumer-profile.module').then( m => m.ConsumerProfilePageModule)
  },
  {
    path: 'consumer-contacts',
    loadChildren: () => import('./consumer-contacts/consumer-contacts.module').then( m => m.ConsumerContactsPageModule)
  },
  {
    path: 'consumer-invite-contact',
    loadChildren: () => import('./consumer-invite-contact/consumer-invite-contact.module').then( m => m.ConsumerInviteContactPageModule)
  },
  {
    path: 'petty-cash',
    loadChildren: () => import('./petty-cash/petty-cash.module').then( m => m.PettyCashPageModule)
  },
  {
    path: 'add-petty-cash',
    loadChildren: () => import('./add-petty-cash/add-petty-cash.module').then( m => m.AddPettyCashPageModule)
  },
  {
    path: 'consumer-about',
    loadChildren: () => import('./consumer-about/consumer-about.module').then( m => m.ConsumerAboutPageModule)
  },
  {
    path: 'consumer-report',
    loadChildren: () => import('./consumer-report/consumer-report.module').then( m => m.ConsumerReportPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'message',
    loadChildren: () => import('./message/message.module').then( m => m.MessagePageModule)
  },
  {
    path: 'startchat',
    loadChildren: () => import('./startchat/startchat.module').then( m => m.StartchatPageModule)
  },
  {
    path: 'chatbox',
    loadChildren: () => import('./chatbox/chatbox.module').then( m => m.ChatboxPageModule)
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  {
    path: 'resetpassword',
    loadChildren: () => import('./resetpassword/resetpassword.module').then( m => m.ResetpasswordPageModule)
  },
  {
    path: 'userprofile',
    loadChildren: () => import('./userprofile/userprofile.module').then( m => m.UserprofilePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'edit-petty-cash',
    loadChildren: () => import('./edit-petty-cash/edit-petty-cash.module').then( m => m.EditPettyCashPageModule)
  },
  {
    path: 'create-consumer',
    loadChildren: () => import('./create-consumer/create-consumer.module').then( m => m.CreateConsumerPageModule)
  },
  {
    path: 'create-group',
    loadChildren: () => import('./create-group/create-group.module').then( m => m.CreateGroupPageModule)
  },
  {
    path: 'view-group',
    loadChildren: () => import('./view-group/view-group.module').then( m => m.ViewGroupPageModule)
  },
  {
    path: 'group-members',
    loadChildren: () => import('./group-members/group-members.module').then( m => m.GroupMembersPageModule)
  },
  {
    path: 'edit-story',
    loadChildren: () => import('./edit-story/edit-story.module').then( m => m.EditStoryPageModule)
  },
  {
    path: 'edit-activity',
    loadChildren: () => import('./edit-activity/edit-activity.module').then( m => m.EditActivityPageModule)
  },
  {
    path: 'edit-group',
    loadChildren: () => import('./edit-group/edit-group.module').then( m => m.EditGroupPageModule)
  },
  {
    path: 'new-status',
    loadChildren: () => import('./new-status/new-status.module').then( m => m.NewStatusPageModule)
  },
  {
    path: 'consumer-gallery',
    loadChildren: () => import('./consumer-gallery/consumer-gallery.module').then( m => m.ConsumerGalleryPageModule)
  },
  {
    path: 'visitors-list',
    loadChildren: () => import('./visitors-list/visitors-list.module').then( m => m.VisitorsListPageModule)
  },
  {
    path: 'add-activity',
    loadChildren: () => import('./add-activity/add-activity.module').then( m => m.AddActivityPageModule)
  },
  {
    path: 'consumer-overview',
    loadChildren: () => import('./consumer-overview/consumer-overview.module').then( m => m.ConsumerOverviewPageModule)
  },
  {
    path: 'birthdays',
    loadChildren: () => import('./birthdays/birthdays.module').then( m => m.BirthdaysPageModule)
  },
  {
    path: 'visitor-booking-list',
    loadChildren: () => import('./visitor-booking-list/visitor-booking-list.module').then( m => m.VisitorBookingListPageModule)
  },
  {
    path: 'create-maintenance',
    loadChildren: () => import('./create-maintenance/create-maintenance.module').then( m => m.CreateMaintenancePageModule)
  },
  {
    path: 'maintenance',
    loadChildren: () => import('./maintenance/maintenance.module').then( m => m.MaintenancePageModule)
  },
  {
    path: 'maintenance-comment',
    loadChildren: () => import('./maintenance-comment/maintenance-comment.module').then( m => m.MaintenanceCommentPageModule)
  },
  {
    path: 'maintenance-details',
    loadChildren: () => import('./maintenance-details/maintenance-details.module').then( m => m.MaintenanceDetailsPageModule)
  },
  {
    path: 'maintenance-count',
    loadChildren: () => import('./maintenance-count/maintenance-count.module').then( m => m.MaintenanceCountPageModule)
  },
  {
    path: 'select-contact-type',
    loadChildren: () => import('./select-contact-type/select-contact-type.module').then( m => m.SelectContactTypePageModule)
  },
  {
    path: 'cb-disabletimeslot',
    loadChildren: () => import('./cb-disabletimeslot/cb-disabletimeslot.module').then( m => m.CbDisabletimeslotPageModule)
  },
  {
    path: 'feedback',
    loadChildren: () => import('./feedback/feedback.module').then( m => m.FeedbackPageModule)
  },
  {
    path: 'resources-list',
    loadChildren: () => import('./resources-list/resources-list.module').then( m => m.ResourcesListPageModule)
  },
  {
    path: 'resources-details',
    loadChildren: () => import('./resources-details/resources-details.module').then( m => m.ResourcesDetailsPageModule)
  },
  {
    path: 'activity-feedback',
    loadChildren: () => import('./activity-feedback/activity-feedback.module').then( m => m.ActivityFeedbackPageModule)
  },
  {
    path: 'feedback-dashboard',
    loadChildren: () => import('./feedback-dashboard/feedback-dashboard.module').then( m => m.FeedbackDashboardPageModule)
  },
  {
    path: 'subfloderfiles',
    loadChildren: () => import('./subfloderfiles/subfloderfiles.module').then( m => m.SubfloderfilesPageModule)
  },
  {
    path: 'menu-booking',
    loadChildren: () => import('./menu-booking/menu-booking.module').then( m => m.MenuBookingPageModule)
  },
  {
    path: 'feedback-list',
    loadChildren: () => import('./feedback-list/feedback-list.module').then( m => m.FeedbackListPageModule)
  },
  {
    path: 'din-choose-consumer',
    loadChildren: () => import('./din-choose-consumer/din-choose-consumer.module').then( m => m.DinChooseConsumerPageModule)
  },
  {
    path: 'din-choose-dining-area',
    loadChildren: () => import('./din-choose-dining-area/din-choose-dining-area.module').then( m => m.DinChooseDiningAreaPageModule)
  },
  {
    path: 'din-choose-dt-mealtime',
    loadChildren: () => import('./din-choose-dt-mealtime/din-choose-dt-mealtime.module').then( m => m.DinChooseDtMealtimePageModule)
  },
  {
    path: 'din-choose-dt-mlt-da',
    loadChildren: () => import('./din-choose-dt-mlt-da/din-choose-dt-mlt-da.module').then( m => m.DinChooseDtMltDaPageModule)
  },
  {
    path: 'din-dietary-preference',
    loadChildren: () => import('./din-dietary-preference/din-dietary-preference.module').then( m => m.DinDietaryPreferencePageModule)
  },
  {
    path: 'din-dining-area',
    loadChildren: () => import('./din-dining-area/din-dining-area.module').then( m => m.DinDiningAreaPageModule)
  },
  {
    path: 'din-dining-table',
    loadChildren: () => import('./din-dining-table/din-dining-table.module').then( m => m.DinDiningTablePageModule)
  },
  {
    path: 'dining-add-items',
    loadChildren: () => import('./dining-add-items/dining-add-items.module').then( m => m.DiningAddItemsPageModule)
  },
  {
    path: 'dining-choose-season-date',
    loadChildren: () => import('./dining-choose-season-date/dining-choose-season-date.module').then( m => m.DiningChooseSeasonDatePageModule)
  },
  {
    path: 'dining-dashboard',
    loadChildren: () => import('./dining-dashboard/dining-dashboard.module').then( m => m.DiningDashboardPageModule)
  },
  {
    path: 'dining-fi-details',
    loadChildren: () => import('./dining-fi-details/dining-fi-details.module').then( m => m.DiningFiDetailsPageModule)
  },
  {
    path: 'dining-ind-res-order',
    loadChildren: () => import('./dining-ind-res-order/dining-ind-res-order.module').then( m => m.DiningIndResOrderPageModule)
  },
  {
    path: 'dining-menu-fi-details',
    loadChildren: () => import('./dining-menu-fi-details/dining-menu-fi-details.module').then( m => m.DiningMenuFiDetailsPageModule)
  },
  {
    path: 'dining-menu-listing',
    loadChildren: () => import('./dining-menu-listing/dining-menu-listing.module').then( m => m.DiningMenuListingPageModule)
  },
  {
    path: 'dining-select-choice',
    loadChildren: () => import('./dining-select-choice/dining-select-choice.module').then( m => m.DiningSelectChoicePageModule)
  },
  {
    path: 'dining-table',
    loadChildren: () => import('./dining-table/dining-table.module').then( m => m.DiningTablePageModule)
  },
  {
    path: 'dining-take-order',
    loadChildren: () => import('./dining-take-order/dining-take-order.module').then( m => m.DiningTakeOrderPageModule)
  },
  {
    path: 'dining-view-menu',
    loadChildren: () => import('./dining-view-menu/dining-view-menu.module').then( m => m.DiningViewMenuPageModule)
  },
  {
    path: 'dining-vm-select-date',
    loadChildren: () => import('./dining-vm-select-date/dining-vm-select-date.module').then( m => m.DiningVmSelectDatePageModule)
  },
  {
    path: 'din-inroom-dining',
    loadChildren: () => import('./din-inroom-dining/din-inroom-dining.module').then( m => m.DinInroomDiningPageModule)
  },
  {
    path: 'din-ir-fi-details',
    loadChildren: () => import('./din-ir-fi-details/din-ir-fi-details.module').then( m => m.DinIrFiDetailsPageModule)
  },
  {
    path: 'din-continental-breakfast',
    loadChildren: () => import('./din-continental-breakfast/din-continental-breakfast.module').then( m => m.DinContinentalBreakfastPageModule)
  },
  {
    path: 'din-continental-fi-details',
    loadChildren: () => import('./din-continental-fi-details/din-continental-fi-details.module').then( m => m.DinContinentalFiDetailsPageModule)
  },
  {
    path: 'din-choose-dt-mlt-consumed',
    loadChildren: () => import('./din-choose-dt-mlt-consumed/din-choose-dt-mlt-consumed.module').then( m => m.DinChooseDtMltConsumedPageModule)
  },
  {
    path: 'dining-consumed',
    loadChildren: () => import('./dining-consumed/dining-consumed.module').then( m => m.DiningConsumedPageModule)
  },
  {
    path: 'resident-overview',
    loadChildren: () => import('./resident-overview/resident-overview.module').then( m => m.ResidentOverviewPageModule)
  },
  {
    path: 'resident-profile',
    loadChildren: () => import('./resident-profile/resident-profile.module').then( m => m.ResidentProfilePageModule)
  },
  {
    path: 'rv-create-task',
    loadChildren: () => import('./rv-create-task/rv-create-task.module').then( m => m.RvCreateTaskPageModule)
  },
  {
    path: 'rv-res-add-note',
    loadChildren: () => import('./rv-res-add-note/rv-res-add-note.module').then( m => m.RvResAddNotePageModule)
  },
  {
    path: 'rv-res-documents',
    loadChildren: () => import('./rv-res-documents/rv-res-documents.module').then( m => m.RvResDocumentsPageModule)
  },
  {
    path: 'rv-res-list',
    loadChildren: () => import('./rv-res-list/rv-res-list.module').then( m => m.RvResListPageModule)
  },
  {
    path: 'rv-res-profile',
    loadChildren: () => import('./rv-res-profile/rv-res-profile.module').then( m => m.RvResProfilePageModule)
  },
  {
    path: 'rv-res-task-list',
    loadChildren: () => import('./rv-res-task-list/rv-res-task-list.module').then( m => m.RvResTaskListPageModule)
  },
  {
    path: 'rv-resident-notes',
    loadChildren: () => import('./rv-resident-notes/rv-resident-notes.module').then( m => m.RvResidentNotesPageModule)
  },
  {
    path: 'rv-resident-profile-landing',
    loadChildren: () => import('./rv-resident-profile-landing/rv-resident-profile-landing.module').then( m => m.RvResidentProfileLandingPageModule)
  },
  {
    path: 'rv-service-activities',
    loadChildren: () => import('./rv-service-activities/rv-service-activities.module').then( m => m.RvServiceActivitiesPageModule)
  },
  {
    path: 'rv-service-feedback',
    loadChildren: () => import('./rv-service-feedback/rv-service-feedback.module').then( m => m.RvServiceFeedbackPageModule)
  },
  {
    path: 'rv-service-maintenance',
    loadChildren: () => import('./rv-service-maintenance/rv-service-maintenance.module').then( m => m.RvServiceMaintenancePageModule)
  },
  {
    path: 'rv-service-tasks',
    loadChildren: () => import('./rv-service-tasks/rv-service-tasks.module').then( m => m.RvServiceTasksPageModule)
  },
  {
    path: 'rv-services-home',
    loadChildren: () => import('./rv-services-home/rv-services-home.module').then( m => m.RvServicesHomePageModule)
  },
  {
    path: 'rv-create-resident',
    loadChildren: () => import('./rv-create-resident/rv-create-resident.module').then( m => m.RvCreateResidentPageModule)
  },
  {
    path: 'rv-add-feedback',
    loadChildren: () => import('./rv-add-feedback/rv-add-feedback.module').then( m => m.RvAddFeedbackPageModule)
  },
  {
    path: 'rv-add-contact',
    loadChildren: () => import('./rv-add-contact/rv-add-contact.module').then( m => m.RvAddContactPageModule)
  },
  {
    path: 'rv-res-docs-inner',
    loadChildren: () => import('./rv-res-docs-inner/rv-res-docs-inner.module').then( m => m.RvResDocsInnerPageModule)
  },
  {
    path: 'onboarding-p1',
    loadChildren: () => import('./onboarding-p1/onboarding-p1.module').then( m => m.OnboardingP1PageModule)
  },
  {
    path: 'onboarding-p2',
    loadChildren: () => import('./onboarding-p2/onboarding-p2.module').then( m => m.OnboardingP2PageModule)
  },
  {
    path: 'onboarding-p3',
    loadChildren: () => import('./onboarding-p3/onboarding-p3.module').then( m => m.OnboardingP3PageModule)
  },
  {
    path: 'onboarding-p4',
    loadChildren: () => import('./onboarding-p4/onboarding-p4.module').then( m => m.OnboardingP4PageModule)
  },
  {
    path: 'onboarding-welcome',
    loadChildren: () => import('./onboarding-welcome/onboarding-welcome.module').then( m => m.OnboardingWelcomePageModule)
  },
  {
    path: 'rv-tasks',
    loadChildren: () => import('./rv-tasks/rv-tasks.module').then( m => m.RvTasksPageModule)
  },
  {
    path: 'rv-task-details',
    loadChildren: () => import('./rv-task-details/rv-task-details.module').then( m => m.RvTaskDetailsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
