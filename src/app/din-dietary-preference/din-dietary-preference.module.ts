import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinDietaryPreferencePageRoutingModule } from './din-dietary-preference-routing.module';

import { DinDietaryPreferencePage } from './din-dietary-preference.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinDietaryPreferencePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinDietaryPreferencePage]
})
export class DinDietaryPreferencePageModule {}
