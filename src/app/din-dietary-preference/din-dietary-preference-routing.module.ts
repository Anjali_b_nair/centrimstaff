import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinDietaryPreferencePage } from './din-dietary-preference.page';

const routes: Routes = [
  {
    path: '',
    component: DinDietaryPreferencePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinDietaryPreferencePageRoutingModule {}
