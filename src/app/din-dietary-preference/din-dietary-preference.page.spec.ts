import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinDietaryPreferencePage } from './din-dietary-preference.page';

describe('DinDietaryPreferencePage', () => {
  let component: DinDietaryPreferencePage;
  let fixture: ComponentFixture<DinDietaryPreferencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinDietaryPreferencePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinDietaryPreferencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
