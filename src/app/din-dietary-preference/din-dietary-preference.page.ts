import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { MarkLeaveComponent } from '../components/dining/mark-leave/mark-leave.component';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment-timezone';
import { ItemOptionsComponent } from '../components/dining/item-options/item-options.component';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
@Component({
  selector: 'app-din-dietary-preference',
  templateUrl: './din-dietary-preference.page.html',
  styleUrls: ['./din-dietary-preference.page.scss'],
})
export class DinDietaryPreferencePage implements OnInit {
subscription:Subscription;
consumer:any;
details:any;
selectedList:any='1';
meal_preference:any[]=[];
hide:boolean=false;
last_updated_at:any;
leaveList:any=[];
name:any;
selected_diets:any;
expand:boolean=true;
flag:any;
cid:any;
  constructor(private modalCntrl:ModalController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private router:Router,private route:ActivatedRoute,private platform:Platform,
    private alertCntlr:AlertController,private popCntl:PopoverController,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }

async ionViewWillEnter(){
  this.showLoading();
  this.hide=false;
  this.consumer=this.route.snapshot.paramMap.get('consumer');
  this.name=this.route.snapshot.paramMap.get('name');
  this.flag=this.route.snapshot.paramMap.get('flag');
  this.cid=this.route.snapshot.paramMap.get('cid');
  let url=this.config.domain_url+'get_dietry_profile';

  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  const tz=await this.storage.get('TIMEZONE');

  let headers=await this.config.getHeader();
  let body={
    user_id:this.consumer,
    company_id:cid
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.details=res.data.profile;
    if(res.data.selected_diets){
      this.selected_diets=res.data.selected_diets
    }
    if(this.details){
        this.last_updated_at=moment.utc(this.details.last_updated_at).tz(tz).format('DD MMMM YYYY hh:mm A');
    }
    this.meal_preference=res.data.meal_preferences;
    this.dismissLoader();
  },error=>{
    this.dismissLoader();
  })
  this.getLeaveDetails();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    
      back(){
        if(this.flag){
          this.router.navigate(['/resident-profile',{id:this.cid,flag:this.flag}],{replaceUrl:true}) ;
        }else{
        this.router.navigate(['/din-choose-consumer'])
        }
      }


      changeView(i){
        if(i==1){
          this.selectedList='1';
         
        }else{
          this.selectedList='2';
         
        }
      }

  async markLeave(){
    const modal = await this.modalCntrl.create({
      component: MarkLeaveComponent,
      cssClass:'din-markleave-modal',
      componentProps:{
        consumer:this.consumer
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
        this.getLeaveDetails();
      }
    });
    return await modal.present();
  }

  hideBanner(){
    this.hide=true
  }

  async getLeaveDetails(){
    let url=this.config.domain_url+'get_upcoming_dietary_leaves';

    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    
    let headers=await this.config.getHeader();
    let body={
      user_id:this.consumer,
      company_id:cid,
      limit:3
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('leavereq:',res);
      this.leaveList=res.data.list;
    })
  }

  async removeLeave(item){
    let url=this.config.domain_url+'delete_consumer_laeave';

    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    const uid=await this.storage.get('USER_ID');
    let headers=await this.config.getHeader();
    let body={
      leave_id:item.id
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('leavereq:',res);
      this.getLeaveDetails();
    })
  }

  async presentAlertConfirm(item) {
  
    const alert = await this.alertCntlr.create({
      header: 'Are you sure?',
      mode:'ios',
      backdropDismiss:true,
      message: 'You will not be able to recover this!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
           
            
           this.removeLeave(item);
  
          }
        }
      ]
    });
    
    await alert.present();
   
  }

  async showOptions(options,ev){
    
      const popover = await this.popCntl.create({
        component: ItemOptionsComponent,
        event:ev,
        backdropDismiss:true,
        componentProps:{
          data:options
        },
       
        
        
      });
      return await popover.present();
    }

    async showComment(comment,ev){
    
      const popover = await this.popCntl.create({
        component: CbCommentComponent,
        event:ev,
        backdropDismiss:true,
        componentProps:{
          data:comment
        },
       
        
        
      });
      return await popover.present();
    }

    async showLoading() {
      const loading = await this.loadingCtrl.create({
        cssClass: 'dining-loading',
        
        // message: 'Please wait...',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }
}
