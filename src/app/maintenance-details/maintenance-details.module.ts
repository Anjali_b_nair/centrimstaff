import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceDetailsPageRoutingModule } from './maintenance-details-routing.module';

import { MaintenanceDetailsPage } from './maintenance-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ResidentInfoComponent } from '../components/resident-info/resident-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [MaintenanceDetailsPage,ResidentInfoComponent],
  entryComponents:[ResidentInfoComponent]
})
export class MaintenanceDetailsPageModule {}
