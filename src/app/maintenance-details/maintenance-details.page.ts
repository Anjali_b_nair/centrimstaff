import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { MaintenanceImageComponent } from '../components/maintenance-image/maintenance-image.component';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';
import moment  from 'moment';
import { ResidentInfoComponent } from '../components/resident-info/resident-info.component';
@Component({
  selector: 'app-maintenance-details',
  templateUrl: './maintenance-details.page.html',
  styleUrls: ['./maintenance-details.page.scss'],
})
export class MaintenanceDetailsPage implements OnInit {
  details:any=[];
  comment:any=[];
  images:any=[];
  docs:any=[];
  technician:any=[];
  type:any=[];
  selected_type:any;
  selected_tec:any;
  risk:any;
  hazard:any;
  out_of_order:any;
  priority:any;
  request_id:any;
  filter:any;
  subscription:Subscription;
  pick:boolean=false;
  status:any;
  start:any=0;
  end:any=5;
  content:boolean=true;
  edit:boolean=false;

  preferred_status:boolean=false;
  entry_permission:boolean=false;
  hazard_status:boolean=false;
  out_of_order_status:boolean=false;
  priority_status:boolean=false;
  photo_status:boolean=false;
  risk_status:boolean=false;
  preferred_date:any;
  permission:any='0';
  resident_name:any;
  rv_branch:any;

  assigned_resident:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
    private storage:Storage,private router:Router,private platform:Platform,private popoverController:PopoverController,
    private toastCntlr:ToastController,private loadingCtrl:LoadingController,private iab:InAppBrowser,
    private dialog:SpinnerDialog,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  setContent(i){
    if(i==1){
      this.content=true;
    }else{
      this.content=false
    }
  }
  async ionViewWillEnter(){
    this.showLoading();
    this.details=[];
    this.comment=[];
    this.images=[];
    this.docs=[];
    this.technician=[];
    this.type=[];
    this.request_id=this.route.snapshot.paramMap.get('req_id');
    this.filter=this.route.snapshot.paramMap.get('filter');
    const rv=await this.storage.get('RVSETTINGS');
    let headers=await this.config.getHeader();;
  this.rv_branch=rv
  if(rv==1){
    this.getResident();
  }
  const type=await this.storage.get('USER_TYPE');
    let url=this.config.domain_url+'get_request_details/'+this.request_id+'/'+type;
    
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.edit')) {

        this.edit = true
      } else {

        this.edit = false;
        
      }
    })
    this.getPublicSettings();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.details=res.data;
      // this.images=res.images;
      this.comment=res.comment;
      this.technician=res.technician;
      this.type=res.type;
      if(res.images&&res.images.length>0){
        res.images.forEach(element => {
          if(element.image.includes('.jpg')||element.image.includes('.png')||element.image.includes('jpeg')){
            this.images.push(element)
          }else{
            this.docs.push(element)
          }
        });
      }
      this.details.forEach(element => {
        if(element.priority!=null){
        this.priority=element.priority.toString();
        }
        if(element.risk_rating!=null){
        this.risk=element.risk_rating.toString();
        }
        if(element.hazard!=null){
        this.hazard=element.hazard.toString();
        }
        if(element.out_of_order!=null){
        this.out_of_order=element.out_of_order.toString();
        }
        if(element.status!=null){
        this.status=element.status.toString();
        }
        if(element.type!=null){
        this.selected_type=element.type.id.toString();
        }
        if(element.assignee&&element.assignee.length){
          this.selected_tec=element.assignee[0].technician.name;
          if(element.assignee.length>1){
            element.assignee.forEach(ele => {
              if(this.selected_tec==ele.technician.name){

              }else{
                this.selected_tec=this.selected_tec+','+ele.technician.name
              }
            });
          }
        }
        if(element.preferred_datetime) {
          this.preferred_date=new Date(moment(element.preferred_datetime).format()).toISOString();
        } 
        
        if(element.permission_to_enter){
          this.permission=element.permission_to_enter.toString();
        }
        

        // if(element.technician!=null){
          // this.selected_tec=element.technician.user_id.toString();
        // }
        // if(element.status==0 && element.technician==null){
        //   this.pick=true;
        // }
        this.dismissLoader();
        console.log("p:",this.technician)
      });
    },error=>{
      this.dismissLoader();
      console.log(error);
      
    });
   
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      if(this.filter==0){
        this.router.navigate(['/menu']);
      }   else{
      this.router.navigate(['/maintenance',{filter:this.filter}]) ;
      }
    }); 
  }
back(){
  if(this.filter==0){
    this.router.navigate(['/menu']);
  }   else{
  this.router.navigate(['/maintenance',{filter:this.filter}]) ;
  }
}

async changeType(typeId){
  console.log("type:",typeId);
  
    const data=await this.storage.get("USER_ID");
    let headers=await this.config.getHeader();;
      let url=this.config.domain_url+'update_request_status';
      let body={
          id:this.request_id,
          from_id:data,
          status:status
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
    
}

async changeStatus(status){
  console.log("status:",status);
  
    const data=await this.storage.get("USER_ID")

      let url=this.config.domain_url+'update_request_status';
      let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          from_id:data,
          status:status
      }
      console.log("body:",body)
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  
  
  
  
  
}
async changeTechnician(tech){
  
    const data=await this.storage.get("USER_ID")

  let url=this.config.domain_url+'assign_technician';
  let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          technician:tech,
          from_id:data
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
   
}
async changePriority(priority){
  console.log("priority:",priority);
  let url=this.config.domain_url+'update_request_priority';
  let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          priority:priority
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  
}
async ChangeRisk(risk){
  let url=this.config.domain_url+'update_risk_rating';
  let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          risk_rating:risk
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}
async changeHazard(hazard){
  let url=this.config.domain_url+'update_hazard';
  let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          hazard:hazard
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}
async ChangeOutofOrder(out_of_order){
  let url=this.config.domain_url+'update_out_of_order';
  let headers=await this.config.getHeader();;
      let body={
          id:this.request_id,
          out_of_order:out_of_order
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}


addComment(){
  this.router.navigate(['/maintenance-comment',{flag:1,req_id:this.request_id}])
}

async pickJob(){
  
    const data=await this.storage.get("USER_ID")

    let headers=await this.config.getHeader();;
      let url=this.config.domain_url+'assign_technician';
      let body={
          id:this.request_id,
          technician:data.toString(),
          from_id:data
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res,res.data.status);
        this.pick=false;
        this.status=res.data.status.toString();
        this.selected_tec=data.toString();
        this.presentAlert('This job assigned to you successfully.')
      },error=>{
        console.log(error);
        
      })

    
}

async showImages(ev: any,i,img) {

  const popover = await this.popoverController.create({
    component: MaintenanceImageComponent,
    event: ev,
    componentProps:{
      data:img,
      type:i
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}
viewMore(){
  this.end +=5;
}
async presentAlert(mes){
const alert = await this.toastCntlr.create({
  message: mes,
  duration: 3000,
  position:'top'      
});
alert.present(); //update
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

openDoc(item){


  // window.open(encodeURI(item.post_attachment),"_system","location=yes");

// if(item.attachment.length>0){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  zoom:'yes'
  }
  if(item.includes('.pdf')){
    this.showpdf(item)
  }else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item+'&embedded=true','_blank',options);
  browser.on('loadstart').subscribe(() => {
    console.log('start');
    this.dialog.show();   
   
  }, err => {
    console.log(err);
    
    this.dialog.hide();
  })

  browser.on('loadstop').subscribe(()=>{
    console.log('stop');
    
    this.dialog.hide();;
  }, err =>{
    this.dialog.hide();
  })

  browser.on('loaderror').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
}
}
async showpdf(file){
  const modal = await this.modalCntl.create({
    component: ShowpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}

async getPublicSettings(){
  let headers=await this.config.getHeader();;
  const type=await this.storage.get('USER_TYPE');
   
    const bid=await this.storage.get('BRANCH');
    
        const tz=await this.storage.get('TIMEZONE');
        // let branch;
        // if (type == 1 || type == 4 || type == 9) {
        //   branch = this.branch
        // } else {
        //   branch = bid
        // }
      
      let url=this.config.domain_url+'maintenance_public_form_settings/'+bid
      this.http.get(url,{headers}).subscribe((res:any)=>{
        
        if(res.data&&res.data.date_time==1){
          this.preferred_status=true;
          
        }else{
          this.preferred_status=false
        }
        if(res.data&&res.data.entry_permission==1){
          this.entry_permission=true
        }else{
          this.entry_permission=false
        }
        if(res.data&&res.data.hazard==1){
          this.hazard_status=true
        }else{
          this.hazard_status=false
        }
        if(res.data&&res.data.out_of_order==1){
          this.out_of_order_status=true
        }else{
          this.out_of_order_status=false
        }
        if(res.data&&res.data.priority==1){
          this.priority_status=true
        }else{
          this.priority_status=false
        }
        if(res.data&&res.data.photos==1){
          this.photo_status=true
        }else{
          this.photo_status=false
        }
        if(res.data&&res.data.risk_rating==1){
          this.risk_status=true
        }else{
          this.risk_status=false
        }
      })
      
   
}
async getResident() {
  const type=await this.storage.get('USER_TYPE');
  let headers=await this.config.getHeader();;
 
  let url = this.config.domain_url + 'recurringdetailsapp/' + this.request_id+'/'+type;
  
  this.http.get(url,{headers}).subscribe((res: any) => {
    console.log('doc:', res, url);
    
    if(res.data[0].assigned_resident){
      let other;
      if(res.data[0].other_resident_details&&!Array.isArray(res.data[0].other_resident_details)){
        other=res.data[0].other_resident_details
      }
      this.assigned_resident={other:other,phone:res.data[0].assigned_resident.phone};
      this.resident_name=res.data[0].assigned_resident.name;
    }
  })

}
async openResidentInfo(event) {
 let phone,partner=null;
 console.log('other:',this.assigned_resident);
 
 if(this.assigned_resident.phone.substring(0,2)==61){
  phone=this.assigned_resident.phone.slice(2)
 }else{
  phone=this.assigned_resident.phone
 }
 if(this.assigned_resident.other){
  partner=this.assigned_resident.other.fname+' '+this.assigned_resident.other.lname;
 }
  
  const popover = await this.modalCntl.create({
    component: ResidentInfoComponent,
    cssClass:'resident-info-modal',
    componentProps: {
      // name: this.resident_name,
      phone:phone,
      partner:partner
    },
    
  });

 
   
   
  return await popover.present();

}
}
