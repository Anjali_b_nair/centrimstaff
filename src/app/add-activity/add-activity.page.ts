import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Clipboard } from '@ionic-native/clipboard/ngx';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Subscription, throwError } from 'rxjs';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { ActivityRecurringComponent } from '../components/activity-recurring/activity-recurring.component';
import { DOCUMENT } from '@angular/common';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
declare var $: any;
@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.page.html',
  styleUrls: ['./add-activity.page.scss'],
})
export class AddActivityPage implements OnInit {
  subscription:Subscription;
  types:any=[];
  typeId:any;
  categories:any=[];
  category:any=[];
  branch:any=[];
  facility:any=[];
  title:any;
  description:any;
  minDate: string = new Date().toISOString();
  date:any;
  start:any;
  end:any;
  venue:any;
  status:any=0;
  img:any=[];
  imageResponse:any=[];
  docs:any=[];
  doc:any=[];
  docResponse:any=[];
  // item:any;
  flag:any;
  id:any;
  attendees:any[]=[];
  attendee_id:any;
  startDates:any[]=[];
  calendar:any[]=[];
  calendar_type:any;
  hide:boolean=true;
  base64File:any;
  callLink:boolean=false;
  cal:any=0;
  stat:any=0;
  reason:any;
  branch_id:any;
  other:any;
  cid:any;
  current:any;
  isLoading:boolean=false;
  from:any;
  disable:boolean=false;
  one_to_one:boolean=true;
  staffArray:any[]=[];
staff:any[]=[];
futureOrNot:any=0;
repeat:any={};
rec_details:any;
name:any;

listener;
selectCalCheckBox: any;
checkBoxesCal: HTMLCollection;

customAlertOptions: any = {
  
  header: 'Calendar types',
  // subHeader: 'Select All:',
  message: '<ion-checkbox id="selectAllCheckBox2"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
    constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,
      private actionsheetCntlr:ActionSheetController,
      private route:ActivatedRoute,private platform:Platform,private router:Router,
      private toastCntlr:ToastController,private alertCntlr:AlertController,
      private clip:Clipboard,private loadingCtrl:LoadingController,private modalCntrl:ModalController,
      @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) {  }
  
    ngOnInit() {
      // console.log("fac:",this.facility)
      // $('#summernote4').summernote({
      //   placeholder: 'Enter text here',
      //   tabsize: 2,
      //   height: 120,
      //   toolbar: [
      //     ['font', ['bold', 'underline', 'clear']],
      //     ['para', ['ul', 'ol', 'paragraph']],
      //     ['insert', ['link']],
      //   ]
      // });
     
      
      
    }
    async ionViewWillEnter(){
      this.disable=false;
     
      this.minDate= new Date().toISOString();
    this.date=new Date().toISOString();
    this.current=moment(this.date).format('yyyy-MM-DD');
    this.start=new Date(this.current+'T09:00').toISOString();
    this.end=new Date(this.current+'T09:30').toISOString();
      this.cal=0;
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.name=this.route.snapshot.paramMap.get('name');
    this.id=this.route.snapshot.paramMap.get('id');
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.branch_id=this.route.snapshot.paramMap.get('branch');
    
      const bid=await this.storage.get("BRANCH")

  
      this.types=[];
      this.categories=[];
      this.imageResponse=[];
      this.img=[];
      this.docs=[];
      this.docResponse=[];
      this.attendees=[];
      this.branch=[];
      this.category=[];
      this.facility=[];
      let url=this.config.domain_url+'article_types';
      let headers=await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        for(var i in res.data){
          this.types.push(res.data[i]);
        }
        console.log(this.types);
        
        
      });
      this.attendees.push(this.id);
      let cat_url=this.config.domain_url+'activity_categories';
      
      this.http.get(cat_url,{headers}).subscribe((res:any)=>{
        console.log("cat:",res);
        // this.categories.push({id:0,category_name:'Select All'});
        res.data.forEach(element => {
          this.categories.push({id:element.id,category_name:element.category_name})
        });
        console.log("cat1:",this.categories);
        
      })
      // this.storage.ready().then(()=>{
        const data=await this.storage.get("USER_TYPE")

          const cid=await this.storage.get("COMPANY_ID")

  
            let url1=this.config.domain_url+'branch_viatype';
            let body={
              company_id:cid,
              role_id:data
            }
            if(data=='1'){
              this.http.post(url1,body,{headers}).subscribe((res:any)=>{
                if(data==1){
                  this.branch.push({id:0,name:'Select All'});
                  }
                res.data.forEach(element => {
                  this.branch.push({id:element.id,name:element.name});
                });
              })
            }else{
            this.http.post(url1,body,{headers}).subscribe((res:any)=>{
              res.data.forEach(element => {
                this.branch.push({id:element.id,name:element.name});
              });
              this.facility=this.branch[0].id.toString();
            })
          }
  
          this.calendar=[];
          // this.Calendar.push({id:0,wing:'All wings'});
         
              console.log("branch:",bid);
              let branch=cid.toString();
              let body1={company_id:branch}
              let url2=this.config.domain_url+'get_calendar_types';
              this.http.post(url2,body1,{headers}).subscribe((res:any)=>{
                console.log("wing:",res);
                for(let i in res.data){
                    console.log("data:",res.data[i]);
                    
                  let obj={id:res.data[i].calendar_id,name:res.data[i].calendar_types.type};
                  this.calendar.push(obj)
                
                }
                console.log("array:",this.calendar);
                
                
              },error=>{
                console.log(error);
                
              })
           
  
         
      this.from=this.route.snapshot.paramMap.get('from');
      this.getStaff();
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
        this.router.navigate(['/resident-profile',{id:this.cid,flag:this.from}],{replaceUrl:true})
     
      this.clearData();
        }); 
        
        }
        ionViewWillLeave() { 
        this.subscription.unsubscribe();
        }
  
  
        selectBranch(ev,facility){
          console.log("branchselect:",facility);
          if(facility.includes('0')){
            this.facility=[];
            // let b=this.branch.splice(0,1);
            this.branch.forEach(element => {
              if(this.facility.includes(element.id.toString())){
    
              }else{
                if((element.id.toString())!='0'){
    
               
              this.facility.push(element.id.toString());
                }
              }
    
            });
            
            console.log("fac:",this.facility);
            
            
          }
          
      }
  
  select(event){
    if(event.currentTarget.checked==true){
      this.status=1
    }else{
      this.status=0
    }
    console.log("notify",event.currentTarget.checked);
  }
  
  back(){
    
      this.router.navigate(['/resident-profile',{id:this.cid,flag:this.from}],{replaceUrl:true})
    
    this.clearData();
  }
  
  async publish(){
    this.disable=true;
    if(this.title==undefined || this.title==''){
      this.presentAlert('The activity title field is required.');
      this.disable=false;
    }else if(this.start==undefined||this.start==''||this.end==undefined||this.end==''){
      this.presentAlert('The start&end time field is required');
      this.disable=false;
    }else if(this.typeId==undefined||this.typeId==''){
      this.presentAlert('Please select wellness dimension.');
      this.disable=false;
    }else if(this.futureOrNot==0&&(this.reason==undefined||this.reason=='')){
      this.presentAlert('Please select a feedback.');
      this.disable=false;
    }else if(this.reason=='other'&&this.other==undefined || this.other==''){
      this.presentAlert('Please specify the reason.');
      this.disable=false;
    // }else if(Array.isArray(this.facility)&&this.facility.length==0){
    //   this.presentAlert('Please select atleast one facility.');
    }else{
    
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID")
        const rv=await this.storage.get('RVSETTINGS');
        let headers=await this.config.getHeader();;
          let s=moment(this.start).format('hh:mm')
          let stime=moment(this.start).format('hh:mm A');
          let etime=moment(this.end).format('hh:mm A');
          let img;
          
          let url=this.config.domain_url+'consumer_activity'
          if(this.img.length==0){
            img=null
          }else{
            img=this.img
          }
         
  
          if(this.calendar_type==undefined){
            this.calendar_type=null;
          }
          if(this.description==undefined || this.description==''){
            this.description=null;
          }
  
          if(this.reason=='other'){
            // if(){
            //   // this.presentAlert('Please specify the reason.');
            // }else{
            this.reason=this.other;
            }
          // }
          if(this.futureOrNot==1){
            this.reason=null
          }
      let one_to_one;
          if(this.one_to_one){
            one_to_one=1
          }else{
            one_to_one=0
          }
          let staff=[];
    console.log("staff:",this.staff);
    if(this.staff.length>0){
      staff=this.staff;
      console.log("Array:",staff)
   
  }else{
    staff=null;
  }
  console.log('startdate:',this.startDates)
  if(this.startDates.length==0){
    this.startDates.push(moment(this.date).format('yyyy-MM-DD'))
  }

  for(let i=0;i<this.startDates.length;i++){
    let ac_date=this.startDates[i]+' '+s
    console.log("activity_date:",ac_date,this.startDates);
          let body={
            activity:{
            activity_type:this.typeId,
            title:this.title,
            activity_info:this.description,
            activity_start_date:moment(ac_date).format('yyyy-MM-DD HH:mm:ss'),
            start_time:stime,
            end_time:etime,
            venue:null,
            created_by:uid,
            status:1,
            web_display:1,
            enable_notification:1,
            company_id:cid,
            one_to_one_session:one_to_one,
            individual_activity:1
          },
            categories:null,
            branches:this.branch_id,
            document:null,
            images:img,
            attendees:this.id,
            attend_reason:this.reason,
            calendar_type:this.calendar_type,
            assigned_to:staff,
            rv_required:rv,
            slot_limit:1,
            paid_activity:0,
            cost:null,
            waiting_list:0,
            future_activity:this.futureOrNot
          }
          this.clip.clear();
        
          console.log("body:",body);
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
  
            this.presentAlert("Activity created successfully.");
            this.clearData();
            this.router.navigate(['/resident-profile',{id:this.cid,flag:this.from}],{replaceUrl:true});
            this.disable=false;
          },error=>{
            this.presentAlert("Something went wrong. Please try again later.");
            this.disable=false;
          })
        
        }
      
    
}
    // if(res.status=="success"){
     
  // }else{
    // this.presentAlert('Something went wrong. Please try again later.')
  // }
    
  }
  addCalendar(ev){
    if(ev.currentTarget.checked==true){
      this.hide=false;
    }else{
      this.calendar_type=null;
      this.hide=true;
    }
  }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.captureImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  async captureImage(){
    
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   // let base64Image = 'data:image/jpeg;base64,' + imageData;
    //   this.imageResponse.push('data:image/jpeg;base64,' + imageData)
    //           this.uploadImage('data:image/jpeg;base64,' +imageData);
      
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.uploadImage(base64Image);

  }
  
  // pickImage(){
  //   const options: CameraOptions = {
  //     quality: 100,
  //     sourceType: this.camera.PictureSourceType.CAMERA,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     // imageData is either a base64 encoded string or a file URI
  //     // If it's base64 (DATA_URL):
  //     let base64Image = 'data:image/jpeg;base64,' + imageData;
      
  //     this.imageResponse.push(base64Image);
  //     this.uploadImage(imageData);
       
  //   }, (err) => {
  //     // Handle error
  //   });
    
  // }
  
  async addImage(){
  
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1,
    //   quality:100
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    //   this.imagePicker.getPictures(options).then((results) => {
    //     for (var i = 0; i < results.length; i++) {
    //         // console.log('Image URI: ' + results[i]);
            
    //         if((results[i]==='O')||results[i]==='K'){
    //           console.log("no img");
              
    //           }else{
    //             // this.img.push(results[i]);
    //           this.imageResponse.push('data:image/jpeg;base64,' + results[i])
    //           this.uploadImage('data:image/jpeg;base64,' +results[i]);
    //           }
    //     }
    //   }, (err) => { });
    
    // // console.log("imagg:",this.img);

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    
    }
  
    
  
  
    // selectDoc(){
    //   const options: CameraOptions = {
    //     quality: 100,
    //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //     destinationType: this.camera.DestinationType.FILE_URI,
    //     // encodingType: this.camera.EncodingType.JPEG,
    //     mediaType: this.camera.MediaType.ALLMEDIA
    //   }
    //   this.camera.getPicture(options).then((imageData) => {
    //     // imageData is either a base64 encoded string or a file URI
    //     // If it's base64 (DATA_URL):
    //     console.log('doc:',imageData);
    //     if((imageData).includes('.pdf')||(imageData).includes('.doc')){
    //       console.log('attach vdo');
    //       let fileUrl='file://'+imageData;
          
    //       // this.attach=fileUrl;
          
    //             let ext;
    //             if((imageData).includes('.pdf')){
    //               ext='pdf'
    //             }else if((imageData).includes('.doc')){
    //               ext='doc'
    //             }
                
    //               console.log("vdo:",imageData);
                  
    
    //             this.uploadDoc(fileUrl,ext);
    //            }else{
    //             console.log('not supported');
                
    //             this.alert('File format not supported');
    //           }
         
    //   }, (err) => {
    //     // Handle error
    //   });
      
    // }
    async alert(mes){
      // let message;
      // if(type==2){
      //   message='File format not supported'
      // }
      const alert = await this.alertCntlr.create({
        message: mes,
        backdropDismiss:true
      });
    
      await alert.present();
    }
  
  
  async uploadImage(img){
    if(this.isLoading==false){
      this.showLoading();
      }
    let url=this.config.domain_url+'upload_file';
    let body={file:img};
    let headers=await this.config.getHeader();;
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("uploaded:",res);
      this.img.push(res.data);
      this.dismissLoader();
      
    })
  }
  async uploadDoc(file,fname){
    let headers=await this.config.getHeader();;
    let fileName ;
    let name=fname.split('.',1)
    if(name.toString().length>3){
      fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
    }
    this.docResponse.push({name:fileName});
    // let path =  file.substring(0,  file.lastIndexOf("/") + 1);
    // //path=path.replace('Users/','');
    // console.log("path:",path);
    //  console.log('name:',fileName);
    //  let that=this;
    //  this.file.resolveLocalFilesystemUrl(file).then((entry: any)=>{
    //    entry.file(function (file) {
    //      var reader = new FileReader();
   
    //      reader.onloadend = function (encodedFile: any) {
    //        var src = encodedFile.target.result;
    //        src = src.split("base64,");
    //        var contentAsBase64EncodedString = src[1];
    //        that.base64File=contentAsBase64EncodedString;
    //        console.log("base:",contentAsBase64EncodedString);
    //        console.log("vdo:",that.base64File);
           let  url = this.config.domain_url+'upload_file';
           let body={file:file}
          // console.log("body:",img);
  
    
      
           this.http.post(url,body,{headers}).subscribe((res:any)=>{
             console.log("doc up:",res);
             this.docs.push(res.data);
           },error=>{
             console.log(error);
             
           })
           
    //      };
    //      reader.readAsDataURL(file);
    //    })
    //  }).catch((error)=>{
    //    console.log(error);
    //  })
  
  
  
  
  //   let url=this.config.domain_url+'upload_file';
  //   let body={file:doc}
  // console.log("body:",this.docs);
  
  //   this.http.post(url,body).subscribe((res:any)=>{
  //     console.log("uploaded:",res);
  //     this.doc.push(res.data);
      
  //   })
  }
  
  
  
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  
  
  
  
  removeImg(i){
    this.imageResponse.splice(i,1);
    this.img.splice(i,1);
  }
  
  
  removeDoc(i){
    this.docResponse.splice(i,1);
    this.docs.splice(i,1);
  }
  
  typeChange(typeId){
    console.log('id:',typeId);
    this.typeId=typeId;
    if(typeId==9){
      console.log('meeting');
      
      this.callLink=true;
    }else{
      this.callLink=false;
    }
  }
  
  generateRoom(){
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  
    for (let i = 0; i < 9; i++) {
  
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
  
      
    }
    return outString;
  }
  
  async generate(){
    let jitsiRoom=this.generateRoom();
   
     const data=await this.storage.get("COMPANY_ID")

       let url=this.config.domain_url+'generatecallurl';
       let headers=await this.config.getHeader();
       let body={company_id:data}
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
         let callLink=res.vdocalllink+jitsiRoom;
        //  this.jitsiCall(callLink);
        //  this.sendLink(callLink);
        this.callPrompt(callLink);
         
       })
     
  
  }
  
  async callPrompt(callLink) {
    
    const alert = await this.alertCntlr.create({
      mode:'ios',
      header: 'Copy&paste video call link',
      backdropDismiss:true,
      message: 'Copy and paste the link in the description field.',
      inputs: [
        {
          name: 'link',
          type: 'text',
          
          value: callLink,
          disabled:true
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Copy & Paste',
          handler: () => {
           
            this.clip.copy(callLink);
            this.clip.paste().then(
              (resolve: string) => {
                let des=$('#summernote').summernote('code')+'<p><a href="'+resolve+'" target="_blank">Click here to start video call</a><br></p>';
                $('#summernote').summernote('code',des);
                console.log('resolve:',resolve);
               },
               (reject: string) => {
                 console.log("err:",reject);
                 
               }
             );
           
          
  
          }
        }
      ]
    });
    
    await alert.present();
   
  }
  clearData(){
    this.types=[];
      this.categories=[];
      this.imageResponse=[];
      this.img=[];
      this.docs=[];
      this.docResponse=[];
      this.attendees=[];
      this.branch=[];
      this.category=[];
      this.facility=[];
      this.title='';
      this.venue='';
      this.typeId='';
      this.callLink=false;
      this.hide=true;
      this.calendar_type='';
      this.start='';
      this.end='';
      this.stat=0;
      this.cal=0;
      this.description='';
  
  
  
  
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
     
      componentProps: {
       data:this.imageResponse,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }


  async getStaff(){
  
    const bid=await this.storage.get("BRANCH")

  
    const utype=await this.storage.get("USER_TYPE")

    const uid=await this.storage.get("USER_ID")

  const cid=await this.storage.get("COMPANY_ID")
  this.staffArray=[];
 
  this.staffArray.push({user_id:'0',name:'All'})
  
  
        

  let url1=this.config.domain_url+'activity_staffs';
 
    let headers=await this.config.getHeader();
   

      let body={company_id:cid,status:1}
        this.http.post(url1,body,{headers}).subscribe((res:any)=>{
          console.log("arrStaff:",res);
          let staff=res.data.super_admins.concat(res.data.managers).concat(res.data.staffs)
          console.log('staff:',staff)
          staff.forEach(element => {
            this.staffArray.push({user_id:element.user_id,name:element.name})
          });
         
        })
    
}
selectStaff(ev,staff){
  console.log("staffselect:",staff);
  if(staff.includes('0')){
    this.staff=[];
    // let b=this.branch.splice(0,1);
    this.staffArray.forEach(element => {
      if(this.staff.includes(element.user_id.toString())){

      }else{
        if((element.user_id.toString())!='0'){

       
      this.staff.push(element.user_id.toString());
        }
      }
      console.log("staffAee:",this.staff);
    });
    
    
    
    
  }
  
}

futureActivity(event){
  console.log('future:',this.futureOrNot,event);
  if(this.futureOrNot==1){
    this.minDate='2040'
  }else{
    this.minDate=new Date().toISOString();
    this.date=new Date().toISOString();
    this.startDates=[];
  }
}

async  makeRecurring(){
  const modal = await this.modalCntrl.create({
    component: ActivityRecurringComponent,
   
    componentProps: {
      data:this.date,
      rep:this.repeat
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data == null||dataReturned.data == undefined) {
      
      this.startDates=[];
      
    }else{
      console.log("dataret:",dataReturned);
      this.repeat=dataReturned.role;
      this.startDates = dataReturned.data;
      this.rec_details = this.repeat.rec_details;
    }
    console.log("data:",this.startDates);
    
  });
  return await modal.present();
}




openSelector(selector) {
  selector.open().then((alert)=>{
    this.selectCalCheckBox = this.document.getElementById("selectAllCheckBox2");
   this.checkBoxesCal = this.document.getElementsByClassName("alert-checkbox");
    this.listener = this.renderer.listen(this.selectCalCheckBox, 'click', () => {
        if (this.selectCalCheckBox.checked) {
          for (let checkbox of this.checkBoxesCal) {
            if (checkbox.getAttribute("aria-checked")==="false") {
              (checkbox as HTMLButtonElement).click();
              this.calendar_type=[];
            };
          };
        } else {
          for (let checkbox of this.checkBoxesCal) {
            if (checkbox.getAttribute("aria-checked")==="true") {
              (checkbox as HTMLButtonElement).click();
              this.calendar.forEach(element => {
                if(this.calendar_type.includes(element.id.toString())){
      
                }else{
                  if((element.id.toString())!='0'){
      
                 
                this.calendar_type.push(element.id.toString());
                  }
                }
      
              });
            };
          };
        }
    });
    alert.onWillDismiss().then(()=>{
      this.listener();
      console.log('fac:',this.calendar_type)
    });
  })
  
}
}
