import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerGalleryPageRoutingModule } from './consumer-gallery-routing.module';

import { ConsumerGalleryPage } from './consumer-gallery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerGalleryPageRoutingModule
  ],
  declarations: [ConsumerGalleryPage]
})
export class ConsumerGalleryPageModule {}
