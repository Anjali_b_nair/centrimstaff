import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { OpenImageComponent } from '../components/open-image/open-image.component';
import { UtbVdoComponent } from '../components/utb-vdo/utb-vdo.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-gallery',
  templateUrl: './consumer-gallery.page.html',
  styleUrls: ['./consumer-gallery.page.scss'],
})
export class ConsumerGalleryPage implements OnInit {
id:any;
cid:any;
items:any[]=[];
media:any[]=[];
start:any=0;
end:any=20;
flag:any;
from:any;
subscription:Subscription;
  constructor(private route:ActivatedRoute,private config:HttpConfigService,private http:HttpClient,
    private iab:InAppBrowser,private smedia:StreamingMedia,private modalCntl:ModalController,
    private orient:ScreenOrientation,private popCntlr:PopoverController,private platform:Platform,
    private router:Router) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.id=this.route.snapshot.paramMap.get('id');
  this.cid=this.route.snapshot.paramMap.get('cid');
  this.flag=this.route.snapshot.paramMap.get('flag');
  this.from=this.route.snapshot.paramMap.get('from');
  this.media=[];
  this.items=[];
  // if(this.from==1){
  //   let url=this.config.domain_url+'consumer_profile_gallery';
  //   let body={
  //     consumer_id:this.cid
  //   }
  //   this.http.post(url,{body}).subscribe((res:any)=>{
  //     console.log(res);
  //   })
  // }else{
    let headers=await this.config.getHeader();
  let url=this.config.domain_url+'user_profile_gallery/'+this.cid;
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log(res);
res.data.forEach(ele => {
  if(ele.videos.length!=0){
    ele.videos.forEach(element => {
      let url;
      if(element.file==null || element.file==''){
        
    if(element.url.includes('www.youtube.com')){
      url=(element.url).replace('watch?v=','embed/');
     }else if(element.url.includes('youtu.be')){
      let id=(element.url).substr(element.url.lastIndexOf('/') + 1);
      url='https://www.youtube.com/embed/'+id
     }else 
        url=element.url;
      }else{
        url=element.file;
      }
      let item={img:element.thumbnail,url:url,vid:1};
      this.items.push(item);
    });
  }
  if(ele.images.length!=0){
    ele.images.forEach(element => {
      let item={img:element.post_attachment,url:null,vid:0};
      this.items.push(item);
    });
 
  }
});
    
    console.log("items:",this.items);
    this.loadmore(false, "");

  })

// }
 
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    
  this.back();
    }); 
    
}
back(){
  if(this.from==1){
    this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
  }else{
      
  this.router.navigate(['/resident-profile',{id:this.cid,flag:this.flag}])
  }
}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

loadmore(isFirstLoad,event){
if(!isFirstLoad){
  for(let i=0;i<20;i++){
    if(i<this.items.length){
  this.media.push(this.items[i])
    }
  }
}else{
  for(let i=this.end;i<this.end+20;i++){
    if(i<this.items.length){
    this.media.push(this.items[i])
    }
  }
  this.end +=20;
}
if(isFirstLoad)
event.target.complete();
console.log("media:",this.media);
}
doInfinite(event) {
  setTimeout(()=>{
    this.loadmore(true, event)
  },1000)
  
  }
open(item){
  if(item.vid==1){
    if(item.url.includes('vimeo.com')){
      let options:InAppBrowserOptions ={
        location:'no',
        hideurlbar:'yes',
        zoom:'no'
      }
      screen.orientation.lock('landscape');
        
      const browser = this.iab.create(item.url,'_blank',options);
      browser.on('exit').subscribe(()=>{
        screen.orientation.unlock();
        screen.orientation.lock('portrait');
      })
    }else if((item.url.includes('www.youtube.com'))){
      this.playUtb(item.url)
    }else{
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played')

        
       },
        errorCallback: (e) => { console.log('Error streaming')
        
       },
        
        orientation: 'landscape',
        shouldAutoClose: true,
        controls: true
      };
    
      this.smedia.playVideo(item.url, options);
      }
    
  }else{
    this.openImg(item.img)
  }
}
async playUtb(link){
  const modal = await this.modalCntl.create({
    component: UtbVdoComponent,
    cssClass:"vdoModal",
    componentProps: {
     data:link,
      
        
    }
  });
  modal.onDidDismiss().then(()=>{
    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
  })
  return await modal.present();
}


async openImg(item) {
  console.log("openDoc",item);
  
    const popover = await this.popCntlr.create({
      component: OpenImageComponent,
      backdropDismiss:true,
      componentProps:{
        data:item
      },
      cssClass:'msg_attach'
    });
    return await popover.present();
  }

  viewMore(){
    this.end +=20;
  }
}
