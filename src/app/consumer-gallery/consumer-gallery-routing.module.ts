import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerGalleryPage } from './consumer-gallery.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumerGalleryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerGalleryPageRoutingModule {}
