import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-petty-cash',
  templateUrl: './petty-cash.page.html',
  styleUrls: ['./petty-cash.page.scss'],
})
export class PettyCashPage implements OnInit {
  consumer:any={};
  img:any;
  name:any;
  room:any;
  bal:any[]=[];
  balance:number;
  pettyCash:any[]=[];
  id:any;
  flag:any;
  subscription:Subscription;
  wing:any;
  create:boolean=false;
  editStatus:boolean=false;
  constructor(private objService:GetobjectService,private http:HttpClient,private config:HttpConfigService,
    private router:Router,private route:ActivatedRoute,private platform:Platform,
    private storage:Storage) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.pettyCash=[];
    this.bal=[];
    // this.consumer=this.objService.getExtras();
    console.log("res:",this.consumer);
    this.id=this.route.snapshot.paramMap.get('id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    
      const bid=await this.storage.get("BRANCH")

   let url1=this.config.domain_url+'consumer/'+this.id;
   let headers=await this.config.getHeader();;
   this.http.get(url1,{headers}).subscribe((res:any)=>{
    
     this.consumer=res.data;
    this.img=this.consumer.user.profile_pic;
    this.name=this.consumer.user.name;
    this.room=this.consumer.room;
    this.wing=this.consumer.wing.name;
   })
    let url=this.config.domain_url+'pettycash/'+this.id

    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("data:",res,url);

      this.balance=0;
      res.data.reverse().forEach(ele=>{
        this.balance=this.balance+parseFloat(ele.amount);
        console.log('inte:',parseFloat(ele.amount));
        
        let petty={'petty_cash':ele,'bal':this.balance};
        this.bal.push(petty)
      })
      console.log("bal:",this.bal);
      this.pettyCash=this.bal.reverse();

      // this.pettyCash=res.data
    });
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.finance==1&&routes.includes('finance.create')) {

        this.create = true
      } else {

        this.create = false;
        
      }
      if (res.main_permissions.finance==1&&routes.includes('finance.edit')) {

        this.editStatus = true
      } else {

        this.editStatus = false;
        
      }
    })
 
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
  }); 
  
    }
    ionViewWillLeave() { 
      this.subscription.unsubscribe();
   }
  add(){
    this.objService.setExtras(this.consumer);
    this.router.navigate(['/add-petty-cash',{flag:this.flag}])
  }
  back(){
    if(this.flag==2){
      this.router.navigate(['/consumers',{flag:2}])
      
    }else{
      this.router.navigate(['/resident-profile',{id:this.id,flag:this.flag}])
    }
  }
  edit(item){
    this.objService.setExtras(this.consumer);
    this.router.navigate(['/edit-petty-cash',{flag:this.flag,type:item.petty_cash.type,amount:item.petty_cash.amount,note:item.petty_cash.note,date:item.petty_cash.date,id:item.petty_cash.id}])
  }
}
