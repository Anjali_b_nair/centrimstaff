import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateConsumerPage } from './create-consumer.page';

describe('CreateConsumerPage', () => {
  let component: CreateConsumerPage;
  let fixture: ComponentFixture<CreateConsumerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConsumerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateConsumerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
