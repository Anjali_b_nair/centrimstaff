import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateConsumerPage } from './create-consumer.page';

const routes: Routes = [
  {
    path: '',
    component: CreateConsumerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateConsumerPageRoutingModule {}
