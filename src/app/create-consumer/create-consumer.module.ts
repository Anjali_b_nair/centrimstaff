import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateConsumerPageRoutingModule } from './create-consumer-routing.module';

import { CreateConsumerPage } from './create-consumer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CreateConsumerPageRoutingModule
  ],
  declarations: [CreateConsumerPage]
})
export class CreateConsumerPageModule {}
