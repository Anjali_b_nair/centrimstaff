import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActionSheetController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from '../services/http-config.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import moment from 'moment';
import { CbDisabletimeslotComponent } from '../components/cb-disabletimeslot/cb-disabletimeslot.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-create-consumer',
  templateUrl: './create-consumer.page.html',
  styleUrls: ['./create-consumer.page.scss'],
})
export class CreateConsumerPage implements OnInit {
  fname:any;
  lname:any;
  dob:any;
  gender:any;
  genderArray:any=[];
  img:any;
  pic:any;
  name:any
  wingArray:any=[];
  wing:any;
  wingId:any;
  branch:any=[];
  branch1:any;
  branch_id:any;
  room:any;
  con_type:any=3;
  adm_date:any=new Date().toISOString();
  status:any=1;
  user_name:any;
  phone:any;
  email:any;
  description:any;
  current_date:string = new Date().toISOString();
  consent:any=1;
  subscription:Subscription;
  flag:any;
  disable:boolean=false;
  password:any;
  confirmPassword:any;
  expand1:boolean=true;
  expand2:boolean=false;
  expand3:boolean=false;
  con_pass_type:any='password';
  new_pass_type:any='password';
  passwordForm:FormGroup;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,private webView:WebView,
    private camera:Camera,private actionsheetCntlr:ActionSheetController,
    private toastCntlr:ToastController,private router:Router,private platform:Platform,private route:ActivatedRoute) { }

  ngOnInit() {
    this.passwordForm=new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)]),
     
      confirmPassword : new FormControl('',[Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)])
    });
  }
  async ionViewWillEnter(){
    this.disable=false;
    const bid=await this.storage.get("BRANCH");
    this.branch1=await this.storage.get("BNAME");
    this.getWing(bid);
   this.getBranch();
   this.getGender();
   this.flag=this.route.snapshot.paramMap.get('flag');
   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
     if(this.flag==3){
       this.router.navigate(['/menu'],{replaceUrl:true})
     }else{
    this.router.navigate(['/consumers',{flag:1}],{replaceUrl:true}) ; 
     }
}); 

  }
  back(){
    if(this.flag==3){
      this.router.navigate(['/menu'],{replaceUrl:true})
    }else{
   this.router.navigate(['/consumers',{flag:1}],{replaceUrl:true}) ; 
    }
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
 async getWing(bid){
  this.wingArray=[];
//  this.wingArray.push({id:0,wing:'All facilities'});

  //  const bid=await this.storage.get("BRANCH")
  let headers=await this.config.getHeader();

     let branch=bid.toString();
     let body={company_id:branch}
     let url=this.config.domain_url+'branch_wings/'+bid;
     this.http.get(url,{headers}).subscribe((res:any)=>{
       console.log("wing:",res,url);
       for(let i in res.data.details){
        console.log("data:",res.data.details[i]);
        
      let obj={id:res.data.details[i].id,wing:res.data.details[i].name};
   
         this.wingArray.push(obj);
        //  if(res.data.details[i].id==this.consumer.wing_id){
           
           
        //    this.wing=res.data.details[i].name;
        //    this.wingId=this.consumer.wing_id;
        //    console.log("wingg:",this.wing);
        //  }
       
       }
       console.log("array:",this.wingArray);
       
       
     },error=>{
       console.log(error);
       
     })
  
 }
  setWing(wing){
    this.wingArray.forEach(element => {
      if(element.wing==wing){
        this.wingId=element.id;
      }
    });
    console.log("wingId:",this.wingId);
    
  }
   async getBranch(){
   
      const data=await this.storage.get("USER_TYPE")

        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")


       
        // this.user_type=data;
        let url=this.config.domain_url+'branch_viatype';
        let body={
          company_id:cid,
          role_id:data
        }
        this.branch_id=bid
        if(data=='1'){
          let headers=await this.config.getHeader();
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
           this.branch=res.data;
          //  this.branch1=res.data[0].name;
          //  this.branch_id=res.data[0].id;
          
        })
      }else{
        let headers=await this.config.getHeader();
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          this.branch=res.data;
          // this.branch1=res.data[0].name;
          // this.branch_id=res.data[0].id;
         
       })
      }
      this.getWing(this.branch_id)
    
   }
  
   setBranch(branch){
    this.branch.forEach(element => {
      if(element.name==this.branch1){
        this.branch_id=element.id;
        this.getWing(this.branch_id)
      }
    });
    
    console.log("wingId:",this.branch_id);
   }
   changeGender(id){
    this.gender=id
  }
  residentType(type){
    this.con_type=type
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.pic=base64Image;
      
this.uploadProfilePic(base64Image);
      //  this.pic=this.webView.convertFileSrc(imageData);
        
      
        
      
      
    }, (err) => {
      // Handle error
    });
  }

  async uploadProfilePic(img){
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      // this.img.push(res.data);
      this.img=res.data;
      // console.log("uploaded:",this.media);
    })
  }
// select gallery or camera
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  changeStatus(ev){
    if(ev.currentTarget.checked==true){
      this.status=1;
    }else{
      this.status=0;
    }
  }
  changeConsent(ev){
    if(ev.currentTarget.checked==true){
      this.consent=1;
    }else{
      this.consent=0;
    }
  }
  finish(){
    // var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
    //             '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    //             '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    //             '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    //             '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    //             '(\\#[-a-z\\d_]*)?$','i');
    var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var pass=new RegExp('(?=[A-Za-z0-9!@#$%^&*]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).*$');
console.log('email:',this.email)
                if(this.phone==undefined || this.phone==''){
                  this.phone=null;
                }
    if(this.fname==undefined || this.lname==undefined || this.dob==undefined || !this.gender||
       this.room==undefined  || this.email==undefined ||this.wingId==undefined ||this.adm_date==undefined||this.password==undefined){
         this.presentAlert('Please fill all mandatory fields.')
       }else if(this.fname=='' || this.lname=='' || this.dob=='' || this.room=='' ||
        this.email=='' ||this.wingId==''||this.adm_date==''||this.password==''){
          this.presentAlert('Please fill all mandatory fields.')
      //  }else if(this.password.length<8){
      //     this.presentAlert('Password must be atleast 8 characters.');
      //  }else if(!pass.test(this.password)){
      //   this.presentAlert('Password must contain atleast one uppercase, lowercase, digit and symbol.')
        }else if(this.password&&!this.confirmPassword){
          this.presentAlert('Please verify your password.')
        }else if(this.password&&this.password!==this.confirmPassword){
          this.presentAlert("Confirmed password doesn't match with new password.");
        
       }else if(this.password&&this.passwordForm.invalid){
        this.presentAlert("Incorrect password format.")
      }else if(this.phone!=null&&(this.phone.toString().length<10 || this.phone.toString().length>10)){
        this.presentAlert('Please enter a 10 digit mobile number.');
       }else if(!pattern.test(this.email)){
        this.presentAlert('Please enter a valid email Id.')
       }else if(/^\d+$/.test(this.fname)){
        this.presentAlert('First name field contains only digits.');
      }else{
          if(this.img==undefined){
            this.img=null
          }
          this.disable=true;
            this.addConsumer();
          
          
        }
    

        
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  async addConsumer(){
   
      const cid=await this.storage.get("COMPANY_ID")

        const uid=await this.storage.get("USER_ID")



          let dob=moment(this.dob).format('yyyy-MM-DD');
          let ad_date=moment(this.adm_date).format('yyyy-MM-DD');
          let phone;
          if(this.phone!=null){
            phone=this.phone
          }else{
            phone=null
          }
          if(this.description==undefined||this.description=='undefined'){
            this.description=null
          }
        
          let url=this.config.domain_url+'consumer';
          let headers=await this.config.getHeader();;
              let body={
                  fname:this.fname,
                  lname:this.lname,
                  dob:dob,
                  wing:this.wingId,
                  room:this.room,
                  status:this.status,
                  description:this.description,
                  email:this.email,
                  phone:phone,
                  branch:this.branch_id,
                  ad_date:ad_date,
                  gender:this.gender,
                  consumer_type:this.con_type,
                  profile_pic:this.img,
                  consent_to_tag:this.consent,
                  company_id:cid,
                  created_by:uid,
                  share_details:1,
                  password:this.password
                
              }
              console.log(url,body,this.img);
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log("data:",res);
                if(res.status=='error'){
                  this.presentAlert(res.message);
                  this.disable=false;
                }else if(res.status=='success'){
                  this.presentAlert('Resident profile added successfully.');
                  this.disable=false;
                  this.router.navigate(['/consumers',{flag:1}],{replaceUrl:true})
                }
              },error=>{
                this.disable=false;
                this.presentAlert('Something went wrong.Please try again later.');
                console.log(error);
                
              })



       
    

  }
  // upload(){
  //   this.storage.ready().then(()=>{
  //     const cid=await this.storage.get("COMPANY_ID")

  //       const uid=await this.storage.get("USER_ID")

  //             let dob=moment(this.dob).format('yyyy-MM-DD');
  //             let ad_date=moment(this.adm_date).format('yyyy-MM-DD');

  //             const fileTransfer: FileTransferObject=this.transfer.create();
  //             let options: FileUploadOptions = {
  //               fileKey: 'profile_pic',
  //               chunkedMode:false,
  //               httpMethod:'post',
  //               mimeType:'image/jpeg',
  //               headers:this.config.head,
  //               params:{
  //                 fname:this.fname,
  //                 lname:this.lname,
  //                 dob:dob,
  //                 wing:this.wingId,
  //                 room:this.room,
  //                 status:this.status,
  //                 description:this.description,
  //                 email:this.email,
  //                 phone:this.phone,
  //                 branch:this.branch_id,
  //                 ad_date:ad_date,
  //                 gender:this.gender,
  //                 consumer_type:this.con_type,
  //                 consent_to_tag:this.consent,
  //                 company_id:cid,
  //                 created_by:uid
  //               }
  //             }
  //             let url=this.config.domain_url+'consumer';
  //             fileTransfer.upload(this.img,url,options).then((data)=>{
  //               console.log("success");
  //               console.log(data);
  //               // if(data.status=='error'){
  //               //   this.presentAlert(data.message);
  //               // }
                
  //             },error=>{
  //               console.log(error);
                
  //             })
  //       })
  //     })
  //   })



  // }
  expandSection(i){
    if(i==1){
      this.expand1=!this.expand1;
      this.expand2=false;
      this.expand3=false;
    }else if(i==2){
      this.expand1=false;
      this.expand2=!this.expand2;
      this.expand3=false;
    }else if(i==3){
      this.expand1=false;
      this.expand2=false;
      this.expand3=!this.expand3;
    }
  }

  async getGender(){
    let url=this.config.domain_url+'get_gender';
  let headers=await this.config.getHeader();;
  console.log("dat:",url,headers)
   this.http.get(url,{headers}).subscribe((res:any)=>{

    console.log('gen:',res);
    this.genderArray=res;

    
   })
  }

  checkStrength(password) {
    var strength = 0;
    let passed:any = {
     'special':false,
     'alpa':false,
     'num':false,
     'len':false,
    }
    var text = document.getElementById('result');
    var bar = document.getElementById('password-strength');
    var character = document.getElementById('one-char').children[0];
    var num = document.getElementById('one-num').children[0];
    var special = document.getElementById('one-special-char').children[0];
    var len = document.getElementById('eight-char').children[0];

    //If password contains both lower and uppercase characters, increase strength value.
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
     // strength += 1;
     passed.alpa =true
     character.setAttribute('name', 'checkmark');
    } else {
     passed.alpa =false
     character.setAttribute('name', 'ellipse-outline');
    }

    //If it has numbers and characters, increase strength value.
    if (password.match(/([0-9])/)) {
     // strength += 1;
     passed.num = true
     num.setAttribute('name', 'checkmark');
    } else {
     passed.num = false
     num.setAttribute('name', 'ellipse-outline');
    }

    //If it has one special character, increase strength value.
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
     // strength += 1;
     passed.special = true
     special.setAttribute('name', 'checkmark');
    } else {
     passed.special = false
     special.setAttribute('name', 'ellipse-outline');
    }

    if (password.length > 7) {
     // strength += 1;
     passed.len =true
     len.setAttribute('name', 'checkmark');
    } else {
     passed.len =false
     len.setAttribute('name', 'ellipse-outline');
    }

    strength = 0;
    for (const key in passed) {
     if (passed.hasOwnProperty(key) && passed[key] === true) {
        strength++;
     }
    }
    console.log(strength)

    if(strength<=0 ){
     text.textContent =''
     text.classList.value =''
     bar.classList.value =''
    // }else if(strength ==0 && password.length>0){
    //  text.textContent = 'Very weak';
    //  text.classList.value ='';
    //  text.classList.add('vweak-pswd');
    //  bar.classList.value ='';
    }
    else if(strength ==1){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-weak', 'p-v-weak');
     text.classList.add('vweak-pswd')
     text.textContent = 'Very weak';
    }else if(strength ==2){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-weak', 'p-weak');
     text.classList.add('weak-pswd')
     text.textContent = 'Weak';
    }
    else if(strength ==3){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-strong', 'p-strong');
     text.classList.add('strong-pswd')
     text.textContent = 'Strong';
    }
    else if(strength ==4){
     text.classList.value =''
     bar.classList.value='';
     bar.classList.add('pswd-very-strong', 'p-v-strong');
     text.classList.add('vstrong-pswd')
     text.textContent = 'Very strong';
     return true;
    }


    return false;
}

  typeChange(i) {
    if (i == 1) {
      this.con_pass_type = 'text';
    } else if (i == 2) {
      this.new_pass_type = 'text';
    } else if (i == 3) {
      this.con_pass_type = 'password';
    } else if (i == 4) {
      this.new_pass_type = 'password';
    }
  }
}
