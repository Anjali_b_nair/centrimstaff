import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResDocumentsPageRoutingModule } from './rv-res-documents-routing.module';

import { RvResDocumentsPage } from './rv-res-documents.page';
import { RvDocsOptionsComponent } from '../components/rv-docs-options/rv-docs-options.component';
import { RvDocEditComponent } from '../components/rv-doc-edit/rv-doc-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResDocumentsPageRoutingModule
  ],
  declarations: [RvResDocumentsPage,
  // RvDocsOptionsComponent,
RvDocEditComponent],
  entryComponents:[
    // RvDocsOptionsComponent,
    RvDocEditComponent
  ]
})
export class RvResDocumentsPageModule {}
