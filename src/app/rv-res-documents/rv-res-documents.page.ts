import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { RvDocsOptionsComponent } from '../components/rv-docs-options/rv-docs-options.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-res-documents',
  templateUrl: './rv-res-documents.page.html',
  styleUrls: ['./rv-res-documents.page.scss'],
})
export class RvResDocumentsPage implements OnInit {
  cid:any;
  id:any;
  consumer:any;
  folder:any=[];
  subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private popCntl:PopoverController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.consumer=this.route.snapshot.paramMap.get('consumer');

    this.getDocs();
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.back();
      
  }); 
  
  }

  back(){
    this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}])
  }

  async options(ev,item){
    const popover = await this.popCntl.create({
      component:RvDocsOptionsComponent,
      cssClass:'rv-doc-options-popover',
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:item,
       

      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data!=undefined||dataReturned.data!=null){
     this.getDocs();
    
      }
   
   
      
    });
    return await popover.present();
  }

  async getDocs(){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    let url=this.config.domain_url+'resident_documents';
    let headers=await this.config.getHeader();
    let body={
      resident_user_id:this.id
    }
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('docs:',res);
      this.folder=res.folders
    })
  }

  openFolder(item){
    let res=[{id:item.id,title:item.title}]
    this.router.navigate(['/rv-res-docs-inner',{id:this.id,cid:this.cid,res:JSON.stringify(res),consumer:this.consumer,count:0}])
  }
}
