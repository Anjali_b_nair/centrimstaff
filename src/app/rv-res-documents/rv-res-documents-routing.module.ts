import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResDocumentsPage } from './rv-res-documents.page';

const routes: Routes = [
  {
    path: '',
    component: RvResDocumentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResDocumentsPageRoutingModule {}
