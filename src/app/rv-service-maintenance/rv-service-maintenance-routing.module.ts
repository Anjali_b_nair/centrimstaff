import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvServiceMaintenancePage } from './rv-service-maintenance.page';

const routes: Routes = [
  {
    path: '',
    component: RvServiceMaintenancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvServiceMaintenancePageRoutingModule {}
