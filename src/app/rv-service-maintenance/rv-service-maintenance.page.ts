import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { RvAssignedStaffComponent } from '../components/rv-assigned-staff/rv-assigned-staff.component';
import { RvMaintenaceFilterComponent } from '../components/rv-maintenace-filter/rv-maintenace-filter.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-service-maintenance',
  templateUrl: './rv-service-maintenance.page.html',
  styleUrls: ['./rv-service-maintenance.page.scss'],
})
export class RvServiceMaintenancePage implements OnInit {
  cid:any;
  id:any;
  sdate:any;
  edate:any;
  offset:any=0;
  terms: any;
  maintenance:any=[];
  subscription:Subscription;
  risk:any='0';
  category:any='0';
  date_filter:any='6';
  assignedTo:any=[];
  staff:any;
  status:any='unresolved';
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,private popCntl:PopoverController,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.maintenance=[];
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.edate=moment().format('YYYY-MM-DD HH:mm:ss');
    this.sdate=moment().subtract(30,'days').format('YYYY-MM-DD HH:mm:ss');
    this.getMaintenanceList(false,'');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  }); 
  
  }
  back(){
    this.router.navigate(['/rv-services-home',{id:this.id,cid:this.cid}])
  }

  async getMaintenanceList(isFirstLoad, event){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    const utype = await this.storage.get('USER_TYPE');
    let url=this.config.domain_url+'consumer_maintenance_filter';
    let headers=await this.config.getHeader();
    let body;
    body={
      created_for:this.id,
      sdate:this.sdate,
      edate:this.edate,
      limit:20,
      offset:this.offset,
      status:this.status,
      role_id:utype
    }
    // if(this.type){
    //   body.feedback_type=this.type
    // }

    if(this.terms){
      body.search=this.terms
     
      
    }
    if(this.risk!=='0'){
      body.risk_rating=this.risk
    }

    if(this.assignedTo.length){
      body.assigned_to=this.assignedTo[0];
      body.technician=this.assignedTo[0];
    }
    if(this.category!=='0'){
      body.category=this.category
    }
    console.log(body,{headers});
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
     
     let dateArray=Object.keys(res.data)
      for (let i = this.offset; i < dateArray.length; i++) {
       
        this.maintenance.push({date:dateArray[i],maintenance:res.data[dateArray[i]]});
      }
      
      console.log('act:',this.maintenance);
     
      if (isFirstLoad)
      event.target.complete();
  
    this.offset=this.offset+20;
      
     },error=>{
       console.log(error);
      
     })
  }

  doInfinite(event) {
    this.getMaintenanceList(true, event)
    }

   
    cancel() {
     
      this.terms = undefined;
      this.ionViewWillEnter();
    }
  
    async search(){
      this.offset=0;
      this.maintenance=[];
    this.getMaintenanceList(false,'');
    }

    async filter(){
      const modal = await this.modalCntl.create({
        component: RvMaintenaceFilterComponent,
        componentProps: {
          
          date_filter:this.date_filter,
         risk:this.risk,
         status:this.status,
         sdate:this.sdate,
         edate:this.edate,
         assignedTo:this.assignedTo,
         staff:this.staff,
         category:this.category
           
        },
        
        
      });
      modal.onDidDismiss().then((dataReturned)=>{
        if(dataReturned.data){
          this.date_filter=dataReturned.data.date_filter;
          this.status=dataReturned.data.status;
          this.risk=dataReturned.data.risk;
          this.sdate=dataReturned.data.sdate;
          this.edate=dataReturned.data.edate;
          this.category=dataReturned.data.category;
          this.assignedTo=dataReturned.data.assignedTo;
          this.staff=dataReturned.data.staff;
          this.offset=0;
          this.maintenance=[];
          this.getMaintenanceList(false,'');
        }
        
      })
      return await modal.present();
    }


    async assigned(item){
      const popover = await this.modalCntl.create({
        component:RvAssignedStaffComponent,
        cssClass:'cbmorepop',
      backdropDismiss:true,
        componentProps:{
          data:item,
         flag:1
  
        },
        
        
        
      });
      
     
     
        
    
      return await popover.present();
    }
}
