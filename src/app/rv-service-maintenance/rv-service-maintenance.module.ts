import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvServiceMaintenancePageRoutingModule } from './rv-service-maintenance-routing.module';

import { RvServiceMaintenancePage } from './rv-service-maintenance.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { RvMaintenaceFilterComponent } from '../components/rv-maintenace-filter/rv-maintenace-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvServiceMaintenancePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvServiceMaintenancePage,
    RvMaintenaceFilterComponent
  ],
  entryComponents:[
    RvMaintenaceFilterComponent
  ]
})
export class RvServiceMaintenancePageModule {}
