import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallbookingListPage } from './callbooking-list.page';

const routes: Routes = [
  {
    path: '',
    component: CallbookingListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CallbookingListPageRoutingModule {}
