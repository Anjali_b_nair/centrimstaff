import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CallbookingListPageRoutingModule } from './callbooking-list-routing.module';

import { CallbookingListPage } from './callbooking-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CallbookingListPageRoutingModule,
    ApplicationPipesModule,
    CalendarModule
  ],
  declarations: [CallbookingListPage]
})
export class CallbookingListPageModule {}
