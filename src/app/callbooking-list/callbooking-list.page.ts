import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
import { CbInfoComponent } from '../components/cb-info/cb-info.component';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { CbOptionsComponent } from '../components/cb-options/cb-options.component';
import { CbMoreComponent } from '../components/cb-more/cb-more.component';
import { Storage } from '@ionic/storage-angular'
;
import { CbDisabletimeslotComponent } from '../components/cb-disabletimeslot/cb-disabletimeslot.component';


// declare var jitsiplugin;

@Component({
  selector: 'app-callbooking-list',
  templateUrl: './callbooking-list.page.html',
  styleUrls: ['./callbooking-list.page.scss'],
})
export class CallbookingListPage implements OnInit {
  subscription:Subscription;
  date:Date;
  date_1:any;
  today:any;
  current:Date=new Date();
  callList:any[]=[];
  timezone:any;
  terms:any;
  staffArray:any[]=[];
staff:any;
staff_id:any;
create:boolean=false;
edit:boolean=false;
  constructor(private platform:Platform,private router:Router,private http:HttpClient,private config:HttpConfigService,
    private popCntlr:PopoverController,private iab:InAppBrowser,private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {
    this.date=this.current;
    this.today=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
  }
 async ionViewWillEnter(){
  this.terms='';
  // const tabBar = document.getElementById('myTabBar');
  // tabBar.style.display="none";
  
    const bid=await this.storage.get("BRANCH")

  
    
  this.staffArray=[];
 
  this.staffArray.push({user_id:'all',name:'All'})
  let url1=this.config.domain_url+'staff_viabranch';
  // let headers=await this.config.getHeader();
  // this.storage.ready().then(()=>{
    let headers=await this.config.getHeader();
    const utype=await this.storage.get("USER_TYPE")

      const uid=await this.storage.get("USER_ID")

    const cid=await this.storage.get("COMPANY_ID")

      let body={company_id:cid}
        this.http.post(url1,body,{headers}).subscribe((res:any)=>{
          console.log("arr:",res);
          res.data.forEach(element => {
            this.staffArray.push({user_id:element.user_id,name:element.name})
          });
          if(utype==3){
            this.staff_id=uid;
            this.staffArray.forEach(ele=>{
              // console.log("el:",ele.id,this.wingId);
              
              if(ele.user_id==uid){
                this.staff=ele.name;
              }
            })
          }else{
            this.staff='All';
            this.staff_id='all'
          }
        })
        
     
  this.getContent();
  (await this.config.getUserPermission()).subscribe((res: any) => {
    console.log('permissions:', res);
    let routes = res.user_routes;
    if (res.main_permissions.communication==1&&routes.includes('call_booking.create')) {

      this.create = true
    } else {

      this.create = false;
    }
    if (res.main_permissions.communication==1&&routes.includes('call_booking.edit')) {

      this.edit = true
    } else {

      this.edit = false;
    }
  })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu-booking']) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
async getContent(){
  this.callList=[];
  
    const bid=await this.storage.get("BRANCH")

  let url=this.config.domain_url+'staff_viabookings';
  
  let headers=await this.config.getHeader();
  let body={
    staff_id:this.staff_id,
    date:this.date_1
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("res:",res);
    this.callList=res.data;
    
  })

}
setstaff($event){
  // this.storage.ready().then(()=>{
  //   const bid=await this.storage.get("BRANCH")

  this.staffArray.forEach(element => {
    if(element.name==this.staff){
      this.staff_id=element.user_id;

    }
  });
  this.getContent();
//   this.callList=[];
//   let url=this.config.domain_url+'staff_viabookings';
//   let head=let headers=await this.config.getHeader();
//   let headers=await this.config.getHeader();
//   let body={
//     staff_id:this.staff_id,
//     date:this.date_1
//   }
//   this.http.post(url,body,{headers}).subscribe((res:any)=>{
//     console.log("res:",res);
//     this.callList=res.data;
    
//   })
//   console.log("staff:",this.staff,this.staff_id);
// })
//   })
}
onChange(ev){
console.log("cdate:",this.date);
this.date_1=this.date;
this.ionViewWillEnter();
}
 //  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}
getTimeZone(timezone){
  return moment().tz(timezone).format('hh:mm');
}



async showComment(item,ev){
  const popover = await this.popCntlr.create({
    component: CbCommentComponent,
    cssClass:'comments-pop',
    event:ev,
    backdropDismiss:true,
    componentProps:{
      data:item.comment
    },
   
    
    
  });
  return await popover.present();
}


async showInfo(ev,item){
  const popover = await this.popCntlr.create({
    component: CbInfoComponent,
    event:ev,
    backdropDismiss:true,
    componentProps:{
      data:item
    },
 
    
    
  });
  return await popover.present();
}



async showOptions(ev,item){
  const popover = await this.popCntlr.create({
    component: CbOptionsComponent,
    event:ev,
    backdropDismiss:true,
    componentProps:{
      data:item,
      cdate:this.date_1
    },
    
    
    
  });
  popover.onDidDismiss().then(()=>{
    this.ionViewWillEnter();
  })
  return await popover.present();
}



cancel(){
  // this.hide=false;
  this.terms='';
}

start(item){
  console.log("url:",item.link);
 
//  if(this.platform.is('android')){
//   const roomId =item.link.substr(item.link.lastIndexOf('/') + 1);

//   // const roomId = 'VqyaB3flP';
//   jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, false, function (data) {
//     if (data === "CONFERENCE_WILL_LEAVE") {
//         jitsiplugin.destroy(function (data) {
//             // call finished
//         }, function (err) {
//             console.log(err);
//         });
//     }
// }, function (err) {
//     console.log(err);
// });
//  }else {
  let options:InAppBrowserOptions ={
    location:'no',
    hideurlbar:'yes',
    zoom:'no'
  }
  
  const browser = this.iab.create(item.link,'_system',options);
  
//  }

}

async showPeople(ev,item){
  const modal = await this.modalCntl.create({
    component: CbMoreComponent,
    // event:ev,
    cssClass:'cbmorepop',
    backdropDismiss:true,
    componentProps:{
      data:item.family,
      fam:1,
      call:1
    },
    
    
    
  });
  return await modal.present();
}

async disableSlot(){

  this.router.navigate(['/cb-disabletimeslot']);
  // const popover = await this.modalCntl.create({
  //   component: CbDisabletimeslotComponent,
  //   cssClass:'morepop',
  //   backdropDismiss:true,
    
 
    
    
  // });
  // return await popover.present();
}
}
