import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CallbookingListPage } from './callbooking-list.page';

describe('CallbookingListPage', () => {
  let component: CallbookingListPage;
  let fixture: ComponentFixture<CallbookingListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallbookingListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CallbookingListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
