import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubfloderfilesPageRoutingModule } from './subfloderfiles-routing.module';

import { SubfloderfilesPage } from './subfloderfiles.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubfloderfilesPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [SubfloderfilesPage]
})
export class SubfloderfilesPageModule {}
