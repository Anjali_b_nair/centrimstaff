import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubfloderfilesPage } from './subfloderfiles.page';

describe('SubfloderfilesPage', () => {
  let component: SubfloderfilesPage;
  let fixture: ComponentFixture<SubfloderfilesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubfloderfilesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubfloderfilesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
