import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-subfloderfiles',
  templateUrl: './subfloderfiles.page.html',
  styleUrls: ['./subfloderfiles.page.scss'],
})
export class SubfloderfilesPage implements OnInit {
  res_id:any;
  fl_id:any;
  resource:any=[];
  title:any;
  res_title:any;
  hide:boolean=false;
  terms:any;
  subscription:Subscription;
  constructor(private route:ActivatedRoute,private http:HttpClient,private iab: InAppBrowser,
    private config:HttpConfigService,private router:Router,private platform:Platform,private storage:Storage,
    private dialog:SpinnerDialog,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    this.fl_id=this.route.snapshot.paramMap.get('fl_id');
    this.title=this.route.snapshot.paramMap.get('title');
    this.res_title=this.route.snapshot.paramMap.get('res_title');
    this.getContent();
    this.terms='';
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resources-details',{res_id:this.res_id,res_title:this.res_title}])
    });  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
  async getContent(){
    
      const data=await this.storage.get("TOKEN")
        let token=data;
        const bid=await this.storage.get("BRANCH")


          let branch=bid.toString();
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'resource/'+this.fl_id;
    // let headers=await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.resource=res.resource_files;
        
       
        
      },error=>{
        console.log(error);
        
      })
    
  }


  openDoc(item){
    if(item.url=="null"){
      // window.open(encodeURI(item.file),"_system","location=yes");
      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes'
      }
      if(item.file.includes('.pdf')){
        this.showpdf(item.file)
      }else{
      const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
    

      browser.on('loadstart').subscribe(() => {
    
        this.dialog.show();   
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
      }else{
        let options:InAppBrowserOptions ={
          location:'no',
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.url,'_blank',options);
      
      }


  }
  back(){
    this.router.navigate(['/resources-details',{res_id:this.res_id,res_title:this.res_title}])
  }
  cancel(){
    this.hide=false;
    this.terms='';
  }
  search(){
      this.hide=true;
      
    }
    async showpdf(file){
      const modal = await this.modalCntl.create({
        component: ShowpdfComponent,
        cssClass:'fullWidthModal',
        componentProps: {
          
          data:file,
          
           
        },
        
        
      });
      return await modal.present();
    }
}
