import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubfloderfilesPage } from './subfloderfiles.page';

const routes: Routes = [
  {
    path: '',
    component: SubfloderfilesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubfloderfilesPageRoutingModule {}
