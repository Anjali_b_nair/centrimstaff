import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-maintenance-count',
  templateUrl: './maintenance-count.page.html',
  styleUrls: ['./maintenance-count.page.scss'],
})
export class MaintenanceCountPage implements OnInit {
  request:any={};
  subscription:Subscription;
  type:any[]=[];
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.request={};
    
      const data=await this.storage.get("USER_ID")
      let headers=await this.config.getHeader();;
        let url=this.config.domain_url+'get_maintenance_request_technician?user_id='+data;
        // let id=data.toString();
       
        console.log("head:",url);
        // let body={user_id:}
        this.http.get(url,{headers}).subscribe((res:any)=>{
          
          this.request=res;
          this.type=res.type;
          console.log("main:",res);
        },error=>{
          console.log(error);
          
        })
      
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 
  }
  gotoDetails(){
    this.router.navigate(['/maintenance',{filter:1}])
  }
  gotoDetailsByStatus(id){
    this.router.navigate(['/maintenance',{filter:2,data:id}])
  }
  gotoDetailsByPriority(id){
    this.router.navigate(['/maintenance',{filter:3,data:id}])
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  createReq(){
    this.router.navigate(['/create-maintenance',{type:JSON.stringify(this.type)}])
  }
}
