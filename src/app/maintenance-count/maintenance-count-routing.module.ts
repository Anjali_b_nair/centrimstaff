import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceCountPage } from './maintenance-count.page';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceCountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceCountPageRoutingModule {}
