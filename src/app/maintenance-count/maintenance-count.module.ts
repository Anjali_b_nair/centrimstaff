import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceCountPageRoutingModule } from './maintenance-count-routing.module';

import { MaintenanceCountPage } from './maintenance-count.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceCountPageRoutingModule
  ],
  declarations: [MaintenanceCountPage]
})
export class MaintenanceCountPageModule {}
