import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningMenuListingPage } from './dining-menu-listing.page';

describe('DiningMenuListingPage', () => {
  let component: DiningMenuListingPage;
  let fixture: ComponentFixture<DiningMenuListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningMenuListingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningMenuListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
