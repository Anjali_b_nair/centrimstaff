import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningMenuListingPage } from './dining-menu-listing.page';

const routes: Routes = [
  {
    path: '',
    component: DiningMenuListingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningMenuListingPageRoutingModule {}
