import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { ChooseDateComponent } from '../components/dining/choose-date/choose-date.component';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { ViewMenuAdditionalComponent } from '../components/dining/view-menu-additional/view-menu-additional.component';
@Component({
  selector: 'app-dining-menu-listing',
  templateUrl: './dining-menu-listing.page.html',
  styleUrls: ['./dining-menu-listing.page.scss'],
})
export class DiningMenuListingPage implements OnInit {
subscription:Subscription;
menu:any;
date:any;
menuItems:any[]=[];
additionalItems:any[]=[];
additional_by_ser_time:any=[];
dates:any[]=[];
  currentIdx:any;
  constructor(private modalCntrl:ModalController,private router:Router,private platform:Platform,private loadingCtrl:LoadingController,
    private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private popCntrl:PopoverController) { }

  ngOnInit() {
  }
async chooseDate(){
  const modal = await this.modalCntrl.create({
    component: ChooseDateComponent,
    cssClass:'din-choosedate-modal',
    componentProps:{
      menu:this.menu
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if(dataReturned.data){
      this.date=dataReturned.data
      this.getList();
    }
  });
  return await modal.present();
}

async ionViewWillEnter(){
  let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    this.date=this.route.snapshot.paramMap.get('date');
    this.additional_by_ser_time=[];
    this.getList();
    let currentDate = moment().format('DD MMM YYYY');
    let startDate = moment(this.menu.start_date);
    let end = moment(this.menu.enddate);
    var start = startDate.clone();
    while (start.isSameOrBefore(end)) {
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1, 'days');
    }
    console.log('foot:', moment(this.date), moment(currentDate).clone(), moment(this.date).isSameOrAfter(moment(currentDate).clone()))
      let date = moment(this.date).format('DD MMM YYYY')
      this.currentIdx = this.dates.map(item => item).indexOf(date);
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    
      back(){
        this.router.navigate(['/dining-vm-select-date',{menu:JSON.stringify(this.menu)}])
      }

    async getList(){
      this.showLoading();
      this.additional_by_ser_time=[];
      let url=this.config.domain_url+'get_menu_details_bydate/'+this.menu.id+'/'+moment(this.date).format('yyyy-MM-DD');
    console.log(url);
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.menuItems=res.data.menu_details;
      this.additionalItems=res.data.additionalitems;
      this.menuItems.forEach(el=>{
        this.setAdditionalitem(el.serving_time_id);
      })
      this.dismissLoader();
    })
    }
    showDetails(i,st,st_id,f){
      if(i.subitem==1){
        this.router.navigate(['/din-continental-breakfast',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(i),ser_time:st,ser_time_id:st_id}])
      }else{
      this.router.navigate(['/dining-menu-fi-details',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(i),ser_time:st,ser_time_id:st_id,flag:f}])
      }
    }
    async showLoading() {
     
      const loading = await this.loadingCtrl.create({
        cssClass: 'dining-loading',
        // message: 'Please wait...',
        // message: '<ion-img src="assets/loader.gif"></ion-img>',
        spinner: null,
        // duration: 3000
      });
      return await loading.present();
    }
    
    async dismissLoader() {
     
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }

    showOption(menu){
      let op=false;
      let item=[];
      this.additionalItems.forEach(x=>{
        x.servingtime.map(y=>{
          if(y.serving_time_id==menu.serving_time_id){
            op=true;
            
          }
        })
      })
      // this.additional_by_ser_time.push({item:item,ser_time:menu.serving_time_id,show:false});
      // console.log('add:',this.additional_by_ser_time);
      
      return op;
    }
    setAdditionalitem(id){
      console.log('setadditionalitem:',id)
      let item=[];
      this.additionalItems.forEach(element => {
        element.servingtime.map(x => {
          if(id==x.servingtime[0].id){
            item.push(element.item)
            
            // this.servingTime=x.servingtime[0].servingtime
          }
        })
        
      });
      this.additional_by_ser_time.push({item:item,ser_time:id,show:false});
      console.log('add:',this.additional_by_ser_time);
    }
    async options(ev,id) {
     
      
      let item;
      this.additional_by_ser_time.forEach(el=>{
        if(el.ser_time==id){
          item=el
        }
      })
  
      const popover = await this.popCntrl.create({
        component: ViewMenuAdditionalComponent,
        id:'menu_options',
        event: ev,
        backdropDismiss: true,
        cssClass: 'dining-more-options-popover',
        componentProps: {
          menu:item,
         
          // sel_additional_item:this.sel_additional_item
        },
      });
        popover.onDidDismiss().then((dataReturned) => {
          // if(dataReturned.data==1){
            
            
          // }else if(dataReturned.data==2){
            if(dataReturned.data){
           const idx= this.additional_by_ser_time.map(x=>x.ser_time==id).indexOf(id);
           this.additional_by_ser_time[idx]=dataReturned.role;
           console.log('change:',this.additional_by_ser_time)
         
          }
  
        });
  
      
      return await popover.present();
    }


    previous() {
      if (this.currentIdx == 0) {
        console.log('reached first item')
      } else {
        console.log('curr:', this.currentIdx, this.dates[this.currentIdx]);
        this.currentIdx--;
        console.log('changedcur:', this.currentIdx, this.dates[this.currentIdx])
        this.date = this.dates[this.currentIdx];
        this.getList();
        // this.showLoading();
        
  
        let currentDate = moment().format('DD MMM YYYY');
        
      }
    }
    next() {
      if (this.currentIdx == (this.dates.length - 1)) {
        console.log('reached end')
      } else {
        this.currentIdx++;
        this.date = this.dates[this.currentIdx];
        this.getList();
        
  
        let currentDate = moment().format('DD MMM YYYY');
        
      }
    }

    checkNoItems(menu){
      let no_items=true;
      if(menu.coloumns.length==0){
        no_items=false
      }else if(menu.coloumns.length){
          menu.coloumns.map(x=>{
            if(!x.items||!x.items.length){
              no_items=false;
            }
          })
      }else{
        no_items=true
      }
      return no_items;
    }
}
