import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningMenuListingPageRoutingModule } from './dining-menu-listing-routing.module';

import { DiningMenuListingPage } from './dining-menu-listing.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningMenuListingPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningMenuListingPage]
})
export class DiningMenuListingPageModule {}
