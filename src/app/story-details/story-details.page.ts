import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { AlertController, IonContent, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { StoryViewedListComponent } from '../components/story-viewed-list/story-viewed-list.component';
import { TaggedConsumerInfoComponent } from '../components/tagged-consumer-info/tagged-consumer-info.component';
import { ViewImageComponent } from '../components/view-image/view-image.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { ShowpdfComponent } from '../components/showpdf/showpdf.component';
import { SendMailWarningComponent } from '../components/send-mail-warning/send-mail-warning.component';
@Component({
  selector: 'app-story-details',
  templateUrl: './story-details.page.html',
  styleUrls: ['./story-details.page.scss'],
})
export class StoryDetailsPage implements OnInit {
  @ViewChild('content', { static: false }) content: IonContent;
 
  @ViewChild('commentSection' , { static: false }) comment: any;
  // comment:HTMLElement;
  id:any;
  token:string;
  user_id:any;
  user_img:string;
  story:any={};
  creator:any={};
  story_time:any;
  commentText:string;
  comments:any=[];
  images:any=[];
  videos:any=[];
  category:any=[];
  tagged:any=[];
  date:number;
  current:number;
  com_time:number;
  time:string;
  name:string;
  p_name:string;
  doc:any=[];
  status:any;
  subscription:Subscription;
  // contentStyle ={top:'75px'};
  key:boolean;
  media:any=[];
  start:any=0;
  end:any=5;
  flag:any;
  cid:any;
  from:any;

  canEdit:boolean=true;
  edit:boolean=false;
  delete_story:boolean=false;
  publish:boolean=false;
  send_email:any=0;
  constructor(private keyboard:Keyboard,private route:ActivatedRoute,public http:HttpClient,private storage:Storage,
    private config:HttpConfigService,public popoverController:PopoverController,private platform:Platform,
    private router:Router,public element:ElementRef,private alertController:AlertController,private iab:InAppBrowser,
    private toastCntlr:ToastController,private nav:NavController,private modalCntrl:ModalController,
    private dialog:SpinnerDialog) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display="none";
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.id = this.route.snapshot.paramMap.get('post_id');
    if(this.flag==3){
      this.cid=this.route.snapshot.paramMap.get('cid');
      this.from=this.route.snapshot.paramMap.get('from');
    }
    this.current=(new Date()).getTime();
    this.comments=[];
    this.media=[];
    this.doc=[];
    
      
      const tok=await this.storage.get("TOKEN")
        this.token=tok;
     
      const data=await this.storage.get("USER_ID")

        this.user_id=data;
      
      const pic=await this.storage.get("PRO_IMG")
        this.user_img=pic;
    
      const name=await this.storage.get("NAME")
        this.name=name;
        this.p_name=this.name.substring(0,1);
     
    this.getContent();
    this.loadComments();
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      if (res.main_permissions.life_style==1&&routes.includes('story.edit')) {

        this.edit = true
      } else {

        this.edit = false;
      }
      if (res.main_permissions.life_style==1&&routes.includes('story.destroy')) {

        this.delete_story = true
      } else {

        this.delete_story = false;
      }
      if (res.main_permissions.life_style==1&&routes.includes('story.publish')) {
  
        this.publish = true
      } else {

        this.publish = false;
       
      }
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
      if(this.flag==1){
        this.router.navigate(['/feeds'])
      } else if(this.flag==2){ 
      this.router.navigate(['/stories']) ;
      }else{
        this.router.navigate(['/resident-profile',{id:this.cid,flag:this.from}])
      }
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
// like the post
  
    async like(id,liked){
      console.log("like invoked");
      
      if(liked==0){
        this.status=0;
      }else{
        this.status=1;
      }
      console.log("likesta:",this.status);
      
      
      
        
          const bid=await this.storage.get("BRANCH")

        
        
          const data=await this.storage.get("USER_ID")

            
            this.user_id=data;
            
       
      let story_id=id;
      let body={
        post_id: story_id,
        user_id: this.user_id,
        action_type: '1',
        status:this.status
      };
      console.log("body:",body);
      let url=this.config.domain_url+'update_post_action';
      let headers=await this.config.getHeader();;
      this.http.post(url,body,{headers}).subscribe(data=>{
        console.log(data);
        this.story.liked=!this.story.liked
        this.story.liked ? this.story.like_count++ : this.story.like_count--;
      },error=>{
        console.log(error);
        
      })
    
  }


// fetch story details
async getContent(){
  this.doc=[];
 
      
    
      const type=await this.storage.get("USER_TYPE")
    
      const bid=await this.storage.get("BRANCH")

    
    const data=await this.storage.get("USER_ID")

    
      this.user_id=data;
  let url=this.config.domain_url+'storydetails';
  let body={
    post_id:this.id,
    user_id:this.user_id

  }
  let headers=await this.config.getHeader();;
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      if(type==1||type==2){
      }else{
        if(data!==res.story.creator.user_id){
        this.canEdit=false
      }
      }
      this.story=res.story;
      this.creator=res.story.creator;
      this.images=res.story.images;
      this.videos=res.story.videos;
      this.category=res.story.category;
      this.status=res.story.liked;
      this.tagged=res.story.tagged_users;
      


      if(this.images.length==0){
        this.videos.forEach(element => {
          
          let item={img:element.thumbnail,vid:1};
          this.media.push(item);
        });
      }else if(this.videos.length==0){
        this.images.forEach(element => {
          let item={img:element.post_attachment,vid:0};
          this.media.push(item);
        });
      }else{
        this.images.forEach(element => {
          let item={img:element.post_attachment,vid:0};
          this.media.push(item);
        });
        this.videos.forEach(element => {
          
          let item={img:element.thumbnail,vid:1};
          this.media.push(item);
        });
      }






      var mon = new Array();
      mon[0] = "Jan";
      mon[1] = "Feb";
      mon[2] = "Mar";
      mon[3] = "Apr";
      mon[4] = "May";
      mon[5] = "Jun";
      mon[6] = "Jul";
      mon[7] = "Aug";
      mon[8] = "Sep";
      mon[9] = "Oct";
      mon[10] = "Nov";
      mon[11] = "Dec";

    //   let t=new Date(res.story.activate_date.replace(' ', 'T'));
    //   let d=mon[t.getMonth()]+' '+t.getDate()+' '+t.getFullYear();

    //   var hours = t.getHours();
    //   let minutes = t.getMinutes();
    //   var ampm = hours >= 12 ? 'pm' : 'am';
    //   hours = hours % 12;
    //   hours = hours ? hours : 12; // the hour '0' should be '12'
    //  var hour= hours < 10 ? '0'+hours:hours
    //   var minute = minutes < 10 ? '0'+minutes : minutes;
    //   this.story_time= d+', '+hour + ':' + minute + ' ' + ampm;
    this.story_time=moment(res.story.activate_date).format('MMM DD YYYY, hh:mm a')
      console.log(this.story_time);
      
      res.story.attachments.forEach(el=>{
        let attch_name=el.post_attachment.substr(el.post_attachment.lastIndexOf('_') + 1);
        let ext=el.post_attachment.substr(el.post_attachment.lastIndexOf('.') + 1);
        this.doc.push({'title':attch_name,'post_attachment':el.post_attachment,'ext':ext});
        console.log("ext:",ext)
      })
      console.log("date_1:",this.doc);
      
      
    },error=>{
      console.log(error);
      
    })
  
}

// fetch comments

async loadComments(){
  this.comments=[];
  
    const bid=await this.storage.get("BRANCH")

  let body={
    post_id: this.id
  }
  let url=this.config.domain_url+'comments';
  let headers=await this.config.getHeader();;
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      // this.comments=res.comments;
      res.comments.forEach(ele=>{
        // let x=this.current.getTime();
        this.date=(new Date(ele.updated_at)).getTime();
        let t=this.current-this.date;
        let min=Math.floor(t/ (60*1000));
        let hr = Math.floor(t/ 3600 / 1000); // round to nearest second
        let days= Math.floor(t/ (60*60*24*1000));
        let month=Math.floor(t/ (60*60*24*1000*31));
        let yr=Math.floor(t/ (60*60*24*1000*31*12))
        // console.log("time:",this.com_time);
        if(month>12){
          this.time=yr+ ' yrs';
        }
        else if(days>31){
          this.time=month+ ' m';
        }
        else if(hr>24){
          
          this.time=days+ ' d';
        }else if(min>60){
          this.time=hr+' hrs ';
        }
        else if(min<=60 && min>1){
          this.time=min+' min';
        }
        else{
          this.time="Now";
        }
        console.log("t:",this.time);
        
        this.comments.push({'content':ele.content,'user':ele.user,'updated_at':ele.updated_at,'time':this.time,'id':ele.id});
        
      })
      
      console.log("res:",this.comments);
    },error=>{
      console.log(error);
      
    })
  
}

// post a comment

async postComment(id){
  console.log("com:",this.commentText)
  
  
      
    
      const bid=await this.storage.get("BRANCH")

    
    
      const data=await this.storage.get("USER_ID")

      
        this.user_id=data;
        if(this.commentText==undefined|| this.commentText==""){
          console.log("cccc:",this.commentText);
          
        }else{
    
  let story_id=id;
    let body={
      post_id: story_id,
      user_id: this.user_id,
      action_type: '2',
      comment: this.commentText
    };
    console.log("body:",body);
    
    let url=this.config.domain_url+'create_post_action';
    let headers=await this.config.getHeader();;
    this.http.post(url,body,{headers}).subscribe(data=>{
      console.log(data);
      this.story.comment_count++;
      this.Toast('Comment added successfully')
      this.loadComments();
      // this.comment=document.getElementById('commentSection');
      this.content.scrollToPoint(0, this.comment.nativeElement.offsetTop, 500);
    },error=>{
      console.log(error);
      
    });
    this.commentText="";
    
  }


}


async showImages(ev: any) {

  const popover = await this.popoverController.create({
    component: ViewImageComponent,
    event: ev,
    componentProps:{
      data:this.images,
      video:this.videos
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}


// open document
openDoc(item){


  // window.open(encodeURI(item.post_attachment),"_system","location=yes");


  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  zoom:'yes'
  }
  if(item.post_attachment.includes('.pdf')){
    this.showpdf(item.post_attachment)
  }else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.post_attachment+'&embedded=true','_blank',options);
  browser.on('loadstart').subscribe(() => {
    console.log('start');
    this.dialog.show();   
   
  }, err => {
    console.log(err);
    
    this.dialog.hide();
  })

  browser.on('loadstop').subscribe(()=>{
    console.log('stop');
    
    this.dialog.hide();;
  }, err =>{
    this.dialog.hide();
  })

  browser.on('loaderror').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
}
}
// refresh the page
doRefresh(event) {
  this.ionViewWillEnter();

  setTimeout(() => {
    
    event.target.complete();
  }, 2000);
}




getMIMEtype(extn){
  let ext=extn.toLowerCase();
  let MIMETypes={
    'txt' :'text/plain',
    'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'doc' : 'application/msword',
    'pdf' : 'application/pdf',
    'jpg' : 'image/jpeg',
    'bmp' : 'image/bmp',
    'png' : 'image/png',
    'xls' : 'application/vnd.ms-excel',
    'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'rtf' : 'application/rtf',
    'ppt' : 'application/vnd.ms-powerpoint',
    'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  }
  return MIMETypes[ext];
}



async deletePop(id,idx){
  const alert = await this.alertController.create({
    header: 'Confirm delete?',
    mode:'ios',
    message: 'Do you want to delete this comment?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.deleteComment(id,idx);
        }
      }
    ]
  });

  await alert.present();
}
async deleteComment(id,idx){

let url=this.config.domain_url+'comment/'+id;
    let headers=await this.config.getHeader();
    this.http.delete(url,{headers}).subscribe(data=>{
      console.log(data);
      this.Toast('Comment deleted successfully')
      this.comments.splice(idx,1);
      // this.loadComments();
      this.story.comment_count--;

    },error=>{
      console.log(error);
    });
   
}


viewMore(){
  this.end +=5;
}

back(){
  if(this.flag==1){
    this.router.navigate(['/feeds'],{replaceUrl: true})
  } else if(this.flag==2){ 
  this.router.navigate(['/stories'],{replaceUrl: true}) ;
  }else{
      this.router.navigate(['/resident-profile',{id:this.cid,flag:this.from}],{replaceUrl: true})
  }
}

async delete(){
  let headers=await this.config.getHeader();
  const alert = await this.alertController.create({
    mode:'ios',
    header: 'Delete story?',
    message: 'Are you sure you want to delete this story?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Delete',
        handler: () => {
        this.http.delete(this.config.domain_url+'story/'+this.id,{headers}).subscribe((res:any)=>{
          console.log('delete:',res);
          if(res.status=="success"){
            this.Toast('Story deleted successfully.')
          this.back();
          }else{
            this.Toast('Something went wrong.Please try again later.');
          }
        })
          
        }
      }
    ]
  });

  await alert.present();
}
async Toast(mes){
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}
editStory(){
  this.router.navigate(['/edit-story',{id:this.id,flag:this.flag,description:this.story.description,cid:this.cid,from:this.from}])
}

async viewList() {
  const modal = await this.modalCntrl.create({
    component: StoryViewedListComponent,
    componentProps: {
      data:this.id
    }
  });
  
  return await modal.present();
}
async taggedCon() {
  const modal = await this.modalCntrl.create({
    component: TaggedConsumerInfoComponent,
    componentProps: {
      data:this.tagged
    }
  });
  
  return await modal.present();
}
async showpdf(file){
  const modal = await this.modalCntrl.create({
    component: ShowpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}

async approveAlert(){
  const alert = await this.alertController.create({
    mode:'ios',
    header: 'Approve story?',
    message: 'Are you sure to approve this story?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Continue',
        handler: () => {
        // this.approve();
          this.sendMailWarning()
        }
      }
    ]
  });

  await alert.present();
}
async sendMailWarning(){
  const modal = await this.modalCntrl.create({
    component: SendMailWarningComponent,
   cssClass:'full-width-modal',
    componentProps: {
      
      
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data == 1) {
      this.send_email=1
    }else{
      this.send_email=0
    }
    this.approve();
  });
  return await modal.present();
}
async approve(){
  const bid=await this.storage.get("BRANCH")

    
    
  const data=await this.storage.get("USER_ID")

  
    
    


let body={
  post_id: this.id,
  updated_by: data,
  status: 1,
  send_email:this.send_email
};
console.log("body:",body);

let url=this.config.domain_url+'update_story_status';
let headers=await this.config.getHeader();;
this.http.post(url,body,{headers}).subscribe(data=>{
  console.log(data);
  this.story.status=1;
  this.Toast('Approved successfully.')
},error=>{
  console.log(error);
  
});
}
}
