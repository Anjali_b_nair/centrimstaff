import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StoryDetailsPageRoutingModule } from './story-details-routing.module';

import { StoryDetailsPage } from './story-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoryDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [StoryDetailsPage]
})
export class StoryDetailsPageModule {}
