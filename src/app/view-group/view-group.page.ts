import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-view-group',
  templateUrl: './view-group.page.html',
  styleUrls: ['./view-group.page.scss'],
})
export class ViewGroupPage implements OnInit {
group:any[]=[];
subscription:Subscription;
flag:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private router:Router,
    private platform:Platform,private storage:Storage,private route:ActivatedRoute) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.group=[];
 
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
    this.flag = this.route.snapshot.paramMap.get('flag');

    
  let url=this.config.domain_url+'all_groups';
  let headers=await this.config.getHeader();
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.group=res.data
    
  })

  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
}); 

  }
  back(){
    if(this.flag==1){
      this.router.navigate(['/rv-res-list']) ;
    }else{
      this.router.navigate(['/consumers',{flag:1}]) ;
    }
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }



}
