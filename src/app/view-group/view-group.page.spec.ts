import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewGroupPage } from './view-group.page';

describe('ViewGroupPage', () => {
  let component: ViewGroupPage;
  let fixture: ComponentFixture<ViewGroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGroupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewGroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
