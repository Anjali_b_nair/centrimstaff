import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-edit-petty-cash',
  templateUrl: './edit-petty-cash.page.html',
  styleUrls: ['./edit-petty-cash.page.scss'],
})
export class EditPettyCashPage implements OnInit {
  img:any;
  name:any;
  id:any;
  room:any;
  minDate: string = new Date().toISOString();
  date:any;
  amount:any;
  type:any;
  note:any;
  consumer:any={};
  flag:any;
  petty_id:any;
  subscription:Subscription;
  wing:any;
  constructor(private router:Router,private storage:Storage,private http:HttpClient,private route:ActivatedRoute,
    private config:HttpConfigService,private objService:GetobjectService,private toastCntlr:ToastController,
    private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
      
    this.consumer=this.objService.getExtras();
    this.id=this.consumer.id;
    this.img=this.consumer.user.profile_pic;
    this.name=this.consumer.user.name;
    this.room=this.consumer.room;
    this.wing=this.consumer.wing.name;
    this.type=this.route.snapshot.paramMap.get('type');
    let a=this.route.snapshot.paramMap.get('amount')
    this.amount=a.substring(a.lastIndexOf('-')+1);
    this.note=this.route.snapshot.paramMap.get('note');
    this.date=this.route.snapshot.paramMap.get('date');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.petty_id=this.route.snapshot.paramMap.get('id');

    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/petty-cash',{flag:this.flag,id:this.id}])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }

selectType(type){
  this.type=type;
  console.log("type:",type);
  
}
async save(){
  if(this.type==undefined || this.date==undefined || this.note==undefined || this.note=='' || this.amount==undefined || this.amount==''){
      this.presentAlert("All fields required.")
  }else if(!(/^\d*\.?\d*$/.test(this.amount))){
    this.presentAlert("The amount format is invalid.")
  }else if(/^\d+$/.test(this.note)){
    this.presentAlert("Please specify the purpose correctly.")
  }else{

    const cid=await this.storage.get("COMPANY_ID")

      const uid=await this.storage.get("USER_ID")

        this.date=this.date.slice(0,19);
        this.date=this.date.toString().replace('T',' ');
        let amount;
        if(this.type=='Debit'){
         amount='-'+this.amount;
        }else{
          amount=this.amount
        }
        let url=this.config.domain_url+'update_pettycash';
        let body={
          user_id:this.id,
          company_id:cid,
          type:this.type,
          date:this.date,
          note:this.note,
          created_by:uid,
          amount:amount,
          pettycash_id:this.petty_id
        }
        console.log("body:",body);
        let headers=await this.config.getHeader();;
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.date='';
          this.note='';
          this.amount='';
          this.router.navigate(['/petty-cash',{flag:this.flag,id:this.id}]);
        })
        
        
        
    
  }
}


async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}
}
