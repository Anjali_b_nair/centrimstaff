import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPettyCashPageRoutingModule } from './edit-petty-cash-routing.module';

import { EditPettyCashPage } from './edit-petty-cash.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPettyCashPageRoutingModule
  ],
  declarations: [EditPettyCashPage]
})
export class EditPettyCashPageModule {}
