import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditPettyCashPage } from './edit-petty-cash.page';

describe('EditPettyCashPage', () => {
  let component: EditPettyCashPage;
  let fixture: ComponentFixture<EditPettyCashPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPettyCashPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditPettyCashPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
