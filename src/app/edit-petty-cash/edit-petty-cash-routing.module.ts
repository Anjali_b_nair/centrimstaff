import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPettyCashPage } from './edit-petty-cash.page';

const routes: Routes = [
  {
    path: '',
    component: EditPettyCashPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPettyCashPageRoutingModule {}
