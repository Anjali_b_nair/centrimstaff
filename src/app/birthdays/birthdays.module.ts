import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BirthdaysPageRoutingModule } from './birthdays-routing.module';

import { BirthdaysPage } from './birthdays.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BirthdaysPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [BirthdaysPage]
})
export class BirthdaysPageModule {}
