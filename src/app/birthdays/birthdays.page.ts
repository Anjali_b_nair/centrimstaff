import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { BdyfilterComponent } from '../components/bdyfilter/bdyfilter.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-birthdays',
  templateUrl: './birthdays.page.html',
  styleUrls: ['./birthdays.page.scss'],
})
export class BirthdaysPage implements OnInit {
today:any[]=[];
tomorrow:any[]=[];
wings:any[]=[];
terms:any;
thisMonth:any[]=[];
nextMonth:any[]=[];
thisWeek:any[]=[];
bdy:any[]=[];
bdyFilter=4;
searchItem:boolean=false;
subscription:Subscription;
year:any;
current_year:any;
current_month:any;
all_list:any[]=[];
yearView:boolean=true;
message:any='This Month';
rv:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private popCntl:PopoverController,private router:Router,private platform:Platform,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
 async ionViewWillEnter(){
   this.terms='';
   this.current_year=new Date().getFullYear();
   this.year=this.current_year;
   this.current_month=new Date().getMonth();
   this.all_list=[];
 
    this.showLoading();
    const bid=await this.storage.get("BRANCH")
    this.rv=await this.storage.get('RVSETTINGS');
      let url=this.config.domain_url+'birthday_filter';
       let headers=await this.config.getHeader();
       this.http.get(url,{headers}).subscribe((res:any)=>{
         console.log("fil:",res,this.current_month,url,this.rv);
        this.today=res.data.today;
        this.tomorrow=res.data.tomorrow.filter(x=>x.do_not_share_dob==1);
        this.thisWeek=res.data.this_week.filter(x=>x.do_not_share_dob==1).sort(this.compare);
        this.thisMonth=res.data.this_month.filter(x=>x.do_not_share_dob==1).sort(this.compare);
        // this.thisMonth=res.data.month.filter(x=>x.do_not_share_dob==1).sort(this.compare);
        this.nextMonth=res.data.next_month.filter(x=>x.do_not_share_dob==1).sort(this.compare);
       
          this.all_list.push({month:'January',bdy:res.all_list[1].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'February',bdy:res.all_list[2].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'March',bdy:res.all_list[3].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'April',bdy:res.all_list[4].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'May',bdy:res.all_list[5].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'June',bdy:res.all_list[6].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'July',bdy:res.all_list[7].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'August',bdy:res.all_list[8].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'September',bdy:res.all_list[9].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'October',bdy:res.all_list[10].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'November',bdy:res.all_list[11].filter(x=>x.do_not_share_dob==1).sort(this.compare)});
          this.all_list.push({month:'December',bdy:res.all_list[12].filter(x=>x.do_not_share_dob==1).sort(this.compare)});

        // }
        console.log("all:",this.all_list);
        this.dismissLoader();
       })
       
       
     
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/menu'],{replaceUrl:true})
   
    
      }); 
      
      
}
back(){
  this.router.navigate(['/menu'],{replaceUrl:true})
}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}
cancel(){
  this.terms='';
  this.searchItem=false;
}

async filter(ev){
  const popover = await this.popCntl.create({
    component: BdyfilterComponent,
    event: ev,
    backdropDismiss:true,
    cssClass:'filterpop'
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data != null) {
      this.bdyFilter = dataReturned.data;
      this.year=this.current_year;
      this.yearView=false;
      if(this.bdyFilter==1){
        this.bdy=this.today;
        this.message='Today'
      }else if(this.bdyFilter==2){
        this.bdy=this.tomorrow;
        this.message='Tomorrow'
      }else if(this.bdyFilter==3){
        this.bdy=this.thisWeek;
        this.message='This Week'
      }else if(this.bdyFilter==4){
        this.bdy=this.thisMonth;
        this.message='This Month'
      }else if(this.bdyFilter==5){
        this.bdy=this.nextMonth;
        this.message='Next Month';
        if(this.current_month==11){
        this.year=this.current_year+1;
        }
      }
      else if(this.bdyFilter==6){
        this.bdy=[];
        this.yearView=true
      }
      
    }
  });
  return await popover.present();
}
getDOB(item){
  return moment(item).format('MMMM DD');
}

compare(i,j){
  let c=i.dob;
  let d= j.dob
  let a=new Date(c).getDate() - new Date(d).getDate();
  return a;
}

showProfile(item){
  if(this.rv==1){
    this.router.navigate(['/rv-resident-profile-landing',{id:item.id,flag:2}])
  }else{
  this.router.navigate(['/resident-profile',{id:item.id,flag:3}],{replaceUrl:true});
  }
}
changeYear(i){
  this.yearView=true;
  this.message='This Month';
  // this.bdyFilter=4;
  if(i==1){
    this.year=this.current_year;
    // this.bdy=this.thisMonth
    this.bdy=[];
  }else{
    this.year=this.current_year+1;
    this.bdy=[]
  }
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
calculateAge(date){
  var years = moment(). diff(date, 'years');
  return years;
}
}
