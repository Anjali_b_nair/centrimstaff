import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ModalController, ToastController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { RvAssignedResidentComponent } from '../components/rv-assigned-resident/rv-assigned-resident.component';
import { RvAssignedStaffComponent } from '../components/rv-assigned-staff/rv-assigned-staff.component';
import { RvTaskFilterComponent } from '../components/rv-task-filter/rv-task-filter.component';
import { RvTaskOptionsComponent } from '../components/rv-task-options/rv-task-options.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-rv-tasks',
  templateUrl: './rv-tasks.page.html',
  styleUrls: ['./rv-tasks.page.scss'],
})
export class RvTasksPage implements OnInit {

 
offset:any=0;
tasks:any=[];
status:any='0';
priority:any='3';
sdate:any;
edate:any;

terms: any;
subscription:Subscription;
date_filter:any='7';
assignedTo:any=[];
staff:any;
companian:any=[];
task_create:boolean=false;
count:any;
filterStatus:any=1;
tz:any;
resident:any;
resident_name:any;
  constructor(private router:Router,private platform:Platform,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,private modalCntl:ModalController,
    private toastCtlr:ToastController,private popCntl:PopoverController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.tz = await this.storage.get("TIMEZONE");
    this.edate=moment().format('YYYY-MM-DD');
    this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD');
    this.getTaskList(false,'');
    this.filterStatus=1;
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
     
      if (res.main_permissions.rv==1&&routes.includes('task.create')) {

        this.task_create = true
      } else {

        this.task_create = false;
      }
     
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.back();
      
  }); 
  
  }

  back(){
    this.router.navigate(['/menu'],{replaceUrl:true})
  }

  async getTaskList(isFirstLoad, event){
    
    const uid = await this.storage.get("USER_ID");;
    let url=this.config.domain_url+'filter_task';
    let headers=await this.config.getHeader();
    let body;
    body={
     
      logged_user_id:uid,
      take:20,
      skip:this.offset
    }
    if(this.date_filter!=='7'){
      body.sdate=this.sdate;
      body.edate=this.edate;
    }
    if(this.status!=='5'&&this.status!=='6'){
      body.status=this.status
    }
    if(this.priority!=='3'){
      body.priority=parseInt(this.priority)
    }

    if(this.assignedTo.length){
      body.staff=this.assignedTo[0]
    }

    if(this.terms){
      body.search=this.terms
     
      
    }

    if(this.filterStatus==4){
      body.type=1
    }
    if(this.resident){
      body.resident_user_id=this.resident
    }
    console.log('body:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
   
      console.log('res:',res);
      this.count={
        total:res.counts.total_count,
        pending:res.counts.pending_count,
        ip:res.counts.inprogress_count,
        overdue:res.counts.overdue_count,
        completed:res.counts.completed_count
      }
     
    for (let i in res.data) {
       if(this.status!=='6'){
        this.tasks.push({task:res.data[i],expanded:false});
       }else{
        if(this.overdue(res.data[i].due_date)&&res.data[i].status!==2){
          this.tasks.push({task:res.data[i],expanded:false});
        }
       }
  
      }
      
      if(this.tasks&&this.tasks.length)
      this.tasks[0].expanded=true;
      console.log('tasks:',this.tasks);
     
      if (isFirstLoad)
      event.target.complete();
  
    this.offset=this.offset+20;
      
     },error=>{
       console.log(error);
      
     })
  }

  doInfinite(event) {
    this.getTaskList(true, event)
    }

  createTask(){
    
    this.router.navigate(['/rv-create-task',{flag:3}])
  }

  async taskFilter(){
    const modal = await this.modalCntl.create({
      component: RvTaskFilterComponent,
      cssClass:'full-width-modal',
      componentProps: {
        
       date_filter:this.date_filter,
       priority:this.priority,
       status:this.status,
       sdate:this.sdate,
       edate:this.edate,
       assignedTo:this.assignedTo,
       staff:this.staff,
       flag:1,
       resident:this.resident,
       res_name:this.resident_name
         
      },
      
      
    });
    modal.onDidDismiss().then((dataReturned)=>{
      if(dataReturned.data){
        this.filterStatus=1;
        this.date_filter=dataReturned.data.date_filter;
        this.priority=dataReturned.data.priority;
        this.status=dataReturned.data.status;
        this.sdate=dataReturned.data.sdate;
        this.edate=dataReturned.data.edate;
        this.assignedTo=dataReturned.data.assignedTo;
        this.staff=dataReturned.data.staff;
        this.resident=dataReturned.data.resident;
        this.resident_name=dataReturned.data.res_name;
        this.offset=0;
        this.tasks=[];
        this.getTaskList(false,'');
      }
      
    })
    return await modal.present();
  }
  changeStatus(ev){
   
    if(ev.detail.value==5){
      this.status=undefined;
    }else{
    this.status=ev.detail.value
    }
    this.tasks=[];
    this.offset=0;
    this.getTaskList(false,'');
  }
  overdue(date){
    // if(new Date(date).getTime()<new Date(moment().add(1,'days').format()).getTime()){
    //   return true;
    // }else{
    //   return false;
    // }
    if(moment(date,'YYYY-MM-DD').isBefore(moment(),'day')){
      return true;
    }else{
      return false;
    }
  }

  expandItem(item){
   
          if (item.expanded) {
            item.expanded = false;
          } else {
           
            this.tasks.map(listItem => {
              if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                
              } else {
                listItem.expanded = false;
              }
              return listItem;
            });
          
          }
  }

  async changeTaskStatus(ev,item){
    const bid = await this.storage.get("BRANCH");
    const cid = await this.storage.get("COMPANY_ID");
    const uid = await this.storage.get("USER_ID");;
    let url=this.config.domain_url+'update_task_status';
    let headers=await this.config.getHeader();
    let body;
    body={
     task_id:item.task.id,
     status:ev.detail.value,
     completed_at:moment().format('YYYY-MM-DD HH:mm:ss'),
     completed_by:uid

    }
  
   console.log('stat:',body);
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      this.offset=0;
      this.tasks=[];
    this.getTaskList(false,'');
      this.presentAlert('Status updated successfully.')
    })
  }
  async presentAlert(mes) {
    const alert = await this.toastCtlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  cancel() {
   
    this.terms = undefined;
    this.offset=0;
    this.tasks=[];
    
  this.getTaskList(false,'');
  }

  async search(){
    this.offset=0;
    this.tasks=[];
    
  this.getTaskList(false,'');
  }

  async options(ev,item){
    const popover = await this.popCntl.create({
      component:RvTaskOptionsComponent,
      event:ev,
      backdropDismiss:true,
      cssClass:'task-mo-popover',
      componentProps:{
        data:item,
        companian:this.companian,
        flag:3
      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned);
      if(dataReturned.data!=undefined||dataReturned.data!=null){
     
     this.offset=0;
     this.tasks=[];
     this.getTaskList(false,'');
      }
   
   
      
    });
    return await popover.present();
  }

  ShowContent(a,b,i){
    var x = document.getElementById(a+i);
    var y = document.getElementById(b+i);
          x.style.display = 'none';
          y.style.display = 'block';
  }

  async assigned(item){
    const popover = await this.modalCntl.create({
      component:RvAssignedStaffComponent,
      cssClass:'cbmorepop',
    backdropDismiss:true,
      componentProps:{
        data:item,
       

      },
      
      
      
    });
    
   
   
      
  
    return await popover.present();
  }

  async filters(i){
    this.edate=moment().format('YYYY-MM-DD');
    this.sdate=moment().subtract(31,'days').format('YYYY-MM-DD');
    this.filterStatus=i;
    if(i==1){
      this.status='0';
      this.priority='3';
      this.assignedTo=[];
      this.date_filter='7';
      this.staff=null;
      this.offset=0;
      this.tasks=[];
      this.getTaskList(false,'');
    }else if(i==2){
      this.edate=moment().format('YYYY-MM-DD');
      this.sdate=moment().format('YYYY-MM-DD');
      this.date_filter='0'
      this.offset=0;
      this.assignedTo=[];
      this.tasks=[];
      this.getTaskList(false,'');
    }else if(i==3){
      const uid = await this.storage.get("USER_ID");;
      this.assignedTo=[uid];
      this.staff=await this.storage.get('NAME');
      this.offset=0;
      this.tasks=[];
      this.date_filter='7';
      this.getTaskList(false,'');
    }else{
      this.offset=0;
      this.assignedTo=[];
      this.tasks=[];
      this.date_filter='7';
      this.getTaskList(false,'');
    }
  }

  async assignedRes(item){
    let data=item.slice(1);
    const popover = await this.popCntl.create({
      component:RvAssignedResidentComponent,
      cssClass:'cbmorepop',
    backdropDismiss:true,
      componentProps:{
        data:data,
       

      },
      
      
      
    });
    
   
   
      
  
    return await popover.present();
  }

  gotoDetails(item){
    this.router.navigate(['/rv-task-details',{task:JSON.stringify(item)}])
  }

  formatDate(date){
   
    return moment.utc(date).tz(this.tz).format('DD MMM YYYY ,hh:mm A')
  }
  showCRMAlert(item){
    if(item.task.crm_task==1){
      this.presentAlert('Please use Centrim Life web application to view CRM related tasks')
    }
  }
}
