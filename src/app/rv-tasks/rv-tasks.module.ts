import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvTasksPageRoutingModule } from './rv-tasks-routing.module';

import { RvTasksPage } from './rv-tasks.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvTasksPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [RvTasksPage]
})
export class RvTasksPageModule {}
