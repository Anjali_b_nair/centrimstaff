import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvTasksPage } from './rv-tasks.page';

const routes: Routes = [
  {
    path: '',
    component: RvTasksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvTasksPageRoutingModule {}
