import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvTasksPage } from './rv-tasks.page';

describe('RvTasksPage', () => {
  let component: RvTasksPage;
  let fixture: ComponentFixture<RvTasksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvTasksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvTasksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
