import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
@Component({
  selector: 'app-din-choose-dt-mealtime',
  templateUrl: './din-choose-dt-mealtime.page.html',
  styleUrls: ['./din-choose-dt-mealtime.page.scss'],
})
export class DinChooseDtMealtimePage implements OnInit {
subscription:Subscription;
ser_time:any;
  date:any;
  minDate:any;
  servingtime:any;
  servetime:any[]=[];
  wingArray:any=[];
  wing:any=[];
  wingName:any=[];
  constructor(private router:Router,private platform:Platform,private http:HttpClient,private storage:Storage,
    private config:HttpConfigService,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.date=new Date();
  this.minDate=new Date().toISOString();
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  this.wing=[];
  this.wingName=[];
  this.getWing();
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_servingtime';
  
  
  this.http.get(url,{headers}).subscribe((res:any)=>{
    this.servetime=res.data;

    let closest = null;
    let remaining=[];
    this.servetime.forEach(ele=>{
      let date=moment(this.date).format('DD MMM,YYYY')+' '+ele.end_time
      if(moment(date).isAfter(moment())){
        remaining.push(ele)
      }
    })
    remaining.reduce((acc, obj, i) => {
      // let date=moment(this.date).format('DD MMM,YYYY')+' '+obj.end_time
     
      // if(moment(date).isAfter(moment())){
        
      //   console.log('time:',obj.end_time,moment(date).isAfter(moment()));
        
      
      let diff = Math.abs(this.timeToSecs(obj.end_time) - this.timeToSecs(moment(this.date).format('HH:mm:ss')));

      
      if (diff < acc) {
       

        
        acc = diff;
        closest = obj;
        
      // }
      return acc;
    }
    
    }, Number.POSITIVE_INFINITY);
  
    console.log('res:',res,closest,remaining)
    this.servingtime=closest.servingtime;
        this.ser_time=closest.id;
    // console.log('res:',res,closest,)
  })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    this.back();
    }); 
    
    }
    ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    
      back(){
        this.router.navigate(['/dining-dashboard'],{replaceUrl:true})
      }

      view(){
        console.log('ser_time_id:',this.ser_time,'ser_time:',this.servingtime)
        if(!this.ser_time){
          this.presentAlert('Please choose a serving time.');
        }else if(this.wing.length==0){
          this.presentAlert('Please choose a wing.');
        }else{

          // let wing=[];
          // if(this.wing.includes(0)){
          //   wing=this.wingArray.map(x=>x.id)
          // }else{
          //   wing=this.wing
          // }
        this.router.navigate(['/din-inroom-dining',{ser_time_id:this.ser_time,ser_time:this.servingtime,date:this.date,wing:this.wing}],{replaceUrl:true})
        }
      }

      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass:'toastStyle',
          duration: 3000,
          position:'top'      
        });
        alert.present(); //update
      }

      setServetime(item){
        this.servingtime=item.servingtime;
        this.ser_time=item.id;
      }

      timeToSecs(time) {
        let [h, m, s] = time.split(':');
        return h*3.6e3 + m*60 + s*1;
      }

      setWing(item){
        this.wing=item.id;
        this.wingName=item.wing;
        // if(!this.wing.includes(item.id)){
        //   if(this.wing.includes(0)&&item.id!=0){
        //     const idx=this.wing.indexOf(0);
        //     this.wing.splice(idx,1);
        //     const Idx=this.wingName.indexOf('All wings');
        //     this.wingName.splice(Idx,1);
        //     // this.wing.push(item.id);
        //     // this.wingName.push(item.wing);
        //   }else if(!this.wing.includes(0)&&item.id==0){
        //     this.wing=[];
        //     this.wingName=[];
        //   //   this.wing.push(item.id);
        //   //   this.wingName.push(item.wing);
        //   // }else{
        //   //   this.wing.push(item.id);
        //   //   this.wingName.push(item.wing);
        //   }
        //   this.wing.push(item.id);
        //     this.wingName.push(item.wing);
        // }else{
        //   const idx=this.wing.indexOf(item.id);
        //     this.wing.splice(idx,1);
        //     const Idx=this.wingName.indexOf(item.name);
        //     this.wingName.splice(Idx,1);
        // }
      }

      async getWing(){
        this.wingArray=[];
      
      
         const bid=await this.storage.get("BRANCH")
      
         let headers=await this.config.getHeader();
           let branch=bid.toString();
           let body={company_id:branch}
           let url=this.config.domain_url+'branch_wings/'+bid;
          
           this.http.get(url,{headers}).subscribe((res:any)=>{
             console.log("wing:",res);
            //  let expanded;
             for(let i in res.data.details){
              // console.log("data:",res.data.details[i]);
              // if(i=='0'){
              //   expanded=true;
              // }else{
              //   expanded=false
              // }
            let obj={id:res.data.details[i].id,wing:res.data.details[i].name};
         
               this.wingArray.push(obj);
            
             
             }
            //  this.wingArray.push({id:0,wing:'All wings'})
             this.wing=this.wingArray[0].id;
             this.wingName=this.wingArray[0].wing;
             
            //  this.wing=this.wingArray[0].id;
            //  console.log("array:",this.wingArray);
             
             
           },error=>{
             console.log(error);
             
           })
        
       }
}
