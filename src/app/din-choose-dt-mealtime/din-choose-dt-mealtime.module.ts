import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseDtMealtimePageRoutingModule } from './din-choose-dt-mealtime-routing.module';

import { DinChooseDtMealtimePage } from './din-choose-dt-mealtime.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseDtMealtimePageRoutingModule
  ],
  declarations: [DinChooseDtMealtimePage]
})
export class DinChooseDtMealtimePageModule {}
