import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseDtMealtimePage } from './din-choose-dt-mealtime.page';

describe('DinChooseDtMealtimePage', () => {
  let component: DinChooseDtMealtimePage;
  let fixture: ComponentFixture<DinChooseDtMealtimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseDtMealtimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseDtMealtimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
