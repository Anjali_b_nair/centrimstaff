import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseDtMealtimePage } from './din-choose-dt-mealtime.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseDtMealtimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseDtMealtimePageRoutingModule {}
