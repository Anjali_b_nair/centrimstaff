import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResListPage } from './rv-res-list.page';

describe('RvResListPage', () => {
  let component: RvResListPage;
  let fixture: ComponentFixture<RvResListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
