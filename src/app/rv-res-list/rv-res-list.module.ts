import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResListPageRoutingModule } from './rv-res-list-routing.module';

import { RvResListPage } from './rv-res-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CommunityPositionFilterComponent } from '../components/community-position-filter/community-position-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResListPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [RvResListPage,CommunityPositionFilterComponent],
  entryComponents:[CommunityPositionFilterComponent]
})
export class RvResListPageModule {}
