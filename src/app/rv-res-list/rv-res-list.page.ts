import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PopoverController, Platform, LoadingController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { FilterConsumersComponent } from '../components/filter-consumers/filter-consumers.component';
import { ViewGroupConsumersComponent } from '../components/view-group-consumers/view-group-consumers.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { CommunityPositionFilterComponent } from '../components/community-position-filter/community-position-filter.component';
@Component({
  selector: 'app-rv-res-list',
  templateUrl: './rv-res-list.page.html',
  styleUrls: ['./rv-res-list.page.scss'],
})
export class RvResListPage implements OnInit {
  consumers:any[]=[];
  flag:any;
  terms:any;
  subscription:Subscription;
  type:any;
  create:boolean=false;
  finance:boolean=false;
  offset:any = 1;
  res_type:any=1;
  filter_status:boolean=false;
  position:any;
  tz:any;
  ct_type:any;
  constructor(private http:HttpClient,private router:Router,private config:HttpConfigService,
    private route:ActivatedRoute,private popCntlr:PopoverController,private storage:Storage,
    private platform:Platform,private loadingCtrl:LoadingController,private modalCntl:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){

   
      
      this.terms=null;
      this.offset=1;
      this.consumers=[];
                
                this.flag=this.route.snapshot.paramMap.get('flag');
                this.showLoading();
              
                this.getAllresidents(false,'');
                  const bid=await this.storage.get("BRANCH")
                    this.tz=await this.storage.get('TIMEZONE');
                    const type=await this.storage.get('USER_TYPE')
                      this.type=type;
               
                
                  (await this.config.getUserPermission()).subscribe((res: any) => {
                    console.log('permissions:', res);
                    let routes = res.user_routes;
                    if (res.main_permissions.residents==1&&routes.includes('residents.create')) {
              
                      this.create = true
                    } else {
              
                      this.create = false;
                      
                    }
                    // if (res.main_permissions.finance==1&&routes.includes('finance.index')) {
  
                    //   this.finance= true
                    // } else {
              
                    //   this.finance = false;
                      
                    // }
                  })
                
  
                
                
          this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
            this.router.navigate(['/menu']) ; 
        }); 
        
          }
          ionViewWillLeave() { 
            this.subscription.unsubscribe();
         }
  
   gotoDetails(item){
     if(this.flag==1){
    // this.router.navigate(['/consumer-profile',{id:item.id,flag:1}])
    this.router.navigate(['/rv-resident-profile-landing',{id:item.id,flag:1}])
     }else{
      if(this.finance)
      this.router.navigate(['/petty-cash',{flag:2,id:item.id}])
     }
   }
  
   
  
   cancel(){
    // this.hide=false;
    this.terms=null;
  }
  search(){
      // this.hide=true;
      this.offset=1;
      this.consumers=[];
      this.getAllresidents(false,'')
    }
  
  
  async group(ev){
    const popover = await this.popCntlr.create({
      component: ViewGroupConsumersComponent,
      event: ev,
      backdropDismiss:true,
      cssClass:'filterpop'
      
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        let type = dataReturned.data;
        
        
        //alert('Modal Sent Data :'+ dataReturned);
        if(type==1){
          this.router.navigate(['/create-group',{flag:1}]);
        }else if(type==2){
         this.router.navigate(['/view-group',{flag:1}]);
        }
      }
    });
    return await popover.present();
  }
  async filter(ev){
    const popover = await this.modalCntl.create({
      component: FilterConsumersComponent,
   
      backdropDismiss:true,
      cssClass:'res-filter-modal',
      componentProps:{
        flag:1
      }
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log(dataReturned)
      if(dataReturned.data){
        if (dataReturned.data.status) {
          let type = dataReturned.data.status;
          this.res_type=type;
        }else{
          this.res_type=1
        }
        if(dataReturned.data.type){
          this.ct_type=dataReturned.data.type
        }else{
          this.ct_type=undefined
        }
          this.filter_status=true;
        
        
        
        
          this.offset=1;
          this.consumers=[];
          this.getAllresidents(false,'');
      }
      
    });
    return await popover.present();
  }
  
  
  // async activeUsers(){
  
  //     const cid=await this.storage.get("COMPANY_ID")
  
  //       const bid=await this.storage.get("BRANCH")
  
  //         let url=this.config.domain_url+'residents?type=1';
  //         let headers=await this.config.getHeader();
  //         console.log(url);
        
       
  //         this.http.get(url,{headers}).subscribe((data:any)=>{
           
  //           console.log("actdata:",data);
  //           this.consumers=data.data
           
              
  //         },error=>{
  //           console.log(error);
  //         });
       
   
  // }
  // async inactiveUsers(){
   
  //     const cid=await this.storage.get("COMPANY_ID")
  
  //       const bid=await this.storage.get("BRANCH")
  
  //         let url=this.config.domain_url+'residents?type=2';
  //         let headers=await this.config.getHeader();
  //         console.log(url);
        
          
  //         this.http.get(url,{headers}).subscribe((data:any)=>{
           
  //           console.log("inactdata:",data);
  //           this.consumers=data.data
           
              
  //         },error=>{
  //           console.log(error);
  //         });
      
  // }
  // async noContractUsers(){
  //   const cid=await this.storage.get("COMPANY_ID")
  
  //   const bid=await this.storage.get("BRANCH")

  //     let url=this.config.domain_url+'residents?type=3';
  //     let headers=await this.config.getHeader();
  //     console.log(url);
    
   
  //     this.http.get(url,{headers}).subscribe((data:any)=>{
       
  //       console.log("inactdata:",data);
  //       this.consumers=data.data
       
          
  //     },error=>{
  //       console.log(error);
  //     });
  // }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  getCompanian(residents,user){
   
    let companian;
    const idx = residents.map(x => x.user_id).indexOf(user);
    // console.log('residents:',residents,idx,residents[idx].user_details.name)
      if(idx<=0){
        
        companian=residents[idx+1].user_details.name
      }else{
        companian=residents[0].user_details.name;
      }
      return companian;
    }

  
  

  markAway(away){
    let current=moment().tz(this.tz);
    if(away.type==0&&moment(away.move_in)<=current){
      return false
    }else if(away.type==1&&moment(away.move_in)>current&&moment(away.move_out)<=current){
      return false;
    }else{
    return true;
  }
  }


  // single function to list residents - new api


  async getAllresidents(isFirstLoad,event){
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID");
    const type=await this.storage.get('USER_TYPE');

    // let url=this.config.domain_url+'get_all_residents_in_branch_with_filter';
                 
    // let headers=await this.config.getHeader();

    // let body;
    // body={
    //   type:this.res_type,  // 1 - active 2 -inactive 3 - no contract
    //   sort:'fname',  // sortby
    //   order:'ASC',   // ASC or DESC
    
    //   offset:this.offset, // 0   -- increment by 20
    //   limit:20, // 20 -- constant
      
    // }

    // if(this.terms){
    // body.keyword=this.terms
    // }
    // if(this.position){
    //   body.position=this.position
    // }
    let url=this.config.domain_url+'get_all_residents_list_via_filters';
                 
    let headers=await this.config.getHeader();

    let body;
    body={
      type:this.res_type,  // 1 - active 2 -inactive 3 - no contract
      sort:'fname',  // sortby
      order:'ASC',   // ASC or DESC
    
      page:this.offset, // 0   -- increment by 20
      // limit:20, // 20 -- constant
      
    }

    if(this.terms){
    body.keyword=this.terms
    }
    if(this.position){
      body.position=this.position
    }

    if(this.ct_type){
      body.ct_type_val=this.ct_type
    }
                 console.log('body:',body,cid,bid);
                 
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
                   
      console.log("data:",data);

      if(this.terms){
        this.consumers=[];
      }

      // for (let i = this.offset; i < data.data.residents.length; i++) {
      //   this.consumers.push(data.data.residents[i]);
      // }

      for(let i in data.data.residents){
        this.consumers.push(data.data.residents[i]);
      }

      if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      // this.offset=this.offset+20; 
      this.offset=++this.offset;
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });

  }
  doInfinite(event) {
    this.getAllresidents(true, event)
    }
    removeFilter(){
      this.filter_status=false;
     this.position=undefined;
       this.res_type=1;
       this.offset=1;
       this.ct_type=undefined;
       this.consumers=[];
       this.getAllresidents(false,'');
    }

    async cpFilter(ev){
      const popover = await this.popCntlr.create({
        component: CommunityPositionFilterComponent,
        event: ev,
        backdropDismiss:true,
        cssClass:'filterpop',
        componentProps:{
          position:this.position
        }
        // translucent: true
      });
      popover.onDidDismiss().then((dataReturned) => {
        console.log(dataReturned)
        if (dataReturned.data) {
          if(dataReturned.data.id==0){
            this.position=undefined;
          }else{
          this.position=dataReturned.data.id;
          }
          this.filter_status=true;
          this.offset=1;
          this.consumers=[];
          this.getAllresidents(false,'');
        }
      });
      return await popover.present();
    }
}
  

