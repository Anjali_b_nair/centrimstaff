import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResListPage } from './rv-res-list.page';

const routes: Routes = [
  {
    path: '',
    component: RvResListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResListPageRoutingModule {}
