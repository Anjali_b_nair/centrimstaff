import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendedReasonPageRoutingModule } from './attended-reason-routing.module';

import { AttendedReasonPage } from './attended-reason.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttendedReasonPageRoutingModule
  ],
  declarations: [AttendedReasonPage]
})
export class AttendedReasonPageModule {}
