import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttendedReasonPage } from './attended-reason.page';

describe('AttendedReasonPage', () => {
  let component: AttendedReasonPage;
  let fixture: ComponentFixture<AttendedReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendedReasonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AttendedReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
