import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendedReasonPage } from './attended-reason.page';

const routes: Routes = [
  {
    path: '',
    component: AttendedReasonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendedReasonPageRoutingModule {}
