import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningDashboardPageRoutingModule } from './dining-dashboard-routing.module';

import { DiningDashboardPage } from './dining-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningDashboardPageRoutingModule
  ],
  declarations: [DiningDashboardPage]
})
export class DiningDashboardPageModule {}
