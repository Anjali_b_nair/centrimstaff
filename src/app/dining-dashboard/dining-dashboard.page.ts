import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-dining-dashboard',
  templateUrl: './dining-dashboard.page.html',
  styleUrls: ['./dining-dashboard.page.scss'],
})
export class DiningDashboardPage implements OnInit {

  subscription:Subscription;
  profile:boolean=false;
  serving:boolean=false;
  meals:boolean=false;
  order:boolean=false;
  view:boolean=false;
  constructor(private router:Router,private platform:Platform,private config:HttpConfigService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
      if (res.main_permissions.dining==1&&routes.includes('dietry_profile.index')) {

        this.profile= true
      } else {

        this.profile = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('serving.index')) {

        this.serving= true
      } else {

        this.serving = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('dining_menu.index')) {

        this.view= true
      } else {

        this.view = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('meal_status.index')) {

        this.meals= true
      } else {

        this.meals = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('place_order')) {

        this.order= true
      } else {

        this.order = false;
        
      }
    })
 this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
this.back();
}); 

}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

  back(){
    this.router.navigate(['/menu'],{replaceUrl:true})
  }

}
