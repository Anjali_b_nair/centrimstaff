import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningDashboardPage } from './dining-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: DiningDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningDashboardPageRoutingModule {}
