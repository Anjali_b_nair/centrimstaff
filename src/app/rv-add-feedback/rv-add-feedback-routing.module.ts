import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvAddFeedbackPage } from './rv-add-feedback.page';

const routes: Routes = [
  {
    path: '',
    component: RvAddFeedbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvAddFeedbackPageRoutingModule {}
