import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvAddFeedbackPageRoutingModule } from './rv-add-feedback-routing.module';

import { RvAddFeedbackPage } from './rv-add-feedback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvAddFeedbackPageRoutingModule
  ],
  declarations: [RvAddFeedbackPage]
})
export class RvAddFeedbackPageModule {}
