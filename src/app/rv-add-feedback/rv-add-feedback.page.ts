import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, LoadingController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { RvSelectStaffComponent } from '../components/rv-select-staff/rv-select-staff.component';
@Component({
  selector: 'app-rv-add-feedback',
  templateUrl: './rv-add-feedback.page.html',
  styleUrls: ['./rv-add-feedback.page.scss'],
})
export class RvAddFeedbackPage implements OnInit {
  cid:any;
  id:any;
 consumer:any;
 type:any='2';
 category:any;
 categories:any=[];
 feedback:any;
 images:any=[];
 status:any='1';
 isLoading:boolean=false;
 date:any=new Date().toISOString();
  subscription:Subscription;
  assignedTo:any=[];

staff:any;
    constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,
      private http:HttpClient,private config:HttpConfigService,private storage:Storage,
      private actionsheetCntlr:ActionSheetController,private popCntl:PopoverController,
      private loadingCtrl:LoadingController,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.consumer=this.route.snapshot.paramMap.get('consumer');
    this.getCategories();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
      this.back();
      
  }); 
  
  }

  back(){
    this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}],{replaceUrl:true})
  }

  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.uploadImg(base64Image);
  
       
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
   
    this.uploadImg(base64Image);
  }
  async addImage(){
    
  //   let options;
  //   options={
  //   maximumImagesCount: 5,
  //   outputType: 1,
  //   quality:100
  // }
  // if(this.platform.is('ios')){
  //   options.disable_popover=true
  // }
  //   this.imagePicker.getPictures(options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //         console.log('Image URI: ' + results[i]);
          
          
  //         if((results[i]==='O')||results[i]==='K'){
  //           console.log("no img");
            
  //           }else{
             
  //           this.uploadImg('data:image/jpeg;base64,' + results[i])
  //           }
  //     }
  //   }, (err) => { });
  
  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
          console.log('Image URI: ' + image.photos[i]);
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            
            this.uploadImg('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }

  
  
  }
  removeImg(i){
    this.images.splice(i,1);
    
  }

  async uploadImg(img){
    if(this.isLoading==false){
      this.showLoading();
      }
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      
      this.images.push(res.data);
      this.dismissLoader();
      
    })
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }

  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async save(){
    if(!this.feedback){
      this.presentAlert('Please enter the feedback.')
    }else{
    const bid = await this.storage.get("BRANCH");
    const uid = await this.storage.get("USER_ID");
    const cid = await this.storage.get("COMPANY_ID");

      let url=this.config.domain_url+'saveManualFeedback';
    let headers=await this.config.getHeader();
  
    let body={
      // public:0,
      branch_id:bid,
      company_id:cid,
      type:this.type,
      category_id:this.category,
      status:this.status,
      feedback:this.feedback,
      via_app:1,
      user_type:1,
      created_date:moment().format('YYYY-MM-DD'),
      feedbacker_id:this.id,
      assignee:[7227],
      images:this.images
    }
    console.log('body:',body);
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     
      this.presentAlert('Feedback added successfully.')
      this.back();
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
  }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async getCategories(){
    const bid = await this.storage.get("BRANCH");
    
    const cid = await this.storage.get("COMPANY_ID");

      let url=this.config.domain_url+'feedback_categories';
    let headers=await this.config.getHeader();

    this.http.get(url,{headers}).subscribe((res:any)=>{
     
      console.log('cat:',res);
      this.categories=res.data
       this.category=this.categories[0].id.toString();
     },error=>{
       console.log(error);
      
     })
  }

  async openAssignedTo(){
   

    const popover = await this.popCntl.create({
     
      component:RvSelectStaffComponent,
      cssClass:'rv-select-staf-pop',
      backdropDismiss:true,
      componentProps:{
        data:this.assignedTo,
       flag:1,
       status:0
  
      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned,this.assignedTo);
      if(dataReturned.data){
        
        
            this.assignedTo=dataReturned.data.id;
            console.log('data is here:',this.assignedTo);
            if(dataReturned.data.name.length>1){
            this.staff=dataReturned.data.name[0]+', '+dataReturned.data.name[1];
            }else{
              this.staff=dataReturned.data.name[0]
            }
          }
   
          console.log('data is here1:',this.assignedTo);
      
    });
    return await popover.present();
  
  }
}
