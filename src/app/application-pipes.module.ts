import { NgModule } from '@angular/core';
import { DateformatPipe } from './pipes/dateformat.pipe';
import { MessagesearchPipe } from './pipes/messagesearch.pipe';
import { ConsumersearchPipe } from './pipes/consumersearch.pipe';
import { CbsearchPipe } from './pipes/cbsearch.pipe';
import { Consumersearch2Pipe } from './pipes/consumersearch2.pipe';
import { VisitorsearchPipe } from './pipes/visitorsearch.pipe';
import { LocationsearchPipe } from './pipes/locationsearch.pipe';
// import { ContactsComponent } from './components/expandable/contacts/contacts.component';
import { ResourcesearchPipe } from './pipes/resourcesearch.pipe';
import { DiningResidentsPipe } from './pipes/dining-residents.pipe';
import { DiningTableResidentsPipe } from './pipes/dining-table-residents.pipe';
import { DebounceDirective } from './directive/debounce.directive';
import { PdfViewerModule } from 'ng2-pdf-viewer';
// import { ResourcesearchPipe } from './pipes/resourcesearch.pipe';
// import { CKEditorModule } from '@ckeditor/ckeditor5-build-classic';

@NgModule({
    imports: [
      // dep modules
      // FileUploadModule
     
    ],
    declarations: [ 
      DateformatPipe, MessagesearchPipe, ConsumersearchPipe, CbsearchPipe, 
      Consumersearch2Pipe, VisitorsearchPipe, LocationsearchPipe, ResourcesearchPipe, DiningResidentsPipe, DiningTableResidentsPipe, DebounceDirective
    //   ResourcesearchPipe
      // MultiFileUploadComponent
    
    ],
    exports: [
      DateformatPipe,
      MessagesearchPipe,
      ConsumersearchPipe,
      CbsearchPipe,
      Consumersearch2Pipe,
      VisitorsearchPipe,
      LocationsearchPipe,
      DiningResidentsPipe,
      ResourcesearchPipe,
      DiningTableResidentsPipe,
      DebounceDirective,
      PdfViewerModule
      // MultiFileUploadComponent
      
    ]
  })
  export class ApplicationPipesModule {}