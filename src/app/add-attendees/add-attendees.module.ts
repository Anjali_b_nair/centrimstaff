import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddAttendeesPageRoutingModule } from './add-attendees-routing.module';

import { AddAttendeesPage } from './add-attendees.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ActAssignedStatusFilterComponent } from '../components/act-assigned-status-filter/act-assigned-status-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddAttendeesPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [AddAttendeesPage,ActAssignedStatusFilterComponent],
  entryComponents:[ActAssignedStatusFilterComponent]
})
export class AddAttendeesPageModule {}
