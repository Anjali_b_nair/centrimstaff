import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityAttendanceGroupComponent } from '../components/activity-attendance-group/activity-attendance-group.component';
import { ActAssignedStatusFilterComponent } from '../components/act-assigned-status-filter/act-assigned-status-filter.component';
@Component({
  selector: 'app-add-attendees',
  templateUrl: './add-attendees.page.html',
  styleUrls: ['./add-attendees.page.scss'],
})
export class AddAttendeesPage implements OnInit {
  selected:any='1';
  consumers:any[]=[];
  terms:any;
  family:any=[];
  other:any[]=[];
  sel_id:any[]=[];
  temp:any[]=[];
  rem:any[]=[];
  all:boolean=false;
  sel_fam:any[]=[];
  temp_fam:any[]=[];
  act_id:any;
  subscription:Subscription;
  act_atnd:any[]=[];
  con:any[]=[];
  attendees:any[]=[];
  rv:any;
  limit_required:any;
  limit:any;
  count:any;
  rv_details:any;
  signupCount:any;
  offset:any = 1;
  selectAllResidentsStatus:boolean=false;
  selectAllFamilyStatus:boolean=false;
  type:any;
  skip:any=0;
unselected_ids:any=[]=[];
  constructor(private config:HttpConfigService,private http:HttpClient,private route:ActivatedRoute,
    private router:Router,private platform:Platform,private toastCntlr:ToastController,private storage:Storage,
    private modalCntl:ModalController,private loadingCtrl:LoadingController,private popCntl:PopoverController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.act_id=this.route.snapshot.paramMap.get('act_id');
    this.attendees=JSON.parse(this.route.snapshot.paramMap.get('attendee'));
    
    
    this.limit_required=this.route.snapshot.paramMap.get('limit');
    this.rv_details=JSON.parse(this.route.snapshot.paramMap.get('rv_details'));
    this.signupCount=parseInt(this.route.snapshot.paramMap.get('signupCount'));
    this.offset=1;
    this.skip=0;
    if(this.rv_details){
    this.limit=this.rv_details.slot_limit-this.signupCount;
    this.count=this.limit
    }
    
    this.rv=await this.storage.get('RVSETTINGS');
    this.terms='';
                this.sel_id=[];
                this.temp=[];
                this.sel_fam=[];
                this.temp_fam=[];
                this.act_atnd=[];
                this.consumers=[];
                this.con=[];
                this.other=[];
               
                this.showLoading();
                  const bid=await this.storage.get("BRANCH")

                this.terms='';
                            this.sel_id=[];
                            this.temp=[];
                            this.temp_fam=[];
                            this.sel_fam=[];
                            if(this.attendees.length){
                              this.sel_id=this.attendees;
                              this.temp=this.attendees;
                              this.temp_fam=this.attendees;
                              
                            }else{
                              this.sel_id=[];
                              this.sel_fam=[];
                              this.temp=[];
                            this.temp_fam=[];
                            }
                //   let url=this.config.domain_url+'residents';
                //   let headers=await this.config.getHeader();
                //   console.log(url,this.sel_id,this.attendees);
                // // this.selected=true;
                //   // let headers=await this.config.getHeader();
                //   this.http.get(url,{headers}).subscribe((data:any)=>{
                   
                    
                //     data.data.forEach(element => {
                //       this.consumers.push({con:element,selected:false});
                //       this.con.push({con:element,selected:false});
                //     });
                    
                //     console.log("data:",this.consumers);
                //       this.dismissLoader();
                //   },error=>{
                //     console.log(error);
                //     this.dismissLoader();
                //   });
                this.getResidentList(false,'');
               
                
              this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
                this.router.navigate(['/activity-details',{act_id:this.act_id}])
             
              
                }); 
                
        }

async getResidentList(isFirstLoad,event){
  let url=this.config.domain_url+'get_all_residents_list_via_filters';  
  let headers=await this.config.getHeader();

  let body;
  body={
    type:1,  // 1 - active 2 -inactive 3 - no contract
    sort:'fname',  // sortby
    order:'ASC',   // ASC or DESC
  
    page:this.offset, 
    
  }

  if(this.terms){
  body.keyword=this.terms
  }
               console.log('body:',body,headers);
               
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
                 
    console.log("data:",data);

    for (let i = 0; i < data.data.residents.length; i++) {
     
      this.consumers.push({con:data.data.residents[i],selected:false});
      this.con.push({con:data.data.residents[i],selected:false});
      if(this.selectAllResidentsStatus){
        if(this.limit_required==1&&this.rv_details.waiting_list==0&&this.rv_details.slot_limit<=this.sel_id.length){
  
        }else{
          this.temp.push(data.data.residents[i].user_id);
          this.sel_id.push(data.data.residents[i].user_id);
        }
  
      }
    }

    if (isFirstLoad)
    event.target.complete();
    
    
    this.dismissLoader();
    this.offset=++this.offset;    
        
    },error=>{
      this.dismissLoader();
      console.log(error);
    });

}
doInfinite(event) {
  if(this.type==3||this.type==4){
    this.getActivityAttendee(true,event);
  }else{
  this.getResidentList(true, event)
  }
  }


                ionViewWillLeave() { 
                this.subscription.unsubscribe();
                }
              
              segmentChange(i){
                if(i==1){
                  this.selected='1'
                }else{
                  this.selected='2';
                  this.getFamily();
                }
              }
              
              
              async getFamily(){
                
                  const bid=await this.storage.get("BRANCH")

                this.other=[];
                
                  
                  let url=this.config.domain_url+'resident_contact';
                  let headers=await this.config.getHeader();
                  this.http.get(url,{headers}).subscribe((data:any)=>{ 
                    console.log("fam:",data);
                    data.data.forEach(element => {
                    if(element.contacts.length>0){
                    this.other.push(element)
                    }
                  })
                    });
                
                console.log("other:",this.other);
                 
              }
              
              // async dismiss(){
              //   let attendees=this.sel_id.concat(this.sel_fam)
              //   await this.modalCntl.dismiss(attendees);
              //   console.log("ond:",attendees);
                
              // }
              
              selectConsumer(ev,item){
                if(this.temp.includes(item.con.user_id)){
                  var index = this.sel_id.indexOf(item.con.user_id);
                  this.sel_id.splice(index, 1);
                  this.unselected_ids.push(item.con.user_id);
                  console.log("index:",this.sel_id.indexOf(item.con.user_id))
                  this.temp.splice(this.temp.indexOf(item.con.user_id),1);
                  if(this.count<=this.limit)
                  this.count=++this.count
                }else{
                  if(this.limit_required==1&&this.rv_details.waiting_list==0&&this.rv_details.slot_limit<=this.sel_id.length){
                    this.presentAlert('No more slots available.')
                  }else{
                  // if(this.limit!==0&&this.limit>=this.temp.length+this.temp_fam.length){
                    
                  
                  if(this.count>0)
                  this.count=--this.count;
                  if(this.limit_required==1&&this.rv_details.waiting_list&&this.count<=0){
                    this.presentAlert('No slots available, adding to waiting list.')
                  }
                  this.temp.push(item.con.user_id);
                  var ind = this.unselected_ids.indexOf(item.con.user_id);
                  this.unselected_ids.splice(ind,1);
                  }
                // }else{
                //   this.presentAlert('No more slots available.')
                // }
                }
                this.sel_id=this.temp.reduce((arr,x)=>{
                  let exists = !!arr.find(y => y === x);
                  if(!exists){
                      arr.push(x);
                  }
                  
                  return arr;
              }, []);
              console.log("sel:",this.count);
              }
              // selectConsumer(ev,item){
              //   item.selected=true;
              //   if(this.temp.includes(item.con.user.user_id)){
              
              //   }else{
              //     this.temp.push(item.con.user.user_id);
              //   }
              //   this.sel_id=this.temp.reduce((arr,x)=>{
              //     let exists = !!arr.find(y => y === x);
              //     if(!exists){
              //         arr.push(x);
              //     }
                  
              //     return arr;
              // }, []);
              // console.log("sel:",this.sel_id,this.temp);
              
              // }
              
              selectAll(ev){

                this.unselected_ids=[];

                  if(this.selected=='1'){
                    this.selectAllResidentsStatus=!this.selectAllResidentsStatus;
                
                if(this.sel_id.length>=this.consumers.length){
                  this.temp=[];
                  this.sel_id=[];
                  this.count=this.limit
                
                }else{
                  this.consumers.forEach(element=>{
                    
                     
                       if(this.temp.includes(element.con.user_id)){
              
                    }else{
                      if(this.limit_required==1&&this.rv_details.waiting_list==0&&this.rv_details.slot_limit<=this.sel_id.length){
                        this.presentAlert('No more slots available.')
                      }else{
                      this.temp.push(element.con.user_id);
                      if(this.count>0)
                      this.count=--this.count;
                      }
                    }
                    
                    this.sel_id=this.temp.reduce((arr,x)=>{
                      let exists = !!arr.find(y => y=== x);
                      if(!exists){
                          arr.push(x);
                      }
                      return arr;
                  }, []);
                 
                
                    
                  })
                }
                  console.log("selc:",this.sel_id);
              }else{
                  this.selectAllFamilyStatus=!this.selectAllFamilyStatus
                if(this.sel_fam.length>=this.other.length){
                  this.temp_fam=[];
                  this.sel_fam=[];
                  this.count=this.limit
                }else{
                  this.other.forEach(element=>{
                    
                     for(let i in element.contacts){
                       if(this.temp_fam.includes(element.contacts[i].user.user_id)){
              
                    }else{
                      if(this.limit_required==1&&this.rv_details.waiting_list==0&&this.rv_details.slot_limit<=this.sel_id.length){
                        this.presentAlert('No more slots available.')
                      }else{
                      this.temp_fam.push(element.contacts[i].user.user_id);
                      if(this.count>0)
                      this.count=--this.count
                      }
                    }
                    
                    this.sel_fam=this.temp_fam.reduce((arr,x)=>{
                      let exists = !!arr.find(y => y=== x);
                      if(!exists){
                          arr.push(x);
                      }
                      return arr;
                  }, []);
                 
                
                    }
                    
                  })
                }
                
                console.log("self:",this.sel_fam);

              }
                  
              }
              
              remove(item){
                // item.selected=false;
               
              
              this.sel_id.forEach(ele=>{
                if(ele==item.con.user_id){
                  var index = this.sel_id.indexOf(ele);
                    this.sel_id.splice(index, 1);
                    this.temp.splice(this.temp.indexOf(ele),1);
                    if(this.count<=this.limit)
                    this.count=++this.count
                }
                
              
              })
              console.log("rem:",this.sel_id);
                // this.rem.push({id:item.con.user.user_id});
                
                // this.sel_id=this.rem.reduce((arr,x)=>{
                // let exists = !!this.sel_id.find(y => y.id === x.id);
                // console.log("exist:",exists,arr);
                
                //     if(exists){
                //       var index = arr.indexOf(x.id);
                //     arr.splice(index, 1);
                //     }else{
                    
                //   }
              
                //   return arr;
                //   }, []);
                //   var idx = this.sel_id.indexOf('90');
                //   if (idx != -1) {
                //     this.sel_id.splice(this.sel_id.indexOf(item.con.user.user_id), 1);
                // }
                
                
              }
              
              selectFam(ev,item){
                if(ev.currentTarget.checked==true){
                if(this.sel_fam.includes(item)){
              
                }else{
                  if(this.limit_required==1&&this.rv_details.waiting_list==0&&this.rv_details.slot_limit<=this.sel_id.length){
                    this.presentAlert('No more slots available.')
                  }else{
                  
                  if(this.count>0)
                  this.count=--this.count
                  }
                  if(this.limit_required==1&&this.rv_details.waiting_list&&this.count<=0){
                    this.presentAlert('No slots available, adding to waiting list.')
                  }
                  this.sel_fam.push(item);
                  this.temp_fam.push(item);
                  var ind = this.unselected_ids.indexOf(item);
                  this.unselected_ids.splice(ind,1);
                  // }else{
                  //   this.presentAlert('No more slots available.')
                  // }
                }
              }else{
                var index = this.sel_fam.indexOf(item);
                this.sel_fam.splice(index, 1);
                this.temp_fam.splice(index,1);
                this.unselected_ids.push(item);
                if(this.count<=this.limit)
                this.count=++this.count
              }
              console.log("sel:",this.sel_fam);
              
              }
              cancel(){
                this.terms='';
              }
              
              
              
              async byGroup(ev){
                let headers=await this.config.getHeader();
                const popover = await this.modalCntl.create({
                  component: ActivityAttendanceGroupComponent,
                  backdropDismiss:true,
                  cssClass:'act-res-group-modal'
                  
                  // translucent: true
                });
                popover.onDidDismiss().then((dataReturned) => {
                  if (dataReturned == null || dataReturned.data ==undefined) {
                  }else{
                    
                    let id = dataReturned.data;
                    this.consumers=[];
                    if(id!=0){
                    let url=this.config.domain_url+'group/'+id;
                this.http.get(url,{headers}).subscribe((res:any)=>{
                  console.log("mem:",res);
                
                  
                  res.data.residents.forEach(element => {
                
                    this.consumers.push({con:element,selected:false});
                  });
                  
                })
              }else{
                this.consumers=this.con;
              }
                  }
                
                });
                return await popover.present();
              }
  
  async add(){
    if(this.sel_id.length<=0&&this.sel_fam.length<=0){
      this.presentAlert('Please select attendees.')
    }else{
      const bid=await this.storage.get("BRANCH");
      let headers=await this.config.getHeader();
    let a=this.sel_id.concat(this.sel_fam);
    let set = new Set(a);

    let attendees  = Array.from(set);
    let selectAll=0;
    if(this.selectAllResidentsStatus){
      selectAll=1
    }
    let unselected=[];
    // let url=this.config.domain_url+'add_attendee';
    let url=this.config.domain_url+'add_tagged_attendee'

    // let body={
    //   activity_id:this.act_id,
    //   user_id:attendees,
    //   action:0,
    //   branch_id:bid
    // }
    let body={
      is_select_all:selectAll,
      unselected_arr:this.unselected_ids,
      activity_id:this.act_id,
      user_id:attendees,
      action:0,
      branch_id:bid
    }

    console.log('body:',body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('added:',res);
      if(res.status=='success'){
        this.presentAlert('Added successfully.')
        this.router.navigate(['/activity-details',{act_id:this.act_id}])
      }else{
        this.presentAlert('Something went wrong. Please try again later.')
      }
    })
    // await this.modalCntl.dismiss(attendees);
    console.log("ond:",attendees);
  }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async assignedStatusFilter(event){
    const popover = await this.popCntl.create({
      component: ActAssignedStatusFilterComponent,
      event: event,
      backdropDismiss:true,
      cssClass:'filterpop',
     
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log("closedData:",dataReturned);
      
      if (dataReturned !== null) {
        this.type=dataReturned.data;
        if(this.type==3||this.type==4){
          this.skip=0;
          this.consumers=[];
          this.getActivityAttendee(false,'')
        }else{
          this.selected='1';
          this.offset=1;
          this.consumers=[];
          this.getResidentList(false,'')
        }
      }
    });
    return await popover.present();
  }

  async getActivityAttendee(isFirstLoad,event){
    let url=this.config.domain_url+'activity_attendee';
    const utype=await this.storage.get("USER_TYPE");
    const cid=await this.storage.get("COMPANY_ID")
    let headers=await this.config.getHeader();
    let body;
    body={
      activity_id:this.act_id,
      role_id:utype,
      company_id:cid,
      skip:this.skip,
      take:20,
      type:this.type // 3- assigned, 4- unassigned
    }

   


    console.log('body:',body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('actAttendees:',res);
      for (let i = 0; i < res.data.length; i++) {
        let wing=null;
        if(res.data[i].wing){
          wing=res.data[i].wing.name
        }
        let property=null;
        if(res.data[i].property_details&&res.data[i].property_details.contract_details&&res.data[i].property_details.contract_details.property&&res.data[i].property_details.contract_details.property.location){
          property=res.data[i].property_details.contract_details.property.location.name
        }
        let user={user_id:res.data[i].user.user_id,name:res.data[i].user.name,photo:res.data[i].user.profile_pic,room:res.data[i].room,wing_name:wing,property_name:property}
        let selected;
        if(this.type==3){
          selected=true
        }else{
          selected=false
        }
        this.consumers.push({con:user,selected:selected});
        this.con.push({con:user,selected:selected});
      }
      if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      this.skip=this.skip+20;    
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });
  }

}
