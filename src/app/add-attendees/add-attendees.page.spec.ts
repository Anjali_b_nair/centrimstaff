import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddAttendeesPage } from './add-attendees.page';

describe('AddAttendeesPage', () => {
  let component: AddAttendeesPage;
  let fixture: ComponentFixture<AddAttendeesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttendeesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAttendeesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
