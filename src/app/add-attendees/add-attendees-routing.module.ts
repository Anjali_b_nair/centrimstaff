import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAttendeesPage } from './add-attendees.page';

const routes: Routes = [
  {
    path: '',
    component: AddAttendeesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddAttendeesPageRoutingModule {}
