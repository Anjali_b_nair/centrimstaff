import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarkAttendanceOtherAttendeesPage } from './mark-attendance-other-attendees.page';

describe('MarkAttendanceOtherAttendeesPage', () => {
  let component: MarkAttendanceOtherAttendeesPage;
  let fixture: ComponentFixture<MarkAttendanceOtherAttendeesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkAttendanceOtherAttendeesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarkAttendanceOtherAttendeesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
