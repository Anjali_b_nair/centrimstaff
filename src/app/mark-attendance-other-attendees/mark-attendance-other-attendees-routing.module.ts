import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarkAttendanceOtherAttendeesPage } from './mark-attendance-other-attendees.page';

const routes: Routes = [
  {
    path: '',
    component: MarkAttendanceOtherAttendeesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarkAttendanceOtherAttendeesPageRoutingModule {}
