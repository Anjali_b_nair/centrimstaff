import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarkAttendanceOtherAttendeesPageRoutingModule } from './mark-attendance-other-attendees-routing.module';

import { MarkAttendanceOtherAttendeesPage } from './mark-attendance-other-attendees.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarkAttendanceOtherAttendeesPageRoutingModule
  ],
  declarations: [MarkAttendanceOtherAttendeesPage]
})
export class MarkAttendanceOtherAttendeesPageModule {}
