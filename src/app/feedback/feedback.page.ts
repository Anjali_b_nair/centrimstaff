import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage  {
  // subscription:Subscription;
  category:any=[];
  type:any;
  imageResponse:any=[];
img:any[]=[];
feedback:any;
cat_id:any;
cat:HTMLElement;
  feed:HTMLElement;
  show:any;
  contacts:any[]=[];
  con_id:any;
  utype:any;
  name:any;
  email:any;
  phone:any;
  isLoading:boolean=false;
  subscription:Subscription;
  sid:any;
  role:any;
  flag:any;
  // @ViewChild(IonContent, { static: false }) content: IonContent;
  constructor(private config:HttpConfigService,private http:HttpClient,private loadingCtrl:LoadingController,
    private modalCntlr:ModalController,private actionsheetCntlr:ActionSheetController,
    private storage:Storage,private toast:ToastController,private modalCntrl:ModalController,
    private platform:Platform,private router:Router,private orient:ScreenOrientation,private route:ActivatedRoute) { }

 

  async ionViewWillEnter(){
    // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
    this.imageResponse=[];
      this.img=[];
      this.type;
      this.cat_id;
      this.feedback;
      this.flag = this.route.snapshot.paramMap.get("flag");
      if(this.flag==1){
      this.show=1
      
    
    
    this.cat_id = this.route.snapshot.paramMap.get("cat_id");
    this.role = this.route.snapshot.paramMap.get("role");
    this.type = this.route.snapshot.paramMap.get("type");
      }else{
    this.show=2;
      }
      const data=await this.storage.get("TOKEN");
      const bid=await this.storage.get("BRANCH");
      const com=await this.storage.get("COMPANY_ID");
   
      const dat=await this.storage.get("PCFBRANCH")
        let id=dat.toString();
        // this.id=id.toString()
        const x=await this.storage.get("SURVEY_CID")
          let cid=x.toString();
          // this.cid=cid.toString()
          // let url=this.config.feedback_url+'get_all_reviews';
          let url=this.config.feedback_url+'get_defaulted_review';
          let body = `company_id=${cid}&branch_id=${id}`;
          //  body=({
          //   company_id:"14",
          //   user_id:"3"
          // })
          // body.set('company_id','14');
          // body.set('user_id','3');
          console.log("body:",body,url);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
           
          })
          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            console.log(data);
            // if(data.data.length==1){
            //  this.sid=data.data[0].review_branch_id
            if(data.data){
              this.show=2
            this.sid=data.data.review_branch_id;
            
            // }else{
            //   this.router.navigate(['/feedback'])
            // }
            
        //     let body=new FormData();
        // body.append('company_id',cid);
        // body.append('review_bid',this.sid);
        // body.append('language_id','1');
        let body = `company_id=${cid}&review_bid=${this.sid}&language_id=1`;
        let url=this.config.feedback_url+'get_review_categories';
        console.log('catbody:',body,cid,this.sid);
        
      // let url='http://52.65.155.193/centrim_api/api/feedback_categories';
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("cat:",res)
        this.category=res.display_all_review_languages
      },error=>{
        console.log(error);
        
      })
    }else{
      this.show=5
    }
          })
  
      


    //   console.log(this.type,this.cat_id,this.feedback);
      
    //   let url=this.config.domain_url+'feedback_categories'
    
    // this.http.get(url).subscribe((res:any)=>{
    //   this.category=res.data
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/menu'],{replaceUrl:true})
   
      }); 
      
  }
  back(){
    this.router.navigate(['/menu'],{replaceUrl:true})
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
setRole(i){
  this.show=0;
  if(i==1){
    this.role=1
  }else{
    this.role=2
  }
}
  next(id){
    this.type=id;
    this.show=1;
    // this.content.scrollToPoint(0, this.cat.offsetTop, 1000);
    // this.router.navigate(['/select-category',{type:id}])
  }
  async openModal() {
    const modal = await this.modalCntlr.create({
      component: ThankyouComponent,
      componentProps: {
        "flag":2,
        data:this.type
      }
    });
    return await modal.present();
  }
  
  gotoFeedback(cat){
    this.cat_id=cat.id;
   
    this.router.navigate(['/feedback-dashboard',{sid:this.sid,lid:1,cat_id:this.cat_id,type:this.type,role:this.role}],{replaceUrl:true})
    // this.show=2
    // this.content.scrollToPoint(0, this.feed.offsetTop, 1000);
  }
  
  async addImage(){
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    //   this.imagePicker.getPictures(options).then((results) => {
    //     for (var i = 0; i < results.length; i++) {
    //         // console.log('Image URI: ' + results[i]);
    //         if((results[i]==='O')||results[i]==='K'){
    //           console.log("no img");
              
    //           }else{
    //             // this.img.push(results[i]);
    //          let im='data:image/jpeg;base64,'+results[i]
    //          this.imageResponse.push(im);
    //          this.uploadImage(im);
    //         //  this.img.push(results[i]);
    //           }
            
             
    //     }
    //   }, (err) => { });
    
    // console.log("imagg:",this.imageResponse);


    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  

  }
  
  
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        // role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.imageResponse.push(base64Image);
    //   this.uploadImage(base64Image);
    //   // this.img.push(imageData);
       
    // }, (err) => {
    //   // Handle error
    // });
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.uploadImage(base64Image);

  }
  
  ionViewDidLeave(){
    this.type='';
    this.cat_id='';
    this.feedback='';
    this.show=2;
    this.name='';
    this.email='';
    this.phone='';
  }
  
  removeImg(i){
    this.imageResponse.splice(i,1);
    this.img.splice(i,1);
  }
  
  
  continue(){
    if(this.feedback==undefined || this.feedback==''){
      
      this.presentAlert('Please enter your feedback');
    }else if(this.type==undefined || this.type==''){
      
      this.presentAlert('Please select a feedback type');
    }else if(this.cat_id==undefined || this.cat_id==''){
      
      this.presentAlert('Please select a category');
    }else{
      this.show=4;
    }
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
  
  
  
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    this.isLoading=false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  previous(){
    if(this.show==1){
      this.show=0
    }else if(this.show==0){
      this.show=2
    }
  }



  async submit(){
    let pattern= "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
    if(this.name==undefined || this.name==''){
      
      this.presentAlert('Please enter your name');
    }else if(this.email==undefined || this.email==''){
      
      this.presentAlert('Please enter your email address');
    }else if((this.phone!=null)&&(this.phone.toString().length<10||this.phone.toString().length>10)){
      
      this.presentAlert('Please enter 10 digit phone number');
    }else if(/^\d+$/.test(this.name)){
        this.presentAlert("Please enter proper name.")
    }else if((this.email!=''||this.email!=undefined)&&!(this.email.match(pattern))){
        this.presentAlert("Please enter a valid email address.");
    }else{
   
           
      const cid=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")

          const tz=await this.storage.get('TIMEZONE')
       let url=this.config.domain_url+'saveManualFeedback';
       // let url='http://52.65.155.193/centrim_api/api/SaveFeedbackResponse';
       let headers=await this.config.getHeader();
       let body;
       let date=new Date();
       let img;
       if(this.img.length==0){
         img=null;
       }else{
         img=this.img
       }

         body={
           company_id:cid,
           branch_id:bid,
           public:1,
           type:this.type,
           category_id:this.cat_id,
           feedback:this.feedback,
           status:1,
           via_app:1,
           images:img,
           created_time:moment().tz(tz).format('YYYY-MM-DD'),
           user_type:3,
           resident_id:null,
           resident_name:null,
           name:this.name,
           email:this.email,
           phone:this.phone
         
       }
      
       console.log("body:",body,tz);
       this.showLoading()
     
       
 
       this.http.post(url,body,{headers}).subscribe((data:any)=>{
        
         console.log(data);
 
         if(data.status=="success"){
           this.dismissLoader();
         this.openModal();
         }else{
           this.dismissLoader();
           this.presentAlert('Something went wrong.Please try again.');
         }
 
       },error=>{
         console.log(error);
         this.dismissLoader();
         this.presentAlert('Something went wrong.Please try again.');
       })
     
  
}
  }

  async uploadImage(img){
    if(this.isLoading==false){
    this.showLoading();
    }
    let url=this.config.domain_url+'upload_file';
    let headers=await this.config.getHeader();;
    let body={file:img}
  console.log("body:",img);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log("uploaded:",res);
      this.img.push(res.data);
      this.dismissLoader();
    },error=>{
      console.log(error);
      this.dismissLoader();
    })
  }

  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
     
      componentProps: {
       data:this.imageResponse,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }
}
