import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  email:any;
  subscription:Subscription;
  constructor(private router: Router,private http:HttpClient,private config:HttpConfigService,
    private toastCntlr:ToastController,private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/login']) ;
    });
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  
  reset(){
    if(this.email){
      var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
      if(!pattern.test(this.email)){
        this.presentAlert('Please enter a valid email Id.');
      }else{
      let url=this.config.domain_url+'forgetPassword';
    
    let body={
      email:this.email
    }
 
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        if(res.status=='error'){
          this.presentAlert(res.message)
        }else{
        this.presentAlert('A mail has been send to your registered email id.');
        this.router.navigateByUrl('/login');
        }
        
      },error=>{
        console.log(error);
        this.presentAlert('Something went wrong. Please verify your email id. ');
        
      })
    }
    }else{
      this.presentAlert('Please enter an email id.')
    }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }
}
