import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallbookingSuccessPage } from './callbooking-success.page';

const routes: Routes = [
  {
    path: '',
    component: CallbookingSuccessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CallbookingSuccessPageRoutingModule {}
