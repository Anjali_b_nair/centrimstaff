import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-callbooking-success',
  templateUrl: './callbooking-success.page.html',
  styleUrls: ['./callbooking-success.page.scss'],
})
export class CallbookingSuccessPage implements OnInit {
date:any;
slot:any;
duration:any;
subscription:Subscription;
  constructor(private route:ActivatedRoute,private platform:Platform,private router:Router) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.date=this.route.snapshot.paramMap.get('date');
    this.slot=this.route.snapshot.paramMap.get('slot');
    this.duration=this.route.snapshot.paramMap.get('duration');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
}
