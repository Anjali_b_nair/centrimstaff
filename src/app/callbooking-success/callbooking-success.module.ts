import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CallbookingSuccessPageRoutingModule } from './callbooking-success-routing.module';

import { CallbookingSuccessPage } from './callbooking-success.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CallbookingSuccessPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [CallbookingSuccessPage]
})
export class CallbookingSuccessPageModule {}
