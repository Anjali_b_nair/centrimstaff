import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditActivityPageRoutingModule } from './edit-activity-routing.module';

import { EditActivityPage } from './edit-activity.page';
import { ActivityStaffComponent } from '../components/activity-staff/activity-staff.component';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditActivityPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [EditActivityPage,ActivityStaffComponent],
  entryComponents:[ActivityStaffComponent]
})
export class EditActivityPageModule {}
