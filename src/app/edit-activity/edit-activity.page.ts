import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, HostListener, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ViewAttendeesComponent } from '../components/view-attendees/view-attendees.component';
import { ActivityRecurringComponent } from '../components/activity-recurring/activity-recurring.component';
import { ActivityAttendeesComponent } from '../components/activity-attendees/activity-attendees.component';
import { File } from '@ionic-native/file/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { DOCUMENT } from '@angular/common';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { ActivityStaffComponent } from '../components/activity-staff/activity-staff.component';
import { ActivityWarningComponent } from '../components/activity-warning/activity-warning.component';
declare var $: any;
@Component({
  selector: 'app-edit-activity',
  templateUrl: './edit-activity.page.html',
  styleUrls: ['./edit-activity.page.scss'],
})
export class EditActivityPage implements OnInit {
  act_id:any;
  activity:any=[];
  subscription:Subscription;
  
  type:any={};
  // date:string;
  image:any=[];


  types:any=[];
typeId:any;
categories:any=[];
category:any=[];
branch:any=[];
facility:any=[];
title:any;
description:any;
minDate: string = new Date().toISOString();
date:any;
start:any;
end:any;
venue:any;
status:any=0;
img:any=[];
imageResponse:any=[];
docs:any=[];
doc:any=[];
docResponse:any=[];
// item:any;
flag:any;
id:any;
attendees:any[]=[];
attendeesCopy:any[]=[];
attendee_id:any;
startDates:any[]=[];
calendar:any[]=[];
calendar_type:any=[];
hide:boolean=true;
base64File:any;
callLink:boolean=false;
isLoading:boolean=false;
pageLoaded:boolean=false;

repeat:any={};


staffArray:any[]=[];
staff:any[]=[];
staffCopy:any[]=[];
disable:boolean=false;
utype:any;
listener;
selectAllCheckBox: any;
checkBoxes: HTMLCollection;
cal;
// customAlertOptions: any = {
  
//   // header: 'Facility',
//   // subHeader: 'Select All:',
//   message: '<ion-checkbox id="selectAllCheckBox2"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
// };
rvsettings:any;
slot:any;
waiting:any=false;
paid:any=false;
cost:any;
one_to_one:any=false;
assign:boolean=false;
limit_required:any;
payment_required:any;
loaded:boolean=false;
public customOptions: any = {
  header: "Calendar types",
  message: '<ion-checkbox id="selectAllCheckbox3"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
public categoryOptions: any = {
  header: "Categories"
};
  constructor(private route:ActivatedRoute,private platform:Platform,private router:Router,
    private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private nav:NavController,private actionsheetCntlr:ActionSheetController,
    private toastCntlr:ToastController,private alertCntlr:AlertController,
    private modalCntrl:ModalController,private file:File,private chooser:Chooser,
    private clip:Clipboard,private loadingCtrl:LoadingController,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2,private cdRef: ChangeDetectorRef) { }

    ngOnInit() {
     
      $('#summernote3').summernote({
        placeholder: 'Enter text here',
        tabsize: 2,
        height: 120,
        toolbar: [
          ['font', ['bold', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['link']],
        ]
      });
  
      
    
    }
  async ionViewWillEnter(){
    this.disable=false;
    this.image=[];
this.imageResponse=[];
this.docs=[];
this.doc=[];
this.docResponse=[];
this.types=[];
    this.categories=[];
    this.branch=[];
    this.attendees=[];
    this.facility=[];
    this.category=[];
    //get activity id through navigation 
  this.act_id=this.route.snapshot.paramMap.get('act_id');
  this.description=this.route.snapshot.paramMap.get('description');
//  $('#summernote3').summernote('code',this.description)
  this.getContent();
  this.getStaff();
  const rv=await this.storage.get('RVSETTINGS');
  this.rvsettings=rv
  if(rv==1){
    this.getRVsettings();
  }
    const bid=await this.storage.get("BRANCH")
    let headers=await this.config.getHeader();;

    
  let url2=this.config.domain_url+'article_types';
  this.http.get(url2,{headers}).subscribe((res:any)=>{
   
    for(var i in res.data){
      this.types.push(res.data[i]);
    }
    
    
    
  });
  let cat_url=this.config.domain_url+'activity_categories';
  
  this.http.get(cat_url,{headers}).subscribe((res:any)=>{
  
    // this.categories.push({id:0,category_name:'Select All'});
    res.data.forEach(element => {
      this.categories.push({id:element.id,category_name:element.category_name});
      // this.category.push(element.id)
    });
   
    
  })
  // this.storage.ready().then(()=>{
    const data=await this.storage.get("USER_TYPE")

      this.utype=data;
      const cid=await this.storage.get("COMPANY_ID")

// let headers=await this.config.getHeader();;
      // this.branch.push({id:0,name:'Select All'})
        // this.user_type=data;
        let url=this.config.domain_url+'branch_viatype';
        let body={
          company_id:cid,
          role_id:data
        }
        if(data=='1'){
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            if(data==1){
              // this.branch.push({id:0,name:'Select All'});
              }
            res.data.forEach(element => {
              this.branch.push({id:element.id,name:element.name});
            });
          })
        }else{
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          res.data.forEach(element => {
            this.branch.push({id:element.id,name:element.name});
          });
        
          
          // this.facility=this.branch[0].id.toString();
        })
      }

      this.calendar=[];
      // this.Calendar.push({id:0,wing:'All wings'});
     
       
          let branch=cid.toString();
          let body1={company_id:branch}
          let url1=this.config.domain_url+'get_calendar_types';
          this.http.post(url1,body1,{headers}).subscribe((res:any)=>{
           
            for(let i in res.data){
             
                
              let obj={id:res.data[i].calendar_id,name:res.data[i].calendar_types.type};
              this.calendar.push(obj)
            
            }
        
            
            
          },error=>{
            console.log(error);
            
          });
       
          (await this.config.getUserPermission()).subscribe((res: any) => {
            console.log('permissions:', res);
            let routes = res.user_routes;
            if (res.main_permissions.life_style==1&&routes.includes('activity.assign')) {
      
              this.assign = true
            } else {
      
              this.assign = false;
              
            }
          })
     


  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
  this.router.navigate(['/activities'],{replaceUrl: true}) ;
  }); 
  
  }

  setType(id){
    this.typeId=id;
    
    if(id==9){
      console.log('meeting');
      
      this.callLink=true;
    }else{
      this.callLink=false;
    }
    
  }


  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  selectBranch(ev,facility){
  
    if(facility.includes('0')){
      // this.facility=[];
      // let b=this.branch.splice(0,1);
      this.branch.forEach(element => {
        if(this.facility.includes(element.id.toString())){
            this.facility=this.facility
        }else{
          if((element.id.toString())!='0'){

         
        this.facility.push(element.id.toString());
          }
        }

      });
      
      console.log("fac:",this.facility);
      
      
    }
    
}
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
  }
    async getContent(){
      this.ngOnInit();
      
        const data=await this.storage.get("TOKEN")
          let token=data;
          const bid=await this.storage.get("BRANCH")


            let branch=bid.toString();
      let headers=await this.config.getHeader();
      let url=this.config.domain_url+'activity/'+this.act_id;
      // let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("act:",res);
          this.activity=res.activity;
          // this.image.pthis.activity.images.;
          // this.imageResponse=this.activity.images;
          this.date=new Date(this.activity.activity_start_date.split(' ')[0]).toISOString();
          
          
          this.type=this.activity.type;
          this.typeId=this.type.id.toString();
          if(this.typeId==9){
            console.log('meeting');
            
            this.callLink=true;
          }else{
            this.callLink=false;
          }
          this.title=this.activity.title;

          // this.date=this.activity.
          let d=this.activity.activity_start_date.split(" ",1);
          this.venue=this.activity.venue;
          this.status=this.activity.status;
          if(this.activity.rv_details){
          this.slot=this.activity.rv_details.slot_limit;
          if(this.activity.rv_details.paid_activity==1){
            this.paid=true;
            this.cost=this.activity.rv_details.cost
          }
          if(this.activity.rv_details.waiting_list==1){
            this.waiting=true
          }
          }
          if(this.activity.one_to_one_session==1){
            this.one_to_one=true
          }
          if(this.description==null || this.description=='<p><br></p>'||this.description=='null'){
            
            
          }else{
          
            
          $('#summernote3').summernote('code',this.description);
          }
        
          if(this.activity.category_details.length>0){
            this.activity.category_details.forEach(element => {
              this.category.push(element.id.toString());
            });
          }
          //   if(this.activity.category_details.length>0){
          //   for(let i=0;i<=this.activity.category_details.length;i++) {
          //     if(i==0){
          //       this.category=this.activity.category_details[0].id.toString();
          //     }else{
          //       if(this.activity.category_details.length>1){
          //         this.category.push(this.activity.category_details[i].id.toString())
          //   // this.category=this.category+','+this.activity.category_details[i].id.toString();
          //       }
          // // })
          //     }
          //   }
          //   }

          this.signupList();
            // if(this.activity.tagged_users.length>0){
            //   this.activity.tagged_users.forEach(element => {
            //     this.attendees.push(element.user_id);
            //   });
            // }
            if(this.activity.images.length>0){
              this.activity.images.forEach(element => {
                this.imageResponse.push(element.activity_image);
                this.image.push(element.activity_image);
              });
            }
            console.log("image:",this.imageResponse);
           
            if(this.activity.branch_details.length>0){
              this.activity.branch_details.forEach(element => {
                this.facility.push(element.id.toString())
              });
            }
            this.loaded=true;
          // if((this.activity.branch_details).length>0){
          //   for(let i=0;i<=this.activity.branch_details.length;i++){
          //     if(i==0){
          //       this.facility=this.activity.branch_details[0].id.toString();
          //     }else{
          //       if(this.activity.branch_details.length>1){
          //         this.facility.push(this.activity.branch_details[i].id.toString())
          //   // this.facility=this.facility+','+this.activity.branch_details[i].id.toString();
          //       }
          // // })
          //   }
          // // });
          //   }
          // }
          if(this.activity.custom_calendar.length>0){
            
            
            for(let i in this.activity.custom_calendar){
              
             
                  this.calendar_type.push(this.activity.custom_calendar[i].calendar_type.toString())
                  this.hide=false
          
            }
          }
          if(this.activity.assigned_to.length>0){
          this.activity.assigned_to.forEach(element => {
            if(this.staff.includes(element.user.user_id.toString())){

            }else{
            this.staff.push(element.user.user_id.toString());
           
            }
          });
        }
        let st=this.activity.start_time.split(/(?::| )+/);
        let sh;
        let eh;
        
        if(st[st.length-1]=='AM'){
          if(st[0]==12){
            sh='00'
          }else{
          sh=this.fixDigit(st[0]);
          }
        }else{
          if(st[0]==12){
            sh=st[0]
          }else{
          sh=parseInt(st[0])+12
          }
        }
        let et=this.activity.end_time.split(/(?::| )+/);
        if(et[et.length-1]=='AM'){
          if(et[0]==12){
            eh='00'
          }else{
          eh=this.fixDigit(et[0])
          }
        }else{
          if(et[0]==12){
            eh=et[0]
          }else{
          eh=parseInt(et[0])+12
          }
        }
        
          
          this.start=new Date(d+'T'+sh+':'+st[1]).toISOString() ;
          this.end=new Date(d+'T'+eh+':'+et[1]).toISOString();
          this.activity.attachments.forEach(el=>{
          
            let attch_name;
            attch_name=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('/') + 1);
            let ext=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('.') + 1);

            if(attch_name.toString().length>3){
              attch_name=attch_name.toString().substring(0,3)+'...'+ext;
            }
            this.docResponse.push({name:attch_name});
            this.docs.push(el.activity_attachment);
          })
          
         
        })
     
  }

  select(event){
    if(event.currentTarget.checked==true){
      this.status=1
    }else{
      this.status=0
    }
    console.log("notify",event.currentTarget.checked);
  }


 async captureImage(){
   
    // const options: CameraOptions = {
    //   quality: 75,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   // let base64Image = 'data:image/jpeg;base64,' + imageData;
    //   this.imageResponse.push('data:image/jpeg;base64,' + imageData)
    //           this.uploadImage('data:image/jpeg;base64,' +imageData);
      
    // }, (err) => {
    //   // Handle error
    // });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push( base64Image);
    this.uploadImage(base64Image);
  }
  
 async addImage(){

    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1,
    //   quality:100
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
    //   this.imagePicker.getPictures(options).then((results) => {
    //     for (var i = 0; i < results.length; i++) {
    //         // console.log('Image URI: ' + results[i]);
            
    //         if((results[i]==='O')||results[i]==='K'){
    //           console.log("no img");
              
    //           }else{
    //             // this.img.push(results[i]);
    //           this.imageResponse.push('data:image/jpeg;base64,' + results[i])
    //           this.uploadImage('data:image/jpeg;base64,' +results[i]);
    //           }
    //     }
    //   }, (err) => { });
    
    // // console.log("imagg:",this.img);

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  
    
    }
    async uploadImage(img){
      let headers=await this.config.getHeader();;
      if(this.isLoading==false){
        this.showLoading();
        }
      let url=this.config.domain_url+'upload_file';
      let body={file:img}
    console.log("body:",img);
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("uploaded:",res);
        this.image.push(res.data);
        this.dismissLoader();
        
      },error=>{
        console.log(error);
        this.dismissLoader();
      })
      console.log("uploaded:",this.image);
      
    }

    async selectImage() {
      const actionSheet = await this.actionsheetCntlr.create({
        header: "Select Image source",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            this.addImage();
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.captureImage();
          }
        },
        {
          text: 'Cancel',
          // role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
  
    addCalendar(ev){
      if(this.pageLoaded){
      console.log('evvv:',ev.currentTarget.checked)
      if(ev.currentTarget.checked==true){
        this.hide=false;
      }else{
        this.calendar_type=null;
        this.hide=true;
      }
    }
    }

    @HostListener('touchstart')
    onTouchStart() {
      console.log('pageload:',this.loaded)
      this.pageLoaded=true;
    }
   
    selectDoc(){
      this.chooser.getFile()
    .then(file => {
      console.log(file ? file : 'canceled');
      if(file){
        if(file.mediaType=="application/pdf" || file.mediaType=="application/msword" || file.mediaType=="application/doc"
        || file.mediaType=="application/ms-doc" || file.mediaType=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        || file.mediaType=="application/excel"|| file.mediaType=="application/vnd.ms-excel"|| file.mediaType=="application/x-excel"
        || file.mediaType=="application/x-msexcel"|| file.mediaType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
        this.uploadDoc(file.dataURI,file.name);
        
      }else{
        this.alert('File format not supported');
      }
    }
    })
    .catch((error: any) => console.error(error));
    }

    async uploadDoc(file,fname){
      let headers=await this.config.getHeader();;
      this.showLoading();
      let fileName ;
      let name=fname.split('.',1)
      if(name.toString().length>3){
        fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
      }
      this.docResponse.push({name:fileName});
     
             let  url = this.config.domain_url+'upload_file';
             let body={file:file}
         
             this.http.post(url,body,{headers}).subscribe((res:any)=>{
               console.log("doc up:",res);
               this.docs.push(res.data);
               this.dismissLoader();
             },error=>{
               console.log(error);
               this.dismissLoader();
             })
             
    
    }
    
    
    async  makeRecurring(){
      const modal = await this.modalCntrl.create({
        component: ActivityRecurringComponent,
       
        componentProps: {
          data:this.date,
          rep:this.repeat
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned !== null ||dataReturned.data!=undefined) {
          console.log("dataret:",dataReturned);
          if(this.repeat.status==2){
          this.repeat=dataReturned.role;
          this.startDates = dataReturned.data;
          }
          
        }
      });
      return await modal.present();
    }
    async addAttendees() {
      if(this.limit_required==1&&!this.slot){
        this.presentAlert('Please enter the slot limit.')
      }else{
      this.attendeesCopy=this.attendees.filter(x=>x);
      const modal = await this.modalCntrl.create({
        component: ActivityAttendeesComponent,
        cssClass:'full-width-modal',
        componentProps: {
          data:this.attendeesCopy,
          slot:this.slot,
          waiting:this.waiting,
          limit_required:this.limit_required 
          
        }
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data == null || dataReturned.data == undefined) {

        }else{
          console.log("dataret:",dataReturned);
          
          this.attendees=dataReturned.data;
          
          
        }
      });
      return await modal.present();
    }
    }
   
    
    async viewAttendees(){
      const modal = await this.modalCntrl.create({
        component: ViewAttendeesComponent,
        cssClass:'full-width-modal',
        componentProps: {
          data:this.attendees
          
        }
      });
      return await modal.present();
    }
    
    
    removeImg(i){
      this.imageResponse.splice(i,1);
      this.image.splice(i,1);
    }
    
    
    removeDoc(i){
      this.docResponse.splice(i,1);
      this.docs.splice(i,1);
    }


    async alert(mes){
      // let message;
      // if(type==2){
      //   message='File format not supported'
      // }
      const alert = await this.alertCntlr.create({
        mode:'ios',
        message: mes,
        backdropDismiss:true
      });
    
      await alert.present();
    }
  


    async publish(){
      this.disable=true;
      let a=this.attendees[0];
      let desc=$('#summernote3').summernote('code');
      if(this.attendees.length==0){
        this.attendee_id=null;
      }
      this.attendees.forEach(ele=>{
        if(ele==a){
        this.attendee_id=ele;
        }else{
          this.attendee_id=this.attendee_id+','+ele
        }
      })
      
        const cid=await this.storage.get("COMPANY_ID")
        const bid=await this.storage.get("BRANCH");
          const uid=await this.storage.get("USER_ID")

            let s=moment(this.start).format('HH:mm')
            let stime=moment(this.start).format('hh:mm A');
            let etime=moment(this.end).format('hh:mm A');
            let img;
            let doc;
            let url=this.config.domain_url+'activity/'+this.act_id;
            if(this.image.length==0){
              img=null
            }else{
              img=this.image
            }
            if(this.docs.length==0){
              doc=null
            }else{
              doc=this.docs
            }
          
            
            if(this.startDates.length==0){
              this.startDates.push(moment(this.date).format('yyyy-MM-DD'))
            }
            let category;
  
    
    if(this.category.length>0){
      let c=this.category[0];
        this.category.forEach(ele => {
          if(ele==c){
            category=c;
          }else{
            category=category+','+ele;
          }
        });
          
        
     
    }else{category=null}
    // let facility;
    // console.log("facility:",this.facility);
    
    // if(this.facility.length>0){
      
      let facility=[];

    if(Array.isArray(this.facility)){
      facility=this.facility;
    
    }else{
      facility.push(this.facility.toString());
    
    }
    let staff=[];
   
    if(this.staff.length>0){
     
   
  }else{
    staff=null;
  }
  // }else{facility=null}
  let rv,slot,waiting,paid,cost;
        if(this.rvsettings==1){
          rv=1;
          if(this.limit_required==1){
            slot=this.slot;
            }else{
              slot=null
            }
          if(this.waiting){
            waiting=1
          }else{
            waiting=0
          }
          if(this.paid){
            paid=1
            cost=this.cost
          }else{
            paid=0;
            cost=null
          }
        }else{
          rv=0;
          slot=null
        }
        let one_to_one;
        if(this.one_to_one){
          one_to_one=1
        }else{
          one_to_one=0
        }
            for(let i=0;i<this.startDates.length;i++){
              let ac_date=this.startDates[i]+' '+s
            
              
            let body={
              activity:{
              activity_type:this.typeId,
              title:this.title,
              activity_info:desc,
              activity_start_date:moment(ac_date).format('yyyy-MM-DD HH:mm:ss'),
              start_time:stime,
              end_time:etime,
              venue:this.venue,
              created_by:uid,
              status:this.status,
              web_display:1,
              enable_notification:1,
              company_id:cid,
              updated_by:uid,
              one_to_one_session:one_to_one
            },
              categories:this.category,
              branches:facility,
              document:doc,
              images:img,
              attendees:this.attendee_id,
              calendar_type:this.calendar_type,
              assigned_to:staff,
              rv_required:rv,
          slot_limit:slot,
          paid_activity:paid,
          cost:cost,
          waiting_list:waiting,
          branch_id:bid
          

    
            }
            let headers=await this.config.getHeader();
            console.log("body:",body);
            this.http.put(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.presentAlert("Activity edited successfully.");
              this.router.navigate(['/activities'],{replaceUrl: true});
              this.disable=false;
              
            },error=>{
              console.log(error);
              this.presentAlert("Something went wrong. Please try again.");
              this.disable=false;
            })
          }
          
     
      
    }

    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'      
      });
      alert.present(); //update
    }

  back(){
    this.router.navigate(['/activity-details',{act_id:this.act_id}],{replaceUrl: true});
  }


 
  
  generateRoom(){
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  
    for (let i = 0; i < 9; i++) {
  
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
  
      
    }
    return outString;
  }
  
  async generate(){
    let jitsiRoom=this.generateRoom();
   
     const data=await this.storage.get("COMPANY_ID")

       let url=this.config.domain_url+'generatecallurl';
       let headers=await this.config.getHeader();
       let body={company_id:data}
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
         let callLink=res.vdocalllink+jitsiRoom;
        //  this.jitsiCall(callLink);
        //  this.sendLink(callLink);
        this.callPrompt(callLink);
         
       })
     
  
  }
  
  async callPrompt(callLink) {
    
    const alert = await this.alertCntlr.create({
      mode:'ios',
      header: 'Copy&paste video call link',
      backdropDismiss:true,
      message: 'Copy and paste the link in the description field.',
      inputs: [
        {
          name: 'link',
          type: 'text',
          
          value: callLink,
          disabled:true
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Copy & Paste',
          handler: () => {
           
            this.clip.copy(callLink);
            this.clip.paste().then(
              (resolve: string) => {
                let des=$('#summernote3').summernote('code')+'<p><a href="'+resolve+'" target="_blank">Click here to start video call</a><br></p>';
                $('#summernote3').summernote('code',des);
                console.log('resolve:',resolve);
               },
               (reject: string) => {
                 console.log("err:",reject);
                 
               }
             );
           
          
  
          }
        }
      ]
    });
    
    await alert.present();
   
  }
  async showLoading() {
    this.isLoading=true;
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    this.isLoading=false;
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async presentActionSheet() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: 'Select',
      buttons: [{
        text: 'Image',
        // role: 'destructive',
        icon: 'image-outline',
        handler: () => {
          this.selectImage();
          console.log('Delete clicked');
        }
      }, {
        text: 'Document',
        icon: 'document-outline',
        handler: () => {
          this.selectDoc();
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        // role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  confirm(){
    this.disable=true;
    let desc=$('#summernote').summernote('code');
   
  let start=new Date(moment().format('yyyy-MM-DD')+' '+moment(this.start).format('hh:mm A')).getTime();
  let end=new Date(moment().format('yyyy-MM-DD')+' '+moment(this.end).format('hh:mm A')).getTime();
    
    if(this.title==undefined||this.title==''){
      this.presentAlert('Please enter the title.');
      this.disable=false;
    }
    else if(this.typeId==undefined||this.typeId==''){
      this.presentAlert('Please select wellness dimension.');
      this.disable=false;
    }else if(this.start==undefined||this.start==''||this.end==undefined||this.end==''){
      this.presentAlert('Please enter start & end time.');
      this.disable=false;
    }else if(start>end){
      this.presentAlert('The end time must be a time after start time.');
      this.disable=false;
    }else if(Array.isArray(this.facility)&&this.facility.length==0){
      this.presentAlert('Please select atleast one facility.');
      this.disable=false;
    }else if(this.rvsettings==1&&(this.slot==undefined||this.slot=='')){
      this.presentAlert('Please enter the slot limit.');
      this.disable=false;
    }else if(this.rvsettings==1&&this.paid&&(this.cost==''||this.cost==undefined)){
      this.presentAlert('Please enter the cost.');
      this.disable=false;
    }else if(!this.hide&&!this.calendar_type){
      this.presentAlert('Please select a calendar type.');
      this.disable=false;
    }else{
      
      if(this.calendar_type&&this.calendar_type.length){
        this.validate_Activity();
      }else{
      this.publish();
      }
      
    }
  }
  async validate_Activity(){
    const cid=await this.storage.get("COMPANY_ID")
        
    let headers=await this.config.getHeader();
    let s=moment(this.start).format('HH:mm')
    let stime=moment(this.start).format('hh:mm A');
    let ac_date=moment(this.date).format('yyyy-MM-DD')+' '+s
    let url=this.config.domain_url+'validate_activity';
    let body={
      company_id:cid,
      activity_start_date:moment(ac_date).format('yyyy-MM-DD HH:mm:ss'),
      start_time:stime,
      calendar_type:this.calendar_type
    }
      console.log('validatebody:',body);
      
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('validate:',res);
      if(res.status=='success'){
        this.publish();
      }else{
        this.activityTimeWarning();
      }
      
     })
  }
  async activityTimeWarning(){
    const modal = await this.modalCntrl.create({
      component: ActivityWarningComponent,
      cssClass: 'full-width-modal'
      
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {
        this.publish();
      }else{
        this.disable=false;
      }
    });
    return await modal.present();
  }
  async viewImage(){
    const modal = await this.modalCntrl.create({
      component: AttchedImagesComponent,
     
      componentProps: {
       data:this.imageResponse,
       
       
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      
    });
    return await modal.present();
  }



  async getStaff(){
    
      const bid=await this.storage.get("BRANCH")

    
      
    this.staffArray=[];
   
    this.staffArray.push({user_id:'0',name:'All'})
    let url1=this.config.domain_url+'activity_staffs';
    // let headers=await this.config.getHeader();
    // this.storage.ready().then(()=>{
      let headers=await this.config.getHeader();
      const utype=await this.storage.get("USER_TYPE")

        const uid=await this.storage.get("USER_ID")

      const cid=await this.storage.get("COMPANY_ID")

        let body={company_id:cid,status:1}
          this.http.post(url1,body,{headers}).subscribe((res:any)=>{
            console.log("arr:",res);
            let staff=res.data.super_admins.concat(res.data.managers).concat(res.data.staffs)
            staff.forEach(element => {
              this.staffArray.push({user_id:element.user_id,name:element.name})
            });
            // if(utype==3){
            //   this.staff_id=uid;
            //   this.staffArray.forEach(ele=>{
            //     // console.log("el:",ele.id,this.wingId);
                
            //     if(ele.user_id==uid){
            //       this.staff=ele.name;
            //     }
            //   })
            // }else{
            //   this.staff='All';
            //   this.staff_id='all'
            // }
          })
          
       
  }
  selectStaff(ev,staff){
    console.log("staffselect:",staff);
    if(staff.includes('0')){
      this.staff=[];
      this.staffCopy=[];
      // let b=this.branch.splice(0,1);
      this.staffArray.forEach(element => {
        if(this.staff.includes(element.user_id.toString())){
  
        }else{
          if((element.user_id.toString())!='0'){
  
         
        this.staff.push(element.user_id.toString());
       
          }
        }
        console.log("staffAee:",this.staff);
      });
      
      
      
      
    }
    
  }


  openselecter(a) {
    console.log(a);
    console.log(this.calendar);
    console.log(this.calendar_type);
    this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
    a.open().then((alert) => {
      this.selectAllCheckBox =
        this.document.getElementById("selectAllCheckbox3");
              // check the array "calender_type" is undefined or not and if not check the length of the array with Data "calender" array
      // if (
      //   this.calendar_type != undefined &&
      //   this.calendar.length == this.calendar_type.length
      // ) {
      //   console.log("1079");
      //   // if selected option is clicked before , then select all option check button is checked using HTMLButtonElement click function
        (this.selectAllCheckBox as HTMLButtonElement).click();
      // } else console.log("1080");

      for (let checkbox of this.checkBoxes) {
      
        if (checkbox.getAttribute("aria-checked") === "false") {
          (this.selectAllCheckBox as HTMLButtonElement).click();
        }
      }
     
    
      this.listener = this.renderer.listen(
        this.selectAllCheckBox,
        "click",
        () => {
          if (this.selectAllCheckBox.checked) {
            this.cdRef.detectChanges();
            console.log("tyruue");
            // this.calendar_type = [];
            for (let checkbox of this.checkBoxes) {
              console.log(checkbox);
              if (checkbox.getAttribute("aria-checked") === "false") {
                (checkbox as HTMLButtonElement).click();
              }
            }
          } else {
            console.log("false");
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked") === "true") {
                (checkbox as HTMLButtonElement).click();
                this.branch.forEach((element) => {
                  if (this.calendar_type.includes(element.id.toString())) {
                  } else {
                    if (element.id.toString() != "0") {
                      this.calendar_type.push(element.id.toString());
                    }
                  }
                });
              }
            }
          }
        }
      );
      alert.onWillDismiss().then(() => {
        this.listener();
        console.log('hh')
        console.log(this.calendar_type)
        this.cal = false;
        for(let checkbox of this.checkBoxes){
          if(checkbox.getAttribute("aria-checked")==='true'){
            this.cal = true
          }
        }
      
        if(this.cal == false){
          this.calendar_type =[];
        }
        if(this.calendar_type == null){
          this.calendar_type =[]
        }
        console.log('alendar:',this.calendar_type);
      });
      
    });
    
  }
  async getRVsettings(){
    const bid=await this.storage.get("BRANCH");
    let headers=await this.config.getHeader();;
    let url=this.config.domain_url+'get_rv_settings/'+bid
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log('rvset:',res);
      
      this.limit_required=res.data.limit_required;
      this.payment_required=res.data.payment_required;
    })
  }
  async addStaff(i) {
    this.staffCopy=this.staff.filter(x=>x);
    const modal = await this.modalCntrl.create({
      component: ActivityStaffComponent,
      cssClass:'full-width-modal',
      mode:'md',
      componentProps: {
        data:this.staffCopy, 
        flag:i
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {

   
      
        this.staff=dataReturned.data;
       
        
      }else{
        this.staffCopy=this.staff;
      }
    });
    return await modal.present();
  }

  async signupList(){
    
   
    
   
    const bid=await this.storage.get("BRANCH");
    const cid=await this.storage.get("COMPANY_ID")
    const uid=await this.storage.get("USER_ID")
    const utype=await this.storage.get("USER_TYPE")
    let headers=await this.config.getHeader();
  
  
    let uri=this.config.domain_url+'activity_all_attendees';
    let body={
      activity_id:this.act_id,
      role_id:utype,
      company_id:cid,
      branch_id:bid,
      user_id:uid
    }
  
    this.http.post(uri,body,{headers}).subscribe((res:any)=>{
      console.log('allatte:',res);
  
      res.data.activity_attendee.forEach(element => {
        if(element.signups.action==1){
       
        this.attendees.push(element.user_id);
        }
      });
      res.data.contact_attendee.forEach(el => {
        if(el.signups.action==1){
     
      this.attendees.push(el.user_id)
        }
      })
  
     
    },error=>{
      
    })
  }
  
}
