import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular'
;
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
import { CrfilterComponent } from '../components/crfilter/crfilter.component';
import { Platform, PopoverController } from '@ionic/angular';
import { MoreVisitorsComponent } from '../components/more-visitors/more-visitors.component';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-visitors-list',
  templateUrl: './visitors-list.page.html',
  styleUrls: ['./visitors-list.page.scss'],
})
export class VisitorsListPage implements OnInit {
data:any={};
visitor:any[]=[];
minDate: string=new Date().toISOString() ;
date:any;
tabStatus:boolean=true;
start:any;
end:any;
resStatus='1';
resData:any={};
residents:any[]=[];
filterText:string;
onPremise:any;
terms:any;
terms1:any;
resCheckinCount:any;
subscription:Subscription;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private popCntlr:PopoverController,private platform:Platform,private router:Router) { }

  ngOnInit() {
  }
  cancel(){
    // this.hide=false;
    this.terms='';
    this.terms1='';
  }
  async ionViewWillEnter(){
    this.terms='';
    this.terms1='';
    this.minDate= new Date().toISOString();
    this.date= new Date().toISOString();
    
    
      const cid=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")

          const zone=await this.storage.get('TIMEZONE')
            let url=this.config.domain_url+'get_visitor_list';
            // let url='http://52.65.155.193/centrim_api/api/get_visitor_list';
            let headers=await this.config.getHeader();
            let body={
              date: moment().tz(zone).format('yyyy-MM-DD'),
              current_datetime:moment().tz(zone).format('yyyy-MM-DD HH:mm:ss')
            }
            console.log(body,cid,bid);
            
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.data=res;
              this.visitor=res.data;
              
            })
         
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.router.navigate(['/menu'])
   
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }

  getTime(time){
    return moment(time).format('hh:mm A');
  }
  getDate(time){
    return moment(time).format('DD MMM YYYY');
  }


  async getContent(){
   
      const cid=await this.storage.get("COMPANY_ID")

        const bid=await this.storage.get("BRANCH")

          const zone=await this.storage.get('TIMEZONE')
            let url=this.config.domain_url+'get_visitor_list';
            // let url='http://52.65.155.193/centrim_api/api/get_visitor_list';
            let headers=await this.config.getHeader();
            let body={
              date: moment(this.date).format('yyyy-MM-DD'),
              current_datetime:moment().tz(zone).format('yyyy-MM-DD HH:mm:ss')
            }
            console.log(body,cid,bid);
            
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.data=res;
              this.visitor=res.data;
              
            })
          
  }

  changeContent(i){
    if(i==1){
      this.tabStatus=true;
      this.ionViewWillEnter();
    }else{
      this.tabStatus=false;
      this.start=moment(this.minDate).subtract(1,'months').format('YYYY-MM-DD');
    this.end=moment(this.minDate).format('YYYY-MM-DD');
    this.filterText=moment(this.start).format('MMM DD')+' - '+moment(this.end).format('MMM DD');
      console.log('start:',this.start);
      
      this.getResidents();
    }

  }

    async datefilter(ev){
      const popover = await this.popCntlr.create({
        component: CrfilterComponent,
        event: ev,
        backdropDismiss:true,
        cssClass:'filterpop1'
        
        // translucent: true
      });
      popover.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data!=undefined || dataReturned.role!='backdrop') {
          console.log("dataret:",dataReturned);
          
          this.start = dataReturned.data;
          this.end = dataReturned.role;
          this.filterText=moment(this.start).format('MMM DD')+' - '+moment(this.end).format('MMM DD');
          this.getResidents();
          
        }
      });
      return await popover.present();
    }
    selectResStatus(){
      this.filterText=moment(this.start).format('MMM DD')+' - '+moment(this.end).format('MMM DD');
      this.getResidents();
    }
    async getResidents(){
      
     
        const cid=await this.storage.get("COMPANY_ID")

          const bid=await this.storage.get("BRANCH")

            const zone=await this.storage.get('TIMEZONE')
              let url=this.config.domain_url+'get_resident_visitor_list';
              // let url='http://52.65.155.193/centrim_api/api/get_resident_visitor_list';
              let headers=await this.config.getHeader();
              let body={
                sdate: this.start,
                edate:this.end,
                type:this.resStatus
              }
              console.log(body,cid,bid);
              
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log("resid:",res);
                this.resData=res;
                // if(this.start===this.end){
                  this.resCheckinCount=res.checkedin_today;
                // }
                this.residents=res.data;
                
              })


              // let url1='http://52.65.155.193/centrim_api/api/residents_on_premises';
              let url1=this.config.domain_url+'residents_on_premises';
      let body1={branch_id:bid}

      // let headers=await this.config.getHeader();
      this.http.post(url1,body1,{headers}).subscribe((data:any)=>{
       
        console.log("data:",data);
       this.onPremise=data.data.length;
      
          
      },error=>{
        
        console.log(error);
      });
            
    }

async viewMore(ev,item){
  const popover = await this.popCntlr.create({
    component: MoreVisitorsComponent,
    event:ev,
    cssClass:'cbmorepop',
    backdropDismiss:true,
    componentProps:{
      data:item
    },
    
    
    
  });
  return await popover.present();
}
}
