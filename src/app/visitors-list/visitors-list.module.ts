import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisitorsListPageRoutingModule } from './visitors-list-routing.module';

import { VisitorsListPage } from './visitors-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisitorsListPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [VisitorsListPage]
})
export class VisitorsListPageModule {}
