import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvResTaskListPageRoutingModule } from './rv-res-task-list-routing.module';

import { RvResTaskListPage } from './rv-res-task-list.page';

import { ApplicationPipesModule } from '../application-pipes.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvResTaskListPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    RvResTaskListPage,
    
  ],
 
})
export class RvResTaskListPageModule {}
