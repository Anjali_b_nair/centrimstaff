import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvResTaskListPage } from './rv-res-task-list.page';

const routes: Routes = [
  {
    path: '',
    component: RvResTaskListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvResTaskListPageRoutingModule {}
