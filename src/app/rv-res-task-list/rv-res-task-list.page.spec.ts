import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvResTaskListPage } from './rv-res-task-list.page';

describe('RvResTaskListPage', () => {
  let component: RvResTaskListPage;
  let fixture: ComponentFixture<RvResTaskListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvResTaskListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvResTaskListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
