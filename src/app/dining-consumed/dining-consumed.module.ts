import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningConsumedPageRoutingModule } from './dining-consumed-routing.module';

import { DiningConsumedPage } from './dining-consumed.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningConsumedPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningConsumedPage]
})
export class DiningConsumedPageModule {}
