import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningConsumedPage } from './dining-consumed.page';

describe('DiningConsumedPage', () => {
  let component: DiningConsumedPage;
  let fixture: ComponentFixture<DiningConsumedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningConsumedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningConsumedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
