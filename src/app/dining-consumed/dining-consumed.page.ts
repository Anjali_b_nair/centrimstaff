import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ModalController, LoadingController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { DiningAddItemsComponent } from '../components/dining/dining-add-items/dining-add-items.component';
import { ItemOptionsComponent } from '../components/dining/item-options/item-options.component';
import { ServingChangeCountComponent } from '../components/dining/serving-change-count/serving-change-count.component';
import { ServingRefusedComponent } from '../components/dining/serving-refused/serving-refused.component';
import { UnserveComponent } from '../components/dining/unserve/unserve.component';
import { ServingConsumedComponent } from '../components/dining/serving-consumed/serving-consumed.component';
import { UndoRefuseComponent } from '../components/dining/undo-refuse/undo-refuse.component';

@Component({
  selector: 'app-dining-consumed',
  templateUrl: './dining-consumed.page.html',
  styleUrls: ['./dining-consumed.page.scss'],
})
export class DiningConsumedPage implements OnInit {
  subscription:Subscription;
  area:any;
  area_id:any;
  ser_time:any;
  servetime:any;
  date:any;
  details:any=[];
  detailsCopy:any=[];
  wingArray:any[]=[];
  hide:boolean=true;
  // wing:any;
  selectedItem: any[] = [{ Resident: 0, item: 0 , qty:1}];
  selectedAdd: any[]  =[{ Resident: 0, item: 0 ,qty:1}]
  order_id: any;
  terms:any;
  menu_id:any;
  selected:any=[];
  tables:any=[];
  residents:any=[];
  wing:any=[];
  wingName:any=[];
  serving_order:any;
  count:any={};
  edit:boolean=false;
  order:boolean=false;
  loading:boolean=true;
  status:any='1';
has_pcs:any;

  constructor(private router:Router,private platform:Platform,private modalCntrl:ModalController,
    private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private loadingCtrl:LoadingController,private popCntlr:PopoverController,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
   
    this.ser_time=this.route.snapshot.paramMap.get('ser_time_id');
    this.area_id=this.route.snapshot.paramMap.get('area_id');
    this.servetime=this.route.snapshot.paramMap.get('ser_time');
    this.date=this.route.snapshot.paramMap.get('date');
    this.wing=JSON.parse(this.route.snapshot.paramMap.get('wing'));
    // this.wingName=JSON.parse(this.route.snapshot.paramMap.get('wingName'));
    this.hide=true;
    this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
    this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
  //   if(this.area_id==0){
  // this.getWing();
  //   }else{
      // this.getDetails(0);
    //   this.getTableDetails();
    // }
    // this.getTableDetails();
    this.getDetailsNew();
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
      if (res.main_permissions.dining==1&&routes.includes('meal_status.edit')) {
  
        this.edit= true
      } else {
  
        this.edit = false;
        
      }
      if (res.main_permissions.dining==1&&routes.includes('place_order')) {

        this.order= true
      } else {

        this.order = false;
        
      }
    })


     this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
       this.back();
       }); 
       
       }
       ionViewWillLeave() { 
       this.subscription.unsubscribe();
       }
       
         back(){
          this.popCntlr.dismiss(null,null,'countPop');
           this.router.navigate(['/din-choose-dt-mlt-consumed'],{replaceUrl:true})
         }

         async getDetailsNew(){
          
         
          const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
         
          let headers=await this.config.getHeader();
          let url=this.config.domain_url+'get_serving_resident_detail_list';
          console.log(url);
          let body;
          body={
            serving_time_id:this.ser_time,
            date:moment(this.date).format('YYYY-MM-DD'),
            wing_id_arr:this.wing,
            serving_area_id:this.area_id,
            consume_page:1
          }
          if(this.terms){
            body.keyword=this.terms
          }
          console.log('body:',body);
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            
            console.log('res:',res);
            this.details=[];
            this.detailsCopy=[];
            console.log('deta1:',this.details,this.detailsCopy)
            if(res.data.menu_id){
              this.count=res.data.count;
              this.has_pcs=res.data.has_pcs;
            this.serving_order=res.data.serving_order;
            this.menu_id=res.data.menu_id;
            for(let i in res.data.residents){
            this.details.push({res:res.data.residents[i],expanded:false})
            this.detailsCopy.push({res:res.data.residents[i],expanded:false})
            }
            this.loading=false;
            console.log('deta:',this.details,this.detailsCopy)
          }else{

            this.back();
              this.presentAlert('No menu available')
          }
          
          },error=>{
            this.loading=false
            console.log(error);
            
          })
         }

         async getDetails(wing){
          this.details=[];
          this.detailsCopy=[];
          const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
         
          let headers=await this.config.getHeader();
          let url=this.config.domain_url+'serving_item_listing';
          console.log(url);
          let body={
            company_id:cid,
            servingtimeid:this.ser_time,
            date:moment(this.date).format('YYYY-MM-DD'),
            serveareaid:this.area_id,
            wing_id:this.wing
          }
          console.log('body:',body);
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            
            console.log('res:',res);
            if(res.data.menu){
              this.count=res.data.count;
            this.serving_order=res.data.serving_order;
            this.menu_id=res.data.menu.id;
            for(let i in res.data.residents){
            this.details.push({res:res.data.residents[i],expanded:false})
            this.detailsCopy.push({res:res.data.residents[i],expanded:false})
            }
            this.loading=false;
            console.log('deta:',this.details)
          }else{

            this.back();
              this.presentAlert('No menu available')
          }
          },error=>{
            this.loading=false
          })
        }



        async getWing(){
          this.wingArray=[];
        
        
           const bid=await this.storage.get("BRANCH")
           let headers=await this.config.getHeader();
        
             let branch=bid.toString();
             let body={company_id:branch}
             let url=this.config.domain_url+'branch_wings/'+bid;
             this.http.get(url,{headers}).subscribe((res:any)=>{
               console.log("wing:",res);
               let expanded;
               for(let i in res.data.details){
                console.log("data:",res.data.details[i]);
                if(i=='0'){
                  expanded=true;
                }else{
                  expanded=false
                }
              let obj={id:res.data.details[i].id,wing:res.data.details[i].name,expanded:expanded};
           
                 this.wingArray.push(obj);
              
               
               }
        
               this.getDetailsNew();
             this.wing=this.wingArray[0].id;
               console.log("array:",this.wingArray);
               
               
             },error=>{
               console.log(error);
               
             })
          
         }
        //  expand(item){
        //   if (item.expanded) {
        //     item.expanded = false;
        //   } else {
        //     this.wingArray.map(listItem => {
        //       if (item == listItem) {
        //         listItem.expanded = !listItem.expanded;
        //         if(listItem.expanded){
        //           this.getDetails(listItem.id);
        //           this.wing=listItem.id;
        //           this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
        //           this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
        //         }
        //       } else {
        //         listItem.expanded = false;
        //       }
        //       return listItem;
        //     });
        //   }
        //  }
         expandTab(item){
          if (item.expanded) {
            item.expanded = false;
          } else {
            this.tables.map(listItem => {
              if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                if(listItem.expanded){
                  this.showTableResidents(listItem.table.id);
                  
                  // this.wing=listItem.id;
                  this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                  this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
                }
              } else {
                listItem.expanded = false;
              }
              return listItem;
            });
          }
         }
  
         async refuse(item,i) {
          // if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
      
          // } else if (this.selectedItem[0].Resident == item.res.user.user_id) {
            const modal = await this.modalCntrl.create({
              component: ServingRefusedComponent,
              cssClass: 'full-width-modal'
              
            });
            modal.onDidDismiss().then((dataReturned) => {
              if (dataReturned.data) {
                this.refuseItem(dataReturned.data, dataReturned.role,i,item);
              }
            });
            return await modal.present();
          // }
        }
       
        async dietaryPreference(item) {
          const modal = await this.modalCntrl.create({
            component: DietaryPreferencesComponent,
            cssClass: 'dining-dpreference-modal',
            componentProps: {
              details: item
            }
          });
          modal.onDidDismiss().then((dataReturned) => {
      
          });
          return await modal.present();
        }
      
        setAllergies(item) {
          let a = [];
          a = item.split(/\, +/);
          return a;
        }
      
        async serve(item,i) {
          
            const cid = await this.storage.get('COMPANY_ID');
            const bid = await this.storage.get('BRANCH');
            const uid = await this.storage.get('USER_ID');
            // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
            let headers=await this.config.getHeader();
            let url = this.config.domain_url + 'serve_orders';
      
            let items = [];
            let add=[];
            let quantity=[];
            if(this.selectedItem.length>0&&this.selectedItem[0].Resident!=0){
            this.selectedItem.forEach(ele => {
              items.push(ele.Item)
              quantity[ele.Item]=ele.qty
            })
          }
          if(this.selectedAdd.length>0&&this.selectedAdd[0].Resident!=0){
            this.selectedAdd.forEach(ele => {
              add.push(ele.Item)
              quantity[ele.Item]=ele.qty
            })
          }
      
            let body = {
              company_id: cid,
              served_by: uid,
              order_id: this.order_id,
              order_details_id: items,
              order_add_details_id: add,
              itemqunatity:quantity,
              serveqty:1
            }
            console.log('body:',body)
            this.http.post(url, body, { headers }).subscribe((res: any) => {
      
              console.log('res:', res);
              this.selectedItem= [{ Resident: 0, item: 0 , qty:1 }];
              this.selectedAdd= [{ Resident: 0, item: 0 , qty:1 }];
              // if(this.count.toserved>0){
              //   this.count.toserved--;
              //   }
                this.count.served++;
                
              this.details[i].res.tag_status='served';
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='served';
              
              
            },error=>{
              console.log(error)
            })
          
        }
        async leave(item,i) {
          const cid = await this.storage.get('COMPANY_ID');
          const bid = await this.storage.get('BRANCH');
          const uid = await this.storage.get('USER_ID');
          // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
          let headers=await this.config.getHeader();
          let url = this.config.domain_url + 'mark_dietry_leave';
      
      
          let body = {
            company_id: cid,
            created_by: uid,
            consumer_id: item.res.user.user_id,
            start: moment(this.date).format('YYYY-MM-DD')
      
          }
          this.http.post(url, body, { headers }).subscribe((res: any) => {
      
            console.log('res:', res);
            this.details[i].res.is_leave=1
            this.details[i].res.tag_status='leave';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.is_leave=1
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='leave';
            this.selectedItem= [{ Resident: 0, item: 0 ,qty:1 }];
            this.selectedAdd= [{ Resident: 0, item: 0 , qty:1 }];
            this.count.leave++;
          },error=>{
            console.log(error)
          })
        }
        async refuseItem(reason, otherReason,i,item) {
          const cid = await this.storage.get('COMPANY_ID');
          const bid = await this.storage.get('BRANCH');
          const uid = await this.storage.get('USER_ID');
          // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
          let headers=await this.config.getHeader();
          let url = this.config.domain_url + 'refuse_orders';
      
          let items = [];
          let add=[];
          // if(this.selectedItem&&this.selectedItem.length>0){
          //   this.selectedItem.forEach(ele => {
          //     items.push(ele.Item)
          //   })
          // }else{
            item.res.order.order_items.forEach(ele=>{
              items.push(ele.id)
            });
            item.res.additional_order.forEach(ele=>{
              add.push(ele.id)
            });
            this.order_id=item.res.order.id
          // }
      
          let body = {
            company_id: cid,
            served_by: uid,
            order_id: this.order_id,
            order_details_id: items,
            order_add_details_id:add,
            reason: reason,
            reason_text: otherReason
          }
  
          console.log('refbody:',body)
          this.http.post(url, body, { headers }).subscribe((res: any) => {
      
            console.log('res:', res);
            this.count.refused++;
            if(this.count.to_consume>0){
              this.count.to_consume--
            }
            this.details[i].res.tag_status='refused';
            this.details[i].res.order.refuse_reason=reason;
            this.details[i].res.order.refuse_comment=otherReason;
            this.details[i].res.order.order_items.map(x=>x.serving_status=2);


            console.log('ref:',this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res);
            
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='refused';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_reason=reason;
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_comment=otherReason;
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items.map(x=>x.serving_status=2);
            // this.detailsCopy[i].res.tag_status='refused';
            // this.detailsCopy[i].res.order.refuse_reason=reason;
            // this.detailsCopy[i].res.order.refuse_comment=otherReason;
            // this.detailsCopy[i].res.order.order_items.map(x=>x.serving_status=2);
            this.selectedItem= [{ Resident: 0, item: 0 , qty: 1 }];
            this.selectedAdd= [{ Resident: 0, item: 0 , qty: 1 }];
           
          },error=>{
            console.log(error)
          })
        }
        // selectItems(item, order_id, order,i,itemQty) {
        //   if(i==1){
        //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
        //     this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
        //     this.order_id = order;
        //     console.log('first entry');
        //     this.selectedItem.splice(0, 1);
        //   } else {
        //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
        //     if (targetIdx >= 0) {
        //       const ItemIdx = this.selectedItem.map(item => item.Item).indexOf(order_id);
        //       if (ItemIdx >= 0) {
        //         this.selectedItem.splice(ItemIdx, 1);
        //         console.log('item already presented');
        //         if (this.selectedItem.length == 0) {
        //           this.selectedItem.push({ Resident: 0, item: 0 ,qty: 1 })
        //         }
        //       } else {
        //         this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
        //         this.order_id = order;
        //       }
        //     } else {
        //       console.log('another resident');
        //     }
        //   }
        // }else{
  
        //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
        //     if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
        //       this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
        //       this.order_id = order;
        //       console.log('first entry');
        //       this.selectedAdd.splice(0, 1);
        //     } else {
        //       const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
        //       if (targetIdx >= 0) {
        //         const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
        //         if (ItemIdx >= 0) {
        //           this.selectedAdd.splice(ItemIdx, 1);
        //           console.log('item already presented');
        //           if (this.selectedAdd.length == 0) {
        //             this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
        //           }
        //         } else {
        //           this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
        //           this.order_id = order;
        //         }
        //       } else {
        //         console.log('another resident');
        //       }
        //     }
        //   }else{
        //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
        //     if (targetIdx >= 0 && item.res.user.user_id==this.selectedItem[targetIdx].Resident) {
        //       if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
        //         this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
        //         this.order_id = order;
        //         console.log('first entry');
        //         this.selectedAdd.splice(0, 1);
        //       } else {
        //         const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
        //         if (targetIdx >= 0) {
        //           const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
        //           if (ItemIdx >= 0) {
        //             this.selectedAdd.splice(ItemIdx, 1);
        //             console.log('item already presented');
        //             if (this.selectedAdd.length == 0) {
        //               this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
        //             }
        //           } else {
        //             this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
        //             this.order_id = order;
        //           }
        //         } else {
        //           console.log('another resident');
        //         }
        //       }
        //     }
        //   }
          
        // }
        //   console.log('selectedItems:', this.selectedItem,this.selectedAdd)
        // }
  
       async selectItems(item, orderid, order_id,k,order,i,index) {
        if(this.edit){
          if(item.res.tag_status!=='refused'){
          let s_url=this.config.domain_url+'consume_individual_orders';
          let u_url=this.config.domain_url+'unconsume_individual_orders';
          const cid = await this.storage.get('COMPANY_ID');
          const bid = await this.storage.get('BRANCH');
          const uid = await this.storage.get('USER_ID');
          // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
          let headers=await this.config.getHeader();
          
          if(k==1){
            let body={
              company_id:cid,
              served_by:uid,
              order_details_id:orderid,
              itemqunatity:order.served_quantity
    
            }
            if(order.serving_status==0||order.serving_status==1){
              this.http.post(s_url,body,{headers}).subscribe(res=>{
                
                this.selectedItem.push({ Resident: item.res.user.user_id, Item: orderid ,qty:order.served_quantity});
                this.details[i].res.order.order_items[index].serving_status=3;
                this.details[i].res.tag_status='consumed';
                if(this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order){
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index].serving_status=3;
                }
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='consumed';

                
             
                if(this.selectedItem[0].Resident == 0){
                  
                  this.selectedItem.splice(0,1);

                }
                if(this.selectedItem.length==1){
                  if(this.count.to_consume>0){
                    this.count.to_consume--
                  }
                  this.count.consumed++;
                }
               
              
              },err=>{
                console.log(err)
              })
            }else{
              this.http.post(u_url,body,{headers}).subscribe(res=>{
               
                this.details[i].res.order.order_items[index].serving_status=0;
                this.details[i].res.tag_status='';
                if(this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order){
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index].serving_status=0;
                }
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='';
                const idx=this.selectedItem.map(x=>x.Item).indexOf(orderid);
          if(idx>=0){
            this.selectedItem.splice(idx,1);
          }

          if(!this.details[i].res.order.order_items.map(x=>x.serving_status).includes(3)){
            if(this.count.consumed>0){
              this.count.consumed--;
            }
            this.count.to_consume++;
          }

          if(!this.selectedItem.length){
            this.details[i].res.tag_status='default';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
          }
              },err=>{
                console.log(err)
              })
            }
  
          }else{
            let body={
              company_id:cid,
              served_by:uid,
              order_add_details_id:orderid,
              itemqunatity:order.served_quantity
    
            }
            if(order.serving_status==0){
              this.http.post(s_url,body,{headers}).subscribe(res=>{
                console.log("servedaddsingle:",index,this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index]);
                this.details[i].res.additional_order[index].serving_status=3;
                this.details[i].res.tag_status='consumed';
              
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[index].serving_status=3;
                
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='';
                this.selectedAdd.push({ Resident: item.res.user.user_id, Item: orderid ,qty:order.served_quantity});
          if(this.selectedAdd[0].Resident == 0){
            this.selectedAdd.splice(0,1);
          }
              },err=>{
                console.log(err)
              })
            }else{
              this.http.post(u_url,body,{headers}).subscribe(res=>{
                console.log("unservedaddsingle:",index,this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[index]);
                this.details[i].res.additional_order[index].serving_status=0;
                this.details[i].res.tag_status='';
               
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[index].serving_status=0;
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='';
                const idx=this.selectedAdd.map(x=>x.Item).indexOf(orderid);
          if(idx>=0){
            this.selectedAdd.splice(0,1);
          }
          if(!this.selectedAdd.length){
            this.details[i].res.tag_status='default';
            this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
          }
              },err=>{
                console.log(err)
              })
            }
          }
        }else{
          this.presentAlert('Please undo refused to mark consumed')
        }
      }
        }
      
        showSelectedItems(id) {
          let s;
          const idx = this.selectedItem.map(x => x.Item).indexOf(id);
          if (idx >= 0) {
            s = true;
          } else {
            s = false;
          }
          return s;
        }
      
        async addItems(item) {
          const modal = await this.modalCntrl.create({
            component: DiningAddItemsComponent,
            cssClass: 'full-width-modal',
            componentProps: {
              date: this.date,
              serve_time: this.ser_time,
              item: item,
              menu_id:this.menu_id
            }
          });
          modal.onDidDismiss().then(() => {
            this.loading=true;
              // this.getDetails(this.wing)
              this.status='1';
              this.getDetailsNew();
          });
          return await modal.present();
        }
  
        cancel(){
          this.hide=true;
          this.terms='';
          this.getDetailsNew();
          console.log('cancel:',this.hide);
        }
        search(){
          this.hide=false;
        }
        expand(item){
          if (item.expanded) {
            item.expanded = false;
          } else {
            this.wingArray.map(listItem => {
              if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                if(listItem.expanded){
                  // this.getDetails(listItem.id);
                  this.getDetailsNew();
                  this.loading=true;
                  this.wing=listItem.id;
                  this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                  this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
                }
              } else {
                listItem.expanded = false;
              }
              return listItem;
            });
          }
         }
        expandProfile(item){
          this.selected=[];
          if (item.expanded) {
            item.expanded = false;
          } else {
            // if(this.area_id==0){
            this.details.map(listItem => {
              if (item == listItem) {
                listItem.expanded = !listItem.expanded;
                if(listItem.expanded){
                  this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                  this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
                  
                }
              } else {
                listItem.expanded = false;
              }
              return listItem;
            });
          // }else{
          //   this.residents.map(listItem => {
          //     if (item == listItem) {
          //       listItem.expanded = !listItem.expanded;
          //       if(listItem.expanded){
                 
                  
          //       }
          //     } else {
          //       listItem.expanded = false;
          //     }
          //     return listItem;
          //   });
          // }
          }
         }
  
         async showComment(item,ev){
          const popover = await this.popCntlr.create({
            component: CbCommentComponent,
            id:'commentModal',
            event:ev,
            backdropDismiss:true,
            componentProps:{
              data:item
            },
           
            
            
          });
          return await popover.present();
        }
  
  
        async consumed(item,i){
          const cid=await this.storage.get('COMPANY_ID');
          
          const uid=await this.storage.get('USER_ID');
          let headers=await this.config.getHeader();
          if(this.has_pcs==1){
          // if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0 &&this.selectedAdd.length==1&&this.selectedAdd[0].Resident==0) {
          //   this.presentAlert('Please select atleast one item')
          
          // } else if (this.selectedItem[0].Resident == item.user.user_id || this.selectedAdd[0].Resident == item.user.user_id ) {
          const modal = await this.modalCntrl.create({
            component: ServingConsumedComponent,
            cssClass: 'dining-consumed-modal',
            componentProps: {
              res:item.res,
              ml_time:this.servetime,
              // area:this.area
            }
          });
          modal.onDidDismiss().then((dataReturned) => {
            
            if(dataReturned.data){
              // this.ionViewWillEnter();
              this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
              this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
              // this.getDetails(this.wing);
              this.status='1';
              this.getDetailsNew();
            }
          });
          return await modal.present();
        }else{
          let url=this.config.domain_url+'consume_orders';

          let items = [];
          let add=[];
          let quantity=[];
          let id=this.details[i].res.order.id;
          if(this.details[i].res.order.order_items&&this.details[i].res.order.order_items.length){
            this.details[i].res.order.order_items.forEach(ele => {
            items.push(ele.id)
            quantity[ele.id]=ele.quantity
          })
        }
        if(this.details[i].res.additional_order&&this.details[i].res.additional_order.length){
          this.details[i].res.additional_order.forEach(ele => {
            add.push(ele.id)
            quantity[ele.id]=ele.quantity
          })
        }

       
          let body={
            company_id:cid,
            served_by:uid,
            order_id:id,
            order_details_id:items,
            order_add_details_id:add,
            itemqunatity:quantity,
            serveqty:null,
            carenote:null
          }

          
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log('cres:',res);
           this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
           this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
           // this.getDetails(this.wing);
           this.status='1';
           this.getDetailsNew();
          },error=>{
            console.log(error)
            
          })
        }
        // }
        }
  
        async unserve(item,ev,i){
          let id;
          if(item.res.order){
            id=item.res.order.id
          }else if(item.res.additional_order&&item.res.additional_order.length){
            id=item.res.additional_order[0].order_id
          }
          let flag=1;
          if(item.res.tag_status=='consumed'){
            flag=2
          }
          const popover = await this.popCntlr.create({
            component: UnserveComponent,
            event: ev,
            backdropDismiss: true,
            cssClass: 'dining-more-options-popover',
            componentProps: {
              
              order_id:id,
              flag:flag
            },
          });
            popover.onDidDismiss().then((dataReturned) => {
              if(dataReturned.data){
                this.details[i].res.tag_status='default';
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
              this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
              this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
              this.selected=[]
              if(flag==1){
              if(this.count.served>0){
                this.count.served--;
              
              }
              this.count.toserved++;
            }else{
              this.details[i].res.order.order_items.map(x=>x.serving_status=0);
              this.details[i].res.additional_order.map(x=>x.serving_status=0);
              if(this.count.consumed>0){
                this.count.consumed--;
              
              }
              this.count.to_consume++;
            }
                 }
                
            });
      
          
          return await popover.present();
        }
  
        async serveAll(item,i){
          // let id;
          this.selected=[];
          if(item.res.order){
            this.order_id=item.res.order.id
          }else if(item.res.additional_order&&item.res.additional_order.length){
            this.order_id=item.res.additional_order[0].order_id
          }
          // const cid = await this.storage.get('COMPANY_ID');
          //   const bid = await this.storage.get('BRANCH');
          //   const uid = await this.storage.get('USER_ID');
          //   let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
          //   let url = this.config.domain_url + 'serve_orders';
      
            // let items = [];
            // let add_items=[];
            // let quantity=[];
            if(item.res.order){
              item.res.order.order_items.forEach((ele,index)=>{
                // items.push(ele.id);
                // quantity[ele.id]=ele.quantity
                // this.selectedItem.push({ Resident: item.res.user.user_id, Item: ele.id ,qty:ele.quantity});
                // this.selected.push(ele.id)
                // // this.order_id = id;
                // this.selectedItem.splice(0, 1);
                this.selectItems(item,ele.id,ele.order_id,1,ele,i,index)
                
              })
              
            }
            if(item.res.additional_order&&item.res.additional_order.length){
              item.res.additional_order.forEach((ele,index)=>{
                // add_items.push(ele.item.id);
                // quantity[ele.item.id]=ele.quantity
                // this.selectedAdd.push({ Resident: item.res.user.user_id, Item: ele.id ,qty:ele.quantity});
                //     // this.order_id = id;
                //     this.selected.push(ele.item.id);
                //     this.selectedAdd.splice(0, 1);
                this.selectItems(item,ele.id,ele.order_id,2,ele,i,index)
              })
              
            }
            // this.selectedItem.forEach(ele => {
            //   items.push(ele.Item)
            // })
      
            // let body = {
            //   company_id: cid,
            //   served_by: uid,
            //   order_id: id,
            //   order_details_id: items,
            //   order_add_details_id: add_items,
            //   itemqunatity:quantity,
            //   serveqty:1
      
            // }
  
            // console.log('body:',body)
            // this.http.post(url, body, { headers }).subscribe((res: any) => {
      
            //   console.log('res:', res);
            //   this.details[i].res.servestatus='served';
            //   // this.selectedItem= [{ Resident: 0, item: 0 }];
            //   if(this.count.toserved>0){
            //   this.count.toserved--;
            //   }
            //   this.count.served++;
            // },error=>{
            //   console.log(error)
            // })
        }
  
        async showOptions(options,ev){
      
          const popover = await this.popCntlr.create({
            component: ItemOptionsComponent,
            event:ev,
            backdropDismiss:true,
            componentProps:{
              data:options,
              flag:1
            },
           
            
            
          });
          return await popover.present();
        }
        async changeCount(item,order,i,j,k,ev){
         
          
          if(this.edit&&item.res.tag_status!=='refused'){
          const popover = await this.popCntlr.create({
            component: ServingChangeCountComponent,
            event:ev,
            id:'countPop',
            backdropDismiss:false,
            componentProps:{
              quantity:order.served_quantity
            },
           
            
            
            
          });
          popover.onDidDismiss().then((dataReturned) => {
            console.log(dataReturned.data)
            if(k==1){
              this.details[i].res.order.order_items[j].served_quantity=dataReturned.data;
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.order_items[j].served_quantity=dataReturned.data;
            }else{
              this.details[i].res.additional_order[j].served_quantity=dataReturned.data;
              this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.additional_order[j].served_quantity=dataReturned.data;
            }
           
              
          });
          return await popover.present();
        }
        }


        async presentAlert(mes) {
          const alert = await this.toastCntlr.create({
            message: mes,
            cssClass:'toastStyle',
            duration: 3000,
            position:'top'      
          });
          alert.present(); //update
        }


        async getTableDetails(){
         
          const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
         
          let headers=await this.config.getHeader();
          let url=this.config.domain_url+'serving_item_listing_table';
          
          let body={
            company_id:cid,
            servingtimeid:this.ser_time,
            date:moment(this.date).format('YYYY-MM-DD'),
            serveareaid:this.area_id,
            wing_id:[0]
          }
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            
            
            res.data.tables.forEach(el=>{
              this.tables.push({table:el,expanded:false})
            })
            console.log('tabres:',this.tables);
            this.showTableResidents(this.tables[0].table.id);
            // for(let i in this.table.seats){
            //   if(this.table.seats[i].residents&&this.table.seats[i].residents.length>0){
            //     for(let j in this.table.seats[i].residents)
            //     this.residents.push({res:this.table.seats[i].residents[j],expanded:false})
            //   }
            // }
          },error=>{
            console.log(error)
            
          })
        }
        showTableResidents(id){
          this.residents=[];
         const idx= this.tables.map(x=>x.table.id).indexOf(id);
         this.tables[idx].table.seats.forEach(y => {
           y.residents.forEach(ele => {
             console.log('ele:',ele)
            this.residents.push({res:ele,expanded:false});
            // this.loading=false
           });
           console.log('resi:',this.residents,y)
         });
         
        }

        segmentChanged(ev,status){
          this.status=status
          console.log(status)
          if(status==1){
            this.loading=true;
            // this.getDetails(this.wing)
            this.getDetailsNew();
          }else{
            // let details=this.details;
            this.details=[];
            this.detailsCopy.forEach(ele=>{
              if(status==2&&ele.res.tag_status=='default'){
                this.details.push(ele)
              }else if(status==3&&(ele.res.tag_status=='served'||ele.res.tag_status=='consumed')){
                this.details.push(ele)
              }else if(status==4&&ele.res.tag_status=='refused'){
                this.details.push(ele)
              }else if(status==5&&ele.res.is_leave==1){
                this.details.push(ele);
              }else if(status==6&&ele.res.tag_status=='consumed'){
                this.details.push(ele);
              }else if(status==7&&ele.res.tag_status=='default'){
                this.details.push(ele)
              }else if(status==8&&ele.res.tag_status=='pending'){
                this.details.push(ele)
              }
            })
          }

        }

        async undoRefuse(item,ev,i){
          let id;
          if(item.res.order){
            id=item.res.order.id
          }else if(item.res.additional_order&&item.res.additional_order.length){
            id=item.res.additional_order[0].order_id
          }
          const popover = await this.popCntlr.create({
            component: UndoRefuseComponent,
            event: ev,
            backdropDismiss: true,
            cssClass: 'dining-more-options-popover',
            componentProps: {
              
              order_id:id
              
            },
          });
            popover.onDidDismiss().then((dataReturned) => {
              if(dataReturned.data){
                console.log('undorefu:',this.details[i].res,this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res)
                this.details[i].res.tag_status='default';
                // this.detailsCopy[i].res.tag_status='default';
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.tag_status='default';
                
                this.details[i].res.order.refuse_status=0;
                // this.detailsCopy[i].res.order.refuse_status=0;
                this.detailsCopy[this.detailsCopy.findIndex(x=>x.res.user_id==this.details[i].res.user_id)].res.order.refuse_status=0;
              this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
              this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
              this.selected=[]
              if(this.count.refused>0){
                this.count.refused--;
              
              }
              this.count.to_consume++;
                 }
                
            });
      
          
          return await popover.present();
        }

}
