import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningConsumedPage } from './dining-consumed.page';

const routes: Routes = [
  {
    path: '',
    component: DiningConsumedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningConsumedPageRoutingModule {}
