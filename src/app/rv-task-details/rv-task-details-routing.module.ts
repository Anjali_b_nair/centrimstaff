import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvTaskDetailsPage } from './rv-task-details.page';

const routes: Routes = [
  {
    path: '',
    component: RvTaskDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvTaskDetailsPageRoutingModule {}
