import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvTaskDetailsPageRoutingModule } from './rv-task-details-routing.module';

import { RvTaskDetailsPage } from './rv-task-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvTaskDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [RvTaskDetailsPage]
})
export class RvTaskDetailsPageModule {}
