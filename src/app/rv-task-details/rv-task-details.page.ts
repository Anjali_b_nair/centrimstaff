import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { RvAssignedStaffComponent } from '../components/rv-assigned-staff/rv-assigned-staff.component';
import moment from 'moment';
@Component({
  selector: 'app-rv-task-details',
  templateUrl: './rv-task-details.page.html',
  styleUrls: ['./rv-task-details.page.scss'],
})
export class RvTaskDetailsPage implements OnInit {
task:any;
  constructor(private router:Router,private route:ActivatedRoute,private modalCntl:ModalController) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.task=JSON.parse(this.route.snapshot.paramMap.get('task'));
  }
  
  overdue(date){
   
    if(moment(date,'YYYY-MM-DD').isBefore(moment(),'day')){
      return true;
    }else{
      return false;
    }
  }
  ShowContent(a,b,i){
    var x = document.getElementById(a+i);
    var y = document.getElementById(b+i);
          x.style.display = 'none';
          y.style.display = 'block';
  }

  async assigned(item){
    const popover = await this.modalCntl.create({
      component:RvAssignedStaffComponent,
      cssClass:'cbmorepop',
    backdropDismiss:true,
      componentProps:{
        data:item,
       

      },
      
      
      
    });
    
   
   
      
  
    return await popover.present();
  }
}
