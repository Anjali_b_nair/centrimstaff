import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningTablePage } from './dining-table.page';

describe('DiningTablePage', () => {
  let component: DiningTablePage;
  let fixture: ComponentFixture<DiningTablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningTablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
