import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningTablePageRoutingModule } from './dining-table-routing.module';

import { DiningTablePage } from './dining-table.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningTablePageRoutingModule
  ],
  declarations: [DiningTablePage]
})
export class DiningTablePageModule {}
