import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseConsumerPage } from './din-choose-consumer.page';

describe('DinChooseConsumerPage', () => {
  let component: DinChooseConsumerPage;
  let fixture: ComponentFixture<DinChooseConsumerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseConsumerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseConsumerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
