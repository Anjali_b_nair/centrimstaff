import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseConsumerPageRoutingModule } from './din-choose-consumer-routing.module';

import { DinChooseConsumerPage } from './din-choose-consumer.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseConsumerPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinChooseConsumerPage]
})
export class DinChooseConsumerPageModule {}
