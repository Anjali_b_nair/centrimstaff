import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-din-choose-consumer',
  templateUrl: './din-choose-consumer.page.html',
  styleUrls: ['./din-choose-consumer.page.scss'],
})
export class DinChooseConsumerPage implements OnInit {
subscription:Subscription;
consumers:any[]=[];
terms:any;
  hide:boolean=true;
  rv:any;
  offset:any = 1;
  constructor(private router:Router,private storage:Storage,private config:HttpConfigService,
    private http:HttpClient,private loadingCtrl:LoadingController,private platform:Platform) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.showLoading();
             this.terms='';
    const bid=await this.storage.get("BRANCH");
    this.rv=await this.storage.get('RVSETTINGS');

    this.getAllresidents(false,'');
    // let url=this.config.domain_url+'residents';
  
    // let headers=await this.config.getHeader();
   
    // this.http.get(url,{headers}).subscribe((data:any)=>{
     
    //   console.log("data:",data);
    //   this.consumers=data.data
    //  this.dismissLoader();
        
    // },error=>{
    //   this.dismissLoader();
    //   console.log(error);
    // });



this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
this.back();
}); 

}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

  back(){
    this.router.navigate(['/dining-dashboard'],{replaceUrl:true})
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'dining-loading',
      
      // message: 'Please wait...',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  gotoPreference(item){
    this.router.navigate(['/din-dietary-preference',{consumer:item.user_id,name:item.name}]);
  }

   cancel(){
    this.hide=true;
    this.terms='';
    this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
    console.log('cancel:',this.hide);
  }
  search(){
    this.hide=false;
    this.offset=1;
  this.consumers=[];
  this.getAllresidents(false,'')
  }


  async getAllresidents(isFirstLoad,event){
   
  
    // let url=this.config.domain_url+'get_all_residents_in_branch_with_filter';
    let url=this.config.domain_url+'get_all_residents_list_via_filters';
    let headers=await this.config.getHeader();
  
    let body;
    body={
      type:1,  // 1 - active 2 -inactive 3 - no contract
      sort:'fname',  // sortby
      order:'ASC',   // ASC or DESC
    
      page:this.offset, 
      
    }
  
    if(this.terms){
    body.keyword=this.terms
    }
                 console.log('body:',body,headers);
                 
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
                   
      console.log("data:",data);
  
      for (let i = 0; i < data.data.residents.length; i++) {
        this.consumers.push(data.data.residents[i]);
      }
  
      if (isFirstLoad)
      event.target.complete();
      this.dismissLoader();
      this.offset=++this.offset;    
          
      },error=>{
        this.dismissLoader();
        console.log(error);
      });
  
  }
  doInfinite(event) {
    this.getAllresidents(true, event)
    }
}
