import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseConsumerPage } from './din-choose-consumer.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseConsumerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseConsumerPageRoutingModule {}
