import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingP3PageRoutingModule } from './onboarding-p3-routing.module';

import { OnboardingP3Page } from './onboarding-p3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnboardingP3PageRoutingModule
  ],
  declarations: [OnboardingP3Page]
})
export class OnboardingP3PageModule {}
