import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RvCreateTaskPageRoutingModule } from './rv-create-task-routing.module';

import { RvCreateTaskPage } from './rv-create-task.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RvCreateTaskPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [RvCreateTaskPage,],
 
})
export class RvCreateTaskPageModule {}
