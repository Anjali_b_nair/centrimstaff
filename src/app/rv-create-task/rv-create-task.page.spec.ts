import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RvCreateTaskPage } from './rv-create-task.page';

describe('RvCreateTaskPage', () => {
  let component: RvCreateTaskPage;
  let fixture: ComponentFixture<RvCreateTaskPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RvCreateTaskPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RvCreateTaskPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
