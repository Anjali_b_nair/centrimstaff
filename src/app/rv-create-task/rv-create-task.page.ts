import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { RvSelectStaffComponent } from '../components/rv-select-staff/rv-select-staff.component';
import { ResidentComponent } from '../components/resident/resident.component';
@Component({
  selector: 'app-rv-create-task',
  templateUrl: './rv-create-task.page.html',
  styleUrls: ['./rv-create-task.page.scss'],
})
export class RvCreateTaskPage implements OnInit {
cid:any;
id:any;
flag:any;
type:any='0';
date:any;
priority:any='1';
consumer:any;
assignedTo:any=[];
selectedStaff:any=[];
selectedStaffCopy:any=[];
staff:any;
title:any;
description:any;
subscription:Subscription;
companian:any=[];
resident:any=[];
resident_name:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private popCntl:PopoverController,
    private router:Router,private route:ActivatedRoute,private platform:Platform, 
    private toastCntlr: ToastController,private storage:Storage,private modalCntrl:ModalController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.id=this.route.snapshot.paramMap.get('id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.consumer=this.route.snapshot.paramMap.get('consumer');
    this.companian=JSON.parse(this.route.snapshot.paramMap.get('companian'));
    if(this.companian&&this.companian.length){
      this.resident.push(this.id);
    }
    console.log('resi:',this.resident);
    
    this.date=new Date(moment().format()).toISOString();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      
      this.back();
      
  }); 
  }

  back(){
    if(this.flag==1){
      this.router.navigate(['/rv-resident-profile-landing',{id:this.cid}],{replaceUrl:true})
    }else if(this.flag==3){
      this.router.navigate(['/rv-tasks'],{replaceUrl:true});
    }else{
    this.router.navigate(['/rv-res-task-list',{cid:this.cid,id:this.id,consumer:this.consumer,companian:JSON.stringify(this.companian)}],{replaceUrl:true})
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top'
    });
    alert.present(); //update
  }

  async save(){

    const bid = await this.storage.get("BRANCH");
    const uid = await this.storage.get("USER_ID");
    const cid = await this.storage.get("COMPANY_ID");
    const tz = await this.storage.get('TIMEZONE');
    let res=[];
   
    if(!this.title||this.title==''){
      this.presentAlert('Please enter the title.')
    }else if(!this.id&&(!this.resident||!this.resident.length)){
      this.presentAlert('Please choose resident.')
    }else{
      if(this.flag==3){
        res=this.resident.map(x=>parseInt(x))
      }else{
      if(this.companian&&this.companian.length){
       
        res=this.resident.map(x=>parseInt(x))
      }else{
        
        res.push(parseInt(this.id))
      }
    }
      let url=this.config.domain_url+'create_resident_task';
    let headers=await this.config.getHeader();
    console.log("dat:",url,headers);
    let due_date,due,date;
    let currentDate = new Date();
          if (this.priority == 2) {
            due = currentDate
          } else if (this.priority == 1) {
            due = new Date(Date.now() + 2 * 24 * 60 * 60 * 1000);
          } else if (this.priority == 0) {
            due = new Date(Date.now() + 3 * 24 * 60 * 60 * 1000);
          }
          date = due.getFullYear() + '-' + this.fixDigit((due.getMonth() + 1)) + '-' + this.fixDigit(due.getDate());
          // due_date=moment(date).tz(tz).format('YYYY-MM-DD');
          due_date=moment(this.date).format('YYYY-MM-DD');
          // let date_time=moment(this.date).tz(tz);
          
          
         
          var s = moment.tz(this.date,      // input date time string
                  'YYYY-MM-DD hh:mm:ss',      // format of input
                  tz);

          // date_time:moment.utc(moment(this.date).tz(tz)).format('YYYY-MM-DD HH:mm:ss'),
          let body;
          body={
      date_time:moment.utc(s).format('YYYY-MM-DD HH:mm:ss'),
      due_date:due_date,
      title:this.title,
      priority:parseInt(this.priority),
      description:this.description,
      type:this.type,
      created_by:uid,
      created_time:moment().format('YYYY-MM-DD HH:mm:ss'),
      resident_user_id:res,
      assignee:this.assignedTo,
      
    }
if(this.flag!==3){
body.user_id=parseInt(this.id)
}
    console.log('body:',body,this.date,s);
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
     
     console.log(res);
     
      this.presentAlert('Task created successfully.')
      this.back();
    },error=>{
      console.log(error);
      this.presentAlert('Something went wrong. Please try again later.')
    })
    }
  }


  fixDigit(val) {
    return val.toString().length === 1 ? "0" + val : val;
  }

  async openAssignedTo(){
  
    // this.selectedStaffCopy=this.selectedStaff.filter(x=>x);
    const popover = await this.popCntl.create({
     
      component:RvSelectStaffComponent,
      cssClass:'rv-select-staf-pop',
      backdropDismiss:true,
      componentProps:{
        data:this.selectedStaff,
       flag:1,
       status:1
  
      },
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log('data:',dataReturned,this.assignedTo);
      if(dataReturned.data){
        
        
            this.assignedTo=dataReturned.data.id;
            this.selectedStaff=this.assignedTo;
            console.log('data is here:',this.assignedTo);
            if(dataReturned.data.name.length>1){
            this.staff=dataReturned.data.name[0]+', '+dataReturned.data.name[1];
            }else{
              this.staff=dataReturned.data.name[0]
            }
          // }else{
          //   this.selectedStaff=[];
          //   this.selectedStaffCopy=this.selectedStaff
          }
   
          console.log('data is here1:',this.assignedTo);
      
    });
    return await popover.present();
  
  }

  async openResident() {
   
   
    const bid = await this.storage.get("BRANCH");
    const popover = await this.modalCntrl.create({
      component: ResidentComponent,

      componentProps: {
        data: this.resident,
        rv:1,
        bid:bid,
        name:this.resident_name,
        flag:2
      },
      cssClass:'fullWidthModal'
    });

    popover.onDidDismiss().then((dataReturned) => {
      console.log('res:',dataReturned);
      if (dataReturned.data != undefined) {
        
        this.resident=dataReturned.data.data;
        this.id=dataReturned.data.data;
        if(dataReturned.data.name.length>1){
        this.resident_name = dataReturned.data.name[0]+','+dataReturned.data.name[1];
      }else{
        this.resident_name=dataReturned.data.name[0]
      }
      }else{
        if(dataReturned.role!='backdrop'){
        this.resident = [];
        this.resident_name = undefined;
        this.id=undefined
        }
      }
      console.log("data:", dataReturned);

    });
    return await popover.present();
  
  }
}
