import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RvCreateTaskPage } from './rv-create-task.page';

const routes: Routes = [
  {
    path: '',
    component: RvCreateTaskPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RvCreateTaskPageRoutingModule {}
