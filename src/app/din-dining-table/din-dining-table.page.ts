import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { ServingRefusedComponent } from '../components/dining/serving-refused/serving-refused.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
import { DiningAddItemsComponent } from '../components/dining/dining-add-items/dining-add-items.component';
import { CbCommentComponent } from '../components/cb-comment/cb-comment.component';
import { ServingConsumedComponent } from '../components/dining/serving-consumed/serving-consumed.component';
import { UnserveComponent } from '../components/dining/unserve/unserve.component';
import { ItemOptionsComponent } from '../components/dining/item-options/item-options.component';
import { ServingChangeCountComponent } from '../components/dining/serving-change-count/serving-change-count.component';
import { MarkAsServedComponent } from '../components/dining/mark-as-served/mark-as-served.component';
import { AssignSeatComponent } from '../components/dining/assign-seat/assign-seat.component';
import { RemoveSeatComponent } from '../components/dining/remove-seat/remove-seat.component';
import { ServeConfirmationComponent } from '../components/dining/serve-confirmation/serve-confirmation.component';
import { UndoRefuseComponent } from '../components/dining/undo-refuse/undo-refuse.component';

@Component({
  selector: 'app-din-dining-table',
  templateUrl: './din-dining-table.page.html',
  styleUrls: ['./din-dining-table.page.scss'],
})
export class DinDiningTablePage implements OnInit {
  segment: any;
  servetime: any;
  date: any;
  table: any;
  subscription: Subscription;
  area_id: any;
  area: any;
  ser_time_id: any;

  selectedItem: any[] = [{ Resident: 0, item: 0 , qty:1}];
  selectedAdd: any[]  =[{ Resident: 0, item: 0 ,qty:1}]
  order_id: any;
  residents:any[]=[];
  menu_id:any;
  terms:any;
  hide:boolean=true;
  status:any='1';
  selected:any=[];
  serving_order:any;
  details:any=[];
  edit:boolean=false;
  order:boolean=false;
  rv:any;
  count:any;
  wingArray:any[]=[];
  constructor(private router: Router, private platform: Platform, private modalCntrl: ModalController,
    private route: ActivatedRoute, private storage: Storage, private config: HttpConfigService, private http: HttpClient,
    private popCntlr:PopoverController,private loadingCtrl:LoadingController,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
 async ionViewWillEnter() {
    this.residents=[];
    this.selected=[];
    this.details=[];
    this.rv=await this.storage.get('RVSETTINGS');
    this.wingArray=[];
    this.getWing();
    // this.area_id = this.route.snapshot.paramMap.get('area_id');
    // this.area = this.route.snapshot.paramMap.get('area');
    // this.ser_time_id = this.route.snapshot.paramMap.get('ser_time_id');
    // this.servetime = this.route.snapshot.paramMap.get('ser_time');
    // this.date = this.route.snapshot.paramMap.get('date');
    // this.menu_id = this.route.snapshot.paramMap.get('menu_id');
    // this.table = JSON.parse(this.route.snapshot.paramMap.get('table'));
    // this.serving_order = this.route.snapshot.paramMap.get('serving_order');
    this.hide=true;
    this.area=this.route.snapshot.paramMap.get('area');
 this.ser_time_id=this.route.snapshot.paramMap.get('ser_time_id');
 this.area_id=this.route.snapshot.paramMap.get('area_id');
 this.servetime=this.route.snapshot.paramMap.get('ser_time');
 this.date=this.route.snapshot.paramMap.get('date');
 


 (await this.config.getUserPermission()).subscribe((res: any) => {
  console.log('permissions:', res);
  let routes = res.user_routes;
  
  if (res.main_permissions.dining==1&&routes.includes('serving.edit')) {

    this.edit= true
  } else {

    this.edit = false;
    
  }
  if (res.main_permissions.dining==1&&routes.includes('place_order')) {

    this.order= true
  } else {

    this.order = false;
    
  }
})
    // for(let i in this.table.seats){
    //   if(this.table.seats[i].residents&&this.table.seats[i].residents.length>0){
    //     for(let j in this.table.seats[i].residents)
    //     this.residents.push({res:this.table.seats[i].residents[j],expanded:false})
    //   }
    // }
    // console.log('tab:', this.table,this.residents)
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.back();
    });

  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  back() {
    this.popCntlr.dismiss(null,null,'countPop');
    this.router.navigate(['/din-choose-dt-mlt-da'],{replaceUrl:true})
    // this.router.navigate(['/din-dining-area', { area_id: this.area_id, area: this.area, ser_time_id: this.ser_time_id, ser_time: this.servetime, date: this.date }], { replaceUrl: true })
  }

  async refuse(item,i,j) {
    // if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {

    // } else if (this.selectedItem[0].Resident == item.res.user.user_id) {
      const modal = await this.modalCntrl.create({
        component: ServingRefusedComponent,
        cssClass: 'full-width-modal'

      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data) {
          this.refuseItem(dataReturned.data, dataReturned.role,i,item,j);
        }
      });
      return await modal.present();
    // }
  }
  segmentChanged(ev) {
    // if(this.segment=='refused'){
    //   this.refuse();
    // }
  }
  async dietaryPreference(item) {
    const modal = await this.modalCntrl.create({
      component: DietaryPreferencesComponent,
      cssClass: 'dining-dpreference-modal',
      componentProps: {
        details: item
      }
    });
    modal.onDidDismiss().then((dataReturned) => {

    });
    return await modal.present();
  }

  setAllergies(item) {
    let a = [];
    a = item.split(/\, +/);
    return a;
  }

  // async serve(item,i) {
  //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {

  //   } else if (this.selectedItem[0].Resident == item.res.user.user_id) {
  //     const cid = await this.storage.get('COMPANY_ID');
  //     const bid = await this.storage.get('BRANCH');
  //     const uid = await this.storage.get('USER_ID');
  //     let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
  //     let url = this.config.domain_url + 'serve_orders';

  //     let items = [];
  //     this.selectedItem.forEach(ele => {
  //       items.push(ele.Item)
  //     })

  //     let body = {
  //       company_id: cid,
  //       served_by: uid,
  //       order_id: this.order_id,
  //       order_details_id: items,

  //     }
  //     this.http.post(url, body, { headers }).subscribe((res: any) => {

  //       console.log('res:', res);
  //       this.residents[i].res.servestatus='served';
  //       this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
  //               this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
  //       if(this.table.tabtoservedcount>0){
  //       this.table.tabtoservedcount--;
  //       }
  //       this.count.toserved++;
  //     },error=>{
  //       console.log(error)
  //     })
  //   }
  // }


  async serve(item,i,j) {
    if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0 &&this.selectedAdd.length==1&&this.selectedAdd[0].Resident==0) {
      this.presentAlert('Please select atleast one item')
    
    } else if (this.selectedItem[0].Resident == item.user.user_id || this.selectedAdd[0].Resident == item.user.user_id ) {
      let ordereditems=[];
      let item_length=0;
      let add_length=0;
      if(item.order&&item.order.length){
        
        item.order[0].order_items.forEach(ele=>{
         
          
          ordereditems.push(ele.id)
         
          
        })
        
      }
      if(item.additional_order&&item.additional_order.length){
        
        item.additional_order.forEach(ele=>{
         
              ordereditems.push(ele.item.id);
             
        })
        
      }
      let len1=0;let len2=0;
      if(this.selectedItem[0].Resident!==0){
        len1=this.selectedItem.length
      }
      if(this.selectedAdd[0].Resident!==0){
        len2=this.selectedAdd.length
      }
      if(len1+len2==ordereditems.length){
          this.serveItem(item,i,j,1)
      }else{
        this.serveConfirmation(item,i,j)
      }
      
    }
  }
    async serveConfirmation(item,i,j){
      const modal = await this.modalCntrl.create({
        component: ServeConfirmationComponent,
        cssClass: 'full-width-modal'
        
      });
      modal.onDidDismiss().then((dataReturned) => {
        if (dataReturned.data) {
          this.serveItem(item,i,j,2);
        }
      });
      return await modal.present();
    }

    async serveItem(item,i,j,n){
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'serve_orders';

    let items = [];
    let add=[];
    let quantity=[];
    console.log('serveselected:',this.selectedItem,this.selectedAdd);
    
    if(this.selectedItem.length>0&&this.selectedItem[0].Resident!=0){
    this.selectedItem.forEach(ele => {
      items.push(ele.Item)
      quantity[ele.Item]=ele.qty
    })
  }
  if(this.selectedAdd.length>0&&this.selectedAdd[0].Resident!=0){
    this.selectedAdd.forEach(ele => {
      add.push(ele.Item)
      quantity[ele.Item]=ele.qty
    })
  }

    let body = {
      company_id: cid,
      served_by: uid,
      order_id: this.order_id,
      order_details_id: items,
      order_add_details_id: add,
      itemqunatity:quantity,
      serveqty:1
    }
    console.log('body:',body)
    this.http.post(url, body, { headers }).subscribe((res: any) => {

      console.log('res:', res);
      if(n==1){
      this.residents[j].res.seatres[i].servestatus='served';
      if(this.residents[j].res.seatres[i].order[0].order_items&&this.residents[j].res.seatres[i].order[0].order_items.length){
        this.residents[j].res.seatres[i].order[0].order_items.forEach((ele,index)=>{
          this.residents[j].res.seatres[i].order[0].order_items[index].serving_status=1;
        })
      }
        if(this.residents[j].res.seatres[i].additional_order&&this.residents[j].res.seatres[i].additional_order.length){
          this.residents[j].res.seatres[i].additional_order.forEach((ele,index)=>{
            this.residents[j].res.seatres[i].additional_order[index].serving_status=1;
          })
        }
      }
        this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
        if(this.count.toserved>0){
        this.count.toserved--;
        }
        this.count.served++;
      
      
    },error=>{
      console.log(error)
    })
  }
  



async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}


  async leave(item,i,j) {
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'mark_dietry_leave';


    let body = {
      company_id: cid,
      created_by: uid,
      consumer_id: item.user.user_id,
      start: moment(this.date).format('YYYY-MM-DD')

    }
    this.http.post(url, body, { headers }).subscribe((res: any) => {

      console.log('res:', res);
      this.residents[j].res.seatres[i].isleave=1
      this.residents[j].res.seatres[i].servestatus='leave';
      this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
      this.count.leavecount++;
    },error=>{
      console.log(error)
    })
  }
  async refuseItem(reason, otherReason,i,item,j) {
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'refuse_orders';

    let items = [];
    let add=[];
  //   if(this.selectedItem&&this.selectedItem.length>1){
  //   this.selectedItem.forEach(ele => {
  //     items.push(ele.Item)
  //   })
  // }else{
    if(item.order&&item.order.length){
    item.order[0].order_items.forEach(ele=>{
      items.push(ele.id)
    });
    this.order_id=item.order[0].id
  }
  if(item.additional_order&&item.additional_order.length){
    item.additional_order.forEach(ele=>{
      add.push(ele.id)
    });
    if(!item.order.length){
      this.order_id=item.additional_order.order_id
    }
  }
  
  // }

    let body = {
      company_id: cid,
      served_by: uid,
      order_id: this.order_id,
      order_details_id: items,
      order_add_details_id:add,
      reason: reason,
      reason_text: otherReason
    }
    console.log('refItem:',body)
    this.http.post(url, body, { headers }).subscribe((res: any) => {

      console.log('res:', res);
      this.residents[j].res.seatres[i].servestatus='refused';
      this.residents[j].res.seatres[i].order[0].refuse_reason=reason;
      this.residents[j].res.seatres[i].order[0].refuse_comment=otherReason;
      this.residents[j].res.seatres[i].order[0].refuse_status=1;
      this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
                this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
                this.count.refused++;
    },error=>{
      console.log(error)
    })
  }
  // selectItems(item, order_id, order) {
  //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
  //     this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id });
  //     this.order_id = order;
  //     console.log('first entry');
  //     this.selectedItem.splice(0, 1);
  //   } else {
  //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
  //     if (targetIdx >= 0) {
  //       const ItemIdx = this.selectedItem.map(item => item.Item).indexOf(order_id);
  //       if (ItemIdx >= 0) {
  //         this.selectedItem.splice(ItemIdx, 1);
  //         console.log('item already presented');
  //         if (this.selectedItem.length == 0) {
  //           this.selectedItem.push({ Resident: 0, item: 0 })
  //         }
  //       } else {
  //         this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id });
  //         this.order_id = order;
  //       }
  //     } else {
  //       console.log('another resident');
  //     }
  //   }
  //   console.log('selectedItems:', this.selectedItem)
  // }


  // selectItems(item, order_id, order,i,itemQty) {
  //   if(i==1){
  //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
  //     this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
  //     this.order_id = order;
  //     console.log('first entry');
  //     this.selectedItem.splice(0, 1);
  //   } else {
  //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
  //     if (targetIdx >= 0) {
  //       const ItemIdx = this.selectedItem.map(item => item.Item).indexOf(order_id);
  //       if (ItemIdx >= 0) {
  //         this.selectedItem.splice(ItemIdx, 1);
  //         console.log('item already presented');
  //         if (this.selectedItem.length == 0) {
  //           this.selectedItem.push({ Resident: 0, item: 0 ,qty: 1 })
  //         }
  //       } else {
  //         this.selectedItem.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
  //         this.order_id = order;
  //       }
  //     } else {
  //       console.log('another resident');
  //     }
  //   }
  // }else{

  //   if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {
  //     if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
  //       this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
  //       this.order_id = order;
  //       console.log('first entry');
  //       this.selectedAdd.splice(0, 1);
  //     } else {
  //       const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
  //       if (targetIdx >= 0) {
  //         const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
  //         if (ItemIdx >= 0) {
  //           this.selectedAdd.splice(ItemIdx, 1);
  //           console.log('item already presented');
  //           if (this.selectedAdd.length == 0) {
  //             this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
  //           }
  //         } else {
  //           this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
  //           this.order_id = order;
  //         }
  //       } else {
  //         console.log('another resident');
  //       }
  //     }
  //   }else{
  //     const targetIdx = this.selectedItem.map(item => item.Resident).indexOf(item.res.user.user_id);
  //     if (targetIdx >= 0 && item.res.user.user_id==this.selectedItem[targetIdx].Resident) {
  //       if (this.selectedAdd.length == 1 && this.selectedAdd[0].Resident == 0) {
  //         this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id , qty :itemQty.quantity});
  //         this.order_id = order;
  //         console.log('first entry');
  //         this.selectedAdd.splice(0, 1);
  //       } else {
  //         const targetIdx = this.selectedAdd.map(item => item.Resident).indexOf(item.res.user.user_id);
  //         if (targetIdx >= 0) {
  //           const ItemIdx = this.selectedAdd.map(item => item.Item).indexOf(order_id);
  //           if (ItemIdx >= 0) {
  //             this.selectedAdd.splice(ItemIdx, 1);
  //             console.log('item already presented');
  //             if (this.selectedAdd.length == 0) {
  //               this.selectedAdd.push({ Resident: 0, item: 0 ,qty: 1 })
  //             }
  //           } else {
  //             this.selectedAdd.push({ Resident: item.res.user.user_id, Item: order_id ,qty:itemQty.quantity});
  //             this.order_id = order;
  //           }
  //         } else {
  //           console.log('another resident');
  //         }
  //       }
  //     }
  //   }
    
  // }
  //   console.log('selectedItems:', this.selectedItem)
  // }

  async selectItems(item, orderid, order_id,k,order,i,index,j) {
    if(this.edit){
      console.log('item:',item)
      if(item.order[0].refuse_status!=1&&item.isleave!==1){
    let s_url=this.config.domain_url+'serve_individual_orders';
    let u_url=this.config.domain_url+'unserve_individual_orders';
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    let headers=await this.config.getHeader();
    
    if(k==1){
      let body={
        company_id:cid,
        served_by:uid,
        order_details_id:orderid,
        itemqunatity:order.served_quantity

      }
      if(order.serving_status==0){
        this.http.post(s_url,body,{headers}).subscribe(res=>{
          console.log("servedsingle:",res,body);
          this.selectedItem.push({ Resident: item.user.user_id, Item: orderid ,qty:order.served_quantity});
          this.residents[j].res.seatres[i].order[0].order_items[index].serving_status=1;
          // if(this.selectedItem[0].Resident == 0){
          //   this.selectedItem.splice(0,1);
          // }
          this.residents[j].res.seatres[i].servestatus='served';
          const idx=this.selectedItem.map(x=>x.Resident).indexOf(item.user.user_id);
          console.log('selected:',this.residents[j].res.seatres[i]);
          if(this.selectedItem[0].Resident == 0){
            this.selectedItem.splice(0,1);
          }
          // if(idx<0){
            if(this.selectedItem.length==1){
          if(this.count.toserved>0){
            this.count.toserved--;
            }
            this.count.served++;
          }
        // }
          // this.residents[j].res.seatres[i].servestatus='served';
        },err=>{
          console.log(err)
        })
      }else{
        this.http.post(u_url,body,{headers}).subscribe(res=>{
          console.log("unservedsingle:",res);
          this.residents[j].res.seatres[i].order[0].order_items[index].serving_status=0;
          const idx=this.selectedItem.map(x=>x.Item).indexOf(orderid);
          if(idx>=0){
            this.selectedItem.splice(idx,1);
          }
        
           
        //  if(!this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1)){
        //     this.count.toserved++;
        //     if(this.count.served>0){
        //     this.count.served--;
        //     }
        //     this.residents[j].res.seatres[i].servestatus='default';
        //   }
        if(!this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1)&&!this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1)){
            this.count.toserved++;
            if(this.count.served>0){
            this.count.served--;
            }
            this.residents[j].res.seatres[i].servestatus='default';
            console.log('everything unserved,status default:',this.residents[j].res.seatres[i].servestatus,this.residents[j].res.seatres[i].order,this.residents[j].res.seatres[i].additional_order,this.selectedItem[0].Resident,this.selectedAdd[0].Resident);
            
          }
          if(!this.selectedItem.length){
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
           
          }
          if(!this.selectedAdd.length){
           
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
          }
        },err=>{
          console.log(err)
        })
      }

    }else{
      let body={
        company_id:cid,
        served_by:uid,
        order_add_details_id:orderid,
        itemqunatity:order.served_quantity

      }
      if(order.serving_status==0){
        this.http.post(s_url,body,{headers}).subscribe(res=>{
          
          this.residents[j].res.seatres[i].additional_order[index].serving_status=1;
          this.residents[j].res.seatres[i].servestatus='served';
          this.selectedAdd.push({ Resident: item.user.user_id, Item: orderid ,qty:order.served_quantity});
          if(this.selectedAdd[0].Resident == 0){
            this.selectedAdd.splice(0,1);
          }
          console.log("servedaddsingle:",this.selectedAdd,orderid);
          const idx=this.selectedAdd.map(x=>x.Resident).indexOf(item.user.user_id);
          if(this.selectedAdd.length==1){
            if(this.count.toserved>0){
              this.count.toserved--;
              }
              this.count.served++;
            }
          // if(idx<0){
          // if(this.count.toserved>0){
          //   this.count.toserved--;
          //   }
          //   this.count.served++;
          // }
          // this.residents[i].res.servestatus='served';
        },err=>{
          console.log(err)
        })
      }else{
        this.http.post(u_url,body,{headers}).subscribe(res=>{
         
          this.residents[j].res.seatres[i].additional_order[index].serving_status=0;
          
          const idx=this.selectedAdd.map(x=>x.Item).indexOf(orderid);
          if(idx>=0){
            this.selectedAdd.splice(idx,1);
          }

          console.log("unservedaddsingle:",idx,this.selectedAdd,orderid);
          // if(!this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1)){
          //   this.count.toserved++;
          //   if(this.count.served>0){
          //   this.count.served--;
          //   }
          //   this.residents[j].res.seatres[i].servestatus='default';
          // }

          if(!this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1)&&!this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1)){
              this.count.toserved++;
              if(this.count.served>0){
              this.count.served--;
              }
              this.residents[j].res.seatres[i].servestatus='default';
              console.log('everything unserved,status default:',this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1),this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1));
              
            }
          if(!this.selectedAdd.length){
           
            this.selectedAdd= [{ Resident: 0, item: 0 ,qty :1 }];
            // this.residents[j].res.seatres[i].servestatus='default';
         
            // this.count.toserved++;
            // if(this.count.served>0){
            // this.count.served--;
            // }
          }
          if(!this.selectedItem.length){
            this.selectedItem= [{ Resident: 0, item: 0 ,qty :1 }];
           
          }
        },err=>{
          console.log(err)
        })
      }
    }

    console.log('reside:',this.residents[j].res.seatres[i]);
    
    // if(!this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1)&&!this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1)){
    //   this.count.toserved++;
    //   if(this.count.served>0){
    //   this.count.served--;
    //   }
    //   this.residents[j].res.seatres[i].servestatus='default';
    //   console.log('everything unserved,status default:',this.residents[j].res.seatres[i].order[0].order_items.map(x=>x.serving_status).includes(1),this.residents[j].res.seatres[i].additional_order.map(x=>x.serving_status).includes(1));
      
    // }
  }
  }
  }

  showSelectedItems(id) {
    let s;
    const idx = this.selectedItem.map(x => x.Item).indexOf(id);
    if (idx >= 0) {
      s = true;
    } else {
      s = false;
    }
    return s;
  }

  async addItems(item) {
    const modal = await this.modalCntrl.create({
      component: DiningAddItemsComponent,
      cssClass: 'full-width-modal',
      componentProps: {
        date: this.date,
        serve_time: this.ser_time_id,
        item: item,
        menu_id:this.menu_id,
        
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    
      this.getDetails(0);
    });
    return await modal.present();
  }
  expandProfile(item){
    this.selected=[];
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.residents.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
          if(listItem.expanded){
            console.log('expandeditem:',listItem)

            this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
            this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];

            console.log('expandeditem:',listItem);
            if(item.order&&item.order.length){
            if(item.order[0].order_items&&item.order[0].order_items.length){
              item.order[0].order_items.forEach(element => {
                if(element.serving_status==1){
                  this.selectedItem = [{ Resident: item.user.user_id, item: element.id , qty:element.served_quantity }];
                }
              });
            }
          }
          if(item.additional_order&&item.additional_order){
            item.additional_order.forEach(element => {
              if(element.serving_status==1){
                this.selectedAdd = [{ Resident: item.user.user_id, item: element.id , qty:element.served_quantity }];
              }
            });
          }
          
            
          }
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
   }

  

  cancel(){
    this.hide=true;
    this.terms='';
    console.log('cancel:',this.hide);
  }
  search(){
    this.hide=false;
  }


  async showComment(item,ev){
    const popover = await this.popCntlr.create({
      component: CbCommentComponent,
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:item
      },
     
      
      
    });
    return await popover.present();
  }


  async consumed(item,i){
    if (this.selectedItem.length == 1 && this.selectedItem[0].Resident == 0) {

    } else if (this.selectedItem[0].Resident == item.user.user_id) {
    const modal = await this.modalCntrl.create({
      component: ServingConsumedComponent,
      cssClass: 'dining-consumed-modal',
      componentProps: {
        res:item.user.name
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
      this.serve(item,i,1);
      }
    });
    return await modal.present();
  }
  }

  async unserve(item,ev,i,j){
    let id;
    if(item.order&&item.order.length){
      id=item.order[0].id
    }else if(item.additional_order&&item.additional_order.length){
      id=item.additional_order[0].order_id
    }
    const popover = await this.popCntlr.create({
      component: UnserveComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        
        order_id:id,
        flag:1
      },
    });
      popover.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data){
         
          this.residents[j].res.seatres[i].servestatus='default';
          this.residents[j].res.seatres[i].order[0].order_items.forEach((ele,index)=>{
            this.residents[j].res.seatres[i].order[0].order_items[index].serving_status=0
          })
          this.residents[j].res.seatres[i].additional_order.forEach((ele,index)=>{
            this.residents[j].res.seatres[i].additional_order[index].serving_status=0
          })
          this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
          this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
          this.selected=[];
        if(this.count.toserved>0){
          this.count.toserved--;
        
        }
        this.count.toserved++;
           }
          
      });

    
    return await popover.present();
  }

  // async serveAll(item,i){
  //   let id;
  //   if(item.res.order&&item.res.order.length){
  //     id=item.res.order[0].id
  //   }else if(item.res.additional_order&&item.res.additional_order.length){
  //     id=item.res.additional_order[0].order_id
  //   }
  //   const cid = await this.storage.get('COMPANY_ID');
  //     const bid = await this.storage.get('BRANCH');
  //     const uid = await this.storage.get('USER_ID');
  //     let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
  //     let url = this.config.domain_url + 'serve_orders';

  //     let items = [];
  //     let add_items=[];
  //     let quantity=[];
  //     if(item.res.order&&item.res.order.length){
  //       item.res.order[0].order_items.forEach(ele=>{
  //         items.push(ele.id);
  //         quantity[ele.id]=ele.quantity
  //       })
        
  //     }
  //     if(item.res.additional_order&&item.res.additional_order.length){
  //       item.res.additional_order.forEach(ele=>{
  //         add_items.push(ele.item.id);
  //         quantity[ele.item.id]=ele.quantity
  //       })
        
  //     }
  //     // this.selectedItem.forEach(ele => {
  //     //   items.push(ele.Item)
  //     // })

  //     let body = {
  //       company_id: cid,
  //       served_by: uid,
  //       order_id: id,
  //       order_details_id: items,
  //       order_add_details_id: add_items,
  //       itemqunatity:quantity,
  //       serveqty:1

  //     }

  //     console.log('body:',body)
  //     this.http.post(url, body, { headers }).subscribe((res: any) => {

  //       console.log('res:', res);
  //       this.residents[i].res.servestatus='served';
  //       // this.selectedItem= [{ Resident: 0, item: 0 }];
  //       if(this.table.tabtoservedcount>0){
  //       this.table.tabtoservedcount--;
  //       }
  //       this.count.toserved++;
  //     },error=>{
  //       console.log(error)
  //     })
  // }

  async serveAll(item,i,j){
    // let id;
    this.selected=[];
    this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
            this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
    if(item.order&&item.order.length){
      this.order_id=item.order[0].id
    }else if(item.additional_order&&item.additional_order.length){
      this.order_id=item.additional_order[0].order_id
    }
    // const cid = await this.storage.get('COMPANY_ID');
    //   const bid = await this.storage.get('BRANCH');
    //   const uid = await this.storage.get('USER_ID');
    //   let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    //   let url = this.config.domain_url + 'serve_orders';

      // let items = [];
      // let add_items=[];
      // let quantity=[];
      if(item.order&&item.order.length){
        if(item.order[0].order_items&&item.order[0].order_items.length){
        item.order[0].order_items.forEach((ele,index)=>{
          // items.push(ele.id);
          // quantity[ele.id]=ele.quantity
          // this.selectedItem.push({ Resident: item.user.user_id, Item: ele.id ,qty:ele.quantity});
          // this.selected.push(ele.id)
          // this.order_id = id;
          
          this.selectItems(item,ele.id,ele.order_id,1,ele,i,index,j)
        })
        // this.selectedItem.splice(0, 1);
      }
      }
      if(item.additional_order&&item.additional_order.length){
        item.additional_order.forEach((ele,index)=>{
          // add_items.push(ele.item.id);
          // quantity[ele.item.id]=ele.quantity
          // this.selectedAdd.push({ Resident: item.user.user_id, Item: ele.id ,qty:ele.quantity});
          //     // this.order_id = id;
          //     this.selected.push(ele.item.id);
              // this.selectedAdd.splice(0, 1);
              this.selectItems(item,ele.id,ele.order_id,2,ele,i,index,j)
        })
        // this.selectedAdd.splice(0, 1);
      }
      // this.selectedItem.forEach(ele => {
      //   items.push(ele.Item)
      // })

      // let body = {
      //   company_id: cid,
      //   served_by: uid,
      //   order_id: id,
      //   order_details_id: items,
      //   order_add_details_id: add_items,
      //   itemqunatity:quantity,
      //   serveqty:1

      // }

      // console.log('body:',body)
      // this.http.post(url, body, { headers }).subscribe((res: any) => {

      //   console.log('res:', res);
      //   this.details[i].res.servestatus='served';
      //   // this.selectedItem= [{ Resident: 0, item: 0 }];
      //   if(this.count.toserved>0){
      //   this.count.toserved--;
      //   }
      //   this.count.served++;
      // },error=>{
      //   console.log(error)
      // })
  }

  async showOptions(options,ev){

    const popover = await this.popCntlr.create({
      component: ItemOptionsComponent,
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:options,
        flag:1
      },
     
      
      
    });
    return await popover.present();
  }
  async changeCount(item,order,i,j,k,ev,ind){
    if(this.edit){
    const popover = await this.popCntlr.create({
      component: ServingChangeCountComponent,
      event:ev,
      id:'countPop',
      backdropDismiss:false,
      componentProps:{
        quantity:order.served_quantity
      },
     
      
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      console.log(dataReturned.data)
      if(k==1){
        this.residents[ind].res.residents[i].order[0].order_items[j].served_quantity=dataReturned.data;
      }else{
        this.residents[ind].res.residents[i].additional_order[j].served_quantity=dataReturned.data;
      }
     
        
    });
    return await popover.present();
  }
  }

  // async markasserved(ev){
  //   const popover = await this.popCntlr.create({
  //     component: MarkAsServedComponent,
  //     event:ev,
  //     backdropDismiss:true,
  //     componentProps:{
       
  //     },
     
      
      
  //   });
  //   return await popover.present();
  // }



  async getDetails(t){
    this.showLoading();
    this.details=[];
    const cid=await this.storage.get('COMPANY_ID');
    const bid=await this.storage.get('BRANCH');
   
    let headers=await this.config.getHeader();
    // let url=this.config.domain_url+'serving_item_listing_table';
    let url=this.config.domain_url+'serving_item_listing_table_with_seats';
    let wing=[];
    wing.push(0)
    // let body={
    //   company_id:cid,
    //   servingtimeid:parseInt(this.ser_time_id),
    //   date:moment(this.date).format('YYYY-MM-DD'),
    //   serveareaid:parseInt(this.area_id),
    //   wing_id:wing
    // }
    let body={
      company_id:cid,
      servingtimeid:parseInt(this.ser_time_id),
      date:moment(this.date).format('YYYY-MM-DD'),
      serveareaid:parseInt(this.area_id),
      wing_id:this.wingArray,
      diettype:0

    }
    // this.http.post(url1,body1,{headers}).subscribe((res:any)=>{
    //   console.log('newRes:',res);
      
    // })
    console.log('detres:',url,body,{headers});
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      console.log('res:',res);
      
      if(res.data.menu){
      this.menu_id=res.data.menu.id;
      this.serving_order=res.data.serving_order;
      this.count=res.data.count;
      res.data.tables.forEach((ele,index)=>{
        let expanded=false;
        // if(index==0){
        //   expanded=true
        // }else{
        //   expanded=false
        // }
        
        this.details.push({table:ele,expanded:expanded})
      });
      if(t){
        if(this.details&&this.details.length){
        this.details[t].expanded=true;
        
        this.table=this.details[t].table;
        
            this.residents=[];
            for(let i in this.table.tableresidents){
                // if(this.table.seats[i].residents&&this.table.seats[i].residents.length>0){
                //   for(let j in this.table.seats[i].residents)
                //   this.residents.push({res:this.table.seats[i].residents[j],expanded:false,seat:this.table.seats[i]})
                this.residents.push({res:this.table.tableresidents[i],expanded:false})
              // }
              }
            }
      }else{
        if(this.details&&this.details.length){
        this.details[0].expanded=true;
        
      console.log('detres:',res,this.details);
      this.table=this.details[0].table;
            this.residents=[];
            for(let i in this.table.tableresidents){
                // if(this.table.seats[i].residents&&this.table.seats[i].residents.length>0){
                //   for(let j in this.table.seats[i].residents)
                //   this.residents.push({res:this.table.seats[i].residents[j],expanded:false,seat:this.table.seats[i]})
                this.residents.push({res:this.table.tableresidents[i],expanded:false})
              // }
              }
            }
          }
      this.dismissLoader();
            }else{
              this.back();
              this.presentAlert('No menu available')
              this.dismissLoader();
            }
    },error=>{
      console.log(error)
      
      this.dismissLoader();
    })
  }

  async assignSeat(seat,t,table){
    const modal = await this.modalCntrl.create({
      component: AssignSeatComponent,
      cssClass: 'full-width-modal',
      componentProps:{
        seat:seat,
        area:this.area_id,
        table_id:table
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
     this.getDetails(t);
      }
    });
    return await modal.present();
  }


  async showLoading() {
  
    const loading = await this.loadingCtrl.create({
      cssClass: 'dining-loading',
      // message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

 

  async removeSeat(item,ev,t){
    const popover = await this.popCntlr.create({
      component: RemoveSeatComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        
        consumer:item.user.user_id
        
      },
    });
      popover.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data){
          this.getDetails(t);
           }
      });

    
    return await popover.present();
  }

  expand(item){
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.details.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
          if(listItem.expanded){
            this.table=item.table;
            this.residents=[];
            for(let i in this.table.tableresidents){
                // if(this.table.seats[i].residents&&this.table.seats[i].residents.length>0){
                  // for(let j in this.table.seats[i].residents)
                  // this.residents.push({res:this.table.seats[i].residents[j],expanded:false,seat:this.table.seats[i]})
                  this.residents.push({res:this.table.tableresidents[i],expanded:false})
                // }
              }
              console.log('resi:',this.residents)
          }
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
   }

   async undoRefuse(item,ev,i,j){
    let id;
    if(item.order&&item.order.length){
      id=item.order[0].id
    }else if(item.additional_order&&item.additional_order.length){
      id=item.additional_order[0].order_id
    }
    const popover = await this.popCntlr.create({
      component: UndoRefuseComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        
        order_id:id
        
      },
    });
      popover.onDidDismiss().then((dataReturned) => {
        if(dataReturned.data){
          this.residents[j].res.seatres[i].servestatus='default';
          this.residents[j].res.seatres[i].order[0].order_items.forEach((ele,index)=>{
            this.residents[j].res.seatres[i].order[0].order_items[index].serving_status=0;
            this.residents[j].res.seatres[i].order[0].refuse_status=0
          })
          this.residents[j].res.seatres[i].additional_order.forEach((ele,index)=>{
            this.residents[j].res.seatres[i].additional_order[index].serving_status=0;
            this.residents[j].res.seatres[i].order[0].refuse_status=0
          })
          this.selectedItem = [{ Resident: 0, item: 0 , qty:1 }];
          this.selectedAdd = [{ Resident: 0, item: 0 ,qty : 1 }];
          this.selected=[];
        if(this.count.refused>0){
          this.count.refused--;
        
        }
        this.count.toserved++;
        console.log('undoRefuse:',this.residents[j],this.residents[j].res.seatres[i]);
        
           }
          
      });

    
    return await popover.present();
  }

  async getWing() {
   
    
      const bid=await this.storage.get("BRANCH")
      let headers=await this.config.getHeader();
       
        let url = this.config.domain_url + 'branch_wings/' + bid;
        this.http.get(url,{headers}).subscribe((res: any) => {
          console.log("wing:", res);
          for (let i in res.data.details) {
            console.log("data:", res.data.details[i]);

         

            this.wingArray.push(res.data.details[i].id);
           

          }
          this.getDetails(0);
        }, error => {
          console.log(error);

        })
     
  }

  async markasserved(ev){
    const popover = await this.popCntlr.create({
      component: MarkAsServedComponent,
      cssClass:'mark-served-popover',
      event:ev,
      backdropDismiss:true,
      componentProps:{
       
      },
     
      
      
    });
    popover.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
        this.markAllServed();
      }
    })
    return await popover.present();
  }

  async markAllServed(){
    let url=this.config.domain_url+'mark_all_as_served';
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    // let headers = new HttpHeaders({ 'company_id': cid.toString(), 'branch_id': bid.toString() });
    let headers=await this.config.getHeader();
    let body={
      company_id:cid,
      date:moment(this.date).format('YYYY-MM-DD'),
      servingtimeid:this.ser_time_id,
      wing_id:this.wingArray.map(String),
      searchvalue:null,
      serveareaid:parseInt(this.area_id),
      diettype:null,
      served_by:uid

    }
    console.log('markallbo:',body)
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('markall:',res);
      if(res.data==0&&res.data2==0){
        this.presentAlert('No order available')
      
      }else{
        this.presentAlert('All marked as served')
      }
      this.ionViewWillEnter();
    })
  }
}
