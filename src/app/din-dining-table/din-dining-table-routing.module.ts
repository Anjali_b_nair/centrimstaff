import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinDiningTablePage } from './din-dining-table.page';

const routes: Routes = [
  {
    path: '',
    component: DinDiningTablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinDiningTablePageRoutingModule {}
