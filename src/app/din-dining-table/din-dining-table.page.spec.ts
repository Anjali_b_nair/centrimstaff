import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinDiningTablePage } from './din-dining-table.page';

describe('DinDiningTablePage', () => {
  let component: DinDiningTablePage;
  let fixture: ComponentFixture<DinDiningTablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinDiningTablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinDiningTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
