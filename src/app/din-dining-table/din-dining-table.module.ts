import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinDiningTablePageRoutingModule } from './din-dining-table-routing.module';

import { DinDiningTablePage } from './din-dining-table.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinDiningTablePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinDiningTablePage]
})
export class DinDiningTablePageModule {}
