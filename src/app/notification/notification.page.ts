import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  subscription:Subscription;
  notification:any=[];
  currentDate:number;
  date:number;
  time:any;
  flag:any;
  message_handle: boolean = false;
  maintenance_view: boolean = false;
  act_view: boolean = false;
  story_view: boolean = false;
  call_view: boolean = false;
  visit: boolean = false;
  offset:any=0;
  constructor(private router:Router,private platform:Platform,private http:HttpClient,private config:HttpConfigService,private loadingCtrl:LoadingController,
    private storage:Storage,private route:ActivatedRoute,private alertCntlr:AlertController,private toastCntl:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display="none";
    this.notification=[];
    this.offset=0;
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.currentDate=(new Date()).getTime();
   
    this.showLoading();
    
    this.getNotification(false,'');

        (await this.config.getUserPermission()).subscribe((res: any) => {
          console.log('permissions:', res);
          let routes = res.user_routes;
          if (res.main_permissions.maintenance==1&&routes.includes('maintenance.index')) {
    
            this.maintenance_view = true
          } else {
    
            this.maintenance_view = false;
          }
    
          if (res.main_permissions.communication==1&&routes.includes('message')) {
    
            this.message_handle = true
          } else {
    
            this.message_handle = false
          }
    
          // activity permissions
          if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {
    
            this.act_view = true
          } else {
    
            this.act_view = false;
          }
    
          if (res.main_permissions.life_style==1&&routes.includes('story.index')) {
    
            this.story_view = true
          } else {
    
            this.story_view = false;
          }
          
          if (res.main_permissions.communication==1&&routes.includes('call_booking.index')) {
    
            this.call_view = true
          } else {
    
            this.call_view = false;
          }
          if (res.main_permissions.visitors==1&&routes.includes('visitor_booking.index')) {
  
            this.visit= true
          } else {
    
            this.visit = false;
            
          }
          
        })
      
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
   this.back();
});  

}

async getNotification(isFirstLoad,event){
  const data=await this.storage.get("USER_ID")
  const id=await this.storage.get('USER_TYPE');
  let url=this.config.domain_url+'notification';
  let headers=await this.config.getHeader();
  let body={
    id:data,
    offset:this.offset,
    limit:20,
    staff_app:1
  }
  console.log("head:",headers,url);
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log('res:',res);
   //  this.notification=res.data
   res.data.forEach(element => {
     this.date=(new Date(element.created_at)).getTime();
     let t=this.currentDate-this.date;
     let min=Math.floor(t/ (60*1000));
     let hr = Math.floor(t/ 3600 / 1000); // round to nearest second
     let days= Math.floor(t/ (60*60*24*1000));
     let month=Math.floor(t/ (60*60*24*1000*31));
     let yr=Math.floor(t/ (60*60*24*1000*31*12))
     // console.log("time:",this.com_time);
     if(month>12){
       this.time=yr+ ' years ago';
     }
     else if(days>31){
       this.time=month+ ' months ago ';
     }
     else if(hr>24){
       
       this.time=days+ ' days ago ';
     }else if(min>60){
       this.time=hr+' hours ago ';
     }
     else if(min<=60 && min>1){
       this.time=min+' minutes ago ';
     }
     else {
       this.time="Now";
     }
     
    
    if(id!=4){   
     if(element.not_type==11 ||element.not_type == 16||element.not_type ==17||element.not_type == 18||element.not_type == 19){
     }else{
     this.notification.push({'id': element.id,
     'user_id': element.user_id,
     'not_type': element.not_type,
     'link_id': element.link_id,
     'notification_icon': element.notificaiton_icon,
     'title':element.title,
     'payload': element.payload,
     'readstatus':element.readstatus,
     'created_at': this.time})
     }
   }else{
     if(element.not_type==12){
       this.notification.push({'id': element.id,
     'user_id': element.user_id,
     'not_type': element.not_type,
     'link_id': element.link_id,
     'notification_icon': element.notificaiton_icon,
     'title':element.title,
     'payload': element.payload,
     'readstatus':element.readstatus,
     'created_at': this.time})
     }else{
     
     }
   }

   
    });
    this.dismissLoader();
    if (isFirstLoad)
    event.target.complete();

  this.offset=this.offset+20;
    console.log("not:",this.notification,this.offset);
   });
}
back(){
  if(this.flag==1){
    this.router.navigate(['/stories'],{replaceUrl:true})
  }else if(this.flag==2){
    this.router.navigate(['/menu'],{replaceUrl:true});
  }else{
    this.router.navigate(['/feeds'],{replaceUrl:true});
  }
}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

async gotoDetails(item,idx){
  // this.readItem(item.id);
  
    const id=await this.storage.get('USER_TYPE')
  if(item.not_type==1 ||item.not_type==2 ||item.not_type==3 || item.not_type==5 || item.not_type==6 || item.not_type==14){
    if(this.story_view ){
    this.router.navigate(['/story-details',{post_id:item.link_id,flag:2}])
    }else{
      this.presentAlert('You are not allowed to perform this function.')
    }
  }
  else if(item.not_type==4 ){
    if(this.act_view){
    this.router.navigate(['/activity-details',{act_id:item.link_id}]);
  }else{
    this.presentAlert('You are not allowed to perform this function.')
  }
  }
  else if(item.not_type==7){
    if(this.act_view){
    this.router.navigate(['/activity-details',{act_id:item.link_id}]);
  }else{
    this.presentAlert('You are not allowed to perform this function.')
  }
  }
  else if(item.not_type==8){
    if(this.message_handle){
    this.router.navigate(['/message']);
  }else{
    this.presentAlert('You are not allowed to perform this function.')
  }
  }
  else if(item.not_type==9){
    if(this.call_view){
    this.router.navigate(['/callbooking-list']);
  }else{
    this.presentAlert('You are not allowed to perform this function.')
  }
  
  
  
  // else if(item.not_type==11){
  //   this.router.navigate(['/newsletter']);
  }else if(item.not_type==21){
    if(this.visit){
    this.router.navigate(['/visit-booking-list']);
  }else{
    this.presentAlert('You are not allowed to perform this function.')
  }
  }else if(item.not_type==12||item.not_type==13){
    if(this.maintenance_view){
    
      this.router.navigate(['/maintenance-details',{filter:0,req_id:item.link_id}]);
    }else{
      this.presentAlert('You are not allowed to perform this function.')
    }
    
  }
  this.readItem(item.id,idx)

}


async readItem(id,idx){
 
    const bid=await this.storage.get("BRANCH")

  console.log("delete");
  
  let url=this.config.domain_url+'readnotification';
  let body={not_id:id}
  let headers=await this.config.getHeader();;
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.notification.splice(idx,1);
  })

}
async readAll(){
  
    const data=await this.storage.get("USER_ID")

      
        const bid=await this.storage.get("BRANCH")

      let url=this.config.domain_url+'readallnotification';
      let body={user_id:data}
      let headers=await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.notification=[];
        this.presentAlert('You have cleared all notifications')
      })
    
}
doInfinite(event){
  this.getNotification(true,event)
}
async readAllAlert(){
  const alert = await this.alertCntlr.create({
    mode:'ios',
    header: 'Clear Notification',
    message: 'Are you sure you want to clear all the notifications.',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Clear',
        handler: () => {
          this.readAll();
        }
      }
    ]
  });

  await alert.present();
}

async presentAlert(mes){
  const alert = await this.toastCntl.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

}
