import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateActivityPageRoutingModule } from './create-activity-routing.module';

import { CreateActivityPage } from './create-activity.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ActivityStaffComponent } from '../components/activity-staff/activity-staff.component';
// import { RichTextModule } from 'ionic-rich-text/dist/rich-text-module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CreateActivityPageRoutingModule,
    ApplicationPipesModule
    // RichTextModule
  ],
  declarations: [CreateActivityPage,ActivityStaffComponent],
  entryComponents:[ActivityStaffComponent]
})
export class CreateActivityPageModule {}
