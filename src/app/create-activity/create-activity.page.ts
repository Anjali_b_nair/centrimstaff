import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActivityAttendeesComponent } from '../components/activity-attendees/activity-attendees.component';
import { ActivityRecurringComponent } from '../components/activity-recurring/activity-recurring.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ViewAttendeesComponent } from '../components/view-attendees/view-attendees.component';
import { File } from '@ionic-native/file/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { AttchedImagesComponent } from '../components/attched-images/attched-images.component';
import { DOCUMENT } from '@angular/common';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { ActivityStaffComponent } from '../components/activity-staff/activity-staff.component';
import { ActivityWarningComponent } from '../components/activity-warning/activity-warning.component';
declare var $: any;
@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.page.html',
  styleUrls: ['./create-activity.page.scss'],
})
export class CreateActivityPage implements OnInit {
  @ViewChild('summernote') private summernote: ElementRef;
// markupStr :any; 
subscription:Subscription;
types:any=[];
typeId:any;
categories:any=[];
category:any=[];
branch:any=[];
facility:any=[];
title:any;
description:any;
minDate: string = new Date().toISOString();
date:any;
start:any;
end:any;
venue:any;
status:any=1;
img:any=[];
imageResponse:any=[];
docs:any=[];
doc:any=[];
docResponse:any=[];
// item:any;
flag:any;
id:any;
attendees:any[]=[];
attendeesCopy:any[]=[];
attendee_id:any;
startDates:any[]=[];
calendar:any[]=[];
calendar_type:any;
hide:boolean=true;
base64File:any;
callLink:boolean=false;
cal:any=0;
stat:any=0;
isLoading:boolean=false;

repeat:any={};
current:any;
staffArray:any[]=[];
staff:any[]=[];
staffCopy:any[]=[];
disable:boolean=false;

listener;
selectAllCheckBox: any;
checkBoxes: HTMLCollection;

customAlertOptions: any = {
  
  header: 'Calendar types',
  // subHeader: 'Select All:',
  message: '<ion-checkbox id="selectAllCheckBox1"></ion-checkbox><ion-label class="selectAll-label">Select All</ion-label>'
};
utype:any;
rec_details:any;

rvsettings:any;
slot:any;
waiting:any=false;
paid:any=false;
cost:any;
one_to_one:any=false;
assign:boolean=false;
limit_required:any;
payment_required:any;
// public customOptions: any = {
//   header: "Calendar types"
// };
public categoryOptions: any = {
  header: "Categories"
};
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,
    private actionsheetCntlr:ActionSheetController,
    private modalCntrl:ModalController,private route:ActivatedRoute,private platform:Platform,private router:Router,
    private toastCntlr:ToastController,private file:File,private alertCntlr:AlertController,
    private chooser:Chooser,private clip:Clipboard,private loadingCtrl:LoadingController,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) {  }

  ngOnInit() {
   
    $('#summernote').summernote({
      placeholder: 'Enter text here',
      tabsize: 2,
      height: 120,
      toolbar: [
        ['font', ['bold', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
      ]
    });

    
   
  }
  async ionViewWillEnter(){
    this.ngOnInit();
    this.disable=false;
    this.minDate= new Date().toISOString();
    this.date=new Date().toISOString();
    this.current=moment(this.date).format('yyyy-MM-DD');
    this.start=new Date(this.current+'T09:00').toISOString();
    this.end=new Date(this.current+'T09:30').toISOString();

    
    this.cal=0;
  this.flag=this.route.snapshot.paramMap.get('flag');
  this.id=this.route.snapshot.paramMap.get('id');
  const rv=await this.storage.get('RVSETTINGS');
  this.rvsettings=rv
  if(rv==1){
    this.getRVsettings();
  }
    const bid=await this.storage.get("BRANCH")

    let headers=await this.config.getHeader();
    this.types=[];
    this.categories=[];
    this.imageResponse=[];
    this.img=[];
    this.docs=[];
    this.docResponse=[];
    this.attendees=[];
    this.branch=[];
    this.category=[];
    this.facility=[];
    this.startDates=[];
    this.getStaff();
    let url=this.config.domain_url+'article_types';
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      for(var i in res.data){
        this.types.push(res.data[i]);
      }
     
      
      
    });
    let cat_url=this.config.domain_url+'activity_categories';
  
    this.http.get(cat_url,{headers}).subscribe((res:any)=>{
    
      // this.categories.push({id:0,category_name:'Select All'});
      res.data.forEach(element => {
        this.categories.push({id:element.id,category_name:element.category_name})
      });
     
      
    })
    // this.storage.ready().then(()=>{
      const data=await this.storage.get("USER_TYPE")

        this.utype=data;
        const cid=await this.storage.get("COMPANY_ID")

// let headers=await this.config.getHeader();;
        // this.branch.push({id:0,name:'Select All'})
          // this.user_type=data;
          let url2=this.config.domain_url+'branch_viatype';
          let body={
            company_id:cid,
            role_id:data
          }
          if(data=='1'){
            this.http.post(url2,body,{headers}).subscribe((res:any)=>{
              if(data==1){
                // this.branch.push({id:0,name:'Select All'});
                }
              res.data.forEach(element => {
                this.branch.push({id:element.id,name:element.name});
              });
              this.facility=bid.toString();
            })
          }else{
          this.http.post(url2,body,{headers}).subscribe((res:any)=>{
            res.data.forEach(element => {
              this.branch.push({id:element.id,name:element.name});
            });
            // this.facility=this.branch[0].id.toString();
            this.facility=bid.toString();
          })
        }

        this.calendar=[];
        // this.Calendar.push({id:0,wing:'All wings'});
       
            
            let branch=cid.toString();
            let body1={company_id:branch}
            let url1=this.config.domain_url+'get_calendar_types';
            
            this.http.post(url1,body1,{headers}).subscribe((res:any)=>{
             
              for(let i in res.data){
                 
                  
                let obj={id:res.data[i].calendar_id,name:res.data[i].calendar_types.type};
                this.calendar.push(obj)
              
              }
             
              
              
            },error=>{
              
              
            });
         
            (await this.config.getUserPermission()).subscribe((res: any) => {
             
              let routes = res.user_routes;
              if (res.main_permissions.life_style==1&&routes.includes('activity.assign')) {
        
                this.assign = true
              } else {
        
                this.assign = false;
                
              }
            })
       
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      if(this.flag==1){
      this.router.navigate(['/activities'],{replaceUrl: true}) ;
    }else if(this.flag==2){
      this.router.navigate(['/consumer-profile',{id:this.id}],{replaceUrl: true})
    }else{
      this.router.navigate(['/menu'],{replaceUrl: true});
    }
    this.clearData();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }


      selectBranch(ev,facility){
        
        if(facility.includes('0')){
          this.facility=[];
          // let b=this.branch.splice(0,1);
          this.branch.forEach(element => {
            if(this.facility.includes(element.id.toString())){
  
            }else{
              if((element.id.toString())!='0'){
  
             
            this.facility.push(element.id.toString());
              }
            }
  
          });
          
          
          
          
        }
        
    }

select(event){
  if(event.currentTarget.checked==true){
    this.status=1
  }else{
    this.status=0
  }
 
}

back(){
  if(this.flag==1){
    this.router.navigate(['/activities'],{replaceUrl: true}) ;
  }else if(this.flag==2){
    this.router.navigate(['/consumer-profile',{id:this.id}],{replaceUrl: true})
  }else{
    this.router.navigate(['/menu'],{replaceUrl: true});
  }
  this.clearData();
}

async publish(){
  this.disable=true;
  let a=this.attendees[0];
  this.attendees.forEach(ele=>{
    if(ele==a){
    this.attendee_id=ele.toString();
    }else{
      this.attendee_id=this.attendee_id+','+ele
    }
  })
 
    const cid=await this.storage.get("COMPANY_ID")
    const bid=await this.storage.get("BRANCH")
      const uid=await this.storage.get("USER_ID")

        let s=moment(this.start).format('HH:mm')
        let stime=moment(this.start).format('hh:mm A');
        let etime=moment(this.end).format('hh:mm A');
        let img;
        let doc;
        let url=this.config.domain_url+'activity';
        let headers=await this.config.getHeader();;
        if(this.img.length==0){
          img=null
        }else{
          img=this.img
        }
        if(this.docs.length==0){
          doc=null
        }else{
          doc=this.docs
        }



        let category;
   
    
    if(this.category.length>0){
      let c=this.category[0];
        this.category.forEach(ele => {
          if(ele==c){
            category=c;
          }else{
            category=category+','+ele;
          }
        });
          
        
     
    }else{category=null}
    let facility=[];
   
    if(Array.isArray(this.facility)){
      facility=this.facility;
    
    
   
  }else{
    facility.push(this.facility.toString());
   
  }
  let staff=[];
   
    if(this.staff.length>0){
      staff=this.staff;
   
   
  }else{
    staff=null;
  }
    // this.start=moment(this.start).format('hh:mm A');
    // this.end==moment(this.end).format('hh:mm A');
      
        if(this.startDates.length==0){
          this.startDates.push(moment(this.date).format('yyyy-MM-DD'))
        }
        let rv,slot,waiting,paid,cost;
        if(this.rvsettings==1){
          rv=1;
          if(this.limit_required==1){
          slot=this.slot;
          }else{
            slot=0;
          }
          if(this.waiting){
            waiting=1
          }else{
            waiting=0
          }
          if(this.paid){
            paid=1
            cost=this.cost
          }else{
            paid=0;
            cost=null
          }
        }else{
          rv=0;
          slot=0
        }
        let one_to_one;
        if(this.one_to_one){
          one_to_one=1
        }else{
          one_to_one=0
        }
        for(let i=0;i<this.startDates.length;i++){
          let ac_date=this.startDates[i]+' '+s
        
          
        let body={
          activity:{
          activity_type:this.typeId,
          title:this.title,
          activity_info:$('#summernote').summernote('code'),
          activity_start_date:moment(ac_date).format('yyyy-MM-DD HH:mm:ss'),
          start_time:stime,
          end_time:etime,
          venue:this.venue,
          created_by:uid,
          status:this.status,
          web_display:1,
          enable_notification:1,
          company_id:cid,
          one_to_one_session:one_to_one
        },
          categories:this.category,
          branches:facility,
          document:doc,
          images:img,
          attendees:this.attendee_id,
          calendar_type:this.calendar_type,
          assigned_to:staff,
          rv_required:rv,
          slot_limit:slot,
          paid_activity:paid,
          cost:cost,
          waiting_list:waiting,
          branch_id:bid

        }
        this.clip.clear();
      
       
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
         

          this.presentAlert("Activity created successfully.");
          this.clearData();
          this.router.navigate(['/activities'],{replaceUrl: true});
          this.disable=false;
        },error=>{
         
          this.presentAlert("Something went wrong. Please try again later.");
          this.disable=false;
        })
      }
     
  // if(res.status=="success"){
   
// }else{
  // this.presentAlert('Something went wrong. Please try again later.')
// }
  
}
addCalendar(ev){
  if(ev.currentTarget.checked==true){
    this.hide=false;
  }else{
    this.calendar_type=null;
    this.hide=true;
  }
}
async selectImage() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.addImage();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.captureImage();
      }
    },
    {
      text: 'Cancel',
      // role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
}

async captureImage(){
  
  

  const image = await Camera.getPhoto({
    quality: 90,
    allowEditing: false,
    resultType: CameraResultType.Base64,
    correctOrientation:true,
    source:CameraSource.Camera
  });

 
  var imageUrl = image.base64String;

 
  let base64Image = 'data:image/jpeg;base64,' + imageUrl;
  console.log('image:',imageUrl);
  this.imageResponse.push(base64Image);
  this.uploadImage(base64Image);
}



async addImage(){

  

  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
     
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
            this.uploadImage('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }
  
  }

  selectDoc(){
    this.chooser.getFile()
  .then(file => {
   
    if(file){
      console.log('type:',file.mediaType);
    
      if(file.mediaType=="application/pdf" || file.mediaType=="application/msword" || file.mediaType=="application/doc"
      || file.mediaType=="application/ms-doc" || file.mediaType=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      || file.mediaType=="application/excel"|| file.mediaType=="application/vnd.ms-excel"|| file.mediaType=="application/x-excel"
      || file.mediaType=="application/x-msexcel"|| file.mediaType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
      this.uploadDoc(file.dataURI,file.name);
    }else{
      this.alert('File format not supported');
    }
  }
  })
  .catch((error: any) => console.error(error));
  }



  async alert(mes){
    
    const alert = await this.alertCntlr.create({
      mode:'ios',
      message: mes,
      backdropDismiss:true
    });
  
    await alert.present();
  }


async uploadImage(img){
  if(this.isLoading==false){
  this.showLoading();
  }
  let url=this.config.domain_url+'upload_file';
  let body={file:img}

let headers=await this.config.getHeader();;
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
  
    this.img.push(res.data);
    this.dismissLoader();
  },error=>{
   
    console.log(error);
    this.dismissLoader();
  })
}
async uploadDoc(file,fname){

  this.showLoading();
  
  let fileName ;
  let name=fname.split('.',1)
  if(name.toString().length>3){
    fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
  }
  this.docResponse.push({name:fileName});
 
         let  url = this.config.domain_url+'upload_file';
         let headers=await this.config.getHeader();;
         let body={file:file}
       
         this.http.post(url,body,{headers}).subscribe((res:any)=>{
         
           this.docs.push(res.data);
           this.dismissLoader();
         },error=>{
           console.log(error);
this.dismissLoader();
         })
         
 
}


async  makeRecurring(){
  const modal = await this.modalCntrl.create({
    component: ActivityRecurringComponent,
    cssClass:'full-width-modal',
    componentProps: {
      data:this.date,
      rep:this.repeat
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    console.log('recreturned:',dataReturned);
    
    if (dataReturned.data == null||dataReturned.data == undefined) {
      
      this.startDates=[];
      
    }else{
      
      this.repeat=dataReturned.role;
      if(this.repeat.status==2){
      this.startDates = dataReturned.data;
      this.rec_details = this.repeat.rec_details;
      }
    }
   
    
  });
  return await modal.present();
}
async addAttendees() {
  if(this.limit_required==1&&!this.slot){
    this.presentAlert('Please enter the slot limit.')
  }else{
  this.attendeesCopy=this.attendees.filter(x=>x);
  const modal = await this.modalCntrl.create({
    component: ActivityAttendeesComponent,
    cssClass:'full-width-modal',
    componentProps: {
      data:this.attendeesCopy,
      slot:this.slot,
      waiting:this.waiting,
      limit_required:this.limit_required
      
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data ) {

   
     
      
      this.attendees=dataReturned.data;
      
      
    }else{
      this.attendeesCopy=this.attendees
    }
  });
  return await modal.present();
}
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'      
  });
  alert.present(); //update
}

async viewAttendees(){
  const modal = await this.modalCntrl.create({
    component: ViewAttendeesComponent,
    cssClass:'full-width-modal',
    componentProps: {
      data:this.attendees
      
    }
  });
  return await modal.present();
}


removeImg(i){
  this.imageResponse.splice(i,1);
  this.img.splice(i,1);
}


removeDoc(i){
  this.docResponse.splice(i,1);
  this.docs.splice(i,1);
}

typeChange(typeId){
 this.typeId=typeId
 
  
  if(typeId==9){
   
    
    this.callLink=true;
  }else{
    this.callLink=false;
  }
}

generateRoom(){
  let outString: string = '';
  let inOptions: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  for (let i = 0; i < 9; i++) {

    outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

    
  }
  return outString;
}

async generate(){
  let jitsiRoom=this.generateRoom();
 
   const data=await this.storage.get("COMPANY_ID")

     let url=this.config.domain_url+'generatecallurl';
     let headers=await this.config.getHeader();
     let body={company_id:data}
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
       let callLink=res.vdocalllink+jitsiRoom;
      //  this.jitsiCall(callLink);
      //  this.sendLink(callLink);
      this.callPrompt(callLink);
       
     })
   

}

async callPrompt(callLink) {
  
  const alert = await this.alertCntlr.create({
    mode:'ios',
    header: 'Copy&paste video call link',
    backdropDismiss:true,
    message: 'Copy and paste the link in the description field.',
    inputs: [
      {
        name: 'link',
        type: 'text',
        
        value: callLink,
        disabled:true
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
        
        }
      }, {
        text: 'Copy & Paste',
        handler: () => {
         
          this.clip.copy(callLink);
          this.clip.paste().then(
            (resolve: string) => {
              let des=$('#summernote').summernote('code')+'<p><a href="'+resolve+'" target="_blank">Click here to start video call</a><br></p>';
              $('#summernote').summernote('code',des);
             
             },
             (reject: string) => {
               
               
             }
           );
         
        

        }
      }
    ]
  });
  
  await alert.present();
 
}
clearData(){
  this.types=[];
    this.categories=[];
    this.imageResponse=[];
    this.img=[];
    this.docs=[];
    this.docResponse=[];
    this.attendees=[];
    this.branch=[];
    this.category=[];
    this.facility=[];
    this.startDates=[];
    this.title='';
    this.venue='';
    this.typeId='';
    this.callLink=false;
    this.hide=true;
    this.calendar_type='';
    this.start='';
    this.end='';
    this.status=1;
    this.cal=0;
    $('#summernote').summernote('code','');




}
async showLoading() {
  this.isLoading=true;
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    message: 'Please wait...',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
  this.isLoading=false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

confirm(){
  this.disable=true;
 
  let currenttime=new Date().getTime();
  let date=moment(this.date).format('yyyy-MM-DD');
  let start=new Date(this.current+' '+moment(this.start).format('hh:mm A')).getTime();
  let end=new Date(this.current+' '+moment(this.end).format('hh:mm A')).getTime();
 
  
  let desc=$('#summernote').summernote('code');
  if(this.title==undefined||this.title==''){
    this.presentAlert('Please enter the title.');
    this.disable=false;
  }
  else if(this.typeId==undefined||this.typeId==''){
    this.presentAlert('Please select wellness dimension.');
    this.disable=false;
  }else if(this.start==undefined||this.start==''||this.end==undefined||this.end==''){
    this.presentAlert('Please enter start & end time.');
    this.disable=false;
  }else if(Array.isArray(this.facility)&&this.facility.length==0){
    this.presentAlert('Please select atleast one facility.');
    this.disable=false;
  }else if(date==this.current && (start<currenttime|| end<currenttime)){
    this.presentAlert('Please select valid date & time.');
    this.disable=false;
  }else if(start>end){
    this.presentAlert('The end time must be a time after start time.');
    this.disable=false;
  }else if(this.rvsettings==1&&this.limit_required==1&&(this.slot==undefined||this.slot=='')){
    this.presentAlert('Please enter the slot limit.');
    this.disable=false;
  }else if(this.rvsettings==1&&this.paid&&(this.cost==''||this.cost==undefined)){
    this.presentAlert('Please enter the cost.');
    this.disable=false;
  }else if(this.cal==1&&!this.calendar_type){
    this.presentAlert('Please select a calendar type.');
    this.disable=false;
  }else{
    if(this.calendar_type&&this.calendar_type.length){
      this.validate_Activity();
    }else{
    this.publish();
    }
  }
}
async validate_Activity(){
  const cid=await this.storage.get("COMPANY_ID")
      
  let headers=await this.config.getHeader();
  let s=moment(this.start).format('HH:mm')
  let stime=moment(this.start).format('hh:mm A');
  let ac_date=moment(this.date).format('yyyy-MM-DD')+' '+s
  let url=this.config.domain_url+'validate_activity';
  let body={
    company_id:cid,
    activity_start_date:moment(ac_date).format('yyyy-MM-DD HH:mm:ss'),
    start_time:stime,
    calendar_type:this.calendar_type
  }
    console.log('validatebody:',body);
    
   this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log('validate:',res);
    if(res.status=='success'){
      this.publish();
    }else{
      this.activityTimeWarning();
    }
    
   })
}
async activityTimeWarning(){
  const modal = await this.modalCntrl.create({
    component: ActivityWarningComponent,
    cssClass: 'full-width-modal'
    
  });
  modal.onDidDismiss().then((dataReturned) => {
    if (dataReturned.data) {
      this.publish();
    }else{
      this.disable=false;
    }
  });
  return await modal.present();
}
async presentActionSheet() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: 'Select',
    buttons: [{
      text: 'Image',
      // role: 'destructive',
      icon: 'image-outline',
      handler: () => {
        this.selectImage();
        console.log('Delete clicked');
      }
    }, {
      text: 'Document',
      icon: 'document-outline',
      handler: () => {
        this.selectDoc();
        console.log('Favorite clicked');
      }
    }, {
      text: 'Cancel',
      icon: 'close',
      // role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
}

async viewImage(){
  const modal = await this.modalCntrl.create({
    component: AttchedImagesComponent,
   
    componentProps: {
     data:this.imageResponse,
     
     
    }
  });
  modal.onDidDismiss().then((dataReturned) => {
    
  });
  return await modal.present();
}
async getStaff(){
  
    const bid=await this.storage.get("BRANCH")

  
    const utype=await this.storage.get("USER_TYPE")

    const uid=await this.storage.get("USER_ID")

  const cid=await this.storage.get("COMPANY_ID")
  this.staffArray=[];
 
  this.staffArray.push({user_id:'0',name:'All'})
  
 
        

  let url1=this.config.domain_url+'activity_staffs';
 
    let headers=await this.config.getHeader();
   

      let body={company_id:cid,status:1}
        this.http.post(url1,body,{headers}).subscribe((res:any)=>{
         
          let staff=res.data.super_admins.concat(res.data.managers).concat(res.data.staffs)
        
          staff.forEach(element => {
            this.staffArray.push({user_id:element.user_id,name:element.name})
          });
         
        })
    
}
selectStaff(ev,staff){

  if(staff.includes('0')){
    this.staff=[];
    this.staffCopy=[];
    // let b=this.branch.splice(0,1);
    this.staffArray.forEach(element => {
      if(this.staff.includes(element.user_id.toString())){

      }else{
        if((element.user_id.toString())!='0'){

       
      this.staff.push(element.user_id.toString());
      
        }
      }
     
    });
    
    
    
    
  }
  
}


// openSelector(selector) {
//   selector.open().then((alert)=>{
//     this.selectAllCheckBox = this.document.getElementById("selectAllCheckBox1");
//    this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
//     this.listener = this.renderer.listen(this.selectAllCheckBox, 'click', () => {
//         if (this.selectAllCheckBox.checked) {
//           for (let checkbox of this.checkBoxes) {
//             if (checkbox.getAttribute("aria-checked")==="false") {
//               (checkbox as HTMLButtonElement).click();
//               this.calendar_type=[];
//             };
//           };
//         } else {
//           for (let checkbox of this.checkBoxes) {
//             if (checkbox.getAttribute("aria-checked")==="true") {
//               (checkbox as HTMLButtonElement).click();
//               this.branch.forEach(element => {
//                 if(this.calendar_type.includes(element.id.toString())){
      
//                 }else{
//                   if((element.id.toString())!='0'){
      
                 
//                 this.calendar_type.push(element.id.toString());
//                   }
//                 }
      
//               });
//             };
//           };
//         }
//     });
//     alert.onWillDismiss().then(()=>{
//       this.listener();
     
//     });
//   })
  
// }



openSelector(selector) {
  console.log(this.calendar);
  console.log(this.calendar_type);
  selector.open().then((alert) => {
    this.selectAllCheckBox =
      this.document.getElementById("selectAllCheckBox1");
    // check the array "calender_type" is undefined or not and if not check the length of the array with Data "calender" array
    if (
      this.calendar_type != undefined &&
      this.calendar.length == this.calendar_type.length
    ) {
      console.log("1079");
      // if selected option is clicked before , then select all option check button is checked using HTMLButtonElement click function
      (this.selectAllCheckBox as HTMLButtonElement).click();
    } else console.log("1080");
    console.log(this.selectAllCheckBox);
    this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
    this.listener = this.renderer.listen(
      this.selectAllCheckBox,
      "click",
      () => {
      
        if (this.selectAllCheckBox.checked) {
         
          for (let checkbox of this.checkBoxes) {
            if (checkbox.getAttribute("aria-checked") === "false") {
              (checkbox as HTMLButtonElement).click();
              this.calendar_type = [];
            }
          }
        } else {
         

          for (let checkbox of this.checkBoxes) {
            if (checkbox.getAttribute("aria-checked") === "true") {
              (checkbox as HTMLButtonElement).click();
              this.branch.forEach((element) => {
                if (this.calendar_type.includes(element.id.toString())) {
                } else {
                  if (element.id.toString() != "0") {
                    this.calendar_type.push(element.id.toString());
                  }
                }
              });
            }
          }
        }
      }
    );
    alert.onWillDismiss().then(() => {
      this.listener();
    });
  });
}


async getRVsettings(){
  const bid=await this.storage.get("BRANCH");
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_rv_settings/'+bid
  this.http.get(url,{headers}).subscribe((res:any)=>{
    this.limit_required=res.data.limit_required;
    this.payment_required=res.data.payment_required;
  })
}

async addStaff(i) {
 this.staffCopy=this.staff.filter(x=>x);
  const modal = await this.modalCntrl.create({
    component: ActivityStaffComponent,
    cssClass:'full-width-modal',
    mode:'md',
    componentProps: {
      data:this.staffCopy, 
      flag:i
    }
  });
  modal.onDidDismiss().then((dataReturned) => {

    console.log('dataret:',dataReturned,this.staff,this.staffCopy);
    
    if (dataReturned.data) {

   
      
      this.staff=dataReturned.data;
     
      
    }else{
      this.staffCopy=this.staff;
    }
  });
  return await modal.present();
}


}



