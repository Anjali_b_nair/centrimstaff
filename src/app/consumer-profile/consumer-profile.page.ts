import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'
;
import { Subscription } from 'rxjs';
import { GetobjectService } from '../services/getobject.service';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-profile',
  templateUrl: './consumer-profile.page.html',
  styleUrls: ['./consumer-profile.page.scss'],
})
export class ConsumerProfilePage implements OnInit {
id:any;
consumer:any={};
img:any;
name:any;
branch:any;
room:any;
user_id:any;
branch_id:any;
subscription:Subscription;
wing:any;
flag:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
    private router:Router,private objService:GetobjectService,private platform:Platform,
    private storage:Storage,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
 async ionViewWillEnter(){
   this.id=this.route.snapshot.paramMap.get('id');
   this.flag=this.route.snapshot.paramMap.get('flag');
   this.showLoading();
   
    const bid=await this.storage.get("BRANCH")

   let url=this.config.domain_url+'consumer/'+this.id;
   let headers=await this.config.getHeader();;
   console.log("dat:",url,headers)
   this.http.get(url,{headers}).subscribe((res:any)=>{
    
     this.consumer=res.data;
     this.img=this.consumer.user.profile_pic
     this.name=this.consumer.user.name;
     this.room=this.consumer.room;
     this.wing=this.consumer.wing.name;
     this.user_id=this.consumer.user.user_id;
     this.branch=this.consumer.user.user_details.user_branch.name;
     this.branch_id=this.consumer.user.user_details.user_branch.id;
     console.log('con:',this.consumer);
     this.dismissLoader();
      
   })
  
   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
     if(this.flag==1){
    this.router.navigate(['/consumers',{flag:1}]) ; 
     }else{
      this.router.navigate(['/birthdays']) ;
     }
}); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
 about(){
   this.objService.setExtras(this.consumer);
   this.router.navigate(['/consumer-about',{flag:this.flag}])
 }

 contacts(){
  // this.objService.setExtras(this.consumer);
  this.router.navigate(['/consumer-contacts',{id:this.consumer.id,flag:this.flag}]);
}
overview(){
  this.router.navigate(['/consumer-overview',{id:this.user_id,cid:this.id,branch:this.branch_id,flag:this.flag}]);
}
gallery(){
  this.router.navigate(['/consumer-gallery',{id:this.user_id,cid:this.id,flag:this.flag}])
}
pettycash(){
  // this.objService.setExtras(this.consumer);
  this.router.navigate(['/petty-cash',{flag:this.flag,id:this.consumer.id}]);
}

report(){
  // this.objService.setExtras(this.consumer);
  this.router.navigate(['/consumer-report',{id:this.consumer.user.user_id,cid:this.consumer.id,flag:this.flag}])
}


async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

back(){
  if(this.flag==1){
    this.router.navigate(['/consumers',{flag:1}]) ; 
     }else{
      this.router.navigate(['/birthdays']) ;
     }
}
newStatus(){
  this.router.navigate(['/new-status',{name:this.name,id:this.id,branch:this.branch_id,user_id:this.user_id,from:this.flag}],{replaceUrl:true})
}
}
