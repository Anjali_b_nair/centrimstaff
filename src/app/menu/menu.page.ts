import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular'
  ;
import { Observable, Subscription } from 'rxjs';
import { ActionSheetController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { SetBranchComponent } from '../components/set-branch/set-branch.component';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { PushNotifications } from '@capacitor/push-notifications';

// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  type: any;
  name: string;
  img: any;
  p_name: string;
  subscription: Subscription;
  user_type: any;
  msgcount: any;
  counter = 0;
  applaunched: boolean;
  notify: any;
  not_array: any = [];
  welcome: string;
  branch: any = [];
  branch_id: any;
  branch1: any;
  flag: any;
  login_branch: any;
  modules: any[] = [];

  show: boolean = false;

  message_handle: boolean = false;
  maintenance_view: boolean = false;
  act_view: boolean = false;
  story_view: boolean = false;
  res_view: boolean = false;
  user_view: boolean = false;
  call_view: boolean = false;
  vis_view: boolean = false;
  dining_view: boolean = false;
  feedback:boolean=false;
  task_view:boolean=false;
  rv:any;
  // bname:any;
  token_generated:boolean=false;
  apicallednow: boolean = false;
  userstatus: any;
  constructor(public storage: Storage, private platform: Platform, private router: Router, private toastCntlr: ToastController,
    private http: HttpClient, private config: HttpConfigService, private route: ActivatedRoute, private popCntl: PopoverController,
    private orient: ScreenOrientation, private actionsheetCntlr: ActionSheetController, private modalCntl: ModalController) { }

  ngOnInit() {
  }
  // compareFn(e1, e2): boolean {
  //   return  this.branch[0].id ===e1.id  ;
  // }
  setBranch(ev) {
    let rv,pcf;
    this.branch.forEach(element => {
      if (element.name == this.branch1) {
        this.branch_id = element.id;
        rv=element.retirement_living;
        pcf=element.pcs_branch_id;
      }
    });

    console.log("branch:", this.branch_id);
    this.storage.set("BRANCH", this.branch_id.toString());
    this.storage.set('RVSETTINGS',rv);
    this.storage.set("BNAME", this.branch1);
    this.storage.set("PCFBRANCH",pcf);
    this.rv=rv;
  }
  async ionViewWillEnter() {
    this.rv=await this.storage.get('RVSETTINGS');
    this.userstatus = await this.storage.get("loginedupdate");
    let headers=await this.config.getHeader();
    this.platform.ready().then(()=>{
      // if(this.platform.is('ios')||this.platform.is('ipad')||this.platform.is('iphone')){
      //   this.firebase.hasPermission().then((data)=>{
      //     if(!data)
      //     this.firebase.grantPermission().then(()=>{
          
      //     })
      //   });
        
          
      //   }
      })
    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
    this.not_array = [];
    this.flag = this.route.snapshot.paramMap.get('flag');
    if (this.flag == 1) {
      this.login_branch = this.route.snapshot.paramMap.get('branch');
      // this.storage.set('BRANCH',this.login_branch.toString());
    }

    let date = new Date();
    let hr = date.getHours();
    console.log("hr:", hr)
    if (hr <= 6) {
      this.welcome = 'Welcome'
    }
    else if (hr < 12) {
      this.welcome = 'Good morning'
    }
    else if (hr < 17&&hr>=12) {
      this.welcome = 'Good afternoon'
    }
    else if (hr >= 17) {
      this.welcome = 'Good evening'
    }
    // this.storage.set("launched",this.applaunched);

    const name = await this.storage.get("NAME")
    this.name = name;
    this.p_name = this.name.substring(0, 1);

    const pic = await this.storage.get("PRO_IMG")
    this.img = pic;

    const id = await this.storage.get("USER_ID")
    const data = await this.storage.get("USER_TYPE")

    const cid = await this.storage.get("COMPANY_ID")

   
    let murl = this.config.domain_url + 'get_modules/' + cid;
    this.http.get(murl,{headers}).subscribe((mod: any) => {
      this.show = true;
      this.modules = mod
      console.log("modules:", this.modules);

    })
    // if (data == 4) {
    //   const tabBar = document.getElementById('myTabBar');
    //   tabBar.style.display = "none";
    // } else {
    //   const tabBar = document.getElementById('myTabBar');
    //   tabBar.style.display = "flex";
    // }
    const bid = await this.storage.get("BRANCH");
    // this.bname=await this.storage.get('BNAME');
    this.rv=await this.storage.get('RVSETTINGS');
    this.type = data;
    // this.user_type=data;
    let url = this.config.domain_url + 'user_branches';
    let body = {
      company_id: cid,
      role_id: data,
      user_id:id
    }

    console.log("role:", data);
    const bname = await this.storage.get("BNAME");
    if (data == '1') {
      console.log("role1");

      this.http.post(url, body,{headers}).subscribe((res: any) => {
        this.branch = res.data;
        console.log("branchforSadmin:", this.branch);

        //  this.branch_id=this.branch[0].id;
        //  this.branch1=this.branch[0].name;
       
        if (bname) {
          this.branch1 = bname;
        } else {
          this.branch1 = this.branch[0].name;
          this.storage.set("BRANCH", this.branch[0].id.toString());
          this.storage.set("BNAME", this.branch1)
          this.storage.set("TIMEZONE", this.branch[0].timezone);
          this.storage.set('RVSETTINGS', this.branch[0].retirement_living);
          this.storage.set("PCFBRANCH",this.branch[0].pcs_branch_id);
          this.rv=this.branch[0].retirement_living;
        }
        if (bid == 0) {
          this.branch1 = this.branch[0].name;
          this.storage.set("BRANCH", this.branch[0].id.toString());
          this.storage.set("BNAME", this.branch1)
          this.storage.set("TIMEZONE", this.branch[0].timezone);
          this.storage.set('RVSETTINGS', this.branch[0].retirement_living)
          this.storage.set("PCFBRANCH",this.branch[0].pcs_branch_id);
          this.rv=this.branch[0].retirement_living;
        }
      })


    } else {
     
     
      console.log("not1:",  "branch_id:", bid);
      console.log(url, body);

      this.http.post(url, body,{headers}).subscribe((res: any) => {
        console.log('branch:',res)
        this.branch = res.data;
        //  this.branch_id=this.branch[0].id;
        //  this.branch1=this.branch[0].name;
        // const bname = this.storage.get("BNAME")
        // if (bname) {
        //   this.branch1 = bname;
        // } else {
        //   this.branch1 = this.branch[0].name;

        // }
        if (bname) {
          this.branch1 = bname;
        } else {
          this.branch1 = this.branch[0].name;
          this.storage.set("BRANCH", this.branch[0].id.toString());
          this.storage.set("BNAME", this.branch1)
          this.storage.set("TIMEZONE", this.branch[0].timezone);
          this.storage.set('RVSETTINGS', this.branch[0].retirement_living);
          this.storage.set("PCFBRANCH",this.branch[0].pcs_branch_id);
          this.rv=this.branch[0].retirement_living;
        }
        if (bid == 0) {
          this.branch1 = this.branch[0].name;
          this.storage.set("BRANCH", this.branch[0].id.toString());
          this.storage.set("BNAME", this.branch1)
          this.storage.set("TIMEZONE", this.branch[0].timezone);
          this.storage.set('RVSETTINGS', this.branch[0].retirement_living);
          this.storage.set("PCFBRANCH",this.branch[0].pcs_branch_id);
          this.rv=this.branch[0].retirement_living;
        }

      })
    }
    
    //   if(this.user_type!=4){
    //   // const tabBar = document.getElementById('myTabBar');
    //   // tabBar.style.display="flex";
    //   }


    // console.log("branchinit:",this.branch_id);
    // if(this.flag==1){
    (await this.config.getUserPermission()).subscribe((res: any) => {
      console.log('permissions:', res);
      let routes = res.user_routes;
      
      if (res.main_permissions.maintenance==1&&routes.includes('maintenance.index')) {

        this.maintenance_view = true
      } else {

        this.maintenance_view = false;
      }

      if (res.main_permissions.communication==1&&routes.includes('message')) {

        this.message_handle = true
      } else {

        this.message_handle = false
      }

      // activity permissions
      if (res.main_permissions.life_style==1&&routes.includes('activity.index')) {

        this.act_view = true
      } else {

        this.act_view = false;
      }

      if (res.main_permissions.life_style==1&&routes.includes('story.index')) {

        this.story_view = true
      } else {

        this.story_view = false;
      }
      if (res.main_permissions.life_style==1&&routes.includes('resource.index')) {

        this.res_view = true
      } else {

        this.res_view = false;
      }
      if (res.main_permissions.residents==1){
          if(this.rv==0&&routes.includes('residents.index')||this.rv==1&&routes.includes('rv_resident.index')) {

          this.user_view = true
        } else {

          this.user_view = false;
        }
      }
      if (res.main_permissions.dining==1) {

        this.dining_view = true
      } else {

        this.dining_view = false;
      }
      if (res.main_permissions.communication==1&&routes.includes('call_booking.index')) {

        this.call_view = true
      } else {

        this.call_view = false;
      }
      if (res.main_permissions.visitors==1&&routes.includes('visitor_booking.index')) {

        this.vis_view = true
      } else {

        this.vis_view = false;
      }
      if (res.main_permissions.feedback==1&&routes.includes('PCF')) {

        this.feedback = true
      } else {

        this.feedback = false;
      }
      if (res.main_permissions.rv==1&&routes.includes('task.index')) {

        this.task_view = true
      } else {

        this.task_view = false;
      }
    })
    // }
    

    // let headers=await this.config.getHeader();
    let url1 = this.config.domain_url + 'notification';
    let not_body={
      id:id,
      offset:0,
      limit:20,
      staff_app:1
    }
    this.http.post(url1, not_body,{ headers }).subscribe((res: any) => {
     
        this.notify = res.total_count;
        
      
      
      console.log("len:", this.notify, res, url1);

    })
    let m_url = this.config.domain_url + 'msgcount';

    let body1 = {
      user_one: id
    }
    this.http.post(m_url, body1,{headers}).subscribe((mes: any) => {

      this.msgcount = mes.data.msgcount;
      console.log("mes:", this.msgcount,mes);

    })


    this.pushNotificationSettings();
    // const bid=await this.storage.get("BRANCH")

    // const uuid = await this.storage.get("uuid")
    // const token = await this.storage.get('USERTOKEN');
    // console.log("uuid", uuid);
    // let durl = this.config.domain_url + 'devicetoken';
    // let device;
    // if (this.platform.is('ios')) {
    //   device = 2
    // }
    // else if (this.platform.is('android')) {
    //   device = 1
    // }

    // let dbody;
    // dbody = {
    //   user_id: id,
    //   device_type: device,
    //   device_token: uuid,
    //   app_version: 1,
    //   user_token: token
    // }
    // const log_count=await this.storage.get('USERLOG');
    // if(log_count==1){
    //   dbody.userlog=1
    // }
    // // let headers=await this.config.getHeader();;
    // console.log("body:", dbody);
    // this.http.post(durl, dbody, { headers }).subscribe((res: any) => {
    //   console.log("device:", res);
    //   this.storage.set('USERLOG',0);
    //   if (res.message == 'Token Expired Please login again') {
    //     this.storage.clear();
    //     this.storage.set("loggedIn", 0);
    //     this.storage.set("launched", true);
    //     this.router.navigateByUrl('/');
    //   }
    // }, error => {
    //   console.log(error);

    // })



    const log = await this.storage.get("firstLogin")
    if (log == 0) {
      let url = this.config.domain_url + 'update_first_login';
      
      let body = { id: data }
      this.http.post(url, body,{headers}).subscribe((res: any) => {
        console.log(res);

      })
    }





    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      if (this.counter == 0) {
        this.counter++;
        this.presentAlert('Press again to exit.');
        console.log("counter");

        setTimeout(() => { this.counter = 0 }, 3000)
      }
      else {
        // this.applaunched=false;
        // this.storage.set("launched",this.applaunched);
        this.storage.set('USERLOG',0);
        navigator['app'].exitApp();
      }

    });


  }
  ionViewWillLeave() {
    this.token_generated=false;
    this.apicallednow = false;
    this.subscription.unsubscribe();
    // const tabBar = document.getElementById('myTabBar');
    // tabBar.style.display = "none";
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000
    });
    alert.present(); //update
  }
  notification() {
    this.router.navigate(['/notification', { flag: 2 }]);
  }


  async branchModal(ev) {
    if(this.branch&&this.branch.length>1){
    const popover = await this.modalCntl.create({
      component: SetBranchComponent,
      mode:'md',
      cssClass: 'branch-select-modal'

      // translucent: true
    });
    popover.onDidDismiss().then(async (dataReturned) => {
      this.rv=await this.storage.get('RVSETTINGS');
      this.branch1=await this.storage.get('BNAME');

      const cid = await this.storage.get("COMPANY_ID")
      let headers=await this.config.getHeader();
      this.ionViewWillEnter();
      // let murl = this.config.domain_url + 'get_modules/' + cid;
      // this.http.get(murl,{headers}).subscribe((mod: any) => {
      //   this.show = true;
      //   this.modules = mod
      //   console.log("modules:", this.modules);
  
      // })
    });
    return await popover.present();
  }
  }

  async openBooking() {
    const actionSheet = await this.actionsheetCntlr.create({
      // header: "Select Image source",
      cssClass: 'booking-menu',
      buttons: [{
        cssClass: 'left',
        icon: 'assets/menu/callbooking-icon.svg',
        text: 'Call booking',
        handler: () => {
          this.router.navigate(['/callbooking-list'])
        }
      },
      {
        cssClass: 'right',
        icon: 'assets/menu/visitor-booking.svg',
        text: 'Visitor booking',
        handler: () => {
          this.router.navigate(['/visitor-booking-list'])
        }
      },
      {
        icon: 'close',
        text: 'Cancel',
        role: 'cancel',
        handler: () => {

        }
      }
      ]
    });
    await actionSheet.present();
  }

  gotoFeeds(){
    if(this.modules.includes('Story')&&this.story_view){
    this.router.navigate(['/feeds'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoActivity(){
    if(this.modules.includes('Activity')&&this.act_view){
    this.router.navigate(['/activities'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoDining(){
    console.log('mod:',this.modules.includes('Dining Beta'),this.dining_view)
    if(this.rv!==1&&this.modules.includes('Dining Beta')&&this.dining_view){
    this.router.navigate(['/dining-dashboard'])
  }else{
    this.presentAlert("Access denied." )
  }
  }
  gotoResidents(){
    if(this.user_view){
      if(this.rv==1){
        this.router.navigate(['/rv-res-list'])
      }else{
    this.router.navigate(['/consumers',{flag:1}])
      }
  }else{
    this.presentAlert("Access denied." )
  }
 
  }

  async pushNotificationSettings(){
    const uuid = await this.storage.get("uuid")
          let usertoken = await this.storage.get('USERTOKEN');
          const uid=await this.storage.get('USER_ID');
          const log_count=await this.storage.get('USERLOG');
          let headers=await this.config.getHeader();
    let permStatus = await PushNotifications.checkPermissions();
        
          if (permStatus.receive === 'prompt') {
            permStatus = await PushNotifications.requestPermissions();
          }
        
          if (permStatus.receive !== 'granted') {
            throw new Error('User denied permissions!');
          }
          
          await PushNotifications.register();
      
          await PushNotifications.addListener('registration', async token => {
            console.log('Registration token: ', token);
            if(!this.token_generated){
              this.token_generated=!this.token_generated;
            this.storage.set("uuid",token.value);

            let durl = this.config.domain_url + 'devicetoken';
            let device;
            if (this.platform.is('ios')) {
              device = 2
            }
            else if (this.platform.is('android')) {
              device = 1
            }
        
            let dbody;
            dbody = {
              user_id:  uid ==await this.storage.get('loginuserid')?uid:await this.storage.get('loginuserid'),
              device_type: device,
              device_token: token.value,
              app_version: 1,
              user_token: usertoken  == await this.storage.get('checkusertoken')?usertoken:await this.storage.get('checkusertoken')
            }
           
            if(log_count==1){
              dbody.userlog=1
            }
          
            console.log("body:", dbody);

            (await this.devicetokencall(dbody)).subscribe(
              (res: any) => {
                this.storage.set("USERLOG", 0);
                console.log(res, "723");
              },
              (error) => {
                console.log(error);
              }
            );
      
            // this.http.post(durl, dbody, { headers }).subscribe((res: any) => {
            //   console.log("device:", res);
            //   this.storage.set('USERLOG',0);
            //   if (res.message == 'Token Expired Please login again') {
            //     this.storage.clear();
            //     this.storage.set("loggedIn", 0);
            //     this.storage.set("launched", true);
            //     this.router.navigateByUrl('/login');
            //   }
            // }, error => {
            //   console.log(error);
        
            // })
          }
          
          });

          
          
         
  }

  async devicetokencall(body): Promise<Observable<any>> {
    let headers = await this.config.getHeader();
    let url = this.config.domain_url + "devicetoken";
    this.apicallednow = false;
    if (!this.apicallednow) {
      if (this.userstatus == "deleted") {
        this.apicallednow = false;
      } else {
        this.apicallednow = false;
      }
      this.http.post(url, body, { headers }).subscribe(async (res: any) => {
        console.log(res, "766");
        if (res.message == "Device Token Inserted") {
          return false;
        } else if (res.message == "Device Token already there") {
          return false;
        } else if (res.message == "Token Expired Please login again") {
         
          this.router.navigateByUrl("/login");
          // let update_url = this.config.domain_url + "update_login_info";
          // let update_body = {
          //   user_id: uid,
          //   branch_id: bid,
          //   logout: 1,
          // };
          // this.http
          //   .post(update_url, update_body, { headers })
          //   .subscribe((res: any) => {
          //     console.log("login_info:", res);
          //     this.storage.set("loginedupdate", res.message);
          //   });
        }
      });
    } else {
      return new Observable();
    }
  }
}
