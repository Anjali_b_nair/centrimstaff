import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatboxPageRoutingModule } from './chatbox-routing.module';

import { ChatboxPage } from './chatbox.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatboxPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ChatboxPage]
})
export class ChatboxPageModule {}
