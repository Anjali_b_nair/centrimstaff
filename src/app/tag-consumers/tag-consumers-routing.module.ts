import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagConsumersPage } from './tag-consumers.page';

const routes: Routes = [
  {
    path: '',
    component: TagConsumersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagConsumersPageRoutingModule {}
