import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TagConsumersPage } from './tag-consumers.page';

describe('TagConsumersPage', () => {
  let component: TagConsumersPage;
  let fixture: ComponentFixture<TagConsumersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagConsumersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TagConsumersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
