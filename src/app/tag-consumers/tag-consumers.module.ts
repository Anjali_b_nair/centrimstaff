import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TagConsumersPageRoutingModule } from './tag-consumers-routing.module';

import { TagConsumersPage } from './tag-consumers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TagConsumersPageRoutingModule
  ],
  declarations: [TagConsumersPage]
})
export class TagConsumersPageModule {}
