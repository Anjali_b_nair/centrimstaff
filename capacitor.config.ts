import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ecaret.centrimstaff',
  appName: 'centrimstaff',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      ScrollEnabled: 'false',
      BackupWebStorage: 'none',
      SplashMaintainAspectRatio: 'true',
      FadeSplashScreenDuration: '300',
      SplashShowOnlyFirstTime: 'false',
      SplashScreen: 'screen',
      SplashScreenDelay: '3000',
      ShowSplashScreenSpinner: 'false',
      WKWebViewOnly: 'true',
      'deployment-target': '11.0',
      'android-minSdkVersion': '30',
      'android-targetSdkVersion': '33',
      orientation: 'portrait',
      AndroidLaunchMode: 'singleTask'
    }
  },
  plugins:{
    SplashScreen:{
      launchShowDuration:6000,
      backgroundColor:"#2d9efe",
      launchAutoHide:true,
      androidScaleType: "CENTER_CROP",
      splashFullScreen:true,
      splashImmersive:true
    },
    PushNotifications:{
      presentationOptions: ["badge", "sound", "alert"]
    }
  }
};

export default config;
